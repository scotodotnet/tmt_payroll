﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="MstLeavePaymaster.aspx.cs" Inherits="MstLeavePaymaster" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">


<!-- Mirrored from www.ait.sk/uniadmin/ by HTTrack Website Copier/3.x [XR&CO'2010], Tue, 20 Jul 2010 00:37:25 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8"><!-- /Added by HTTrack -->
<head id="hea" runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<meta name="description"  content=""/>
<meta name="keywords" content=""/>
<meta name="robots" content="ALL,FOLLOW"/>
<meta name="Author" content="AIT"/>
<meta http-equiv="imagetoolbar" content="no"/>
<title>Payroll Management Systems</title>

<link rel="stylesheet" href="css/reset.css" type="text/css"/>
<link rel="stylesheet" href="css/screen.css" type="text/css"/>
<link rel="stylesheet" href="css/fancybox.css" type="text/css"/>
<link rel="stylesheet" href="css/jquery.wysiwyg.css" type="text/css"/>
<link rel="stylesheet" href="css/jquery.ui.css" type="text/css"/>
<link rel="stylesheet" href="css/visualize.css" type="text/css"/>
<link rel="stylesheet" href="css/visualize-light.css" type="text/css"/>
<link rel="Stylesheet" href="css/form.css" type="text/css" />
<!--[if IE 7]>
	<link rel="stylesheet" type="text/css" href="css/ie7.css" />
<![endif]-->	

<link media="screen" rel="stylesheet" type="text/css" href="css/admin.css"  />
<!--[if lte IE 6]><link media="screen" rel="stylesheet" type="text/css" href="css/admin-ie.css" /><![endif]-->

<script type="text/javascript" src="js/behaviour.js"></script>

<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery.visualize.js"></script>
<script type="text/javascript" src="js/jquery.wysiwyg.js"></script>
<script type="text/javascript" src="js/tiny_mce/jquery.tinymce.js"></script>
<script type="text/javascript" src="js/jquery.fancybox.js"></script>
<script type="text/javascript" src="js/jquery.idtabs.js"></script>
<script type="text/javascript" src="js/jquery.datatables.js"></script>
<script type="text/javascript" src="js/jquery.jeditable.js"></script>
<script type="text/javascript" src="js/jquery.ui.js"></script>

<script type="text/javascript" src="js/excanvas.js"></script>
<script type="text/javascript" src="js/cufon.js"></script>
<script type="text/javascript" src="js/Geometr231_Hv_BT_400.font.js"></script>
<script type="text/javascript" src="js/script.js"></script>
<script type ="text/jscript" >
        function GoBack() {
            window.history.forward();
        }
   </script>
</head>

<body onload="GoBack();">
<div class="clear">
	
	<div class="sidebar"> <!-- *** sidebar layout *** -->
		<div class="logo clear">
			
				<img src="images/altius_ForDarkBackGround.png" alt="" class="picture" width="140" height="60" />
		
			
		</div>
		
		<div class="Elaya">
			<ul>
			<li><a href="AllHome.aspx">Home</a></li>
            
			<li><a href="MstBank.aspx">Bank Master</a></li>
		    <li><a href="MstDepartment.aspx">Department Master</a></li>
			<li><a href="MstEmployeeType.aspx">Employee Type Master</a></li>
			<li><a href="MstInsuranceCompany.aspx">Insurance Company Master</a></li>
			<li><a href="MstLeave.aspx">Leave Master</a></li>
			<li><a href="MstLeavePaymaster.aspx">Leave Pay Master</a></li>
			<li><a href="MstProbationPeriod.aspx">Probation Period Master</a></li>
			<li><a href="MstQualification.aspx">Qualification Master</a></li>
			<li><a href="UserRegistration.aspx">User Master</a></li>
			<li><a href="Default.aspx">Lagout</a></li>
			</ul>
		</div>
	
	</div>
	
	
	<div class="main"> <!-- *** mainpage layout *** -->
	<div class="main-wrap">
		<div class="header clear">
			<ul class="links clear">
			<li></li>
			<li><a href="#"> <span class="text"><h1>Payroll Management Systems</h1></span></a></li>
			<li><a href="#"><span class="text"> </span></a></li>
			<li><a href="#"> <span class="text"></span></a></li>
			</ul>
		</div>
		
		<div class="page clear">
						<div class="main-icons clear">
					<ul class="links clear">
			<li></li>
			<li><a href="#"> <span class="text"></span></a></li>
			<li><a href="#"><span class="text"> </span></a></li>
			<li><a href="#"> <span class="text"></span></a></li>
			<li><a href="#"> <span class="text"></span></a></li>
			<li><a href="#"><span class="text"> </span></a></li>
			<li><a href="#"> <span class="text"></span></a></li>
			<li><a href="#"> <span class="text"></span></a></li>
			<li><a href="#"><span class="text"> </span></a></li>
			<li><a href="#"> <span class="text"></span></a></li>
			<li><a href="#"> <span class="text"></span></a></li>
			<li><a href="#"><span class="text"> </span></a></li>
			</ul>
			</div>
			
			<!-- MODAL WINDOW -->
			<div id="modal" class="modal-window">
				<!-- <div class="modal-head clear"><a onclick="$.fancybox.close();" href="javascript:;" class="close-modal">Close</a></div> -->
				
				<div class="notification note-info">
					<a href="#" class="close" title="Close notification"><span>close</span></a>
					<span class="icon"></span>
					
				</div>
				
				</div>
			
			<!-- CONTENT BOXES -->
			<div class="content-box">
				<div >
					<ul class="tabs clear">
						<li><a href="#data-table"></a></li>
						<li><a href="#table"></a></li>
						<li><a href="#forms"></a></li>
					</ul>
					
					
				</div>
				
				
			<form id="form1" runat="server" class="form" >		
				<div class="section table_section">
						<!--[if !IE]>start title wrapper<![endif]-->
						<div class="title_wrapper">
							<h2>Leave Pay Master </h2>
							<span class="title_wrapper_left"></span>
							<span class="title_wrapper_right"></span>
						</div>
						<!--[if !IE]>end title wrapper<![endif]-->
						<!--[if !IE]>start section content<![endif]-->
						<div class="section_content">
							<!--[if !IE]>start section content top<![endif]-->
							<div class="sct">
								<div class="sct_left">
									<div class="sct_right">
										<div class="sct_left">
											<div class="sct_right">
												
												<form action="#">
												<fieldset>
												<!--[if !IE]>start table_wrapper<![endif]-->
												<div class="table_wrapper">
													<div class="table_wrapper_inner">

			<cc1:ToolkitScriptManager runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true" ID="ScriptManager1" EnablePartialRendering="true"></cc1:ToolkitScriptManager>											
											
	  <table align="center">
           <tr>
           <td><asp:Label ID="lblleaveid" runat="server" Text="Pay ID" Font-Bold="true"></asp:Label></td>
           <td><asp:TextBox ID="txtleaveid" runat="server"></asp:TextBox></td>
           </tr>
           <tr>
           <td><asp:Label ID="lblleavepaystaff" runat="server" Text ="Leave Pay Staff in Days" Font-Bold="true"></asp:Label></td>
           <td><asp:TextBox ID="txtleavepaystaff" runat="server" ></asp:TextBox></td>
           </tr>
           <tr>
           <td colspan="2"><asp:Button ID="btnsave" runat="server" Text="Save" 
                   onclick="btnsave_Click" /></td>
           </tr>
       
        
           </table>
													</div>
												</div>
	</form>								
												
												
												</fieldset>
												</form>
												
												
									
				
					
					
						<form method="post" action="#">
						
					
						
					
						</form>
					</div><!-- /#table -->
					
					<!-- TABLE -->
					<!-- /#table -->
					
					<!-- Custom Forms -->
					<!-- /#forms -->
				</div> <!-- end of box-body -->
			</div> <!-- end of content-box -->
			
		
			
		
			
		
			
		</div><!-- end of page -->
		
		<div class="footer clear">
			<span class="copy"><strong>© 2012 Copyright by <a href="http://www.altius.co.in"/>Altius Infosystems.</a></strong></span> Powered by <a href="http://www.altius.co.in/">Altius.</a>
		</div>
	</div>
	</div>
</div>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-12958851-7']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</body>

<!-- Mirrored from www.ait.sk/uniadmin/ by HTTrack Website Copier/3.x [XR&CO'2010], Tue, 20 Jul 2010 00:38:01 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8"><!-- /Added by HTTrack -->
</html>
