﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Data.SqlClient;

public partial class MstEmployeeType_1 : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    MastersClass objMas = new MastersClass();
    string SessionAdmin;
    static string lblprobation_Common;
    string SessionCcode;
    string SessionLcode;
    protected void Page_Load(object sender, EventArgs e)
    {
        
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        string ss = Session["UserId"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        if (!IsPostBack)
        {
            MstEmployeeTypeDisplay();
        }
        lblComany.Text = SessionCcode + " - " + SessionLcode;
        lblusername.Text = Session["Usernmdisplay"].ToString();
    }
    protected void btnsave_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        if (rbtstaffloaber.SelectedValue == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select Category ');", true);
            //System.Windows.Forms.MessageBox.Show("", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            ErrFlag = true;
        }
        else if (txtemptypcd.Text == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Employee Type code');", true);
            //System.Windows.Forms.MessageBox.Show("Enter the Employee type Code", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            ErrFlag = true;
        }

        else if (txtemptyp.Text == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Employee Type Name');", true);
            //System.Windows.Forms.MessageBox.Show("Enter the Employee type", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            ErrFlag = true;
        }
        if (!ErrFlag)
        {
            string empcd = objdata.EmpType_check(txtemptypcd.Text, rbtstaffloaber.SelectedValue);
            string empname = objdata.EmpTypeName_check(txtemptyp.Text, rbtstaffloaber.SelectedValue);
            if (empcd.Trim() == txtemptypcd.Text.Trim())
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Employee Type Code Already Exist');", true);
                //System.Windows.Forms.MessageBox.Show("Employee type Code Already Exist", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            if (empname.Trim().ToLower() == txtemptyp.Text.Trim().ToLower())
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Employee Type Already Exist');", true);
                //System.Windows.Forms.MessageBox.Show("Employee type Already Exist", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            if (!ErrFlag)
            {
                objMas.EmpTypeCd = txtemptypcd.Text;
                objMas.EmpType = txtemptyp.Text;
                objMas.EmpCategory = rbtstaffloaber.SelectedValue;

                objdata.MstEmployeeType(objMas);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Employee Save Successfully...');", true);
                //System.Windows.Forms.MessageBox.Show("Employee Saved Successfully", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                txtemptyp.Text = "";
                txtemptypcd.Text = "";
                MstEmployeeTypeDisplay();
            }
        }
    }
    public void MstEmployeeTypeDisplay()
    {
        DataTable dt = new DataTable();

        dt = objdata.MstEmployeeTypeDisplay();
        gvEmployeeType.DataSource = dt;
        gvEmployeeType.DataBind();


    }

    protected void GvEmployeeType_Editing(object sender, GridViewEditEventArgs e)
    {
        gvEmployeeType.EditIndex = e.NewEditIndex;
        MstEmployeeTypeDisplay();
    }
    protected void GvEmployeeType_Canceling(object sender, GridViewCancelEditEventArgs e)
    {
        gvEmployeeType.EditIndex = -1;
        MstEmployeeTypeDisplay();
    }
    protected void GvEmployeeType_Editing(object sender, GridViewUpdateEventArgs e)
    {
        try
        {
            bool ErrFlag = false;
            string staff = "";
            TextBox txtempty1 = (TextBox)gvEmployeeType.Rows[e.RowIndex].FindControl("txtleavetype");
            Label lblstaff = (Label)gvEmployeeType.Rows[e.RowIndex].FindControl("lblcategry");
            if (txtempty1.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Employee Type Name');", true);
                //System.Windows.Forms.MessageBox.Show("Enter the Employee type", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            if (!ErrFlag)
            {
                if (lblstaff.Text == "Staff")
                {
                    staff = "1";
                }
                else if (lblstaff.Text == "Labour")
                {
                    staff = "2";
                }
                string empname = objdata.EmpTypeName_check(txtempty1.Text, staff);
                if (empname.Trim().ToLower() == txtempty1.Text.Trim().ToLower())
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Employee Type Already Exist');", true);
                    //System.Windows.Forms.MessageBox.Show("Employee type Already Exist", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    ErrFlag = true;
                }
                if (!ErrFlag)
                {
                    Label lblemptyCd1 = (Label)gvEmployeeType.Rows[e.RowIndex].FindControl("lbllaevecd");
                    objMas.EmpTypeCd = lblemptyCd1.Text;
                    objMas.EmpType = txtempty1.Text;

                    objdata.UpdateEmployeeType(objMas);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Employee Type Update Successfully...');", true);
                    //System.Windows.Forms.MessageBox.Show("Employee type Updated Successfully", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    gvEmployeeType.EditIndex = -1;
                    MstEmployeeTypeDisplay();
                }
            }
        }
        catch (Exception ex)
        {

        }
    }
    protected void gvEmployeeType_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        bool ErrFlag = false;
        string constr = ConfigurationManager.AppSettings["ConnectionString"];
        SqlConnection con = new SqlConnection(constr);
        Label lblEmpId = (Label)gvEmployeeType.Rows[e.RowIndex].FindControl("lbllaevecd");
        string Typeid = objdata.EmpType_delete(lblEmpId.Text);
        lblprobation_Common = lblEmpId.Text;
        if (Typeid.Trim() == lblEmpId.Text.Trim())
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Employee type cannot be Deleted...');", true);
            //System.Windows.Forms.MessageBox.Show("Employee type cannot be deleted", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            ErrFlag = true;
        }
        if (!ErrFlag)
        {
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "script", "showConfirm();", true);
            //System.Windows.Forms.DialogResult dres = System.Windows.Forms.MessageBox.Show("Are you Sure want to Delete ?", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.YesNo, System.Windows.Forms.MessageBoxIcon.Warning);
            //if (dres.ToString() == "Yes")
            //{
            //    string qry = "Delete from MstEmployeeType where EmpTypeCd = '" + lblEmpId.Text + "'";
            //    SqlCommand cmd = new SqlCommand(qry, con);
            //    con.Open();
            //    cmd.ExecuteNonQuery();
            //    con.Close();
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Deleted Successfully...');", true);
            //    //System.Windows.Forms.MessageBox.Show("Deleted Successfully", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            //    MstEmployeeTypeDisplay();
            //}

        }
    }

    protected void cmdOverriteGrade_Click(object sender, EventArgs e)
    {
        try
        {
            string constr = ConfigurationManager.AppSettings["ConnectionString"];
            SqlConnection con = new SqlConnection(constr);
            //Label lblbank = (Label)gvBank.Rows[e.RowIndex].FindControl("lblBankcd");
            string qry = "Delete from MstEmployeeType where EmpTypeCd = '" + lblprobation_Common + "'";
            SqlCommand cmd = new SqlCommand(qry, con);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Deleted Successfully...');", true);
            //System.Windows.Forms.MessageBox.Show("Deleted Successfully", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            MstEmployeeTypeDisplay();

        }
        catch (Exception ex)
        {
        }
    }

    protected void btncancel_Click(object sender, EventArgs e)
    {
        txtemptyp.Text = "";
        txtemptypcd.Text = "";
        MstEmployeeTypeDisplay();
    }
    protected void btnEmp_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptEmpDownload.aspx");
    }
    protected void btnatt_Click(object sender, EventArgs e)
    {
        Response.Redirect("AttenanceDownload.aspx");
    }
    protected void btnLeave_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptLeaveSample.aspx");
    }
    protected void btnOT_Click(object sender, EventArgs e)
    {
        Response.Redirect("OTDownload.aspx");
    }
    protected void btncontract_Click(object sender, EventArgs e)
    {
        Response.Redirect("ContractRPT.aspx");
    }
    protected void btnSal_Click(object sender, EventArgs e)
    {
        Response.Redirect("FrmDeductionDownload.aspx");
    }
}
