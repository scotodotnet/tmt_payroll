﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using Payroll;
using Payroll.Configuration;
using Payroll.Data;

using Altius.BusinessAccessLayer.BALDataAccess;

public partial class RptAdvance_1 : System.Web.UI.Page
{
    protected System.Resources.ResourceManager rm;
    protected string PayrollRegisterContentMeta, pageTitle;
    BALDataAccess objdata = new BALDataAccess();
    bool searchFlag = false;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    private void clr()
    {
        //lblEmpName1.Text = "";
        lbldesignation1.Text = "";
        lblDepartment1.Text = "";
        lbldop1.Text = "";
        DataTable dtempty = new DataTable();
        gvadvance.DataSource = dtempty;
        gvadvance.DataBind();
        gvHistory.DataSource = dtempty;
        gvadvance.DataBind();
        panelAdvance.Visible = false;
        panelhistory.Visible = false;

    }

    protected void Page_Load(object sender, EventArgs e)
    {

        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        string ss = Session["UserId"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        lblComany.Text = SessionCcode + " - " + SessionLcode;
        if (!IsPostBack)
        {
            category();
            //DropDownDepart();
        }
        lblusername.Text = Session["Usernmdisplay"].ToString();
    }
    public void category()
    {
        DataTable dtcate = new DataTable();
        dtcate = objdata.DropDownCategory();
        ddlcategory.DataSource = dtcate;
        ddlcategory.DataTextField = "CategoryName";
        ddlcategory.DataValueField = "CategoryCd";
        ddlcategory.DataBind();
    }
    public void DropDownDepart()
    {
        DataTable dt = new DataTable();
        dt = objdata.Depart_Labour();
        ddldepartment.DataSource = dt;
        ddldepartment.DataTextField = "DepartmentNm";
        ddldepartment.DataValueField = "DepartmentCd";
        ddldepartment.DataBind();
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        panelhistory.Visible = false;
        string dpt = "";
        DataTable dt = new DataTable();
        clr();

        if (((txtExist.Text == "")))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Enter the Employee Number or Existing Number...!');", true);
            //System.Windows.Forms.MessageBox.Show("Enter the Employee Number or Existing Number...!", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            ErrFlag = true;
        }
        if (!ErrFlag)
        {
            //dt = objdata.AdvanceSalary_EmpNo(txtemp.Text);
            if (txtExist.Text != "")
            {
                if (SessionAdmin == "1")
                {
                    dt = objdata.AdvanceSalary_exist(txtExist.Text, ddldepartment.SelectedValue, SessionCcode, SessionLcode);
                }
                else
                {
                    dt = objdata.AdvanceSalary_exist_user(txtExist.Text, ddldepartment.SelectedValue, SessionCcode, SessionLcode);
                }
            }
            if (dt.Rows.Count > 0)
            {
                panelAdvance.Visible = true;
                searchFlag = true;
                txtExist.Text = dt.Rows[0]["ExisistingCode"].ToString();
                ddlEmpNo.SelectedValue = dt.Rows[0]["EmpNo"].ToString();
                ddlEmpName.SelectedValue = dt.Rows[0]["EmpNo"].ToString();
                lbldop1.Text = dt.Rows[0]["Dateofjoining"].ToString();
                lblDepartment1.Text = dt.Rows[0]["DepartmentNm"].ToString();
                lbldesignation1.Text = dt.Rows[0]["Designation"].ToString();
                DataTable dt1 = new DataTable();
                dt1 = objdata.ADV_ALL_REPORT(dt.Rows[0]["EmpNo"].ToString());
                gvadvance.DataSource = dt1;
                gvadvance.DataBind();

            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Employee Number Not Found....!');", true);
                //System.Windows.Forms.MessageBox.Show("Employee Number Not Found....!", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);

            }
        }
        searchFlag = false;
    }
    protected void txtemp_TextChanged(object sender, EventArgs e)
    {
        if (searchFlag == false)
        {
            clr();
            txtExist.Text = "";
        }
    }
    protected void txtExist_TextChanged(object sender, EventArgs e)
    {
        if (searchFlag == false)
        {
            clr();

        }
    }
    protected void gvadvance_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {

        Label labelID_pass = (Label)gvadvance.Rows[e.NewSelectedIndex].FindControl("lblId");
        DataTable dt = new DataTable();
        gvHistory.DataSource = dt;
        gvHistory.DataBind();
        dt = objdata.Adv_history(labelID_pass.Text, SessionCcode, SessionLcode);
        if (dt.Rows.Count > 0)
        {
            gvHistory.DataSource = dt;
            gvHistory.DataBind();
            panelhistory.Visible = true;
            panelAdvance.Visible = false;
        }

    }
    protected void btnback_Click(object sender, EventArgs e)
    {
        Response.Redirect("SalaryAdvance.aspx");
    }
    protected void btnclr_Click(object sender, EventArgs e)
    {
        clr();
        //txtemp.Text = "";
        txtExist.Text = "";
        DataTable dtempty = new DataTable();
        ddlEmpName.DataSource = dtempty;
        ddlEmpName.DataBind();
        ddlEmpNo.DataSource = dtempty;
        ddlEmpNo.DataBind();
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        panelhistory.Visible = false;
        panelAdvance.Visible = true;

    }
    protected void ddldepartment_SelectedIndexChanged(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        DataTable dtempty = new DataTable();
        ddlEmpNo.DataSource = dtempty;
        ddlEmpNo.DataBind();
        ddlEmpName.DataSource = dtempty;
        ddlEmpName.DataBind();
        txtExist.Text = "";
        string stafforLabour = "";
        if (ddldepartment.SelectedValue == "0")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select the Department Properly....!');", true);
            //System.Windows.Forms.MessageBox.Show("Select the Department Properly....!", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            ErrFlag = true;
        }
        if (!ErrFlag)
        {

            DataTable dtempcode = new DataTable();
            if (ddlcategory.SelectedValue == "1")
            {
                stafforLabour = "S";
            }
            else if (ddlcategory.SelectedValue == "2")
            {
                stafforLabour = "L";
            }
            
            if (SessionAdmin == "1")
            {
                dtempcode = objdata.LoadEmployeeForBonus(stafforLabour, ddldepartment.SelectedValue, SessionCcode, SessionLcode);
            }
            else
            {
                dtempcode = objdata.EmpLoadFORBonus_USER(stafforLabour, ddldepartment.SelectedValue, SessionCcode, SessionLcode);
            }
            ddlEmpNo.DataSource = dtempcode;
            DataRow dr = dtempcode.NewRow();
            dr["EmpNo"] = ddldepartment.SelectedItem.Text;
            dr["EmpName"] = ddldepartment.SelectedItem.Text;
            dtempcode.Rows.InsertAt(dr, 0);

            ddlEmpNo.DataTextField = "EmpNo";
            ddlEmpNo.DataValueField = "EmpNo";
            ddlEmpNo.DataBind();
            ddlEmpName.DataSource = dtempcode;
            ddlEmpName.DataTextField = "EmpName";
            ddlEmpName.DataValueField = "EmpNo";
            ddlEmpName.DataBind();
        }
        clr();
    }
    protected void ddlEmpNo_SelectedIndexChanged(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        panelhistory.Visible = false;
        clr();
        if (ddlEmpNo.SelectedValue == "0")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Employee Code....!');", true);
            //System.Windows.Forms.MessageBox.Show("Select the Employee Code....!", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            ErrFlag = true;
        }
        if (!ErrFlag)
        {
            panelAdvance.Visible = true;
            DataTable dt = new DataTable();
            dt = objdata.AdvanceSalary_EmpNo(ddlEmpNo.SelectedValue, SessionCcode, SessionLcode);
            if (dt.Rows.Count > 0)
            {
                ddlEmpName.DataSource = dt;
                ddlEmpName.DataTextField = "EmpName";
                ddlEmpName.DataValueField = "EmpNo";
                ddlEmpName.DataBind();
                txtExist.Text = dt.Rows[0]["ExisistingCode"].ToString();
                //txtemp.Text = dt.Rows[0]["EmpNo"].ToString();
                //lblEmpName1.Text = dt.Rows[0]["EmpName"].ToString();
                lbldop1.Text = dt.Rows[0]["Dateofjoining"].ToString();
                lblDepartment1.Text = dt.Rows[0]["DepartmentNm"].ToString();
                lbldesignation1.Text = dt.Rows[0]["Designation"].ToString();
                DataTable dt1 = new DataTable();
                dt1 = objdata.ADV_ALL_REPORT(ddlEmpNo.SelectedValue);
                gvadvance.DataSource = dt1;
                gvadvance.DataBind();
            }
        }
        else
        {
            panelAdvance.Visible = false;
        }
    }
    protected void ddlEmpName_SelectedIndexChanged(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        panelhistory.Visible = false;
        clr();
        if (ddlEmpName.SelectedValue == "0")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Employee Code....!');", true);
            //System.Windows.Forms.MessageBox.Show("Select the Employee Code....!", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            ErrFlag = true;
        }

        if (!ErrFlag)
        {
            panelAdvance.Visible = true;
            DataTable dt = new DataTable();
            dt = objdata.AdvanceSalary_EmpNo(ddlEmpName.SelectedValue, SessionCcode, SessionLcode);
            if (dt.Rows.Count > 0)
            {
                ddlEmpNo.DataSource = dt;
                ddlEmpNo.DataTextField = "EmpNo";
                ddlEmpNo.DataValueField = "EmpNo";
                ddlEmpNo.DataBind();
                txtExist.Text = dt.Rows[0]["ExisistingCode"].ToString();
                //txtemp.Text = dt.Rows[0]["EmpNo"].ToString();
                //lblEmpName1.Text = dt.Rows[0]["EmpName"].ToString();
                lbldop1.Text = dt.Rows[0]["Dateofjoining"].ToString();
                lblDepartment1.Text = dt.Rows[0]["DepartmentNm"].ToString();
                lbldesignation1.Text = dt.Rows[0]["Designation"].ToString();
                DataTable dt1 = new DataTable();
                dt1 = objdata.ADV_ALL_REPORT(ddlEmpNo.SelectedValue);
                gvadvance.DataSource = dt1;
                gvadvance.DataBind();
            }
        }
        else
        {
            panelAdvance.Visible = false;
        }
    }
    protected void btnEmp_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptEmpDownload.aspx");
    }
    protected void btnatt_Click(object sender, EventArgs e)
    {
        Response.Redirect("AttenanceDownload.aspx");
    }
    protected void btnLeave_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptLeaveSample.aspx");
    }
    protected void btnOT_Click(object sender, EventArgs e)
    {
        Response.Redirect("OTDownload.aspx");
    }
    protected void ddlcategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        string staffLabour_temp;
        DataTable dtempty = new DataTable();
        ddldepartment.DataSource = dtempty;
        ddldepartment.DataBind();
        if (ddlcategory.SelectedValue == "1")
        {
            staffLabour_temp = "S";
        }
        else if (ddlcategory.SelectedValue == "2")
        {
            staffLabour_temp = "L";
        }
        else
        {
            staffLabour_temp = "0";
        }
        DataTable dtDip = new DataTable();
        dtDip = objdata.DropDownDepartment_category(staffLabour_temp);
        if (dtDip.Rows.Count > 1)
        {
            ddldepartment.DataSource = dtDip;
            ddldepartment.DataTextField = "DepartmentNm";
            ddldepartment.DataValueField = "DepartmentCd";
            ddldepartment.DataBind();
        }
        else
        {
            DataTable dt = new DataTable();
            ddldepartment.DataSource = dt;
            ddldepartment.DataBind();
        }
    }
    protected void btncontract_Click(object sender, EventArgs e)
    {
        Response.Redirect("ContractRPT.aspx");
    }
    protected void btnSal_Click(object sender, EventArgs e)
    {
        Response.Redirect("FrmDeductionDownload.aspx");
    }

}
