﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Design.aspx.cs" Inherits="Design" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
    <head runat="server">
        <title>Payroll Management Systems</title>
        <link href="new_css/main.css" rel="stylesheet" type="text/css" />
        <link href="new_css/colors/dark_blue.css" rel="stylesheet" type="text/css" />
        <link href="new_css/superfish.css" rel="stylesheet" type="text/css" />

        <script src="new_js/jquery-1.4.2.min.js" type="text/javascript"></script>
        <script src="new_js/jquery.hoverintent.minified.js" type="text/javascript"></script>
        <script src="new_js/superfish.js" type="text/javascript"></script>
        <script src="new_js/jquery.tools.min.js" type="text/javascript"></script>
        <%--<script src="new_js/jquery.simplemodal.js" type="text/javascript"></script>--%>
        <script src="new_js/jquery.notifybar.js" type="text/javascript"></script>
        <script src="new_js/jquery.tipsy.js" type="text/javascript"></script>
        <script src="new_js/enhance.js" type="text/javascript"></script>
        <script src="new_js/excanvas.js" type="text/javascript"></script>
        <script src="new_js/visualize.jquery.js" type="text/javascript"></script>
        <script src="new_js/jquery.collapsible.js" type="text/javascript"></script>
        <script src="new_js/jquery.autosuggest.packed.js" type="text/javascript"></script>
        <script src="new_js/platinum-admin.js" type="text/javascript"></script>
        <style>
            .sf-navbar li ul 
            {
	            width: 1100px; /*IE6 soils itself without this*/
	            margin: 0px auto;
	            margin-left:-20em;	            
            }
        </style>
    </head>
    <body>
        <form id="form1" runat="server">
            <cc1:ToolkitScriptManager runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true"
                ID="ScriptManager1" EnablePartialRendering="true">
            </cc1:ToolkitScriptManager>
            <div id="header">
                <div id="header-top">
                    <div id="logo">
	                    <h1>&nbsp;</h1>
	                    <span id="slogan">&nbsp;</span> 
	                </div>
                    <!-- end logo -->
                    <div id="login-info">
                        <img id="profile" alt="profile" src="./new_images/icons/user.png" />
                        <p id="top"><span id="name"><asp:Label ID="lblusername" runat="server"></asp:Label></span><br />
                        <span id="links">
                        <a id="message-link" href="#" title="click to see the messages!">
                        <span id="message-count">1</span> new message. </a></span></p>
	                    <div id="messages-box">
		                    <h4>Messages</h4>
	                        <h5><a id="new-message" href="#">New Message</a></h5>
	                        <hr />
	                        <ul id="messages">
		                        <li class="new"><a href="#">
		                            <img alt="user" src="./new_images/icons/sender.png" />
		                            <span class="sender">John Doe</span>
		                            <span class="description">Hi this is just a description...</span>
		                            <span class="date">20 minutes ago...</span> </a>
		                        </li>
		                        <li><a href="#">
		                            <img alt="user" src="./new_images/icons/sender.png" />
		                            <span class="sender">John Doe</span>
		                            <span class="description">Hi this is just a description...</span>
		                            <span class="date">24 minutes ago...</span> </a>
		                        </li>
		                        <li><a href="#">
		                            <img alt="user" src="./new_images/icons/sender.png" />
		                            <span class="sender">John Doe</span>
		                            <span class="description">Hi this is just a description...</span>
		                            <span class="date">50 minutes ago...</span> </a>
		                        </li>
		                        <li id="last"><a id="all-messages" href="#">See all messages!
		                            <span id="unreaded">8 unreaded.</span> </a>
		                        </li>
		                    </ul>
		                    <br class="clear" />
	                    </div><!-- end messages-box -->
	                    <a id="power" href="#" title="Logout">logout</a>
                    </div>
                    <!-- end login -->
                    <div id="nav">
	                    <ul class="sf-menu sf-navbar">
	                        <li><a id="dashboard" href="Dashboard.aspx">Dashboard</a>
	                            <ul>
		                            <li>&nbsp;
		                            </li>
	                            </ul>
	                        </li>
	                        <li class="current"><a id="Employee" href="#a">Employee</a>
	                            <ul>
		                            <li class="current"><a href="EmployeeRegistration.aspx">Employee Registration</a> </li>					                    
		                            <li><a href="OfficalProfileDetails.aspx">Official Profile</a></li>
		                            <li><a href="LeaveAllocationEmployee.aspx">Leave Allocation</a></li>
		                            <li><a href="ApplyForLeave.aspx">Apply For Leave</a></li>					                    
		                            <li><a href="SalaryRegistration.aspx">Salary Details</a></li>
		                            <li><a href="SalaryAdvance.aspx">Salary Advance</a></li>
		                            <li><a href="EmployeeDeactiveMode.aspx">Employee Deactivate</a></li>
		                            <li><a href="UploadEmployee.aspx">Employee Upload</a></li>					                    
	                            </ul>
	                        </li>
	                        <li><a id="Bonus" href="#">Bonus</a>
	                            <ul>
		                            <li><a href="#">Bonus</a></li>
		                            <li><a href="#">Group Bonus</a></li>					                    
	                            </ul>
	                        </li>
	                        <li><a id="ESI_PF" href="#">ESI & PF</a>
	                            <ul>
		                            <li><a href="#">ESI Half Yearly Statement</a></li>
		                            <li><a href="#">ESI Form 6</a></li>
		                            <li><a href="#">PF Monthly Statement</a></li>
	                            </ul>
	                        </li>
	                        <li><a id="Master" href="#">Masters</a>
	                            <ul>
		                            <li><a href="#">ESI Half Yearly Statement</a></li>
		                            <li><a href="#">ESI Form 6</a></li>
		                            <li><a href="#">PF Monthly Statement</a></li>
	                            </ul>
	                        </li>
	                        <li><a id="Reports" href="#">Reports</a>
	                            <ul>
		                            <li><a href="#">ESI Half Yearly Statement</a></li>
		                            <li><a href="#">ESI Form 6</a></li>
		                            <li><a href="#">PF Monthly Statement</a></li>
	                            </ul>
	                        </li>
                        </ul>
                    </div>
                    <!-- end div#nav -->
                </div>
                <!-- end div#header-top -->
                <div class="breadCrumb">
                    <ul>
                        <li class="first"><a href="#">Home</a> </li>
                        <li><a href="#">Users</a> </li>
                        <li><a href="#">Roles</a> </li>
                        <li class="last">Employee Registration </li>
                    </ul>
                </div>
                <!-- end div#breadCrumb -->
                <%--<div id="search-box">
                    <form id="searchform" action="" method="get">
                        <fieldset class="search"><span>Search</span>
                        <input class="box" type="text" />
                        <button class="btn" title="Submit Search">Search</button></fieldset>
                    </form>
                </div>--%>
                <!-- end div#search-box -->
            </div>
            <!-- end header -->
            <div id="page-wrap">
                <div id="right-sidebar">
                    <div class="innerdiv">
	                    <h2 class="head">Comments</h2>
	                    <div class="innercontent clear">
		                    <h4>Recent Comments</h4>
		                    <ul id="comments" class="tooltip-enabled">
		                        <li><a href="#" title="this comment is from John Doe">
		                            <img alt="user" src="./new_images/icons/sender.png" />
		                            <span class="sender">John Doe</span>
		                            <span class="description">Hey i liked your theme!</span>
		                            <span class="date">20 minutes ago...</span> </a>
		                            <span class="floatright"><a title="Aprrove this comment" href="#">Approve</a> | 
		                            <a title="Delete this comment"  href="#">Delete</a>
		                            </span>
		                            <br class="clear" />
		                        </li>
		                        <li><a href="#" title="again this guy!">
		                            <img alt="user" src="./new_images/icons/sender.png" />
		                            <span class="sender">John Doe</span>
		                            <span class="description">Hi, i need some help.</span>
		                            <span class="date">24 minutes ago...</span> </a>
		                            <span class="floatright"><a title="Aprrove this comment"  href="#">Approve</a> | 
		                            <a title="Delete this comment"  href="#">Delete</a>
		                            </span>
		                            <br class="clear" />
		                        </li>
		                        <li><a href="#" title="is he a spammer?">
		                            <img alt="user" src="./new_images/icons/sender.png" />
		                            <span class="sender">John Doe</span>
		                            <span class="description">Thank you for your help.</span>
		                            <span class="date">50 minutes ago...</span> </a>
		                            <span class="floatright"><a title="Aprrove this comment"  href="#">Approve</a> | 
		                            <a title="Delete this comment"  href="#">Delete</a>
		                            </span>
		                            <br class="clear" />
		                        </li>
		                        <li class="last"><a class="all-messages" title="See all comments awaiting confirmation." href="#">See all comments!
		                            <span class="unreaded">8 awaiting confirm.</span> </a>
		                        </li>
		                    </ul>
	                    </div>
                    </div>
                    <div class="clear"></div>                    
                </div>
	            <!-- end right-sidebar -->                
            </div>
            <!-- end page-wrap -->
            <div id="footer">
                <ul>
                    <li>Copyright &copy; 2012. All rights reserved.</li>
                    <li>Powered by <a href="http://www.altius.co.in" target="_blank">Altius Infosystems</a></li>
                </ul>
            </div>
            <!-- end footer -->
        </form>
    </body>
</html>
