﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;
using Payroll;
using Payroll.Data;
using Payroll.Configuration;
using Altius.BusinessAccessLayer.BALDataAccess;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html;
using iTextSharp.text.html.simpleparser;


using Altius.BusinessAccessLayer.BALDataAccess;


public partial class RptOT : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    string SessionAdmin;
    string Stafflabour;
    string SessionCcode;
    string SessionLcode;
    DateTime dfrom;
    DateTime dtto;
    DateTime MyDate1;
    DateTime MyDate2;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionAdmin = Session["Isadmin"].ToString();
        lblusername.Text = Session["Usernmdisplay"].ToString();
        string ss = Session["UserId"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        lblComany.Text = SessionCcode + " - " + SessionLcode;
        if (!IsPostBack)
        {
            int currentYear = Utility.GetFinancialYear;
            for (int i = 0; i < 10; i++)
            {
                ddlfinance.Items.Add(new System.Web.UI.WebControls.ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                //  ddlShowYear.Items.Add(new ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                currentYear = currentYear - 1;
            }
            //DropDownDepart();
            Months_load();
            DropDwonCategory();
        }
    }
    public void Months_load()
    {
        DataTable dt = new DataTable();
        dt = objdata.months_load();
        ddlMonths.DataSource = dt;
        ddlMonths.DataTextField = "Months";
        ddlMonths.DataValueField = "Months";
        ddlMonths.DataBind();
    }
    public void DropDwonCategory()
    {
        DataTable dtcate = new DataTable();
        dtcate = objdata.DropDownCategory();
        ddlcategory.DataSource = dtcate;
        ddlcategory.DataTextField = "CategoryName";
        ddlcategory.DataValueField = "CategoryCd";
        ddlcategory.DataBind();
    }
    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }
    protected void btnExport_Click(object sender, EventArgs e)
    {
        try
        {
            bool ErrFlag = false;
            if (ddlfinance.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Financial year....!');", true);
                ErrFlag = true;
            }
            else if (ddlMonths.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Months..!');", true);
                ErrFlag = true;
            }
            else if (ddlcategory.SelectedValue == "" || ddlcategory.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Category');", true);
                ErrFlag = true;
            }
            else if (ddlEmployeeType.SelectedValue == "" || ddlEmployeeType.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Employee Type');", true);
                ErrFlag = true;
            }
            else if (txtfrom.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the From Date');", true);
                ErrFlag = true;
            }
            else if (txtTo.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the To Date');", true);
                ErrFlag = true;
            }
            if (!ErrFlag)
            {
                dfrom = Convert.ToDateTime(txtfrom.Text);
                dtto = Convert.ToDateTime(txtTo.Text);
                MyDate1 = DateTime.ParseExact(txtfrom.Text, "dd-MM-yyyy", null);
                MyDate2 = DateTime.ParseExact(txtTo.Text, "dd-MM-yyyy", null);
                if (dfrom > dtto)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Check From Data and To Date...');", true);
                    txtfrom.Text = null;
                    txtTo.Text = null;
                    ErrFlag = true;
                }
            }

            if (!ErrFlag)
            {
                string staff_lab = "";
                string employee_type = "";
                string query = "";
                if (ddlcategory.SelectedValue == "1")
                {
                    staff_lab = "S";
                }
                else if (ddlcategory.SelectedValue == "2")
                {
                    staff_lab = "L";
                }
                employee_type = ddlEmployeeType.SelectedValue.ToString();

                query = "Select OT.EmpNo,ED.ExisistingCode,OT.EmpName,OT.Department,OT.NoHrs,OT.Month,OT.netAmount,OT.Financialyr,OT.HrSalary,OT.ChkManual," +
                        " CAST(right(ED.ExisistingCode,len(ED.ExisistingCode)-1) AS int) as OrderToken, MSD.DepartmentNm" +
                        " from OverTime OT inner Join MstDepartment MSD on MSD.DepartmentCd=OT.Department inner Join EmployeeDetails ED on ED.EmpNo=OT.EmpNo" + 
                        " where OT.Ccode='" + SessionCcode + "' and OT.Lcode='" + SessionLcode + "' and OT.Financialyr='" + ddlfinance.SelectedValue + "'" +
                        " and OT.Month='" + ddlMonths.SelectedValue + "' and ED.EmployeeType='" + employee_type + "'" +
                        " and ED.StafforLabor='" + staff_lab + "' and OT.FromDate = convert(datetime,'" + MyDate1 + "', 105)" + 
                        " and OT.ToDate = Convert(Datetime,'" + MyDate2 + "', 105) Order by OrderToken Asc";
                
                DataTable dt = new DataTable();
                dt = objdata.RptCommonQueryRunDetails(query);
                //dt = objdata.Rpt_overtime(SessionCcode, SessionLcode, ddlfinance.SelectedValue, ddlMonths.SelectedValue);
                gvot.DataSource = dt;
                gvot.DataBind();
                if (dt.Rows.Count > 0)
                {
                    string attachment = "attachment;filename=Overtimereport.xls";
                    Response.ClearContent();
                    Response.AddHeader("content-disposition", attachment);
                    Response.ContentType = "application/ms-excel";

                    StringWriter stw = new StringWriter();
                    HtmlTextWriter htextw = new HtmlTextWriter(stw);
                    gvot.RenderControl(htextw);
                    //gvDownload.RenderControl(htextw);
                    //Response.Write("Contract Details");
                    Response.Write(stw.ToString());
                    Response.End();
                    Response.Clear();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Downloaded Successfully');", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('No Data Found....!');", true);
                }
            }
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('" + ex.Message + "'....!');", true);
        }
    }
    protected void btnEmp_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptEmpDownload.aspx");
    }
    protected void btnatt_Click(object sender, EventArgs e)
    {
        Response.Redirect("AttenanceDownload.aspx");
    }
    protected void btnLeave_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptLeaveSample.aspx");
    }
    protected void btnOT_Click(object sender, EventArgs e)
    {
        Response.Redirect("OTDownload.aspx");
    }
    protected void btncontract_Click(object sender, EventArgs e)
    {
        Response.Redirect("ContractRPT.aspx");
    }
    protected void btnSal_Click(object sender, EventArgs e)
    {
        Response.Redirect("FrmDeductionDownload.aspx");
    }

    protected void ddlcategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable dtempty = new DataTable();
        ddlEmployeeType.DataSource = dtempty;
        ddlEmployeeType.DataBind();

        if (ddlcategory.SelectedValue == "1")
        {
            Stafflabour = "S";
        }
        else if (ddlcategory.SelectedValue == "2")
        {
            Stafflabour = "L";
        }
        else
        {
            Stafflabour = "0";
        }
        EmployeeType();
    }

    public void EmployeeType()
    {
        DataTable dtemp = new DataTable();
        dtemp = objdata.EmployeeDropDown_no(ddlcategory.SelectedValue);
        ddlEmployeeType.DataSource = dtemp;
        ddlEmployeeType.DataTextField = "EmpType";
        ddlEmployeeType.DataValueField = "EmpTypeCd";
        ddlEmployeeType.DataBind();
    }
}
