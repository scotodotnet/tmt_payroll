﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;
using Payroll;
using Payroll.Data;
using Payroll.Configuration;
using Altius.BusinessAccessLayer.BALDataAccess;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html;
using iTextSharp.text.html.simpleparser;

public partial class RptLeaveSample : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string Name;
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Session["UserId"] == null)
            {
                Response.Redirect("Default.aspx");
                Response.Write("Your session expired");
            }
            SessionAdmin = Session["Isadmin"].ToString();
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            string ss = Session["UserId"].ToString();
            Name = Session["Usernmdisplay"].ToString();

            DataTable dt = new DataTable();
            dt = objdata.Sample_Leave(SessionCcode, SessionLcode);
            gvEmp.DataSource = dt;
            gvEmp.DataBind();
            if (dt.Rows.Count > 0)
            {
                string attachment = "attachment;filename=LeaveSample.xls";
                Response.ClearContent();
                Response.AddHeader("content-disposition", attachment);
                Response.ContentType = "application/ms-excel";

                StringWriter stw = new StringWriter();
                HtmlTextWriter htextw = new HtmlTextWriter(stw);
                gvEmp.RenderControl(htextw);
                //gvDownload.RenderControl(htextw);
                //Response.Write("Contract Details");
                Response.Write(stw.ToString());
                Response.End();
                Response.Clear();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Downloaded Successfully');", true);
            }
            Response.Redirect("EmployeeRegistration.aspx");
        }
        catch (Exception ex)
        {
        }
    }
    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }
}
