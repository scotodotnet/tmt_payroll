﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class Reports : System.Web.UI.Page
{

    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    ReportDocument rd = new ReportDocument();
    BALDataAccess objdata = new BALDataAccess();
    SqlConnection con;
    string empNo;
    string salaryDate;
    DateTime SalaryDate_temp;
    string rptType;
    string temp_No;
    string temp_date;
    string temp_type;
    string temp_staff;
    string SessionAdmin;
    string yr;
    string SessionCcode;
    string SessionLcode;
    static string CmpName;
    static string Cmpaddress;
    protected void Page_Load(object sender, EventArgs e)
    {
        SessionAdmin = Session["Isadmin"].ToString();
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        string ss = Session["UserId"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        temp_No = Request.QueryString["empNo"].ToString();
        temp_date = Request.QueryString["dateval"].ToString();
        temp_type = Request.QueryString["rpttype"].ToString();
        temp_staff = Request.QueryString["stafftype"].ToString();
        yr = Request.QueryString["yr"].ToString();

        empNo = temp_No;
        salaryDate = temp_date;
        //SalaryDate_temp = Convert.ToDateTime(salaryDate).AddMonths(-1);
        //salaryDate = Convert.ToDateTime(SalaryDate_temp).ToShortDateString();
        rptType = temp_type;
        DataTable dt = new DataTable();
        dt = objdata.Company_retrive(SessionCcode, SessionLcode);
        if (dt.Rows.Count > 0)
        {
            CmpName = dt.Rows[0]["Cname"].ToString();
            Cmpaddress = (dt.Rows[0]["Address1"].ToString() + ", " + dt.Rows[0]["Address2"].ToString() + ", " + dt.Rows[0]["Location"].ToString() + "-" + dt.Rows[0]["Pincode"].ToString());
        }
        if (temp_type == "payslip")
        {
            con = new SqlConnection(constr);
            //SqlCommand cmd = new SqlCommand("PAYsLIP", con);
            //cmd.CommandText = "PAYsLIP";
            //cmd.CommandType = CommandType.StoredProcedure;
            //cmd.Parameters.Add("@EmpNo", SqlDbType.VarChar).Value = empNo;
            //cmd.Parameters.Add("@SalaryDate", SqlDbType.DateTime).Value = Convert.ToDateTime(salaryDate);
            string SeleQry = "Select SalDet.EmpNo,EmpDet.ExisistingCode,EmpDet.EmpName,EmpDet.Designation,EmpDet.Department,SalDet.Totalworkingdays,SalDet.NFh,SalDet.BasicandDA,SalDet.FDA,SalDet.VDA," +
                             " SalDet.Advance,SalDet.Stamp,SalDet.Unioncg,SalDet.Words,SalDet.WorkedDays,SalDet.Month,SalDet.Year,SalDet.LeaveDays," +
                             " SalDet.SalaryDate,SalDet.allowances1,SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.Deduction1,SalDet.Deduction2,SalDet.Deduction3,SalDet.Deduction4,SalDet.Losspay,SalDet.NetPay,SalDet.TotalDeductions,SalDet.GrossEarnings,SalDet.allowances2,SalDet.Deduction2," +
                             " MstDpt.DepartmentNm,SalDet.ProvidentFund,SalDet.ESI,SalDet.HRA,SalDet.OverTime," +
                             " SalDet.WorkedDays from EmployeeDetails EmpDet inner Join SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo" +
                             "  inner Join MstDepartment as MstDpt on MstDpt.DepartmentCd = EmpDet.Department where " +
                             " SalDet.EmpNo='" + empNo + "'AND SalDet.Month='" + salaryDate + "' and Year='" + yr + "' and SalDet.Ccode='" + SessionCcode + "' and SalDet.Lcode='" + SessionLcode + "'";
            SqlCommand cmd = new SqlCommand(SeleQry, con);
            con.Open();
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataSet ds1 = new DataSet();
            sda.Fill(ds1);
            con.Close();
            
            rd.Load(Server.MapPath("PaySlipCrystalReport.rpt"));
            rd.SetDataSource(ds1.Tables[0]);
            rd.DataDefinition.FormulaFields["CompanyName"].Text = "'" + CmpName + "'";
            rd.DataDefinition.FormulaFields["Unit"].Text = "'" + SessionLcode + "'";
            rd.DataDefinition.FormulaFields["CompanyAddress1"].Text = "'" + Cmpaddress + "'";
            CrystalReportViewer1.ReportSource = rd;
            CrystalReportViewer1.RefreshReport();
            CrystalReportViewer1.DataBind();
            //ExportOptions CrExportOptions;
            //DiskFileDestinationOptions CrDiskFileDestinationOptions = new DiskFileDestinationOptions();
            //ExcelFormatOptions CrFormatTypeOptions = new ExcelFormatOptions();
            //CrDiskFileDestinationOptions.DiskFileName = "D:\\payslip_1.xls";
            //CrExportOptions = rd.ExportOptions;
            //CrExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
            //CrExportOptions.ExportFormatType = ExportFormatType.Excel;
            //CrExportOptions.DestinationOptions = CrDiskFileDestinationOptions;
            //CrExportOptions.FormatOptions = CrFormatTypeOptions;
            //rd.Export();
        }
    }
}
