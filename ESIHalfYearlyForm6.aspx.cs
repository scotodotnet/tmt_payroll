﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class ESIHalfYearlyForm6 : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    string Mont = "";
    DateTime m;
    int d = 0;
    string Year = "";
    string SessionAdmin;
    protected void Page_Load(object sender, EventArgs e)
    {
        SessionAdmin = Session["Isadmin"].ToString();
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        string ss = Session["UserId"].ToString();
    }
    public void Month()
    {

        m = Convert.ToDateTime(txtpfdate.Text);
        d = m.Month;
        int mon = m.Month;
        int Ye = m.Year;
        Year = Convert.ToString(Ye);


        switch (d)
        {
            case 1:
                Mont = "January";
                break;

            case 2:
                Mont = "February";
                break;
            case 3:
                Mont = "March";
                break;
            case 4:
                Mont = "April";
                break;
            case 5:
                Mont = "May";
                break;
            case 6:
                Mont = "June";
                break;
            case 7:
                Mont = "July";
                break;
            case 8:
                Mont = "August";
                break;
            case 9:
                Mont = "September";
                break;
            case 10:
                Mont = "October";
                break;
            case 11:
                Mont = "November";
                break;
            case 12:
                Mont = "December";
                break;
            default:
                break;
        }
    }
    protected void btnsearch_Click(object sender, EventArgs e)
    {
        try
        {
            if (txtempno.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Your Employee Registration Number..!');", true);
            }
            else if (txtpfdate.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Please Select Date..!');", true);
            }
            else
            {
                Month();
                DataTable dt = new DataTable();
                dt = objdata.ESIHalfYarlSearchStatement(txtempno.Text, Mont, Year);
                if (dt.Rows.Count > 0)
                {
                    txtempname.Text = dt.Rows[0]["EmpName"].ToString();
                    txtesinumber.Text = dt.Rows[0]["ESICNumber"].ToString();
                    txtesiamount.Text = dt.Rows[0]["ESI"].ToString();
                    txtdesignation.Text = dt.Rows[0]["Designation"].ToString();
                    txtdepartment.Text = dt.Rows[0]["Department"].ToString();
                    txtjoiningdate.Text = dt.Rows[0]["Dateofjoining"].ToString();
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Data Not Available');", true);
                }
            }
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Server Error Contact Admin..!');", true);
        }
    }
    protected void btnsave_Click(object sender, EventArgs e)
    {

        try
        {
            bool ErrFlag = false;
            if (txtempno.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Your Employee Registration Number..!');", true);
            }
            else if (txtpfdate.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Please Select Your PF Date..!');", true);
            }
            else if (txtesioffice.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Please Enter ESIOffice Name..!');", true);
            }
            else if (txtrateofwages.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Your Rate of Wages In the First Wages Period...!');", true);
            }
            else if (txtnoofdays.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Your No of Days Wages Paid...!');", true);
            }
            else if (txttotalamt.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Your Total Amount of Wages Paid...!');", true);
            }
            else if (txtemployeecontri.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Your Employee Sharee Contribution...!');", true);
            }

            else
            {
                Month();
                ESIHalfYarlyForm6Class objESI6 = new ESIHalfYarlyForm6Class();
                objESI6.EmpNo = txtempno.Text;
                objESI6.ESINumber = txtesinumber.Text;
                objESI6.ESIAmount = txtesiamount.Text;
                objESI6.ESIOffice = txtesioffice.Text;
                objESI6.RateofFirstWages = txtrateofwages.Text;
                objESI6.NoofdayswagesPeriod = txtnoofdays.Text;
                objESI6.TotalAmtWagesPeriod = txttotalamt.Text;
                objESI6.EmpShareContribution = txtemployeecontri.Text;


                DateTime ESIDate;
                ESIDate = DateTime.ParseExact(txtpfdate.Text, "dd-MM-yyyy", null);
                ErrFlag = true;
                if (ErrFlag)
                {
                    objdata.InsertESIForm6Statement(objESI6, Mont, Year, ESIDate);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Your Reocrd Registered Successfully..!');", true);
                    // clear();
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Your Correct Value Formate..!');", true);
                }
            }
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Server Error Contact Admin....!');", true);
        }
    }
}
