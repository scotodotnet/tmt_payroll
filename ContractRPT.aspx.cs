﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using Payroll;
using Payroll.Data;
using Payroll.Configuration;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html;
using iTextSharp.text.html.simpleparser;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class ContractRPT : System.Web.UI.Page
{
    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    SqlConnection con;
    BALDataAccess objdata = new BALDataAccess();
    string SessionAdmin;
    string Stafflabour;
    string SessionCcode;
    string SessionLcode;
    protected void Page_Load(object sender, EventArgs e)
    {
        con = new SqlConnection(constr);
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionAdmin = Session["Isadmin"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        lblComany.Text = SessionCcode + " - " + SessionLcode;
        lblusername.Text = Session["Usernmdisplay"].ToString();
        string ss = Session["UserId"].ToString();
        if (!IsPostBack)
        {

        }
    }
    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }
    protected void btnEmp_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptEmpDownload.aspx");
    }
    protected void btnatt_Click(object sender, EventArgs e)
    {
        Response.Redirect("AttenanceDownload.aspx");
    }
    protected void btnLeave_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptLeaveSample.aspx");
    }
    protected void btnOT_Click(object sender, EventArgs e)
    {
        Response.Redirect("OTDownload.aspx");
    }
    protected void btncontract_Click(object sender, EventArgs e)
    {
        Response.Redirect("ContractRPT.aspx");
    }
    protected void rbcontract_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (rbcontract.SelectedValue == "1")
        {
            panelyear.Visible = true;
            panelMonths.Visible = false;
            DataTable dt = new DataTable();
            dt = objdata.Contract_Report(SessionCcode, SessionLcode, rbcontract.SelectedValue);
            gvReport.DataSource = dt;
            gvReport.DataBind();
        }
        else if (rbcontract.SelectedValue == "2")
        {
            panelyear.Visible = false;
            panelMonths.Visible = true;
            DataTable dt = new DataTable();
            dt = objdata.Contract_Report(SessionCcode, SessionLcode, rbcontract.SelectedValue);
            gvContract1.DataSource = dt;
            gvContract1.DataBind();
        }
        else if (rbcontract.SelectedValue == "3")
        {
            panelyear.Visible = true;
            panelMonths.Visible = false;
            DataTable dt = new DataTable();
            dt = objdata.Contract_Report(SessionCcode, SessionLcode, rbcontract.SelectedValue);
            gvReport.DataSource = dt;
            gvReport.DataBind();
        }
    }
    //public override void VerifyRenderingInServerForm(Control control)
    //{
    //    /* Verifies that the control is rendered */
    //}
    protected void btnclear_Click(object sender, EventArgs e)
    {
        Response.Redirect("ContractCompleted.aspx");
    }
    protected void btnSal_Click(object sender, EventArgs e)
    {
        Response.Redirect("FrmDeductionDownload.aspx");
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        if (rbcontract.SelectedValue == "1")
        {
            panelyear.Visible = true;
            panelMonths.Visible = false;
            DataTable dt = new DataTable();
            dt = objdata.Contract_Report(SessionCcode, SessionLcode, rbcontract.SelectedValue);
            

            gvReport.DataSource = dt;
            gvReport.DataBind();
            string attachment = "attachment;filename=ContractRpt.xls";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/ms-excel";
            StringWriter stw = new StringWriter();
            HtmlTextWriter htextw = new HtmlTextWriter(stw);
            gvReport.RenderControl(htextw);
            Response.Write("<table>");
            Response.Write("<tr align='Center'>");
            Response.Write("<td colspan='10'>");
            Response.Write("Contract Report");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("</table>");
            Response.Write(stw.ToString());
            Response.End();
            Response.Clear();
        }
        else if (rbcontract.SelectedValue == "2")
        {
            panelyear.Visible = false;
            panelMonths.Visible = true;
            DataTable dt = new DataTable();
            dt = objdata.Contract_Report(SessionCcode, SessionLcode, rbcontract.SelectedValue);
            gvContract1.DataSource = dt;
            gvContract1.DataBind();
            string attachment = "attachment;filename=ContractRpt.xls";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/ms-excel";
            StringWriter stw = new StringWriter();
            HtmlTextWriter htextw = new HtmlTextWriter(stw);
            gvContract1.RenderControl(htextw);
            Response.Write("<table>");
            Response.Write("<tr align='Center'>");
            Response.Write("<td colspan='10'>");
            Response.Write("Contract Report");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("</table>");
            Response.Write(stw.ToString());
            Response.End();
            Response.Clear();
        }
        else if (rbcontract.SelectedValue == "3")
        {
            panelyear.Visible = true;
            panelMonths.Visible = false;
            DataTable dt = new DataTable();
            dt = objdata.Contract_Report(SessionCcode, SessionLcode, rbcontract.SelectedValue);
            gvReport.DataSource = dt;
            gvReport.DataBind();
            string attachment = "attachment;filename=ContractRpt.xls";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/ms-excel";
            StringWriter stw = new StringWriter();
            HtmlTextWriter htextw = new HtmlTextWriter(stw);
            gvReport.RenderControl(htextw);
            Response.Write("<table>");
            Response.Write("<tr align='Center'>");
            Response.Write("<td colspan='10'>");
            Response.Write("Contract Report");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("</table>");
            Response.Write(stw.ToString());
            Response.End();
            Response.Clear();
        }
    }
}
