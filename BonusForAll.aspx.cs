﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Payroll;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class Bonusforall_1 : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    string stafflabour;
    string chkboxValue;
    string DeductionVal;
    int rbwages;
    string Basic;
    string HRA;
    string Allowance1;
    string Allowance2;
    string FDA;
    string VDA;
   
    DateTime JoiningDate;
    DateTime today;
    string SessionAdmin;
    string stafflabour_temp;
    string SessionCcode;
    string SessionLcode;
    string Days;
    int Months;
    static string EmpType;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionAdmin = Session["Isadmin"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        lblComany.Text = SessionCcode + " - " + SessionLcode;
        if (!IsPostBack)
        {
            //Department();
            DropDwonCategory();
            int currentYear = Utility.GetFinancialYear;
            for (int i = 0; i < 10; i++)
            {
                ddFinance.Items.Add(new ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                //  ddlShowYear.Items.Add(new ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                currentYear = currentYear - 1;
            }

        }
        lblusername.Text = Session["Usernmdisplay"].ToString();
    }
    public void DropDwonCategory()
    {
        DataTable dtcate = new DataTable();
        dtcate = objdata.DropDownCategory();
        ddlcategory.DataSource = dtcate;
        ddlcategory.DataTextField = "CategoryName";
        ddlcategory.DataValueField = "CategoryCd";
        ddlcategory.DataBind();
    }
    public void clr()
    {
        txtbasic.Text = "";
        txthra.Text = "";
        txtFDA.Text = "";
        txtVDA.Text = "";
        txtded1.Text = "";
        txtded2.Text = "";
        txtpercentage.Text = "";
        txtpercentageCal.Text = "";
        txtAttenanceDays.Text = "";
        txttotalamt.Text = "";
        txtsum.Text = "";
        chkbasic.Checked = false;
        chkHRA.Checked = false;
        chkFDA.Checked = false;
        chkVDA.Checked = false;
        chkAllowance1.Checked = false;
        chkAllowance2.Checked = false;
        chkDeduction1.Checked = false;
        chkdeduction2.Checked = false;
    }
    public void EmployeeIDLoad()
    {
        if (ddlcategory.SelectedValue == "1")
        {
            stafflabour = "S";
        }
        else
        {
            stafflabour = "L";
        }
        DataTable dtempcode = new DataTable();
        if (SessionAdmin == "1")
        {
            dtempcode = objdata.LoadEmployeeForBonus(stafflabour, ddldepat.SelectedValue, SessionCcode, SessionLcode);
        }
        else
        {
            dtempcode = objdata.EmpLoadFORBonus_USER(stafflabour, ddldepat.SelectedValue, SessionCcode, SessionLcode);
        }
        ddlempcode.DataSource = dtempcode;
        DataRow dr = dtempcode.NewRow();
        dr["EmpNo"] = ddldepat.SelectedItem.Text;
        dr["EmpName"] = ddldepat.SelectedItem.Text;
        dtempcode.Rows.InsertAt(dr, 0);

        ddlempcode.DataTextField = "EmpNo";
        ddlempcode.DataValueField = "EmpNo";
        ddlempcode.DataBind();
        ddlempname.DataSource = dtempcode;
        ddlempname.DataTextField = "EmpName";
        ddlempname.DataValueField = "EmpNo";
        ddlempname.DataBind();

    }
    protected void ddlcategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        string staffLabour_temp;
        DataTable dtempty = new DataTable();
        ddldepat.DataSource = dtempty;
        ddldepat.DataBind();
        ddlempcode.DataSource = dtempty;
        ddlempcode.DataBind();
        ddlempname.DataSource = dtempty;
        ddlempname.DataBind();
        if (ddlcategory.SelectedValue == "1")
        {
            staffLabour_temp = "S";
        }
        else if (ddlcategory.SelectedValue == "2")
        {
            staffLabour_temp = "L";
        }
        else
        {
            staffLabour_temp = "0";
        }
        DataTable dtDip = new DataTable();
        dtDip = objdata.DropDownDepartment_category(staffLabour_temp);
        if (dtDip.Rows.Count > 1)
        {
            ddldepat.DataSource = dtDip;
            ddldepat.DataTextField = "DepartmentNm";
            ddldepat.DataValueField = "DepartmentCd";
            ddldepat.DataBind();
        }
        else
        {
            DataTable dt = new DataTable();
            ddldepat.DataSource = dt;
            ddldepat.DataBind();
        }
        clr();
        //panelEmployee.Visible = false;
        txtexistingNo.Text = "";
    }
    protected void ddldepat_SelectedIndexChanged(object sender, EventArgs e)
    {
        clr();
        DataTable dtempty = new DataTable();
        ddlempcode.DataSource = dtempty;
        ddlempname.DataBind();
        ddlempname.DataSource = dtempty;
        ddlempname.DataBind();
        //panelEmployee.Visible = false;
    }
    protected void btnclick_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        if (ddlcategory.SelectedValue == "0")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Category....!');", true);

            //System.Windows.Forms.MessageBox.Show("Select the Category", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            ErrFlag = true;
        }
        else if (ddldepat.SelectedValue == "0")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Department....!');", true);

            //System.Windows.Forms.MessageBox.Show("Select the Department", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            ErrFlag = true;
        }
        if (!ErrFlag)
        {
            
            doj.Text = "";
            yop.Text = "";
            EmpProfile.Text = "";
            EmployeeIDLoad();
            
        }
    }
    protected void btnexitingSearch_Click(object sender, EventArgs e)
    {
        try
        {
           
            string Cate = "";
            bool ErrFlag = false;
            if (txtexistingNo.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Existing No....!');", true);

                //System.Windows.Forms.MessageBox.Show("Select the Category", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            else if (ddlcategory.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Category....!');", true);
                ErrFlag = true;
            }
            else if (ddldepat.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Department....!');", true);
                ErrFlag = true;
            }
            else if (ddldepat.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Department....!');", true);
                ErrFlag = true;
            }
            if (!ErrFlag)
            {
                if (ddlcategory.SelectedValue == "1")
                {
                    Cate = "S";
                }
                else
                {
                    Cate = "L";
                }

                DataTable dtempcode = new DataTable();
                if (SessionAdmin == "1")
                {
                    dtempcode = objdata.ExistNoSearchfor_bonus(ddldepat.SelectedValue, Cate, txtexistingNo.Text, SessionCcode, SessionLcode);
                }
                else
                {
                    dtempcode = objdata.ExistNoSearchfor_bonus_USER(ddldepat.SelectedValue, Cate, txtexistingNo.Text, SessionCcode, SessionLcode);
                }
                if (dtempcode.Rows.Count > 0)
                {
                    int yr = Convert.ToInt32(ddFinance.SelectedValue);
                    //yr = yr - 1;
                    DataTable dt_1 = new DataTable();
                    dt_1 = objdata.Salary_total_bonus(dtempcode.Rows[0]["EmpNo"].ToString(), yr, SessionCcode, SessionLcode);
                    txtSalAmt.Text = dt_1.Rows[0]["NetPay"].ToString();
                    txtAttenanceDays.Text = dt_1.Rows[0]["WorkedDays"].ToString();
                    ddlempcode.SelectedValue = dtempcode.Rows[0]["EmpNo"].ToString();
                    ddlempname.DataSource = dtempcode;
                    ddlempname.DataTextField = "EmpName";
                    ddlempname.DataValueField = "EmpNo";
                    ddlempname.DataBind();

                    DataTable dtsalary = new DataTable();
                    dtsalary = objdata.StaffBonusLoad(ddlempcode.SelectedValue, SessionCcode, SessionLcode);
                    if (dtsalary.Rows.Count > 0)
                    {
                        EmpType = dtsalary.Rows[0]["EmployeeType"].ToString();
                        txtbasic.Text = dtsalary.Rows[0]["Base"].ToString();
                        txthra.Text = dtsalary.Rows[0]["HRA"].ToString();
                        txtFDA.Text = dtsalary.Rows[0]["FDA"].ToString();
                        txtVDA.Text = dtsalary.Rows[0]["VDA"].ToString();
                        txtallowance1.Text = dtsalary.Rows[0]["Alllowance1amt"].ToString();
                        txtallowance2.Text = dtsalary.Rows[0]["Allowance2amt"].ToString();
                        txtded1.Text = dtsalary.Rows[0]["Deduction1"].ToString();
                        txtded2.Text = dtsalary.Rows[0]["Deduction2"].ToString();
                        string dtserver = objdata.ServerDate();
                        TimeSpan dt = new TimeSpan();
                        JoiningDate = DateTime.ParseExact(doj.Text, "dd-MM-yyyy", null);
                        today = DateTime.ParseExact(dtserver, "dd-MM-yyyy", null);
                        dt = (today - JoiningDate);
                        DateTime yrCal = DateTime.MinValue + dt;
                        int Years = yrCal.Year - 1;
                        int Months = yrCal.Month - 1;
                        string Experience = (Years + "." + Months);
                        yop.Text = Experience;
                        string Days = "";
                        Days = objdata.Bonus_attenance_Load(ddlempcode.SelectedValue, ddldepat.SelectedValue, ddFinance.SelectedValue, SessionCcode, SessionLcode);
                        txtAttenanceDays.Text = Days;
                        retrive_bonus();
                    }
                    else
                    {
                        txtbasic.Text = "0";
                        txthra.Text = "0";
                        txtFDA.Text = "0";
                        txtVDA.Text = "0";
                        txtallowance1.Text = "0";
                        txtallowance2.Text = "0";
                        txtded1.Text = "0";
                        txtded2.Text = "0";
                    }
                }
            }
        }
        catch (Exception ex)
        {

        }
    }
    protected void ddlempcode_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            bool ErrFlag = false;
            clr();
            if (ddlempcode.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Employee Code....!');", true);

                //System.Windows.Forms.MessageBox.Show("Select the Employee Code", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            if (!ErrFlag)
            {
                DataTable dtemp2 = new DataTable();
                 dtemp2 = objdata.BonusEmployeeName(ddlempcode.SelectedValue, SessionCcode, SessionLcode);
                 if (dtemp2.Rows.Count > 0)
                 {
                     int yr = Convert.ToInt32(ddFinance.SelectedValue);
                     //yr = yr - 1;
                     ddlempname.DataSource = dtemp2;
                     ddlempname.DataTextField = "EmpName";
                     ddlempname.DataValueField = "EmpNo";
                     txtexistingNo.Text = dtemp2.Rows[0]["ExisistingCode"].ToString();
                     ddlempname.DataBind();
                     doj.Text = dtemp2.Rows[0]["DOJ"].ToString();
                     if (dtemp2.Rows[0]["ProfileType"].ToString() == "1")
                     {
                         EmpProfile.Text = "Probationary Period";
                     }
                     else if (dtemp2.Rows[0]["ProfileType"].ToString() == "2")
                     {
                         EmpProfile.Text = "Conformed Employee";
                     }
                     string dtserver = objdata.ServerDate();
                     string Days = "";
                     Days = objdata.Bonus_attenance_Load(ddlempcode.SelectedValue, ddldepat.SelectedValue, ddFinance.SelectedValue, SessionCcode, SessionLcode);
                     txtAttenanceDays.Text = Days;
                     DataTable dt_1 = new DataTable();
                     dt_1 = objdata.Salary_total_bonus(dtemp2.Rows[0]["EmpNo"].ToString(), yr, SessionCcode, SessionLcode);
                     txtSalAmt.Text = dt_1.Rows[0]["NetPay"].ToString();
                     txtAttenanceDays.Text = dt_1.Rows[0]["WorkedDays"].ToString();
                     DataTable dtsalary = new DataTable();
                        dtsalary = objdata.StaffBonusLoad(ddlempcode.SelectedValue, SessionCcode, SessionLcode);
                        if (dtsalary.Rows.Count > 0)
                        {
                            EmpType = dtsalary.Rows[0]["EmployeeType"].ToString();
                            txtbasic.Text = dtsalary.Rows[0]["Base"].ToString();
                            txthra.Text = dtsalary.Rows[0]["HRA"].ToString();
                            txtFDA.Text = dtsalary.Rows[0]["FDA"].ToString();
                            txtVDA.Text = dtsalary.Rows[0]["VDA"].ToString();
                            txtallowance1.Text = dtsalary.Rows[0]["Alllowance1amt"].ToString();
                            txtallowance2.Text = dtsalary.Rows[0]["Allowance2amt"].ToString();
                            txtded1.Text = dtsalary.Rows[0]["Deduction1"].ToString();
                            txtded2.Text = dtsalary.Rows[0]["Deduction2"].ToString();
                            //string dtserver = objdata.ServerDate();
                            TimeSpan dt = new TimeSpan();
                            JoiningDate = DateTime.ParseExact(doj.Text, "dd-MM-yyyy", null);
                            today = DateTime.ParseExact(dtserver, "dd-MM-yyyy", null);
                            dt = (today - JoiningDate);
                            DateTime yrCal = DateTime.MinValue + dt;
                            int Years = yrCal.Year - 1;
                            int Months = yrCal.Month - 1;
                            string Experience = (Years + "." + Months);
                            yop.Text = Experience;
                            retrive_bonus();
                        }
                        else
                        {
                            txtbasic.Text = "0";
                            txthra.Text = "0";
                            txtFDA.Text = "0";
                            txtVDA.Text = "0";
                            txtallowance1.Text = "0";
                            txtallowance2.Text = "0";
                            txtded1.Text = "0";
                            txtded2.Text = "0";
                        }
                 }
            }

        }
        catch (Exception ex)
        {
        }
    }
    protected void ddlempname_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            bool ErrFlag = false;
            clr();
            if (ddlempname.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Employee Name....!');", true);
                ErrFlag = true;
            }
            if (!ErrFlag)
            {
                DataTable dtemp2 = new DataTable();
                
                    dtemp2 = objdata.BonusEmployeeName(ddlempname.SelectedValue, SessionCcode, SessionLcode);
                    if (dtemp2.Rows.Count > 0)
                    {
                        int yr = Convert.ToInt32(ddFinance.SelectedValue);
                        //yr = yr - 1;
                        ddlempcode.DataSource = dtemp2;
                        ddlempcode.DataTextField = "EmpNo";
                        ddlempcode.DataValueField = "EmpNo";
                        txtexistingNo.Text = dtemp2.Rows[0]["ExisistingCode"].ToString();
                        ddlempcode.DataBind();
                        doj.Text = dtemp2.Rows[0]["DOJ"].ToString();
                        if (dtemp2.Rows[0]["ProfileType"].ToString() == "1")
                        {
                            EmpProfile.Text = "Probationary Period";
                        }
                        else if (dtemp2.Rows[0]["ProfileType"].ToString() == "2")
                        {
                            EmpProfile.Text = "Conformed Employee";
                        }
                        DataTable dt_1 = new DataTable();
                        dt_1 = objdata.Salary_total_bonus(dtemp2.Rows[0]["EmpNo"].ToString(), yr, SessionCcode, SessionLcode);
                        txtSalAmt.Text = dt_1.Rows[0]["NetPay"].ToString();
                        txtAttenanceDays.Text = dt_1.Rows[0]["WorkedDays"].ToString();
                        DataTable dtsalary = new DataTable();
                        string Days = "";
                        Days = objdata.Bonus_attenance_Load(ddlempcode.SelectedValue, ddldepat.SelectedValue, ddFinance.SelectedValue, SessionCcode, SessionLcode);
                        txtAttenanceDays.Text = Days;
                        dtsalary = objdata.StaffBonusLoad(ddlempcode.SelectedValue, SessionCcode, SessionLcode);
                        if (dtsalary.Rows.Count > 0)
                        {
                            EmpType = dtsalary.Rows[0]["EmployeeType"].ToString();
                            txtbasic.Text = dtsalary.Rows[0]["Base"].ToString();
                            txthra.Text = dtsalary.Rows[0]["HRA"].ToString();
                            txtFDA.Text = dtsalary.Rows[0]["FDA"].ToString();
                            txtVDA.Text = dtsalary.Rows[0]["VDA"].ToString();
                            txtallowance1.Text = dtsalary.Rows[0]["Alllowance1amt"].ToString();
                            txtallowance2.Text = dtsalary.Rows[0]["Allowance2amt"].ToString();
                            txtded1.Text = dtsalary.Rows[0]["Deduction1"].ToString();
                            txtded2.Text = dtsalary.Rows[0]["Deduction2"].ToString();
                            string dtserver = objdata.ServerDate();
                            TimeSpan dt = new TimeSpan();
                            JoiningDate = DateTime.ParseExact(doj.Text, "dd-MM-yyyy", null);
                            today = DateTime.ParseExact(dtserver, "dd-MM-yyyy", null);
                            dt = (today - JoiningDate);
                            DateTime yrCal = DateTime.MinValue + dt;
                            int Years = yrCal.Year - 1;
                            int Months = yrCal.Month - 1;
                            string Experience = (Years + "." + Months);
                            yop.Text = Experience;
                            retrive_bonus();
                        }
                        else
                        {
                            txtbasic.Text = "0";
                            txthra.Text = "0";
                            txtFDA.Text = "0";
                            txtVDA.Text = "0";
                            txtallowance1.Text = "0";
                            txtallowance2.Text = "0";
                            txtded1.Text = "0";
                            txtded2.Text = "0";
                        }

                    }
                }
        }
        catch
        {
        }
    }
    protected void ddFinance_SelectedIndexChanged(object sender, EventArgs e)
    {
        clr();
        DataTable dtempty = new DataTable();
        ddlempcode.DataSource = dtempty;
        ddlempcode.DataBind();
        ddlempname.DataSource = dtempty;
        ddlempname.DataBind();
    }
    protected void chkbasic_CheckedChanged(object sender, EventArgs e)
    {
        txttotalamt.Text = "";
        txtsum.Text = "";
        if (chkbasic.Checked == true)
        {
            PanelBasicLabel.Visible = true;
        }
        else
        {
            PanelBasicLabel.Visible = false;
        }
    }
    protected void chkHRA_CheckedChanged(object sender, EventArgs e)
    {
        txttotalamt.Text = "";
        txtsum.Text = "";
        if (chkHRA.Checked == true)
        {
            PanelHRALabel.Visible = true;
        }
        else
        {
            PanelHRALabel.Visible = false;
        }
    }
    protected void chkFDA_CheckedChanged(object sender, EventArgs e)
    {
        txttotalamt.Text = "";
        txtsum.Text = "";
        if (chkFDA.Checked == true)
        {
            PanelFDA.Visible = true;
        }
        else
        {
            PanelFDA.Visible = false;
        }
    }
    protected void chkVDA_CheckedChanged(object sender, EventArgs e)
    {
        txttotalamt.Text = "";
        txtsum.Text = "";
        if (chkVDA.Checked == true)
        {
            PanelVDA.Visible = true;
        }
        else
        {
            PanelVDA.Visible = false;
        }
    }
    protected void chkAllowance1_CheckedChanged(object sender, EventArgs e)
    {
        txttotalamt.Text = "";
        txtsum.Text = "";
        if (chkAllowance1.Checked == true)
        {
            PanelAllow1.Visible = true;
        }
        else
        {
            PanelAllow1.Visible = false;
        }
    }
    protected void chkDeduction1_CheckedChanged(object sender, EventArgs e)
    {
        txttotalamt.Text = "";
        txtsum.Text = "";
        if (chkDeduction1.Checked == true)
        {
            panelDeduction1.Visible = true;
        }
        else
        {
            panelDeduction1.Visible = false;
        }
    }
    protected void chkdeduction2_CheckedChanged(object sender, EventArgs e)
    {
        txttotalamt.Text = "";
        txtsum.Text = "";
        if (chkdeduction2.Checked == true)
        {
            panelDeduction2.Visible = true;
        }
        else
        {
            panelDeduction2.Visible = false;
        }
    }
    protected void chkAllowance2_CheckedChanged(object sender, EventArgs e)
    {
        txttotalamt.Text = "";
        txtsum.Text = "";
        if (chkAllowance2.Checked == true)
        {
            PanelAllow2.Visible = true;
        }
        else
        {
            PanelAllow2 .Visible = false;
        }
    }
    protected void btncalculation_Click(object sender, EventArgs e)
    {
        try
        {
            decimal basic = 0;
            decimal HRA = 0;
            decimal VDA = 0;
            decimal FDA = 0;
            decimal Allowance1 = 0;
            decimal Allowance2 = 0;
            decimal Deduction1 = 0;
            decimal deduction2 = 0;
            bool ErrFlag = false;
            txttotalamt.Text = "";
            if (ddlcategory.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Category....!');", true);
                ErrFlag = true;
            }
            else if (ddldepat.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Department....!');", true);
                ErrFlag = true;
            }
            else if (ddldepat.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Department....!');", true);
                ErrFlag = true;
            }
            else if (doj.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Employee....!');", true);
                ErrFlag = true;
            }
            if (!ErrFlag)
            {
                if (PanelBasicLabel.Visible == true)
                {
                    basic = Convert.ToDecimal(txtbasic.Text);
                }
                else
                {
                    basic = 0;
                }
                if (PanelHRALabel.Visible == true)
                {
                    HRA = Convert.ToDecimal(txthra.Text);
                }
                else
                {
                    HRA = 0;
                }
                if (PanelFDA.Visible == true)
                {
                    FDA = Convert.ToDecimal(txtFDA.Text);
                }
                else
                {
                    FDA = 0;
                }
                if (PanelVDA.Visible == true)
                {
                    VDA = Convert.ToDecimal(txtVDA.Text);
                }
                else
                {
                    VDA = 0;
                }
                if(PanelAllow1.Visible == true)
                {
                    Allowance1 = Convert.ToDecimal(txtallowance1.Text);
                }
                else
                {
                    Allowance1 = 0;
                }
                if (PanelAllow2.Visible == true)
                {
                    Allowance2 = Convert.ToDecimal(txtallowance2.Text);
                }
                else
                {
                    Allowance2 = 0;
                }
                if (panelDeduction1.Visible == true)
                {
                    Deduction1 = Convert.ToDecimal(txtded1.Text);
                }
                else
                {
                    Deduction1 = 0;
                }
                if (panelDeduction2.Visible == true)
                {
                    deduction2 = Convert.ToDecimal(txtded2.Text);
                }
                else
                {
                    deduction2 = 0;
                }
                if (EmpType == "3")
                {
                    txtsum.Text = ((basic) - (Deduction1 + deduction2)).ToString();
                }
                else if (EmpType == "4")
                {
                    txtsum.Text = ((basic + HRA + VDA + FDA + Allowance1 + Allowance2) - (Deduction1 + deduction2)).ToString();
                }
                else if (EmpType == "2")
                {
                    txtsum.Text = ((basic + HRA + VDA + FDA + Allowance1 + Allowance2) - (Deduction1 + deduction2)).ToString();
                }
                else if (EmpType == "1")
                {
                    txtsum.Text = ((basic + HRA + VDA + FDA + Allowance1 + Allowance2) - (Deduction1 + deduction2)).ToString();
                }

            }
        }
        catch (Exception ex)
        {

        }
    }
    protected void btnpercentagecal_Click(object sender, EventArgs e)
    {
        try
        {
            bool ErrFlag = false;
            //if (rbbonus.SelectedValue == "1")
            //{

                //if (txtsum.Text.Trim() == "")
                //{
                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Click the Calculate Button....!');", true);
                //    ErrFlag = true;
                //}
                //else if (txtpercentage.Text.Trim() == "")
                //{
                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Percentage....!');", true);
                //    ErrFlag = true;
                //}
                //if (!ErrFlag)
                //{
                //    txtpercentageCal.Text = ((Convert.ToDecimal(txtsum.Text) * Convert.ToDecimal(txtpercentage.Text)) / 100).ToString();
                //    txtpercentageCal.Text = (Math.Round(Convert.ToDecimal(txtpercentageCal.Text), 2, MidpointRounding.ToEven)).ToString();
                //}
            //}
            //else if (rbbonus.SelectedValue == "2")
            //{
                if (txtSalAmt.Text.Trim() == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Salary Amount....!');", true);
                    ErrFlag = true;
                }
                else if (txtpercentage.Text.Trim() == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Percentage....!');", true);
                    ErrFlag = true;
                }
                if (!ErrFlag)
                {
                    txtpercentageCal.Text = ((Convert.ToDecimal(txtSalAmt.Text) * Convert.ToDecimal(txtpercentage.Text)) / 100).ToString();
                    txtpercentageCal.Text = (Math.Round(Convert.ToDecimal(txtpercentageCal.Text), 2, MidpointRounding.ToEven)).ToString();
                    txttotalamt.Text = (Math.Round(Convert.ToDecimal(txtpercentageCal.Text), 0, MidpointRounding.ToEven)).ToString();
                }
            //}
            //else
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Bonus....!');", true);
            //}
        }
        catch (Exception ex)
        {
        }
    }
    protected void btnSummary_Click(object sender, EventArgs e)
    {
        try
        {
            bool ErrFlag = false;
            if (txtpercentageCal.Text == "")
            {
            }
            else if (txtAttenanceDays.Text == "")
            {
            }
            if (!ErrFlag)
            {
                txttotalamt.Text = (Convert.ToDecimal(txtpercentageCal.Text) * Convert.ToDecimal(txtAttenanceDays.Text)).ToString();
                txttotalamt.Text = (Math.Round(Convert.ToDecimal(txttotalamt.Text), 0, MidpointRounding.AwayFromZero)).ToString();
            }
        }
        catch
        {
        }
    }
    protected void txtAttenanceDays_TextChanged(object sender, EventArgs e)
    {
        txttotalamt.Text = "";

    }
    protected void txtpercentage_TextChanged(object sender, EventArgs e)
    {
        txttotalamt.Text = "";
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            bool ErrFlag = false;
            bool Issaved = false;
            bool IsUpdated = false;
            string empdetail;
            if (ddlcategory.SelectedValue == "1")
            {
                stafflabour = "S";
            }
            else if (ddlcategory.SelectedValue == "2")
            {
                stafflabour = "L";
            }
            if (ddldepat.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Department....!');", true);

                //System.Windows.Forms.MessageBox.Show("Select the Department", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            else if (ddlempname.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Employee Code....!');", true);

                //System.Windows.Forms.MessageBox.Show("Select the Employee", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            //else if (txtsum.Text == "")
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Click Calculation Button....!');", true);

            //    //System.Windows.Forms.MessageBox.Show("Click the Calculation button", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            //    ErrFlag = true;
            //}
            else if (txtpercentage.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Percentage Amount....!');", true);

                //System.Windows.Forms.MessageBox.Show("Entert he Percentage Amount", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            else if (txtpercentageCal.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Click Percentage Button....!');", true);

                //System.Windows.Forms.MessageBox.Show("Click the percentage Button", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            else if (txttotalamt.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Days in Number....!');", true);

                //System.Windows.Forms.MessageBox.Show("Enter the Months in Number", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            if (!ErrFlag)
            {
                
                string basic_1 = "";
                string HRA_1 = "";
                string FDA_1 = "";
                string VDA_1 = "";
                string Allowance1_1 = "";
                string Allownance2_1 = "";
                string Deduction1_1 = "";
                string deduction2_1 = "";
                if (chkbasic.Checked == true)
                {
                    //basic = Convert.ToDecimal(txtbasic.Text);
                    basic_1 = "1";
                }
                else
                {
                    basic_1 = "0";
                    //basic = 0;
                }
                if (chkHRA.Checked == true)
                {
                    HRA_1 = "1";
                    //HRA = Convert.ToDecimal(txthra.Text);
                }
                else
                {
                    HRA_1 = "0";
                    //HRA = 0;
                }
                if (chkFDA.Checked == true)
                {
                    FDA_1 = "1";
                    //FDA = Convert.ToDecimal(txtFDA.Text);
                }
                else
                {
                    FDA_1 = "0";
                    //FDA = 0;
                }
                if (chkVDA.Checked == true)
                {
                    VDA_1 = "1";
                    //VDA = Convert.ToDecimal(txtVDA.Text);
                }
                else
                {
                    VDA_1 = "0";
                    //VDA = 0;
                }
                if (chkAllowance1.Checked == true)
                {
                    Allowance1_1 = "1";
                    //Allowance1 = Convert.ToDecimal(txtallowance1.Text);
                }
                else
                {
                    Allowance1_1 = "0";
                    //Allowance1 = 0;
                }
                if (chkAllowance2.Checked == true)
                {
                    Allownance2_1 = "1";
                    //Allowance2 = Convert.ToDecimal(txtallowance2.Text);
                }
                else
                {
                    Allownance2_1 = "0";
                    //Allowance2 = 0;
                }
                if (chkDeduction1.Checked == true)
                {
                    Deduction1_1 = "1";
                    //Deduction1 = Convert.ToDecimal(txtded1.Text);
                }
                else
                {
                    Deduction1_1 = "0";
                    //Deduction1 = 0;
                }
                if (chkdeduction2.Checked == true)
                {
                    deduction2_1 = "1";
                    //deduction2 = Convert.ToDecimal(txtded2.Text);
                }
                else
                {
                    deduction2_1 = "0";
                    //deduction2 = 0;
                }
                chkboxValue = basic_1 + "," + HRA_1 + "," + FDA_1 + "," + VDA_1 + "," + Allowance1_1 + "," + Allownance2_1;
                DeductionVal = Deduction1_1 + "," + deduction2_1;
                BonusforAllclass objbonus = new BonusforAllclass();
                objbonus.EmpCode = ddlempcode.SelectedValue;
                objbonus.Department = ddldepat.SelectedValue;
                objbonus.Category = stafflabour;
                objbonus.BonusCalculation = chkboxValue;
                objbonus.DeductionChkVal = DeductionVal;
                //objbonus.Basic = txtbasic.Text;
                //objbonus.HRA = txthra.Text;
                //objbonus.Allowance1 = txtallowance1.Text;
                //objbonus.Allowance2 = txtallowance2.Text;
                //objbonus.deduction1 = txtded1.Text;
                //objbonus.deduction2 = txtded2.Text;
                objbonus.CalculationAmt = txtSalAmt.Text;
                objbonus.Percentage = txtpercentage.Text;
                objbonus.PercentageAmt = txtpercentageCal.Text;
                //objbonus.Noofmonths = txtmonths.Text;
                objbonus.Attenance = txtAttenanceDays.Text;
                objbonus.BonusAmt = txttotalamt.Text;
                objbonus.Ccode = SessionCcode;
                objbonus.Lcode = SessionLcode;
                objbonus.FDA = txtFDA.Text;
                objbonus.Finance = ddFinance.SelectedValue;
                objbonus.VDA = txtVDA.Text;
                empdetail = objdata.BonusFinancialYear(ddlempcode.SelectedValue, ddFinance.SelectedValue, SessionCcode, SessionLcode);
                if (empdetail == ddlempcode.SelectedValue)
                {
                    objdata.Bonus_Update(objbonus, ddlempcode.SelectedValue);
                    IsUpdated = true;
                }
                else
                {
                    objdata.BonusforallInsert(objbonus);
                    Issaved = true;

                }
                if (Issaved == true)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Saved Successfully....!');", true);
                }
                if (IsUpdated == true)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Updated Successfully....!');", true);
                }
            }
        }
        catch (Exception ex)
        {
        }
    }
    public void retrive_bonus()
    {
        DataTable dtretrive = new DataTable();
        if (ddlcategory.SelectedValue == "1")
        {
            stafflabour = "S";
        }
        else
        {
            stafflabour = "L";
        }
        dtretrive = objdata.BonusRetrive(ddlempcode.SelectedValue, stafflabour, ddFinance.SelectedValue, SessionCcode, SessionLcode);
        if (dtretrive.Rows.Count > 0)
        {
            string[] Allowance_split = dtretrive.Rows[0]["BonusCalculation"].ToString().Split(',');
            string[] Deduction_split = dtretrive.Rows[0]["DeductionChkVal"].ToString().Split(',');


            txtsum.Text = dtretrive.Rows[0]["CalculationAmt"].ToString();
            txtpercentage.Text = dtretrive.Rows[0]["Precentage"].ToString();
            txtpercentageCal.Text = dtretrive.Rows[0]["PrecentageAmt"].ToString();
            txttotalamt.Text = dtretrive.Rows[0]["BonusAmt"].ToString();
            txtAttenanceDays.Text = dtretrive.Rows[0]["AttenanceDays"].ToString();
            if (Allowance_split[0].ToString() == "1")
            {
                chkbasic.Checked = true;
                PanelBasicLabel.Visible = true;
            }
            else
            {
                chkbasic.Checked = false;
                PanelBasicLabel.Visible = false;
            }
            if (Allowance_split[1].ToString() == "1")
            {
                chkHRA.Checked = true;
                PanelHRALabel.Visible = true;
            }
            else
            {
                chkHRA.Checked = false;
                PanelHRALabel.Visible = false;
            }
            if (Allowance_split[2].ToString() == "1")
            {
                chkFDA.Checked = true;
                PanelFDA.Visible = true;
            }
            else
            {
                chkFDA.Checked = false;
                PanelFDA.Visible = false;
            }
            if (Allowance_split[3].ToString() == "1")
            {
                chkVDA.Checked = true;
                PanelVDA.Visible = true;
            }
            else
            {
                chkVDA.Checked = false;
                PanelVDA.Visible = false;
            }
            if (Allowance_split[4].ToString() == "1")
            {
                chkAllowance1.Checked = true;
                PanelAllow1.Visible = true;
            }
            else
            {
                chkAllowance1.Checked = false;
                PanelAllow1.Visible = false;
            }
            if (Allowance_split[5].ToString() == "1")
            {
                chkAllowance2.Checked = true;
                PanelAllow2.Visible = true;
            }
            else
            {
                chkAllowance2.Checked = false;
                PanelAllow2.Visible = false;
            }
            if (Deduction_split[0].ToString() == "1")
            {
                chkDeduction1.Checked = true;
                panelDeduction1.Visible = true;
            }
            else
            {
                chkDeduction1.Checked = false;
                panelDeduction1.Visible = false;
            }
            if (Deduction_split[1].ToString() == "1")
            {
                chkdeduction2.Checked = true;
                panelDeduction1.Visible = true;
            }
            else
            {
                chkdeduction2.Checked = false;
                panelDeduction1.Visible = false;
            }
        }
        else
        {
            txtsum.Text = "";
            txtpercentage.Text = "";
            txtpercentageCal.Text = "";
            txttotalamt.Text = "";
            //txtAttenanceDays.Text = "";
            chkbasic.Checked = false;
            PanelBasicLabel.Visible = false;
            chkHRA.Checked = false;
            PanelHRALabel.Visible = false;
            chkFDA.Checked = false;
            PanelFDA.Visible = false;
            chkVDA.Checked = false;
            PanelVDA.Visible = false;
            chkAllowance1.Checked = false;
            PanelAllow1.Visible = false;
            chkAllowance2.Checked = false;
            PanelAllow2.Visible = false;
            chkDeduction1.Checked = false;
            panelDeduction1.Visible = false;
            chkdeduction2.Checked = false;
            panelDeduction2.Visible = false;
        }
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        clr();
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("BonusForAll.aspx");
    }
    protected void btnEmp_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptEmpDownload.aspx");
    }
    protected void btnatt_Click(object sender, EventArgs e)
    {
        Response.Redirect("AttenanceDownload.aspx");
    }
    protected void btnLeave_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptLeaveSample.aspx");
    }
    protected void btnOT_Click(object sender, EventArgs e)
    {
        Response.Redirect("OTDownload.aspx");
    }

    protected void btncontract_Click(object sender, EventArgs e)
    {
        Response.Redirect("ContractRPT.aspx");
    }
    protected void rbbonus_SelectedIndexChanged(object sender, EventArgs e)
    {
        //if (rbbonus.SelectedValue == "1")
        //{
        //    lblpermenant.Visible = true;
        //}
        //else if (rbbonus.SelectedValue == "2")
        //{
        //    lblpermenant.Visible = false;
        //}
    }
    protected void btnSal_Click(object sender, EventArgs e)
    {
        Response.Redirect("FrmDeductionDownload.aspx");
    }
}
