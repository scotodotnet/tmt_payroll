﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Data.SqlClient;

public partial class MstBank_1 : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    MastersClass objMas = new MastersClass();
    string SessionAdmin;
    static string lblprobation_Common;
    string SessionCcode;
    string SessionLcode;
    protected void Page_Load(object sender, EventArgs e)
    {
        
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        string ss = Session["UserId"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        lblComany.Text = SessionCcode + " - " + SessionLcode;
        if (!IsPostBack)
        {
            BankDisplay();
        }
        lblusername.Text = Session["Usernmdisplay"].ToString();
    }
    protected void gvBank_Cancel(object sender, GridViewCancelEditEventArgs e)
    {

        gvBank.EditIndex = -1;
        BankDisplay();

    }
    protected void gvBank_Update(object sender, GridViewUpdateEventArgs e)
    {
        bool ErrFlag = false;
        try
        {
            TextBox txtBanknm1 = (TextBox)gvBank.Rows[e.RowIndex].FindControl("txtBanknm");
            string bank_name = objdata.BankName_verify_value(txtBanknm1.Text);
            if (txtBanknm1.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Bankname');", true);
                //System.Windows.Forms.MessageBox.Show("Enter the Bank Name", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            if (bank_name.ToLower() == txtBanknm1.Text.ToLower())
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Bank Name already Exist');", true);
                //System.Windows.Forms.MessageBox.Show("bank name already Exist", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            if (!ErrFlag)
            {
                Label LblBankcd = (Label)gvBank.Rows[e.RowIndex].FindControl("LblBankcd");

                objMas.Bankcd = LblBankcd.Text;
                objMas.Bankname = txtBanknm1.Text;
                objdata.UpdateBank(objMas);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Bankname Update Successfully..');", true);
                //System.Windows.Forms.MessageBox.Show("Bank Name Updated Successfully", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                gvBank.EditIndex = -1;
                BankDisplay();
            }
        }
        catch (Exception ex)
        {

        }
    }
    public void BankDisplay()
    {
        DataTable dt = new DataTable();
        dt = objdata.BankDisplay();

        gvBank.DataSource = dt;

        gvBank.DataBind();
        gvBank.DataSource = dt;


    }
    protected void gvState_RowEditing(object sender, GridViewEditEventArgs e)
{
    gvBank.EditIndex = e.NewEditIndex;
    BankDisplay();
}

protected void gvBank_RowDeleting(object sender, GridViewDeleteEventArgs e)
{
    bool ErrFlag = false;
    string constr = ConfigurationManager.AppSettings["ConnectionString"];
    Label lblbank = (Label)gvBank.Rows[e.RowIndex].FindControl("lblBankcd");
    lblprobation_Common = lblbank.Text;

    string bank_del = objdata.Bank_Delete(lblbank.Text);
    if (bank_del == lblbank.Text)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Cannot b Deleted....!');", true);
        //System.Windows.Forms.MessageBox.Show("Cannot be Deleted", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
        ErrFlag = true;
    }
    if (!ErrFlag)
    {
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "script", "showConfirm();", true);
        //SqlConnection con = new SqlConnection(constr);
        ////System.Windows.Forms.DialogResult dres = System.Windows.Forms.MessageBox.Show("Are you Sure want to Delete ?", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.YesNo, System.Windows.Forms.MessageBoxIcon.Warning);
        ////if (dres.ToString() == "Yes")
        //{
        //    string qry1 = "Delete from Bank where Bankcd='" + lblbank.Text + "'";
        //    SqlCommand cmd1 = new SqlCommand(qry1, con);
        //    con.Open();
        //    cmd1.ExecuteNonQuery();
        //    con.Close();
        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Deleted the Record....! ');", true);
        //    //System.Windows.Forms.MessageBox.Show("Deleted the Record", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
        //    BankDisplay();
        //}
    }
}
    
    protected void btnsave_Click(object sender, EventArgs e)
    {
        try
        {
            //string EmpNo = "";
            //MastersClass objEmp = new MastersClass();
            bool isRegistered = false;
            bool ErrFlag = false;

            if (txtBankcd.Text == "" && txtBanknm.Text == "")
            {

                //Response.Write(@"<script type ='text/jscript'>window.alert('Enter the bank Code')</script>");
                //Page.RegisterStartupScript("S", "<SCRIPT Language='javascript'>alert('Please Enter Correct Username & Password');</SCRIPT>");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Bankcode');", true);
                //System.Windows.Forms.MessageBox.Show("Enter the Bank Code", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            else if (txtBanknm.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Bankname');", true);
                //System.Windows.Forms.MessageBox.Show("Enter the bank Name", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }

            if (!ErrFlag)
            {
                string bank_name = objdata.BankName_verify_value(txtBanknm.Text);
                string bank_code = objdata.BankCd_verify_value(txtBankcd.Text);
                if (bank_name.ToLower() == txtBanknm.Text.ToLower())
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Bank Name already Exist');", true);
                    //System.Windows.Forms.MessageBox.Show("Bank name Already Exist", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    ErrFlag = true;
                }
                if (bank_code == txtBankcd.Text)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Bank Code already Exist');", true);
                    //System.Windows.Forms.MessageBox.Show("Bank Code Already Exist", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    ErrFlag = true;
                }
                if (!ErrFlag)
                {
                    objMas.Bankcd = txtBankcd.Text;
                    objMas.Bankname = txtBanknm.Text;
                    try
                    {
                        objdata.Bank(objMas);
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Bank Save Successfully..!');", true);
                        //System.Windows.Forms.MessageBox.Show("Bank Saved Successfully", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                        txtBankcd.Text = "";
                        txtBanknm.Text = "";
                        BankDisplay();

                    }
                    catch (Exception ex)
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Server Error Contact Admin....!');", true);
                        //System.Windows.Forms.MessageBox.Show("Server Error Contact Admin", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);

                    }
                }

            }
        }
        catch (Exception ex)
        {

            //System.Windows.Forms.MessageBox.Show("Please Contact Admin", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Please Contact Admin....!');", true);
        }
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        txtBankcd.Text = "";
        txtBanknm.Text = "";
        BankDisplay();
    }

    protected void cmdOverriteGrade_Click(object sender, EventArgs e)
    {
        try
        {
            string constr = ConfigurationManager.AppSettings["ConnectionString"];
            //Label lblbank = (Label)gvBank.Rows[e.RowIndex].FindControl("lblBankcd");
            SqlConnection con = new SqlConnection(constr);
            //System.Windows.Forms.DialogResult dres = System.Windows.Forms.MessageBox.Show("Are you Sure want to Delete ?", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.YesNo, System.Windows.Forms.MessageBoxIcon.Warning);
            //if (dres.ToString() == "Yes")
            
                string qry1 = "Delete from Bank where Bankcd='" + lblprobation_Common + "'";
                SqlCommand cmd1 = new SqlCommand(qry1, con);
                con.Open();
                cmd1.ExecuteNonQuery();
                con.Close();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Deleted the Record....! ');", true);
                //System.Windows.Forms.MessageBox.Show("Deleted the Record", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                BankDisplay();
            
        }
        catch (Exception ex)
        {
        }
    }
    protected void btnEmp_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptEmpDownload.aspx");
    }
    protected void btnatt_Click(object sender, EventArgs e)
    {
        Response.Redirect("AttenanceDownload.aspx");
    }
    protected void btnLeave_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptLeaveSample.aspx");
    }
    protected void btnOT_Click(object sender, EventArgs e)
    {
        Response.Redirect("OTDownload.aspx");
    }
    protected void btncontract_Click(object sender, EventArgs e)
    {
        Response.Redirect("ContractRPT.aspx");
    }
    protected void btnSal_Click(object sender, EventArgs e)
    {
        Response.Redirect("FrmDeductionDownload.aspx");
    }
}
