﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Payroll;
using Payroll.Configuration;
using Payroll.Data;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class EmployeeDeactiveMode_1 : System.Web.UI.Page
{
    protected System.Resources.ResourceManager rm;
    protected string PayrollRegisterContentMeta, pageTitle;
    BALDataAccess objdata = new BALDataAccess();
    string isactivemode;
    string EmpNo;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    bool EmployeeSearch = false;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (SessionAdmin == "2")
        {
            Response.Redirect("EmployeeRegistration.aspx");
        }
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        string ss = Session["UserId"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        lblComany.Text = SessionCcode + " - " + SessionLcode;
        if (!IsPostBack)
        {
            DropDwonCategory();
        }
        lblusername.Text = Session["Usernmdisplay"].ToString();
    }
    public void DropDwonCategory()
    {
        DataTable dtcate = new DataTable();
        dtcate = objdata.DropDownCategory();
        ddlcategory.DataSource = dtcate;
        ddlcategory.DataTextField = "CategoryName";
        ddlcategory.DataValueField = "CategoryCd";
        ddlcategory.DataBind();
    }
    protected void ddlcategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        txtEmpno.Text = "";
        txtexist.Text = "";
        DataTable dtempty = new DataTable();
        DataGridView1.DataSource = dtempty;
        DataGridView1.DataBind();
        PanelGrid.Visible = false;
    }
    protected void btnclick_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        string staff = "";
        if (ddlcategory.SelectedValue == "0")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Category....!');", true);
            //System.Windows.Forms.MessageBox.Show("Select the Category", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            ErrFlag = true;
        }
        if (!ErrFlag)
        {
            if (ddlcategory.SelectedValue == "1")
            {
                staff = "S";
            }
            else if (ddlcategory.SelectedValue == "2")
            {
                staff = "L";
            }
            DataTable dt = new DataTable();
            dt = objdata.Deactive_Load_all(staff, SessionCcode, SessionLcode);
            DataGridView1.DataSource = dt;
            DataGridView1.DataBind();
            PanelGrid.Visible = true;
        }
        else
        {
            DataTable dtempty = new DataTable();
            DataGridView1.DataSource = dtempty;
            DataGridView1.DataBind();
            PanelGrid.Visible = false;
        }

    }
    protected void txtEmpno_TextChanged(object sender, EventArgs e)
    {
        if (EmployeeSearch == false)
        {
            txtexist.Text = "";
            DataTable dtempty = new DataTable();
            DataGridView1.DataSource = dtempty;
            DataGridView1.DataBind();
        }
    }
    protected void txtexist_TextChanged(object sender, EventArgs e)
    {
        if (EmployeeSearch == false)
        {
            txtEmpno.Text = "";
            DataTable dtempty = new DataTable();
            DataGridView1.DataSource = dtempty;
            DataGridView1.DataBind();
        }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        PanelGrid.Visible = false;
        //string EmpVerify = objdata.EmployeeVerify(txtEmpno.Text);
        if ((txtEmpno.Text == "") && (txtexist.Text == ""))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Enter the Employee Details....!');", true);
            //System.Windows.Forms.MessageBox.Show("Enter the Employee Details", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            ErrFlag = true;
        }
        if (!ErrFlag)
        {
            PanelGrid.Visible = true;
            DataTable dt = new DataTable();
            if (txtEmpno.Text != "")
            {
                dt = objdata.EmployeeDeactivate_loadgrid(txtEmpno.Text, SessionCcode, SessionLcode);
            }
            else if (txtexist.Text != "")
            {
                dt = objdata.EmployeeDeactivate_loadgrid_exist(txtexist.Text, SessionCcode, SessionLcode);
            }
            if (dt.Rows.Count > 0)
            {
                EmployeeSearch = true;
                DataGridView1.DataSource = dt;
                DataGridView1.DataBind();
                txtexist.Text = dt.Rows[0]["ExisistingCode"].ToString();
                txtEmpno.Text = dt.Rows[0]["EmpNo"].ToString();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Enter the Employee Details Properly....!');", true);
                //System.Windows.Forms.MessageBox.Show("Enter the Employee Details Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            }
            EmployeeSearch = false;
        }
    }
    protected void btnActivate_Click(object sender, EventArgs e)
    {
        bool ErrorFlag = false;
        bool Isregister = false;
        bool chkvalue = false;
        if (DataGridView1.Rows.Count <= 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Employee No Properly');", true);
            //System.Windows.Forms.MessageBox.Show("Enter the Employee Number Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            ErrorFlag = true;
        }
        if (!ErrorFlag)
        {
            foreach (GridViewRow gvrow in DataGridView1.Rows)
            {
                CheckBox check_box = (CheckBox)gvrow.FindControl("chkClear");
                if (check_box != null)
                {
                    if (check_box.Checked)
                    {
                        chkvalue = true;
                    }
                }
            }
            isactivemode = "Y";
            if (chkvalue == true)
            {
                foreach (GridViewRow gvrow in DataGridView1.Rows)
                {
                    CheckBox check_box = (CheckBox)gvrow.FindControl("chkClear");
                    if (check_box != null)
                    {
                        if (check_box.Checked)
                        {
                            for (int i = 0; i < DataGridView1.Rows.Count; i++)
                            {
                                Label lblempNo = (Label)gvrow.FindControl("lblempId");
                                //EmpNo = DataGridView1.Rows[i].Cells[0].Text;
                                objdata.UpdateEmployeeActivateDeactivate(lblempNo.Text, isactivemode, SessionCcode, SessionLcode);
                                Isregister = true;
                            }
                        }
                    }

                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Employee....!');", true);
                //System.Windows.Forms.MessageBox.Show("Select the Employee", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            }

            if (Isregister == true)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Updated Successfully....!');", true);
                //System.Windows.Forms.MessageBox.Show("Updated Successfully", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                PanelGrid.Visible = false;
            }
        }
    }
    protected void btnDeactive_Click(object sender, EventArgs e)
    {
        bool ErrorFlag = false;
        bool Isregister = false;
        bool chkvalue = false;
        if (DataGridView1.Rows.Count <= 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Employee No Properly');", true);
            //System.Windows.Forms.MessageBox.Show("Enter the Employee Number Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            ErrorFlag = true;
        }
        if (!ErrorFlag)
        {
            foreach (GridViewRow gvrow in DataGridView1.Rows)
            {
                CheckBox check_box = (CheckBox)gvrow.FindControl("chkClear");
                if (check_box != null)
                {
                    if (check_box.Checked)
                    {
                        chkvalue = true;
                    }
                }
            }
            isactivemode = "N";
            if (chkvalue == true)
            {
                foreach (GridViewRow gvrow in DataGridView1.Rows)
                {
                    CheckBox check_box = (CheckBox)gvrow.FindControl("chkClear");
                    if (check_box != null)
                    {
                        if (check_box.Checked)
                        {
                            for (int i = 0; i < DataGridView1.Rows.Count; i++)
                            {
                                Label lblempNo = (Label)gvrow.FindControl("lblempId");
                                //EmpNo = DataGridView1.Rows[i].Cells[0].Text;
                                objdata.UpdateEmployeeActivateDeactivate(lblempNo.Text, isactivemode, SessionCcode, SessionLcode);
                                Isregister = true;
                            }
                        }
                    }
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Employee....!');", true);
                //System.Windows.Forms.MessageBox.Show("Select the Employee", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            }
            if (Isregister == true)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Updated Successfully....!');", true);
                //System.Windows.Forms.MessageBox.Show("Updated Successfully", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                PanelGrid.Visible = false;
            }
        }
    }
    protected void btnEmp_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptEmpDownload.aspx");
    }
    protected void btnatt_Click(object sender, EventArgs e)
    {
        Response.Redirect("AttenanceDownload.aspx");
    }
    protected void btnLeave_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptLeaveSample.aspx");
    }
    protected void btnOT_Click(object sender, EventArgs e)
    {
        Response.Redirect("OTDownload.aspx");
    }
    protected void btncontract_Click(object sender, EventArgs e)
    {
        Response.Redirect("ContractRPT.aspx");
    }
    protected void btnSal_Click(object sender, EventArgs e)
    {
        Response.Redirect("FrmDeductionDownload.aspx");
    }
}
