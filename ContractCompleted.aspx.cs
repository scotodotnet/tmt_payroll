﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class ContractCompleted : System.Web.UI.Page
{
    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    System.Globalization.CultureInfo culterInfo = new System.Globalization.CultureInfo("en-GB");
    SqlConnection con;
    BALDataAccess objdata = new BALDataAccess();
    string SessionAdmin;
    string Stafflabour;
    string SessionCcode;
    string SessionLcode;
    protected void Page_Load(object sender, EventArgs e)
    {
        con = new SqlConnection(constr);
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        if (Session["Isadmin"].ToString() != "1")
        {
            Response.Redirect("ContractRPT.aspx");
        }
        SessionAdmin = Session["Isadmin"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        lblComany.Text = SessionCcode + " - " + SessionLcode;
        lblusername.Text = Session["Usernmdisplay"].ToString();
        string ss = Session["UserId"].ToString();
        if (!IsPostBack)
        {

        }
    }
    protected void btnEmp_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptEmpDownload.aspx");
    }
    protected void btnatt_Click(object sender, EventArgs e)
    {
        Response.Redirect("AttenanceDownload.aspx");
    }
    protected void btnLeave_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptLeaveSample.aspx");
    }
    protected void btnOT_Click(object sender, EventArgs e)
    {
        Response.Redirect("OTDownload.aspx");
    }
    protected void btncontract_Click(object sender, EventArgs e)
    {
        Response.Redirect("ContractRPT.aspx");
    }
    protected void btnSal_Click(object sender, EventArgs e)
    {
        Response.Redirect("FrmDeductionDownload.aspx");
    }
    protected void gvReport_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {

    }
    protected void rbcontract_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        DataTable dt_empty = new DataTable();
        gvReport.DataSource = dt_empty;
        gvReport.DataBind();
        if (rbcontract.SelectedValue == "1")
        {
            dt = objdata.Contract_CompleteDays(SessionCcode, SessionLcode, rbcontract.SelectedValue);
            gvReport.DataSource = dt;
            gvReport.DataBind();
        }
        else if (rbcontract.SelectedValue == "2")
        {
            dt = objdata.Contract_CompleteMonths(SessionCcode, SessionLcode, rbcontract.SelectedValue);
            gvReport.DataSource = dt;
            gvReport.DataBind();
        }
        else if (rbcontract.SelectedValue == "3")
        {
            dt = objdata.Contract_CompleteDays(SessionCcode, SessionLcode, rbcontract.SelectedValue);
            gvReport.DataSource = dt;
            gvReport.DataBind();
        }
    }
    protected void btnCompleted_Click(object sender, EventArgs e)
    {
        try
        {
            bool ErrorFlag = false;
            bool Isregister = false;
            bool chkvalue = false;
            OfficialprofileClass objoff = new OfficialprofileClass();
            if (gvReport.Rows.Count <= 0)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Employee No Properly');", true);
                //System.Windows.Forms.MessageBox.Show("Enter the Employee Number Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrorFlag = true;
            }
            if (!ErrorFlag)
            {
                foreach (GridViewRow gvrow in gvReport.Rows)
                {
                    CheckBox check_box = (CheckBox)gvrow.FindControl("chkClear");
                    if (check_box != null)
                    {
                        if (check_box.Checked)
                        {
                            chkvalue = true;
                        }
                    }
                }
                if (chkvalue == true)
                {
                    foreach (GridViewRow gvrow in gvReport.Rows)
                    {
                        CheckBox check_box = (CheckBox)gvrow.FindControl("chkClear");
                        if (check_box != null)
                        {
                            if (check_box.Checked)
                            {
                                for (int i = 0; i < gvReport.Rows.Count; i++)
                                {
                                    Label lblempNo = (Label)gvrow.FindControl("lblEmpNo");
                                    //EmpNo = DataGridView1.Rows[i].Cells[0].Text;
                                    //objdata.UpdateEmployeeActivateDeactivate(lblempNo.Text, isactivemode, SessionCcode, SessionLcode);
                                    DataTable dt = new DataTable();
                                    dt = objdata.Contract_Details_CompletedReport(lblempNo.Text, SessionCcode, SessionLcode);
                                    objoff.EmpNo = dt.Rows[0]["EmpNo"].ToString();
                                    objoff.Contractname = dt.Rows[0]["ContractName"].ToString();
                                    objoff.ContractType = dt.Rows[0]["ContractType"].ToString();
                                    objoff.yr = dt.Rows[0]["yr"].ToString();
                                    objoff.Months = dt.Rows[0]["Months"].ToString();
                                    objoff.Days1 = dt.Rows[0]["Days"].ToString();
                                    objoff.FixedDays = dt.Rows[0]["FixedDays"].ToString();
                                    objoff.YrDays = dt.Rows[0]["YrDays"].ToString();
                                    objoff.PlannedDays = dt.Rows[0]["PlannedDays"].ToString();
                                    objoff.BalanceDays = "0";
                                    string startDate = dt.Rows[0]["StartingDate"].ToString();
                                    DateTime dtime = new DateTime();

                                    
                                    
                                    //DateTime dtime = new DateTime();
                                    //dtime = DateTime.ParseExact(EndDate, "dd-MM-yyyy", Cul.DateTimeFormat);

                                    objdata.Contract_CompletedReport(objoff, SessionCcode, SessionLcode, startDate);
                                    objdata.DeleteContract(dt.Rows[0]["EmpNo"].ToString(), SessionCcode, SessionLcode, dt.Rows[0]["ContractType"].ToString(), dt.Rows[0]["ContractName"].ToString());
                                    Isregister = true;
                                }
                            }
                        }

                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Employee....!');", true);
                    //System.Windows.Forms.MessageBox.Show("Select the Employee", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                }
                if (Isregister == true)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Completed Successfuly....!');", true);
                }
            }
        }
        catch (Exception ex)
        {
        }
    }
    protected void btnbreak_Click(object sender, EventArgs e)
    {
        Response.Redirect("NewContract.aspx");
    }
}
