﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data;
using Payroll;
using Payroll.Data;
using Payroll.Configuration;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html;
using iTextSharp.text.html.simpleparser;
using System.Data;
using Payroll;
using Payroll.Data;
using Payroll.Configuration;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html;
using iTextSharp.text.html.simpleparser;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Data.SqlClient;

public partial class RptBonus : System.Web.UI.Page
{
    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    SqlConnection con;
    BALDataAccess objdata = new BALDataAccess();
    string SessionAdmin;
    string Stafflabour;
    DateTime FromDate;
    DateTime ToDate;
    string SessionCcode;
    string SessionLcode;

    string CmpName = "";
    string Cmpaddress = "";


    DataTable dd = new DataTable();
    DataTable dt = new DataTable();
    DataTable OTdt = new DataTable();
    DataTable TotOTdt = new DataTable();


    protected void Page_Load(object sender, EventArgs e)
    {
        con = new SqlConnection(constr);
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionAdmin = Session["Isadmin"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        string ss = Session["UserId"].ToString();
        if (!IsPostBack)
        {
            category();
           
            //Department();
            //DropDwonCategory();
            int currentYear = Utility.GetFinancialYear;
            for (int i = 0; i < 10; i++)
            {
                ddlFinance.Items.Add(new System.Web.UI.WebControls.ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                //  ddlShowYear.Items.Add(new ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                currentYear = currentYear - 1;
            }
        }
        lblComany.Text = SessionCcode + " - " + SessionLcode;
        lblusername.Text = Session["Usernmdisplay"].ToString();
    }
    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }
    protected void btnclick_Click(object sender, EventArgs e)
    {
       
    }
    public void category()
    {
        DataTable dtcate = new DataTable();
        dtcate = objdata.DropDownCategory();
        ddlcategory.DataSource = dtcate;
        ddlcategory.DataTextField = "CategoryName";
        ddlcategory.DataValueField = "CategoryCd";
        ddlcategory.DataBind();
    }
    
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("StandardBonus.aspx");
    }
    protected void btnEmp_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptEmpDownload.aspx");
    }
    protected void btnatt_Click(object sender, EventArgs e)
    {
        Response.Redirect("AttenanceDownload.aspx");
    }
    protected void btnLeave_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptLeaveSample.aspx");
    }
    protected void btnOT_Click(object sender, EventArgs e)
    {
        Response.Redirect("OTDownload.aspx");
    }
 
    protected void btncontract_Click(object sender, EventArgs e)
    {
        Response.Redirect("ContractRPT.aspx");
    }
    protected void btnSal_Click(object sender, EventArgs e)
    {
        Response.Redirect("FrmDeductionDownload.aspx");
    }
    protected void ddldept_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void EmployeeType()
    {
        // throw new NotImplementedException();
        DataTable dtemp = new DataTable();
        dtemp = objdata.EmployeeDropDown_no(ddlcategory.SelectedValue);
        ddldept.DataSource = dtemp;
        ddldept.DataTextField = "EmpType";
        ddldept.DataValueField = "EmpTypeCd";
        ddldept.DataBind();
    }
    protected void ddlcategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable dtempty = new DataTable();
        ddldept.DataSource = dtempty;
        ddldept.DataBind();

        string Stafflabour = "";

        if (ddlcategory.SelectedValue == "1")
        {
            Stafflabour = "S";
        }
        else if (ddlcategory.SelectedValue == "2")
        {
            Stafflabour = "L";
        }
        else
        {
            Stafflabour = "0";
        }
        EmployeeType();
    }
    //public void Months_load()
    //{
    //    DataTable dt = new DataTable();
    //    dt = objdata.months_load();
    //    ddlMonths.DataSource = dt;
    //    ddlMonths1.DataSource = dt;
    //    ddlMonths.DataTextField = "Months";
    //    ddlMonths1.DataTextField = "Months";
    //    ddlMonths.DataValueField = "Months";
    //    ddlMonths1.DataValueField = "Months";
    //    ddlMonths.DataBind();
    //    ddlMonths1.DataBind();
    //}
    protected void btnclear_Click(object sender, EventArgs e)
    {

    }
    protected void btnexport_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        bool Mon_ErrFlag = false;
        
        


        string SSQL = "";
        string Stafflabour = "";

        string Year = ddlFinance.SelectedItem.Text;
        string[] SliptYear = Year.Split('-');
        
        if (ddlcategory.SelectedValue == "1")
        {
            Stafflabour = "S";
        }
        else if (ddlcategory.SelectedValue == "2")
        {
            Stafflabour = "L";
        }
        else
        {
            Stafflabour = "0";
        }

        if (ddlcategory.SelectedValue == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select Catgory Type..')", true);
            //ErrFlag = true;
        }
        else if (ddldept.SelectedValue == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select The EmployeeType ..')", true);
            //ErrFlag = true;
        }

        if (ddldept.SelectedValue == "1")
        {
            SSQL = "select EmpNo,ExisistingCode,EmpName, ";
            SSQL = SSQL + "isnull(sum(April_WD),0) as April_WD ,isnull(sum(April_WA),0) as April_WA, ";
            SSQL = SSQL + "isnull(sum(May_WD),0) as May_WD ,isnull(sum(May_WA),0) as May_WA, ";
            SSQL = SSQL + "isnull(sum(June_WD),0) as June_WD ,isnull(sum(June_WA),0) as June_WA, ";
            SSQL = SSQL + "isnull(sum(July_WD),0) as July_WD ,isnull(sum(July_WA),0) as July_WA, ";
            SSQL = SSQL + "isnull(sum(August_WD),0) as August_WD ,isnull(sum(August_WA),0) as August_WA,";
            SSQL = SSQL + "isnull(sum(September_WD),0) as September_WD ,isnull(sum(September_WA),0) as September_WA, ";
            SSQL = SSQL + "isnull(sum(October_WD),0) as October_WD ,isnull(sum(October_WA),0) as October_WA, ";
            SSQL = SSQL + "isnull(sum(November_WD),0) as November_WD ,isnull(sum(November_WA),0) as November_WA, ";
            SSQL = SSQL + "isnull(sum(December_WD),0) as December_WD ,isnull(sum(December_WA),0) as December_WA, ";
            SSQL = SSQL + "isnull(sum(January_WD),0) as January_WD ,isnull(sum(January_WA),0) as January_WA, ";
            SSQL = SSQL + "isnull(sum(February_WD),0) as February_WD ,isnull(sum(February_WA),0) as February_WA, ";
            SSQL = SSQL + "isnull(sum(March_WD),0) as March_WD ,isnull(sum(March_WA),0) as March_WA, ";
            SSQL = SSQL + "Sum(isnull(April_WD,0) + isnull(May_WD,0) + isnull(June_WD,0) + isnull(July_WD,0) + isnull(August_WD,0) + isnull(September_WD,0) + isnull(October_WD,0) + isnull(November_WD,0) + isnull(December_WD,0) + isnull(January_WD,0) + isnull(February_WD,0) + isnull(March_WD,0)) as TotalAllDays, ";
            SSQL = SSQL + "Sum(isnull(April_WA,0) + isnull(May_WA,0) + isnull(June_WA,0) + isnull(July_WA,0) + isnull(August_WA,0) + isnull(September_WA,0) + isnull(October_WA,0) + isnull(November_WA,0) + isnull(December_WA,0) + isnull(January_WA,0) + isnull(February_WA,0) + isnull(March_WA,0) +0) as TotalAllWages ";
            SSQL = SSQL + "from ( select ";
            SSQL = SSQL + "ED.EmpNo,ED.ExisistingCode,(ED.EmpName + '.' + ED.Initial)as EmpName, ";
            SSQL = SSQL + "((AD.Days + AD.ThreeSided) - (AD.BonusDays)) as Days,sum(GrossEarnings) Wages, ";
            SSQL = SSQL + "left(DATENAME(MM, AD.FromDate),9) + '_WD' WD_month, ";
            SSQL = SSQL + "left(DATENAME(MM, SD.FromDate),9) + '_WA' WA_month  from SalaryDetails SD ";
            SSQL = SSQL + "inner join EmployeeDetails ED on SD.EmpNo=ED.EmpNo ";
            SSQL = SSQL + "inner join AttenanceDetails AD on SD.EmpNo=AD.EmpNo and SD.Month=AD.Months And SD.FinancialYear=AD.FinancialYear ";
            SSQL = SSQL + "where ED.StafforLabor='" + Stafflabour + "' and ED.EmployeeType='" + ddldept.SelectedValue + "' and  ED.Lcode='" + SessionLcode + "' and  SD.FinancialYear='" + SliptYear[0] + "' and AD.FinancialYear='" + SliptYear[0] + "' ";
            SSQL = SSQL + "group by ED.EmpNo,ED.ExisistingCode,ED.Initial,ED.EmpName,DATENAME(month, SD.FromDate), DATENAME(month, AD.FromDate),AD.Days,AD.BonusDays,AD.ThreeSided";
            SSQL = SSQL + ") as p  ";
            SSQL = SSQL + "PIVOT(  MAX(Days) for WD_month in (April_WD,May_WD,June_WD,July_WD,August_WD,September_WD,October_WD,November_WD,December_WD,January_WD,February_WD,March_WD))as pvt ";
            SSQL = SSQL + "PIVOT(  MAX(Wages) for WA_month in (April_WA,May_WA,June_WA,July_WA,August_WA,September_WA,October_WA,November_WA,December_WA,January_WA,February_WA,March_WA))as pvt ";
            SSQL = SSQL + "group by EmpNo,ExisistingCode,EmpName order by cast(ExisistingCode as int) asc";
        }
        else
        {
            SSQL = "select EmpNo,ExisistingCode,EmpName, ";
            SSQL = SSQL + "isnull(sum(April_WD),0) as April_WD ,isnull(sum(April_WA),0) as April_WA, ";
            SSQL = SSQL + "isnull(sum(May_WD),0) as May_WD ,isnull(sum(May_WA),0) as May_WA, ";
            SSQL = SSQL + "isnull(sum(June_WD),0) as June_WD ,isnull(sum(June_WA),0) as June_WA, ";
            SSQL = SSQL + "isnull(sum(July_WD),0) as July_WD ,isnull(sum(July_WA),0) as July_WA, ";
            SSQL = SSQL + "isnull(sum(August_WD),0) as August_WD ,isnull(sum(August_WA),0) as August_WA,";
            SSQL = SSQL + "isnull(sum(September_WD),0) as September_WD ,isnull(sum(September_WA),0) as September_WA, ";
            SSQL = SSQL + "isnull(sum(October_WD),0) as October_WD ,isnull(sum(October_WA),0) as October_WA, ";
            SSQL = SSQL + "isnull(sum(November_WD),0) as November_WD ,isnull(sum(November_WA),0) as November_WA, ";
            SSQL = SSQL + "isnull(sum(December_WD),0) as December_WD ,isnull(sum(December_WA),0) as December_WA, ";
            SSQL = SSQL + "isnull(sum(January_WD),0) as January_WD ,isnull(sum(January_WA),0) as January_WA, ";
            SSQL = SSQL + "isnull(sum(February_WD),0) as February_WD ,isnull(sum(February_WA),0) as February_WA, ";
            SSQL = SSQL + "isnull(sum(March_WD),0) as March_WD ,isnull(sum(March_WA),0) as March_WA, ";
            SSQL = SSQL + "Sum(isnull(April_WD,0) + isnull(May_WD,0) + isnull(June_WD,0) + isnull(July_WD,0) + isnull(August_WD,0) + isnull(September_WD,0) + isnull(October_WD,0) + isnull(November_WD,0) + isnull(December_WD,0) + isnull(January_WD,0) + isnull(February_WD,0) + isnull(March_WD,0)) as TotalAllDays, ";
            SSQL = SSQL + "Sum(isnull(April_WA,0) + isnull(May_WA,0) + isnull(June_WA,0) + isnull(July_WA,0) + isnull(August_WA,0) + isnull(September_WA,0) + isnull(October_WA,0) + isnull(November_WA,0) + isnull(December_WA,0) + isnull(January_WA,0) + isnull(February_WA,0) + isnull(March_WA,0) +0) as TotalAllWages ";
            SSQL = SSQL + "from ( select ";
            SSQL = SSQL + "ED.EmpNo,ED.ExisistingCode,(ED.EmpName + '.' + ED.Initial)as EmpName, ";
            SSQL = SSQL + "(";
            SSQL = SSQL + "case when (AD.Days + AD.ThreeSided) <= 0 then 0 else";
            SSQL = SSQL + "((AD.Days + AD.ThreeSided) - (AD.BonusDays)) end ) as Days,";
            SSQL = SSQL + "cast(case when (AD.Days + AD.ThreeSided) <= 0 then 0 else ";
            SSQL = SSQL + "(((GrossEarnings + FHRA) / (AD.Days + AD.ThreeSided) ) ";
            SSQL = SSQL + "* ((AD.Days + AD.ThreeSided) - (AD.BonusDays))) end as decimal(18,2)) as Wages,";

            SSQL = SSQL + "left(DATENAME(MM, AD.FromDate),9) + '_WD' WD_month, ";
            SSQL = SSQL + "left(DATENAME(MM, SD.FromDate),9) + '_WA' WA_month  from SalaryDetails SD ";
            SSQL = SSQL + "inner join EmployeeDetails ED on SD.EmpNo=ED.EmpNo ";
            SSQL = SSQL + "inner join AttenanceDetails AD on SD.EmpNo=AD.EmpNo and SD.Month=AD.Months And SD.FinancialYear=AD.FinancialYear ";
            SSQL = SSQL + "where ED.StafforLabor='" + Stafflabour + "' and ED.EmployeeType='" + ddldept.SelectedValue + "' and  ED.Lcode='" + SessionLcode + "' and  SD.FinancialYear='" + SliptYear[0] + "' and AD.FinancialYear='" + SliptYear[0] + "' ";
            SSQL = SSQL + "group by ED.EmpNo,ED.ExisistingCode,ED.Initial,ED.EmpName,DATENAME(month, SD.FromDate), DATENAME(month, AD.FromDate),AD.Days,AD.BonusDays,AD.ThreeSided,SD.FHRA,SD.GrossEarnings";
            SSQL = SSQL + ") as p  ";
            SSQL = SSQL + "PIVOT(  MAX(Days) for WD_month in (April_WD,May_WD,June_WD,July_WD,August_WD,September_WD,October_WD,November_WD,December_WD,January_WD,February_WD,March_WD))as pvt ";
            SSQL = SSQL + "PIVOT(  MAX(Wages) for WA_month in (April_WA,May_WA,June_WA,July_WA,August_WA,September_WA,October_WA,November_WA,December_WA,January_WA,February_WA,March_WA))as pvt ";
            SSQL = SSQL + "group by EmpNo,ExisistingCode,EmpName order by cast(ExisistingCode as int) asc";
        }

        


        SqlCommand cmd = new SqlCommand(SSQL, con);
        DataTable dt_1 = new DataTable();
        SqlDataAdapter sda = new SqlDataAdapter(cmd);
        //DataSet ds1 = new DataSet();
        con.Open();
        sda.Fill(dt_1);
        con.Close();

        GV_Bonus.DataSource = dt_1;
        GV_Bonus.DataBind();

        string attachment = "attachment;filename=Bonus.xls";
        Response.ClearContent();
        Response.AddHeader("content-disposition", attachment);
        Response.ContentType = "application/ms-excel";
        DataTable dt = new DataTable();
        dt = objdata.Company_retrive(SessionCcode, SessionLcode);
        if (dt.Rows.Count > 0)
        {
            CmpName = dt.Rows[0]["Cname"].ToString();
            Cmpaddress = (dt.Rows[0]["Address1"].ToString() + ", " + dt.Rows[0]["Address2"].ToString() + ", " + dt.Rows[0]["Location"].ToString() + "-" + dt.Rows[0]["Pincode"].ToString());
        }

        DataTable Slap_DT = new DataTable();

        StringWriter stw = new StringWriter();
        HtmlTextWriter htextw = new HtmlTextWriter(stw);
        GV_Bonus.RenderControl(htextw);
        Response.Write("<table>");
        Response.Write("<tr align='Center'>");
        Response.Write("<td colspan='12'>");
        Response.Write("<h3><b>" + CmpName + "</b></h3>");
        Response.Write("</td>");
        Response.Write("</tr>");
        Response.Write("<tr align='Center'>");
        Response.Write("<td colspan='12'>");
        Response.Write(" UNIT-I");
        Response.Write("</td>");
        Response.Write("</tr>");
        Response.Write("<tr align='Center'>");
        Response.Write("<td colspan='12'>");
        Response.Write("" + Cmpaddress + "");
        Response.Write("</td>");
        Response.Write("</tr>");

        Response.Write("<tr>");
        Response.Write("<td colspan='10'>");
        Response.Write(ddldept.SelectedItem.Text + "-" + "bonus Year of " + SliptYear[0] + " - " + SliptYear[1]);
        Response.Write("</td>");
        Response.Write("</tr>");
        Response.Write("</table>");
        Response.Write(stw.ToString());
        Response.End();
        Response.Clear();
        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Downloaded Successfully');", true);



    }
}
