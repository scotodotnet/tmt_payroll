﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="MstInsuranceCompany.aspx.cs" Inherits="MstInsuranceCompany_1" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
    <head id="Head1" runat="server">
        <title>Payroll Management Systems</title>
        <link href="new_css/main.css" rel="stylesheet" type="text/css" />
        <link href="new_css/colors/dark_blue.css" rel="stylesheet" type="text/css" />
        <link href="new_css/superfish.css" rel="stylesheet" type="text/css" />

        <script src="new_js/jquery-1.4.2.min.js" type="text/javascript"></script>
        <script src="new_js/jquery.hoverintent.minified.js" type="text/javascript"></script>
        <script src="new_js/superfish.js" type="text/javascript"></script>
        <script src="new_js/jquery.tools.min.js" type="text/javascript"></script>
        <%--<script src="new_js/jquery.simplemodal.js" type="text/javascript"></script>--%>
        <script src="new_js/jquery.notifybar.js" type="text/javascript"></script>
        <script src="new_js/jquery.tipsy.js" type="text/javascript"></script>
        <script src="new_js/enhance.js" type="text/javascript"></script>
        <script src="new_js/excanvas.js" type="text/javascript"></script>
        <script src="new_js/visualize.jquery.js" type="text/javascript"></script>
        <script src="new_js/jquery.collapsible.js" type="text/javascript"></script>
        <script src="new_js/jquery.autosuggest.packed.js" type="text/javascript"></script>
        <script src="new_js/platinum-admin.js" type="text/javascript"></script>
        <script type ="text/jscript" >
            function GoBack() {
                window.history.forward();
            }
            function showConfirm() 
            {
                if (confirm("Are you Sure want to Delete ?") == true) 
                {
                    //__doPostBack('<%= this.cmdOverriteGrade.ClientID %>', "true");
                    var clickButton = document.getElementById("<%= cmdOverriteGrade.ClientID %>");
                    clickButton.click();
                }
                return false;
            }
        </script>
        <style>
            .sf-navbar li ul 
            {
	            width: 1100px; /*IE6 soils itself without this*/
	            margin: 0px auto;
	            margin-left:-20em;	            
            }
        </style>
    </head>
    <body onload="GoBack();">
        <form id="form1" runat="server">
            <cc1:ToolkitScriptManager runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true"
                ID="ScriptManager1" EnablePartialRendering="true">
            </cc1:ToolkitScriptManager>
            <div id="header">
                <div id="header-top">
                    <div id="logo">
	                    <h1>&nbsp;</h1>
	                    <span id="slogan">&nbsp;</span> 
	                </div>
                    <!-- end logo -->
                    <div id="login-info">
                        <img id="profile" alt="profile" src="./new_images/icons/user.png" />
                        <p id="top"><span id="name"><asp:Label ID="lblusername" runat="server"></asp:Label></span><br />
                        <span id="links">
                        <%--<a id="message-link" href="#" title="click to see the messages!">
                        <span id="message-count">1</span> new message. </a>--%></span></p>
                        <div>
                            <asp:Label ID="lblComany" runat="server" Text="" Font-Bold="true"></asp:Label>
                        </div>
	                    <div id="messages-box">
		                    <h4>Messages</h4>
	                        <h5><a id="new-message" href="#">New Message</a></h5>
	                        <hr />
	                        <ul id="messages">
		                        <li class="new"><a href="#">
		                            <img alt="user" src="./new_images/icons/sender.png" />
		                            <span class="sender">John Doe</span>
		                            <span class="description">Hi this is just a description...</span>
		                            <span class="date">20 minutes ago...</span> </a>
		                        </li>
		                        <li><a href="#">
		                            <img alt="user" src="./new_images/icons/sender.png" />
		                            <span class="sender">John Doe</span>
		                            <span class="description">Hi this is just a description...</span>
		                            <span class="date">24 minutes ago...</span> </a>
		                        </li>
		                        <li><a href="#">
		                            <img alt="user" src="./new_images/icons/sender.png" />
		                            <span class="sender">John Doe</span>
		                            <span class="description">Hi this is just a description...</span>
		                            <span class="date">50 minutes ago...</span> </a>
		                        </li>
		                        <li id="last"><a id="all-messages" href="#">See all messages!
		                            <span id="unreaded">8 unreaded.</span> </a>
		                        </li>
		                    </ul>
		                    <br class="clear" />
	                    </div><!-- end messages-box -->
	                    <a id="power" href="Default.aspx" title="Logout">logout</a>
                    </div>
                    <!-- end login -->
                    <div id="nav">
	                    <ul class="sf-menu sf-navbar">
	                        <li><a id="dashboard" href="Dashboard.aspx">Dashboard</a>
	                            <ul>
		                            <li>&nbsp;
		                            </li>
	                            </ul>
	                        </li>
	                        <li><a id="Employee" href="EmployeeRegistration.aspx">Employee</a>
	                            <ul>
		                            <li><a href="EmployeeRegistration.aspx">Employee Registration</a> </li>					                    
		                            <li><a href="OfficalProfileDetails.aspx">Official Profile</a></li>
		                            <li><a href="SalaryMaster.aspx">Salary Master</a></li>
		                            <li><a href="LeaveAllocationEmployee.aspx">Leave Allocation</a></li>
		                            <li><a href="ApplyForLeave.aspx">Apply Leave</a></li>						                    
		                            <li><a href="SalaryRegistration.aspx">Salary Details</a></li>
		                            <li><a href="SalaryAdvance.aspx">Salary Advance</a></li>
		                             <li><a href="Settlement.aspx">Settlement</a></li>
		                            <li><a href="EmployeeDeactiveMode.aspx">Employee Re-Activate</a></li>		                    
	                            </ul>
	                        </li>
	                        <li><a id="Reports" href="RptDepartmentwiseEmployee.aspx">Reports</a>
	                            <ul>
		                            <li><a href="RptDepartmentwiseEmployee.aspx">Employee Details</a></li>
		                            <li><a href="RptAttendanceReport.aspx">Attendance Report</a></li>
		                            <li><a href="RptProbationperiod.aspx">Probationary Period</a></li>
		                            <li><a href="ContractBreak.aspx">Contract Details</a></li>
		                            <li><a href="RptResign.aspx">Resignation Reports</a></li>
		                            <li><a href="PaySlipGen.aspx">Payslip</a></li>
		                            <%--<li><a href="RptSalary.aspx">Salary Details</a></li>--%>
	                            </ul>
	                        </li>
	                        <li><a id="OT" href="OverTime.aspx">OT</a>
	                            <ul>
	                                <li><a href="OverTime.aspx">OT</a></li>
	                            </ul>
	                        </li>
	                        <li class="current"><a id="Master" href="MstEmployeeType.aspx">Masters</a>
	                            <ul>
		                            <li><a href="MstEmployeeType.aspx">Employee Type</a></li>  
		                            <li><a href="MstLeave.aspx">Leave</a></li>
		                            <li><a href="MstProbationPeriod.aspx">Probationary Period</a></li>
		                            <li><a href="UserRegistration.aspx">User</a></li>
		                            <li><a href="MSTPFESI.aspx">PF</a></li>
		                            <li><a href="MstBank.aspx">Bank</a></li>
		                            <li><a href="MstDepartment.aspx">Department</a></li>
		                            <li class="current"><a href="MstInsuranceCompany.aspx">Insurance</a></li>
		                            <li><a href="MstQualification.aspx">Qualification</a></li>
	                            </ul>
	                        </li>
	                        <li><a id="Bonus" href="BonusForAll.aspx">Bonus</a>
	                            <ul>
		                            <li><a href="BonusForAll.aspx">Bonus</a></li>
		                            <li><a href="StandardBonus.aspx">Group Bonus</a></li>					                    
	                            </ul>
	                        </li>
	                        <li><a id="ESI_PF" href="PFForm5.aspx">ESI & PF</a>
	                            <ul>
		                            <li><a href="PFForm5.aspx">PF Form 5</a></li>
		                            <li><a href="PFDownload.aspx">PF Download</a></li>
		                            <li><a href="FOrm10.aspx">Form 10</a></li>
		                            <li><a href="PFForm3Anew.aspx">PF Form 3A</a></li>
		                            <li><a href="PFform6A.aspx">PF Form 6A</a></li>
		                            <li><a href="ESIForm7.aspx">ESI Form 7</a></li>
	                            </ul>
	                        </li>
	                        <li><a id="Upload" href="AttenanceUpload.aspx">Upload</a>
	                            <ul>
		                            <li><a href="AttenanceUpload.aspx">Attenance Upload</a></li>
		                            <li><a href="UploadEmployee.aspx">Employee Upload</a></li>	
	                            </ul>
	                        </li>

                        </ul>
                    </div>
                    <!-- end div#nav -->
                </div>
                <!-- end div#header-top -->
                <div class="breadCrumb">
                    <ul>
                        <li class="first"><a href="#">Home</a> </li>
                        <li><a href="#">Users</a> </li>
                        <li><a href="#">Roles</a> </li>
                        <li class="last">Insurance Master </li>
                    </ul>
                </div>
                <!-- end div#breadCrumb -->
                <%--<div id="search-box">
                    <form id="searchform" action="" method="get">
                        <fieldset class="search"><span>Search</span>
                        <input class="box" type="text" />
                        <button class="btn" title="Submit Search">Search</button></fieldset>
                    </form>
                </div>--%>
                <!-- end div#search-box -->
            </div>
            <!-- end header -->
            <div id="page-wrap">
                <div id="right-sidebar">
                    <div class="innerdiv">
	                    <h2 class="head">Comments</h2>
	                    <div class="innercontent clear">
		                    <h4>Recent Comments</h4>
		                    <ul id="comments" class="tooltip-enabled">
		                        <li><a href="#" title="this comment is from John Doe">
		                            <img alt="user" src="./new_images/icons/sender.png" />
		                            <span class="sender">John Doe</span>
		                            <span class="description">Hey i liked your theme!</span>
		                            <span class="date">20 minutes ago...</span> </a>
		                            <span class="floatright"><a title="Aprrove this comment" href="#">Approve</a> | 
		                            <a title="Delete this comment"  href="#">Delete</a>
		                            </span>
		                            <br class="clear" />
		                        </li>
		                        <li><a href="#" title="again this guy!">
		                            <img alt="user" src="./new_images/icons/sender.png" />
		                            <span class="sender">John Doe</span>
		                            <span class="description">Hi, i need some help.</span>
		                            <span class="date">24 minutes ago...</span> </a>
		                            <span class="floatright"><a title="Aprrove this comment"  href="#">Approve</a> | 
		                            <a title="Delete this comment"  href="#">Delete</a>
		                            </span>
		                            <br class="clear" />
		                        </li>
		                        <li><a href="#" title="is he a spammer?">
		                            <img alt="user" src="./new_images/icons/sender.png" />
		                            <span class="sender">John Doe</span>
		                            <span class="description">Thank you for your help.</span>
		                            <span class="date">50 minutes ago...</span> </a>
		                            <span class="floatright"><a title="Aprrove this comment"  href="#">Approve</a> | 
		                            <a title="Delete this comment"  href="#">Delete</a>
		                            </span>
		                            <br class="clear" />
		                        </li>
		                        <li class="last"><a class="all-messages" title="See all comments awaiting confirmation." href="#">See all comments!
		                            <span class="unreaded">8 awaiting confirm.</span> </a>
		                        </li>
		                    </ul>
	                    </div>
                    </div>
                    <div class="clear"></div>                    
                </div>
	                <!-- end right-sidebar -->
	                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
	                        <ContentTemplate>
	                            <div id="main-content">
                                <br /><br /><br /><br /> 
	                            <div class="clear_body">
                                    <div class="innerdiv">
                                        <div class="innercontent">
                                            <!-- tab "panes" -->				                
                                                <div>
                                                     <table class="full">
                                                        <thead>
                                                            <tr align="center">
                                                                <th colspan="2" style="text-align:center;"><h4>INSURANCE MASTER</h4></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td><asp:Label ID="Label1" runat="server" Text="Insurance CompCd" Font-Bold="true"></asp:Label></td>
                                                                <td><asp:TextBox ID="txtinscompcd" runat="server" ></asp:TextBox>
                                                                 <cc1:FilteredTextBoxExtender ID="FilterTextboxExtenderempname" TargetControlID="txtinscompcd" runat="server" FilterMode="ValidChars" FilterType="Custom"  ValidChars="0123456789"> </cc1:FilteredTextBoxExtender>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td><asp:Label ID="Label2" runat="server" Text="Insurance Company Name" Font-Bold="true"></asp:Label></td>
                                                                <td><asp:TextBox ID="txtinsurancecompnm" runat="server" ></asp:TextBox>
                                                                 <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" TargetControlID="txtinsurancecompnm"  runat="server" FilterMode="ValidChars" FilterType="UppercaseLetters,LowercaseLetters,Custom" ValidChars=" "> </cc1:FilteredTextBoxExtender>
                                                                </td>
                                                            </tr>
                                                            <tr align="center">
                                                                <td colspan="2"><asp:Button ID="btnsave" runat="server" Text="Save" onclick="btnsave_Click" CssClass="button green" />
                                                                <asp:Button ID="btncancel" runat="server" Text="Cancel" onclick="btncancel_Click" CssClass="button green" /></td>
                                                            </tr>
                                                            <tr >
                                                                <td colspan="2">
                                                                    <asp:GridView ID="gvInsurance" runat="server" Width="100%"  
                                                                        AutoGenerateColumns="false" onrowcancelingedit="RowEdit_Canceling" 
                                                                        onrowediting="RowEdit_Editing" onrowupdating="RowEdit_Updating" 
                                                                        onrowdeleting="gvInsurance_RowDeleting"  >
                                                                        <Columns>
                                                                            <asp:TemplateField>
                                                                            <HeaderTemplate>Insurance Cmp Code</HeaderTemplate>
                                                                            <ItemTemplate>
                                                                            <asp:Label ID="lblinscompcd" runat="server" Text='<%# Eval("Insurancecmpcd") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField>
                                                                            <HeaderTemplate>Insurance Comp Name</HeaderTemplate>
                                                                            <ItemTemplate>
                                                                            <asp:Label ID="lblInscompanynm" runat="server" Text='<%# Eval("InsuranceCmpNm") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                            <EditItemTemplate>
                                                                            <asp:TextBox ID="txtinsurancempnm" runat="server" Text='<%# Eval("InsuranceCmpNm") %>'></asp:TextBox>
                                                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" TargetControlID="txtinsurancempnm"  runat="server" FilterMode="ValidChars" FilterType="UppercaseLetters,LowercaseLetters,Custom" ValidChars=" "> </cc1:FilteredTextBoxExtender>
                                                                            </EditItemTemplate>
                                                                            </asp:TemplateField>
                                                                    
                                                                    
                                                                          
                                                                            <asp:CommandField CancelText ="Cancel" DeleteText="Delete" UpdateText="Update" EditText="Edit" ShowEditButton="true" ShowDeleteButton="true" ButtonType="Button" ControlStyle-CssClass="button green" />
                                                                        </Columns>
                                                                        
                                                                    </asp:GridView></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <%--Delete Button Code--%>
                                <asp:Button ID="cmdOverriteGrade" runat="server" style="display:none;" OnClick="cmdOverriteGrade_Click"/>
	                        </ContentTemplate>
	                    </asp:UpdatePanel>                
            </div>
            <!-- end page-wrap -->
            <div id="footer">
                <ul>
                    <li>Copyright &copy; 2012. All rights reserved.</li>
                    <li>Powered by <a href="http://www.altius.co.in" target="_blank">Altius Infosystems</a></li>
                </ul>
            </div>
            <!-- end footer -->
        </form>
    </body>
</html>
