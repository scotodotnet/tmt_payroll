﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Configuration;
using System.Data.SqlClient;
using System.Data.OleDb;
using Payroll;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class SalaryCalculation : System.Web.UI.Page
{
    string SessionAdmin;
    string DepartmentCode;
    string Name_Upload;
    string Name_Upload1;
    BALDataAccess objdata = new BALDataAccess();
    SalaryClass objSal = new SalaryClass();
    string SessionCcode = "0";
    string SessionLcode = "0";
    static string EmployeeDays = "0";
    static string Work = "0";
    static string NFh = "0";
    static string lopdays="0";
    static string WeekOff = "0";
    static string Home = "0";
    static string halfNight = "0";
    static string FullNight = "0";
    static string ThreeSided = "0";
    static string HalfNight_Amt = "0";
    static string FullNight_Amt = "0";
    static string ThreeSided_Amt = "0";
    static string SpinningAmt = "0";
    static string DayIncentive = "0";
    static string cl = "0";
    static string OTDays = "0";
    static string basic = "0";
    static decimal NewHRA = 0;
    static decimal NewTA = 0;

    //STAFF
    decimal mon_sal;
    static string withpay = "0.00";
    static string WorikingDays = "";
    static string VDA = "0";
    static string FDA = "0";
    static string HRA = "0";
    static string FixedBasicDA = "0";
    static string FixedDays = "0";

    static string BonusDays = "0";
    static string NfhCount = "0";
    static string NfhPresent = "0";
    static string FineAmt = "0";
    static string WHPresent = "0";
    static string FineAmt_Sal = "0";

    static string FBasic = "0";
    static string FFDA = "0";
    static string FFHRA = "0";
    static string FFTotal = "0";
    static string DaysCount = "0";

    static string All1 = "0";
    static string All2 = "0";
    static string All3 = "0";
    static string Al4 = "0";
    static string All5 = "0";
    static string Ded1 = "0";
    static string ded2 = "0";
    static string ded3 = "0";
    static string ded4 = "0";
    static string ded5 = "0";
    static string lop = "0";
    static string Advance = "0";
    static string pfAmt = "0";
    static string ESI = "0";
    static string stamp = "0";
    static string Union = "0";
    static string TotalEarnings = "0";
    static string TotalDeductions = "0";
    static string NetPay = "0";
    static string Words = "";
    static string PFS = "0.00";
    static string ExistNo = "";
    static string PF_salary;
    static string Lunch = "";
    static string BasicWages = "0.00";
    static string TA = "0.00";
    static string ConveyAllowance = "0.00";
    static string WashingAloowance = "0.00";
    static string TotalWages = "0.00";
    string TempDate;
    DateTime TransDate;
    DateTime MyDate;
    static int d = 0;
    static decimal AdvAmt;
    static string ID;
    static bool isUK = false;
    string SMonth;
    string Year;
    int dd;
    string MyMonth;
    static decimal val;
    string WagesType;
    string RemainDays = "";
    static string Rest = "0";
    string HostelDeduction = "0";

    static string AvaibleDasy = "0.0";


    static string Basic_Half;
    static string PF_Salary_Check="6500.00";

    //Incentive Variable
    static string Worker_11Days_Incentive = "";
    static string Worker_12Days_Incentive = "";
    protected void Page_Load(object sender, EventArgs e)
    {
     
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        if (SessionAdmin == "2")
        {
            Response.Redirect("EmployeeRegistration.aspx");

        }
        
        SessionAdmin = Session["Isadmin"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        lblComany.Text = SessionCcode + " - " + SessionLcode;
        if (!IsPostBack)
        {
            DropDwonCategory();
            Months_load();
            int currentYear = Utility.GetFinancialYear;
            for (int i = 0; i < 10; i++)
            {
                ddlfinance.Items.Add(new ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                //  ddlShowYear.Items.Add(new ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                currentYear = currentYear - 1;
            }
        }
        lblusername.Text = Session["Usernmdisplay"].ToString();
    }
    public void Months_load()
    {
        DataTable dt = new DataTable();
        dt = objdata.months_load();
        ddlMonths.DataSource = dt;
        ddlMonths.DataTextField = "Months";
        ddlMonths.DataValueField = "Months";
        ddlMonths.DataBind();
    }
    public string NumerictoNumber(int Number, bool isUK)
    {
        if (Number == 0)
            return "Zero";

        string and = isUK ? "and " : "";
        if (Number == -2147483648)
            return "Minus Two Billion One Hundred " + and +
        "Forty Seven Million Four Hundred " + and + "Eighty Three Thousand " +
        "Six Hundred " + and + "Forty Eight";

        int[] num = new int[4];
        int first = 0;
        int u, h, t = 0;
        System.Text.StringBuilder sb = new System.Text.StringBuilder();
        if (Number < 0)
        {
            sb.Append("Minus");
            Number = -Number;
        }

        string[] words0 = { "", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine" };
        string[] words1 = { "Ten ", "Eleven ", "Twelve ", "Thirteen ", "Fourteen ", "Fifteen ", "Sixteen ", "Seventeen ", "Eighteen ", "Nineteen " };
        string[] words2 = { "Twenty ", "Thirty ", "Forty ", "Fifty ", "Sixty ", "Seventy ", "Eighty ", "Ninety " };
        string[] words3 = { "Thousand ", "Lak", "Crore", "Million ", "Billion " };
        num[0] = Number % 1000;
        num[1] = Number / 1000;
        num[2] = Number / 1000000;
        num[1] = num[1] - 1000 * num[2];  // thousands
        num[2] = num[2] - 100 * num[3];//laks
        num[3] = Number / 10000000;     // billions
        num[2] = num[2] - 1000 * num[3];  // millions

        for (int i = 3; i > 0; i--)
        {
            if (num[i] != 0)
            {
                first = i;
                break;
            }
        }
        for (int i = first; i >= 0; i--)
        {
            if (num[i] == 0) continue;
            u = num[i] % 10;
            t = num[i] / 10;
            h = num[i] / 100;
            t = t - 10 * h;

            if (h > 0)
                sb.Append(words0[h] + " Hundred ");
            if (u > 0 || t > 0)
            {
                if (h > 0 || i < first)
                    sb.Append(and);

                if (t == 0)
                    sb.Append(words0[u]);
                else if (t == 1)
                    sb.Append(words1[u]);
                else
                    sb.Append(words2[t - 2] + words0[u]);

            }

            if (i != 0)
                sb.Append(words3[i - 1]);

        }

        return sb.ToString().TrimEnd();



    }
    protected void btnUpload_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        bool SaveFlag = false;
        decimal func;
        string Stafflabour = "";

        HRA = "0";
        VDA = "0";
        FDA = "0";

        try
        {

            if (ddlMonths.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Month.');", true);
                //System.Windows.Forms.MessageBox.Show("Select the Month", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            else if (txtFrom.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the from Date Properly.');", true);
                //System.Windows.Forms.MessageBox.Show("Enter the Days Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            else if (txtTo.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the To Date Properly.');", true);
                //System.Windows.Forms.MessageBox.Show("Enter the Days Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            else if (ddlcategory.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Category.');", true);
                //System.Windows.Forms.MessageBox.Show("Enter the Days Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            else if (ddlcategory.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Category Properly.');", true);
                //System.Windows.Forms.MessageBox.Show("Enter the Days Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            if (rbsalary.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Wges Type.');", true);
                ErrFlag = true;
            }
            //else if (Convert.ToInt32(txtdays.Text) > 31)
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the days properly.');", true);
            //    //System.Windows.Forms.MessageBox.Show("Enter the Days Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            //    ErrFlag = true;
            //}
            if (FileUpload.HasFile)
            {
                FileUpload.SaveAs(Server.MapPath("Upload/" + FileUpload.FileName));

            }
            if (!ErrFlag)
            {
                DateTime dfrom = Convert.ToDateTime(txtFrom.Text);
                DateTime dtto = Convert.ToDateTime(txtTo.Text);
                DateTime MyDate1 = DateTime.ParseExact(txtFrom.Text, "dd-MM-yyyy", null);
                DateTime MyDate2 = DateTime.ParseExact(txtTo.Text, "dd-MM-yyyy", null);
                string Date_Check = "";
                Date_Check = "15-" + MyDate1.Date.Month.ToString() + "-" + MyDate1.Date.Year.ToString();
                if (dtto < dfrom)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the From Date...');", true);
                    txtFrom.Text = null;
                    txtTo.Text = null;
                    ErrFlag = true;
                }
                if (!ErrFlag)
                {
                    string connectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + Server.MapPath("Upload/" + FileUpload.FileName) + ";" + "Extended Properties=Excel 8.0;";
                    OleDbConnection sSourceConnection = new OleDbConnection(connectionString);
                    DataTable dts = new DataTable();
                    using (sSourceConnection)
                    {
                        sSourceConnection.Open();

                        OleDbCommand command = new OleDbCommand("Select * FROM [Sheet1$];", sSourceConnection);
                        sSourceConnection.Close();

                        using (OleDbCommand cmd = sSourceConnection.CreateCommand())
                        {
                            command.CommandText = "Select * FROM [Sheet1$];";
                            sSourceConnection.Open();
                        }
                        using (OleDbDataReader dr = command.ExecuteReader())
                        {

                            if (dr.HasRows)
                            {

                            }

                        }
                        OleDbDataAdapter objDataAdapter = new OleDbDataAdapter();
                        objDataAdapter.SelectCommand = command;
                        DataSet ds = new DataSet();

                        objDataAdapter.Fill(ds);
                        DataTable dt = new DataTable();
                        dt = ds.Tables[0];
                        for (int i = 0; i < dt.Columns.Count; i++)
                        {
                            dt.Columns[i].ColumnName = dt.Columns[i].ColumnName.Replace(" ", string.Empty).ToString();

                        }
                        string constr = ConfigurationManager.AppSettings["ConnectionString"];
                        for (int j = 0; j < dt.Rows.Count; j++)
                        {
                            SqlConnection cn = new SqlConnection(constr);
                            string Department = dt.Rows[j][0].ToString();
                            string MachineID = dt.Rows[j][1].ToString();
                            string EmpNo = dt.Rows[j][2].ToString();
                            string ExistingCode = dt.Rows[j][3].ToString();
                            string FirstName = dt.Rows[j][4].ToString();
                            string SAll3 = dt.Rows[j][5].ToString();
                            string SAll4 = dt.Rows[j][6].ToString();
                            string SAll5 = dt.Rows[j][7].ToString();
                            string Sded3 = dt.Rows[j][8].ToString();
                            string Sded4 = dt.Rows[j][9].ToString();
                            string Sded5 = dt.Rows[j][10].ToString();
                            string SAdvance = dt.Rows[j][11].ToString();

                           

                            if (Department == "")
                            {
                                j = j + 2;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Department. The Row Number is " + j + "');", true);
                                //System.Windows.Forms.MessageBox.Show("Enter the Department. The Row number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                ErrFlag = true;
                            }
                            else if (MachineID == "")
                            {
                                j = j + 2;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Machine ID. The Row Number is " + j + "');", true);
                                //System.Windows.Forms.MessageBox.Show("Enter the Machine ID. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                ErrFlag = true;
                            }
                            else if (EmpNo == "")
                            {
                                j = j + 2;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Employee Number. The Row Number is " + j + "');", true);
                                //System.Windows.Forms.MessageBox.Show("Enter the Employee Number. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                ErrFlag = true;
                            }
                            else if (ExistingCode == "")
                            {
                                j = j + 2;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Existing Code. The Row Number is " + j + "');", true);
                                //System.Windows.Forms.MessageBox.Show("Enter the Existing Code. The Roe Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                ErrFlag = true;
                            }
                            else if (FirstName == "")
                            {
                                j = j + 2;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Name. The Row Number is " + j + "');", true);
                                //System.Windows.Forms.MessageBox.Show("Enter the Name. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                ErrFlag = true;
                            }
                            else if (SAll3 == "")
                            {
                                j = j + 2;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Allowance 3s. The Row Number is " + j + "');", true);
                                //System.Windows.Forms.MessageBox.Show("Enter the Name. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                ErrFlag = true;
                            }
                            else if (SAll4 == "")
                            {
                                j = j + 2;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Allowance 4. The Row Number is " + j + "');", true);
                                //System.Windows.Forms.MessageBox.Show("Enter the Name. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                ErrFlag = true;
                            }
                            else if (SAll5 == "")
                            {
                                j = j + 2;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Allowance 5. The Row Number is " + j + "');", true);
                                //System.Windows.Forms.MessageBox.Show("Enter the Name. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                ErrFlag = true;
                            }
                            else if (Sded3 == "")
                            {
                                j = j + 2;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Deduction 3. The Row Number is " + j + "');", true);
                                //System.Windows.Forms.MessageBox.Show("Enter the Name. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                ErrFlag = true;
                            }
                            else if (Sded4 == "")
                            {
                                j = j + 2;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Deduction 4. The Row Number is " + j + "');", true);
                                //System.Windows.Forms.MessageBox.Show("Enter the Name. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                ErrFlag = true;
                            }
                            else if (Sded5 == "")
                            {
                                j = j + 2;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Deduction 5. The Row Number is " + j + "');", true);
                                //System.Windows.Forms.MessageBox.Show("Enter the Name. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                ErrFlag = true;
                            }
                            else if (SAdvance == "")
                            {
                                j = j + 2;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Advance. The Row Number is " + j + "');", true);
                                //System.Windows.Forms.MessageBox.Show("Enter the Name. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                ErrFlag = true;
                            }
                            cn.Open();
                            string qry_dpt = "Select DepartmentNm from MstDepartment where DepartmentNm = '" + Department + "'";
                            SqlCommand cmd_dpt = new SqlCommand(qry_dpt, cn);
                            SqlDataReader sdr_dpt = cmd_dpt.ExecuteReader();
                            if (sdr_dpt.HasRows)
                            {
                            }
                            else
                            {
                                j = j + 2;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Department name not found in the department Details. The Row Number is " + j + "');", true);
                                //System.Windows.Forms.MessageBox.Show("Department Name not Found in Department Details. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                ErrFlag = true;
                            }
                            cn.Close();
                            cn.Open();
                            string qry_dpt1 = "Select ED.Department from EmployeeDetails ED inner Join MstDepartment MSt on Mst.DepartmentCd=ED.Department where DepartmentNm = '" + Department + "' and ED.EmpNo='" + EmpNo + "'";
                            SqlCommand cmd_dpt1 = new SqlCommand(qry_dpt1, cn);
                            SqlDataReader sdr_dpt1 = cmd_dpt1.ExecuteReader();
                            if (sdr_dpt1.HasRows)
                            {
                            }
                            else
                            {
                                j = j + 2;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Department name not found in the department Details. The Row Number is " + j + "');", true);
                                //System.Windows.Forms.MessageBox.Show("Department Name not Found in Department Details. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                ErrFlag = true;
                            }
                            cn.Close();
                            cn.Open();
                            string Qry_SalaryType = "Select EmpNo from Officialprofile where WagesType ='" + rbsalary.SelectedValue + "'";
                            SqlCommand cmd_wages = new SqlCommand(Qry_SalaryType, cn);
                            SqlDataReader sdr_wages = cmd_wages.ExecuteReader();
                            if (sdr_wages.HasRows)
                            {
                            }
                            else
                            {
                                j = j + 2;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('This " + EmpNo + " is Wages Type is incorrect.');", true);
                                //System.Windows.Forms.MessageBox.Show("Department Name not Found in Department Details. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                ErrFlag = true;
                            }
                            cn.Close();
                            cn.Open();
                            string qry_empNo = "Select EmpNo from EmployeeDetails where EmpNo= '" + EmpNo + "' and BiometricID = '" + MachineID + "' and ExisistingCode = '" + ExistingCode + "' and EmpName = '" + FirstName + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                            SqlCommand cmd_Emp = new SqlCommand(qry_empNo, cn);
                            SqlDataReader sdr_Emp = cmd_Emp.ExecuteReader();
                            if (sdr_Emp.HasRows)
                            {
                            }
                            else
                            {
                                j = j + 2;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Employee Details Properly. The Row Number is " + j + "');", true);
                                //System.Windows.Forms.MessageBox.Show("Enter the Employee Details Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                ErrFlag = true;
                            }
                            cn.Close();

                        }
                        if (!ErrFlag)
                        {
                            for (int i = 0; i < dt.Rows.Count; i++)
                            {
                                string Updateval = "1";
                                bool PassFlag = false;
                                bool ErrVerify = false;
                                string EmpNo = "";
                                SqlConnection cn = new SqlConnection(constr);
                                cn.Open();
                                string qry_dpt1 = "Select DepartmentCd from MstDepartment where DepartmentNm = '" + dt.Rows[i][0].ToString() + "'";
                                SqlCommand cmd_dpt1 = new SqlCommand(qry_dpt1, cn);
                                DepartmentCode = Convert.ToString(cmd_dpt1.ExecuteScalar());
                                string qry_emp = "Select EmpNo from SalaryDetails where EmpNo='" + dt.Rows[i][2].ToString() + "' and " +
                                                 "Month='" + ddlMonths.Text + "' and FinancialYear='" + ddlfinance.SelectedValue + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'" +
                                                 " and Process_Mode='1' and convert(datetime,FromDate,105) = convert(datetime,'" + txtFrom.Text + "',105) and convert(datetime, ToDate, 105) = convert(datetime, '" + txtTo.Text + "', 105)";
                                // " and Process_Mode='1' and convert(datetime,TODate,105) >= convert(datetime,'" + txtFrom.Text + "',105)";

                                SqlCommand cmd_verify = new SqlCommand(qry_emp, cn);
                                Name_Upload = Convert.ToString(cmd_verify.ExecuteScalar());
                                //DateTime dfrom = Convert.ToDateTime(txtFrom.Text);
                                //DateTime dtto = Convert.ToDateTime(txtTo.Text);
                                //DateTime MyDate1 = DateTime.ParseExact(txtFrom.Text, "dd-MM-yyyy", null);
                                //DateTime MyDate2 = DateTime.ParseExact(txtTo.Text, "dd-MM-yyyy", null);
                                TempDate = Convert.ToDateTime(txtFrom.Text).AddMonths(0).ToShortDateString();
                                EmpNo = dt.Rows[i][2].ToString();
                                All3 = dt.Rows[i][5].ToString();
                                Al4 = dt.Rows[i][6].ToString();
                                All5 = dt.Rows[i][7].ToString();
                                ded3 = dt.Rows[i][8].ToString();
                                ded4 = dt.Rows[i][9].ToString();
                                ded5 = dt.Rows[i][10].ToString();
                                Advance = dt.Rows[i][11].ToString();
                                ExistNo = dt.Rows[i][3].ToString();

                                if (ExistNo == "547")
                                {
                                    string nw = "";
                                }

                                string Month = TempDate;
                                DateTime m;
                                m = Convert.ToDateTime(TempDate);
                                d = m.Month;
                                int mon = m.Month;
                                int yr = m.Year;
                                #region Month
                                string Mont = "";
                                switch (d)
                                {
                                    case 1:
                                        Mont = "January";
                                        break;

                                    case 2:
                                        Mont = "February";
                                        break;
                                    case 3:
                                        Mont = "March";
                                        break;
                                    case 4:
                                        Mont = "April";
                                        break;
                                    case 5:
                                        Mont = "May";
                                        break;
                                    case 6:
                                        Mont = "June";
                                        break;
                                    case 7:
                                        Mont = "July";
                                        break;
                                    case 8:
                                        Mont = "August";
                                        break;
                                    case 9:
                                        Mont = "September";
                                        break;
                                    case 10:
                                        Mont = "October";
                                        break;
                                    case 11:
                                        Mont = "November";
                                        break;
                                    case 12:
                                        Mont = "December";
                                        break;
                                    default:
                                        break;
                                }
                                #endregion

                                SMonth = Convert.ToString(mon);
                                Year = Convert.ToString(yr);
                                dd = DateTime.DaysInMonth(yr, mon);
                                MyMonth = Mont;
                                DataTable dtAttenance = new DataTable();

                                dtAttenance = objdata.Salary_attenanceDetails(EmpNo, Mont, ddlfinance.SelectedValue, DepartmentCode, SessionCcode, SessionLcode, MyDate1, MyDate2);
                                if (dtAttenance.Rows.Count <= 0)
                                {
                                    //ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('No Data Found');", true);
                                    //ErrVerify = true;

                                    EmployeeDays = "0";
                                    Work = "0";
                                    WorikingDays = "0";
                                    NFh = "0";
                                    cl = "0";
                                    lopdays = "0";
                                    WeekOff = "0";
                                    Home = "0";
                                    halfNight = "0";
                                    FullNight = "0";
                                    ThreeSided = "0";
                                    Rest = "0";
                                    FixedDays = "0";
                                    BonusDays = "0";
                                    NfhCount = "0";
                                    NfhPresent = "0";
                                    FineAmt = "0";
                                    WHPresent = "0";

                                }
                                else
                                {
                                    if (dtAttenance.Rows.Count > 0)
                                    {

                                        EmployeeDays = dtAttenance.Rows[0]["Days"].ToString(); //Get Working Days from Attendance Details
                                        Work = dtAttenance.Rows[0]["TotalDays"].ToString();
                                        WorikingDays = dtAttenance.Rows[0]["WorkingDays"].ToString();
                                        NFh = dtAttenance.Rows[0]["NFh"].ToString();
                                        cl = dtAttenance.Rows[0]["CL"].ToString();
                                        lopdays = dtAttenance.Rows[0]["AbsentDays"].ToString();
                                        WeekOff = dtAttenance.Rows[0]["weekoff"].ToString();
                                        Home = dtAttenance.Rows[0]["home"].ToString();
                                        halfNight = dtAttenance.Rows[0]["halfNight"].ToString();
                                        FullNight = dtAttenance.Rows[0]["FullNight"].ToString();
                                        ThreeSided = dtAttenance.Rows[0]["ThreeSided"].ToString();
                                        Rest = dtAttenance.Rows[0]["Rest"].ToString();
                                        FixedDays = dtAttenance.Rows[0]["FixedDays"].ToString();
                                        BonusDays = dtAttenance.Rows[0]["BonusDays"].ToString();
                                        NfhCount = dtAttenance.Rows[0]["NfhCount"].ToString();
                                        NfhPresent = dtAttenance.Rows[0]["NfhPresent"].ToString();
                                        FineAmt = dtAttenance.Rows[0]["FineAmt"].ToString();
                                        WHPresent = dtAttenance.Rows[0]["WHPresent"].ToString();





                                        //txtlossofpay.Text = (Convert.ToDecimal(txtwork.Text) - (Convert.ToDecimal(txtempdays.Text) + (Convert.ToDecimal(txtNFh.Text) + (Convert.ToDecimal(txtcl.Text))))).ToString();
                                        DataTable dt_ot = new DataTable();
                                        dt_ot = objdata.Attenance_ot(EmpNo, Mont, ddlfinance.SelectedValue, DepartmentCode, SessionCcode, SessionLcode, MyDate1, MyDate2);
                                        if (dt_ot.Rows.Count > 0)
                                        {
                                            //OTDays = dt_ot.Rows[0]["netAmount"].ToString();
                                            OTDays = "0";
                                        }
                                        else
                                        {
                                            OTDays = "0";
                                        }
                                    }

                                    string Salarymonth = objdata.SalaryMonthFromLeave(EmpNo, ddlfinance.SelectedValue, SessionCcode, SessionLcode, Mont, MyDate1, MyDate2);

                                    string MonthEEE = Salarymonth.Replace(" ", "");

                                    int result = string.Compare(Salarymonth, Mont, true);
                                    //int i = DateTime.ParseExact(Salarymonth, "MMMM", CultureInfo.CurrentCulture).Month;
                                    string PayDay = objdata.SalaryPayDay();
                                    // txtattendancewithpay.Text = PayDay;

                                    if (Name_Upload == "")
                                    {
                                        if (Mont == MonthEEE)
                                        {
                                            Updateval = "1";
                                            //ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('This " + Mont + " Month Salary All Ready Processed...!');", true);
                                            //ErrVerify = true;
                                        }
                                        else
                                        {
                                            Updateval = "1";
                                        }

                                    }
                                    else
                                    {
                                        Updateval = "0";
                                    }
                                }
                                        //if (Mont == MonthEEE)
                                        //{
                                        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('This " + Mont + " Month Salary All Ready Processed...!');", true);
                                        //    ErrVerify = true;
                                        //}
                                        //else
                                        {
                                            if (ddlcategory.SelectedValue == "1") //STAFF SALARY CALCULATION
                                            {
                                                
                                                AdvAmt = 0;
                                                ID = "";
                                                string Employee_Type_Check = "";
                                                DataTable dt1 = new DataTable();
                                                DataTable dteligible = new DataTable();
                                                DataTable dtpf = new DataTable();
                                                DataTable dtSalDiv = new DataTable();
                                                dt1 = objdata.NewSlary_Load(SessionCcode, SessionLcode, EmpNo);

                                                string qry_emptype = "Select EmployeeType from EmployeeDetails Where EmpNo='" + dt.Rows[i][2].ToString() + "' And Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                                                SqlCommand cmd_emptype_verify = new SqlCommand(qry_emptype, cn);
                                                Employee_Type_Check = Convert.ToString(cmd_emptype_verify.ExecuteScalar());

                                                if (ddlcategory.SelectedValue == "1")
                                                {
                                                    Stafflabour = "S";
                                                }
                                                else if (ddlcategory.SelectedValue == "2")
                                                {
                                                    Stafflabour = "L";
                                                }
                                                else
                                                {
                                                    Stafflabour = "0";
                                                }

                                               
                                                dtpf = objdata.Load_pf_details(SessionCcode, SessionLcode);
                                                dteligible = objdata.EligibleESI_PF(SessionCcode, SessionLcode, EmpNo);
                                                dtSalDiv = objdata.MstSalDivisionSelect_SP(rbsalary.SelectedValue, Stafflabour, Employee_Type_Check, SessionCcode, SessionLcode);


                                                if (dt1.Rows.Count > 0)
                                                {

                                                    //PFS = dt1.Rows[0]["PFS"].ToString();
                                                    if (FixedDays == "0")
                                                    {
                                                       
                                                        basic = "0";
                                                        FBasic = "0";
                                                        FFHRA = "0";
                                                        BasicWages = "0";
                                                        HRA = "0";
                                                        TA = "0";



                                                    }
                                                    else
                                                    {
                                                        FineAmt_Sal = FineAmt;
                                                        BasicWages = (Convert.ToDecimal(dt1.Rows[0]["Base"].ToString())).ToString();
                                                        HRA = (Convert.ToDecimal(dt1.Rows[0]["HRA"].ToString())).ToString();
                                                        TA = (Convert.ToDecimal(dt1.Rows[0]["FDA"].ToString())).ToString();

                                                        FFTotal = (Convert.ToDecimal(BasicWages) + Convert.ToDecimal(HRA) + Convert.ToDecimal(TA)).ToString();

                                                        EmployeeDays = (Convert.ToDecimal(NfhPresent) + Convert.ToDecimal(EmployeeDays) + Convert.ToDecimal(NfhCount)).ToString();

                                                        FBasic = (Convert.ToDecimal(FFTotal) / Convert.ToDecimal(FixedDays)).ToString();
                                                        FFHRA = (Convert.ToDecimal(FBasic) * Convert.ToDecimal(WHPresent)).ToString();
                                                        FFHRA = (Math.Round(Convert.ToDecimal(FFHRA), 0, MidpointRounding.AwayFromZero)).ToString();  // calculate remainning days to salary

                                                        FFDA = Convert.ToString(Convert.ToDecimal(WHPresent));

                                                        BasicWages = (Convert.ToDecimal(BasicWages) / Convert.ToDecimal(FixedDays)).ToString();
                                                        BasicWages = (Convert.ToDecimal(BasicWages) * (Convert.ToDecimal(EmployeeDays))).ToString();
                                                        BasicWages = (Math.Round(Convert.ToDecimal(BasicWages), 0, MidpointRounding.AwayFromZero)).ToString();


                                                        //Commend by Selva start
                                                        HRA = (Convert.ToDecimal(HRA) / Convert.ToDecimal(FixedDays)).ToString();
                                                        HRA = (Convert.ToDecimal(HRA) * (Convert.ToDecimal(EmployeeDays))).ToString();
                                                        HRA = (Math.Round(Convert.ToDecimal(HRA), 0, MidpointRounding.AwayFromZero)).ToString();

                                                        TA = (Convert.ToDecimal(TA) / Convert.ToDecimal(FixedDays)).ToString();
                                                        TA = (Convert.ToDecimal(TA) * (Convert.ToDecimal(EmployeeDays))).ToString();
                                                        TA = (Math.Round(Convert.ToDecimal(TA), 0, MidpointRounding.AwayFromZero)).ToString();

                                                        basic = (Convert.ToDecimal(BasicWages) + Convert.ToDecimal(HRA) + Convert.ToDecimal(TA)).ToString();

                                                        TotalWages = basic;
                                                        //Commend by Selva end

                                                        //HRA = (Convert.ToDecimal(BasicWages) * Convert.ToDecimal(0.2)).ToString();
                                                        ////HRA = (Convert.ToDecimal(HRA) * (Convert.ToDecimal(EmployeeDays))).ToString();
                                                        //HRA = (Math.Round(Convert.ToDecimal(HRA), 0, MidpointRounding.AwayFromZero)).ToString();


                                                        //ConveyAllowance = (Convert.ToDecimal(HRA) / Convert.ToDecimal(4)).ToString();
                                                        //ConveyAllowance = (Math.Round(Convert.ToDecimal(ConveyAllowance), 0, MidpointRounding.AwayFromZero)).ToString();

                                                        //WashingAloowance = (Convert.ToDecimal(HRA) / Convert.ToDecimal(4)).ToString();
                                                        //WashingAloowance = (Math.Round(Convert.ToDecimal(WashingAloowance), 0, MidpointRounding.AwayFromZero)).ToString();
                                                        //TA = "0.00";


                                                        //basic = (Convert.ToDecimal(BasicWages) + Convert.ToDecimal(HRA) + Convert.ToDecimal(ConveyAllowance) + Convert.ToDecimal(WashingAloowance)).ToString();


                                                        //if (ddlMonths.SelectedValue == "February")
                                                        //{
                                                            
                                                        //    if (Convert.ToDecimal(EmployeeDays) > Convert.ToDecimal(24)) // Grater than fixed days 
                                                        //    {

                                                        //        DaysCount = (Convert.ToDecimal(EmployeeDays) - Convert.ToDecimal(24)).ToString(); // Minius employee days to fixed 

                                                        //        FBasic = (Convert.ToDecimal(FFTotal) / Convert.ToDecimal(24)).ToString();
                                                        //        FFHRA = (Convert.ToDecimal(FBasic) * (Convert.ToDecimal(DaysCount) + Convert.ToDecimal(NFh))).ToString();
                                                        //        FFHRA = (Math.Round(Convert.ToDecimal(FFHRA), 0, MidpointRounding.AwayFromZero)).ToString();  // calculate remainning days to salary

                                                        //        FFDA = Convert.ToString(Convert.ToDecimal(DaysCount) + Convert.ToDecimal(NFh));

                                                        //        BasicWages = (Convert.ToDecimal(BasicWages) / Convert.ToDecimal(24)).ToString();
                                                        //        BasicWages = (Convert.ToDecimal(BasicWages) * Convert.ToDecimal(24)).ToString();
                                                        //        BasicWages = (Math.Round(Convert.ToDecimal(BasicWages), 0, MidpointRounding.AwayFromZero)).ToString();

                                                        //        HRA = (Convert.ToDecimal(HRA) / Convert.ToDecimal(24)).ToString();
                                                        //        HRA = (Convert.ToDecimal(HRA) * Convert.ToDecimal(24)).ToString();
                                                        //        HRA = (Math.Round(Convert.ToDecimal(HRA), 0, MidpointRounding.AwayFromZero)).ToString();

                                                        //        TA = (Convert.ToDecimal(TA) / Convert.ToDecimal(24)).ToString();
                                                        //        TA = (Convert.ToDecimal(TA) * Convert.ToDecimal(24)).ToString();
                                                        //        TA = (Math.Round(Convert.ToDecimal(TA), 0, MidpointRounding.AwayFromZero)).ToString();

                                                        //        basic = (Convert.ToDecimal(BasicWages) + Convert.ToDecimal(HRA) + Convert.ToDecimal(TA)).ToString();
                                                                  

                                                        //        EmployeeDays = Convert.ToString(24);


                                                        //    }
                                                        //    else
                                                        //    {
                                                               

                                                        //       // DaysCount = (Convert.ToDecimal(EmployeeDays) - Convert.ToDecimal(24)).ToString(); // Minius employee days to fixed 

                                                        //        FBasic = (Convert.ToDecimal(FFTotal) / Convert.ToDecimal(24)).ToString();
                                                        //        FFHRA = (Convert.ToDecimal(FBasic) * Convert.ToDecimal(NFh)).ToString();
                                                        //        FFHRA = (Math.Round(Convert.ToDecimal(FFHRA), 0, MidpointRounding.AwayFromZero)).ToString();  // calculate remainning days to salary

                                                        //        FFDA = Convert.ToString(Convert.ToDecimal(NFh));

                                                        //        BasicWages = (Convert.ToDecimal(BasicWages) / Convert.ToDecimal(24)).ToString();
                                                        //        BasicWages = (Convert.ToDecimal(BasicWages) * (Convert.ToDecimal(EmployeeDays))).ToString();
                                                        //        BasicWages = (Math.Round(Convert.ToDecimal(BasicWages), 0, MidpointRounding.AwayFromZero)).ToString();

                                                        //        HRA = (Convert.ToDecimal(HRA) / Convert.ToDecimal(24)).ToString();
                                                        //        HRA = (Convert.ToDecimal(HRA) * (Convert.ToDecimal(EmployeeDays))).ToString();
                                                        //        HRA = (Math.Round(Convert.ToDecimal(HRA), 0, MidpointRounding.AwayFromZero)).ToString();

                                                        //        TA = (Convert.ToDecimal(TA) / Convert.ToDecimal(24)).ToString();
                                                        //        TA = (Convert.ToDecimal(TA) * (Convert.ToDecimal(EmployeeDays))).ToString();
                                                        //        TA = (Math.Round(Convert.ToDecimal(TA), 0, MidpointRounding.AwayFromZero)).ToString();

                                                        //        basic = (Convert.ToDecimal(BasicWages) + Convert.ToDecimal(HRA) + Convert.ToDecimal(TA)).ToString();

                                                        //    }

                                                        //}
                                                        //else
                                                        //{
                                                        //    if (Convert.ToDecimal(EmployeeDays) > Convert.ToDecimal(26))
                                                        //    {


                                                        //        DaysCount = (Convert.ToDecimal(EmployeeDays) - Convert.ToDecimal(26)).ToString(); // Minius employee days to fixed 

                                                        //        FBasic = (Convert.ToDecimal(FFTotal) / Convert.ToDecimal(26)).ToString();
                                                        //        FFHRA = (Convert.ToDecimal(FBasic) * (Convert.ToDecimal(DaysCount) + Convert.ToDecimal(NFh))).ToString();
                                                        //        FFHRA = (Math.Round(Convert.ToDecimal(FFHRA), 0, MidpointRounding.AwayFromZero)).ToString();  // calculate remainning days to salary

                                                        //        FFDA = Convert.ToString(Convert.ToDecimal(DaysCount) + Convert.ToDecimal(NFh));


                                                        //        BasicWages = (Convert.ToDecimal(BasicWages) / Convert.ToDecimal(26)).ToString();
                                                        //        BasicWages = (Convert.ToDecimal(BasicWages) * Convert.ToDecimal(26)).ToString();
                                                        //        BasicWages = (Math.Round(Convert.ToDecimal(BasicWages), 0, MidpointRounding.AwayFromZero)).ToString();

                                                        //        HRA = (Convert.ToDecimal(HRA) / Convert.ToDecimal(26)).ToString();
                                                        //        HRA = (Convert.ToDecimal(HRA) * Convert.ToDecimal(26)).ToString();
                                                        //        HRA = (Math.Round(Convert.ToDecimal(HRA), 0, MidpointRounding.AwayFromZero)).ToString();

                                                        //        TA = (Convert.ToDecimal(TA) / Convert.ToDecimal(26)).ToString();
                                                        //        TA = (Convert.ToDecimal(TA) * Convert.ToDecimal(26)).ToString();
                                                        //        TA = (Math.Round(Convert.ToDecimal(TA), 0, MidpointRounding.AwayFromZero)).ToString();

                                                        //        basic = (Convert.ToDecimal(BasicWages) + Convert.ToDecimal(HRA) + Convert.ToDecimal(TA)).ToString();

                                                        //        EmployeeDays = Convert.ToString(26);
                                                        //    }
                                                        //    else
                                                        //    {
                                                        //        FBasic = (Convert.ToDecimal(FFTotal) / Convert.ToDecimal(26)).ToString();
                                                        //        FFHRA = (Convert.ToDecimal(FBasic) * Convert.ToDecimal(NFh)).ToString();
                                                        //        FFHRA = (Math.Round(Convert.ToDecimal(FFHRA), 0, MidpointRounding.AwayFromZero)).ToString();  // calculate remainning days to salary

                                                        //        FFDA = Convert.ToString(Convert.ToDecimal(NFh));


                                                        //        BasicWages = (Convert.ToDecimal(BasicWages) / Convert.ToDecimal(26)).ToString();
                                                        //        BasicWages = (Convert.ToDecimal(BasicWages) * (Convert.ToDecimal(EmployeeDays))).ToString();
                                                        //        BasicWages = (Math.Round(Convert.ToDecimal(BasicWages), 0, MidpointRounding.AwayFromZero)).ToString();

                                                        //        HRA = (Convert.ToDecimal(HRA) / Convert.ToDecimal(26)).ToString();
                                                        //        HRA = (Convert.ToDecimal(HRA) * (Convert.ToDecimal(EmployeeDays))).ToString();
                                                        //        HRA = (Math.Round(Convert.ToDecimal(HRA), 0, MidpointRounding.AwayFromZero)).ToString();

                                                        //        TA = (Convert.ToDecimal(TA) / Convert.ToDecimal(26)).ToString();
                                                        //        TA = (Convert.ToDecimal(TA) * (Convert.ToDecimal(EmployeeDays))).ToString();
                                                        //        TA = (Math.Round(Convert.ToDecimal(TA), 0, MidpointRounding.AwayFromZero)).ToString();

                                                        //        basic = (Convert.ToDecimal(BasicWages) + Convert.ToDecimal(HRA) + Convert.ToDecimal(TA)).ToString();
                                                        //    }

                                                        //}
                                           
                                                    }

                                                    //txtSAll1.Text = (Convert.ToDecimal(dt1.Rows[0]["Alllowance1amt"].ToString()) * Convert.ToDecimal(txtempdays.Text)).ToString();

                                                    //STAFF VDA FDA HRA
                                                    //VDA = ((Convert.ToDecimal(EmployeeDays)) * Convert.ToDecimal(dt1.Rows[0]["VDA"].ToString())).ToString();
                                                    VDA = (Convert.ToDecimal(dt1.Rows[0]["VDA"].ToString())).ToString();
                                                    VDA = (Math.Round(Convert.ToDecimal(VDA), 0, MidpointRounding.AwayFromZero)).ToString();

                                                    //FDA = ((Convert.ToDecimal(EmployeeDays)) * Convert.ToDecimal(dt1.Rows[0]["FDA"].ToString())).ToString();
                                                    FDA = (Convert.ToDecimal(dt1.Rows[0]["FDA"].ToString())).ToString();
                                                    FDA = (Math.Round(Convert.ToDecimal(FDA), 0, MidpointRounding.AwayFromZero)).ToString();

                                                    //FixedBasicDA = (Convert.ToDecimal(basic) * Convert.ToDecimal(dtSalDiv.Rows[0]["BasicDA"].ToString())).ToString();
                                                    //FixedBasicDA = (Convert.ToDecimal(FixedBasicDA) / Convert.ToDecimal(100)).ToString();
                                                    //FixedBasicDA = (Math.Round(Convert.ToDecimal(FixedBasicDA), 0, MidpointRounding.AwayFromZero)).ToString();

                                                    //HRA = Convert.ToString(Convert.ToDecimal(basic) * Convert.ToDecimal(dtSalDiv.Rows[0]["HRA"].ToString()));
                                                    //HRA = Convert.ToString(Convert.ToDecimal(HRA) / Convert.ToDecimal(100));
                                                    //HRA = (Math.Round(Convert.ToDecimal(HRA), 0, MidpointRounding.AwayFromZero)).ToString();

                                              
                                                    //All1 = ((Convert.ToDecimal(EmployeeDays)) * Convert.ToDecimal(dt1.Rows[0]["Alllowance1amt"].ToString())).ToString();
                                                    All1 = (Convert.ToDecimal(dt1.Rows[0]["Alllowance1amt"].ToString())).ToString();
                                                    All1 = (Math.Round(Convert.ToDecimal(All1), 0, MidpointRounding.AwayFromZero)).ToString();
                                                    //txtSAll2.Text = (Convert.ToDecimal(dt1.Rows[0]["Allowance2amt"].ToString()) * Convert.ToDecimal(txtempdays.Text)).ToString();

                                                    //All2 = ((Convert.ToDecimal(EmployeeDays)) * Convert.ToDecimal(dt1.Rows[0]["Allowance2amt"].ToString())).ToString();
                                                    All2 = (Convert.ToDecimal(dt1.Rows[0]["Allowance2amt"].ToString())).ToString();
                                                    All2 = (Math.Round(Convert.ToDecimal(All2), 0, MidpointRounding.AwayFromZero)).ToString();
                                                    //txtSded1.Text = (Convert.ToDecimal(dt1.Rows[0]["Deduction1"].ToString()) * Convert.ToDecimal(txtempdays.Text)).ToString();

                                                    //Ded1 = ((Convert.ToDecimal(EmployeeDays) + Convert.ToDecimal(Rest)) * Convert.ToDecimal(dt1.Rows[0]["Deduction1"].ToString())).ToString();
                                                    Ded1 = (Convert.ToDecimal(dt1.Rows[0]["Deduction1"].ToString())).ToString();
                                                    //Ded1 = (Convert.ToInt32(EmployeeDays) * Convert.ToInt32(Get_Dut1)).ToString();  
                                                    Ded1 = (Math.Round(Convert.ToDecimal(Ded1), 0, MidpointRounding.AwayFromZero)).ToString();
                                                    //txtSded2.Text = (Convert.ToDecimal(dt1.Rows[0]["Deduction2"].ToString()) * Convert.ToDecimal(txtempdays.Text)).ToString();

                                                    //ded2 = ((Convert.ToDecimal(EmployeeDays)) * Convert.ToDecimal(dt1.Rows[0]["Deduction2"].ToString())).ToString();
                                                    ded2 = (Convert.ToDecimal(dt1.Rows[0]["Deduction2"].ToString())).ToString();
                                                    ded2 = (Math.Round(Convert.ToDecimal(ded2), 0, MidpointRounding.AwayFromZero)).ToString();

                                                    //lop = ((Convert.ToDecimal(dt1.Rows[0]["Base"].ToString()) / Convert.ToDecimal(Work)) * Convert.ToDecimal(lop)).ToString();
                                                    lop = (Convert.ToDecimal(0)).ToString();
                                                    lop = (Math.Round(Convert.ToDecimal(lop), 0, MidpointRounding.AwayFromZero)).ToString();
                                                    stamp = "0";
                                                    
                                                    HalfNight_Amt = "0.00";
                                                    FullNight_Amt = "0.00";
                                                    DayIncentive = "0.00";
                                                    SpinningAmt = "0.00";
                                                    ThreeSided_Amt = "0.00";

                                                    //decimal basicsalary = (Convert.ToDecimal(basic) - Convert.ToDecimal(lop));

                                                   
                                                    if (dteligible.Rows[0]["EligiblePF"].ToString() == "1")
                                                    {
                                                        PF_salary = "0";
                                                        pfAmt = (Convert.ToDecimal(BasicWages)).ToString();
                                                        pfAmt = (Convert.ToDecimal(Convert.ToDecimal(pfAmt) * Convert.ToDecimal(dtpf.Rows[0]["PF_per"].ToString()))).ToString();
                                                        pfAmt = (Convert.ToDecimal(Convert.ToDecimal(pfAmt) / 100)).ToString();
                                                        pfAmt = (Math.Round(Convert.ToDecimal(pfAmt), 0, MidpointRounding.AwayFromZero)).ToString();
                                                        PFS = pfAmt;
                                                    }
                                                    else
                                                    {
                                                        pfAmt = "0";
                                                        PFS = "0";

                                                    }

                                                    string dt_ot1 = "";
                                                    dt_ot1 = objdata.OT_Salary(EmpNo, SessionCcode, SessionLcode, Mont, MyDate1, MyDate2);
                                                    if (dt_ot1.Trim() != "")
                                                    {
                                                        //OTDays = dt_ot1.ToString();
                                                        // OTDays = "0";
                                                        OTDays = "0";
                                                    }
                                                    else
                                                    {
                                                        OTDays = "0";
                                                    }


                                                    if (dteligible.Rows[0]["ElgibleESI"].ToString() == "1")
                                                    {

                                                        ESI = (Convert.ToDecimal(basic)).ToString();
                                                        ESI = (Convert.ToDecimal(Convert.ToDecimal(ESI) * Convert.ToDecimal(dtpf.Rows[0]["ESI_per"].ToString()))).ToString();
                                                        ESI = (Convert.ToDecimal(Convert.ToDecimal(ESI) / 100)).ToString();


                                                        //ESI = ((ESI_val * Convert.ToDecimal(1.75)) / 100).ToString();
                                                        ESI = (Math.Round(Convert.ToDecimal(ESI), 2, MidpointRounding.ToEven)).ToString();
                                                        string[] split_val = ESI.Split('.');
                                                        string Ist = split_val[0].ToString();
                                                        string IInd = split_val[1].ToString();

                                                        if (Convert.ToInt32(IInd) > 0)
                                                        {
                                                            ESI = (Convert.ToInt32(Ist) + 1).ToString();
                                                        }
                                                        else
                                                        {
                                                            ESI = Ist;
                                                        }

                                                    }
                                                    else
                                                    {
                                                        ESI = "0";
                                                    }
                                                    //txtStamp.Text = dtpf.Rows[0]["StampCg"].ToString();

                                                    //TotalEarnings = (Convert.ToDecimal(BasicWages) + Convert.ToDecimal(HRA) + Convert.ToDecimal(TA) + Convert.ToDecimal(All1) + Convert.ToDecimal(All2) + Convert.ToDecimal(All3) + Convert.ToDecimal(Al4) + Convert.ToDecimal(OTDays) + Convert.ToDecimal(All5) + Convert.ToDecimal(HalfNight_Amt) + Convert.ToDecimal(FullNight_Amt) + Convert.ToDecimal(SpinningAmt) + Convert.ToDecimal(ThreeSided_Amt) + Convert.ToDecimal(DayIncentive)).ToString();

                                                    TotalEarnings = (Convert.ToDecimal(BasicWages) + Convert.ToDecimal(HRA) + Convert.ToDecimal(TA) + Convert.ToDecimal(ConveyAllowance) + Convert.ToDecimal(WashingAloowance) + Convert.ToDecimal(All1) + Convert.ToDecimal(All2) + Convert.ToDecimal(All3) + Convert.ToDecimal(Al4) + Convert.ToDecimal(OTDays) + Convert.ToDecimal(All5) + Convert.ToDecimal(HalfNight_Amt) + Convert.ToDecimal(FullNight_Amt) + Convert.ToDecimal(SpinningAmt) + Convert.ToDecimal(ThreeSided_Amt) + Convert.ToDecimal(DayIncentive)).ToString();
                                                    TotalEarnings = (Math.Round(Convert.ToDecimal(TotalEarnings), 2, MidpointRounding.ToEven)).ToString();
                                                    TotalDeductions = (Convert.ToDecimal(pfAmt) + Convert.ToDecimal(ESI) + Convert.ToDecimal(Advance) + Convert.ToDecimal(stamp) + Convert.ToDecimal(lop) + Convert.ToDecimal(Ded1) + Convert.ToDecimal(ded2) + Convert.ToDecimal(ded3) + Convert.ToDecimal(ded4) + Convert.ToDecimal(ded5) + Convert.ToDecimal(FineAmt_Sal)).ToString();
                                                    TotalDeductions = (Math.Round(Convert.ToDecimal(TotalDeductions), 2, MidpointRounding.ToEven)).ToString();
                                                    NetPay = (Convert.ToDecimal(TotalEarnings) - Convert.ToDecimal(TotalDeductions)).ToString();
                                                    NetPay = (Math.Round(Convert.ToDecimal(NetPay), 0, MidpointRounding.AwayFromZero)).ToString();
                                                    if (Convert.ToDecimal(NetPay) < 0)
                                                    {
                                                        NetPay = "0";
                                                    }
                                                    Words = NumerictoNumber(Convert.ToInt32(NetPay), isUK).ToString() + " " + "Only";
                                                }
                                            }
                                            else          //LABOUR SALARY Calculate
                                            {
                                                string Employee_Type_Check = "";

                                                string qry_emptype = "Select EmployeeType from EmployeeDetails Where EmpNo='" + dt.Rows[i][2].ToString() + "' And Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                                                SqlCommand cmd_emptype_verify = new SqlCommand(qry_emptype, cn);
                                                Employee_Type_Check = Convert.ToString(cmd_emptype_verify.ExecuteScalar());
                                                AdvAmt = 0;
                                                ID = "";
                                                DataTable dt1 = new DataTable();
                                                dt1 = objdata.NewSlary_Load(SessionCcode, SessionLcode, EmpNo);

                                                DataTable dteligible = new DataTable();
                                                DataTable dtpf = new DataTable();
                                                DataTable dtMess = new DataTable();
                                                DataTable dtSalDiv = new DataTable();
                                                DataTable dtIncentive = new DataTable();
                                                dtpf = objdata.Load_pf_details(SessionCcode, SessionLcode);
                                                dteligible = objdata.EligibleESI_PF(SessionCcode, SessionLcode, EmpNo);

                                                if (ddlcategory.SelectedValue == "1")
                                                {
                                                    Stafflabour = "S";
                                                }
                                                else if (ddlcategory.SelectedValue == "2")
                                                {
                                                    Stafflabour = "L";
                                                }
                                                else
                                                {
                                                    Stafflabour = "0";
                                                }
                                                //txtStamp.Text = dtpf.Rows[0]["StampCg"].ToString();

                                                dtMess = objdata.MstMessAmtSelect_SP(rbsalary.SelectedValue, Stafflabour, Employee_Type_Check, SessionCcode, SessionLcode);
                                                dtSalDiv = objdata.MstSalDivisionSelect_SP(rbsalary.SelectedValue, Stafflabour, Employee_Type_Check, SessionCcode, SessionLcode);

                                                dtIncentive = objdata.MstIncentive_Get(rbsalary.SelectedValue, Stafflabour, Employee_Type_Check);
                                                
                                              
                                                if (dt1.Rows.Count > 0)
                                                {
                                                    //Basic Salary Labour
                                                    if ((Employee_Type_Check == "2") || (Employee_Type_Check == "4"))
                                                    {
                                                        if (EmpNo == "Per0420153195")
                                                        {
                                                            string stop = "";
                                                        }

                                                        if ((Convert.ToDecimal(EmployeeDays) + Convert.ToDecimal(NfhPresent) + Convert.ToDecimal(WHPresent)) >= Convert.ToDecimal(dtIncentive.Rows[0]["ShiftI"].ToString()))
                                                        {
                                                            EmployeeDays = (Convert.ToDecimal(EmployeeDays) + Convert.ToDecimal(NfhCount) + Convert.ToDecimal(NfhPresent)).ToString();

                                                            //DaysCount = (Convert.ToDecimal(EmployeeDays) - Convert.ToDecimal(26)).ToString(); // Minius employee days to fixed 
                                                            FineAmt_Sal = FineAmt;
                                                            FBasic = (Convert.ToDecimal(dt1.Rows[0]["Base"].ToString())).ToString();
                                                            FFHRA = (Convert.ToDecimal(FBasic) * Convert.ToDecimal(WHPresent)).ToString();
                                                            FFHRA = (Math.Round(Convert.ToDecimal(FFHRA), 0, MidpointRounding.AwayFromZero)).ToString();  // calculate remainning days to salary

                                                            FFDA = Convert.ToString(WHPresent);

                                                            basic = (Convert.ToDecimal(dt1.Rows[0]["Base"].ToString()) * (Convert.ToDecimal(EmployeeDays))).ToString();

                                                            basic = (Math.Round(Convert.ToDecimal(basic), 0, MidpointRounding.AwayFromZero)).ToString();

                                                            BasicWages = Convert.ToString(Convert.ToDecimal(basic) * Convert.ToDecimal(dtSalDiv.Rows[0]["BasicDA"].ToString()));
                                                            BasicWages = Convert.ToString(Convert.ToDecimal(BasicWages) / Convert.ToDecimal(100));
                                                            BasicWages = (Math.Round(Convert.ToDecimal(BasicWages), 0, MidpointRounding.AwayFromZero)).ToString();

                                                            //HRA amount percentage change 12/06/2020
                                                            //TotalWages = (Convert.ToDecimal(basic) - Convert.ToDecimal(BasicWages)).ToString();
                                                            //TotalWages = (Convert.ToDecimal(basic)).ToString();
                                                            //12/06/2020 end

                                                            //HRA = (Convert.ToDecimal(TotalWages) / Convert.ToDecimal(2)).ToString();
                                                            //HRA = (Math.Round(Convert.ToDecimal(HRA), 0, MidpointRounding.AwayFromZero)).ToString();

                                                            //TA = (Convert.ToDecimal(TotalWages) - Convert.ToDecimal(HRA)).ToString();

                                                            HRA = (Convert.ToDecimal(basic) * Convert.ToDecimal(0.2)).ToString();
                                                            HRA = (Math.Round(Convert.ToDecimal(HRA), 0, MidpointRounding.AwayFromZero)).ToString();

                                                            ConveyAllowance = (Convert.ToDecimal(HRA) / Convert.ToDecimal(4)).ToString();
                                                            ConveyAllowance = (Math.Round(Convert.ToDecimal(ConveyAllowance), 0, MidpointRounding.AwayFromZero)).ToString();

                                                            WashingAloowance=(Convert.ToDecimal(HRA) / Convert.ToDecimal(4)).ToString();
                                                            WashingAloowance = (Math.Round(Convert.ToDecimal(WashingAloowance), 0, MidpointRounding.AwayFromZero)).ToString();
                                                            TA = "0.00";

                                                            TotalWages = (Convert.ToDecimal(BasicWages) + Convert.ToDecimal(HRA) + Convert.ToDecimal(ConveyAllowance) + Convert.ToDecimal(WashingAloowance)).ToString();

                                                            //EmployeeDays = Convert.ToString(26);

                                                            //if (ddlMonths.SelectedValue == "February")
                                                            //{

                                                            //    if (Convert.ToDecimal(EmployeeDays) > Convert.ToDecimal(24)) // Grater than fixed days 
                                                            //    {
                                                            //        DaysCount = (Convert.ToDecimal(EmployeeDays) - Convert.ToDecimal(24)).ToString(); // Minius employee days to fixed 

                                                            //        FBasic = (Convert.ToDecimal(dt1.Rows[0]["Base"].ToString())).ToString();
                                                            //        FFHRA = (Convert.ToDecimal(FBasic) * Convert.ToDecimal(DaysCount)).ToString();
                                                            //        FFHRA = (Math.Round(Convert.ToDecimal(FFHRA), 0, MidpointRounding.AwayFromZero)).ToString();  // calculate remainning days to salary

                                                            //        FFDA = Convert.ToString(DaysCount);


                                                            //        basic = (Convert.ToDecimal(dt1.Rows[0]["Base"].ToString()) * (Convert.ToDecimal(24) + Convert.ToInt32(cl) + Convert.ToInt32(NFh))).ToString();

                                                            //        basic = (Math.Round(Convert.ToDecimal(basic), 0, MidpointRounding.AwayFromZero)).ToString();

                                                            //        BasicWages = Convert.ToString(Convert.ToDecimal(basic) * Convert.ToDecimal(dtSalDiv.Rows[0]["BasicDA"].ToString()));
                                                            //        BasicWages = Convert.ToString(Convert.ToDecimal(BasicWages) / Convert.ToDecimal(100));
                                                            //        BasicWages = (Math.Round(Convert.ToDecimal(BasicWages), 0, MidpointRounding.AwayFromZero)).ToString();

                                                            //        TotalWages = (Convert.ToDecimal(basic) - Convert.ToDecimal(BasicWages)).ToString();

                                                            //        HRA = (Convert.ToDecimal(TotalWages) / Convert.ToDecimal(2)).ToString();
                                                            //        HRA = (Math.Round(Convert.ToDecimal(HRA), 0, MidpointRounding.AwayFromZero)).ToString();

                                                            //        TA = (Convert.ToDecimal(TotalWages) - Convert.ToDecimal(HRA)).ToString();

                                                            //        EmployeeDays = Convert.ToString(24);

                                                            //    }
                                                            //    else
                                                            //    {
                                                            //        FBasic = "0";
                                                            //        FFHRA = "0";
                                                            //        FFDA = "0";


                                                            //        basic = (Convert.ToDecimal(dt1.Rows[0]["Base"].ToString()) * (Convert.ToDecimal(EmployeeDays) + Convert.ToInt32(cl) + Convert.ToInt32(NFh))).ToString();


                                                            //        basic = (Math.Round(Convert.ToDecimal(basic), 0, MidpointRounding.AwayFromZero)).ToString();

                                                            //        BasicWages = Convert.ToString(Convert.ToDecimal(basic) * Convert.ToDecimal(dtSalDiv.Rows[0]["BasicDA"].ToString()));
                                                            //        BasicWages = Convert.ToString(Convert.ToDecimal(BasicWages) / Convert.ToDecimal(100));
                                                            //        BasicWages = (Math.Round(Convert.ToDecimal(BasicWages), 0, MidpointRounding.AwayFromZero)).ToString();

                                                            //        TotalWages = (Convert.ToDecimal(basic) - Convert.ToDecimal(BasicWages)).ToString();

                                                            //        HRA = (Convert.ToDecimal(TotalWages) / Convert.ToDecimal(2)).ToString();
                                                            //        HRA = (Math.Round(Convert.ToDecimal(HRA), 0, MidpointRounding.AwayFromZero)).ToString();

                                                            //        TA = (Convert.ToDecimal(TotalWages) - Convert.ToDecimal(HRA)).ToString();    
                                                            //    }

                                                            //}
                                                            //else
                                                            //{
                                                            //    if (Convert.ToDecimal(EmployeeDays) > Convert.ToDecimal(26)) // Grater than fixed days 
                                                            //    {
                                                            //        DaysCount = (Convert.ToDecimal(EmployeeDays) - Convert.ToDecimal(26)).ToString(); // Minius employee days to fixed 

                                                            //        FBasic = (Convert.ToDecimal(dt1.Rows[0]["Base"].ToString())).ToString();
                                                            //        FFHRA = (Convert.ToDecimal(FBasic) * Convert.ToDecimal(DaysCount)).ToString();
                                                            //        FFHRA = (Math.Round(Convert.ToDecimal(FFHRA), 0, MidpointRounding.AwayFromZero)).ToString();  // calculate remainning days to salary

                                                            //        FFDA = Convert.ToString(DaysCount);


                                                            //        basic = (Convert.ToDecimal(dt1.Rows[0]["Base"].ToString()) * (Convert.ToDecimal(26) + Convert.ToInt32(cl) + Convert.ToInt32(NFh))).ToString();

                                                            //        basic = (Math.Round(Convert.ToDecimal(basic), 0, MidpointRounding.AwayFromZero)).ToString();

                                                            //        BasicWages = Convert.ToString(Convert.ToDecimal(basic) * Convert.ToDecimal(dtSalDiv.Rows[0]["BasicDA"].ToString()));
                                                            //        BasicWages = Convert.ToString(Convert.ToDecimal(BasicWages) / Convert.ToDecimal(100));
                                                            //        BasicWages = (Math.Round(Convert.ToDecimal(BasicWages), 0, MidpointRounding.AwayFromZero)).ToString();

                                                            //        TotalWages = (Convert.ToDecimal(basic) - Convert.ToDecimal(BasicWages)).ToString();

                                                            //        HRA = (Convert.ToDecimal(TotalWages) / Convert.ToDecimal(2)).ToString();
                                                            //        HRA = (Math.Round(Convert.ToDecimal(HRA), 0, MidpointRounding.AwayFromZero)).ToString();

                                                            //        TA = (Convert.ToDecimal(TotalWages) - Convert.ToDecimal(HRA)).ToString();

                                                            //        EmployeeDays = Convert.ToString(26);

                                                            //    }
                                                            //    else
                                                            //    {
                                                            //        FBasic = "0";
                                                            //        FFHRA = "0";
                                                            //        FFDA = "0";


                                                            //        basic = (Convert.ToDecimal(dt1.Rows[0]["Base"].ToString()) * (Convert.ToDecimal(EmployeeDays) + Convert.ToInt32(cl) + Convert.ToInt32(NFh))).ToString();


                                                            //        basic = (Math.Round(Convert.ToDecimal(basic), 0, MidpointRounding.AwayFromZero)).ToString();

                                                            //        BasicWages = Convert.ToString(Convert.ToDecimal(basic) * Convert.ToDecimal(dtSalDiv.Rows[0]["BasicDA"].ToString()));
                                                            //        BasicWages = Convert.ToString(Convert.ToDecimal(BasicWages) / Convert.ToDecimal(100));
                                                            //        BasicWages = (Math.Round(Convert.ToDecimal(BasicWages), 0, MidpointRounding.AwayFromZero)).ToString();

                                                            //        TotalWages = (Convert.ToDecimal(basic) - Convert.ToDecimal(BasicWages)).ToString();

                                                            //        HRA = (Convert.ToDecimal(TotalWages) / Convert.ToDecimal(2)).ToString();
                                                            //        HRA = (Math.Round(Convert.ToDecimal(HRA), 0, MidpointRounding.AwayFromZero)).ToString();

                                                            //        TA = (Convert.ToDecimal(TotalWages) - Convert.ToDecimal(HRA)).ToString();  
                                                            //    }
                                                            //}

                                                        }
                                                        else 
                                                        {
                                                            //basic = (Convert.ToDecimal(dt1.Rows[0]["Base"].ToString()) * (Convert.ToDecimal(EmployeeDays) + Convert.ToInt32(cl) + Convert.ToInt32(NFh))).ToString();


                                                            //Start 

                                                            EmployeeDays = (Convert.ToDecimal(EmployeeDays) + Convert.ToDecimal(NfhCount) + Convert.ToDecimal(NfhPresent)).ToString();



                                                            //DaysCount = (Convert.ToDecimal(EmployeeDays) - Convert.ToDecimal(24)).ToString(); // Minius employee days to fixed 
                                                            FineAmt_Sal = FineAmt;
                                                            basic = (Convert.ToDecimal(dt1.Rows[0]["Base"].ToString()) - Convert.ToDecimal(dtIncentive.Rows[0]["ShiftII_III"].ToString())).ToString();



                                                            FBasic = (Convert.ToDecimal(basic)).ToString();
                                                            FFHRA = (Convert.ToDecimal(basic) * Convert.ToDecimal(WHPresent)).ToString();
                                                            FFHRA = (Math.Round(Convert.ToDecimal(FFHRA), 0, MidpointRounding.AwayFromZero)).ToString();  // calculate remainning days to salary

                                                            FFDA = Convert.ToString(WHPresent);


                                                            //basic = (Convert.ToDecimal(dt1.Rows[0]["Base"].ToString()) - Convert.ToDecimal(dtIncentive.Rows[0]["ShiftII_III"].ToString())).ToString();
                                                            basic = (Convert.ToDecimal(basic) * (Convert.ToDecimal(EmployeeDays))).ToString();

                                                            basic = (Math.Round(Convert.ToDecimal(basic), 0, MidpointRounding.AwayFromZero)).ToString();

                                                            BasicWages = Convert.ToString(Convert.ToDecimal(basic) * Convert.ToDecimal(dtSalDiv.Rows[0]["BasicDA"].ToString()));
                                                            BasicWages = Convert.ToString(Convert.ToDecimal(BasicWages) / Convert.ToDecimal(100));
                                                            BasicWages = (Math.Round(Convert.ToDecimal(BasicWages), 0, MidpointRounding.AwayFromZero)).ToString();
                                                  
                                                    
                                                            //HRA amount percentage change 12/06/2020
                                                            //TotalWages = (Convert.ToDecimal(basic) - Convert.ToDecimal(BasicWages)).ToString();
                                                            //TotalWages = (Convert.ToDecimal(basic)).ToString();
                                                            //12/06/2020 end

                                                            //HRA = (Convert.ToDecimal(TotalWages) / Convert.ToDecimal(2)).ToString();
                                                            //HRA = (Math.Round(Convert.ToDecimal(HRA), 0, MidpointRounding.AwayFromZero)).ToString();

                                                            //TA = (Convert.ToDecimal(TotalWages) - Convert.ToDecimal(HRA)).ToString();

                                                            HRA = (Convert.ToDecimal(basic) * Convert.ToDecimal(0.2)).ToString();
                                                            HRA = (Math.Round(Convert.ToDecimal(HRA), 0, MidpointRounding.AwayFromZero)).ToString();

                                                            ConveyAllowance = (Convert.ToDecimal(HRA) / Convert.ToDecimal(4)).ToString();
                                                            ConveyAllowance = (Math.Round(Convert.ToDecimal(ConveyAllowance), 0, MidpointRounding.AwayFromZero)).ToString();

                                                            WashingAloowance = (Convert.ToDecimal(HRA) / Convert.ToDecimal(4)).ToString();
                                                            WashingAloowance = (Math.Round(Convert.ToDecimal(WashingAloowance), 0, MidpointRounding.AwayFromZero)).ToString();
                                                            TA = "0.00";

                                                            TotalWages = (Convert.ToDecimal(BasicWages) + Convert.ToDecimal(HRA) + Convert.ToDecimal(ConveyAllowance) + Convert.ToDecimal(WashingAloowance)).ToString();
                                                            //EmployeeDays = (Convert.ToDecimal(EmployeeDays) + Convert.ToDecimal(NfhCount) + Convert.ToDecimal(NfhPresent)).ToString();

                                                            //EmployeeDays = Convert.ToString(24);

                                                            // End




                                                            //if (ddlMonths.SelectedValue == "February")
                                                            //{
                                                            //    if (Convert.ToDecimal(EmployeeDays) > Convert.ToDecimal(24)) // Grater than fixed days 
                                                            //    {
                                                            //        DaysCount = (Convert.ToDecimal(EmployeeDays) - Convert.ToDecimal(24)).ToString(); // Minius employee days to fixed 

                                                            //        basic = (Convert.ToDecimal(dt1.Rows[0]["Base"].ToString()) - Convert.ToDecimal(dtIncentive.Rows[0]["ShiftII_III"].ToString())).ToString();



                                                            //        FBasic = (Convert.ToDecimal(basic)).ToString();
                                                            //        FFHRA = (Convert.ToDecimal(basic) * Convert.ToDecimal(DaysCount)).ToString();
                                                            //        FFHRA = (Math.Round(Convert.ToDecimal(FFHRA), 0, MidpointRounding.AwayFromZero)).ToString();  // calculate remainning days to salary

                                                            //        FFDA = Convert.ToString(DaysCount);


                                                            //        //basic = (Convert.ToDecimal(dt1.Rows[0]["Base"].ToString()) - Convert.ToDecimal(dtIncentive.Rows[0]["ShiftII_III"].ToString())).ToString();
                                                            //        basic = (Convert.ToDecimal(basic) * (Convert.ToDecimal(24) + Convert.ToInt32(cl) + Convert.ToInt32(NFh))).ToString();

                                                            //        basic = (Math.Round(Convert.ToDecimal(basic), 0, MidpointRounding.AwayFromZero)).ToString();

                                                            //        BasicWages = Convert.ToString(Convert.ToDecimal(basic) * Convert.ToDecimal(dtSalDiv.Rows[0]["BasicDA"].ToString()));
                                                            //        BasicWages = Convert.ToString(Convert.ToDecimal(BasicWages) / Convert.ToDecimal(100));
                                                            //        BasicWages = (Math.Round(Convert.ToDecimal(BasicWages), 0, MidpointRounding.AwayFromZero)).ToString();

                                                            //        TotalWages = (Convert.ToDecimal(basic) - Convert.ToDecimal(BasicWages)).ToString();

                                                            //        HRA = (Convert.ToDecimal(TotalWages) / Convert.ToDecimal(2)).ToString();
                                                            //        HRA = (Math.Round(Convert.ToDecimal(HRA), 0, MidpointRounding.AwayFromZero)).ToString();

                                                            //        TA = (Convert.ToDecimal(TotalWages) - Convert.ToDecimal(HRA)).ToString();


                                                            //        EmployeeDays = Convert.ToString(24);

                                                            //    }
                                                            //    else
                                                            //    {
                                                            //        FBasic = "0";
                                                            //        FFHRA = "0";
                                                            //        FFDA = "0";


                                                            //        basic = (Convert.ToDecimal(dt1.Rows[0]["Base"].ToString()) - Convert.ToDecimal(dtIncentive.Rows[0]["ShiftII_III"].ToString())).ToString();
                                                            //        basic = (Convert.ToDecimal(basic) * (Convert.ToDecimal(EmployeeDays) + Convert.ToInt32(cl) + Convert.ToInt32(NFh))).ToString();

                                                            //        basic = (Math.Round(Convert.ToDecimal(basic), 0, MidpointRounding.AwayFromZero)).ToString();

                                                            //        BasicWages = Convert.ToString(Convert.ToDecimal(basic) * Convert.ToDecimal(dtSalDiv.Rows[0]["BasicDA"].ToString()));
                                                            //        BasicWages = Convert.ToString(Convert.ToDecimal(BasicWages) / Convert.ToDecimal(100));
                                                            //        BasicWages = (Math.Round(Convert.ToDecimal(BasicWages), 0, MidpointRounding.AwayFromZero)).ToString();

                                                            //        TotalWages = (Convert.ToDecimal(basic) - Convert.ToDecimal(BasicWages)).ToString();

                                                            //        HRA = (Convert.ToDecimal(TotalWages) / Convert.ToDecimal(2)).ToString();
                                                            //        HRA = (Math.Round(Convert.ToDecimal(HRA), 0, MidpointRounding.AwayFromZero)).ToString();

                                                            //        TA = (Convert.ToDecimal(TotalWages) - Convert.ToDecimal(HRA)).ToString();
                                                            //    }
                                                            //}
                                                            //else
                                                            //{
                                                            //    if (Convert.ToDecimal(EmployeeDays) > Convert.ToDecimal(26)) // Grater than fixed days 
                                                            //    {

                                                            //        DaysCount = (Convert.ToDecimal(EmployeeDays) - Convert.ToDecimal(26)).ToString(); // Minius employee days to fixed 

                                                            //        basic = (Convert.ToDecimal(dt1.Rows[0]["Base"].ToString()) - Convert.ToDecimal(dtIncentive.Rows[0]["ShiftII_III"].ToString())).ToString();



                                                            //        FBasic = (Convert.ToDecimal(basic)).ToString();
                                                            //        FFHRA = (Convert.ToDecimal(basic) * Convert.ToDecimal(DaysCount)).ToString();
                                                            //        FFHRA = (Math.Round(Convert.ToDecimal(FFHRA), 0, MidpointRounding.AwayFromZero)).ToString();  // calculate remainning days to salary

                                                            //        FFDA = Convert.ToString(DaysCount);


                                                            //        //basic = (Convert.ToDecimal(dt1.Rows[0]["Base"].ToString()) - Convert.ToDecimal(dtIncentive.Rows[0]["ShiftII_III"].ToString())).ToString();
                                                            //        basic = (Convert.ToDecimal(basic) * (Convert.ToDecimal(26) + Convert.ToInt32(cl) + Convert.ToInt32(NFh))).ToString();

                                                            //        basic = (Math.Round(Convert.ToDecimal(basic), 0, MidpointRounding.AwayFromZero)).ToString();

                                                            //        BasicWages = Convert.ToString(Convert.ToDecimal(basic) * Convert.ToDecimal(dtSalDiv.Rows[0]["BasicDA"].ToString()));
                                                            //        BasicWages = Convert.ToString(Convert.ToDecimal(BasicWages) / Convert.ToDecimal(100));
                                                            //        BasicWages = (Math.Round(Convert.ToDecimal(BasicWages), 0, MidpointRounding.AwayFromZero)).ToString();

                                                            //        TotalWages = (Convert.ToDecimal(basic) - Convert.ToDecimal(BasicWages)).ToString();

                                                            //        HRA = (Convert.ToDecimal(TotalWages) / Convert.ToDecimal(2)).ToString();
                                                            //        HRA = (Math.Round(Convert.ToDecimal(HRA), 0, MidpointRounding.AwayFromZero)).ToString();

                                                            //        TA = (Convert.ToDecimal(TotalWages) - Convert.ToDecimal(HRA)).ToString();


                                                            //        EmployeeDays = Convert.ToString(24);
                                                            //    }
                                                            //    else
                                                            //    {

                                                            //        FBasic = "0";
                                                            //        FFHRA = "0";
                                                            //        FFDA = "0";

                                                            //        basic = (Convert.ToDecimal(dt1.Rows[0]["Base"].ToString()) - Convert.ToDecimal(dtIncentive.Rows[0]["ShiftII_III"].ToString())).ToString();
                                                            //        basic = (Convert.ToDecimal(basic) * (Convert.ToDecimal(EmployeeDays) + Convert.ToInt32(cl) + Convert.ToInt32(NFh))).ToString();

                                                            //        basic = (Math.Round(Convert.ToDecimal(basic), 0, MidpointRounding.AwayFromZero)).ToString();

                                                            //        BasicWages = Convert.ToString(Convert.ToDecimal(basic) * Convert.ToDecimal(dtSalDiv.Rows[0]["BasicDA"].ToString()));
                                                            //        BasicWages = Convert.ToString(Convert.ToDecimal(BasicWages) / Convert.ToDecimal(100));
                                                            //        BasicWages = (Math.Round(Convert.ToDecimal(BasicWages), 0, MidpointRounding.AwayFromZero)).ToString();

                                                            //        TotalWages = (Convert.ToDecimal(basic) - Convert.ToDecimal(BasicWages)).ToString();

                                                            //        HRA = (Convert.ToDecimal(TotalWages) / Convert.ToDecimal(2)).ToString();
                                                            //        HRA = (Math.Round(Convert.ToDecimal(HRA), 0, MidpointRounding.AwayFromZero)).ToString();

                                                            //        TA = (Convert.ToDecimal(TotalWages) - Convert.ToDecimal(HRA)).ToString();

                                                            //    }
                                                            //}


                                                            //basic = (Convert.ToDecimal(dt1.Rows[0]["Base"].ToString()) - Convert.ToDecimal(dtIncentive.Rows[0]["ShiftII_III"].ToString())).ToString();
                                                            //basic = (Convert.ToDecimal(basic) * (Convert.ToDecimal(EmployeeDays) + Convert.ToInt32(cl) + Convert.ToInt32(NFh))).ToString();

                                                            //basic = (Math.Round(Convert.ToDecimal(basic), 0, MidpointRounding.AwayFromZero)).ToString();

                                                            //BasicWages = Convert.ToString(Convert.ToDecimal(basic) * Convert.ToDecimal(dtSalDiv.Rows[0]["BasicDA"].ToString()));
                                                            //BasicWages = Convert.ToString(Convert.ToDecimal(BasicWages) / Convert.ToDecimal(100));
                                                            //BasicWages = (Math.Round(Convert.ToDecimal(BasicWages), 0, MidpointRounding.AwayFromZero)).ToString();

                                                            //TotalWages = (Convert.ToDecimal(basic) - Convert.ToDecimal(BasicWages)).ToString();

                                                            //HRA = (Convert.ToDecimal(TotalWages) / Convert.ToDecimal(2)).ToString();
                                                            //HRA = (Math.Round(Convert.ToDecimal(HRA), 0, MidpointRounding.AwayFromZero)).ToString();

                                                            //TA = (Convert.ToDecimal(TotalWages) - Convert.ToDecimal(HRA)).ToString();

                                                        }
                                                        AvaibleDasy = "0";
                                                    }
                                                    else if (Employee_Type_Check == "3") // Temp Worker Salary Calc
                                                    {
                                                        if (dt.Rows[i][2].ToString() == "Per0420153173")
                                                        {
                                                            string stop = "";
                                                        }
                                                        if (Convert.ToDateTime(txtFrom.Text) > Convert.ToDateTime(dteligible.Rows[0]["Dateofjoining"].ToString()))
                                                        {
                                                            if ((Convert.ToDecimal(EmployeeDays)+Convert.ToDecimal(WHPresent)+Convert.ToDecimal(NfhPresent)) >= Convert.ToDecimal(dtIncentive.Rows[0]["ShiftI"].ToString()))
                                                            {
                                                                //basic = (Convert.ToDecimal(dt1.Rows[0]["Base"].ToString()) - Convert.ToDecimal(dtIncentive.Rows[0]["ShiftII_III"].ToString())).ToString();

                                                                FineAmt_Sal = FineAmt;
                                                                basic = (Convert.ToDecimal(dt1.Rows[0]["Base"].ToString()) * (Convert.ToDecimal(EmployeeDays) + Convert.ToInt32(cl) + Convert.ToInt32(NFh))).ToString();
                                                                basic = (Math.Round(Convert.ToDecimal(basic), 0, MidpointRounding.AwayFromZero)).ToString();

                                                                BasicWages = "0.00";
                                                                HRA = "0.00";
                                                                TA = "0.00";
                                                                TotalWages = "0.00";
                                                                AvaibleDasy = "0";
                                                            }
                                                            else
                                                            {
                                                                basic = (Convert.ToDecimal(dt1.Rows[0]["Base"].ToString()) - Convert.ToDecimal(dtIncentive.Rows[0]["ShiftII_III"].ToString())).ToString();
                                                                basic = (Convert.ToDecimal(basic) * (Convert.ToDecimal(EmployeeDays) + Convert.ToInt32(cl) + Convert.ToInt32(NFh))).ToString();
                                                                basic = (Math.Round(Convert.ToDecimal(basic), 0, MidpointRounding.AwayFromZero)).ToString();

                                                                BasicWages = "0.00";
                                                                HRA = "0.00";
                                                                TA = "0.00";
                                                                TotalWages = "0.00";
                                                                AvaibleDasy = "0";
                                                            }
                                                        }
                                                        else
                                                        {
                                                            FineAmt_Sal = FineAmt;
                                                            basic = (Convert.ToDecimal(dt1.Rows[0]["Base"].ToString())).ToString();
                                                            basic = (Convert.ToDecimal(basic) * (Convert.ToDecimal(EmployeeDays) + Convert.ToInt32(cl) + Convert.ToInt32(NFh))).ToString();
                                                            basic = (Math.Round(Convert.ToDecimal(basic), 0, MidpointRounding.AwayFromZero)).ToString();

                                                            BasicWages = "0.00";
                                                            HRA = "0.00";
                                                            TA = "0.00";
                                                            TotalWages = "0.00";
                                                            AvaibleDasy = "0";
                                                        }
                                                        TotalWages = basic;
                                                    }
                                                    else if (Employee_Type_Check == "5") // Others Worker Calc
                                                    {
                                                        if ((Convert.ToDecimal(EmployeeDays)+Convert.ToDecimal(WHPresent)+Convert.ToDecimal(NfhPresent)) >= Convert.ToDecimal(dtIncentive.Rows[0]["ShiftI"].ToString()))
                                                        {

                                                            //basic = (Convert.ToDecimal(dt1.Rows[0]["Base"].ToString()) - Convert.ToDecimal(dtIncentive.Rows[0]["ShiftII_III"].ToString())).ToString();
                                                            FineAmt_Sal = FineAmt;
                                                            basic = (Convert.ToDecimal(dt1.Rows[0]["Base"].ToString()) * (Convert.ToDecimal(EmployeeDays) + Convert.ToInt32(cl) + Convert.ToInt32(NFh))).ToString();
                                                            basic = (Math.Round(Convert.ToDecimal(basic), 0, MidpointRounding.AwayFromZero)).ToString();

                                                            BasicWages = "0.00";
                                                            HRA = "0.00";
                                                            TA = "0.00";
                                                            TotalWages = "0.00";
                                                            AvaibleDasy = "0";
                                                        }
                                                        else
                                                        {
                                                            FineAmt_Sal = FineAmt;
                                                            basic = (Convert.ToDecimal(dt1.Rows[0]["Base"].ToString()) - Convert.ToDecimal(dtIncentive.Rows[0]["ShiftII_III"].ToString())).ToString();
                                                            basic = (Convert.ToDecimal(basic) * (Convert.ToDecimal(EmployeeDays) + Convert.ToInt32(cl) + Convert.ToInt32(NFh))).ToString();
                                                            basic = (Math.Round(Convert.ToDecimal(basic), 0, MidpointRounding.AwayFromZero)).ToString();

                                                            BasicWages = "0.00";
                                                            HRA = "0.00";
                                                            TA = "0.00";
                                                            TotalWages = "0.00";
                                                            AvaibleDasy = "0";
                                                        }
                                                        TotalWages = basic;
                                                    }
                                                    else if (Employee_Type_Check == "6")  // Driver Salary
                                                    {
                                                        //Skip   

                                                        FBasic = "0";
                                                        FFHRA = "0";
                                                        FFHRA = "0";  // calculate remainning days to salary

                                                        FFDA = "0";

                                                        FineAmt_Sal = FineAmt;
                                                        basic = (Convert.ToDecimal(dt1.Rows[0]["Base"].ToString())).ToString();

                                                        EmployeeDays = (Convert.ToDecimal(EmployeeDays) + Convert.ToDecimal(NfhPresent) + Convert.ToDecimal(NfhCount) + Convert.ToDecimal(WHPresent)).ToString();


                                                        basic = ( Convert.ToDecimal(basic) * Convert.ToDecimal(EmployeeDays)).ToString();

                                                        basic = (Math.Round(Convert.ToDecimal(basic), 0, MidpointRounding.AwayFromZero)).ToString();

                                                        BasicWages = Convert.ToString(Convert.ToDecimal(basic) * Convert.ToDecimal(dtSalDiv.Rows[0]["BasicDA"].ToString()));
                                                        BasicWages = Convert.ToString(Convert.ToDecimal(BasicWages) / Convert.ToDecimal(100));
                                                        BasicWages = (Math.Round(Convert.ToDecimal(BasicWages), 0, MidpointRounding.AwayFromZero)).ToString();

                                                        //HRA amount percentage change 12/06/2020
                                                        //TotalWages = (Convert.ToDecimal(basic) - Convert.ToDecimal(BasicWages)).ToString();
                                                        //TotalWages = (Convert.ToDecimal(basic)).ToString();
                                                        //12/06/2020 end

                                                        //HRA = (Convert.ToDecimal(TotalWages) / Convert.ToDecimal(2)).ToString();
                                                        //HRA = (Math.Round(Convert.ToDecimal(HRA), 0, MidpointRounding.AwayFromZero)).ToString();

                                                        //TA = (Convert.ToDecimal(TotalWages) - Convert.ToDecimal(HRA)).ToString();
                                                        HRA = (Convert.ToDecimal(basic) * Convert.ToDecimal(0.2)).ToString();
                                                        HRA = (Math.Round(Convert.ToDecimal(HRA), 0, MidpointRounding.AwayFromZero)).ToString();

                                                        ConveyAllowance = (Convert.ToDecimal(HRA) / Convert.ToDecimal(4)).ToString();
                                                        ConveyAllowance = (Math.Round(Convert.ToDecimal(ConveyAllowance), 0, MidpointRounding.AwayFromZero)).ToString();

                                                        WashingAloowance = (Convert.ToDecimal(HRA) / Convert.ToDecimal(4)).ToString();
                                                        WashingAloowance = (Math.Round(Convert.ToDecimal(WashingAloowance), 0, MidpointRounding.AwayFromZero)).ToString();
                                                        TA = "0.00";
                                                        TotalWages = (Convert.ToDecimal(BasicWages) + Convert.ToDecimal(HRA) + Convert.ToDecimal(ConveyAllowance) + Convert.ToDecimal(WashingAloowance)).ToString();
                                                        //EmployeeDays = (Convert.ToDecimal(EmployeeDays) + Convert.ToDecimal(NFh)).ToString();
                                                    }
                                                    else
                                                    {
                                                        //Skip
                                                    }

                                                    if ((Employee_Type_Check == "7"))  // Security Salary Calc
                                                    {
                                                        EmployeeDays = (Convert.ToDecimal(EmployeeDays) + Convert.ToDecimal(NfhCount) + Convert.ToDecimal(NfhPresent)).ToString();

                                                        //DaysCount = (Convert.ToDecimal(EmployeeDays) - Convert.ToDecimal(26)).ToString(); // Minius employee days to fixed 
                                                        FineAmt_Sal = FineAmt;
                                                        FBasic = (Convert.ToDecimal(dt1.Rows[0]["Base"].ToString())).ToString();
                                                        FFHRA = (Convert.ToDecimal(FBasic) * Convert.ToDecimal(WHPresent)).ToString();
                                                        FFHRA = (Math.Round(Convert.ToDecimal(FFHRA), 0, MidpointRounding.AwayFromZero)).ToString();  // calculate remainning days to salary

                                                        FFDA = Convert.ToString(WHPresent);


                                                        basic = (Convert.ToDecimal(dt1.Rows[0]["Base"].ToString()) * (Convert.ToDecimal(EmployeeDays))).ToString();

                                                        basic = (Math.Round(Convert.ToDecimal(basic), 0, MidpointRounding.AwayFromZero)).ToString();

                                                        BasicWages = Convert.ToString(Convert.ToDecimal(basic) * Convert.ToDecimal(dtSalDiv.Rows[0]["BasicDA"].ToString()));
                                                        BasicWages = Convert.ToString(Convert.ToDecimal(BasicWages) / Convert.ToDecimal(100));
                                                        BasicWages = (Math.Round(Convert.ToDecimal(BasicWages), 0, MidpointRounding.AwayFromZero)).ToString();

                                                //HRA amount percentage change 12/06/2020
                                                //TotalWages = (Convert.ToDecimal(basic) - Convert.ToDecimal(BasicWages)).ToString();
                                                //TotalWages = (Convert.ToDecimal(basic)).ToString();
                                                //12/06/2020 end

                                                //HRA = (Convert.ToDecimal(TotalWages) / Convert.ToDecimal(2)).ToString();
                                                //HRA = (Math.Round(Convert.ToDecimal(HRA), 0, MidpointRounding.AwayFromZero)).ToString();

                                                //TA = (Convert.ToDecimal(TotalWages) - Convert.ToDecimal(HRA)).ToString();
                                                HRA = (Convert.ToDecimal(basic) * Convert.ToDecimal(0.2)).ToString();
                                                        HRA = (Math.Round(Convert.ToDecimal(HRA), 0, MidpointRounding.AwayFromZero)).ToString();

                                                        ConveyAllowance = (Convert.ToDecimal(HRA) / Convert.ToDecimal(4)).ToString();
                                                        ConveyAllowance = (Math.Round(Convert.ToDecimal(ConveyAllowance), 0, MidpointRounding.AwayFromZero)).ToString();

                                                        WashingAloowance = (Convert.ToDecimal(HRA) / Convert.ToDecimal(4)).ToString();
                                                        WashingAloowance = (Math.Round(Convert.ToDecimal(WashingAloowance), 0, MidpointRounding.AwayFromZero)).ToString();
                                                        TA = "0.00";
                                                        TotalWages = (Convert.ToDecimal(BasicWages) + Convert.ToDecimal(HRA) + Convert.ToDecimal(ConveyAllowance) + Convert.ToDecimal(WashingAloowance)).ToString();
                                                        //BasicWages = (Convert.ToDecimal(dt1.Rows[0]["Base"].ToString())).ToString();
                                                        //HRA = (Convert.ToDecimal(dt1.Rows[0]["HRA"].ToString())).ToString();
                                                        // TA = (Convert.ToDecimal(dt1.Rows[0]["FDA"].ToString())).ToString();

                                                        //FFTotal = (Convert.ToDecimal(BasicWages) + Convert.ToDecimal(HRA) + Convert.ToDecimal(TA)).ToString();


                                                        //EmployeeDays = (Convert.ToDecimal(NfhCount) + Convert.ToDecimal(EmployeeDays) + Convert.ToDecimal(NfhPresent) + Convert.ToDecimal(WHPresent)).ToString();



                                                        //if (Convert.ToDecimal(EmployeeDays) > Convert.ToDecimal(FixedDays))
                                                        //{
                                                            
                                                        //    DaysCount = (Convert.ToDecimal(EmployeeDays) - Convert.ToDecimal(FixedDays)).ToString();

                                                        //    FineAmt_Sal = FineAmt;
                                                        //    //DaysCount = (Convert.ToDecimal(EmployeeDays) - Convert.ToDecimal(28)).ToString(); // Minius employee days to fixed 

                                                        //    FBasic = (Convert.ToDecimal(FFTotal) / Convert.ToDecimal(FixedDays)).ToString();
                                                        //    FFHRA = (Convert.ToDecimal(FBasic) * (Convert.ToDecimal(DaysCount))).ToString();
                                                        //    FFHRA = (Math.Round(Convert.ToDecimal(FFHRA), 0, MidpointRounding.AwayFromZero)).ToString();  // calculate remainning days to salary

                                                        //    FFDA = Convert.ToString(Convert.ToDecimal(DaysCount));

                                                        //    BasicWages = (Convert.ToDecimal(BasicWages) / Convert.ToDecimal(FixedDays)).ToString();
                                                        //    BasicWages = (Convert.ToDecimal(BasicWages) * Convert.ToDecimal(FixedDays)).ToString();
                                                        //    BasicWages = (Math.Round(Convert.ToDecimal(BasicWages), 0, MidpointRounding.AwayFromZero)).ToString();

                                                        //    HRA = (Convert.ToDecimal(HRA) / Convert.ToDecimal(FixedDays)).ToString();
                                                        //    HRA = (Convert.ToDecimal(HRA) * Convert.ToDecimal(FixedDays)).ToString();
                                                        //    HRA = (Math.Round(Convert.ToDecimal(HRA), 0, MidpointRounding.AwayFromZero)).ToString();

                                                        //    TA = (Convert.ToDecimal(TA) / Convert.ToDecimal(FixedDays)).ToString();
                                                        //    TA = (Convert.ToDecimal(TA) * Convert.ToDecimal(FixedDays)).ToString();
                                                        //    TA = (Math.Round(Convert.ToDecimal(TA), 0, MidpointRounding.AwayFromZero)).ToString();

                                                        //    basic = (Convert.ToDecimal(BasicWages) + Convert.ToDecimal(HRA) + Convert.ToDecimal(TA)).ToString();

                                                        //    EmployeeDays = FixedDays;
                                                        //}
                                                        //else
                                                        //{

                                                        //    FineAmt_Sal = FineAmt;
                                                        //    //DaysCount = (Convert.ToDecimal(EmployeeDays) - Convert.ToDecimal(28)).ToString(); // Minius employee days to fixed 

                                                        //    FBasic = (Convert.ToDecimal(FFTotal) / Convert.ToDecimal(FixedDays)).ToString();
                                                        //    FFHRA = (Convert.ToDecimal(FBasic) * (Convert.ToDecimal(0))).ToString();
                                                        //    FFHRA = (Math.Round(Convert.ToDecimal(FFHRA), 0, MidpointRounding.AwayFromZero)).ToString();  // calculate remainning days to salary

                                                        //    FFDA = Convert.ToString(Convert.ToDecimal(0));

                                                        //    BasicWages = (Convert.ToDecimal(BasicWages) / Convert.ToDecimal(FixedDays)).ToString();
                                                        //    BasicWages = (Convert.ToDecimal(BasicWages) * Convert.ToDecimal(EmployeeDays)).ToString();
                                                        //    BasicWages = (Math.Round(Convert.ToDecimal(BasicWages), 0, MidpointRounding.AwayFromZero)).ToString();

                                                        //    HRA = (Convert.ToDecimal(HRA) / Convert.ToDecimal(FixedDays)).ToString();
                                                        //    HRA = (Convert.ToDecimal(HRA) * Convert.ToDecimal(EmployeeDays)).ToString();
                                                        //    HRA = (Math.Round(Convert.ToDecimal(HRA), 0, MidpointRounding.AwayFromZero)).ToString();

                                                        //    TA = (Convert.ToDecimal(TA) / Convert.ToDecimal(FixedDays)).ToString();
                                                        //    TA = (Convert.ToDecimal(TA) * Convert.ToDecimal(EmployeeDays)).ToString();
                                                        //    TA = (Math.Round(Convert.ToDecimal(TA), 0, MidpointRounding.AwayFromZero)).ToString();

                                                        //    basic = (Convert.ToDecimal(BasicWages) + Convert.ToDecimal(HRA) + Convert.ToDecimal(TA)).ToString();
                                                        
                                                        //}




                                                        //if (ddlMonths.SelectedValue == "February")
                                                        //{

                                                        //    if (Convert.ToDecimal(EmployeeDays) >= Convert.ToDecimal(28)) // Grater than fixed days 
                                                        //    {
                                                        //        FineAmt_Sal = FineAmt;
                                                        //        DaysCount = (Convert.ToDecimal(EmployeeDays) - Convert.ToDecimal(28)).ToString(); // Minius employee days to fixed 

                                                        //        FBasic = (Convert.ToDecimal(FFTotal) / Convert.ToDecimal(28)).ToString();
                                                        //        FFHRA = (Convert.ToDecimal(FBasic) * (Convert.ToDecimal(DaysCount) + Convert.ToDecimal(NFh))).ToString();
                                                        //        FFHRA = (Math.Round(Convert.ToDecimal(FFHRA), 0, MidpointRounding.AwayFromZero)).ToString();  // calculate remainning days to salary

                                                        //        FFDA = Convert.ToString(Convert.ToDecimal(DaysCount) + Convert.ToDecimal(NFh));

                                                        //        BasicWages = (Convert.ToDecimal(BasicWages) / Convert.ToDecimal(28)).ToString();
                                                        //        BasicWages = (Convert.ToDecimal(BasicWages) * Convert.ToDecimal(28)).ToString();
                                                        //        BasicWages = (Math.Round(Convert.ToDecimal(BasicWages), 0, MidpointRounding.AwayFromZero)).ToString();

                                                        //        HRA = (Convert.ToDecimal(HRA) / Convert.ToDecimal(28)).ToString();
                                                        //        HRA = (Convert.ToDecimal(HRA) * Convert.ToDecimal(28)).ToString();
                                                        //        HRA = (Math.Round(Convert.ToDecimal(HRA), 0, MidpointRounding.AwayFromZero)).ToString();

                                                        //        TA = (Convert.ToDecimal(TA) / Convert.ToDecimal(28)).ToString();
                                                        //        TA = (Convert.ToDecimal(TA) * Convert.ToDecimal(28)).ToString();
                                                        //        TA = (Math.Round(Convert.ToDecimal(TA), 0, MidpointRounding.AwayFromZero)).ToString();

                                                        //        basic = (Convert.ToDecimal(BasicWages) + Convert.ToDecimal(HRA) + Convert.ToDecimal(TA)).ToString();


                                                        //        EmployeeDays = Convert.ToString(28);

                                                        //    }
                                                        //    else
                                                        //    {

                                                        //        // DaysCount = (Convert.ToDecimal(EmployeeDays) - Convert.ToDecimal(24)).ToString(); // Minius employee days to fixed 

                                                        //        FineAmt_Sal = FineAmt;
                                                        //        FBasic = (Convert.ToDecimal(FFTotal) / Convert.ToDecimal(28)).ToString();
                                                        //        FFHRA = (Convert.ToDecimal(FBasic) * Convert.ToDecimal(NFh)).ToString();
                                                        //        FFHRA = (Math.Round(Convert.ToDecimal(FFHRA), 0, MidpointRounding.AwayFromZero)).ToString();  // calculate remainning days to salary

                                                        //        FFDA = Convert.ToString(Convert.ToDecimal(NFh));

                                                        //        BasicWages = (Convert.ToDecimal(BasicWages) / Convert.ToDecimal(28)).ToString();
                                                        //        BasicWages = (Convert.ToDecimal(BasicWages) * (Convert.ToDecimal(EmployeeDays))).ToString();
                                                        //        BasicWages = (Math.Round(Convert.ToDecimal(BasicWages), 0, MidpointRounding.AwayFromZero)).ToString();

                                                        //        HRA = (Convert.ToDecimal(HRA) / Convert.ToDecimal(28)).ToString();
                                                        //        HRA = (Convert.ToDecimal(HRA) * (Convert.ToDecimal(EmployeeDays))).ToString();
                                                        //        HRA = (Math.Round(Convert.ToDecimal(HRA), 0, MidpointRounding.AwayFromZero)).ToString();

                                                        //        TA = (Convert.ToDecimal(TA) / Convert.ToDecimal(28)).ToString();
                                                        //        TA = (Convert.ToDecimal(TA) * (Convert.ToDecimal(EmployeeDays))).ToString();
                                                        //        TA = (Math.Round(Convert.ToDecimal(TA), 0, MidpointRounding.AwayFromZero)).ToString();

                                                        //        basic = (Convert.ToDecimal(BasicWages) + Convert.ToDecimal(HRA) + Convert.ToDecimal(TA)).ToString();

                                                        //    }

                                                        //}
                                                        //else
                                                        //{
                                                        //    if (Convert.ToDecimal(EmployeeDays) >= Convert.ToDecimal(30))
                                                        //    {
                                                        //        FineAmt_Sal = FineAmt;

                                                        //        DaysCount = (Convert.ToDecimal(EmployeeDays) - Convert.ToDecimal(30)).ToString(); // Minius employee days to fixed 

                                                        //        FBasic = (Convert.ToDecimal(FFTotal) / Convert.ToDecimal(30)).ToString();
                                                        //        FFHRA = (Convert.ToDecimal(FBasic) * (Convert.ToDecimal(DaysCount) + Convert.ToDecimal(NFh))).ToString();
                                                        //        FFHRA = (Math.Round(Convert.ToDecimal(FFHRA), 0, MidpointRounding.AwayFromZero)).ToString();  // calculate remainning days to salary

                                                        //        FFDA = Convert.ToString(Convert.ToDecimal(DaysCount) + Convert.ToDecimal(NFh));


                                                        //        BasicWages = (Convert.ToDecimal(BasicWages) / Convert.ToDecimal(30)).ToString();
                                                        //        BasicWages = (Convert.ToDecimal(BasicWages) * Convert.ToDecimal(30)).ToString();
                                                        //        BasicWages = (Math.Round(Convert.ToDecimal(BasicWages), 0, MidpointRounding.AwayFromZero)).ToString();

                                                        //        HRA = (Convert.ToDecimal(HRA) / Convert.ToDecimal(30)).ToString();
                                                        //        HRA = (Convert.ToDecimal(HRA) * Convert.ToDecimal(30)).ToString();
                                                        //        HRA = (Math.Round(Convert.ToDecimal(HRA), 0, MidpointRounding.AwayFromZero)).ToString();

                                                        //        TA = (Convert.ToDecimal(TA) / Convert.ToDecimal(30)).ToString();
                                                        //        TA = (Convert.ToDecimal(TA) * Convert.ToDecimal(30)).ToString();
                                                        //        TA = (Math.Round(Convert.ToDecimal(TA), 0, MidpointRounding.AwayFromZero)).ToString();

                                                        //        basic = (Convert.ToDecimal(BasicWages) + Convert.ToDecimal(HRA) + Convert.ToDecimal(TA)).ToString();

                                                        //        EmployeeDays = Convert.ToString(30);
                                                        //    }
                                                        //    else
                                                        //    {
                                                        //        FineAmt_Sal = FineAmt;
                                                        //        FBasic = (Convert.ToDecimal(FFTotal) / Convert.ToDecimal(30)).ToString();
                                                        //        FFHRA = (Convert.ToDecimal(FBasic) * Convert.ToDecimal(NFh)).ToString();
                                                        //        FFHRA = (Math.Round(Convert.ToDecimal(FFHRA), 0, MidpointRounding.AwayFromZero)).ToString();  // calculate remainning days to salary

                                                        //        FFDA = Convert.ToString(Convert.ToDecimal(NFh));


                                                        //        BasicWages = (Convert.ToDecimal(BasicWages) / Convert.ToDecimal(30)).ToString();
                                                        //        BasicWages = (Convert.ToDecimal(BasicWages) * (Convert.ToDecimal(EmployeeDays))).ToString();
                                                        //        BasicWages = (Math.Round(Convert.ToDecimal(BasicWages), 0, MidpointRounding.AwayFromZero)).ToString();

                                                        //        HRA = (Convert.ToDecimal(HRA) / Convert.ToDecimal(30)).ToString();
                                                        //        HRA = (Convert.ToDecimal(HRA) * (Convert.ToDecimal(EmployeeDays))).ToString();
                                                        //        HRA = (Math.Round(Convert.ToDecimal(HRA), 0, MidpointRounding.AwayFromZero)).ToString();

                                                        //        TA = (Convert.ToDecimal(TA) / Convert.ToDecimal(30)).ToString();
                                                        //        TA = (Convert.ToDecimal(TA) * (Convert.ToDecimal(EmployeeDays))).ToString();
                                                        //        TA = (Math.Round(Convert.ToDecimal(TA), 0, MidpointRounding.AwayFromZero)).ToString();

                                                        //        basic = (Convert.ToDecimal(BasicWages) + Convert.ToDecimal(HRA) + Convert.ToDecimal(TA)).ToString();
                                                        //    }

                                                        //}

                                                    }
                                                    else
                                                    { 
                                                    //Skip


                                                    }

                                    

                                                    //Incentive Add
                                                    //Get Employee Type


                                                    string Employee_Total_Days = "";
                                                    Worker_11Days_Incentive = "0.00";
                                                    Worker_12Days_Incentive = "0.00";


                                                    //lop = ((Convert.ToDecimal(basic) / 30) * Convert.ToDecimal(lopdays)).ToString();
                                                    lop = "0.00";
                                                    //txtSLOP.Text = (Math.Round(Convert.ToDecimal(txtSLOP.Text), 0, MidpointRounding.AwayFromZero)).ToString();

                                                    if (dteligible.Rows[0]["EligiblePF"].ToString() == "1")
                                                    {

                                                        if ((Employee_Type_Check == "2") || (Employee_Type_Check == "6") || (Employee_Type_Check == "7"))
                                                        {
                                                            PF_salary = "0";
                                                            pfAmt = (Convert.ToDecimal(BasicWages)).ToString();
                                                            pfAmt = (Convert.ToDecimal(Convert.ToDecimal(pfAmt) * Convert.ToDecimal(dtpf.Rows[0]["PF_per"].ToString()))).ToString();
                                                            pfAmt = (Convert.ToDecimal(Convert.ToDecimal(pfAmt) / 100)).ToString();
                                                            pfAmt = (Math.Round(Convert.ToDecimal(pfAmt), 0, MidpointRounding.AwayFromZero)).ToString();
                                                            PFS = pfAmt;
                                                        }
                                                        else
                                                        {
                                                            PF_salary = "0";
                                                            pfAmt = (Convert.ToDecimal(basic)).ToString();
                                                            //pfAmt = (Convert.ToDecimal(pfAmt) + (Convert.ToDecimal(FDA))).ToString();
                                                            pfAmt = (Convert.ToDecimal(Convert.ToDecimal(pfAmt)) * Convert.ToDecimal(dtpf.Rows[0]["PF_per"].ToString())).ToString();
                                                            pfAmt = (Convert.ToDecimal(Convert.ToDecimal(pfAmt) / 100)).ToString();
                                                            pfAmt = (Math.Round(Convert.ToDecimal(pfAmt), 0, MidpointRounding.AwayFromZero)).ToString();
                                                            PFS = pfAmt;
                                                        }

                                                    }
                                                    else
                                                    {
                                                        PFS = "0";
                                                        pfAmt = "0";
                                                    }

                                                    HalfNight_Amt = "0.00";
                                                    FullNight_Amt = "0.00";
                                                    ThreeSided_Amt = "0.00";
                                                    DayIncentive = "0.00";
                                                    SpinningAmt = "0.00";

                                                    if (dteligible.Rows[0]["ElgibleESI"].ToString() == "1")
                                                    {
                                                        ESI = (Convert.ToDecimal(TotalWages)).ToString();
                                                        ESI = (Convert.ToDecimal(Convert.ToDecimal(ESI) * Convert.ToDecimal(dtpf.Rows[0]["ESI_per"].ToString()))).ToString();
                                                        ESI = (Convert.ToDecimal(Convert.ToDecimal(ESI) / 100)).ToString();


                                                        //ESI = ((ESI_val * Convert.ToDecimal(1.75)) / 100).ToString();
                                                        ESI = (Math.Round(Convert.ToDecimal(ESI), 2, MidpointRounding.ToEven)).ToString();
                                                        string[] split_val = ESI.Split('.');
                                                        string Ist = split_val[0].ToString();
                                                        string IInd = split_val[1].ToString();

                                                        if (Convert.ToInt32(IInd) > 0)
                                                        {
                                                            ESI = (Convert.ToInt32(Ist) + 1).ToString();
                                                        }
                                                        else
                                                        {
                                                            ESI = Ist;
                                                        }

                                                    }
                                                    else
                                                    {
                                                        ESI = "0";
                                                    }
                                                    stamp = "0";
                                                    string dt_ot1 = "";

                                                

                                                    if ((dteligible.Rows[0]["Eligibleforovertime"].ToString()).Trim() == "1")
                                                    {
                                                        //Total_OT = (Convert.ToDecimal(OT_Hours) + Convert.ToDecimal(OT_Days)).ToString();
                                                        //OTDays = (Convert.ToDecimal(dt1.Rows[0]["Base"].ToString())).ToString();
                                                        //OTDays = (Convert.ToDecimal(OTDays) * Convert.ToDecimal(ThreeSided)).ToString();

                                                    }
                                                    else
                                                    {
                                                        OTDays = "0";
                                                    }

                                                    //SpinningAmt = Worker_12Days_Incentive;
                                                    //DayIncentive = Worker_11Days_Incentive;

                                                    TotalEarnings = (Convert.ToDecimal(TotalWages) + Convert.ToDecimal(VDA) + Convert.ToDecimal(FDA)  + Convert.ToDecimal(All1) + Convert.ToDecimal(All2) + Convert.ToDecimal(All3) + Convert.ToDecimal(Al4) + Convert.ToDecimal(OTDays) + Convert.ToDecimal(All5) + Convert.ToDecimal(HalfNight_Amt) + Convert.ToDecimal(FullNight_Amt) + Convert.ToDecimal(SpinningAmt) + Convert.ToDecimal(ThreeSided_Amt) + Convert.ToDecimal(DayIncentive)).ToString();
                                                    TotalEarnings = (Math.Round(Convert.ToDecimal(TotalEarnings), 2, MidpointRounding.ToEven)).ToString();
                                                    TotalDeductions = (Convert.ToDecimal(pfAmt) + Convert.ToDecimal(ESI) + Convert.ToDecimal(Advance) + Convert.ToDecimal(stamp) + Convert.ToDecimal(lop) + Convert.ToDecimal(Ded1) + Convert.ToDecimal(ded2) + Convert.ToDecimal(ded3) + Convert.ToDecimal(ded4) + Convert.ToDecimal(ded5) + Convert.ToDecimal(HostelDeduction) + Convert.ToDecimal(FineAmt_Sal)).ToString();
                                                    TotalDeductions = (Math.Round(Convert.ToDecimal(TotalDeductions), 2, MidpointRounding.ToEven)).ToString();
                                                    NetPay = (Convert.ToDecimal(TotalEarnings) - Convert.ToDecimal(TotalDeductions)).ToString();
                                                    NetPay = (Math.Round(Convert.ToDecimal(NetPay), 0, MidpointRounding.AwayFromZero)).ToString();
                                                    if (Convert.ToDecimal(NetPay) < 0)
                                                    {
                                                        NetPay = "0";
                                                    }
                                                    Words = NumerictoNumber(Convert.ToInt32(NetPay), isUK).ToString() + " " + "Only";

                                                }
                                            }
                                        }
                                if (!ErrVerify)
                                {
                                    if (EmpNo == "CON062013645")
                                    {
                                    }
                                    if (EmpNo == "Gen0520142472")
                                    {
                                        string VAl = "";
                                    }
                                    string pfEligible = objdata.PF_Eligible_Salary(EmpNo, SessionCcode, SessionLcode);
                                    DateTime MyDate = new DateTime();
                                    objSal.TotalWorkingDays = Work;
                                    objSal.NFh = NFh;
                                    objSal.Lcode = SessionLcode;
                                    objSal.Ccode = SessionCcode;
                                    objSal.ApplyLeaveDays = lopdays;
                                    objSal.GrossEarnings = TotalEarnings;
                                    objSal.TotalDeduction = TotalDeductions;
                                    objSal.TotalBasic = TotalWages;
                                    objSal.NetPay = NetPay;
                                    objSal.Words = Words;
                                    objSal.AvailableDays = AvaibleDasy;
                                    objSal.SalaryDate = txtsalaryday.Text;
                                    objSal.work = EmployeeDays;
                                    objSal.weekoff = WeekOff;
                                    objSal.CL = cl;
                                    objSal.FixedBase = FBasic;
                                    objSal.FixedFDA = FFDA;
                                    objSal.FixedFRA = FFHRA;
                                    objSal.HomeTown = Home;
                                    objSal.HalfNightAmt = HalfNight_Amt;
                                    objSal.FullNightAmt = FullNight_Amt;
                                    objSal.ThreesidedAmt = ThreeSided_Amt;
                                    objSal.SpinningAmt = SpinningAmt;
                                    objSal.DayIncentive = DayIncentive;
                                    MyDate = DateTime.ParseExact(objSal.SalaryDate, "dd-MM-yyyy", null);
                                    //if (EmpType == "1")
                                    //{
                                    objSal.BasicAndDA = BasicWages;
                                    objSal.HRA = HRA;
                                    objSal.FDA = TA;
                                    objSal.Conveyance = ConveyAllowance;
                                    objSal.Washing = WashingAloowance;
                                  
                                    objSal.VDA = VDA;
                                    //objSal.OT = "0";
                                    objSal.Allowances1 = All1;
                                    objSal.Allowances2 = All2;
                                    objSal.Allowances3 = All3;
                                    objSal.Allowances4 = Al4;
                                    objSal.Allowances5 = All5;
                                    objSal.Deduction1 = Ded1;
                                    objSal.Deduction2 = ded2;
                                    objSal.Deduction3 = ded3;
                                    objSal.Deduction4 = ded4;
                                    objSal.Deduction5 = ded5;
                                    objSal.ProvidentFund = pfAmt;
                                    objSal.WithPay = withpay;
                                    objSal.ESI = ESI;
                                    objSal.Stamp = "0";
                                    objSal.Advance = Advance;
                                    objSal.LossofPay = lop;
                                    objSal.OT = OTDays;
                                    objSal.HostelDeduction = HostelDeduction;

                                    objSal.BonusDays = BonusDays;
                                    objSal.NfhCount = NfhCount;
                                    objSal.NfhPresent = NfhPresent;
                                    objSal.FineAmt = FineAmt_Sal;
                                    objSal.WHPresent = WHPresent;


                                    if (pfEligible == "1")
                                    {
                                        objSal.PfSalary = PFS;
                                        objSal.Emp_PF = PFS;
                                    }
                                    else
                                    {
                                        objSal.PfSalary = "0.00";
                                        objSal.Emp_PF = "0.00";
                                    }
                                    PassFlag = true;
                                }
                                if (PassFlag == true)
                                {
                                    if (Updateval == "0")
                                    {
                                    }
                                    else if (Updateval == "1")
                                    {
                                        string Del = "Delete from SalaryDetails where EmpNo='" + EmpNo + "' and " +
                                                 "Month='" + ddlMonths.Text + "' and FinancialYear='" + ddlfinance.SelectedValue + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";

                                                // " and Process_Mode='0' and convert(datetime,FromDate,105) = convert(datetime,'" + txtFrom.Text + "',105) and convert(datetime, ToDate, 105) = convert(datetime, '" + txtTo.Text + "', 105)";
                                        //cn.Open();
                                        SqlCommand cmd_verify1 = new SqlCommand(Del, cn);
                                        cmd_verify1.ExecuteNonQuery();
                                        MyDate = DateTime.ParseExact(objSal.SalaryDate, "dd-MM-yyyy", null);

                                        TempDate = Convert.ToDateTime(txtsalaryday.Text).AddMonths(0).ToShortDateString();
                                        TransDate = DateTime.ParseExact(TempDate, "dd/MM/yyyy", null);
                                        objdata.SalaryDetails(objSal, EmpNo, ExistNo, MyDate, MyMonth, Year, ddlfinance.SelectedValue, TransDate, MyDate1, MyDate2);
                                    }
                                }
                                
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Finished Success Fully');", true);
                                //sSourceConnection.Close();
                                cn.Close();
                            }

                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            Response.Write("<b> Error messsage: </b><br />" + ex.Message);
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Please Upload Correct File Format...');", true);
            //System.Windows.Forms.MessageBox.Show("Please Upload Correct File Format", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
        }
    }
    protected void btnEmp_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptEmpDownload.aspx");
    }
    protected void btnatt_Click(object sender, EventArgs e)
    {
        Response.Redirect("AttenanceDownload.aspx");
    }
    protected void btnLeave_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptLeaveSample.aspx");
    }
    protected void btnOT_Click(object sender, EventArgs e)
    {
        Response.Redirect("OTDownload.aspx");
    }
    protected void btncontract_Click(object sender, EventArgs e)
    {
        Response.Redirect("ContractRPT.aspx");
    }
    protected void rbsalary_SelectedIndexChanged(object sender, EventArgs e)
    {
        //if (rbsalary.SelectedValue == "1")
        //{
        //    PanelMonthly.Visible = true;
        //    panelWeekly.Visible = true;
        //    panelBimonth.Visible = false;

        //}
        //else if (rbsalary.SelectedValue == "2")
        //{
        //    PanelMonthly.Visible = true;
        //    panelWeekly.Visible = false;
        //    panelBimonth.Visible = false;
        //}
        //else if (rbsalary.SelectedValue == "3")
        //{
        //    PanelMonthly.Visible = true;
        //    panelWeekly.Visible = false;
        //    panelBimonth.Visible = true;
        //}
    }
    protected void txtFrom_TextChanged(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        txtTo.Text = null;
        int val_Month = Convert.ToDateTime(txtFrom.Text).Month;
        string Mont = "";
        switch (val_Month)
        {
            case 1:
                Mont = "January";
                break;

            case 2:
                Mont = "February";
                break;
            case 3:
                Mont = "March";
                break;
            case 4:
                Mont = "April";
                break;
            case 5:
                Mont = "May";
                break;
            case 6:
                Mont = "June";
                break;
            case 7:
                Mont = "July";
                break;
            case 8:
                Mont = "August";
                break;
            case 9:
                Mont = "September";
                break;
            case 10:
                Mont = "October";
                break;
            case 11:
                Mont = "November";
                break;
            case 12:
                Mont = "December";
                break;
            default:
                break;
        }
        if (Mont != ddlMonths.SelectedValue)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Date Properly...');", true);
            ErrFlag = true;
            txtFrom.Text = null;
        }
    }
    protected void txtTo_TextChanged(object sender, EventArgs e)
    {
        try
        {
            bool ErrFlag = false;
            if (txtFrom.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the From Date...');", true);
                ErrFlag = true;
            }

            if (!ErrFlag)
            {
                DateTime dfrom = Convert.ToDateTime(txtFrom.Text);
                DateTime dtto = Convert.ToDateTime(txtTo.Text);
                if (dfrom >= dtto)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Date Properly...');", true);
                    ErrFlag = true;
                    txtTo.Text = null;
                }
                
            }
        }
        catch (Exception ex)
        {
        }
    }
    public void DropDwonCategory()
    {
        DataTable dtcate = new DataTable();
        dtcate = objdata.DropDownCategory();
        ddlcategory.DataSource = dtcate;
        ddlcategory.DataTextField = "CategoryName";
        ddlcategory.DataValueField = "CategoryCd";
        ddlcategory.DataBind();
    }
    protected void ddlcategory_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void btnSal_Click(object sender, EventArgs e)
    {
        Response.Redirect("FrmDeductionDownload.aspx");
    }
}

