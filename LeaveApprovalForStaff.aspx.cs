﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class LeaveApprovalForStaff : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public void FillApplyLeaveForGridview()
    {
        bool ErrFlag = false;
        if (txtleavecd.Text == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Leave Code...');", true);
            ErrFlag = true;
        }
        if(!ErrFlag)
        {
            DataTable dt = new DataTable();
            dt = objdata.FillApplyForGridViewForLeaveCode(txtleavecd.Text);
            if (dt.Rows.Count > 0)
            {
                GvLeaveApprovalStaff.DataSource = dt;
                GvLeaveApprovalStaff.DataBind();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Valid Leave Code...');", true);
            }
        }
    }
    protected void btnsearch_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        if (txtleavecd.Text == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Leave Code...');", true);
            ErrFlag = true;
        }
        if (!ErrFlag)
        {
            DataTable dt = new DataTable();
            dt = objdata.FillApplyForGridViewForLeaveCode(txtleavecd.Text);
            if (dt.Rows.Count > 0)
            {
                GvLeaveApprovalStaff.DataSource = dt;
                GvLeaveApprovalStaff.DataBind();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Valid Leave Code...');", true);
                GvLeaveApprovalStaff.DataSource = null;
                GvLeaveApprovalStaff.DataBind();
            }
        }
    }
    protected void btnapproval_Click(object sender, EventArgs e)
    {
        for (int i = 0; i < GvLeaveApprovalStaff.Rows.Count; i++)
        {
            CheckBox check_box = (CheckBox)GvLeaveApprovalStaff.Rows[i].FindControl("chkclear");
            if (check_box != null)
            {
                if (check_box.Checked)
                {
                    Label lblgvempno = (Label)GvLeaveApprovalStaff.Rows[i].FindControl("lblempno");
                    Label lblgvleavecode = (Label)GvLeaveApprovalStaff.Rows[i].FindControl("lblleavecode");
                    string sid = lblgvempno.Text;
                   // string Status = "Y";
                    string Leavecd = lblgvleavecode.Text;
                   // string Noofleave = lblgvnoofleave.Text;
                  //  objdata.UpdateForApplyLeave(sid, Status, Leavecd, Noofleave);
                  //  objdata.UpdateForApplyLeave(sid, Status, Leavecd);
                  //  FillApplyLeaveForGridview();
                }
            }
        }

        //ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Leave Approval Update Successfully...');", true);

    
    }
   
}
