﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Data.SqlClient;
using System.Collections.Specialized;
using Payroll;
using Payroll.Configuration;
using Payroll.Data;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class Administrator_1 : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    Administratorclass AD = new Administratorclass();
    UserRegistrationClass UR = new UserRegistrationClass();
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    static string lblprobation_Common;
    static string del_company;
    static string del_Loc;
    string SesRoleCode;
    string UserID;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        string ss = Session["UserId"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        UserID = Session["UserId"].ToString();
        SesRoleCode = Session["RoleCode"].ToString();
        lblComany.Text = SessionCcode + " - " + SessionLcode;
        lblusername.Text = Session["Usernmdisplay"].ToString();
        if (!IsPostBack)
        {
            //District();
            loadgrid();
        }
    }
    public void clr()
    {
        txtcode.Text = "";
        txtname.Text = "";
        txtlocCode.Text = "";
        txtlocName.Text = "";
        txtAddress1.Text = "";
        txtAddress2.Text = "";
        txtstate.Text = "";
        txtpin.Text = "";
        txtPhone.Text = "";
        txtemail.Text = "";
        txtEstablishment.Text = "";
        //District();
        //DataTable dt = new DataTable();
        //ddltaluk.DataSource = dt;
        //ddltaluk.DataBind();
        txtcode.Enabled = true;
        txtlocCode.Enabled = true;
        loadgrid();
    }
    public void loadgrid()
    {
        DataTable dtempty = new DataTable();
        DataTable dt = new DataTable();
        gvCompany.DataSource = dtempty;
        gvCompany.DataBind();
        dt = objdata.AdminRights_load();
        gvCompany.DataSource = dt;
        gvCompany.DataBind();
    }
    //public void District()
    //{


    //    DataTable dt1 = new DataTable();
    //    dt1 = objdata.DropDownDistrict();
    //    ddlDistrict.DataSource = dt1;
    //    ddlDistrict.DataTextField = "DistrictNm";
    //    ddlDistrict.DataValueField = "DistrictCd";
    //    ddlDistrict.DataBind();
    //}
    //protected void ddlDistrict_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    DataTable dttaluk = new DataTable();
    //    dttaluk = objdata.DropDownPSTaluk(ddlDistrict.SelectedValue);
    //    ddltaluk.DataSource = dttaluk;
    //    ddltaluk.DataTextField = "TalukNm";
    //    ddltaluk.DataValueField = "TalukCd";
    //    ddltaluk.DataBind();
    //}
    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            bool ErrFlag = false;
            bool SaveFlag = false;
            bool UpdateFlag = false;
            if (txtcode.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Company Code...!');", true);
                ErrFlag = true;
            }
            else if (txtname.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Companyh Name ...!');", true);
                ErrFlag = true;
            }
            else if (txtlocCode.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Location Code...!');", true);
                ErrFlag = true;
            }
            else if (txtlocName.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Location Name...!');", true);
                ErrFlag = true;
            }
            else if (txtAddress1.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Address 1...!');", true);
                ErrFlag = true;
            }
            //else if (ddlDistrict.SelectedValue == "")
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the District...!');", true);
            //    ErrFlag = true;
            //}
            //else if (ddlDistrict.SelectedValue == "0")
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the District Properly...!');", true);
            //    ErrFlag = true;
            //}
            //else if (ddltaluk.SelectedValue == "")
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Taluk...!');", true);
            //    ErrFlag = true;
            //}
            //else if (ddltaluk.SelectedValue == "0")
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Taluk Properly...!');", true);
            //    ErrFlag = true;
            //}
            else if (txtpin.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Pincode...!');", true);
                ErrFlag = true;
            }
            else if (txtstate.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the State...!');", true);
                ErrFlag = true;
            }
            if (!ErrFlag)
            {
                if ((txtcode.Enabled == false) && (txtlocCode.Enabled == false))
                {
                    AD.Ccode = txtcode.Text.Trim();
                    AD.Cname = txtname.Text.Trim();
                    AD.Lcode = txtlocCode.Text.Trim();
                    AD.Location = txtlocName.Text.Trim();
                    AD.Address1 = txtAddress1.Text.Trim();
                    AD.Address2 = txtAddress2.Text.Trim();

                    AD.Pincode = txtpin.Text.Trim();
                    AD.Phone = txtPhone.Text.Trim();
                    AD.state = txtstate.Text.Trim();
                    AD.Email = txtemail.Text.Trim();
                    AD.Establishment = txtEstablishment.Text.Trim();
                    objdata.AdminRights_Update(AD);
                    UpdateFlag = true;
                    clr();
                }
                else
                {
                    string Comp_Code = objdata.CompnyCode_verify(txtcode.Text.Trim());
                    string Loc_code = objdata.LocCode_verify(txtlocCode.Text.Trim());

                    if ((txtlocCode.Text.Trim().ToLower() == Loc_code.Trim().ToLower()) && (txtcode.Text.Trim().ToLower() == Comp_Code.Trim().ToLower()))
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Company Code and Location Code already Exist...!');", true);
                        ErrFlag = true;
                    }

                    if (!ErrFlag)
                    {
                        AD.Ccode = txtcode.Text.Trim();
                        AD.Cname = txtname.Text.Trim();
                        AD.Lcode = txtlocCode.Text.Trim();
                        AD.Location = txtlocName.Text.Trim();
                        AD.Address1 = txtAddress1.Text.Trim();
                        AD.Address2 = txtAddress2.Text.Trim();

                        AD.Pincode = txtpin.Text.Trim();
                        AD.Phone = txtPhone.Text.Trim();
                        AD.state = txtstate.Text.Trim();
                        AD.Email = txtemail.Text.Trim();
                        AD.Establishment = txtEstablishment.Text.Trim();
                        objdata.AdminRights_Insert(AD);
                        //UR.Ccode = txtcode.Text.Trim();
                        //UR.Lcode = txtname.Text.Trim();
                        //UR.UserCode = txtuser.Text.Trim();
                        UR.IsAdmin = "1";

                        SaveFlag = true;
                        clr();
                    }
                }

            }
            if (SaveFlag == true)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Saved Successfully...!');", true);
            }
            if (UpdateFlag == true)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Updated Successfully...!');", true);
            }
        }
        catch (Exception ex)
        {
        }
    }
    protected void btncancel_Click(object sender, EventArgs e)
    {
        clr();
    }
    protected void gvCompany_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        string constr = ConfigurationManager.AppSettings["ConnectionString"];
        Label lblcompany = (Label)gvCompany.Rows[e.RowIndex].FindControl("lblGcode");
        Label LocCode = (Label)gvCompany.Rows[e.RowIndex].FindControl("lblLoccode");
        del_company = lblcompany.Text.Trim();
        del_Loc = LocCode.Text.Trim();
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "script", "showConfirm();", true);
    }
    protected void cmdOverriteGrade_Click(object sender, EventArgs e)
    {
        try
        {
            string constr = ConfigurationManager.AppSettings["ConnectionString"];
            //Label lblbank = (Label)gvBank.Rows[e.RowIndex].FindControl("lblBankcd");
            SqlConnection con = new SqlConnection(constr);
            //System.Windows.Forms.DialogResult dres = System.Windows.Forms.MessageBox.Show("Are you Sure want to Delete ?", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.YesNo, System.Windows.Forms.MessageBoxIcon.Warning);
            //if (dres.ToString() == "Yes")

            string qry1 = "Delete from AdminRights where Ccode='" + del_company + "' and LCode='" + del_Loc + "'";
            SqlCommand cmd1 = new SqlCommand(qry1, con);
            con.Open();
            cmd1.ExecuteNonQuery();
            con.Close();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Deleted the Record....! ');", true);
            //System.Windows.Forms.MessageBox.Show("Deleted The Record", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            loadgrid();

        }
        catch (Exception ex)
        {
        }
    }
    protected void gvCompany_RowEditing(object sender, GridViewEditEventArgs e)
    {
        Label lbledit = (Label)gvCompany.Rows[e.NewEditIndex].FindControl("lblGcode");
        DataTable dt = new DataTable();
        dt = objdata.Admin_Retrive(lbledit.Text);
        if (dt.Rows.Count > 0)
        {
            txtcode.Enabled = false;
            txtlocCode.Enabled = false;
            txtcode.Text = dt.Rows[0]["Ccode"].ToString();
            txtname.Text = dt.Rows[0]["Cname"].ToString();
            txtlocCode.Text = dt.Rows[0]["LCode"].ToString();
            txtlocName.Text = dt.Rows[0]["Location"].ToString();
            txtAddress1.Text = dt.Rows[0]["Address1"].ToString();
            txtAddress2.Text = dt.Rows[0]["Address2"].ToString();
            txtstate.Text = dt.Rows[0]["State"].ToString();
            txtpin.Text = dt.Rows[0]["Pincode"].ToString();
            txtPhone.Text = dt.Rows[0]["Phone"].ToString();
            txtemail.Text = dt.Rows[0]["Email"].ToString();
            txtEstablishment.Text = dt.Rows[0]["Establishmnet"].ToString();
            //ddlDistrict.SelectedValue = dt.Rows[0]["District"].ToString();
            string TalukCd = dt.Rows[0]["Taluk"].ToString();
            string Districtcd = dt.Rows[0]["District"].ToString();
            //int intTaluckCd = Convert.ToInt32(TalukCd);
            //int intDistrict = Convert.ToInt32(Districtcd);
            //ddltaluk.SelectedValue = TalukCd;
            //DataTable dtTlDis = new DataTable();
            //dtTlDis = objdata.DropDownPSTaluk_ForEdit(intDistrict, intTaluckCd);
            //ddltaluk.DataSource = dtTlDis;
            //ddltaluk.DataTextField = "TalukNm";
            //ddltaluk.DataValueField = "TalukCd";
            //ddltaluk.DataBind();
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('No Company Found...!');", true);
            clr();
        }
    }
    protected void btnEmp_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptEmpDownload.aspx");
    }
    protected void btnatt_Click(object sender, EventArgs e)
    {
        Response.Redirect("AttenanceDownload.aspx");
    }
    protected void btnLeave_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptLeaveSample.aspx");
    }
    protected void btnOT_Click(object sender, EventArgs e)
    {
        Response.Redirect("OTDownload.aspx");
    }
    protected void btncontract_Click(object sender, EventArgs e)
    {
        Response.Redirect("ContractRPT.aspx");
    }
    protected void btnSal_Click(object sender, EventArgs e)
    {
        Response.Redirect("FrmDeductionDownload.aspx");
    }
}
