﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;
using Payroll;
using Payroll.Data;
using Payroll.Configuration;
using Altius.BusinessAccessLayer.BALDataAccess;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html;
using iTextSharp.text.html.simpleparser;

public partial class RptProbationperiod_1 : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    string temp;
    string Stafflabour;
    bool searchflag = false;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionAdmin = Session["Isadmin"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        string ss = Session["UserId"].ToString();
        if (!IsPostBack)
        {            
            DropDwonCategory();
        }
        lblComany.Text = SessionCcode + " - " + SessionLcode;
        lblusername.Text = Session["Usernmdisplay"].ToString();
    }
    public void DropDwonCategory()
    {
        DataTable dtcate = new DataTable();
        dtcate = objdata.DropDownCategory();
        ddlCategory.DataSource = dtcate;
        ddlCategory.DataTextField = "CategoryName";
        ddlCategory.DataValueField = "CategoryCd";
        ddlCategory.DataBind();
    }
    protected void btSearch_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        DataTable dtempty = new DataTable();
        gridprobation.DataSource = dtempty;
        gridprobation.DataBind();
        if (ddlCategory.SelectedValue == "0")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select Employee Category');", true);
            //System.Windows.Forms.MessageBox.Show("Select Employee Category", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            ErrFlag = true;
        }
        else if (ddldepartment.SelectedValue == "0")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Department');", true);
            //System.Windows.Forms.MessageBox.Show("Select the Department", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            ErrFlag = true;
        }
        else if ((txtempNo.Text == "") && (txtexist.Text == ""))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Employee Number or Existing number');", true);
            //System.Windows.Forms.MessageBox.Show("Enter the Employee Number or Existing number", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            ErrFlag = true;
        }

        if (!ErrFlag)
        {
            searchflag = true;

            DataTable dt = new DataTable();
            if (ddlCategory.SelectedValue == "1")
            {
                Stafflabour = "S";
            }
            else if (ddlCategory.SelectedValue == "2")
            {
                Stafflabour = "L";
            }
            if (txtempNo.Text != "")
            {
                if (SessionAdmin == "1")
                {
                    dt = objdata.Probationperiodsearch(ddldepartment.SelectedValue, txtempNo.Text, Stafflabour, SessionCcode, SessionLcode);
                }
                else
                {
                    dt = objdata.Probationperiodsearch_user(ddldepartment.SelectedValue, txtempNo.Text, Stafflabour, SessionCcode, SessionLcode);
                }
            }
            else if (txtexist.Text != "")
            {
                if (SessionAdmin == "1")
                {
                    dt = objdata.Probationperiodsearch_exist(ddldepartment.SelectedValue, txtexist.Text, Stafflabour, SessionCcode, SessionLcode);
                }
                else
                {
                    dt = objdata.Probationperiodsearch_exist_user(ddldepartment.SelectedValue, txtexist.Text, Stafflabour, SessionCcode, SessionLcode);
                }
            }
            if (dt.Rows.Count > 0)
            {
                gridprobation.DataSource = dt;
                gridprobation.DataBind();
                txtexist.Text = dt.Rows[0]["ExisistingCode"].ToString();
                txtempNo.Text = dt.Rows[0]["EmpNo"].ToString();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Employee Not Found');", true);
                //System.Windows.Forms.MessageBox.Show("Employee Not Found", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            }
        }
        searchflag = false;
    }
    protected void Department_SelectedIndexchanged(object sender, EventArgs e)
    {
        bool ErrFlag = false;

        try
        {
            DataTable dtempty = new DataTable();
            gridprobation.DataSource = dtempty;
            gridprobation.DataBind();
            if (ddlCategory.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select Employee Category');", true);
                //System.Windows.Forms.MessageBox.Show("Select Employee Category", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            if (!ErrFlag)
            {
                DataTable dt = new DataTable();
                if (ddlCategory.SelectedValue == "1")
                {
                    string Staff = "S";
                    if (SessionAdmin == "1")
                    {
                        dt = objdata.Probationperiod(ddldepartment.SelectedValue, Staff, SessionCcode, SessionLcode);
                    }
                    else
                    {
                        dt = objdata.Probationperiod_user(ddldepartment.SelectedValue, Staff, SessionCcode, SessionLcode);
                    }
                    gridprobation.DataSource = dt;
                    gridprobation.DataBind();
                }
                else if (ddlCategory.SelectedValue == "2")
                {
                    string Labour = "L";
                    if (SessionAdmin == "1")
                    {
                        dt = objdata.Probationperiod(ddldepartment.SelectedValue, Labour, SessionCcode, SessionLcode);
                    }
                    else
                    {
                        dt = objdata.Probationperiod_user(ddldepartment.SelectedValue, Labour, SessionCcode, SessionLcode);
                    }
                    gridprobation.DataSource = dt;
                    gridprobation.DataBind();
                }

            }
        }
        catch (Exception ex)
        {

        }
    }
    protected void btnexport_Click(object sender, EventArgs e)
    {
        if (ddlrptReport.SelectedValue == "1")
        {

            string attachment = "attachment; filename=Probationperiod.xls";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/ms-excel";

            StringWriter stw = new StringWriter();
            HtmlTextWriter htextw = new HtmlTextWriter(stw);
            gridprobation.RenderControl(htextw);
            Response.Write("Probationary Period Details");
            Response.Write(stw.ToString());
            Response.End();
        }
        else if (ddlrptReport.SelectedValue == "2")
        {

            string attachment = "attachment; filename=Probationperiod.pdf";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/pdf";
            StringWriter stw = new StringWriter();


            HtmlTextWriter htextw = new HtmlTextWriter(stw);
            gridprobation.RenderControl(htextw);

            Document document = new Document(PageSize.A4, 10f, 10f, 10f, 10f);


            PdfWriter.GetInstance(document, Response.OutputStream);
            document.Open();
            StringReader str = new StringReader(stw.ToString());
            HTMLWorker htmlworker = new HTMLWorker(document);
            htmlworker.Parse(str);
            document.Close();
            Response.Write(document);
            Response.End();
        }
        else if (ddlrptReport.SelectedValue == "3")
        {

            string attachment = "attachment; filename=Probationperiod.doc";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/ms-excel";
            StringWriter stw = new StringWriter();
            HtmlTextWriter htextw = new HtmlTextWriter(stw);
            gridprobation.RenderControl(htextw);
            Response.Write("Probationary Period Details");
            Response.Write(stw.ToString());
            Response.End();
        }

    }
    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }
    protected void ddlCategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable dtempty = new DataTable();
        ddldepartment.DataSource = dtempty;
        ddldepartment.DataBind();
        gridprobation.DataSource = dtempty;
        gridprobation.DataBind();
        if (ddlCategory.SelectedValue == "1")
        {
            Stafflabour = "S";
        }
        else if (ddlCategory.SelectedValue == "2")
        {
            Stafflabour = "L";
        }
        else
        {
            Stafflabour = "0";
        }
        DataTable dtDip = new DataTable();
        dtDip = objdata.DropDownDepartment_category(Stafflabour);
        if (dtDip.Rows.Count > 1)
        {
            ddldepartment.DataSource = dtDip;
            ddldepartment.DataTextField = "DepartmentNm";
            ddldepartment.DataValueField = "DepartmentCd";
            ddldepartment.DataBind();
        }
        else
        {
            DataTable dt = new DataTable();
            ddldepartment.DataSource = dtempty;
            ddldepartment.DataBind();
        }
    }
    protected void txtempNo_TextChanged(object sender, EventArgs e)
    {
        if (searchflag == false)
        {
            txtexist.Text = "";
        }
    }
    protected void txtexist_TextChanged(object sender, EventArgs e)
    {
        if (searchflag == false)
        {
            txtempNo.Text = "";
        }
    }
}
