﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Payroll;
using Payroll.Data;
using Payroll.Configuration;
using Altius.BusinessAccessLayer.BALDataAccess;


public partial class Leaveallocation : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    MastersClass objMas = new MastersClass();
    string stafforLabour;
    string SessionCcode;
    string SessionLcode;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
          
            LeaveallocationfromGrid();
            Department();
        }
        if (rbtnstfflbrothers.SelectedValue == "1")
        {
            Panel1stflbrothers.Visible = false;
        }
        else if (rbtnstfflbrothers.SelectedValue == "2")
        {
            Panel1stflbrothers.Visible = true;
        }
    }
    public void Department()
    {
        DataTable dtdep = new DataTable();
        dtdep = objdata.DropDownDepartment();
        ddlDepartment.DataSource = dtdep;
        ddlDepartment.DataTextField = "DepartmentNm";
        ddlDepartment.DataValueField = "DepartmentCd";
        ddlDepartment.DataBind();
    }
    protected void RowEdit_Canceling(object sender, GridViewCancelEditEventArgs e)
    {
        gvLeaveallocation.EditIndex = -1;
        LeaveallocationfromGrid();
    }
    protected void RowEdit_Editing(object sender, GridViewEditEventArgs e)
    {
        gvLeaveallocation.EditIndex = e.NewEditIndex;
        LeaveallocationfromGrid();
        
    }
    protected void RowEdit_Updating(object sender, GridViewUpdateEventArgs e)
    {

    }
    
      public void LeaveallocationfromGrid()
    {
        DataTable dt1 = new DataTable();
        dt1 = objdata.LeaveallocationfromGrid();
        gvLeaveallocation.DataSource = dt1;
        gvLeaveallocation.DataBind();
    }
      public void EmployeeDetails_Load()
      {

          if (rbtnstfflbrothers.SelectedValue == "1")
          {
              stafforLabour = "S";
          }
          else if (rbtnstfflbrothers.SelectedValue == "2")
          {
              stafforLabour = "L";
          }
      }

      protected void btnsave_Click(object sender, EventArgs e)
      {
          //objMas.allocatedleave = txtallocation.text;
          //objdata.allocation(objleave);
      
      }
      protected void btnsave_Click1(object sender, EventArgs e)
      {
          //    if (rbtnstaff.SelectedValue == "1")
          //    {
          //        foreach (GridViewRow gvrow in gvStudentBulk.Rows)
          //        {

          //            CheckBox check_box = (CheckBox)gvrow.FindControl("chkclear");
          //            if (check_box != null)
          //            {
          //                if (check_box.Checked)
          //                {
          //                    for (int i = 0; i < gvStudentBulk.Rows.Count; i++)
          //                    {


          //                        Label lblgvstuid = (Label)gvrow.FindControl("lblstudentid");


          //                        string sid = lblgvstuid.Text;
          //                        string Activemod = "1";

          //                        objdata.UpdateStudentActiveMode(sid, Activemod);
          //                        StudentActiveModeDisplay();


          //                    }
          //                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Updated Successfully...');", true);

          //                }
          //            }
          //        }
          //    }

          //    //if(rbtnstfflbrothers.SelectedValue=="2")
          //    //{
          //    //    foreach(GridViewRow gvrow in gvLeaveallocation.Rows)
          //    //    {
          //    //        CheckBox check_box=(CheckBox)gvrow.FindControl("chklabour");
          //    //        if(check_box !=null)
          //    //        {
          //    //            if(check_box.Checked)
          //    //            {
          //    //                for(int i=0; i<gvLeaveallocation.Rows.Count; i++)
          //    //                {
          //    //                    TextBox txtallocation=(TextBox)gvrow.FindControl("txtallocation");

          //    //                    string sid=txtallocation.Text;
          //    //                    string Activemod="1";




          //}

      }

      protected void rbtnstfflbrothers_SelectedIndexChanged(object sender, EventArgs e)
      {
          //if (rbtnstfflbrothers.SelectedValue == "1")
          //{
          //    Panel1stflbrothers.Visible = false;
          //}
          //else if (rbtnstfflbrothers.SelectedValue == "2")
          //{
          //    Panel1stflbrothers.Visible = true;
          //}
      }
      protected void btnindividual_Click(object sender, EventArgs e)
      {

      }
      protected void ddlDepartment_Textchanged(object sender, EventArgs e)
      {

          if (ddlDepartment.SelectedValue == "0")
          {
              Panel1stflbrothers.Visible = false;
              panelbtn.Visible = true;
              EmployeeDetails_Load();
              Panel1stflbrothers.Visible = false;
          }
          else if (ddlDepartment.SelectedValue == "1")
          {
              Panel1stflbrothers.Visible = true;
              panelbtn.Visible = true;
              EmployeeDetails_Load();
              Panel1stflbrothers.Visible = false;
          }
          else if (ddlDepartment.SelectedValue == "0")
          {
              Panel1stflbrothers.Visible = true;
              panelbtn.Visible = true;
              EmployeeDetails_Load();
              Panel1stflbrothers.Visible = false;
          }
              
      }
      protected void btnSearch_Click(object sender, EventArgs e)
      {
          bool ErrFlag = false;
        
              if(rbtnstfflbrothers.SelectedValue=="1")
          {
              stafforLabour = "S";
          }
          else if (rbtnstfflbrothers.SelectedValue=="2")
          {
              stafforLabour = "L";
          }
         
          //string EmpVerify = objdata.EmployeeVerifyforLeaveAllocation(txtempno.Text, ddlDepartment.SelectedValue, stafforLabour);
       
              if(txtempno.Text=="")
          {
              ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Enter the Employee No..!');", true);
              ErrFlag = true;
          }
          if (!ErrFlag)
          {
            
              Panel1stflbrothers.Visible = true;
              DataTable DT1 = new DataTable();
              DT1 = objdata.EmployeeVerifyforLeaveAllocation(txtempno.Text, ddlDepartment.SelectedValue, stafforLabour, SessionCcode, SessionLcode);
              gvLeaveallocation.DataSource = DT1;
              gvLeaveallocation.DataBind();
             
          }


      }
}
