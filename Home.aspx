﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Home.aspx.cs" Inherits="Home" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">

<head  id="hea" runat="server" >
<meta content="text/html; charset=UTF-8" http-equiv="Content-Type" />
<title>Payroll Management Systems</title>




<link rel="Stylesheet" type="text/css" href="Home/css/superfish.css" />
<link rel="Stylesheet" type="text/css"  href="Home/css/colors/dark_blue.css" />
<link rel="Stylesheet" type="text/css" href="Home/css/main.css" />




<script src="Home/js/jquery-1.4.2.min.js" type="text/javascript"></script>
<script src="Home/js/jquery.hoverintent.minified.js" type="text/javascript"></script>
<script src="Home/js/superfish.js" type="text/javascript"></script>
<script src="Home/js/jquery.tools.min.js" type="text/javascript"></script>

<script src="Home/js/jquery.simplemodal.js" type="text/javascript"></script>
<script src="Home/js/jquery.notifybar.js" type="text/javascript"></script>
<script src="Home/js/jquery.tipsy.js" type="text/javascript"></script>
<script src="Home/js/enhance.js" type="text/javascript"></script>
<script src="Home/js/excanvas.js" type="text/javascript"></script>
<script src="Home/js/visualize.jquery.js" type="text/javascript"></script>
<script src="Home/js/jquery.collapsible.js" type="text/javascript"></script>
<script src="Home/js/jquery.autosuggest.packed.js" type="text/javascript"></script>
<script src="Home/js/platinum-admin.js" type="text/javascript"></script>
<meta charset="UTF-8"></head>

<body>

<div id="header">
	<div id="header-top">
		<div id="logo">
			<h1>Altius Infosystems </h1>
			<span id="slogan">a altius payroll management systems admin </span> </div>
			
			<div id="logo1">
			
			<h2> Payroll Management Systems </h2>
			<%--<span id="Span1">a altius payroll management systems admin </span> --%>
			</div>
			
		<!-- end logo -->

		<!-- end login -->
		<div id="nav">
			<ul class="sf-menu sf-navbar">
			<%--	<li><a id="dashboard" href="Dashboard.aspx">Dashboard</a>--%>
				<%--<ul>
					<li>&nbsp;
					</li>
				</ul>
				</li>--%>
			<%--	<li class="current"><a id="articles" href="#a">Articles</a>--%>
				<%--<ul>
					<li class="current"><a href="#aa">write an article</a> </li>
					<li><a href="#ab">manage articles</a>
					<ul>
						<li><a href="#">New Article</a></li>
						<li><a href="#aba">List All</a></li>
						<li><a href="#abb">Add New</a></li>
						<li><a href="#abc">Categorys</a></li>
						<li><a href="#abd">Tags</a></li>
					</ul>
					</li>
					<li><a href="#">categorys</a> </li>
					<li><a href="#">tags</a> </li>
				</ul>
				</li>--%>
				<%--<li><a id="users" href="#">Users</a>--%>
				<%--<ul>
					<li><a href="#">list users</a>
					<ul>
						<li><a href="#">Admins</a></li>
						<li><a href="#">Editors</a></li>
						<li><a href="#">Writers</a></li>
						<li><a href="#">Others</a></li>
					</ul>
					</li>
					<li><a href="#">roles</a>
					<ul>
						<li><a href="#">List Roles</a></li>
						<li><a href="#">Add Role</a></li>
						<li><a href="#">Delete Role</a></li>
						<li><a href="#">Edit Roles</a></li>
					</ul>
					</li>
					<li><a href="#">manage</a> </li>
					<li><a href="#">edit</a> </li>
				</ul>--%>
				<%--</li>--%>
			<%--	<li><a id="settings" href="#">Settings</a>--%>
				<%--<ul>
					<li><a href="#">themes</a>
					<ul>
						<li><a href="#">Change Theme</a></li>
						<li><a href="#">Edit Theme</a></li>
					</ul>
					</li>
					<li><a href="#">profile</a>
					<ul>
						<li><a href="#">Profile picture</a></li>
						<li><a href="#">Messages</a></li>
						<li><a href="#">Other Settings</a></li>
					</ul>
					</li>
					<li><a href="#">menu item</a> </li>
					<li><a href="#">menu item</a> </li>
				</ul>--%>
				</li>
			</ul>
		</div>
		<!-- end div#nav --></div>
	<!-- end div#header-top -->
	<div class="breadCrumb">
		<%--<ul>
			<li class="first"><a href="#">Home</a> </li>
			<li><a href="#">Users</a> </li>
			<li><a href="#">Roles</a> </li>
			<li class="last">Add new role </li>
		</ul>--%>
	</div>
	<!-- end div#breadCrumb -->
	<div id="search-box">
		<form id="searchform" action="" method="get">
			<%--<fieldset class="search"><span>Search</span>
			<input class="box" type="text" />
			<button class="btn" title="Submit Search">Search</button></fieldset>--%>
		</form>
	</div>
	<!-- end div#search-box --></div>
<!-- end header -->
<div id="page-wrap">
	<div id="right-sidebar">
		<!-- Mini Form Start -->
	<%--	<div class="innerdiv">
			<h2 class="head">Mini Form</h2>
			<div class="innercontent">
				<form action="">
				<fieldset class="compact">
				<label>Category</label><input type="text" />
				<label>Other input</label><input type="text" />
				<input type="submit" class="button" value="Ok" />
				</fieldset></form>
			</div>
		</div>--%>
		<!-- end Miniform -->
	</div>
	<!-- end right-sidebar -->
	<div id="main-content">
		<div>
			<div>
				<h3>What would you like to do?</h3>
				<ul class="iconset">
					<li><a href="EmployeeRegistration.aspx"><span>
					<img alt="event" src="./images/icons/write.png" /> <br />
					Employee Registration </span></a></li>
					<li><a href="OfficalProfileDetails.aspx"><span>
					<img alt="event" src="./images/icons/event.png" /> <br />
					Official Profile  </span></a></li>
					<li><a href="LeaveDetails.aspx"><span>
					<img alt="event" src="./images/icons/adduser.png" /> <br />
					Leave Details
					</span></a></li>
					<li><a href="SalaryRegistration.aspx"><span>
					<img alt="event" src="./images/icons/database.png" /> <br />
					Salary Details </span></a></li>
					<li id="Masterpanel" runat="server" visible="false" ><a href="MastersHome.aspx"><span>
					<img alt="event" src="./images/icons/theme.png" /> <br />
					Masters </span></a>
					
					</li>
					<li id="ReportPanel" runat="server" visible="false"><a href="#"><span>
					<img alt="event" src="./images/icons/stats.png" /> <br />
					Reports </span></a></li>
					<li><a href="Default.aspx"><span>
					<img alt="event" src="./images/icons/stats.png" /> <br />
					Logout </span></a></li>
					<%--<li><a class="modal-box" href="#modal-content"><span>
					<img alt="event" src="./images/icons/dialog.png" /> <br />
					Open Modal </span></a></li>--%>
				</ul>
	
				<br class="clear" />
			</div>
		</div>
		<%--<div class="innerdiv clear">
			<!-- Tabs -->
			<h2 class="head-alt">Tabs</h2>
			<!-- the tabs -->
			<ul class="tabs">
				<li><a href="#"></a></li>
				<li><a href="#"></a></li>
				<li><a href="#"></a></li>
			</ul>
			<div class="innercontent">
				<!-- tab "panes" -->
				<div class="panes">
					<div>
						
						
						<form id="form1" runat="server" class="full"  >
             
             


    <div>
    <div class="borders2">
       
    
     </div>
    </div>
    </form>
						
							
					</div>
					
					
				</div>
			</div>
		</div>--%>
		<!-- end div.tab -->
		
		
		
		<!-- end innerdiv notification -->
		
		<!-- end div.notify -->
		
		<!-- end div alerts -->
		
		<!-- end div.buttons -->
		
		<!-- end div.buttons -->
		
		
		
				
	</div>
	<!-- end main-content --></div>
<!-- end page-wrap -->
<div id="footer">
	<p>© 2012Copyright<a href="http://www.altius.co.in"  /> by Altius Infosystems</p>
</div>
<!-- end footer -->

</body>

</html>
