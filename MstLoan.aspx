﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="MstLoan.aspx.cs" Inherits="loanMaster" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">


<html xmlns="http://www.w3.org/1999/xhtml">


<!-- Mirrored from www.ait.sk/uniadmin/ by HTTrack Website Copier/3.x [XR&CO'2010], Tue, 20 Jul 2010 00:37:25 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8"><!-- /Added by HTTrack -->
<head id="hea" runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<meta name="description"  content=""/>
<meta name="keywords" content=""/>
<meta name="robots" content="ALL,FOLLOW"/>
<meta name="Author" content="AIT"/>
<meta http-equiv="imagetoolbar" content="no"/>
<title>Payroll Management Systems</title>

<link rel="stylesheet" href="css/reset.css" type="text/css"/>

<link rel="Stylesheet" href="css/ScreenForGrid.css" type="text/css" />

<link rel="stylesheet" href="css/fancybox.css" type="text/css"/>
<link rel="stylesheet" href="css/jquery.wysiwyg.css" type="text/css"/>
<link rel="stylesheet" href="css/jquery.ui.css" type="text/css"/>
<link rel="stylesheet" href="css/visualize.css" type="text/css"/>
<link rel="stylesheet" href="css/visualize-light.css" type="text/css"/>
<link rel="Stylesheet" href="css/form.css" type="text/css" />
<!--[if IE 7]>
	<link rel="stylesheet" type="text/css" href="css/ie7.css" />
<![endif]-->	

<link media="screen" rel="stylesheet" type="text/css" href="css/admin.css"  />
<!--[if lte IE 6]><link media="screen" rel="stylesheet" type="text/css" href="css/admin-ie.css" /><![endif]-->

<script type="text/javascript" src="js/behaviour.js"></script>

<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery.visualize.js"></script>
<script type="text/javascript" src="js/jquery.wysiwyg.js"></script>
<script type="text/javascript" src="js/tiny_mce/jquery.tinymce.js"></script>
<script type="text/javascript" src="js/jquery.fancybox.js"></script>
<script type="text/javascript" src="js/jquery.idtabs.js"></script>
<script type="text/javascript" src="js/jquery.datatables.js"></script>
<script type="text/javascript" src="js/jquery.jeditable.js"></script>
<script type="text/javascript" src="js/jquery.ui.js"></script>

<script type="text/javascript" src="js/excanvas.js"></script>
<script type="text/javascript" src="js/cufon.js"></script>
<script type="text/javascript" src="js/Geometr231_Hv_BT_400.font.js"></script>
<script type="text/javascript" src="js/script.js"></script>

</head>

<body>
<div class="clear">
	
	<div class="sidebar"> <!-- *** sidebar layout *** -->
		<div class="logo clear">
			
				<img src="images/logo.png" alt="" class="picture" />
				
			
		</div>
		
		<div class="Elaya">
			<ul>
			<li><a href="EmployeeRegistration.aspx">Home</a></li>
			<li><a href="MstBank.aspx">Bank Master</a></li>
		    <li><a href="MstDepartment.aspx">Department Master</a></li>
			<li><a href="MstEmployeeType.aspx">Employee Type Master</a></li>
			<li><a href="MstInsuranceCompany.aspx">Insurance Company Master</a></li>
			<li><a href="MstLeave.aspx">Leave Master</a></li>
			<li><a href="MstLeavePaymaster.aspx">Leave Pay Master</a></li>
			<li><a href="MstProbationPeriod.aspx">Probation Period Master</a></li>
			<li><a href="MstQualification.aspx">Qualification Master</a></li>
			<li><a href="MstTaluk.aspx">Taluk Master</a></li>
			<li><a href="MstLoan.aspx">Loan Master</a></li>
			<li><a href="Default.aspx">Lagout</a></li>
			</ul>
		</div>
		<div class="Elaya">
				
			<%--	<ul>
				<li><a href="MastersHome.aspx">Home</a></li>
				</ul>	--%>
		
		</div>
	</div>
	
	
	<div class="main"> <!-- *** mainpage layout *** -->
	<div class="main-wrap">
		<div class="header clear">
			<ul class="links clear">
			<li></li>
			<li><a href="#"> <span class="text"><h1>Payroll Management Systems</h1></span></a></li>
			<li><a href="#"><span class="text"> </span></a></li>
			<li><a href="#"> <span class="text"></span></a></li>
			</ul>
		</div>
		
		<div class="page clear">
						<div class="main-icons clear">
						
						<ul class="links clear">
			<li></li>
			<li><a href="#"> <span class="text"></span></a></li>
			<li><a href="#"><span class="text"> </span></a></li>
			<li><a href="#"> <span class="text"></span></a></li>
			<li><a href="#"> <span class="text"></span></a></li>
			<li><a href="#"><span class="text"> </span></a></li>
			
			
				
			
			
			</ul>
			<%--	<ul class="clear" visible="false">
				<li visible="false"><a href="MstBank.aspx" class="modal-link"><img src="images/ico_folder_64.png" class="icon" alt="" /><span class="text">Bank Master</span></a>						</li>
				<li><a href="MstDepartment.aspx" class="modal-link"><img src="images/ico_folder_64.png" class="icon" alt="" /><span class="text">Department Master</span></a>						</li>
				<li><a href="MstEmployeeType.aspx" class="modal-link"><img src="images/date/calendar_week .png" class="icon" alt="" /><span class="text">EmployeeType Master</span></a></li>
				<li><a href="MstInsuranceCompany.aspx" class="modal-link"><img src="images/date/calendar_week .png" class="icon" alt="" /><span class="text">Insurance Company Master</span></a></li>
				<li><a href="MstLeave.aspx" class="modal-link"><img src="images/ico_clock_64.png" class="icon" alt="" /><span class="text">Leave Master</span></a></li>
				<li><a href="MstLeavePaymaster.aspx" class="modal-link"><img src="images/ico_clock_64.png" class="icon" alt="" /><span class="text">Leave Pay Master</span></a></li>
				<li><a href="MstProbationPeriod.aspx"class="modal-link"><img src="images/ico_clock_64.png" class="icon" alt="" /><span class="text">Probation Period Master</span></a></li>
				<li><a href="MstQualification.aspx" class="modal-link"><img src="images/ico_users_64.png" class="icon" alt="" /><span class="text">Oualification Master</span></a></li>
				<li><a href="MstTaluk.aspx" class="modal-link"><img src="images/ico_chat_64.png" class="icon" alt="" /><span class="text">Taluk Master</span></a></li>
				</ul>--%>
			</div>
			
			<!-- MODAL WINDOW -->
			<div id="modal" class="modal-window">
				<!-- <div class="modal-head clear"><a onclick="$.fancybox.close();" href="javascript:;" class="close-modal">Close</a></div> -->
				
				<div class="notification note-info">
					<a href="#" class="close" title="Close notification"><span>close</span></a>
					<span class="icon"></span>
					
				</div>
				
				</div>
			
			<!-- CONTENT BOXES -->
			<div class="content-box">
				<div >
					<ul class="tabs clear">
						<li><a href="#data-table"></a></li>
						<li><a href="#table"></a></li>
						<li><a href="#forms"></a></li>
					</ul>
					
					
				</div>
				
				
			<form id="form1" runat="server" class="form" >		
				<div class="section table_section">
						<!--[if !IE]>start title wrapper<![endif]-->
						<div class="title_wrapper">
							<h2>Loan Master</h2>
							<span class="title_wrapper_left"></span>
							<span class="title_wrapper_right"></span>
						</div>
						<!--[if !IE]>end title wrapper<![endif]-->
						<!--[if !IE]>start section content<![endif]-->
						<div class="section_content">
							<!--[if !IE]>start section content top<![endif]-->
							<div class="sct">
								<div class="sct_left">
									<div class="sct_right">
										<div class="sct_left">
											<div class="sct_right">
												
												<form action="#">
												<fieldset>
												<!--[if !IE]>start table_wrapper<![endif]-->
												<div class="table_wrapper">
													<div class="table_wrapper_inner">

			<cc1:ToolkitScriptManager runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true" ID="ScriptManager1" EnablePartialRendering="true"></cc1:ToolkitScriptManager>											
    <table>
    <tr>
    <%--<td colspan="3">
    <asp:Label ID="lblHeading" runat="server" Text="Loan Master" Font-Bold="true" Font-Size="20px" ></asp:Label>
    </td>--%>
    </tr>
    <tr align="center">
    <td><asp:Label ID="lblLoanCode" runat="server" Text="Loan ID" Font-Bold="true" TabIndex="1" ></asp:Label></td>
    <td colspan="2"><asp:TextBox ID="txtLoanCode" runat="server" ></asp:TextBox>
    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterMode="ValidChars" FilterType="Numbers,Custom" TargetControlID="txtLoanCode" ValidChars=""> </cc1:FilteredTextBoxExtender>
    </td>
    </tr>
    <tr align="center">
    <td><asp:Label ID="lblloanname" runat="server" Text="Loan Name" Font-Bold="true" ></asp:Label></td>
    <td colspan="2"><asp:TextBox ID="txtLoanname" runat="server" TabIndex="2" ></asp:TextBox>
    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterMode="ValidChars" FilterType="UppercaseLetters, LowercaseLetters, Custom" TargetControlID="txtLoanname" ValidChars=" "> </cc1:FilteredTextBoxExtender></td>
    </tr>
    <tr align="center">
    <td colspan="3"><asp:Button ID="btnSave" runat="server" Text="Save" 
            onclick="btnSave_Click" />
    <asp:Button ID="btnCancel" runat="server" Text="Cancel" onclick="btnCancel_Click" /></td>
    </tr>
    <tr>
    <td colspan="3">
    <asp:GridView ID="GridLoan" runat="server" AutoGenerateColumns="false" 
            onrowcancelingedit="GridLoan_RowCancelingEdit" 
            onrowediting="GridLoan_RowEditing" onrowupdating="GridLoan_RowUpdating" 
            onselectedindexchanged="GridLoan_SelectedIndexChanged">
    <Columns>
    <asp:TemplateField>
    <HeaderTemplate>Bank Code</HeaderTemplate>
    <ItemTemplate>
    <asp:Label ID="lblLoancd" runat="server" Text='<%# Eval("LoanID") %>'></asp:Label>
    </ItemTemplate>
    </asp:TemplateField>
    <asp:TemplateField>
    <HeaderTemplate>Bank Name</HeaderTemplate>
    <ItemTemplate>
    <asp:Label ID="lblLoannm" runat="server" Text='<%# Eval("LoanName") %>'></asp:Label>
    </ItemTemplate>
    <EditItemTemplate>
    <asp:TextBox ID="txtloannm" runat="server" Text='<%# Eval("LoanName") %>'></asp:TextBox>
    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterMode="ValidChars" FilterType="UppercaseLetters, LowercaseLetters, Custom" TargetControlID="txtloannm" ValidChars=" "> </cc1:FilteredTextBoxExtender></td>
    </EditItemTemplate>
    </asp:TemplateField>
    
    <asp:CommandField ButtonType="Image" CancelText="Cancel" UpdateText="Update" EditText="Edit" ControlStyle-Width="75" ControlStyle-Height="20"
     ShowEditButton="true"  ControlStyle-CssClass="edit_icon" UpdateImageUrl="images/update.png" CancelImageUrl="images/Delete.png" EditImageUrl="images/user_edit.png" ItemStyle-Height="30"/>
    </Columns>
    </asp:GridView></td>
    </tr>
    </table>
    </div>
												</div>
	</form>								
												
												
												</fieldset>
												</form>
												
												
									
				
					
					
						<form method="post" action="#">
						
					
						
					
						</form>
					</div><!-- /#table -->
					
					<!-- TABLE -->
					<!-- /#table -->
					
					<!-- Custom Forms -->
					<!-- /#forms -->
				</div> <!-- end of box-body -->
			</div> <!-- end of content-box -->
			
		
			
		
			
		
			
		</div><!-- end of page -->
		
		<div class="footer clear">
			<span class="copy"><strong>© 2012 Copyright by <a href="http://www.altius.co.in"/>Altius Infosystems.</a></strong></span> Powered by <a href="http://www.altius.co.in/">Altius.</a>
		</div>
	</div>
	</div>
</div>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-12958851-7']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</body>

<!-- Mirrored from www.ait.sk/uniadmin/ by HTTrack Website Copier/3.x [XR&CO'2010], Tue, 20 Jul 2010 00:38:01 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8"><!-- /Added by HTTrack -->
</html>

