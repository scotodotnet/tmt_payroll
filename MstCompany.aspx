﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="MstCompany.aspx.cs" Inherits="MstCompany" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <table>
    <tr>
    <td><asp:Label ID="Label1" runat="server" Text="Company Code"></asp:Label></td>
    <td><asp:TextBox ID="txtcompanycd" runat="server" Enabled="false" ></asp:TextBox></td>
    </tr>
    <tr>
    <td><asp:Label ID="Label2" runat="server" Text="Company Name"></asp:Label></td>
    <td><asp:TextBox ID="txtcompanynm" runat="server" ></asp:TextBox></td>
    </tr>
    <tr>
    <td><asp:Button ID="Button1" runat="server" Text="Save" onclick="Button1_Click"  /></td>
    </tr>
    
    <tr >
    <td colspan="2">
        <asp:GridView ID="gvCompany" runat="server"  
            AutoGenerateColumns="false" onrowcancelingedit="RowEdit_Canceling" 
            onrowediting="RowEdit_Editing" onrowupdating="RowEdit_Updating" >
    <Columns>
    <asp:TemplateField>
    <HeaderTemplate>Company Code</HeaderTemplate>
    <ItemTemplate>
    <asp:Label ID="lblcompanycd" runat="server" Text='<%# Eval("CompanyCd") %>'></asp:Label>
    </ItemTemplate>
    </asp:TemplateField>
    <asp:TemplateField>
    <HeaderTemplate>Company Name</HeaderTemplate>
    <ItemTemplate>
    <asp:Label ID="lblcompanynm" runat="server" Text='<%# Eval("CompanyNm") %>'></asp:Label>
    </ItemTemplate>
    <EditItemTemplate>
    <asp:TextBox ID="txtcompanynm" runat="server" Text='<%# Eval("CompanyNm") %>'></asp:TextBox>
    </EditItemTemplate>
    </asp:TemplateField>
    
    
          
    <asp:CommandField CancelText ="Cancel" UpdateText="Update" EditText="Edit" ShowEditButton="true" ButtonType="Button" />
    </Columns>
    
    </asp:GridView></td>
    </tr>
    </table>
    
    </div>
    </form>
</body>
</html>
