﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SalarySummary.aspx.cs" Inherits="SalarySummary" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
    <head id="Head1" runat="server">
        <title>Payroll Management Systems</title>
        <link href="new_css/main.css" rel="stylesheet" type="text/css" />
        <link href="new_css/colors/dark_blue.css" rel="stylesheet" type="text/css" />
        <link href="new_css/superfish.css" rel="stylesheet" type="text/css" />

        <script src="new_js/jquery-1.4.2.min.js" type="text/javascript"></script>
        <script src="new_js/jquery.hoverintent.minified.js" type="text/javascript"></script>
        <script src="new_js/superfish.js" type="text/javascript"></script>
        <script src="new_js/jquery.tools.min.js" type="text/javascript"></script>
        <%--<script src="new_js/jquery.simplemodal.js" type="text/javascript"></script>--%>
        <script src="new_js/jquery.notifybar.js" type="text/javascript"></script>
        <script src="new_js/jquery.tipsy.js" type="text/javascript"></script>
        <script src="new_js/enhance.js" type="text/javascript"></script>
        <script src="new_js/excanvas.js" type="text/javascript"></script>
        <script src="new_js/visualize.jquery.js" type="text/javascript"></script>
        <script src="new_js/jquery.collapsible.js" type="text/javascript"></script>
        <script src="new_js/jquery.autosuggest.packed.js" type="text/javascript"></script>
        <script src="new_js/platinum-admin.js" type="text/javascript"></script>
        <style>
            .sf-navbar li ul 
            {
	            width: 1100px; /*IE6 soils itself without this*/
	            margin: 0px auto;
	            margin-left:-20em;	            
            }
        </style>
    </head>
    <body>
        <form id="form1" runat="server">
            <cc1:ToolkitScriptManager runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true"
                ID="ScriptManager1" EnablePartialRendering="true">
            </cc1:ToolkitScriptManager>
            <div id="header">
                <div id="header-top">
                    <div id="logo">
	                    <h1>&nbsp;</h1>
	                    <span id="slogan">&nbsp;</span> 
	                </div>
                    <!-- end logo -->
                    <div id="login-info">
                        <img id="profile" alt="profile" src="./new_images/icons/user.png" />
                        <p id="top"><span id="name"><asp:Label ID="lblusername" runat="server"></asp:Label></span><br />
                        <span id="links">
                        <%--<a id="message-link" href="#" title="click to see the messages!">
                        <span id="message-count">1</span> new message. </a>--%></span></p>
                        <div>
                            <asp:Label ID="lblComany" runat="server" Text="" Font-Bold="true"></asp:Label>
                        </div>
	                    <div id="messages-box">
		                    <h4>Messages</h4>
	                        <h5><a id="new-message" href="#">New Message</a></h5>
	                        <hr />
	                        <ul id="messages">
		                        <li class="new"><a href="#">
		                            <img alt="user" src="./new_images/icons/sender.png" />
		                            <span class="sender">John Doe</span>
		                            <span class="description">Hi this is just a description...</span>
		                            <span class="date">20 minutes ago...</span> </a>
		                        </li>
		                        <li><a href="#">
		                            <img alt="user" src="./new_images/icons/sender.png" />
		                            <span class="sender">John Doe</span>
		                            <span class="description">Hi this is just a description...</span>
		                            <span class="date">24 minutes ago...</span> </a>
		                        </li>
		                        <li><a href="#">
		                            <img alt="user" src="./new_images/icons/sender.png" />
		                            <span class="sender">John Doe</span>
		                            <span class="description">Hi this is just a description...</span>
		                            <span class="date">50 minutes ago...</span> </a>
		                        </li>
		                        <li id="last"><a id="all-messages" href="#">See all messages!
		                            <span id="unreaded">8 unreaded.</span> </a>
		                        </li>
		                    </ul>
		                    <br class="clear" />
	                    </div><!-- end messages-box -->
	                    <a id="power" href="Default.aspx" title="Logout">logout</a>
                    </div>
                    <!-- end login -->
                    <div id="nav">
	                    <ul class="sf-menu sf-navbar">
	                        <li><a id="dashboard" href="Dashboard.aspx">Dashboard</a>
	                            <ul>
		                            <li>&nbsp;
		                            </li>
	                            </ul>
	                        </li>
	                        <li><a id="Employee" href="EmployeeRegistration.aspx">Employee</a>
	                            <ul>
		                          <li><a href="EmployeeRegistration.aspx">Employee Registration</a> </li>					                    
		                            <li><a href="OfficalProfileDetails.aspx">Official Profile</a></li>
		                            <li><a href="SalaryMaster.aspx">Salary Master</a></li>
		                            <%--<li><a href="LeaveAllocationEmployee.aspx">Leave Allocation</a></li>--%>
		                            <%--<li><a href="ApplyForLeave.aspx">Apply Leave</a></li>--%>						                    
		                            <li><a href="SalaryRegistration.aspx">Salary Details</a></li>
		                            <li><a href="SalaryAdvance.aspx">Salary Advance</a></li>
		                             <li><a href="Settlement.aspx">Settlement</a></li>
		                            <li><a href="EmployeeDeactiveMode.aspx">Employee Re-Activate</a></li>					                    
	                            </ul>
	                        </li>
	                        <li class="current"><a id="A1" href="RptDepartmentwiseEmployee.aspx">Reports</a>
	                            <ul>
		                            <li><a href="RptDepartmentwiseEmployee.aspx">Employee Details</a></li>
		                            <li><a href="RptAttendanceReport.aspx">Attendance Report</a></li>
		                            <%--<li><a href="RptProbationperiod.aspx">Probationary Period</a></li>
		                            <li><a href="ContractBreak.aspx">Contract Details</a></li>--%>
		                            <li><a href="RptResign.aspx">Resignation Reports</a></li>
		                            <li><a href="PaySlipGen.aspx">Payslip</a></li>
		                            <%--<li><a href="RptSalary.aspx">Salary Details</a></li>--%>
		                            <li class="current"><a href="SalarySummary.aspx">Salary Sumary</a></li>
		                             <li><a href="Incentive.aspx">Incentive Details</a></li><li><a href="RptBonus.aspx">Bonus Report</a></li>
	                            </ul>
	                        </li>
	                        <li><a id="OT" href="OverTime.aspx">OT</a>
	                            <ul>
	                                <li><a href="OverTime.aspx">OT</a></li>
	                                <li><a href="RptOT.aspx">OT Report</a></li>
	                            </ul>
	                        </li>
	                        <li><a id="Master" href="MstEmployeeType.aspx">Masters</a>
	                            <ul>
		                            <li><a href="MstEmployeeType.aspx">Employee Type</a></li>
		                            <li><a href="UserRegistration.aspx">User</a></li>
		                            <li><a href="MSTPFESI.aspx">PF</a></li>
		                            <li><a href="MstBank.aspx">Bank</a></li>
		                            <li><a href="MstDepartment.aspx">Department</a></li>
		                            <li><a href="MstDAfix.aspx">DA Arrears FIX</a></li>
	                            </ul>
	                        </li>
	                        <li><a id="Bonus" href="BonusForAll.aspx">Bonus</a>
	                            <ul>
		                            <li><a href="BonusForAll.aspx">Bonus</a></li>
		                            <li><a href="StandardBonus.aspx">Group Bonus</a></li>					                    
	                            </ul>
	                        </li>
	                        <li><a id="ESI_PF" href="PFDownload.aspx">ESI & PF</a>
	                            <ul>
		                            <%--<li><a href="PFForm5.aspx">PF Form 5</a></li>--%>
		                            <li><a href="PFDownload.aspx">PF Download</a></li>
		                            <li><a href="ESIDownload.aspx">ESI Download</a></li>
		                            <li><a href="PFForm3Anew.aspx">PF Form 3A</a></li>
		                            <li><a href="PFform6A.aspx">PF Form 6A</a></li>
		                            <%--<li><a href="FOrm10.aspx">Form 10</a></li>
		                            <li><a href="PFForm3Anew.aspx">PF Form 3A</a></li>
		                            <li><a href="PFform6A.aspx">PF Form 6A</a></li>
		                            <li><a href="ESIForm7.aspx">ESI Form 7</a></li>--%>
	                            </ul>
	                        </li>
	                        <li><a id="Upload" href="UploadEmployee.aspx">Upload</a>
	                            <ul>
	                            <li><a href="UploadEmployee.aspx">Employee Upload</a></li>
		                            <li><a href="AttenanceUpload.aspx">Attenance Upload</a></li>
		                            	<li><a href="UploadOT.aspx">OT Upload</a></li>
		                            	<li><a href="LoadEmpmusters.aspx">Employee Master</a></li>
	                            </ul>
	                        </li>
                        </ul>
                    </div>
                    <!-- end div#nav -->
                </div>
                <!-- end div#header-top -->
                <div class="breadCrumb">
                    <ul>
                        <li class="first"><a href="#">Home</a> </li>
                        <li><a href="#">Users</a> </li>
                        <li><a href="#">Roles</a> </li>
                        <li class="last">Salary Summary </li>
                    </ul>
                </div>
                <!-- end div#breadCrumb -->
                <%--<div id="search-box">
                    <form id="searchform" action="" method="get">
                        <fieldset class="search"><span>Search</span>
                        <input class="box" type="text" />
                        <button class="btn" title="Submit Search">Search</button></fieldset>
                    </form>
                </div>--%>
                <!-- end div#search-box -->
            </div>
            <!-- end header -->
            <div id="page-wrap">
                <div id="right-sidebar">
                    <div class="innerdiv">
	                    <h2 class="head">DownLoad Format</h2>
	                    <div class="innercontent clear">
		                    <h4>DownLoad Format</h4>
		                    <ul id="comments" class="tooltip-enabled">
		                        <asp:Button ID="btnEmp" runat="server" Text="Employee Download" Width="200" 
                                    CssClass="button green" onclick="btnEmp_Click" />
		                        <asp:Button ID="btnatt" runat="server" Text="Attenance Download" Width="200" 
                                    CssClass="button green" onclick="btnatt_Click"/>
		                        <asp:Button ID="btnLeave" runat="server" Text="Leave Download" Width="200" 
                                    CssClass="button green" OnClick="btnLeave_Click" />
                                    <asp:Button ID="btnOT" runat="server" Text="OT Download" Width="200" 
                                    CssClass="button green" onclick="btnOT_Click" />
		                        <asp:Button ID="btnSal" runat="server" Text="Salary Download" Width="200" 
                                    CssClass="button green" onclick="btnSal_Click" />
		                    </ul>
		                    <%--<img src="new_images/icons/right_img.jpg" alt="" width="230"/>--%>
	                    </div>
                    </div>
                    <div class="clear"></div>                    
                </div>
	            <!-- end right-sidebar -->
	            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
	                <Triggers>	                
	                    <asp:PostBackTrigger ControlID="btnexport" />
	                </Triggers>
                    <ContentTemplate >
                        <div id="main-content">
                            <br /><br /><br /><br /><br />
                            <div class="clear_body">
                                <div class="innerdiv">
                                    <div class="innercontent">
                                        <!-- tab "panes" -->
                                        <div>
                                            <table class="full">
                                            <thead>
				                                    <tr>
					                                    <th colspan="3" style="text-align:center;"><h5>Salary Summary</h5></th>
				                                    </tr>
				                                </thead>
				                                <tbody>
				                                    <tr>
				                                        <td>
				                                            <asp:Label ID="lblFinancialYear" runat="server" Text="Financial Year" Font-Bold="true" ></asp:Label>
				                                        </td>
				                                        <td>
				                                            <asp:DropDownList ID="ddlfinance" runat="server" AutoPostBack="true" Width="180" Height="26"></asp:DropDownList>
				                                        </td>
				                                        <td colspan="3">
				                                            <asp:Button ID="btnclick" runat="server" Text="Click" CssClass="button green" 
                                                                onclick="btnclick_Click" />
				                                        </td>
				                                    </tr>
				                                    <tr>
                                                        <td><asp:Label ID="lblformat" runat="server" Text="Format" Font-Bold="true"></asp:Label></td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlexport" runat="server" Width="140" Height="25">
                                                                <asp:ListItem Text="--Select---" Value="0"></asp:ListItem>
                                                                <asp:ListItem Text="Excel" Value="1"></asp:ListItem>
                                                                <asp:ListItem Text="PDF" Value ="2"></asp:ListItem>
                                                                <asp:ListItem Text="Word" Value="3"></asp:ListItem>
                                                            </asp:DropDownList>
                                                            <asp:Button ID="btnexport" runat="server" Text="Export"  Width="75" Height="28" 
                                                                onclick="btnexport_Click" CssClass="button green"/>
                                                        </td>
                                                        <td colspan="1">&nbsp;</td>
                                                    </tr>        
				                                    <tr>
				                                        <td colspan="3">
				                                            <asp:GridView ID="gvload" runat="server" AutoGenerateColumns="false" >
				                                                <Columns>
				                                                    <asp:TemplateField>
				                                                        <HeaderTemplate>Employee No</HeaderTemplate>
				                                                        <ItemTemplate>
				                                                            <asp:Label ID="lblEmpNo" runat="server" Text='<%# Eval("EmpNo") %>'></asp:Label>
				                                                        </ItemTemplate>
				                                                    </asp:TemplateField>
				                                                    <asp:TemplateField>
				                                                        <HeaderTemplate>Exist No</HeaderTemplate>
				                                                        <ItemTemplate>
				                                                            <asp:Label ID="lblExist" runat="server" Text='<%# Eval("ExisistingCode") %>'></asp:Label>
				                                                        </ItemTemplate>
				                                                    </asp:TemplateField>
				                                                    <asp:TemplateField>
				                                                        <HeaderTemplate>Name</HeaderTemplate>
				                                                        <ItemTemplate>
				                                                            <asp:Label ID="lblEmpName" runat="server" Text='<%# Eval("EmpName") %>'></asp:Label>
				                                                        </ItemTemplate>
				                                                    </asp:TemplateField>
				                                                    <asp:TemplateField>
				                                                        <HeaderTemplate>Department</HeaderTemplate>
				                                                        <ItemTemplate> 
				                                                            <asp:Label ID="lblDepartment" runat="server" Text='<%# Eval("DepartmentNm") %>'></asp:Label>
				                                                        </ItemTemplate>
				                                                    </asp:TemplateField>
				                                                    <asp:TemplateField>
				                                                        <HeaderTemplate>Worked Days</HeaderTemplate>
				                                                        <ItemTemplate>
				                                                            <asp:Label ID="lblDays" runat="server" Text='<%# Eval("WorkedDays") %>'></asp:Label>
				                                                        </ItemTemplate>
				                                                    </asp:TemplateField>
				                                                    <asp:TemplateField>
				                                                        <HeaderTemplate>Salary Amount</HeaderTemplate>
				                                                        <ItemTemplate>
				                                                            <asp:Label ID="lblSalary" runat="server" Text='<%# Eval("NetPay") %>'></asp:Label>
				                                                        </ItemTemplate>
				                                                    </asp:TemplateField>
				                                                </Columns>
				                                            </asp:GridView>
				                                        </td>
				                                    </tr>
				                                </tbody>
				                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>               
            </div>
            <!-- end page-wrap -->
            <div id="footer">
                <ul>
                    <li>Copyright &copy; 2012. All rights reserved.</li>
                    <li>Powered by <a href="http://www.altius.co.in" target="_blank">Altius Infosystems</a></li>
                </ul>
            </div>
            <!-- end footer -->
        </form>
    </body>
</html>

