﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class NewContract : System.Web.UI.Page
{
    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    System.Globalization.CultureInfo culterInfo = new System.Globalization.CultureInfo("en-GB");
    SqlConnection con;
    BALDataAccess objdata = new BALDataAccess();
    string SessionAdmin;
    string Stafflabour;
    string SessionCcode;
    string SessionLcode;
    string Mydate;
    OfficialprofileClass objOff = new OfficialprofileClass();
    private void clr()
    {
        DataTable dt_Empty = new DataTable();
        ddlEmpNo.DataSource = dt_Empty;
        ddlEmpNo.DataBind();
        ddlEmpName.DataSource = dt_Empty;
        ddlEmpName.DataBind();
        txtExist.Text = null;
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        con = new SqlConnection(constr);
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        if (Session["Isadmin"].ToString() != "1")
        {
            Response.Redirect("ContractRPT.aspx");
        }
        SessionAdmin = Session["Isadmin"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        lblComany.Text = SessionCcode + " - " + SessionLcode;
        lblusername.Text = Session["Usernmdisplay"].ToString();
        string ss = Session["UserId"].ToString();
        if (!IsPostBack)
        {
            DropDownDepart();
            DropDownContract();
        }
    }
    public void DropDownDepart()
    {
        DataTable dt = new DataTable();
        dt = objdata.Depart_Labour();
        ddldepartment.DataSource = dt;
        ddldepartment.DataTextField = "DepartmentNm";
        ddldepartment.DataValueField = "DepartmentCd";
        ddldepartment.DataBind();
    }
    public void DropDownContract()
    {
        DataTable dt = new DataTable();
        dt = objdata.Contract_Load();
        ddlContract.DataSource = dt;
        DataRow dr = dt.NewRow();
        dr["ContractName"] = "----Select----";
        dr["ContractCode"] = 0;
        dt.Rows.InsertAt(dr, 0);
        ddlContract.DataTextField = "ContractName";
        ddlContract.DataValueField = "ContractCode";
        ddlContract.DataBind();
    }
    protected void btnEmp_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptEmpDownload.aspx");
    }
    protected void btnatt_Click(object sender, EventArgs e)
    {
        Response.Redirect("AttenanceDownload.aspx");
    }
    protected void btnLeave_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptLeaveSample.aspx");
    }
    protected void btnOT_Click(object sender, EventArgs e)
    {
        Response.Redirect("OTDownload.aspx");
    }
    protected void btncontract_Click(object sender, EventArgs e)
    {
        Response.Redirect("ContractRPT.aspx");
    }
    protected void ddldepartment_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable dt_Empty = new DataTable();
        DataTable dt = new DataTable();
        ddlEmpNo.DataSource = dt_Empty;
        ddlEmpNo.DataBind();
        ddlEmpName.DataSource = dt_Empty;
        ddlEmpName.DataBind();
        dt = objdata.Contract_Load_Employee(SessionCcode, SessionLcode);
        DataRow dr = dt.NewRow();
        dr["EmpNo"] = ddldepartment.SelectedItem.Text;
        dr["EmpName"] = ddldepartment.SelectedItem.Text;
        dt.Rows.InsertAt(dr, 0);
        ddlEmpNo.DataSource = dt;
        ddlEmpNo.DataTextField = "EmpNo";
        ddlEmpNo.DataValueField = "EmpNo";
        ddlEmpNo.DataBind();
        ddlEmpName.DataSource = dt;
        ddlEmpName.DataTextField = "EmpName";
        ddlEmpName.DataValueField = "EmpNo";
        ddlEmpName.DataBind();

    }
    protected void btnclr_Click(object sender, EventArgs e)
    {
        clr();
    }
    protected void ddlEmpNo_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable dtempName = new DataTable();
        dtempName = objdata.BonusEmployeeName(ddlEmpNo.SelectedValue, SessionCcode, SessionLcode);
        ddlEmpName.DataSource = dtempName;
        ddlEmpName.DataTextField = "EmpName";
        ddlEmpName.DataValueField = "EmpNo";
        ddlEmpName.DataBind();
        txtExist.Text = dtempName.Rows[0]["ExisistingCode"].ToString();
    }
    protected void ddlEmpName_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable dtempName = new DataTable();
        dtempName = objdata.BonusEmployeeName(ddlEmpName.SelectedValue, SessionCcode, SessionLcode);
        ddlEmpNo.DataSource = dtempName;
        ddlEmpNo.DataTextField = "EmpNo";
        ddlEmpNo.DataValueField = "EmpNo";
        ddlEmpNo.DataBind();
        txtExist.Text = dtempName.Rows[0]["ExisistingCode"].ToString();
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            bool ErrFlag = false;
            if (txtExist.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Enter the Exist No....!');", true);
                ErrFlag = true;
            }
            else if (ddlEmpNo.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select the Employee No....!');", true);
                ErrFlag = true;
            }
            else if (ddlEmpNo.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select the Employee Name....!');", true);
                ErrFlag = true;
            }
            else if (ddlContract.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select the Contract....!');", true);
                ErrFlag = true;
            }
            else if (txtdobjoin.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Enter the Date....!');", true);
                ErrFlag = true;
            }
            if (!ErrFlag)
            {
                DataTable dt_off = new DataTable();
                dt_off = objdata.Official_contract(ddlContract.SelectedValue);
                if (dt_off.Rows.Count > 0)
                {
                    objOff.EmpNo = ddlEmpNo.Text;
                    objOff.Contractname = ddlContract.SelectedValue;

                    if (dt_off.Rows[0]["ContractType"].ToString() == "1")
                    {
                        objOff.ContractType = "1";
                        objOff.yr = dt_off.Rows[0]["ContractYear"].ToString();
                        objOff.YrDays = dt_off.Rows[0]["YrDays"].ToString();
                        objOff.PlannedDays = dt_off.Rows[0]["PlannedDays"].ToString();
                        objOff.BalanceDays = dt_off.Rows[0]["PlannedDays"].ToString();
                        objOff.Days1 = "0";
                        objOff.Months = "0";
                        objOff.FixedDays = "0";

                    }
                    else if (dt_off.Rows[0]["ContractType"].ToString() == "2")
                    {
                        objOff.ContractType = "2";
                        objOff.yr = "0";
                        objOff.Days1 = "0";
                        objOff.YrDays = "0";
                        objOff.PlannedDays = dt_off.Rows[0]["PlannedDays"].ToString();
                        objOff.BalanceDays = dt_off.Rows[0]["PlannedDays"].ToString();
                        objOff.Months = dt_off.Rows[0]["ContractMonth"].ToString();
                        objOff.FixedDays = dt_off.Rows[0]["FixedDays"].ToString();
                    }
                    else if (dt_off.Rows[0]["ContractType"].ToString() == "3")
                    {
                        objOff.ContractType = "3";
                        objOff.yr = "0";
                        objOff.YrDays = "0";
                        objOff.PlannedDays = dt_off.Rows[0]["PlannedDays"].ToString();
                        objOff.BalanceDays = dt_off.Rows[0]["PlannedDays"].ToString();
                        objOff.Days1 = dt_off.Rows[0]["ContractDays"].ToString();
                        objOff.Months = "0";
                        objOff.FixedDays = "0";
                    }

                }
                if (!ErrFlag)
                {
                    string Date1 = objdata.ServerDate();
                    objOff.Ccode = SessionCcode;
                    objOff.Lcode = SessionLcode;
                    //Mydate = DateTime.Today.ToShortDateString();
                    objdata.Insert_ContractDet(objOff, txtdobjoin.Text);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Saved SuccessFully....!');", true);
                    clr();

                }
            }
        }
        catch
        {
        }
    }
}
