﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Payroll;
using Payroll.Data;
using Payroll.Configuration;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html;
using iTextSharp.text.html.simpleparser;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using Altius.BusinessAccessLayer.BALDataAccess;
public partial class MstOthers : System.Web.UI.Page
{
    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    SqlConnection con;
    BALDataAccess objdata = new BALDataAccess();
    string SessionAdmin;
    string Stafflabour;
    string SessionCcode;
    string SessionLcode;
    protected void Page_Load(object sender, EventArgs e)
    {
        con = new SqlConnection(constr);
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionAdmin = Session["Isadmin"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        lblComany.Text = SessionCcode + " - " + SessionLcode;
        lblusername.Text = Session["Usernmdisplay"].ToString();
        string ss = Session["UserId"].ToString();
        if (!IsPostBack)
        {
            DropDwonCategory();
            DisplayMess();
            DisplaySalary();
            //Months_load();
          
        }
    }
    public void DropDwonCategory()
    {
        DataTable dtcate = new DataTable();
        dtcate = objdata.DropDownCategory();
        ddlcategory.DataSource = dtcate;
        ddlsalcategory.DataSource = dtcate;
        ddlcategory.DataTextField = "CategoryName";
        ddlcategory.DataValueField = "CategoryCd";
        ddlsalcategory.DataTextField = "CategoryName";
        ddlsalcategory.DataValueField = "CategoryCd";
        ddlcategory.DataBind();
        ddlsalcategory.DataBind();
    }
    public void EmployeeType()
    {
        DataTable dtemp = new DataTable();
        dtemp = objdata.EmployeeDropDown_no(ddlcategory.SelectedValue);
        ddldept.DataSource = dtemp;
        ddldept.DataTextField = "EmpType";
        ddldept.DataValueField = "EmpTypeCd";
        ddldept.DataBind();
    }
    public void SalEmptype()
    {
        DataTable dtemp = new DataTable();
        dtemp = objdata.EmployeeDropDown_no(ddlsalcategory.SelectedValue);
        ddlsalemptype.DataSource = dtemp;
        ddlsalemptype.DataTextField = "EmpType";
        ddlsalemptype.DataValueField = "EmpTypeCd";
        ddlsalemptype.DataBind();
    }
    protected void ddlcategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable dtempty = new DataTable();
        ddldept.DataSource = dtempty;
        ddldept.DataBind();

        if (ddlcategory.SelectedValue == "1")
        {
            Stafflabour = "S";
        }
        else if (ddlcategory.SelectedValue == "2")
        {
            Stafflabour = "L";
        }
        else
        {
            Stafflabour = "0";
        }
        EmployeeType();
    }
    public void DisplayMess()
    {
        DataTable dt = new DataTable();
        dt = objdata.MstMessAmtDisplay_Sp();
        gvIncentive.DataSource = dt;
        gvIncentive.DataBind();
    }
    public void DisplaySalary()
    {
        DataTable dt = new DataTable();
        dt = objdata.MstSalDivisionDisplay_SP();
        gvSalary.DataSource = dt;
        gvSalary.DataBind();
    }
    protected void btnEmp_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptEmpDownload.aspx");
    }
    protected void btnatt_Click(object sender, EventArgs e)
    {
        Response.Redirect("AttenanceDownload.aspx");
    }
    protected void btnLeave_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptLeaveSample.aspx");
    }
    protected void btnOT_Click(object sender, EventArgs e)
    {
        Response.Redirect("OTDownload.aspx");
    }
    protected void btncontract_Click(object sender, EventArgs e)
    {

    }
    protected void btnsubmit_Click(object sender, EventArgs e)
    {
        try
        {
            bool ErrFlag = false;
            if ((ddlsalcategory.SelectedValue == "") || (ddlsalcategory.SelectedValue == "0"))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Category Properly');", true);
                ErrFlag = true;
            }
            else if ((ddlsalemptype.SelectedValue == "") || (ddlsalemptype.SelectedValue == "0"))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Employee Type Properly');", true);
                ErrFlag = true;
            }
            if (!ErrFlag)
            {
                if (ddlsalcategory.SelectedValue == "1")
                {
                    Stafflabour = "S";
                }
                else if (ddlsalcategory.SelectedValue == "2")
                {
                    Stafflabour = "L";
                }
                DataTable dt = new DataTable();
                dt = objdata.MstSalDivision_SP(rbSalwages.SelectedValue, Stafflabour, ddlsalemptype.SelectedValue, txtBasicDA.Text, txtHRA.Text, SessionCcode, SessionLcode);
                ErrFlag = true;
                if (ErrFlag == true)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Successfully Added..,');", true);
                }
                DisplaySalary();
            }
        }
        catch (Exception ex)
        {

        }
    }
    protected void btnSalClear_Click(object sender, EventArgs e)
    {

    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            bool ErrFlag = false;
            if ((ddlcategory.SelectedValue == "") || (ddlcategory.SelectedValue == "0"))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Category Properly');", true);
                ErrFlag = true;
            }
            else if ((ddldept.SelectedValue == "") || (ddldept.SelectedValue == "0"))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Employee Type Properly');", true);
                ErrFlag = true;
            }
            if (!ErrFlag)
            {
                if (ddlcategory.SelectedValue == "1")
                {
                    Stafflabour = "S";
                }
                else if (ddlcategory.SelectedValue == "2")
                {
                    Stafflabour = "L";
                }
                DataTable dt = new DataTable();
                //dt = objdata.MstMessAmt_SP(rbsalary.SelectedValue, Stafflabour, ddldept.SelectedValue, txtMessAmt.Text, SessionCcode, SessionLcode);
                dt = objdata.MstIncentive_SP(rbsalary.SelectedValue, Stafflabour, ddldept.SelectedValue, txtShiftI.Text.Trim(), txtshiftII.Text.Trim());
                ErrFlag = true;
                if (ErrFlag == true)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Successfully Added..,');", true);
                }
                DisplayMess();
            }
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Not Added..,');", true);
        }

    }
    protected void btnClear_Click(object sender, EventArgs e)
    {

    }
    protected void rbSalwages_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void rbsalary_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void ddlsalcategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable dtempty = new DataTable();
        ddlsalemptype.DataSource = dtempty;
        ddlsalemptype.DataBind();

        if (ddlsalcategory.SelectedValue == "1")
        {
            Stafflabour = "S";
        }
        else if (ddlsalcategory.SelectedValue == "2")
        {
            Stafflabour = "L";
        }
        else
        {
            Stafflabour = "0";
        }
        SalEmptype();
    }
    protected void ddlcategory_SelectedIndexChanged1(object sender, EventArgs e)
    {
        DataTable dtempty = new DataTable();
        ddldept.DataSource = dtempty;
        ddldept.DataBind();

        if (ddlcategory.SelectedValue == "1")
        {
            Stafflabour = "S";
        }
        else if (ddlcategory.SelectedValue == "2")
        {
            Stafflabour = "L";
        }
        else
        {
            Stafflabour = "0";
        }
        EmployeeType();
    }
    protected void gvIncentive_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        try
        {
            bool ErrFlag = false;
            TextBox txtShiftI1 = (TextBox)gvIncentive.Rows[e.RowIndex].FindControl("txtgvshiftI");
            TextBox txtShiftII2 = (TextBox)gvIncentive.Rows[e.RowIndex].FindControl("txtgvshiftII");
            //if (txtdEmptype1.Text == "")
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter EmpType Name');", true);
            //    ErrFlag = true;
            //}
            if (txtShiftI1.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Shift I Amount');", true);
                ErrFlag = true;
            }
            else if (txtShiftII2.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Shift II Amount');", true);
                ErrFlag = true;
            }
            if (!ErrFlag)
            {
                Label lbldepartmentcd1 = (Label)gvIncentive.Rows[e.RowIndex].FindControl("lblM_Id");
                DataTable dt = new DataTable();
                //dt = objdata.MstMessAmt_Update(lbldepartmentcd1.Text, txtdMessAmt1.Text);
                dt = objdata.MstIncentive_Upt(lbldepartmentcd1.Text, txtShiftI1.Text, txtShiftII2.Text);
                ErrFlag = true;
                DisplayMess();
                gvIncentive.EditIndex = -1;
                gvIncentive.DataBind();
                if (ErrFlag == true)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Updated Successfully..,');", true);

                }

            }
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Not Updated');", true);
        }
    }
    protected void gvIncentive_RowEditing(object sender, GridViewEditEventArgs e)
    {
        gvIncentive.EditIndex = e.NewEditIndex;
        DisplayMess();
    }
    protected void gvIncentive_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        gvIncentive.EditIndex = -1;
        DisplayMess();
    }
    protected void gvIncentive_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            bool ErrFlag = false;
            Label lbldepartmentcd1 = (Label)gvIncentive.Rows[e.RowIndex].FindControl("lblM_Id");
            DataTable dt = new DataTable();
            dt = objdata.MstIncentive_Dele(lbldepartmentcd1.Text);
            ErrFlag = true;
            if (ErrFlag == true)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Deleted Successfully..,');", true);

            }
            DisplayMess();

        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Not Deleted..,');", true);
        }
    }
    protected void gvSalary_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        try
        {
            bool ErrFlag = false;
            TextBox txtdEmptype1 = (TextBox)gvSalary.Rows[e.RowIndex].FindControl("txtgvBasic");
            TextBox txtdMessAmt1 = (TextBox)gvSalary.Rows[e.RowIndex].FindControl("txtgvHRA");
            if (txtdMessAmt1.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter HRA');", true);
                ErrFlag = true;
            }
            else if (txtdEmptype1.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter BasicDA');", true);
                ErrFlag = true;
            }
            if (!ErrFlag)
            {
                Label lbldepartmentcd1 = (Label)gvSalary.Rows[e.RowIndex].FindControl("lblSal_Id");
                DataTable dt = new DataTable();
                dt = objdata.MstSalDivision_Update(lbldepartmentcd1.Text, txtdEmptype1.Text,txtdMessAmt1.Text);
                ErrFlag = true;
                DisplayMess();
                gvSalary.EditIndex = -1;
                DisplaySalary();
                //gvSalary.DataBind();
                if (ErrFlag == true)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Updated Successfully..,');", true);

                }
            }

        }
        catch (Exception ex)

        { 
        
        }
    }
    protected void gvSalary_RowEditing(object sender, GridViewEditEventArgs e)
    {

        gvSalary.EditIndex = e.NewEditIndex;
        DisplaySalary();
        //gvSalary.DataBind();
    }
    protected void gvSalary_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            bool ErrFlag = false;
            Label lbldepartmentcd1 = (Label)gvSalary.Rows[e.RowIndex].FindControl("lblSal_Id");
            DataTable dt = new DataTable();
            dt = objdata.MstSalDivision_Delete(lbldepartmentcd1.Text);
            ErrFlag = true;
            if (ErrFlag == true)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Deleteed Successfully..,');", true);

            }
            DisplaySalary();

        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Not Deleted..,');", true);
        }
    }
    protected void gvSalary_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        gvSalary.EditIndex = -1;
        DisplaySalary();
        //gvSalary.DataBind();
    }
}