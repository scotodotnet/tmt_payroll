﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Payroll;
using Payroll.Configuration;
using Payroll.Data;

using Altius.BusinessAccessLayer.BALDataAccess;

public partial class EditOfficialProfile_1 : System.Web.UI.Page
{
    protected System.Resources.ResourceManager rm;
    protected string PayrollRegisterContentMeta, pageTitle;
    BALDataAccess objdata = new BALDataAccess();
    OfficialprofileClass objOff = new OfficialprofileClass();
    EmployeeClass objEmp = new EmployeeClass();
    System.Globalization.CultureInfo culterInfo = new System.Globalization.CultureInfo("en-GB");
    // static string ExisitingNo = "";
    static string UserID = "", SesRoleCode = "", SessionAdmin = "";
    string SessionCcode;
    string SessionLcode;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        UserID = Session["UserId"].ToString();
        SesRoleCode = Session["RoleCode"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        lblComany.Text = SessionCcode + " - " + SessionLcode;
        if (!IsPostBack)
        {

            OfficialProfileDisplay();
            DropdownProbationPeriod();
            DropDownInsurance();
            MstBankDropDown();
            //Department();
            DropDwonCategory();
            DropDownContract();
            int currentYear = Utility.GetFinancialYear;
            for (int i = 0; i < 10; i++)
            {
                ddlFinancialPeriod.Items.Add(new ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                //  ddlShowYear.Items.Add(new ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                currentYear = currentYear - 1;
            }

        }

        lblusername.Text = Session["Usernmdisplay"].ToString();
    }
    public void DropDownContract()
    {
        DataTable dt = new DataTable();
        dt = objdata.DropDown_contract();
        ddlcontract.DataSource = dt;
        ddlcontract.DataTextField = "ContractName";
        ddlcontract.DataValueField = "ContractCode";
        ddlcontract.DataBind();
    }
    public void MstBankDropDown()
    {

        DataTable dt1 = new DataTable();
        dt1 = objdata.MstBankDropDown();
        ddlbankname.DataSource = dt1;
        ddlbankname.DataTextField = "BankName";
        ddlbankname.DataValueField = "Bankcd";
        ddlbankname.DataBind();
    }
    public void DropDwonCategory()
    {
        DataTable dtcate = new DataTable();
        dtcate = objdata.DropDownCategory();
        ddlcategory.DataSource = dtcate;
        ddlcategory.DataTextField = "CategoryName";
        ddlcategory.DataValueField = "CategoryCd";
        ddlcategory.DataBind();
    }
    public void Department()
    {
        DataTable dtDip = new DataTable();
        dtDip = objdata.DropDownDepartment();
        ddldepartment.DataSource = dtDip;
        ddldepartment.DataTextField = "DepartmentNm";
        ddldepartment.DataValueField = "DepartmentCd";
        ddldepartment.DataBind();
    }
    public void DropdownProbationPeriod()
    {
        DataTable dtProbation = new DataTable();
        dtProbation = objdata.DropDownProbationPriod();
        ddlprobation.DataSource = dtProbation;
        ddlprobation.DataTextField = "ProbationMonth";
        ddlprobation.DataValueField = "ProbationCd";
        ddlprobation.DataBind();
        ddlcontract.DataSource = dtProbation;
        ddlcontract.DataTextField = "ProbationMonth";
        ddlcontract.DataValueField = "ProbationCd";
        ddlcontract.DataBind();
    }
    public void DropDownInsurance()
    {
        DataTable dtIns = new DataTable();
        dtIns = objdata.DropDownInsurance();
        ddlInsurance.DataSource = dtIns;
        ddlInsurance.DataTextField = "InsuranceCmpNm";
        ddlInsurance.DataValueField = "Insurancecmpcd";
        ddlInsurance.DataBind();
    }
    public void Financial_Period()
    {
        int currentYear = Utility.GetFinancialYear;
        for (int i = 0; i < 10; i++)
        {
            ddlFinancialPeriod.Items.Add(new ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
            //  ddlShowYear.Items.Add(new ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
            currentYear = currentYear - 1;
        }

    }
    public void OfficialProfileDisplay()
    {
        //if (rbtnstafflabour.SelectedValue == "1")
        //{
        //    string Staff = "S";
        //    DataTable dtOPP = new DataTable();
        //    dtOPP = objdata.OfficialProfileGridView(Staff);
        //    GvOfficialProfile.DataSource = dtOPP;
        //    GvOfficialProfile.DataBind();
        //    PanelLabour.Visible = false;
        //}
        //else if (rbtnstafflabour.SelectedValue == "2")
        //{
        //    string Staff = "L";
        //    DataTable dtOPP = new DataTable();
        //    dtOPP = objdata.OfficialProfileGridView(Staff);
        //    GvOfficialProfile.DataSource = dtOPP;
        //    GvOfficialProfile.DataBind();
        //    PanelLabour.Visible = true;
        //}
    }
    protected void PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GvOfficialProfile.PageIndex = e.NewPageIndex;
        //OfficialProfileDisplay();
    }
    protected void ddlcategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        panelError.Visible = false;
        string Stafflabour;
        Clear();
        DataTable dempty = new DataTable();
        GvOfficialProfile.DataSource = dempty;
        GvOfficialProfile.DataBind();
        if (ddlcategory.SelectedValue == "1")
        {
            Stafflabour = "S";
        }
        else if (ddlcategory.SelectedValue == "2")
        {
            Stafflabour = "L";
        }
        else
        {
            Stafflabour = "0";
        }
        DataTable dtDip = new DataTable();
        dtDip = objdata.DropDownDepartment_category(Stafflabour);
        if (dtDip.Rows.Count > 1)
        {
            ddldepartment.DataSource = dtDip;
            ddldepartment.DataTextField = "DepartmentNm";
            ddldepartment.DataValueField = "DepartmentCd";
            ddldepartment.DataBind();
        }
        else
        {
            DataTable dt = new DataTable();
            ddldepartment.DataSource = dt;
            ddldepartment.DataBind();
        }
    }
    protected void ddldepartment_SelectedIndexChanged(object sender, EventArgs e)
    {
        panelError.Visible = false;
        Clear();
        DataTable dempty = new DataTable();
        GvOfficialProfile.DataSource = dempty;
        GvOfficialProfile.DataBind();
    }
    protected void btnclick_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        //panelError.Visible = true;
        if (ddlcategory.SelectedValue == "0")
        {
            //lblError.Text = "Select the Category";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select Category Staff or Labour');", true);
            //System.Windows.Forms.MessageBox.Show("Select the Category", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            ErrFlag = true;
        }
        else if (ddldepartment.SelectedValue == "0")
        {
            //lblError.Text = "Select the Department";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select Department');", true);
            //System.Windows.Forms.MessageBox.Show("Select the Department", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            ErrFlag = true;
        }
        if (!ErrFlag)
        {
            if (ddlcategory.SelectedValue == "1")
            {
                string Staff = "S";
                DataTable dtOPP = new DataTable();
                if (SessionAdmin == "1")
                {
                    dtOPP = objdata.EditOfficialProfileGridView(Staff, ddldepartment.SelectedValue, SessionCcode, SessionLcode);
                }
                else
                {
                    dtOPP = objdata.EditOfficialProfileGridView_user(Staff, ddldepartment.SelectedValue, SessionCcode, SessionLcode);
                }
                GvOfficialProfile.DataSource = dtOPP;
                GvOfficialProfile.DataBind();
                PanelLabour.Visible = false;
                PanelOthers.Visible = false;
                panelContract.Visible = false;
                if (dtOPP.Rows.Count > 0)
                {
                    PanelGrid.Visible = true;
                }
                else
                {
                    PanelGrid.Visible = false;
                }
            }
            else if (ddlcategory.SelectedValue == "2")
            {
                string Staff = "L";
                DataTable dtOPP = new DataTable();
                dtOPP = objdata.EditOfficialProfileGridView(Staff, ddldepartment.SelectedValue, SessionCcode, SessionLcode);
                GvOfficialProfile.DataSource = dtOPP;
                GvOfficialProfile.DataBind();
                PanelLabour.Visible = true;
                PanelOthers.Visible = true;
                panelContract.Visible = false;
                if (dtOPP.Rows.Count > 0)
                {
                    PanelGrid.Visible = true;
                }
                else
                {
                    PanelGrid.Visible = false;
                }
            }
            else
            {
                //lblError.Text = "Select the Category";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Category');", true);
                //System.Windows.Forms.MessageBox.Show("Select the Category", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            }
            txtempno.Text = "";
            lblempname1.Text = "";
            txtlabourtype.Text = "";
            Clear();
        }
    }
    protected void btnadd_Click(object sender, EventArgs e)
    {
        //panelError.Visible = true;

        bool ErrFlag = false;
        //if (txtempno.Text == "")
        //{
        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Employee ID');", true);
        //    ErrFlag = true;
        //}
        bool valuechk = false;

        if (ddlcategory.SelectedValue == "1")
        {
            PanelReportAuthrityName.Visible = false;
            PanelOthers.Visible = false;
        }
        else if (ddlcategory.SelectedValue == "2")
        {
            PanelReportAuthrityName.Visible = true;
            PanelOthers.Visible = true;
        }

        foreach (GridViewRow row in GvOfficialProfile.Rows)
        {
            RadioButton rb = (RadioButton)row.FindControl("rbtnstatus");

            if (rb.Checked)
            {
                valuechk = true;
                Label lblempno1 = (Label)GvOfficialProfile.Rows[row.RowIndex].FindControl("lblempno");
                Label lblempname2 = (Label)GvOfficialProfile.Rows[row.RowIndex].FindControl("lblEmpName");
                Label lbllabourtype = (Label)GvOfficialProfile.Rows[row.RowIndex].FindControl("lblcontracttype");
                txtempno.Text = lblempno1.Text;
                lblempname1.Text = lblempname2.Text;
                txtlabourtype.Text = lbllabourtype.Text;
                if (txtlabourtype.Text == "Contract")
                {
                    PanelContarctTypeLabel.Visible = false;
                    PanelContarctTypeRadio.Visible = false;
                    //PanelOthers.Visible = false;
                    panelContract.Visible = true;
                    DataTable dt = new DataTable();
                    dt = objdata.EditOfficialprofile_Load(txtempno.Text);
                    txtdobjoin.Enabled = true;
                    txtdobjoin.Text = dt.Rows[0]["Dateofjoining"].ToString();
                    ddlprobation.SelectedValue = dt.Rows[0]["ProbationPeriod"].ToString();
                    if (txtlabourtype.Text == "Contract")
                    {
                        panelContract.Visible = true;
                        ddlcontract.SelectedValue = dt.Rows[0]["ContractPeriod"].ToString();
                    }
                    else
                    {
                        panelContract.Visible = false;
                    }
                    //txtconfirmationdate.Text = dt.Rows[0]["Confirmationdate"].ToString();
                    txtpfnumber.Text = dt.Rows[0]["PFnumber"].ToString();
                    txtpannumber.Text = dt.Rows[0]["Pannumber"].ToString();
                    txtesic.Text = dt.Rows[0]["ESICnumber"].ToString();
                    ddlInsurance.SelectedValue = dt.Rows[0]["Insurancecompanyname"].ToString();
                    txtinsuranceno.Text = dt.Rows[0]["Insurancenumber"].ToString();
                    txtbankacno.Text = dt.Rows[0]["BankACCNo"].ToString();
                    ddlbankname.SelectedValue = dt.Rows[0]["BankName"].ToString();
                    txtbranch.Text = dt.Rows[0]["Branchname"].ToString();
                    txtauthoritynm.Text = dt.Rows[0]["ReportingAuthorityname"].ToString();
                    ddlFinancialPeriod.SelectedItem.Text = dt.Rows[0]["FinancialYear"].ToString();
                    txtUAN.Text = dt.Rows[0]["UAN"].ToString();
                    
                    //string basicsalary = dt.Rows[0]["BasicSalary"].ToString();

                    string sdla = dt.Rows[0]["Salarythrough"].ToString();
                    
                    string SalaryThr = sdla.Replace(" ", "");
                    if (SalaryThr == "1")
                    {
                        rbtnsalarythroug.SelectedValue = "1";
                    }
                    else if (SalaryThr == "2")
                    {
                        rbtnsalarythroug.SelectedValue = "2";
                    }
                    txtbasicSalary.Text = dt.Rows[0]["BasicSalary"].ToString();
                    string dd = dt.Rows[0]["Contracttype"].ToString();
                    string Contracttype = dd.Replace(" ", "");

                    string Eot = dt.Rows[0]["Eligibleforovertime"].ToString();
                    string Eligible = Eot.Replace(" ", "");

                    string Wag = dt.Rows[0]["Wagestype"].ToString();
                    string Wagestype = Wag.Replace(" ", "");

                    if (dt.Rows[0]["ElgibleESI"].ToString() == "1")
                    {
                        rbtnesi.SelectedValue = "1";
                    }
                    else if (dt.Rows[0]["ElgibleESI"].ToString() == "2")
                    {
                        rbtnesi.SelectedValue = "2";
                    }
                    if (Eligible == "1")
                    {
                        rbtnovertime.SelectedValue = "1";
                    }
                    else if (Eligible == "2")
                    {
                        rbtnovertime.SelectedValue = "2";
                    }
                    if (Wagestype == "1")
                    {
                        rbtnotherwages.SelectedValue = "1";
                    }
                    else if (Wagestype == "2")
                    {
                        rbtnotherwages.SelectedValue = "2";
                    }
                    else if (Wagestype == "3")
                    {
                        rbtnotherwages.SelectedValue = "3";
                    }
                    //if (Contracttype == "1")
                    //{
                    //    rbtncontract.SelectedValue = "1";
                    //}
                    //else if (Contracttype == "2")
                    //{
                    //    rbtncontract.SelectedValue = "2";
                    //}
                    if (dt.Rows[0]["EligiblePF"].ToString() == "1")
                    {
                        rbPF.SelectedValue = "1";
                    }
                    else
                    {
                        rbPF.SelectedValue = "2";
                    }

                    if (dt.Rows[0]["EligibleMess"].ToString() == "1")
                    {
                        rbtnMess.SelectedValue = "1";

                    }
                    else
                    {
                        rbtnMess.SelectedValue = "2";
                    }

                    if (dt.Rows[0]["EligibleIncentive"].ToString() == "1")
                    {
                        rbtnIncentive.SelectedValue = "1";
                    }
                    else
                    {
                        rbtnIncentive.SelectedValue = "2";
                    }

                    if (dt.Rows[0]["ProfileType"].ToString() == "1")
                    {
                        rbtnProfie.SelectedValue = "1";
                    }


                    else
                    {
                        rbtnProfie.SelectedValue = "2";
                        txtconfirmationdate.Text = dt.Rows[0]["Confirmationdate"].ToString();
                        rbtnProfie.Enabled = false;
                        txtconfirmationdate.Enabled = false;
                        ddlcontract.Enabled = false;
                        ddlprobation.Enabled = false;


                    }
                    if (dt.Rows[0]["PF_Type"].ToString() == "1")
                    {
                        chkpf.Checked = true;
                    }
                    else
                    {
                        chkpf.Checked = false;
                    }
                }
                else
                {
                    PanelContarctTypeLabel.Visible = false;
                    PanelContarctTypeRadio.Visible = false;
                    //PanelOthers.Visible = true;
                    panelContract.Visible = false;
                    DataTable dt = new DataTable();
                    dt = objdata.EditOfficialprofile_Load(txtempno.Text);
                    txtdobjoin.Text = dt.Rows[0]["Dateofjoining"].ToString();
                    ddlprobation.SelectedValue = dt.Rows[0]["ProbationPeriod"].ToString();
                    ddlcontract.SelectedValue = dt.Rows[0]["ContractPeriod"].ToString();
                    //txtconfirmationdate.Text = dt.Rows[0]["Confirmationdate"].ToString();
                    txtpfnumber.Text = dt.Rows[0]["PFnumber"].ToString();
                    txtpannumber.Text = dt.Rows[0]["Pannumber"].ToString();
                    txtesic.Text = dt.Rows[0]["ESICnumber"].ToString();
                    ddlInsurance.SelectedValue = dt.Rows[0]["Insurancecompanyname"].ToString();
                    txtinsuranceno.Text = dt.Rows[0]["Insurancenumber"].ToString();
                    txtbankacno.Text = dt.Rows[0]["BankACCNo"].ToString();
                    ddlbankname.SelectedValue = dt.Rows[0]["BankName"].ToString();
                    txtbranch.Text = dt.Rows[0]["Branchname"].ToString();
                    txtauthoritynm.Text = dt.Rows[0]["ReportingAuthorityname"].ToString();
                    ddlFinancialPeriod.SelectedItem.Text = dt.Rows[0]["FinancialYear"].ToString();
                    //string basicsalary = dt.Rows[0]["BasicSalary"].ToString();
                    txtbasicSalary.Text = dt.Rows[0]["BasicSalary"].ToString();

                    txtUAN.Text = dt.Rows[0]["UAN"].ToString();


                    string dd = dt.Rows[0]["Contracttype"].ToString();
                    string Contracttype = dd.Replace(" ", "");

                    string Eot = dt.Rows[0]["Eligibleforovertime"].ToString();
                    string Eligible = Eot.Replace(" ", "");

                    string Wag = dt.Rows[0]["Wagestype"].ToString();
                    string Wagestype = Wag.Replace(" ", "");
                    string sdla = dt.Rows[0]["Salarythrough"].ToString();
                    string SalaryThr = sdla.Replace(" ", "");

                    if (SalaryThr == "1")
                    {
                        rbtnsalarythroug.SelectedValue = "1";
                    }
                    else if (SalaryThr == "2")
                    {
                        rbtnsalarythroug.SelectedValue = "2";
                    }

                    if (dt.Rows[0]["ElgibleESI"].ToString() == "1")
                    {
                        rbtnesi.SelectedValue = "1";
                    }
                    else if (dt.Rows[0]["ElgibleESI"].ToString() == "2")
                    {
                        rbtnesi.SelectedValue = "2";
                    }
                    if (Eligible == "1")
                    {
                        rbtnovertime.SelectedValue = "1";
                    }
                    else if (Eligible == "2")
                    {
                        rbtnovertime.SelectedValue = "2";
                    }
                    if (dt.Rows[0]["EligibleMess"].ToString() == "1")
                    {
                        rbtnMess.SelectedValue = "1";

                    }
                    else
                    {
                        rbtnMess.SelectedValue = "2";
                    }
                    if (dt.Rows[0]["EligibleIncentive"].ToString() == "1")
                    {
                        rbtnIncentive.SelectedValue = "1";
                    }
                    else
                    {
                        rbtnIncentive.SelectedValue = "2";
                    }
                    if (Wagestype == "1")
                    {
                        rbtnotherwages.SelectedValue = "1";
                    }
                    else if (Wagestype == "2")
                    {
                        rbtnotherwages.SelectedValue = "2";
                    }
                    if (Contracttype == "1")
                    {
                        rbtncontract.SelectedValue = "1";
                    }
                    else if (Contracttype == "2")
                    {
                        rbtncontract.SelectedValue = "2";
                    }
                    if (dt.Rows[0]["EligiblePF"].ToString() == "1")
                    {
                        rbPF.SelectedValue = "1";
                    }
                    else
                    {
                        rbPF.SelectedValue = "2";
                    }
                    if (dt.Rows[0]["ProfileType"].ToString() == "1")
                    {
                        rbtnProfie.SelectedValue = "1";
                    }
                    else
                    {
                        rbtnProfie.SelectedValue = "2";
                        txtconfirmationdate.Text = dt.Rows[0]["Confirmationdate"].ToString();
                        rbtnProfie.Enabled = false;
                        txtconfirmationdate.Enabled = true;
                        ddlcontract.Enabled = false;
                        ddlprobation.Enabled = false;
                    }
                    if (dt.Rows[0]["PF_Type"].ToString() == "1")
                    {
                        chkpf.Checked = true;
                    }
                    else
                    {
                        chkpf.Checked = false;
                    }
                }

            }




        }
        if (valuechk == false)
        {
            //lblError.Text = "Select the Employee";

            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select the Employee');", true);
            //System.Windows.Forms.MessageBox.Show("Select the Employee", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
        }
        if (GvOfficialProfile.Rows.Count == 0)
        {
            //lblError.Text = "Select the Department";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select the Department');", true);
            //System.Windows.Forms.MessageBox.Show("Select the Department", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
        }
    }
    public void Clear()
    {
        panelError.Visible = false;
        panelsuccess.Visible = false;
        lbloutput.Text = "";
        lblError.Text = "";
        btnadd.Enabled = true;
        btnsave.Enabled = true;
        btnclick.Enabled = true;
        btncancel.Enabled = true;
        btnback.Enabled = true;
        txtdobjoin.Text = "";
        ddlInsurance.SelectedValue = "0";
        txtconfirmationdate.Text = "";
        txtpannumber.Text = "";
        txtesic.Text = "";
        txtpfnumber.Text = "";
        ddlprobation.SelectedValue = "0";
        txtinsuranceno.Text = "";
        txtempno.Text = "";
        txtauthoritynm.Text = "";
        lblempname1.Text = "";
        txtlabourtype.Text = "";
        txtbasicSalary.Text = "";
        rbtnProfie.Enabled = true;
        ddlprobation.Enabled = true;
        ddlcontract.Enabled = false;
        txtbranch.Text = "";
        //rbtnsalarythroug.SelectedValue = "";
        txtbankacno.Text = "";
        ddlbankname.SelectedValue = "0";
        txtUAN.Text = "";
        // txtovertime.Text = "";
        // rbtnmodeofpay.SelectedValue="0";
    }

    #region Web Form Designer generated code
    override protected void OnInit(EventArgs e)
    {

        rm = (System.Resources.ResourceManager)Application["rm"];
        Utility.SetThreadCulture();
        if (((SSSCultureInfo)System.Threading.Thread.CurrentThread.CurrentCulture).Encoding != null)
            Response.ContentEncoding = System.Text.Encoding.GetEncoding(((SSSCultureInfo)System.Threading.Thread.CurrentThread.CurrentCulture).Encoding);
        PayrollRegisterContentMeta = "text/html; charset=" + ((SSSCultureInfo)System.Threading.Thread.CurrentThread.CurrentCulture).Encoding;
        if (Application[Request.PhysicalPath] == null)
            Application.Add(Request.PhysicalPath, Response.ContentEncoding.WebName);
        InitializeComponent();
        this.Load += new System.EventHandler(this.Page_Load);
        // this.PreRender += new System.EventHandler(this.Page_PreRender);
        base.OnInit(e);
        if (!DBUtility.AuthorizeUser(new string[] { "1", "2", "6" }))
            ////string UId=Session["UserId"].ToString();
            ////if(UId !=null
            //if (Session["UserId"] == null)
            Response.Redirect(Settings.AccessDeniedPage);
    }


    private void InitializeComponent()
    {

    }
    #endregion

    protected void txtdobjoin_TextChanged(object sender, EventArgs e)
    {

    }
    protected void rbtnProfie_SelectedIndexChanged(object sender, EventArgs e)
    {
        //panelError.Visible = true;

        if (rbtnProfie.SelectedValue == "1")
        {
            txtconfirmationdate.Enabled = false;
            txtconfirmationdate.Text = "";
        }
        else if (rbtnProfie.SelectedValue == "2")
        {
            txtconfirmationdate.Enabled = true;
            txtconfirmationdate.Text = "";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('You Have selected the Confirmation Period....!');", true);
            //lblError.Text = "You Have Selected the Confirmation Period";
            //System.Windows.Forms.MessageBox.Show("You have Selected the Confirmation Period", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
        }
    }
    protected void rbPF_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (rbPF.SelectedValue == "1")
        {
            txtpfnumber.Enabled = true;
        }
        else if (rbPF.SelectedValue == "2")
        {
            txtpfnumber.Enabled = false;
        }
    }
    protected void txtconfirmationdate_TextChanged(object sender, EventArgs e)
    {

    }
    protected void rbtnesi_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (rbtnesi.SelectedValue == "1")
        {
            txtesic.Enabled = true;
        }
        else
        {
            txtesic.Enabled = false;
            txtesic.Text = "";
        }
    }
    protected void rbtnsalarythroug_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (rbtnsalarythroug.SelectedValue == "1")
        {
            txtbankacno.Enabled = false;
            ddlbankname.Enabled = false;
            txtbranch.Enabled = false;
            txtbranch.Text = "";
            txtbankacno.Text = "";
            MstBankDropDown();

        }
        else if (rbtnsalarythroug.SelectedValue == "2")
        {
            txtbankacno.Enabled = true;
            ddlbankname.Enabled = true;
            txtbranch.Enabled = true;
        }
    }
    protected void btnsave_Click(object sender, EventArgs e)
    {
        try
        {
            //panelError.Visible = true;
            bool isRegistered = false;
            bool ErrFlag = false;
            string pf_type = "";
            DateTime Dofjoin, ExpiryDate, ExpiredDate1 = Convert.ToDateTime("01-01-1999"), ContractStartingDate, ContractStartingDate1 = Convert.ToDateTime("01-01-1999"), ContractEndingDate, ContractEndingDate1 = Convert.ToDateTime("01-01-1999");
            Dofjoin = new DateTime();
            DateTime DofConfir = Convert.ToDateTime("01-01-1999");

            string ExpDate = "";
            DateTime dateofjoining, confirmationdate;


            if (txtempno.Text == "")
            {
                //lblError.Text = "Select the Employee and click Add";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select Radio Button in Display Record and click the Add button');", true);
                //System.Windows.Forms.MessageBox.Show("Select the Employee", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            else if (txtdobjoin.Text == "")
            {
                //lblError.Text = "Enter the Date of Joining";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Date of joining');", true);
                //System.Windows.Forms.MessageBox.Show("Enter the Date of Joining", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }

            else if (rbtnProfie.SelectedValue == "")
            {
                //lblError.Text = "Select the Profile Type";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Profile type');", true);
                //System.Windows.Forms.MessageBox.Show("Select the Profile type", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            else if (rbtnesi.SelectedValue == "")
            {
                //lblError.Text = "Select the Option Eligible for ESI";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the option Elgible for ESI');", true);
                //System.Windows.Forms.MessageBox.Show("Select the ESI", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            else if (rbtnsalarythroug.SelectedValue == "")
            {
                //lblError.Text = "Select the Salary Through";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select Salary Through');", true);
                //System.Windows.Forms.MessageBox.Show("Select the Salary Through", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            else if (rbPF.SelectedValue == "")
            {
                //lblError.Text = "Select the Eligible for PF";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select Eligible for PF');", true);
                //System.Windows.Forms.MessageBox.Show("Select the Eligible for PF", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            else if (rbtnesi.SelectedValue == "")
            {
                //lblError.Text = "Select the ESI";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the ESI');", true);
                //System.Windows.Forms.MessageBox.Show("Select the ESI", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            else if (txtdobjoin.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Date of Join');", true);
                ErrFlag = true;
            }
            else if (rbPF.SelectedValue == "1")
            {
                if (txtpfnumber.Text == "")
                {
                    //lblError.Text = "Enter the PF Number";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the PF number');", true);
                    //System.Windows.Forms.MessageBox.Show("Enter the PF Number", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    ErrFlag = true;

                }
            }
            else if (rbtnesi.SelectedValue == "1")
            {
                if (txtesic.Text == "")
                {
                    //lblError.Text = "Enter the ESI Value";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the ESI Value');", true);
                    //System.Windows.Forms.MessageBox.Show("Enter the ESI Value", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    ErrFlag = true;
                }
            }
            else if (rbtnsalarythroug.SelectedValue == "2")
            {
                if (ddlbankname.SelectedValue == "0")
                {
                    //lblError.Text = "Select the Bank Name";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select Bank Name');", true);
                    //System.Windows.Forms.MessageBox.Show("Select the Bank Name", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    ErrFlag = true;
                }
                else if (txtbankacno.Text == "")
                {
                    //lblError.Text = "Enter the Account No";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Bank Account Number');", true);
                    //System.Windows.Forms.MessageBox.Show("Enter the Bank Account Number", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    ErrFlag = true;
                }
                else if (txtbranch.Text == "")
                {
                    //lblError.Text = "Enter the Branch Name";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Branch Name');", true);
                    //System.Windows.Forms.MessageBox.Show("Enter the Branch Name", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    ErrFlag = true;
                }
            }
            if (rbtnProfie.SelectedValue == "2")
            {
                if (txtconfirmationdate.Text == "")
                {
                    //lblError.Text = "Enter the Confirmation Date";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the confirmation date');", true);
                    //System.Windows.Forms.MessageBox.Show("Enter the Confirmation Date", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    ErrFlag = true;
                }
            }

            if (ddlcategory.SelectedValue == "2")
            {
                //if (txtlabourtype.Text == "Contract")
                //{
                //    if (rbtncontract.SelectedValue == "")
                //    {
                //        //lblError.Text = "Select the Contract Type";
                //        ScriptManager.RegisterStartupScript(this, this.GetType(), "windows", "alert('Select Contract Type');", true);
                //        //System.Windows.Forms.MessageBox.Show("Select the Contract Type", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                //        ErrFlag = true;
                //    }
                //    else if (ddlcontract.SelectedValue == "0")
                //    {
                //        //lblError.Text = "Select the Contract period";
                //        ScriptManager.RegisterStartupScript(this, this.GetType(), "windows", "alert('Select Contract Period');", true);
                //        //System.Windows.Forms.MessageBox.Show("Select the Contract Period", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                //        ErrFlag = true;
                //    }
                //}
            }
            //    else if (txtlabourtype.Text == "Others")
            //    {
            //        if (rbtnotherwages.SelectedValue == "")
            //        {
            //            //lblError.Text = "Select the Wages type";
            //            ScriptManager.RegisterStartupScript(this, this.GetType(), "windows", "alert('Select Wages Type');", true);
            //            //System.Windows.Forms.MessageBox.Show("Select Wages Type", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            //            ErrFlag = true;
            //        }
            //    }
            //}
            if (rbtnProfie.SelectedValue == "2")
            {
                if (txtconfirmationdate.Text == "")
                {
                    //lblError.Text = "Enter the Confirmation Date";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the confirmation date');", true);
                    //System.Windows.Forms.MessageBox.Show("Enter the Confirmation Date", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    ErrFlag = true;
                }
            }
            if (!ErrFlag)
            {
                if (rbtnProfie.SelectedValue == "2")
                {
                    //dateofjoining = DateTime.ParseExact(txtdobjoin.Text, "dd-MM-yyyy", culterInfo.DateTimeFormat);
                    //confirmationdate = DateTime.ParseExact(txtconfirmationdate.Text, "dd-MM-yyyy", culterInfo.DateTimeFormat);
                    //if (dateofjoining > confirmationdate)
                    //{
                    //    //lblError.Text = "Enter the Date of Joining Properly";
                    //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Enter the date of joining Properly');", true);
                    //    //System.Windows.Forms.MessageBox.Show("Enter the date of Joinning Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    //    ErrFlag = true;
                    //}
                }
            }


            if (!ErrFlag)
            {
                #region Probation Period For Staff
                if (ddlcategory.SelectedValue == "1")
                {
                    #region Probation Period Expired

                    objOff.Dateofjoining = txtdobjoin.Text;
                    Dofjoin = DateTime.ParseExact(objOff.Dateofjoining, "dd-MM-yyyy", null);
                    objOff.Probationperiod = ddlprobation.SelectedItem.Text;
                    if (ddlprobation.SelectedItem.Text == "3 Months")
                    {
                        ExpiryDate = Dofjoin.AddMonths(3);
                        ExpiredDate1 = ExpiryDate.Date;
                    }
                    else if (ddlprobation.SelectedItem.Text == "6 Months")
                    {

                        ExpiryDate = Dofjoin.AddMonths(6);
                        ExpiredDate1 = ExpiryDate.Date;

                    }
                    else if (ddlprobation.SelectedItem.Text == "1 Year")
                    {
                        ExpiryDate = Dofjoin.AddMonths(12);
                        ExpiredDate1 = ExpiryDate.Date;
                    }
                    else if (ddlprobation.SelectedItem.Text == "2 Year")
                    {
                        ExpiryDate = Dofjoin.AddMonths(24);
                        ExpiredDate1 = ExpiryDate.Date;
                    }
                    else if (ddlprobation.SelectedItem.Text == "3 Year")
                    {
                        ExpiryDate = Dofjoin.AddMonths(36);
                        ExpiredDate1 = ExpiryDate.Date;
                    }
                    else if (ddlprobation.SelectedItem.Text == "5 Year")
                    {
                        ExpiryDate = Dofjoin.AddMonths(60);
                        ExpiredDate1 = ExpiryDate.Date;
                    }
                    else
                    {
                        ExpiryDate = Dofjoin;
                        ExpiredDate1 = ExpiryDate.Date;
                    }
                    //if (ddlcontract.SelectedItem.Text == "3 Months")
                    //{
                    //    ContractStartingDate = ExpiredDate1.AddDays(1);
                    //    ContractStartingDate1 = ContractStartingDate.Date;
                    //    ContractEndingDate = ContractStartingDate1.AddMonths(3);
                    //    ContractEndingDate1 = ContractEndingDate.Date;
                    //}
                    //else if (ddlcontract.SelectedItem.Text == "6 Months")
                    //{
                    //    ContractStartingDate = ExpiredDate1.AddDays(1);
                    //    ContractStartingDate1 = ContractStartingDate.Date;
                    //    ContractEndingDate = ContractStartingDate1.AddMonths(6);
                    //    ContractEndingDate1 = ContractEndingDate.Date;
                    //}
                    //else if (ddlcontract.SelectedItem.Text == "1 Year")
                    //{
                    //    ContractStartingDate = ExpiredDate1.AddDays(1);
                    //    ContractStartingDate1 = ContractStartingDate.Date;
                    //    ContractEndingDate = ContractStartingDate1.AddMonths(12);
                    //    ContractEndingDate1 = ContractEndingDate.Date;
                    //}
                    //else if (ddlcontract.SelectedItem.Text == "2 Year")
                    //{
                    //    ContractStartingDate = ExpiredDate1.AddDays(1);
                    //    ContractStartingDate1 = ContractStartingDate.Date;
                    //    ContractEndingDate = ContractStartingDate1.AddMonths(24);
                    //    ContractEndingDate1 = ContractEndingDate.Date;
                    //}
                    //else if (ddlcontract.SelectedItem.Text == "3 Year")
                    //{
                    //    ContractStartingDate = ExpiredDate1.AddDays(1);
                    //    ContractStartingDate1 = ContractStartingDate.Date;
                    //    ContractEndingDate = ContractStartingDate1.AddMonths(36);
                    //    ContractEndingDate1 = ContractEndingDate.Date;
                    //}
                    //else if (ddlcontract.SelectedItem.Text == "5 Year")
                    //{
                    //    ContractStartingDate = ExpiredDate1.AddDays(1);
                    //    ContractStartingDate1 = ContractStartingDate.Date;
                    //    ContractEndingDate = ContractStartingDate1.AddMonths(60);
                    //    ContractEndingDate1 = ContractEndingDate.Date;
                    //}

                    //else
                    //{
                    //    ContractStartingDate = ExpiredDate1;
                    //    ContractStartingDate1 = ContractStartingDate.Date;
                    //    ContractEndingDate = ContractStartingDate1;
                    //    ContractEndingDate1 = ContractEndingDate.Date;
                    //}

                }


                    #endregion
                #endregion
                else if (ddlcategory.SelectedValue == "2")
                {
                    if (txtlabourtype.Text == "Contract")
                    {
                        //if (rbtncontract.SelectedValue == "1")
                        //{
                        //    objOff.Dateofjoining = txtdobjoin.Text;
                        //    Dofjoin = DateTime.ParseExact(objOff.Dateofjoining, "dd-MM-yyyy", null);
                        //    objOff.Probationperiod = ddlprobation.SelectedItem.Text;
                            
                        //    if (ddlcontract.SelectedItem.Text == "3 Months")
                        //    {
                        //        ContractStartingDate = ExpiredDate1.AddDays(1);
                        //        ContractStartingDate1 = ContractStartingDate.Date;
                        //        ContractEndingDate = ContractStartingDate1.AddMonths(3);
                        //        ContractEndingDate1 = ContractEndingDate.Date;
                        //    }
                        //    else if (ddlcontract.SelectedItem.Text == "6 Months")
                        //    {
                        //        ContractStartingDate = ExpiredDate1.AddDays(1);
                        //        ContractStartingDate1 = ContractStartingDate.Date;
                        //        ContractEndingDate = ContractStartingDate1.AddMonths(6);
                        //        ContractEndingDate1 = ContractEndingDate.Date;
                        //    }
                        //    else if (ddlcontract.SelectedItem.Text == "1 Year")
                        //    {
                        //        ContractStartingDate = ExpiredDate1.AddDays(1);
                        //        ContractStartingDate1 = ContractStartingDate.Date;
                        //        ContractEndingDate = ContractStartingDate1.AddMonths(12);
                        //        ContractEndingDate1 = ContractEndingDate.Date;
                        //    }
                        //    else if (ddlcontract.SelectedItem.Text == "2 Year")
                        //    {
                        //        ContractStartingDate = ExpiredDate1.AddDays(1);
                        //        ContractStartingDate1 = ContractStartingDate.Date;
                        //        ContractEndingDate = ContractStartingDate1.AddMonths(24);
                        //        ContractEndingDate1 = ContractEndingDate.Date;
                        //    }
                        //    else if (ddlcontract.SelectedItem.Text == "3 Year")
                        //    {
                        //        ContractStartingDate = ExpiredDate1.AddDays(1);
                        //        ContractStartingDate1 = ContractStartingDate.Date;
                        //        ContractEndingDate = ContractStartingDate1.AddMonths(36);
                        //        ContractEndingDate1 = ContractEndingDate.Date;
                        //    }
                        //    else if (ddlcontract.SelectedItem.Text == "5 Year")
                        //    {
                        //        ContractStartingDate = ExpiredDate1.AddDays(1);
                        //        ContractStartingDate1 = ContractStartingDate.Date;
                        //        ContractEndingDate = ContractStartingDate1.AddMonths(60);
                        //        ContractEndingDate1 = ContractEndingDate.Date;
                        //    }
                        //    else
                        //    {
                        //        ContractStartingDate = ExpiredDate1;
                        //        ContractStartingDate1 = ContractStartingDate.Date;
                        //        ContractEndingDate = ContractStartingDate1;
                        //        ContractEndingDate1 = ContractEndingDate.Date;
                        //    }
                        //}
                        //else if (rbtncontract.SelectedValue == "2")
                        //{
                        //    objOff.Dateofjoining = txtdobjoin.Text;
                        //    Dofjoin = DateTime.ParseExact(objOff.Dateofjoining, "dd-MM-yyyy", null);
                        //    if (ddlprobation.SelectedItem.Text == "3 Months")
                        //    {
                        //        ExpiryDate = Dofjoin.AddMonths(3);
                        //        ExpiredDate1 = ExpiryDate.Date;
                        //    }
                        //    else if (ddlprobation.SelectedItem.Text == "6 Months")
                        //    {

                        //        ExpiryDate = Dofjoin.AddMonths(6);
                        //        ExpiredDate1 = ExpiryDate.Date;
                        //    }
                        //    else if (ddlprobation.SelectedItem.Text == "1 Year")
                        //    {
                        //        ExpiryDate = Dofjoin.AddMonths(12);
                        //        ExpiredDate1 = ExpiryDate.Date;
                        //    }
                        //    else if (ddlprobation.SelectedItem.Text == "2 Year")
                        //    {
                        //        ExpiryDate = Dofjoin.AddMonths(24);
                        //        ExpiredDate1 = ExpiryDate.Date;
                        //    }
                        //    else if (ddlprobation.SelectedItem.Text == "3 Year")
                        //    {
                        //        ExpiryDate = Dofjoin.AddMonths(36);
                        //        ExpiredDate1 = ExpiryDate.Date;
                        //    }
                        //    else if (ddlprobation.SelectedItem.Text == "5 Year")
                        //    {
                        //        ExpiryDate = Dofjoin.AddMonths(60);
                        //        ExpiredDate1 = ExpiryDate.Date;
                        //    }
                        //    else
                        //    {
                        //        ExpiryDate = Dofjoin;
                        //        ExpiredDate1 = ExpiryDate.Date;
                        //    }
                        //    if (ddlcontract.SelectedItem.Text == "3 Months")
                        //    {
                        //        ContractStartingDate = ExpiredDate1.AddDays(1);
                        //        ContractStartingDate1 = ContractStartingDate.Date;
                        //        ContractEndingDate = ContractStartingDate1.AddMonths(3);
                        //        ContractEndingDate1 = ContractEndingDate.Date;
                        //    }
                        //    else if (ddlcontract.SelectedItem.Text == "6 Months")
                        //    {
                        //        ContractStartingDate = ExpiredDate1.AddDays(1);
                        //        ContractStartingDate1 = ContractStartingDate.Date;
                        //        ContractEndingDate = ContractStartingDate1.AddMonths(6);
                        //        ContractEndingDate1 = ContractEndingDate.Date;
                        //    }
                        //    else if (ddlcontract.SelectedItem.Text == "1 Year")
                        //    {
                        //        ContractStartingDate = ExpiredDate1.AddDays(1);
                        //        ContractStartingDate1 = ContractStartingDate.Date;
                        //        ContractEndingDate = ContractStartingDate1.AddMonths(12);
                        //        ContractEndingDate1 = ContractEndingDate.Date;
                        //    }
                        //    else if (ddlcontract.SelectedItem.Text == "2 Year")
                        //    {
                        //        ContractStartingDate = ExpiredDate1.AddDays(1);
                        //        ContractStartingDate1 = ContractStartingDate.Date;
                        //        ContractEndingDate = ContractStartingDate1.AddMonths(24);
                        //        ContractEndingDate1 = ContractEndingDate.Date;
                        //    }
                        //    else if (ddlcontract.SelectedItem.Text == "3 Year")
                        //    {
                        //        ContractStartingDate = ExpiredDate1.AddDays(1);
                        //        ContractStartingDate1 = ContractStartingDate.Date;
                        //        ContractEndingDate = ContractStartingDate1.AddMonths(36);
                        //        ContractEndingDate1 = ContractEndingDate.Date;
                        //    }
                        //    else if (ddlcontract.SelectedItem.Text == "5 Year")
                        //    {
                        //        ContractStartingDate = ExpiredDate1.AddDays(1);
                        //        ContractStartingDate1 = ContractStartingDate.Date;
                        //        ContractEndingDate = ContractStartingDate1.AddMonths(60);
                        //        ContractEndingDate1 = ContractEndingDate.Date;
                        //    }
                        //    else
                        //    {
                        //        ContractStartingDate = ExpiredDate1;
                        //        ContractStartingDate1 = ContractStartingDate.Date;
                        //        ContractEndingDate = ContractStartingDate1;
                        //        ContractEndingDate1 = ContractEndingDate.Date;
                        //    }
                        //}

                    }
                    else
                    {
                        objOff.Dateofjoining = txtdobjoin.Text;

                        Dofjoin = DateTime.ParseExact(objOff.Dateofjoining, "dd-MM-yyyy", null);
                        objOff.Probationperiod = ddlprobation.SelectedItem.Text;
                        if (ddlprobation.SelectedItem.Text == "3 Months")
                        {
                            ExpiryDate = Dofjoin.AddMonths(3);
                            ExpiredDate1 = ExpiryDate.Date;
                        }
                        else if (ddlprobation.SelectedItem.Text == "6 Months")
                        {

                            ExpiryDate = Dofjoin.AddMonths(6);
                            ExpiredDate1 = ExpiryDate.Date;

                        }
                        else if (ddlprobation.SelectedItem.Text == "1 Year")
                        {
                            ExpiryDate = Dofjoin.AddMonths(12);
                            ExpiredDate1 = ExpiryDate.Date;
                        }
                        else if (ddlprobation.SelectedItem.Text == "2 Year")
                        {
                            ExpiryDate = Dofjoin.AddMonths(24);
                            ExpiredDate1 = ExpiryDate.Date;
                        }
                        else if (ddlprobation.SelectedItem.Text == "3 Year")
                        {
                            ExpiryDate = Dofjoin.AddMonths(36);
                            ExpiredDate1 = ExpiryDate.Date;
                        }
                        else if (ddlprobation.SelectedItem.Text == "5 Year")
                        {
                            ExpiryDate = Dofjoin.AddMonths(60);
                            ExpiredDate1 = ExpiryDate.Date;
                        }
                        else
                        {
                            ExpiryDate = Dofjoin;
                            ExpiredDate1 = ExpiryDate.Date;
                        }
                        //if (ddlcontract.SelectedItem.Text == "3 Months")
                        //{
                        //    ContractStartingDate = ExpiredDate1.AddDays(1);
                        //    ContractStartingDate1 = ContractStartingDate.Date;
                        //    ContractEndingDate = ContractStartingDate1.AddMonths(3);
                        //    ContractEndingDate1 = ContractEndingDate.Date;
                        //}
                        //else if (ddlcontract.SelectedItem.Text == "6 Months")
                        //{
                        //    ContractStartingDate = ExpiredDate1.AddDays(1);
                        //    ContractStartingDate1 = ContractStartingDate.Date;
                        //    ContractEndingDate = ContractStartingDate1.AddMonths(6);
                        //    ContractEndingDate1 = ContractEndingDate.Date;
                        //}
                        //else if (ddlcontract.SelectedItem.Text == "1 Year")
                        //{
                        //    ContractStartingDate = ExpiredDate1.AddDays(1);
                        //    ContractStartingDate1 = ContractStartingDate.Date;
                        //    ContractEndingDate = ContractStartingDate1.AddMonths(12);
                        //    ContractEndingDate1 = ContractEndingDate.Date;
                        //}
                        //else if (ddlcontract.SelectedItem.Text == "2 Year")
                        //{
                        //    ContractStartingDate = ExpiredDate1.AddDays(1);
                        //    ContractStartingDate1 = ContractStartingDate.Date;
                        //    ContractEndingDate = ContractStartingDate1.AddMonths(24);
                        //    ContractEndingDate1 = ContractEndingDate.Date;
                        //}
                        //else if (ddlcontract.SelectedItem.Text == "3 Year")
                        //{
                        //    ContractStartingDate = ExpiredDate1.AddDays(1);
                        //    ContractStartingDate1 = ContractStartingDate.Date;
                        //    ContractEndingDate = ContractStartingDate1.AddMonths(36);
                        //    ContractEndingDate1 = ContractEndingDate.Date;
                        //}
                        //else if (ddlcontract.SelectedItem.Text == "5 Year")
                        //{
                        //    ContractStartingDate = ExpiredDate1.AddDays(1);
                        //    ContractStartingDate1 = ContractStartingDate.Date;
                        //    ContractEndingDate = ContractStartingDate1.AddMonths(60);
                        //    ContractEndingDate1 = ContractEndingDate.Date;
                        //}
                        //else
                        //{
                        //    ContractStartingDate = ExpiredDate1;
                        //    ContractStartingDate1 = ContractStartingDate.Date;
                        //    ContractEndingDate = ContractStartingDate1;
                        //    ContractEndingDate1 = ContractEndingDate.Date;
                        //}
                        //PanelOthers.Visible = true;
                    }
                }
                if (chkpf.Checked == true)
                {
                    pf_type = "1";
                }
                else
                {
                    pf_type = "0";
                }
                objOff.Dateofjoining = txtdobjoin.Text;
                Dofjoin = DateTime.ParseExact(objOff.Dateofjoining, "dd-MM-yyyy", null);
                objOff.Probationperiod = ddlprobation.SelectedValue;
                objOff.Confirmationdate = txtconfirmationdate.Text;

                //DofConfir = DateTime.ParseExact(objOff.Confirmationdate, "dd/MM/yyyy", null);
                if (rbtnProfie.SelectedValue == "2")
                {
                    DofConfir = Convert.ToDateTime(objOff.Confirmationdate);
                }
                //DofConfir = Convert.ToDateTime(objOff.Confirmationdate);
                objOff.Pannumber = txtpannumber.Text;
                objOff.ESICnumber = txtesic.Text;
                objOff.PFnumber = txtpfnumber.Text;
                objOff.InsurancecompanyName = ddlInsurance.SelectedValue;
                objOff.Insurancenumber = txtinsuranceno.Text;
                if (ddlcategory.SelectedValue == "1")
                {
                    objOff.Eligibleforovertime = "2";
                }
                else
                {
                    objOff.Eligibleforovertime = rbtnovertime.SelectedValue;
                }
                //objOff.modeofpayment = rbtnmodeofpay.SelectedValue;
                objOff.ReportingAuthorityName = txtauthoritynm.Text;
                if (ddlcategory.SelectedValue == "1")
                {
                    if (txtlabourtype.Text == "Contract")
                    {
                        objOff.ContractType = "2";
                    }
                    else
                    {
                        objOff.ContractType = rbtncontract.SelectedValue;
                    }
                }
                else
                {

                    objOff.ContractType = rbtncontract.SelectedValue;
                }
                objOff.LabourType = txtlabourtype.Text;
                objOff.EligblePF = rbPF.SelectedValue;
                if ((ddlcategory.SelectedValue == "2") && (PanelOthers.Visible == true))
                {
                    objOff.WagesType = rbtnotherwages.SelectedValue;
                }
                else
                {
                    objOff.WagesType = "2";
                }
                objOff.contractperiod = ddlcontract.SelectedValue;
                txtbasicSalary.Text = "0";
                objOff.BasicSalary = txtbasicSalary.Text;
                objOff.EligibleESI = rbtnesi.SelectedValue;
                objOff.Financialperiod = ddlFinancialPeriod.SelectedValue;
                objOff.ProfileType = rbtnProfie.SelectedValue;
                objOff.modeofpayment = rbtnsalarythroug.SelectedValue;
                objOff.BankACno = txtbankacno.Text;
                objOff.BankName = ddlbankname.SelectedValue;
                objOff.Branchname = txtbranch.Text;
                objOff.EligblePF = rbPF.SelectedValue;
                objOff.Ccode = SessionCcode;
                objOff.Lcode = SessionLcode;
                objOff.UAN = txtUAN.Text;
                if (txtlabourtype.Text == "Contract")
                {
                    DataTable dt_off = new DataTable();
                    dt_off = objdata.Official_contract(ddlcontract.SelectedValue);
                    if (dt_off.Rows.Count > 0)
                    {
                        objOff.EmpNo = txtempno.Text;
                        objOff.Contractname = ddlcontract.SelectedValue;

                        if (dt_off.Rows[0]["ContractType"].ToString() == "1")
                        {
                            objOff.ContractType = "1";
                            objOff.yr = dt_off.Rows[0]["ContractYear"].ToString();
                            objOff.YrDays = dt_off.Rows[0]["YrDays"].ToString();
                            objOff.PlannedDays = dt_off.Rows[0]["PlannedDays"].ToString();
                            objOff.BalanceDays = dt_off.Rows[0]["PlannedDays"].ToString();
                            objOff.Days1 = "0";
                            objOff.Months = "0";
                            objOff.FixedDays = "0";

                        }
                        else if (dt_off.Rows[0]["ContractType"].ToString() == "2")
                        {
                            objOff.ContractType = "2";
                            objOff.yr = "0";
                            objOff.Days1 = "0";
                            objOff.YrDays = "0";
                            objOff.PlannedDays = dt_off.Rows[0]["PlannedDays"].ToString();
                            objOff.BalanceDays = dt_off.Rows[0]["PlannedDays"].ToString();
                            objOff.Months = dt_off.Rows[0]["ContractMonth"].ToString();
                            objOff.FixedDays = dt_off.Rows[0]["FixedDays"].ToString();
                        }
                        else if (dt_off.Rows[0]["ContractType"].ToString() == "3")
                        {
                            objOff.ContractType = "3";
                            objOff.yr = "0";
                            objOff.YrDays = "0";
                            objOff.PlannedDays = dt_off.Rows[0]["PlannedDays"].ToString();
                            objOff.BalanceDays = dt_off.Rows[0]["PlannedDays"].ToString();
                            objOff.Days1 = dt_off.Rows[0]["ContractDays"].ToString();
                            objOff.Months = "0";
                            objOff.FixedDays = "0";
                        }

                    }
                }
                else
                {
                }
                isRegistered = true;
            }

            try
            {
                if (isRegistered)
                {
                    objdata.UpdateOfficalProfile(objOff, txtempno.Text, Dofjoin, DofConfir, ExpiredDate1, ContractStartingDate1, ContractEndingDate1, pf_type);

                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Your Official Profile Updated Successfully...!');", true);
                    //System.Windows.Forms.MessageBox.Show("Your Official Profile Updated Successfully", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    Clear();
                    OfficialProfileDisplay();
                    //panelError.Visible = false;
                    ////panelsuccess.Visible = true;
                    ////lbloutput.Text = "Updated Successfully";
                    //btnadd.Enabled = false;
                    //btnsave.Enabled = false;
                    //btnclick.Enabled = false;
                    //btncancel.Enabled = false;
                    //btnback.Enabled = false;
                    //btnok.Focus();
                }

            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Server Error Contact Admin....!');", true);
                //System.Windows.Forms.MessageBox.Show("Server Error, Contact Admin", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                //lblError.Text = "Server Error - Contact Admin";
                ErrFlag = true;
            }
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Please Contact Admin....!');", true);
            //System.Windows.Forms.MessageBox.Show("Please Contact Admin", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            //lblError.Text = "Server Error - Contact Admin";
        }


    }
    protected void btncancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("EditOfficialProfile.aspx");
    }
    protected void btnEdit_Click(object sender, EventArgs e)
    {
        Response.Redirect("OfficalProfileDetails.aspx");
    }
    protected void btnok_Click(object sender, EventArgs e)
    {
        Clear();
        OfficialProfileDisplay();


    }
    protected void btnEmp_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptEmpDownload.aspx");
    }
    protected void btnatt_Click(object sender, EventArgs e)
    {
        Response.Redirect("AttenanceDownload.aspx");
    }
    protected void btnLeave_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptLeaveSample.aspx");
    }
    protected void btnOT_Click(object sender, EventArgs e)
    {
        Response.Redirect("OTDownload.aspx");
    }
    protected void btncontract_Click(object sender, EventArgs e)
    {
        Response.Redirect("ContractRPT.aspx");
    }
    protected void btnSal_Click(object sender, EventArgs e)
    {
        Response.Redirect("FrmDeductionDownload.aspx");
    }
}
