﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data;
using Payroll;
using Payroll.Data;
using Payroll.Configuration;
using System.IO;
using System.Drawing;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html;
using iTextSharp.text.html.simpleparser;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class Incentive : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    string SessionAdmin;
    string Stafflabour;
    //DateTime FromDate;
    //DateTime ToDate;
    string SessionCcode;
    string SessionLcode;
    //string staffLabour;
    string NetWork;
    string NetAmt;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionAdmin = Session["Isadmin"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        string ss = Session["UserId"].ToString();
        lblComany.Text = SessionCcode + " - " + SessionLcode;
        lblusername.Text = Session["Usernmdisplay"].ToString();
        if (!IsPostBack)
        {
            category();
            Months_load();
            int currentYear = Utility.GetFinancialYear;
            for (int i = 0; i < 10; i++)
            {
                ddlfinance.Items.Add(new System.Web.UI.WebControls.ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                //  ddlShowYear.Items.Add(new ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                currentYear = currentYear - 1;
            }
        }
    }
    public void Months_load()
    {
        DataTable dt = new DataTable();
        dt = objdata.months_load();
        ddlMonths.DataSource = dt;
        ddlMonths.DataTextField = "Months";
        ddlMonths.DataValueField = "Months";
        ddlMonths.DataBind();
    }
    public void category()
    {
        DataTable dtcate = new DataTable();
        dtcate = objdata.DropDownCategory();
        ddlcategory.DataSource = dtcate;
        ddlcategory.DataTextField = "CategoryName";
        ddlcategory.DataValueField = "CategoryCd";
        ddlcategory.DataBind();
    }
    protected void btnClick_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        DataTable dtempty = new DataTable();
        gvload.DataSource = dtempty;
        gvload.DataBind();
        if (ddlcategory.SelectedValue == "0")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Category...!');", true);
            ErrFlag = true;
        }
        else if (ddldepartment.SelectedValue == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Department...!');", true);
            ErrFlag = true;
        }
        //else if (ddldepartment.SelectedValue == "0")
        //{
        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Department...!');", true);
        //    ErrFlag = true;
        //}
        else if (txtAmt.Text.Trim() == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Amount...!');", true);
            ErrFlag = true;
        }
        if (!ErrFlag)
        {
            if (ddlcategory.SelectedValue == "1")
            {
                Stafflabour = "S";
            }
            else if (ddlcategory.SelectedValue == "2")
            {
                Stafflabour = "L";
            }

            DataTable dt = new DataTable();
            dt = objdata.IncentiveAmt(ddlfinance.SelectedValue, ddlMonths.SelectedValue, Stafflabour, SessionAdmin, SessionCcode, SessionLcode, ddldepartment.SelectedValue);
            gvload.DataSource = dt;
            gvload.DataBind();
            if (dt.Rows.Count > 0)
            {
                foreach (GridViewRow gvrow in gvload.Rows)
                {
                    Label Days = (Label)gvrow.FindControl("lblDays");
                    Label Amount = (Label)gvrow.FindControl("lblAmount");
                    Amount.Text = (Convert.ToDecimal(txtAmt.Text) * Convert.ToDecimal(Days.Text)).ToString();
                }
            }
        }
    }
    protected void ddlcategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        string staffLabour_temp = "";
        DataTable dtempty = new DataTable();
        gvload.DataSource = dtempty;
        gvload.DataBind();
        
        ddldepartment.DataSource = dtempty;
        ddldepartment.DataBind();
        if (ddlcategory.SelectedValue == "1")
        {
            staffLabour_temp = "S";
        }
        else if (ddlcategory.SelectedValue == "2")
        {
            staffLabour_temp = "L";
        }
        else
        {
            staffLabour_temp = "0";
        }
        DataTable dtDip = new DataTable();
        dtDip = objdata.DropDownDepartment_category(staffLabour_temp);
        if (dtDip.Rows.Count > 1)
        {
            ddldepartment.DataSource = dtDip;
            ddldepartment.DataTextField = "DepartmentNm";
            ddldepartment.DataValueField = "DepartmentCd";
            ddldepartment.DataBind();
        }
        else
        {
            DataTable dt = new DataTable();
            ddldepartment.DataSource = dt;
            ddldepartment.DataBind();
        }
    }
    protected void ddldepartment_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable dtempty = new DataTable();
        gvload.DataSource = dtempty;
        gvload.DataBind();
    }
    protected void btnEmp_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptEmpDownload.aspx");
    }
    protected void btnatt_Click(object sender, EventArgs e)
    {
        Response.Redirect("AttenanceDownload.aspx");
    }
    protected void btnLeave_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptLeaveSample.aspx");
    }
    protected void btnOT_Click(object sender, EventArgs e)
    {
        Response.Redirect("OTDownload.aspx");
    }
    protected void ddlfinance_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable dtempty = new DataTable();
        gvload.DataSource = dtempty;
        gvload.DataBind();
    }
    protected void ddlMonths_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable dtempty = new DataTable();
        gvload.DataSource = dtempty;
        gvload.DataBind();
    }
    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }
    protected void btnexport_Click(object sender, EventArgs e)
    {
        int YR = 0;
            if (ddlMonths.SelectedValue == "January")
            {
                YR = Convert.ToInt32(ddlfinance.SelectedValue);
                YR = YR + 1;
            }
            else if (ddlMonths.SelectedValue == "February")
            {
                YR = Convert.ToInt32(ddlfinance.SelectedValue);
                YR = YR + 1;
            }
            else if (ddlMonths.SelectedValue == "March")
            {
                YR = Convert.ToInt32(ddlfinance.SelectedValue);
                YR = YR + 1;
            }
            else
            {
                YR = Convert.ToInt32(ddlfinance.SelectedValue);
            }
        if (gvload.Rows.Count > 0)
        {
            NetWork = "0";
            NetAmt = "0";
            //for (int i = 0; i < gvload.Rows.Count; i++)
            //{
            //    NetWork = (Convert.ToDecimal(NetWork) + Convert.ToDecimal(gvload.Rows[i]["lblDays"].ToString())).ToString();
            //    NetAmt = (Convert.ToDecimal(NetAmt) + Convert.ToDecimal(gvload.Rows[i]["lblAmount"].ToString())).ToString();
            //}
            foreach (GridViewRow gvrowSave in gvload.Rows)
            {
                Label lbltotalwork = (Label)gvrowSave.FindControl("lblDays");
                NetWork = (Convert.ToDecimal(NetWork) + Convert.ToDecimal(lbltotalwork.Text)).ToString();
                Label lblAmt = (Label)gvrowSave.FindControl("lblAmount");
                NetAmt = (Convert.ToDecimal(NetAmt) + Convert.ToDecimal(lblAmt.Text)).ToString();
            }
                //GridViewExportUtil.Export("Reportsearch.pdf", gvreportserarch);
                string attachment = "attachment; filename=Incentive.xls";
                Response.ClearContent();
                Response.AddHeader("content-disposition", attachment);
                Response.ContentType = "application/ms-excel";
                StringWriter stw = new StringWriter();
                HtmlTextWriter htextw = new HtmlTextWriter(stw);
                gvload.RenderControl(htextw);
                Response.Write("<table>");
                Response.Write("<tr align='Center'>");
                Response.Write("<td colspan='10'>");
                Response.Write(ddlMonths.SelectedValue + "-" + YR);
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("</table>");
                Response.Write(stw.ToString());
                Response.Write("<table>");
                Response.Write("<tr align='Center'>");
                Response.Write("<td>");
                Response.Write("</td>");
                Response.Write("<td>");
                Response.Write("</td>");
                Response.Write("<td>");
                Response.Write("</td>");
                Response.Write("<td>");
                Response.Write("Grand Total");
                Response.Write("</td>");
                Response.Write("<td>");
                Response.Write("" + NetWork + "");
                Response.Write("</td>");
                Response.Write("<td>");
                Response.Write("" + NetAmt + "");
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("</table>");
                Response.End();
                Response.Clear();
            
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('No Data Found...!');", true);
        }
    }
    protected void txtAmt_TextChanged(object sender, EventArgs e)
    {
        DataTable dtempty = new DataTable();
        gvload.DataSource = dtempty;
        gvload.DataBind();
    }
    protected void btnclr_Click(object sender, EventArgs e)
    {
        DataTable dtempty = new DataTable();
        gvload.DataSource = dtempty;
        gvload.DataBind();
        txtAmt.Text = "";
        ddldepartment.DataSource = dtempty;
        ddldepartment.DataBind();
       
    }
    protected void btnSal_Click(object sender, EventArgs e)
    {
        Response.Redirect("FrmDeductionDownload.aspx");
    }
}
