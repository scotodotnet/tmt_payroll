﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;
using Payroll;
using System.Drawing;
using iTextSharp.text.pdf;
using iTextSharp.text.html;
using iTextSharp.text.html.simpleparser;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class ESIForm7_1 : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    string SessionAdmin;
    string Stafflabour;
    protected void Page_Load(object sender, EventArgs e)
    {
        
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        string ss = Session["UserId"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();
        if (!IsPostBack)
        {
            DropDwonCategory();

        }
        lblusername.Text = Session["Usernmdisplay"].ToString();
    }
    public void DropDwonCategory()
    {
        DataTable dtcate = new DataTable();
        dtcate = objdata.DropDownCategory();
        ddlCategory.DataSource = dtcate;
        ddlCategory.DataTextField = "CategoryName";
        ddlCategory.DataValueField = "CategoryCd";
        ddlCategory.DataBind();
    }
    protected void ddlCategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable dtempty = new DataTable();
        ddlEmpno.DataSource = dtempty;
        ddlEmpno.DataBind();
        ddlempname.DataSource = dtempty;
        ddlempname.DataBind();
        gvform7.DataSource = dtempty;
        gvform7.DataBind();
        if (ddlCategory.SelectedValue == "1")
        {
            Stafflabour = "S";
        }
        else if (ddlCategory.SelectedValue == "2")
        {
            Stafflabour = "L";
        }
        else
        {
            Stafflabour = "0";
        }
        DataTable dtDip = new DataTable();
        dtDip = objdata.DropDownDepartment_category(Stafflabour);
        if (dtDip.Rows.Count > 1)
        {
            ddldpt.DataSource = dtDip;
            ddldpt.DataTextField = "DepartmentNm";
            ddldpt.DataValueField = "DepartmentCd";
            ddldpt.DataBind();
        }
        else
        {
            DataTable dt = new DataTable();
            ddldpt.DataSource = dtempty;
            ddldpt.DataBind();
        }
    }
    protected void ddldpt_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable dtempty = new DataTable();
        ddlEmpno.DataSource = dtempty;
        ddlEmpno.DataBind();
        ddlempname.DataSource = dtempty;
        ddlempname.DataBind();
        gvform7.DataSource = dtempty;
        gvform7.DataBind();
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        if (ddlCategory.SelectedValue == "0")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Category....!');", true);
            //System.Windows.Forms.MessageBox.Show("Select the Category", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            ErrFlag = true;
        }
        else if (ddldpt.SelectedValue == "0")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Department....!');", true);
            //System.Windows.Forms.MessageBox.Show("Select the Department", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            ErrFlag = true;
        }
        else if (ddldpt.SelectedValue == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Department....!');", true);
            //System.Windows.Forms.MessageBox.Show("Select the Department", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            ErrFlag = true;
        }
        if (!ErrFlag)
        {
            DataTable dt = new DataTable();
            if (ddlCategory.SelectedValue == "1")
            {
                Stafflabour = "S";
            }
            else if (ddlCategory.SelectedValue == "2")
            {
                Stafflabour = "L";
            }
            if (SessionAdmin == "1")
            {
                dt = objdata.Emp_load_ESI(Stafflabour, ddldpt.SelectedValue);
            }
            else
            {
                dt = objdata.Emp_load_ESI_user(Stafflabour, ddldpt.SelectedValue);
            }
            ddlEmpno.DataSource = dt;
            DataRow dr = dt.NewRow();
            dr["EmpNo"] = ddldpt.SelectedItem.Text;
            dr["EmpName"] = ddldpt.SelectedItem.Text;
            dt.Rows.InsertAt(dr, 0);

            ddlEmpno.DataTextField = "EmpNo";
            ddlEmpno.DataValueField = "EmpNo";
            ddlEmpno.DataBind();
            ddlempname.DataSource = dt;
            ddlempname.DataTextField = "EmpName";
            ddlempname.DataValueField = "EmpNo";
            ddlempname.DataBind();
        }
    }
    protected void ddlEmpno_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable dtempty = new DataTable();
        gvform7.DataSource = dtempty;
        gvform7.DataBind();
        ddlempname.SelectedValue = ddlEmpno.SelectedValue;
    }
    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }
    protected void ddlempname_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable dtempty = new DataTable();
        gvform7.DataSource = dtempty;
        gvform7.DataBind();
        ddlEmpno.SelectedValue = ddlempname.SelectedValue;
    }
    protected void btnExport_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        bool Download = false;
        if (ddlCategory.SelectedValue == "0")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Category....!');", true);
            //System.Windows.Forms.MessageBox.Show("Select the Category", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            ErrFlag = true;
        }
        else if (ddldpt.SelectedValue == "0")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Department....!');", true);
            //System.Windows.Forms.MessageBox.Show("Select the Department", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            ErrFlag = true;
        }
        else if (ddldpt.SelectedValue == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Department....!');", true);
            //System.Windows.Forms.MessageBox.Show("Select the department", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            ErrFlag = true;
        }
        else if (ddlEmpno.SelectedValue == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Employee No....!');", true);
            //System.Windows.Forms.MessageBox.Show("Select the Employee Number", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            ErrFlag = true;
        }
        if (!ErrFlag)
        {
            DataTable dt = new DataTable();
            dt = objdata.Form7_sp(ddlEmpno.SelectedValue);
            gvform7.DataSource = dt;
            gvform7.DataBind();
            if (dt.Rows.Count > 0)
            {
                string attachment = "attachment; filename=Form7.xls";
                Response.ClearContent();
                Response.AddHeader("content-disposition", attachment);
                Response.ContentType = "application/ms-excel";

                StringWriter stw = new StringWriter();
                HtmlTextWriter htextw = new HtmlTextWriter(stw);
                gvform7.RenderControl(htextw);
                Response.Write("<table>");
                Response.Write("<tr align='center'>");
                Response.Write("<td colspan='14'>");
                Response.Write("EMPLOYEES STATE INSURANCE CORPORATION");
                Response.Write("</td>");

                Response.Write("</tr>");
                Response.Write("<tr align='center'>");
                Response.Write("<td colspan='14'>");
                Response.Write("FORM No 7");
                Response.Write("</td>");

                Response.Write("</tr>");

                Response.Write("<tr align='center'>");
                Response.Write("<td colspan='14'>");
                Response.Write("Register of Employee");
                Response.Write("</td>");

                Response.Write("</tr>");
                Response.Write("<tr align='center'>");
                Response.Write("<td colspan='14'>");
                Response.Write("Regulation 32");
                Response.Write("</td>");

                Response.Write("</tr>");
                Response.Write("<tr align='center'>");
                Response.Write("<td colspan='14'>");
                Response.Write("</td>");
                Response.Write("");
                Response.Write("</tr>");
                Response.Write("<tr align='left'>");
                Response.Write("<td>");
                Response.Write("Company Name : ");
                Response.Write("</td>");
                Response.Write("<td colspan='13'>");
                Response.Write("");
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("<tr align='left'>");
                Response.Write("<td>");
                Response.Write("Company Address : ");
                Response.Write("</td>");
                Response.Write("<td colspan='13'>");
                Response.Write("");
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("<tr align='left'>");
                Response.Write("<td>");
                Response.Write("City : ");
                Response.Write("</td>");
                Response.Write("<td colspan='13'>");
                Response.Write("");
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("</table>");
                Response.Write(stw.ToString());

                Response.Write("<Table>");
                Response.Write("<tr align='right'>");
                Response.Write("<td>");
                Response.Write("");
                Response.Write("</td>");
                Response.Write("<td>");
                Response.Write("Total Paid Days : ");
                Response.Write("</td>");
                Response.Write("<td colspan='12'>");
                Response.Write("");
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("<tr align='right'>");
                Response.Write("<td>");
                Response.Write("");
                Response.Write("</td>");
                Response.Write("<td>");
                Response.Write("Total Wages : ");
                Response.Write("</td>");
                Response.Write("<td colspan='12'>");
                Response.Write("");
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("<tr align='right'>");
                Response.Write("<td>");
                Response.Write("");
                Response.Write("</td>");
                Response.Write("<td>");
                Response.Write("Total Employee Contribution : ");
                Response.Write("</td>");
                Response.Write("<td colspan='12'>");
                Response.Write("");
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("<tr align='right'>");
                Response.Write("<td>");
                Response.Write("");
                Response.Write("</td>");
                Response.Write("<td>");
                Response.Write("No of Employee contribution : ");
                Response.Write("</td>");
                Response.Write("<td colspan='12'>");
                Response.Write("0.0");
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("<tr align='right'>");
                Response.Write("<td>");
                Response.Write("");
                Response.Write("</td>");
                Response.Write("<td>");
                Response.Write("Employer's Contribution : ");
                Response.Write("</td>");
                Response.Write("<td colspan='12'>");
                Response.Write("");
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("<tr align='right'>");
                Response.Write("<td>");
                Response.Write("");
                Response.Write("</td>");
                Response.Write("<td>");
                Response.Write("Total Contribution : ");
                Response.Write("</td>");
                Response.Write("<td colspan='12'>");
                Response.Write("");
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("<tr align='right'>");
                Response.Write("<td>");
                Response.Write("");
                Response.Write("</td>");
                Response.Write("<td>");
                Response.Write("Total contribution paid : ");
                Response.Write("</td>");
                Response.Write("<td colspan='12'>");
                Response.Write("");
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("<tr align='right'>");
                Response.Write("<td>");
                Response.Write("");
                Response.Write("</td>");
                Response.Write("<td>");
                Response.Write("Total Date of Payment : ");
                Response.Write("</td>");
                Response.Write("<td colspan='12'>");
                Response.Write("");
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("</table>");
                Download = true;
                Response.End();

            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('No Data Found');", true);
                //System.Windows.Forms.MessageBox.Show("No Data Found", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            }
            if (Download == true)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Exported Successfully in the name of Form7.xls');", true);
                //System.Windows.Forms.MessageBox.Show("Exported Successfully in the name of Form7.xls", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            }
        }
    }
}
