<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SalaryMaster.aspx.cs" Inherits="SPMSalaryMaster" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
    <head id="Head1" runat="server">
        <title>Payroll Management Systems</title>
        <link href="new_css/main.css" rel="stylesheet" type="text/css" />
        <link href="new_css/colors/dark_blue.css" rel="stylesheet" type="text/css" />
        <link href="new_css/superfish.css" rel="stylesheet" type="text/css" />

        <script src="new_js/jquery-1.4.2.min.js" type="text/javascript"></script>
        <script src="new_js/jquery.hoverintent.minified.js" type="text/javascript"></script>
        <script src="new_js/superfish.js" type="text/javascript"></script>
        <script src="new_js/jquery.tools.min.js" type="text/javascript"></script>
        <%--<script src="new_js/jquery.simplemodal.js" type="text/javascript"></script>--%>
        <script src="new_js/jquery.notifybar.js" type="text/javascript"></script>
        <script src="new_js/jquery.tipsy.js" type="text/javascript"></script>
        <script src="new_js/enhance.js" type="text/javascript"></script>
        <script src="new_js/excanvas.js" type="text/javascript"></script>
        <script src="new_js/visualize.jquery.js" type="text/javascript"></script>
        <script src="new_js/jquery.collapsible.js" type="text/javascript"></script>
        <script src="new_js/jquery.autosuggest.packed.js" type="text/javascript"></script>
        <script src="new_js/platinum-admin.js" type="text/javascript"></script>
        <style>
            .sf-navbar li ul 
            {
	            width: 1100px; /*IE6 soils itself without this*/
	            margin: 0px auto;
	            margin-left:-20em;	            
            }
        </style>
    </head>
    <body>
        <form id="form1" runat="server">
            <cc1:ToolkitScriptManager runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true"
                ID="ScriptManager1" EnablePartialRendering="true">
            </cc1:ToolkitScriptManager>
            <div id="header">
                <div id="header-top">
                    <div id="logo">
	                    <h1>&nbsp;</h1>
	                    <span id="slogan">&nbsp;</span> 
	                </div>
                    <!-- end logo -->
                    <div id="login-info">
                        <img id="profile" alt="profile" src="./new_images/icons/user.png" />
                        <p id="top"><span id="name"><asp:Label ID="lblusername" runat="server"></asp:Label></span><br />
                        <span id="links">
                        <%--<a id="message-link" href="#" title="click to see the messages!">
                        <span id="message-count">1</span> new message. </a>--%></span></p>
                         <div>
                            <asp:Label ID="lblComany" runat="server" Text="" Font-Bold="true"></asp:Label>
                        </div>
	                    <div id="messages-box">
		                    <h4>Messages</h4>
	                        <h5><a id="new-message" href="#">New Message</a></h5>
	                        <hr />
	                        <ul id="messages">
		                        <li class="new"><a href="#">
		                            <img alt="user" src="./new_images/icons/sender.png" />
		                            <span class="sender">John Doe</span>
		                            <span class="description">Hi this is just a description...</span>
		                            <span class="date">20 minutes ago...</span> </a>
		                        </li>
		                        <li><a href="#">
		                            <img alt="user" src="./new_images/icons/sender.png" />
		                            <span class="sender">John Doe</span>
		                            <span class="description">Hi this is just a description...</span>
		                            <span class="date">24 minutes ago...</span> </a>
		                        </li>
		                        <li><a href="#">
		                            <img alt="user" src="./new_images/icons/sender.png" />
		                            <span class="sender">John Doe</span>
		                            <span class="description">Hi this is just a description...</span>
		                            <span class="date">50 minutes ago...</span> </a>
		                        </li>
		                        <li id="last"><a id="all-messages" href="#">See all messages!
		                            <span id="unreaded">8 unreaded.</span> </a>
		                        </li>
		                    </ul>
		                    <br class="clear" />
	                    </div><!-- end messages-box -->
	                    <a id="power" href="Default.aspx" title="Logout">logout</a>
                    </div>
                    <!-- end login -->
                    <div id="nav">
	                    <ul class="sf-menu sf-navbar">
	                        <li><a id="dashboard" href="Dashboard.aspx">Dashboard</a>
	                            <ul>
		                            <li>&nbsp;
		                            </li>
	                            </ul>
	                        </li>
	                        <li class="current"><a id="Employee" href="EmployeeRegistration.aspx">Employee</a>
	                            <ul>
		                            <li><a href="EmployeeRegistration.aspx">Employee</a> </li>					                    
		                            <li><a href="OfficalProfileDetails.aspx">Profile</a></li>
		                            <li class="current"><a href="SalaryMaster.aspx">Salary Master</a></li>
		                            <%--<li><a href="LeaveAllocationEmployee.aspx">Leave Allocation</a></li>--%>
		                            <%--<li><a href="ApplyForLeave.aspx">Apply Leave</a></li>--%>						                    
		                            <li><a href="SalaryRegistration.aspx">Salary Details</a></li>
		                            <li><a href="SalaryAdvance.aspx">Advance</a></li>
		                             <li><a href="Settlement.aspx">Settlement</a></li>
		                            <li><a href="EmployeeDeactiveMode.aspx">Employee Re-Activate</a></li>
		                            <%--<li><a href="SalarySummary.aspx">Salary Summary</a></li>--%>
	 
		                             				                    
	                            </ul>
	                        </li>
	                         <li><a id="Reports" href="RptDepartmentwiseEmployee.aspx">Reports</a>
	                            <ul>
		                            <li><a href="RptDepartmentwiseEmployee.aspx">Employee Details</a></li>
		                            <%--<li><a href="RptAttendanceReport.aspx">Attendance Report</a></li>--%>
		                           <%-- <li><a href="RptProbationperiod.aspx">Probationary Period</a></li>
		                            <li><a href="ContractBreak.aspx">Contract Details</a></li>--%>
		                            <li><a href="RptResign.aspx">Resignation Reports</a></li>
		                            <li><a href="PaySlipGen.aspx">Payslip</a></li>
		                            <%--<li><a href="RptSalary.aspx">Salary Details</a></li>--%>
		                            <%--<li><a href="Incentive.aspx">Incentive Details</a></li>--%>
		                            <li><a href="RptDepartmentSalaryAbstract.aspx">Salary Abstract</a></li>
	                            </ul>
	                        </li>
	                        <li><a id="OT" href="OverTime.aspx">OT</a>
	                            <ul>
	                                <li><a href="OverTime.aspx">OT</a></li>
	                                <li><a href="RptOT.aspx">OT Report</a></li>
	                            </ul>
	                        </li>
	                        <li><a id="Master" href="MstEmployeeType.aspx">Masters</a>
	                            <ul>
		                            <li><a href="MstEmployeeType.aspx">Employee Type</a></li>
		                            <li><a href="UserRegistration.aspx">User</a></li>
		                            <li><a href="MSTPFESI.aspx">PF</a></li>
		                            <li><a href="MstBank.aspx">Bank</a></li>
		                            <li><a href="MstDepartment.aspx">Department</a></li>
		                            <li><a href="MstDAfix.aspx">DA Arrears FIX</a></li>
	                            </ul>
	                        </li>
	                        <li><a id="Bonus" href="BonusForAll.aspx">Bonus</a>
	                            <ul>
		                            <li><a href="BonusForAll.aspx">Bonus</a></li>
		                            <li><a href="StandardBonus.aspx">Group Bonus</a></li>
		                            <li><a href="RptBonus.aspx">Bonus Report</a></li>					                    
	                            </ul>
	                        </li>
	                        <li><a id="ESI_PF" href="PFDownload.aspx">ESI & PF</a>
	                            <ul>
		                            <%--<li><a href="PFForm5.aspx">PF Form 5</a></li>--%>
		                            <li><a href="PFDownload.aspx">PF Download</a></li>
		                            <li><a href="ESIDownload.aspx">ESI Download</a></li>
		                            <li><a href="PFForm3Anew.aspx">PF Form 3A</a></li>
		                            <li><a href="PFform6A.aspx">PF Form 6A</a></li>
		                            <%--<li><a href="FOrm10.aspx">Form 10</a></li>
		                            <li><a href="PFForm3Anew.aspx">PF Form 3A</a></li>
		                            <li><a href="PFform6A.aspx">PF Form 6A</a></li>
		                            <li><a href="ESIForm7.aspx">ESI Form 7</a></li>--%>
	                            </ul>
	                        </li>
	                        <li><a id="Upload" href="UploadEmployee.aspx">Upload</a>
	                            <ul>
	                            <li><a href="UploadEmployee.aspx">Employee Upload</a></li>
		                            <li><a href="AttenanceUpload.aspx">Attenance Upload</a></li>
		                            	<li><a href="UploadOT.aspx">OT Upload</a></li>
		                            	<%--<li><a href="LoadEmpmusters.aspx">Employee Master</a></li>--%>
	                            </ul>
	                        </li>
                        </ul>
                    </div>
                    <!-- end div#nav -->
                </div>
                <!-- end div#header-top -->
                <div class="breadCrumb">
                    <ul>
                        <li class="first"><a href="#">Home</a> </li>
                        <li><a href="#">Users</a> </li>
                        <li><a href="#">Roles</a> </li>
                        <li class="last">Salary Master</li>
                    </ul>
                </div>
                <!-- end div#breadCrumb -->
                <%--<div id="search-box">
                    <form id="searchform" action="" method="get">
                        <fieldset class="search"><span>Search</span>
                        <input class="box" type="text" />
                        <button class="btn" title="Submit Search">Search</button></fieldset>
                    </form>
                </div>--%>
                <!-- end div#search-box -->
            </div>
            <!-- end header -->
            <div id="page-wrap">
                <div id="right-sidebar">
                    <div class="innerdiv">
	                    <h2 class="head">DownLoad Format</h2>
	                    <div class="innercontent clear">
		                    <h4>DownLoad Format</h4>
		                    <ul id="comments" class="tooltip-enabled">
		                    <asp:Button ID="btncontract" runat="server" Text="Contract Report" Width="200" 
                                    CssClass="button green" onclick="btncontract_Click" />
		                        <asp:Button ID="btnEmp" runat="server" Text="Employee Download" Width="200" 
                                    CssClass="button green" onclick="btnEmp_Click" />
		                        <asp:Button ID="btnatt" runat="server" Text="Attenance Download" Width="200" 
                                    CssClass="button green" onclick="btnatt_Click"/>
		                        <asp:Button ID="btnLeave" runat="server" Text="Leave Download" Width="200" 
                                    CssClass="button green" OnClick="btnLeave_Click" />
                                    <asp:Button ID="btnOT" runat="server" Text="OT Download" Width="200" 
                                    CssClass="button green" onclick="btnOT_Click" />
		                        <asp:Button ID="btnSal" runat="server" Text="Salary Download" Width="200" 
                                    CssClass="button green" onclick="btnSal_Click" />
		                    </ul>
		                    <%--<img src="new_images/icons/right_img.jpg" alt="" width="230"/>--%>
	                    </div>
                    </div>
                    <div class="clear"></div>                    
                </div>
	            <!-- end right-sidebar -->
	            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
	                    <ContentTemplate>
	                        <div id="main-content">
                                <br /><br /><br /><br /> 
	                            <div class="clear_body">
                                    <div class="innerdiv">
                                        <div class="innercontent">
                                            <!-- tab "panes" -->				                
                                                <div>
                                                    <table class="full">
                                                        <thead>
                                                            <tr align="center">
                                                                <th colspan="4" style="text-align:center;"><h4>SALARY MASTER</h4></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td><asp:Label ID="label1" runat="server" Text="Category" Font-Bold="true" ></asp:Label></td>
                                                                <td><asp:DropDownList ID="ddlcategory" runat="server" Width="100" Height="25" 
                                                                        AutoPostBack="True" onselectedindexchanged="ddlcategory_SelectedIndexChanged"></asp:DropDownList></td>
                                                                
                                                               
                                                                <td><asp:Label ID="label2" runat="server" Text="Department" Font-Bold="true" ></asp:Label></td>
                                                                <td colspan="1"><asp:DropDownList ID="ddlDepartment" runat="server" Width="100" Height="25" TabIndex="2" 
                                                                onselectedindexchanged="ddlDepartment_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                                                <asp:Button ID="btnclick" runat="server" Text="Click" Width="78" Height="28" onclick="btnclick_Click" CssClass="button green" /></td>   
                                                            </tr>
                                                            <tr>
                                                                <td><asp:Label ID="lblFinance" runat="server" Text="Financial Year" Font-Bold="true" ></asp:Label></td>
                                                                <td colspan="3"><asp:DropDownList ID="ddFinance" runat="server" AutoPostBack="true" 
                                                                        Width="150" Height="25" 
                                                                        onselectedindexchanged="ddFinance_SelectedIndexChanged" ></asp:DropDownList></td>
                                                            </tr>
                                                            
                                                             <tr>
                                                                <td><asp:Label ID="lblexisitingno" runat="server" Text="Existing Number" Font-Bold="true"></asp:Label></td>
                                                                <td><asp:TextBox ID="txtexistingNo" runat="server" ></asp:TextBox>
                                                                 <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender_ExisistingCode" runat="server"
                                                                 TargetControlID="txtexistingNo" FilterMode="ValidChars" FilterType="UppercaseLetters,LowercaseLetters,Numbers,Custom"                                                           ValidChars="0123456789+- ">
                                                                 </cc1:FilteredTextBoxExtender>
                                                                    <asp:DropDownList ID="ddToken" runat="server" AutoPostBack="true" Width="150" 
                                                                        Height="25" onselectedindexchanged="ddToken_SelectedIndexChanged"
                                                                         ></asp:DropDownList>
                                                                 </td>
                                                                 <td colspan="2"><asp:Button ID="btnexitingSearch" runat="server" Text="Search" Width="78" 
                                                                 Height="28" onclick="btnexitingSearch_Click" CssClass="button green"  /></td>
                                                            </tr>
                                                            <tr>
                                                                <td><asp:Label ID="label3" runat="server" Text="Employee No" Font-Bold="true" ></asp:Label></td>
                                                                <td><asp:DropDownList ID="ddlempNo" runat="server" AutoPostBack="true" Width="150" 
                                                                        Height="25" TabIndex="3" 
                                                                        onselectedindexchanged="ddlempNo_SelectedIndexChanged" ></asp:DropDownList></td>
                                                                <td><asp:Label ID="label4" runat="server" Text="Employee Name" Font-Bold="true" ></asp:Label></td>
                                                                <td colspan="1"><asp:DropDownList ID="ddlEmpName" runat="server" 
                                                                        AutoPostBack="true" Width="150" Height="25" TabIndex="4" 
                                                                        onselectedindexchanged="ddlEmpName_SelectedIndexChanged" ></asp:DropDownList></td>
                                                            </tr>
                                                            <tr>
                                                                <td><asp:Label ID="label7" runat="server" Text="Salary Type" Font-Bold="true" ></asp:Label></td>
                                                                 <td colspan="3"><asp:RadioButtonList ID="rbsalary" runat="server" Enabled="false" RepeatColumns="3" >
                                                                 <asp:ListItem Text="Weekly Salary" Value="1"></asp:ListItem>
                                                                 <asp:ListItem Text="Monthly Salary" Value="2"></asp:ListItem>
                                                                 <asp:ListItem Text="Bi-Monthly Salary" Value="3"></asp:ListItem>                                                                 
                                                                 </asp:RadioButtonList></td>
                                                            </tr>
                                                            <asp:Panel ID="Panelall" runat="server" Visible="false" >
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="lblBase" runat="server" Text="Basic Salary" Font-Bold="true" ></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txtbase" runat="server" ></asp:TextBox>
                                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR9" runat="server" FilterMode="ValidChars"
                                                                FilterType="Numbers,Custom" TargetControlID="txtbase" ValidChars="."></cc1:FilteredTextBoxExtender>
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="lblFDA" runat="server" Text="FDA" Font-Bold="true" ></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txtFDA" runat="server" ></asp:TextBox>
                                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR1" runat="server" FilterMode="ValidChars"
                                                                FilterType="Numbers,Custom" TargetControlID="txtFDA" ValidChars="."></cc1:FilteredTextBoxExtender>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="lblVDA" runat="server" Text="VDA " Font-Bold="true" ></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txtVDA" runat="server" ></asp:TextBox>
                                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR2" runat="server" FilterMode="ValidChars"
                                                                FilterType="Numbers,Custom" TargetControlID="txtVDA" ValidChars="."></cc1:FilteredTextBoxExtender>           
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="lblHRA" runat="server" Text="HRA " Font-Bold="true" ></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txthra" runat="server" ></asp:TextBox>
                                                                    <asp:Button ID="btnCal" runat="server" Text="Calculate" CssClass="button green" Width="75" onclick="btnCal_Click" Visible="false" />
                                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR3" runat="server" FilterMode="ValidChars"
                                                                FilterType="Numbers,Custom" TargetControlID="txthra" ValidChars="."></cc1:FilteredTextBoxExtender>
                                                                </td>
                                                            </tr>
                                                            <tr id="Union_tr" visible="false">
                                                                <%--<td>
                                                                    <asp:Label ID="lbltotal" runat="server" Text="Total Earnings / Day" Font-Bold="true" ></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txttotal" runat="server" Enabled="false" ></asp:TextBox>
                                                                </td>--%>
                                                                <td>
                                                                    <asp:Label ID="lblUnion" runat="server" Text="PF Salary" Font-Bold="true" ></asp:Label>
                                                                </td>
                                                                <td colspan="3">
                                                                    <asp:TextBox ID="txtUnion" runat="server" Text="0" ></asp:TextBox>
                                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR5" runat="server" FilterMode="ValidChars"
                                                                FilterType="Numbers,Custom" TargetControlID="txtUnion" ValidChars="."></cc1:FilteredTextBoxExtender>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="lblAllowance1" runat="server" Text="Allowance 1 " Font-Bold="true" ></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txAllowance1" runat="server" Text="0" Font-Bold="true" ></asp:TextBox>
                                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR4" runat="server" FilterMode="ValidChars"
                                                                FilterType="Numbers,Custom" TargetControlID="txAllowance1" ValidChars="."></cc1:FilteredTextBoxExtender>
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="lblDeduction1" runat="server" Text="Deduction 1 " Font-Bold="true" ></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txtDeduction1" runat="server" Text="0" ></asp:TextBox>
                                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR6" runat="server" FilterMode="ValidChars"
                                                                FilterType="Numbers,Custom" TargetControlID="txtDeduction1" ValidChars="."></cc1:FilteredTextBoxExtender>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="lblAllowance2" runat="server" Text="Allowance 2 " Font-Bold="true" ></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txtAllowance2" runat="server" Text="0" Font-Bold="true" ></asp:TextBox>
                                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR7" runat="server" FilterMode="ValidChars"
                                                                FilterType="Numbers,Custom" TargetControlID="txtAllowance2" ValidChars="."></cc1:FilteredTextBoxExtender>
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="lblDeduction2" runat="server" Text="Deduction 2 " Font-Bold="true" ></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txtDeduction2" runat="server" Text="0" ></asp:TextBox>
                                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR8" runat="server" FilterMode="ValidChars"
                                                                FilterType="Numbers,Custom" TargetControlID="txtDeduction2" ValidChars="."></cc1:FilteredTextBoxExtender>
                                                                </td>
                                                            </tr>
                                                            </asp:Panel>
                                                            <asp:Panel ID="PanelStaff" runat="server" Visible ="false" >
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="lblStaffBasic" runat="server" Text="Basic Salary" Font-Bold="true" ></asp:Label>                         
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txtSBasic" runat="server" Text="0" ></asp:TextBox>
                                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR10" runat="server" FilterMode="ValidChars"
                                                                FilterType="Numbers,Custom" TargetControlID="txtSBasic" ValidChars="."></cc1:FilteredTextBoxExtender>
                                                                </td>
                                                                 <td>
                                                                    <asp:Label ID="lblStaffHRA" runat="server" Text="HRA" Font-Bold="true" ></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txtStaffHRA" runat="server" Text="0.0" ></asp:TextBox>
                                                                    
                                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR28" runat="server" FilterMode="ValidChars"
                                                                FilterType="Numbers,Custom" TargetControlID="txtStaffHRA" ValidChars="."></cc1:FilteredTextBoxExtender>
                                                                </td>
                                                            </tr>
                                                                <tr>
                                                              <td>
                                                                    <asp:Label ID="lblStaffTA" runat="server" Text="TA " Font-Bold="true" ></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txtStaffTA" runat="server" Text="0.0" ></asp:TextBox>
                                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR27" runat="server" FilterMode="ValidChars"
                                                                FilterType="Numbers,Custom" TargetControlID="txtStaffTA" ValidChars="."></cc1:FilteredTextBoxExtender>           
                                                                </td>
                                                               
                                                                <td>
                                                                    <asp:Label ID="Label16" runat="server" Text="PF Salary" Font-Bold="true" ></asp:Label>                         
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txtPF" runat="server" Text="0" ></asp:TextBox>
                                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR26" runat="server" FilterMode="ValidChars"
                                                                FilterType="Numbers,Custom" TargetControlID="txtPF" ValidChars="."></cc1:FilteredTextBoxExtender>
                                                                </td>
                                                                
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="lblSall1" runat="server" Text="Allowance 1" Font-Bold="true" ></asp:Label>                         
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txtSall1" runat="server" Text="0" ></asp:TextBox>
                                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR11" runat="server" FilterMode="ValidChars"
                                                                FilterType="Numbers,Custom" TargetControlID="txtSall1" ValidChars="."></cc1:FilteredTextBoxExtender>
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="lblSAded1" runat="server" Text="Deduction 1" Font-Bold="true" ></asp:Label>                         
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txtSded1" runat="server" Text="0" ></asp:TextBox>
                                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR14" runat="server" FilterMode="ValidChars"
                                                                FilterType="Numbers,Custom" TargetControlID="txtSded1" ValidChars="."></cc1:FilteredTextBoxExtender>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="lblSAll2" runat="server" Text="Allowance 2" Font-Bold="true" ></asp:Label>                         
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txtSall2" runat="server" Text="0" ></asp:TextBox>
                                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR12" runat="server" FilterMode="ValidChars"
                                                                FilterType="Numbers,Custom" TargetControlID="txtSall2" ValidChars="."></cc1:FilteredTextBoxExtender>
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="lblSded2" runat="server" Text="Deduction 2" Font-Bold="true" ></asp:Label>                         
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txtSded2" runat="server" Text="0" ></asp:TextBox>
                                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR13" runat="server" FilterMode="ValidChars"
                                                                FilterType="Numbers,Custom" TargetControlID="txtSded2" ValidChars="."></cc1:FilteredTextBoxExtender>
                                                                </td>
                                                            </tr>
                                                        
                                                            
                                                            </asp:Panel>
                                                            <asp:Panel ID="PanelDaily" runat="server" Visible="false" >
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="lblWBasic" runat="server" Text="Basic Salary" Font-Bold="true"></asp:Label>
                                                                    </td>
                                                                    <td colspan="3">
                                                                    <asp:TextBox ID="txtWBasic" runat="server" Text="0" ></asp:TextBox>
                                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR15" runat="server" FilterMode="ValidChars"
                                                                FilterType="Numbers,Custom" TargetControlID="txtWBasic" ValidChars="."></cc1:FilteredTextBoxExtender>
                                                                </td>
                                                                </tr>
                                                                <tr>
                                                                <td>
                                                                    <asp:Label ID="Label5" runat="server" Text="Allowance 1" Font-Bold="true" ></asp:Label>                         
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txtWAll1" runat="server" Text="0" ></asp:TextBox>
                                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR16" runat="server" FilterMode="ValidChars"
                                                                FilterType="Numbers,Custom" TargetControlID="txtWAll1" ValidChars="."></cc1:FilteredTextBoxExtender>
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="Label6" runat="server" Text="Deduction 1" Font-Bold="true" ></asp:Label>                         
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txtWded1" runat="server" Text="0" ></asp:TextBox>
                                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR17" runat="server" FilterMode="ValidChars"
                                                                FilterType="Numbers,Custom" TargetControlID="txtWded1" ValidChars="."></cc1:FilteredTextBoxExtender>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="Label8" runat="server" Text="Allowance 2" Font-Bold="true" ></asp:Label>                         
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txtWAll2" runat="server" Text="0" ></asp:TextBox>
                                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR18" runat="server" FilterMode="ValidChars"
                                                                FilterType="Numbers,Custom" TargetControlID="txtWAll2" ValidChars="."></cc1:FilteredTextBoxExtender>
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="Label9" runat="server" Text="Deduction 2" Font-Bold="true" ></asp:Label>                         
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txtWded2" runat="server" Text="0" ></asp:TextBox>
                                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR19" runat="server" FilterMode="ValidChars"
                                                                FilterType="Numbers,Custom" TargetControlID="txtWded2" ValidChars="."></cc1:FilteredTextBoxExtender>
                                                                </td>
                                                            </tr>
                                                            </asp:Panel>
                                                            <asp:Panel ID="PanelSpecial" runat="server" Visible="false" >
                                                               <tr>
                                                                    <td>
                                                                        <asp:Label ID="Label10" runat="server" Text="Basic Salary" Font-Bold="true"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                    <asp:TextBox ID="txtOBasic" runat="server" Text="0" ></asp:TextBox>
                                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR20" runat="server" FilterMode="ValidChars"
                                                                FilterType="Numbers,Custom" TargetControlID="txtOBasic" ValidChars="."></cc1:FilteredTextBoxExtender>
                                                                </td>
                                                                <td>
                                                                        <asp:Label ID="Label15" runat="server" Text="Union Charge" Font-Bold="true"></asp:Label>
                                                                    </td>
                                                                    <td colspan="1">
                                                                    <asp:TextBox ID="txtOUnion" runat="server" Text="0" ></asp:TextBox>
                                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR25" runat="server" FilterMode="ValidChars"
                                                                FilterType="Numbers,Custom" TargetControlID="txtOUnion" ValidChars="."></cc1:FilteredTextBoxExtender>
                                                                </td>
                                                                </tr>
                                                                <tr>
                                                                <td>
                                                                    <asp:Label ID="Label11" runat="server" Text="Allowance 1" Font-Bold="true" ></asp:Label>                         
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txtOAll1" runat="server" Text="0" ></asp:TextBox>
                                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR21" runat="server" FilterMode="ValidChars"
                                                                FilterType="Numbers,Custom" TargetControlID="txtOAll1" ValidChars="."></cc1:FilteredTextBoxExtender>
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="Label12" runat="server" Text="Deduction 1" Font-Bold="true" ></asp:Label>                         
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txtOded1" runat="server" Text="0" ></asp:TextBox>
                                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR22" runat="server" FilterMode="ValidChars"
                                                                FilterType="Numbers,Custom" TargetControlID="txtOded1" ValidChars="."></cc1:FilteredTextBoxExtender>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="Label13" runat="server" Text="Allowance 2" Font-Bold="true" ></asp:Label>                         
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txtOAll2" runat="server" Text="0" ></asp:TextBox>
                                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR23" runat="server" FilterMode="ValidChars"
                                                                FilterType="Numbers,Custom" TargetControlID="txtOAll2" ValidChars="."></cc1:FilteredTextBoxExtender>
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="Label14" runat="server" Text="Deduction 2" Font-Bold="true" ></asp:Label>                         
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txtOded2" runat="server" Text="0" ></asp:TextBox>
                                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR24" runat="server" FilterMode="ValidChars"
                                                                FilterType="Numbers,Custom" TargetControlID="txtOded2" ValidChars="."></cc1:FilteredTextBoxExtender>
                                                                </td>
                                                            </tr>
                                                            </asp:Panel>
                                                            <tr>
                                                                 <tr><td align="center" colspan="4">
                                                                <asp:Button ID="btnSave" runat="server" Height="28" onclick="btnSave_Click" Text="Save" Width="75" Font-Bold="true" CssClass="button green"/>
                                                                <asp:Button ID="btnreset" runat="server" Height="28"  Text="Reset" Width="75" Font-Bold="true"
                                                                       onclick="btnreset_Click" CssClass="button green" />
                                                                <asp:Button ID="btncancel" runat="server" Height="28" onclick="btncancel_Click" Text="Cancel" Width="75" Font-Bold="true" CssClass="button green"/></td>
                                                            </tr>
                                                            
                                                            
                                                        </tbody>
                                                        <caption>
                                                            &nbsp;</caption>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>              
            </div>
            <!-- end page-wrap -->
            <div id="footer">
                <ul>
                    <li>Copyright &copy; 2012. All rights reserved.i>
                    <li>Powered by <a href="http://www.altius.co.in" target="_blank">Altius Infosystems</a></li>
                </ul>
            </div>
            <!-- end footer -->
        </form>
    </body>
