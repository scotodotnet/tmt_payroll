﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

/// <summary>
/// Summary description for AttenanceClass
/// </summary>
public class AttenanceClass
{
    private string _EmpNo;
    private string _Exist;
    private string _department;
    private string _days;
    private string _Months;
    private string _finance;
    private string _EmpName;
    private string _totalDays;
    private string _Ccode;
    private string _Lcode;
    private string _workingdays;
    private string _nfh;
    private string _cl;
    private string _absent;
    private string _weekoff;
    private string _HomeDays;
    private string _Half;
    private string _Full;
    private string _ThreeSided;
    private string _Rest;
    private string _FixedDays;
    private string _NfhCount;
    private string _NfhPresent;
    private string _FineAmt;
    private string _WHPresent;
    private string _BonusDays;

    public string BonusDays
    {
        get { return _BonusDays; }
        set { _BonusDays = value; }
    }


    public string WHPresent
    {
        get { return _WHPresent; }
        set { _WHPresent = value; }
    }


    public string FineAmt
    {
        get { return _FineAmt; }
        set { _FineAmt = value; }
    }


    public string NfhPresent
    {
        get { return _NfhPresent; }
        set { _NfhPresent = value; }
    }


    public string NfhCount
    {
        get { return _NfhCount; }
        set { _NfhCount = value; }
    }
    

    public string FixedDays
    {
        get { return _FixedDays; }
        set { _FixedDays = value; }
    }

	public AttenanceClass()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public string EmpNo
    {
        get { return _EmpNo; }
        set { _EmpNo = value; }
    }
    public string Exist
    {
        get { return _Exist; }
        set { _Exist = value; }
    }
    public string department
    {
        get { return _department; }
        set { _department = value; }
    }
    public string days
    {
        get { return _days; }
        set { _days = value; }
    }
    public string Months
    {
        get { return _Months; }
        set { _Months = value; }
    }
    public string finance
    {
        get { return _finance; }
        set { _finance = value; }
    }
    public string EmpName
    {
        get { return _EmpName; }
        set { _EmpName = value; }
    }
    public string TotalDays
    {
        get { return _totalDays; }
        set { _totalDays = value; }
    }
    public string Ccode
    {
        get { return _Ccode; }
        set { _Ccode = value; }
    }
    public string Lcode
    {
        get { return _Lcode; }
        set { _Lcode = value; }
    }
    public string nfh
    {
        get { return _nfh; }
        set { _nfh = value; }
    }
    public string workingdays
    {
        get { return _workingdays; }
        set { _workingdays = value; }
    }
    public string cl
    {
        get { return _cl; }
        set { _cl = value; }
    }
    public string Absent
    {
        get { return _absent; }
        set { _absent = value; }
    }
    public string weekoff
    {
        get { return _weekoff; }
        set { _weekoff = value; }
    }
    public string HomeDays
    {
        get { return _HomeDays; }
        set { _HomeDays = value; }
    }
    public string half
    {
        get { return _Half; }
        set { _Half = value; }
    }
    public string Full
    {
        get { return _Full; }
        set { _Full = value; }
    }
    public string ThreeSided
    {
        get { return _ThreeSided; }
        set { _ThreeSided = value; }
    }
    public string Rest
    {
        get { return _Rest; }
        set { _Rest = value; }
    }
}
