﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

/// <summary>
/// Summary description for EmployeeClass
/// </summary>
public class EmployeeClass
{
    private string _EmpCode;
    private string _MachineCode;
    private string _ExisitingCode;
    private string _EmpName;
    private string _EmpFName;
    private string _Gender;
    private string _DOB;
    private string _PMAdd1;
    private string _PMAdd2;
    private string _PMAdd3;
    private string _PMTaluk;
    private string _PMDistrict;
    private string _PSAdd1;
    private string _PSAdd2;
    private string _PSAdd3;
    private string _PSTaluk;
    private string _PSDistrict;
    private string _PMState;
    private string _PSState;
    private string _Phone;
    private string _Mobile;
    private string _MaritalStatus;
    private string _BloodGroup;
    private string _Passportno;
    private string _DrivingLicno;
    private string _Nationality;
    private string _Qualification;
    private string _University;
    private string _CompanyNm;
    private string _Dept;
    private string _Desig;
    private string _Shift;
    private string _EmployeeType;
    private string _RoleCd;
    private string _Grade;
    private string _CreatedBy;
    private string _ModifiedBy;
    private string _Occupation;
    private string _UnEducated;
    private string _SchoolColleg;
    private string _UnEduDept;
    private string _UnEduDesig;
    private string _Ccode;
    private string _Lcode;
    private string _martial;
    private string _Initial;
    private string _contractType;
    private string _biometric;
    private string _InsideOutside;

	public EmployeeClass()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public string UnEduDept
    {
        get { return _UnEduDept; }
        set { _UnEduDept= value; }
    }
    public string UnEduDesig
    {
        get { return _UnEduDesig; }
        set { _UnEduDesig = value; }
    }
    public string Occupation
    {
        get { return _Occupation; }
        set { _Occupation = value; }
    }
    public string UnEducated
    {
        get { return _UnEducated; }
        set { _UnEducated = value; }
    }
    public string SchoolCollege
    {
        get { return _SchoolColleg; }
        set { _SchoolColleg = value; }
    }
    public string EmpCode
    {
        get { return _EmpCode; }
        set { _EmpCode = value; }
    }
    public string MachineCode
    {
        get { return _MachineCode; }
        set { _MachineCode = value; }
    }
    public string ExisitingCode
    {
        get { return _ExisitingCode; }
        set { _ExisitingCode = value; }
    }

    public string EmpName
    {
        get { return _EmpName; }
        set { _EmpName = value; }
    }
    public string EmpFname
    {
        get { return _EmpFName; }
        set { _EmpFName = value; }
    }
    public string   DOB
    {
        get { return _DOB; }
        set { _DOB = value; }
    }
    public string Gender
    {
        get { return _Gender; }
        set { _Gender = value; }
    }
    public string PMAdd1 
    {
        get { return _PMAdd1; }
        set { _PMAdd1 = value; }
    }
    public string PMAdd2
    {
        get { return _PMAdd2; }
        set { _PMAdd2 = value; }
    }
    public string PMAdd3
    {
        get { return _PMAdd3; }
        set { _PMAdd3 = value; }
    }
    public string PMTaluk
    {
        get { return _PMTaluk; }
        set { _PMTaluk = value; }
    }
    public string PMDistrict
    {
        get { return _PMDistrict; }
        set { _PMDistrict = value; }
    }
    public string PSAdd1 
    {
        get { return _PSAdd1; }
        set { _PSAdd1 = value; }
    }
    public string PSAdd2
    {
        get { return _PSAdd2; }
        set { _PSAdd2 = value; }
    }
    public string PSAdd3
    {
        get { return _PSAdd3; }
        set { _PSAdd3 = value; }
    }
    public string PSTaluk
    {
        get { return _PSTaluk; }
        set { _PSTaluk = value; }
    }
    public string PSDistrict
    {
        get { return _PSDistrict; }
        set { _PSDistrict = value; }
    }
    public string PMState
    {
        get{return _PMState;}
        set { _PMState = value; }
       
    }
    public string PSState
    {
        get { return _PSState; }
        set { _PSState = value; }
    }
    public string Phone
    {
        get { return _Phone; }
        set { _Phone = value; }
    }
    public string Mobile
    {
        get { return _Mobile; }
        set { _Mobile = value; }
    }
    public string MaritalStatus
    {
        get { return _MaritalStatus; }
        set { _MaritalStatus = value; }
    }
    public string BloodGroup
    {
        get { return _BloodGroup; }
        set { _BloodGroup = value; }
    }
    public string PassportNo
    {
        get { return _Passportno; }
        set { _Passportno = value; }
    }
    public string DrivingLicNo
    {
        get { return _DrivingLicno; }
        set { _DrivingLicno = value; }
    }
    public string Nationality
    {
        get { return _Nationality; }
        set { _Nationality = value; }
    }
    public string Qualification
    {
        get { return _Qualification; }
        set { _Qualification = value; }
    }
    public string University
    {
        get { return _University; }
        set { _University = value; }
    }
    public string CompanyName
    {
        get { return _CompanyNm; }
        set { _CompanyNm = value; }
    }
    public string Department
    {
        get { return _Dept; }
        set { _Dept = value; }
    }
    public string Desingation
    {
        get { return _Desig; }
        set { _Desig = value; }
    }
    public string Shift
    {
        get { return _Shift; }
        set { _Shift = value; }
    }
    public string EmployeeType
    {
        get { return _EmployeeType; }
        set { _EmployeeType = value; }
    }
    public string RoleCd
    {
        get { return _RoleCd; }
        set { _RoleCd = value; }
    }
    public string Grade
    {
        get { return _Grade; }
        set {_Grade=value;}
    }
    public string CreatedBy
    {
        get { return _CreatedBy; }
        set { _CreatedBy = value; }
    }
    public string ModifiedBy
    {
        get { return _ModifiedBy; }
        set { _ModifiedBy = value; }
    }
    public string Ccode
    {
        get { return _Ccode; }
        set { _Ccode = value; }
    }
    public string Lcode
    {
        get { return _Lcode; }
        set { _Lcode = value; }
    }
    public string Martial
    {
        get { return _martial; }
        set { _martial = value; }
    }
    public string Initial
    {
        get { return _Initial; }
        set { _Initial = value; }
    }
    public string ContractType
    {
        get { return _contractType; }
        set { _contractType = value; }
    }
    public string Bio
    {
        get { return _biometric; }
        set { _biometric = value; }
    }
    public string Ins_Outs
    {
        get { return _InsideOutside; }
        set { _InsideOutside = value; }
    }
}
