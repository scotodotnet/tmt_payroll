﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for PFForm3AClass
/// </summary>
public class PFForm3AClass
{
    private string _EmpNo;
    private string _PFNumber;
    private string _amountwages;
    private string _epf;
    private string _RateofHigher;
    private string _epfDiff;
    private string _PensonFund;
    private string _RefundofAdd;
    private string _NonContribution;
    private string _CompCdnumber;
    private string _CompAdd1;
    private string _CompAdd2;
    private string _Location;
	public PFForm3AClass()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public string EmpNo
    {
        get { return _EmpNo; }
        set { _EmpNo = value; }
    }
    public string PFNumber
    {
        get { return _PFNumber; }
        set { _PFNumber = value; }

    }
    public string AmountWages
    {
        get { return _amountwages; }
        set { _amountwages = value; }
    }
    public string EPF
    {
        get { return _epf; }
        set { _epf = value; }
    }
    public string RateofHiger
    {
        get { return _RateofHigher; }
        set { _RateofHigher = value; }
    }
    public string EPFDiff
    {
        get { return _epfDiff; }
        set { _epfDiff = value; }
    }
    public string PensonFund
    {
        get { return _PensonFund; }
        set { _PensonFund = value; }
    }
    public string RefundofAdd
    {
        get { return _RefundofAdd; }
        set { _RefundofAdd = value; }
    }
    public string NonContribution
    {
        get { return _NonContribution; }
        set { _NonContribution = value; }
    }
    public string CompCdnumber
    {
        get { return _CompCdnumber; }
        set { _CompCdnumber = value; }
    }
    public string CompAdd1
    {
        get { return _CompAdd1; }
        set { _CompAdd1 = value; }
    }
    public string CompAdd2
    {
        get { return _CompAdd2; }
        set { _CompAdd2 = value; }
    }
    public string Location
    {
        get { return _Location; }
        set { _Location = value; }
    }
}
