﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data;
using Payroll;
using Payroll.Data;
using Payroll.Configuration;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html;
using iTextSharp.text.html.simpleparser;
using System.Data;
using Payroll;
using Payroll.Data;
using Payroll.Configuration;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html;
using iTextSharp.text.html.simpleparser;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class SalarySummary : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    string SessionAdmin;
    string Stafflabour;
    DateTime FromDate;
    DateTime ToDate;
    string SessionCcode;
    string SessionLcode;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionAdmin = Session["Isadmin"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        string ss = Session["UserId"].ToString();
        if (!IsPostBack)
        {
            //Department();
            //DropDwonCategory();
            int currentYear = Utility.GetFinancialYear;
            for (int i = 0; i < 10; i++)
            {
                ddlfinance.Items.Add(new System.Web.UI.WebControls.ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                //  ddlShowYear.Items.Add(new ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                currentYear = currentYear - 1;
            }
        }
        lblComany.Text = SessionCcode + " - " + SessionLcode;
        lblusername.Text = Session["Usernmdisplay"].ToString();

    }
    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }
    protected void btnclick_Click(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        dt = objdata.SalarySummary(ddlfinance.SelectedValue, SessionCcode, SessionLcode);
        gvload.DataSource = dt;
        gvload.DataBind();
    }
    protected void btnexport_Click(object sender, EventArgs e)
    {
        if (ddlexport.SelectedValue == "1")
        {
            //GridViewExportUtil.Export("Reportsearch.pdf", gvreportserarch);
            string attachment = "attachment; filename=SalarySummary.xls";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/ms-excel";
            StringWriter stw = new StringWriter();
            HtmlTextWriter htextw = new HtmlTextWriter(stw);
            gvload.RenderControl(htextw);
            Response.Write(stw.ToString());
            Response.End();
        }
        else if (ddlexport.SelectedValue == "2")
        {
            string attachment = "attachment; filename=SalarySummary.pdf";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/pdf";
            StringWriter stw = new StringWriter();
            HtmlTextWriter htextw = new HtmlTextWriter(stw);
            gvload.RenderControl(htextw);
            Document document = new Document(PageSize.A4, 10f, 10f, 10f, 10f);


            PdfWriter.GetInstance(document, Response.OutputStream);
            document.Open();
            StringReader str = new StringReader(stw.ToString());
            HTMLWorker htmlworker = new HTMLWorker(document);
            htmlworker.Parse(str);
            document.Close();
            Response.Write(document);
            Response.End();


        }
        else if (ddlexport.SelectedValue == "3")
        {
            string attachment = "attachment; filename=SalarySummary.doc";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/ms-excel";
            StringWriter stw = new StringWriter();
            HtmlTextWriter htextw = new HtmlTextWriter(stw);
            gvload.RenderControl(htextw);
            Response.Write(stw.ToString());
            Response.End();
        }
    }
    protected void btnEmp_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptEmpDownload.aspx");
    }
    protected void btnatt_Click(object sender, EventArgs e)
    {
        Response.Redirect("AttenanceDownload.aspx");
    }
    protected void btnLeave_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptLeaveSample.aspx");
    }
    protected void btnOT_Click(object sender, EventArgs e)
    {
        Response.Redirect("OTDownload.aspx");
    }
    protected void btnSal_Click(object sender, EventArgs e)
    {
        Response.Redirect("FrmDeductionDownload.aspx");
    }
}
