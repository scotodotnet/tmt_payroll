﻿ <%@ Page Language="C#" AutoEventWireup="true"  CodeFile="Default.aspx.cs" Inherits="_Default" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register TagPrefix="header" TagName="display" Src="~/Header.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="css/960.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/reset.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/text.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/login.css" rel="stylesheet" type="text/css" media="all" />
<title>Payroll Management System Login</title>



<link rel="Stylesheet" type="text/css" href="Login/css/all.css" />

<!-- Only for the login screen -->

<link rel="Stylesheet" type="text/css" href="Login/css/login.css" />


<script type="text/javascript" src="Login/js/jquery-1.6.2.min.js"></script>
<script type="text/javascript" src="Login/js/jquery-ui-1.8.16.custom.min.js"></script>
<script type="text/javascript" src="Login/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="Login/js/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
<script type="text/javascript" src="Login/js/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
<script type="text/javascript" src="Login/js/visualize.jQuery.js"></script>
<script type="text/javascript" src="Login/js/jquery.validationEngine.js"></script>
<script type="text/javascript" src="Login/js/jquery.validationEngine-en.js"></script>
<script type="text/javascript" src="Login/js/jquery.placeholder.min.js"></script>
<script type="text/javascript" src="Login/js/theme-init.js"></script>    
<meta charset="UTF-8">
<script type ="text/jscript" >
        function GoBack() {
            window.history.forward();
        }
   </script>
    <style type="text/css">
        .style1
        {
            height: 67px;
        }
    </style>
</head>

<body onload="GoBack();">
                           <body onload="GoBack()";>
<form id="form2" runat="server" >
  <cc1:ToolkitScriptManager runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true"
                                ID="ScriptManager1" EnablePartialRendering="true">
                            </cc1:ToolkitScriptManager>
<div id="page-wrap">
    <div id="inside">                        
            
             <div id="login" style="width:580px; height:300px;">                
                <div id="container">
                <table>
                <tr align="center">
                <td align="left" width="400">
                <img src="images/logo.png" alt="" width="100px" />
                </td>
                <td></td>
                <td align="right">
                    <h1 style="text-align:right;">Altius <span class="subtitle">Login</span></h1>
                    </td>
                    </tr>
                             </table>       
                    <form action="index.html" method="post">
                    
                        <div id="block" style="height:170px;">
                        <asp:UpdatePanel ID="Panel1" runat="server" >
                             <ContentTemplate>
                             <table>
                                <tr>
                                    <td align="center" valign="middle">
                                        <%--<img src="./images/login_key.png" alt="" Width="120" />--%>
                                        <img src="./new_images/icons/user-login.png" alt="" />
                                    </td>
                                    <td width="20">&nbsp;</td>
                                    <td width="300">
                                        <table>
                                            <tr>
                                                <td>
                                                    <div class="clearfix">
                                                        <label for="username">Company Name</label><br />
                                                        <asp:DropDownList ID="ddlCode" runat="server" AutoPostBack="true" Width="200" 
                                                            Height="28" onselectedindexchanged="ddlCode_SelectedIndexChanged"></asp:DropDownList>
                                                    </div>
                                                </td>
                                                <td width="50">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                                <td>
                                                    <div class="clearfix">
                                                        <label for="username">Location</label><br />
                                                        <asp:DropDownList ID="ddlLocation" runat="server" AutoPostBack="true" Width="200" Height="28">
                                                        </asp:DropDownList>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div class="clearfix">                                                    
                                                        <label for="username">Username</label><br />                                        
                                                        <asp:TextBox ID="txtusername" runat="server" CssClass="large validate[required]" Width="180"></asp:TextBox>
                                                        <cc1:TextBoxWatermarkExtender ID="textusername" runat="server" TargetControlID ="txtusername" WatermarkText="User Code"></cc1:TextBoxWatermarkExtender>
                                                    </div>
                                                </td>
                                                <td width="50">&nbsp;</td>
                                                <td class="style1">
                                                    <div class="clearfix">
                                                        <label for="password">Password</label><br />
                                                        <%-- <input id="password" class="large validate[required]" placeholder="Password">--%>
                                                        <asp:TextBox ID="txtpassword" runat="server" TextMode="Password" CssClass="large validate[required]"  Width="180"></asp:TextBox>
                                                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID ="txtpassword" WatermarkText="Password"></cc1:TextBoxWatermarkExtender>
                                                    </div>
                                                </td>
                                                <%--<td><img src="./images/login_key.png" alt="" Width="100"/></td>--%>
                                            </tr>
                                            <tr>
                                                <td style="text-align:right;" colspan="3">
                                                    <asp:Button ID="btnsave" runat="server" Text="Login" 
                                                        CssClass="button button-green Center space" Height="35" onclick="btnsave_Click" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr> 
                                   
                            </table>  
                            </ContentTemplate>
                        </asp:UpdatePanel>
                           
                            
                                                                             
                                                        
                        </div>  
                        <div class="clearfix"></div>
                        
                        <div>
                           
                            
                            
                            
                            
                            
                            
                            
                            
                  
                            
                        </div>     
                        
                        <div id="clear"></div>                                              
                    
                    </form>
                    
             
                </div> <!-- container -->   
             </div> <!-- login -->

             <div id="footer">
                &copy; Copyright 2012 by <a href="http://www.Altius.co.in" >Altius</a>. All rights reserved.
             </div> <!-- footer -->
         
         
    </div> <!-- page-wrap inside -->
</div>  <!-- page-wrap -->

</form>
</body>
</html>
