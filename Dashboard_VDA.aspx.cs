﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Payroll;
using Payroll.Configuration;
using Payroll.Data;
using System.IO;

using Altius.BusinessAccessLayer.BALDataAccess;

using System.Web.UI.DataVisualization.Charting;
using System.Data;
using System.Data.SqlClient;

public partial class Dashboard_absent : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    bool dd = false;
    string SessionAdmin;
    bool SearchErr = false;
    string Stafflabour;
    string SessionCcode;
    string SessionLcode;
    string Datetime_ser;
    static int month;
    int yr;
    DateTime mydate;
    string val = "1";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        string ss = Session["UserId"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();
        if (!IsPostBack)
        {
            Months_load();
            int currentYear = Utility.GetFinancialYear;
            for (int i = 0; i < 10; i++)
            {
                ddlFinance.Items.Add(new System.Web.UI.WebControls.ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                //  ddlShowYear.Items.Add(new ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                currentYear = currentYear - 1;
            }
            SettingServerDate();
            //DropDwonCategory();
            ddlRptType.Items.Add("---Select---");
            ddlRptType.Items.Add("Month");
            ddlRptType.Items.Add("Year");
            Chart_Call();
        }
        lblComany.Text = SessionCcode + " - " + SessionLcode;
        lblusername.Text = Session["Usernmdisplay"].ToString();
        
    }
    public void Months_load()
    {
        DataTable dt = new DataTable();
        dt = objdata.months_load();
        ddlMonths.DataSource = dt;
        ddlMonths.DataTextField = "Months";
        ddlMonths.DataValueField = "Months";
        ddlMonths.DataBind();
    }
    public void SettingServerDate()
    {
        Datetime_ser = objdata.ServerDate();
        //txtDate.Text = Datetime_ser;
    }
    public void Chart_Call()
    {
        SettingServerDate();
        DataTable dt = new DataTable();
        DataSet ds = new DataSet();
        SqlDataAdapter da = new SqlDataAdapter();
        if (val == "1")
        {
            
            mydate = Convert.ToDateTime(Datetime_ser).AddMonths(-1);
            month = Convert.ToInt32(mydate.Month.ToString());

            
        }
        else
        {
            if (ddlMonths.SelectedValue == "January")
            {
                month = 1;
            }
            else if (ddlMonths.SelectedValue == "February")
            {
                month = 2;
            }
            else if (ddlMonths.SelectedValue == "March")
            {
                month = 3;
            }
            else if (ddlMonths.SelectedValue == "April")
            {
                month = 4;
            }
            else if (ddlMonths.SelectedValue == "May")
            {
                month = 5;
            }
            else if (ddlMonths.SelectedValue == "June")
            {
                month = 6;
            }
            else if (ddlMonths.SelectedValue == "July")
            {
                month = 7;
            }
            else if (ddlMonths.SelectedValue == "August")
            {
                month = 8;
            }
            else if (ddlMonths.SelectedValue == "September")
            {
                month = 9;
            }
            else if (ddlMonths.SelectedValue == "October")
            {
                month = 10;
            }
            else if (ddlMonths.SelectedValue == "November")
            {
                month = 11;
            }
            else if (ddlMonths.SelectedValue == "December")
            {
                month = 12;
            }
        }
        #region Month
        string Mont = "";
        switch (month)
        {
            case 1:
                Mont = "January";
                break;

            case 2:
                Mont = "February";
                break;
            case 3:
                Mont = "March";
                break;
            case 4:
                Mont = "April";
                break;
            case 5:
                Mont = "May";
                break;
            case 6:
                Mont = "June";
                break;
            case 7:
                Mont = "July";
                break;
            case 8:
                Mont = "August";
                break;
            case 9:
                Mont = "September";
                break;
            case 10:
                Mont = "October";
                break;
            case 11:
                Mont = "November";
                break;
            case 12:
                Mont = "December";
                break;
            default:
                break;
        }
        #endregion
        if (month >= 4)
        {
            yr = Convert.ToInt32(ddlFinance.SelectedValue);
        }
        else
        {
            yr = Convert.ToInt32(ddlFinance.SelectedValue);
            yr = yr - 1;
        }
        if (ddlRptType.SelectedValue == "Year")
        {
            yr = Convert.ToInt32(ddlFinance.SelectedValue);
        }
        Chart1.Legends.Clear();
        Chart2.Legends.Clear();
        Chart3.Legends.Clear();
        string constr = ConfigurationManager.AppSettings["ConnectionString"];
        SqlConnection con = new SqlConnection(constr);
        string qry = "";
        if (ddlMonths.Enabled == true)
        {
            qry = "Select Sum(SD.VDA) as VDA,MD.DepartmentNm from SalaryDetails SD inner JOin EmployeeDetails ED on ED.EmpNo=SD.EmpNO inner Join " +
                           " MstDepartment MD on ED.Department= MD.DepartmentCd where SD.FinancialYear='" + ddlFinance.SelectedValue + "' and Month='" + Mont + "' group by MD.DepartmentNm";
        }
        else
        {
            qry = "Select Sum(SD.VDA) as VDA,MD.DepartmentNm from SalaryDetails SD inner JOin EmployeeDetails ED on ED.EmpNo=SD.EmpNO inner Join " +
                           " MstDepartment MD on ED.Department= MD.DepartmentCd where SD.FinancialYear='" + ddlFinance.SelectedValue + "' group by MD.DepartmentNm";
        }
        
        SqlCommand cmd = new SqlCommand(qry, con);
        da.SelectCommand = cmd;
        con.Open();
        da.Fill(ds);
        con.Close();
        dt = ds.Tables[0];
        Chart1.DataSource = dt;
        Chart1.Series["Series1"].XValueMember = "DepartmentNm";
        Chart1.Series["Series1"].YValueMembers = "VDA";

        this.Chart1.Series[0]["PieLabelStyle"] = "Outside";
        this.Chart1.Series[0].BorderWidth = 1;
        this.Chart1.Series[0].BorderColor = System.Drawing.Color.FromArgb(26, 59, 105);
        this.Chart1.Legends.Add("Legend1");
        this.Chart1.Legends[0].Enabled = true;
        this.Chart1.Legends[0].Docking = Docking.Bottom;
        this.Chart1.Legends[0].Alignment = System.Drawing.StringAlignment.Center;
        this.Chart1.Series[0].LegendText = "#PERCENT{P0}";
        this.Chart1.DataManipulator.Sort(PointSortOrder.Descending, Chart2.Series[0]);
        Chart1.DataBind();

        Chart2.DataSource = dt;
        Chart2.Series["Series1"].XValueMember = "DepartmentNm";
        Chart2.Series["Series1"].YValueMembers = "VDA";
        Chart2.Series[0].IsValueShownAsLabel = true;
        Chart2.DataBind();

        //Line Chart
        Chart3.DataSource = dt;
        Chart3.Series["Series1"].XValueMember = "DepartmentNm";
        Chart3.Series["Series1"].YValueMembers = "VDA";
        Chart3.Series[0].IsValueShownAsLabel = true;
        Chart3.DataBind();
    }
protected void  btnSearch_Click(object sender, EventArgs e)
{
    val = "2";
    Chart_Call();
    val = "1";
}
protected void ddlMonths_SelectedIndexChanged(object sender, EventArgs e)
{

}
protected void btnEmp_Click(object sender, EventArgs e)
{
    Response.Redirect("RptEmpDownload.aspx");
}
protected void btnatt_Click(object sender, EventArgs e)
{
    Response.Redirect("AttenanceDownload.aspx");
}
protected void btnLeave_Click(object sender, EventArgs e)
{
    Response.Redirect("RptLeaveSample.aspx");
}
protected void btnOT_Click(object sender, EventArgs e)
{
    Response.Redirect("OTDownload.aspx");
}
protected void btncontract_Click(object sender, EventArgs e)
{
    Response.Redirect("ContractRPT.aspx");
}
protected void ddlRptType_SelectedIndexChanged(object sender, EventArgs e)
{
    if (ddlRptType.SelectedValue == "Year")
    {
        ddlMonths.SelectedIndex = 0;
        ddlMonths.Enabled = false;
    }
    else
    {
        ddlMonths.SelectedIndex = 0;
        ddlMonths.Enabled = true;
    }
}
protected void btnSal_Click(object sender, EventArgs e)
{
    Response.Redirect("FrmDeductionDownload.aspx");
}
}
