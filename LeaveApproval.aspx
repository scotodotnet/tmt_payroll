﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="LeaveApproval.aspx.cs" Inherits="LeaveApproval" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">


<!-- Mirrored from www.ait.sk/uniadmin/ by HTTrack Website Copier/3.x [XR&CO'2010], Tue, 20 Jul 2010 00:37:25 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8"><!-- /Added by HTTrack -->
<head id="hea" runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<meta name="description"  content=""/>
<meta name="keywords" content=""/>
<meta name="robots" content="ALL,FOLLOW"/>
<meta name="Author" content="AIT"/> 
<meta http-equiv="imagetoolbar" content="no"/>
<title>Payroll Management Systems</title>

<link rel="stylesheet" href="css/reset.css" type="text/css"/>
<link rel="stylesheet" href="css/screen.css" type="text/css"/>
<link rel="stylesheet" href="css/fancybox.css" type="text/css"/>
<link rel="stylesheet" href="css/jquery.wysiwyg.css" type="text/css"/>
<link rel="stylesheet" href="css/jquery.ui.css" type="text/css"/>
<link rel="stylesheet" href="css/visualize.css" type="text/css"/>
<link rel="stylesheet" href="css/visualize-light.css" type="text/css"/>
<link rel="Stylesheet" href="css/form.css" type="text/css" />
<link rel="Stylesheet" href="css/orange.css" type="text/css" />
<!--[if IE 7]>
	<link rel="stylesheet" type="text/css" href="css/ie7.css" />
<![endif]-->	

<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery.visualize.js"></script>
<script type="text/javascript" src="js/jquery.wysiwyg.js"></script>
<script type="text/javascript" src="js/tiny_mce/jquery.tinymce.js"></script>
<script type="text/javascript" src="js/jquery.fancybox.js"></script>
<script type="text/javascript" src="js/jquery.idtabs.js"></script>
<script type="text/javascript" src="js/jquery.datatables.js"></script>
<script type="text/javascript" src="js/jquery.jeditable.js"></script>
<script type="text/javascript" src="js/jquery.ui.js"></script>

<script type="text/javascript" src="js/excanvas.js"></script>
<script type="text/javascript" src="js/cufon.js"></script>
<script type="text/javascript" src="js/Geometr231_Hv_BT_400.font.js"></script>
<script type="text/javascript" src="js/script.js"></script>
 <script type="text/javascript" >
     function SelectheaderCheckboxes(cbSelectAll) 
     {

         var gvcheck = document.getElementById('GvLeaveApproval');
         var i;
         //Condition to check header checkbox selected or not if that is true checked all checkboxes
         if (cbSelectAll.checked) {
             for (i = 0; i < gvcheck.rows.length; i++) {
                 var inputs = gvcheck.rows[i].getElementsByTagName('input');
                 inputs[0].checked = true;
             }
         }
         //if condition fails uncheck all checkboxes in gridview
         else {
             for (i = 0; i < gvcheck.rows.length; i++) {
                 var inputs = gvcheck.rows[i].getElementsByTagName('input');
                 inputs[0].checked = false;
             }
         }
     }
     //function to check header checkbox based on child checkboxes condition
     function Selectchildcheckboxes(header) {
         var ck = header;
         var count = 0;
         var gvcheck = document.getElementById('GvLeaveApproval');
         var headerchk = document.getElementById(header);
         var rowcount = gvcheck.rows.length;
         //By using this for loop we will count how many checkboxes has checked
         for (i = 1; i < gvcheck.rows.length; i++) {
             var inputs = gvcheck.rows[i].getElementsByTagName('input');
             if (inputs[0].checked) {
                 count++;
             }
         }
         //Condition to check all the checkboxes selected or not
         if (count == rowcount - 1) {
             headerchk.checked = true;
         }
         else {
             headerchk.checked = false;
         }
     }
    </script>
</head>

 
<body>
<div class="clear">
	
	<div class="sidebar"> <!-- *** sidebar layout *** -->
		<div class="logo clear">
			
				<img src="images/logo12.png" alt="" class="picture" />
			
			
		</div>
		
		<div class="Elaya">
			 <ul >
                    <li><a href="EmployeeRegistration.aspx" >Home</a></li>
                    <li><a href="EmployeeRegistration.aspx"><span class="text">Employee Registration</span></a></li>
                    <li><a href="OfficalProfileDetails.aspx" ><span class="text">Official Profile </span></a></li>
                    <li><a href="LeaveDetails.aspx">Leave Details</a></li>
                    <li><a href="ApplyForLeave.aspx">Apply For Leave</a></li>
                    <li><a href="LeaveApproval.aspx">Leave Approval</a></li>
                    <li><a href="LeaveApprovalForStaff.aspx">Leave Approval For Staff</a></li>
                    <li><a href="SalaryRegistration.aspx">Salary Details</a></li>
                    <li><a href="MstEmployeeType.aspx">Masters</a></li>
                    <li><a href="#">Reports</a></li>
                    <li><a href="Default.aspx">Logout</a></li>
                    
                    
                </ul>
		</div>
	</div>
	
	
	<div class="main"> <!-- *** mainpage layout *** -->
	<div class="main-wrap">
		<div class="header clear">
			<ul class="links clear">
			<li></li>
			<li><h2><span class="title">Payroll Management System</span></h2></li>
			<li><a href="#"> <span class="text"></span></a></li>
			<li><a href="#"><span class="text"> </span></a></li>
			<li><a href="#"> <span class="text"></span></a></li>
			</ul>
		</div>
		 <!-- *** User Name layout *** -->
                    <div class="header1 clear">
                    <ul class="links1 clear">
                        <li></li>
                        <li><h4><span class="title"><asp:Label ID="lblusername" runat="server" ></asp:Label></span></h4></li>
                     
                    </ul>
                    
              
                   </div>
                   
		<div class="page clear">
						<div class="main-icons clear">
						 <ul class="links clear">
			        <li></li>
			        <li><a href="#"> <span class="text"></span></a></li>
			        <li><a href="#"><span class="text"> </span></a></li>
			        <li><a href="#"> <span class="text"></span></a></li>
			        <li><a href="#"> <span class="text"></span></a></li>
			        <li><a href="#"><span class="text"> </span></a></li>
        		
			        </ul>
				<%--<ul class="clear">
				<li><a href="EmployeeRegistration.aspx" s><img src="images/ico_folder_64.png" class="icon" alt="" /><span class="text">Employee Registration</span></a></li>
				 <li><a href="#"> <span class="text"></span></a></li>
				<li><a href="OfficalProfileDetails.aspx" ><img src="images/op2.jpg" class="icon" alt="" /><span class="text">Official Profile</span></a></li>
				 <li><a href="#"> <span class="text"></span></a></li>
				<li><a href="LeaveDetails.aspx" ><img src="images/date/calendar_week .png" class="icon" alt="" /><span class="text">Leave Details</span></a></li>
				 <li><a href="#"> <span class="text"></span></a></li>
				<li><a href="SalaryRegistration.aspx" ><img src="images/rs5.jpg" class="icon" alt="" /><span class="text">Salary Details</span></a></li>
				 <li><a href="#"> <span class="text"></span></a></li>
                
				</ul>--%>
			</div>
			
			<!-- MODAL WINDOW -->
			<div id="modal" class="modal-window">
				<!-- <div class="modal-head clear"><a onclick="$.fancybox.close();" href="javascript:;" class="close-modal">Close</a></div> -->
				
				<div class="notification note-info">
					<a href="#" class="close" title="Close notification"><span>close</span></a>
					<span class="icon"></span>
					
				</div>
				
				</div>
			
			<!-- CONTENT BOXES -->
			<div class="content-box">
				<div class="box-header clear">
				<%--	<ul class="tabs clear">
						<li><a href="#data-table"></a></li>
						<li><a href="#table"></a></li>
						<li><a href="#forms"></a></li>
					</ul>--%>
					
					
				</div>
				
				<div class="box-body clear">
					<!-- TABLE -->
					<div id="data-table">
  <form id="form1" runat="server" class="form">
           <cc1:ToolkitScriptManager runat="Server" EnableScriptGlobalization="true" EnableScriptLocalization="true" ID="ScriptManager1" />

    <div>
    <div >
    
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
           
           <table>
           <tr align="center">
           <td colspan="3"><asp:RadioButtonList ID="rbtnApprovalstatus" runat="server" RepeatColumns="2" 
                   Width="280" AutoPostBack="True" 
                   onselectedindexchanged="rbtnApprovalstatus_SelectedIndexChanged">
           <asp:ListItem Text="Approval" Value="1"></asp:ListItem>
           <asp:ListItem Text ="Status" Value="2"></asp:ListItem>
           </asp:RadioButtonList></td>
           </tr>
           <tr id="PanelApprovalLeave" runat="server" visible="false">
           <td colspan="3"><asp:GridView ID="GvLeaveApproval" runat="server" AutoGenerateColumns="false">
           <Columns>
           <asp:TemplateField>
           <HeaderTemplate>Employee No</HeaderTemplate>
           <ItemTemplate>
           <asp:Label ID="lblempno" runat="server" Text='<%# Eval("EmpNo") %>'></asp:Label>
           </ItemTemplate>
           </asp:TemplateField>
               <asp:TemplateField>
           <HeaderTemplate>Employee Name</HeaderTemplate>
           <ItemTemplate>
           <asp:Label ID="lblempname" runat="server" Text='<%# Eval("EmpName") %>'></asp:Label>
           </ItemTemplate>
           </asp:TemplateField>
               <asp:TemplateField>
           <HeaderTemplate>Department</HeaderTemplate>
           <ItemTemplate>
           <asp:Label ID="lbldepartment" runat="server" Text='<%# Eval("DepartmentNm") %>'></asp:Label>
           </ItemTemplate>
           </asp:TemplateField>
               <asp:TemplateField>
           <HeaderTemplate>From Date</HeaderTemplate>
           <ItemTemplate>
           <asp:Label ID="lblfromdate" runat="server" Text='<%# Eval("FromDate") %>'></asp:Label>
           </ItemTemplate>
           </asp:TemplateField>
               <asp:TemplateField>
           <HeaderTemplate>To Date</HeaderTemplate>
           <ItemTemplate>
           <asp:Label ID="lbltodate" runat="server" Text='<%# Eval("ToDate") %>'></asp:Label>
           </ItemTemplate>
           </asp:TemplateField>
           <asp:TemplateField>
           <HeaderTemplate>No of Leaves</HeaderTemplate>
           <ItemTemplate>
         <%--  <asp:Label ID="lblnoofleaves" runat="server" Text='<%# Eval("NumberofLeave") %>'></asp:Label>--%>
           <asp:TextBox ID="txtnoofleaves" runat="server" Text='<%# Eval("NumberofLeave") %>' Width="50"></asp:TextBox>
           </ItemTemplate>
           </asp:TemplateField>
               <asp:TemplateField>
           <HeaderTemplate>LeaveCode</HeaderTemplate>
           <ItemTemplate>
           <asp:Label ID="lblleavecode" runat="server" Text='<%# Eval("LeaveCode") %>'></asp:Label>
           </ItemTemplate>
           </asp:TemplateField>
            <asp:TemplateField >
           <HeaderTemplate> 
           <asp:CheckBox ID="cbSelectAll" runat="server" onclick="javascript:SelectheaderCheckboxes(this);"  />
               All
           </HeaderTemplate>
            <ItemTemplate>
                 <asp:CheckBox  ID="chkclear" runat="server"  />
            </ItemTemplate>
           
        </asp:TemplateField>
           </Columns>
           </asp:GridView></td>
           </tr>
           <tr id="PanelStatusButton" runat="server" visible="false">
           <td><asp:Label ID="lblmonth" runat="server" Text="Month" Font-Bold="true"></asp:Label></td>
           <td><asp:DropDownList ID="ddlmonth" runat="server" Width="180" Height="30" ></asp:DropDownList></td>
           <td><asp:Button ID="btnsearch" runat="server" Text="Search" Height="28" Width="75"
                   onclick="btnsearch_Click"  /></td>
           </tr>
            <tr id="PanelStatus" runat="server" visible="false">
           <td colspan="3"><asp:GridView ID="gvApplyLeaveStatus" runat="server" AutoGenerateColumns="false">
           <Columns>
           <asp:TemplateField>
           <HeaderTemplate>Employee No</HeaderTemplate>
           <ItemTemplate>
           <asp:Label ID="lblempno" runat="server" Text='<%# Eval("EmpNo") %>'></asp:Label>
           </ItemTemplate>
           </asp:TemplateField>
               <asp:TemplateField>
           <HeaderTemplate>Employee Name</HeaderTemplate>
           <ItemTemplate>
           <asp:Label ID="lblempname" runat="server" Text='<%# Eval("EmpName") %>'></asp:Label>
           </ItemTemplate>
           </asp:TemplateField>
               <asp:TemplateField>
           <HeaderTemplate>Department</HeaderTemplate>
           <ItemTemplate>
           <asp:Label ID="lbldepartment" runat="server" Text='<%# Eval("DepartmentNm") %>'></asp:Label>
           </ItemTemplate>
           </asp:TemplateField>
           <asp:TemplateField>
           <HeaderTemplate>No of Leaves</HeaderTemplate>
           <ItemTemplate>
           <asp:Label ID="lblnoofleaves" runat="server" Text='<%# Eval("NumberofLeave") %>'></asp:Label>
           </ItemTemplate>
           </asp:TemplateField>
               <asp:TemplateField>
           <HeaderTemplate>Status </HeaderTemplate>
           <ItemTemplate>
           <asp:Label ID="lblstatus" runat="server" Text='<%# Eval("Status") %>'></asp:Label>
           </ItemTemplate>
           </asp:TemplateField>
            
           </Columns>
           </asp:GridView></td>
           </tr>
           <tr id="PanelApprovalLeaveButton" runat="server" visible="false">
           <td colspan="3"><asp:Button ID="btnapproval" runat="server" Text="Approval" 
                   onclick="btnapproval_Click" />
           <asp:Button ID="btnrejected" runat="server" Text="Rejected" 
                   onclick="btnrejected_Click" /></td>
           </tr>
           </table>
  
         </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    </div>
    </form>
					
					
						<form method="post" action="#">
						
					
						
					
						</form>
					</div><!-- /#table -->
					
					<!-- TABLE -->
					<!-- /#table -->
					
					<!-- Custom Forms -->
					<!-- /#forms -->
				</div> <!-- end of box-body -->
			</div> <!-- end of content-box -->
			
		
			
		
			
			
			
			
		</div><!-- end of page -->
		
		<div class="footer clear">
			<span class="copy"><strong>© 2012 Copyright by <a href="http://www.altius.co.in"/>Altius Infosystems.</a></strong></span> Powered by <a href="http://www.altius.co.in/">Altius.</a>
		</div>
	</div>
	</div>
</div>
<script type="text/javascript">

    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-12958851-7']);
    _gaq.push(['_trackPageview']);

    (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();

</script>
</body>

<!-- Mirrored from www.ait.sk/uniadmin/ by HTTrack Website Copier/3.x [XR&CO'2010], Tue, 20 Jul 2010 00:38:01 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8"><!-- /Added by HTTrack -->
</html>
