﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;
using Payroll;
using System.Drawing;
using iTextSharp.text.pdf;
using iTextSharp.text.html;
using iTextSharp.text.html.simpleparser;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class FOrm10_1 : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    string SessionAdmin;
    string Stafflabour;
    protected void Page_Load(object sender, EventArgs e)
    {
        //string ss = Session["UserId"].ToString();
        if (!IsPostBack)
        {
            
            if (Session["UserId"] == null)
            {
                Response.Redirect("Default.aspx");
                Response.Write("Your session expired");
            }
            string ss = Session["UserId"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            DropDwonCategory();
            Months_load();
            int currentYear = Utility.GetFinancialYear;
            for (int i = 0; i < 10; i++)
            {
                ddlFinance.Items.Add(new ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                //  ddlShowYear.Items.Add(new ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                currentYear = currentYear - 1;
            }
        }
        lblusername.Text = Session["Usernmdisplay"].ToString();
    }
    public void DropDwonCategory()
    {
        DataTable dtcate = new DataTable();
        dtcate = objdata.DropDownCategory();
        ddlCategory.DataSource = dtcate;
        ddlCategory.DataTextField = "CategoryName";
        ddlCategory.DataValueField = "CategoryCd";
        ddlCategory.DataBind();
    }
    public void Months_load()
    {
        DataTable dt = new DataTable();
        dt = objdata.months_load();
        ddlMonths.DataSource = dt;
        ddlMonths.DataTextField = "Months";
        ddlMonths.DataValueField = "Months";
        ddlMonths.DataBind();
    }
    protected void ddlCategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable dtempty = new DataTable();
        ddldpt.DataSource = dtempty;
        ddldpt.DataBind();
        gvform10.DataSource = dtempty;
        gvform10.DataBind();
        if (ddlCategory.SelectedValue == "1")
        {
            Stafflabour = "S";
        }
        else if (ddlCategory.SelectedValue == "2")
        {
            Stafflabour = "L";
        }
        else
        {
            Stafflabour = "0";
        }
        DataTable dtDip = new DataTable();
        dtDip = objdata.DropDownDepartment_category(Stafflabour);
        if (dtDip.Rows.Count > 1)
        {
            ddldpt.DataSource = dtDip;
            ddldpt.DataTextField = "DepartmentNm";
            ddldpt.DataValueField = "DepartmentCd";
            ddldpt.DataBind();
        }
        else
        {
            DataTable dt = new DataTable();
            ddldpt.DataSource = dtempty;
            ddldpt.DataBind();
        }
    }
    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }
    protected void ddldpt_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable dtempty = new DataTable();
        gvform10.DataSource = dtempty;
        gvform10.DataBind();
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        DataTable dtempty = new DataTable();
        gvform10.DataSource = dtempty;
        gvform10.DataBind();
        bool ErrFlag = false;
        bool download = false;
        if (ddlCategory.SelectedValue == "0")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Category.');", true);

            //System.Windows.Forms.MessageBox.Show("Select the Category", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            ErrFlag = true;
        }
        else if (ddldpt.SelectedValue == "0")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Department.');", true);

            //System.Windows.Forms.MessageBox.Show("Select the Department", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            ErrFlag = true;
        }
        else if (ddlMonths.SelectedValue == "0")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Months.');", true);

            //System.Windows.Forms.MessageBox.Show("Select the Month", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            ErrFlag = true;
        }
        if (!ErrFlag)
        {
            if (ddlCategory.SelectedValue == "1")
            {
                Stafflabour = "S";
            }
            else if (ddlCategory.SelectedValue == "2")
            {
                Stafflabour = "L";
            }

            DataTable dt = new DataTable();
            if (SessionAdmin == "1")
            {
                dt = objdata.Form10_sp(Stafflabour, ddldpt.SelectedValue, ddlMonths.SelectedValue, ddlFinance.SelectedValue);
            }
            else
            {
                dt = objdata.Form10_sp_user(Stafflabour, ddldpt.SelectedValue, ddlMonths.SelectedValue, ddlFinance.SelectedValue);

            }
            gvform10.DataSource = dt;
            gvform10.DataBind();
            if (dt.Rows.Count > 0)
            {
                int yr = 0;
                if ((ddlMonths.SelectedValue == "January") || (ddlMonths.SelectedValue == "feburary") || (ddlMonths.SelectedValue == "March"))
                {
                    yr = Convert.ToInt32(ddlFinance.SelectedValue);
                    yr = yr + 1;
                }
                else
                {
                    yr = Convert.ToInt32(ddlFinance.SelectedValue);
                }
                string attachment = "attachment; filename=Form10.xls";
                Response.ClearContent();
                Response.AddHeader("content-disposition", attachment);
                Response.ContentType = "application/ms-excel";

                StringWriter stw = new StringWriter();
                HtmlTextWriter htextw = new HtmlTextWriter(stw);
                gvform10.RenderControl(htextw);
                Response.Write("<table>");
                Response.Write("<tr align='Center'>");
                Response.Write("<td colspan='7'>");
                Response.Write("Form 10");
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("<tr align='Center'>");
                Response.Write("<td colspan='7'>");
                Response.Write("THE EMPLOYEES PROVIDENT FUNDS SCHEME, 1952");
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("<tr align='Center'>");
                Response.Write("<td colspan='7'>");
                Response.Write("[paragraph 36(2)(a) & (b) EMPLOYEES PENSION SCHEME, 1995 {Paragraph 20(2)}]");
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("<tr align='Center'>");
                Response.Write("<td colspan='7'>");
                Response.Write("Return of the members leaving Service during the month of : " + ddlMonths.SelectedValue + " " + yr);
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("<tr align='Center'>");
                Response.Write("<td colspan='7'>");
                Response.Write("Name & Address of the Factory / Establishment : ");
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("<tr align='Center'>");
                Response.Write("<td colspan='7'>");

                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("</table>");
                Response.Write(stw.ToString());
                Response.Write("<table>");
                Response.Write("<tr align='Center'>");
                Response.Write("<td colspan='7'>");

                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("<tr align='Center'>");
                Response.Write("<td colspan='7'>");

                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("<tr align='Center'>");
                Response.Write("<td colspan='7'>");

                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("<tr>");
                Response.Write("<td align='left'>");
                Response.Write("Date : " + DateTime.Today.ToShortDateString());
                Response.Write("</td>");
                Response.Write("<td colspan='6' align='right'>");
                Response.Write("Signature of the Employer or the authorised Officer stamp of the Factory / Establishment");
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("<tr >");
                Response.Write("<td colspan='7'>");
                string dd = "Please state that member is (a)retiring according to para 69,(i)(a) or (b) of the scheme; (b) Leaving India for the Permenant settlement abroad; (c)retrenched;";
                string dd1 = "(d) ordinarily dismissed for the serious of willful and misconduct; (e)discharged; (f)resigning from or Leaving service; (g) taking up Employement Else where";
                string dd2 = " (g) taking up Employement Else where (the name and address of the new Employer should be stated);(h) dead";
                Response.Write(dd + " " + dd1 + " " + dd2);
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("<tr >");
                Response.Write("<td colspan='7'>");
                string ed = "(1) A request for deduction from the account of a member dismissed for serious and willful misconduct should be reported by the following";
                string ed1 = "Certified that the member mentioned at Sr. No.                     shri.                           was dismissed from the service for willful misconduct";
                string ed2 = "I recommend that employer's contribution for             should be fortified from his account in the fund. A copy of order of dismissal is enclosed";
                Response.Write(ed + " " + ed1 + " " + ed2);
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("<tr >");
                Response.Write("<td colspan='7'>");
                Response.Write("(2) Incase of discharge from service, the following certificate should be filled");
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("<tr >");
                Response.Write("<td colspan='7'>");
                string fd = "Certified that the member mentioned at Sr. No.                     shri.                        was paid /unpaid retrenchment compensation of Rs.          ";
                Response.Write(fd);
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("</table>");
                Response.End();
                download = true;

            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('No Data Found.');", true);

                //System.Windows.Forms.MessageBox.Show("No Data Found", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            }
            if (download == true)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Exported Successfully in the name Form10.xls');", true);

                //System.Windows.Forms.MessageBox.Show("Exported Successfully", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            }
        }
    }
}
