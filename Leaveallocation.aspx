﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Leaveallocation.aspx.cs" Inherits="Leaveallocation" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Leave Allocation</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <table>
    <tr align="center">
    <td colspan="2">
    <asp:RadioButtonList ID="rbtnstfflbrothers" runat="server" RepeatColumns="3" 
            Width="150" AutoPostBack="true" 
            onselectedindexchanged="rbtnstfflbrothers_SelectedIndexChanged">
    <asp:ListItem Text="Staff" Value="1" Selected="False"></asp:ListItem>
    <asp:ListItem Text="Labour" Value="2" Selected="False"></asp:ListItem>
    <asp:ListItem Text="Others" Value="3" Selected="False"></asp:ListItem>
    </asp:RadioButtonList></td>
    </tr>
    </table>
     
  <asp:Panel ID="Panel1stflbrothers" runat="server" Visible="false" Height="91px" >
  <table align="center" cellpadding="0" cellspacing="0" border="0">
  <tr>
            <td>&nbsp;</td>
    </tr>
   
  </table>
 
  <table>
  <tr>
  <td>
  <asp:Label ID="lblDepartment" runat= "server" Text="Department"></asp:Label>

          <asp:DropDownList ID="ddlDepartment" runat="server" Width="215" Height="30" 
          AutoPostBack="true" ontextchanged="ddlDepartment_Textchanged" ></asp:DropDownList>
  </td>
  </tr>
  
          </asp:Panel>
          <tr>
          <asp:Panel ID="panelbtn" runat="server" Visible="false">
          <td><asp:Button ID="btnindividual" runat="server" Text="Individual" 
                  onclick="btnindividual_Click" /></td>
          <td><asp:Button ID="btnall" runat="server" Text="All"/></td>
          </asp:Panel>
          </tr>
          <tr>
          <td><asp:Label ID="lblempno" runat="server" Text="EmployeeNo"></asp:Label></td>
          <td><asp:TextBox ID="txtempno" runat="server"></asp:TextBox></td>
          <td><asp:Button ID="btnSearch" runat="server" Text="Search" 
            onclick="btnSearch_Click" /></td>
          </tr>
  <tr>
  <td>
        <asp:GridView ID="gvLeaveallocation" runat="server" AutoGenerateColumns="false" 
                    onrowcancelingedit="RowEdit_Canceling" onrowediting="RowEdit_Editing" 
                    onrowupdating="RowEdit_Updating">

    <Columns>
    <asp:TemplateField>
    <HeaderTemplate>Sl.No</HeaderTemplate>
    <ItemTemplate><%# Container.DataItemIndex +1 %></ItemTemplate>
    </asp:TemplateField> 
    <asp:TemplateField>
    <HeaderTemplate>Labour id</HeaderTemplate>
    <ItemTemplate>
    <asp:Label ID="lblid" runat="server" Text='<%# Eval("EmpNo")%>'></asp:Label>
    </ItemTemplate>
    </asp:TemplateField>
    <asp:TemplateField>
    <HeaderTemplate>Labour name</HeaderTemplate>
    <ItemTemplate>
    <asp:Label ID="lblname" runat="server" Text='<%# Eval("EmpName")%>'></asp:Label>
    </ItemTemplate>
    </asp:TemplateField>
    <asp:TemplateField>
    <HeaderTemplate>Designation</HeaderTemplate>
    <ItemTemplate>
    <asp:Label ID="lbldesignation" runat="server" Text='<%# Eval("Designation")%>'></asp:Label>
    </ItemTemplate>
    </asp:TemplateField>
    <asp:TemplateField>
    <HeaderTemplate>Leave allocation</HeaderTemplate>
    <ItemTemplate>
    <asp:TextBox ID="txtallocation" runat="server"></asp:TextBox>
    </ItemTemplate>
    </asp:TemplateField>
    <asp:TemplateField>
    <HeaderTemplate>check</HeaderTemplate>
    <ItemTemplate>
    <asp:CheckBox ID="chklabour" runat="server" />
    </ItemTemplate>
    </asp:TemplateField>
    <asp:CommandField ButtonType="Button" CancelText="Cancel" UpdateText="Update" EditText="Edit" ShowEditButton="true" />
    </Columns>
  
    </asp:GridView>
 
    </td>
    </tr>
    
    
    <tr>
    <td><asp:Button ID="btnsave" Text="Save" runat="server" onclick="btnsave_Click1"/></td>
    </tr>
    </table>
  
    </div>
    </form>
</body>
</html>
