﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Header.ascx.cs" Inherits="Header" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<!-- Mirrored from www.ait.sk/uniadmin/ by HTTrack Website Copier/3.x [XR&CO'2010], Tue, 20 Jul 2010 00:37:25 GMT -->
<!--   Added by HTTrack -->
<meta http-equiv="content-type" content="text/html;charset=UTF-8">
<!-- /Added by HTTrack -->
<head id="hea" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <meta name="robots" content="ALL,FOLLOW" />
    <meta name="Author" content="AIT" />
    <meta http-equiv="imagetoolbar" content="no" />
    <title>Payroll Management Systems</title>
    <link rel="stylesheet" href="css/reset.css" type="text/css" />
    <link rel="stylesheet" href="css/screen.css" type="text/css" />
    <link rel="stylesheet" href="css/fancybox.css" type="text/css" />
    <link rel="stylesheet" href="css/jquery.wysiwyg.css" type="text/css" />
    <link rel="stylesheet" href="css/jquery.ui.css" type="text/css" />
    <link rel="stylesheet" href="css/visualize.css" type="text/css" />
    <link rel="stylesheet" href="css/visualize-light.css" type="text/css" />
    <link rel="Stylesheet" href="css/form.css" type="text/css" />
    <!--[if IE 7]>
	<link rel="stylesheet" type="text/css" href="css/ie7.css" />
<![endif]-->

    <script type="text/javascript" src="js/jquery.js"></script>

    <script type="text/javascript" src="js/jquery.visualize.js"></script>

    <script type="text/javascript" src="js/jquery.wysiwyg.js"></script>

    <script type="text/javascript" src="js/tiny_mce/jquery.tinymce.js"></script>

    <script type="text/javascript" src="js/jquery.fancybox.js"></script>

    <script type="text/javascript" src="js/jquery.idtabs.js"></script>

    <script type="text/javascript" src="js/jquery.datatables.js"></script>

    <script type="text/javascript" src="js/jquery.jeditable.js"></script>

    <script type="text/javascript" src="js/jquery.ui.js"></script>

    <script type="text/javascript" src="js/excanvas.js"></script>

    <script type="text/javascript" src="js/cufon.js"></script>

    <script type="text/javascript" src="js/Geometr231_Hv_BT_400.font.js"></script>

    <script type="text/javascript" src="js/script.js"></script>

</head>
<body>
    <div class="clear">
        <div class="sidebar">
            <!-- *** sidebar layout *** -->
            <div class="logo clear">
                <img src="images/logo.png" alt="" class="picture" />
                <span class="title">Altius Infosystems</span> <span class="text">Altius</span>
            </div>
            
        
            
           <div class="page clear">
            <div class="Elaya">
                <ul >
                    <li>
                    <a href="EmployeeRegistration.aspx"><span class="text">Employee Registration</span></a>
                    </li>
                    <li>
                    <a href="OfficalProfileDetails.aspx" ><span class="text">Official Profile </span></a>
                
                    </li>
                    <li><a href="LeaveDetails.aspx">Leave Details</a></li>
                    <li><a href="SalaryRegistration.aspx">Salary Details</a></li>
               <%--     <li><a href="#">Masters</a>
                        <ul>
                            <li><a href="MstDistrict.aspx">District Master</a></li>
                            <li><a href="MstEmployeeType.aspx">Employee Type Master</a></li>
                            <li><a href="MstLeave.aspx">Leave Master</a></li>
                            <li><a href="MstTaluk.aspx">Qualification Master</a></li>
                            <li><a href="MstTaluk.aspx">Taluk</a></li>
                            <li><a href="UserRegistration.aspx">Users</a></li>
                        </ul>
                    </li>--%>
                    <li><a href="#">Reports</a></li>
                </ul>
            </div>
            </div>
        </div>
        <div class="main">
            <!-- *** mainpage layout *** -->
            <div class="main-wrap">
                <div class="header clear">
                    <ul class="links clear">
                        <li></li>
                        <li><a href="#"><span class="text"></span></a></li>
                        <li><a href="#"><span class="text"></span></a></li>
                        <li><a href="#"><span class="text"></span></a></li>
                    </ul>
                </div>
                <div class="page clear">
                    <div class="main-icons clear">
                        <ul class="clear">
                            <li><a href="EmployeeRegistration.aspx"> <img src="images/ico_folder_64.png" class="icon" alt="" /><span class="text">Employee Registration</span></a></li>
                            <li><a href="OfficalProfileDetails.aspx"><img src="images/ico_page_64.png" class="icon" alt="" /><span class="text">Official Profile</span></a></li>
                            <li><a href="LeaveDetails.aspx"><img src="images/date/calendar_week .png" class="icon" alt="" /><span class="text">Leave  Details</span></a></li>
                            <li><a href="SalaryRegistration.aspx"><img src="images/ico_clock_64.png" class="icon" alt="" /><span class="text">Salary Details</span></a></li>
                            <li><a href="UserRegistration.aspx"><img src="images/ico_users_64.png" class="icon" alt="" /><span class="text">Users</span></a></li>
                            <li><a href="#modal" class="modal-link">
                                <img src="images/ico_chat_64.png" class="icon" alt="" /><span class="text">Reports</span></a></li>
                        </ul>
                    </div>
                    <!-- MODAL WINDOW -->
                    <div id="modal" class="modal-window">
                        <!-- <div class="modal-head clear"><a onclick="$.fancybox.close();" href="javascript:;" class="close-modal">Close</a></div> -->
                        <div class="notification note-info">
                            <a href="#" class="close" title="Close notification"><span>close</span></a> <span
                                class="icon"></span>
                        </div>
                    </div>
                    <!-- CONTENT BOXES -->
                    <div class="content-box">
                        <div class="box-header clear">
                            <%--	<ul class="tabs clear">
						<li><a href="#data-table"></a></li>
						<li><a href="#table"></a></li>
						<li><a href="#forms"></a></li>
					</ul>--%>
                        </div>
                        <div class="box-body clear">
                            <!-- TABLE -->
                            
                        </div>
                    </div>
                </div>
                <!-- end of page -->
                <div class="footer clear">
                    <span class="copy"><strong>© 2012 Copyright by <a href="http://www.altius.co.in" />Altius
                        Infosystems.</a></strong></span> Powered by <a href="http://www.altius.co.in/">Altius.</a>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-12958851-7']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

    </script>

</body>
<!-- Mirrored from www.ait.sk/uniadmin/ by HTTrack Website Copier/3.x [XR&CO'2010], Tue, 20 Jul 2010 00:38:01 GMT -->
<!-- Added by HTTrack -->
<meta http-equiv="content-type" content="text/html;charset=UTF-8">
<!-- /Added by HTTrack -->
</html>
