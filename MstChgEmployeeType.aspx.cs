﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Payroll;
using Payroll.Data;
using Payroll.Configuration;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html;
using iTextSharp.text.html.simpleparser;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class MstChgEmployeeType : System.Web.UI.Page
{
    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    SqlConnection con;
    BALDataAccess objdata = new BALDataAccess();
    DateTime TransDate;
    string SessionAdmin;
    string Stafflabour;
    string SessionCcode;
    string SessionLcode;
    protected void Page_Load(object sender, EventArgs e)
    {
        con = new SqlConnection(constr);
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionAdmin = Session["Isadmin"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        lblComany.Text = SessionCcode + " - " + SessionLcode;
        lblusername.Text = Session["Usernmdisplay"].ToString();
        string ss = Session["UserId"].ToString();
        if (!IsPostBack)
        {
            DropDwonCategory();
            Department();
            EmpType();
           
        }
    }
    public void EmpType()
    {
        DataTable dt = new DataTable();
        dt = objdata.EmpType_LOAD();
        ddemployeetype.DataSource = dt;
        ddemployeetype.DataTextField = "EmpType";
        ddemployeetype.DataValueField = "EmpTypeCd";
        ddemployeetype.DataBind();

        
    }
    public void Department()
    {
        DataTable dtDip = new DataTable();
        dtDip = objdata.DDdEPARTMENT_LOAD();
        ddldepartment.DataSource = dtDip;
        ddldepartment.DataTextField = "DepartmentNm";
        ddldepartment.DataValueField = "DepartmentCd";
        ddldepartment.DataBind();
    }
    public void DropDwonCategory()
    {
        DataTable dtcate = new DataTable();
        dtcate = objdata.DropDownCategory();
        ddlcategory.DataSource = dtcate;
        ddlcategory.DataTextField = "CategoryName";
        ddlcategory.DataValueField = "CategoryCd";
        ddlcategory.DataBind();
    }
    protected void btnEmp_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptEmpDownload.aspx");
    }
    protected void btnatt_Click(object sender, EventArgs e)
    {
        Response.Redirect("AttenanceDownload.aspx");
    }
    protected void btnLeave_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptLeaveSample.aspx");
    }
    protected void btnOT_Click(object sender, EventArgs e)
    {
        Response.Redirect("OTDownload.aspx");
    }
    protected void btncontract_Click(object sender, EventArgs e)
    {
        Response.Redirect("ContractRPT.aspx");
    }
    protected void btnSal_Click(object sender, EventArgs e)
    {
        Response.Redirect("FrmDeductionDownload.aspx");
    }
    protected void btnsearch_Click(object sender, EventArgs e)
    {

        try
        {
            bool ErrFlag = false;
            string Stafflabour = "";
            DataTable dtGet = new DataTable();

            if ((ddlcategory.SelectedValue == "") || (ddlcategory.SelectedValue == "0"))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Category Properly');", true);
                ErrFlag = true;
            }

            else if (TxtExistSearch.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Exisisting Number');", true);
                ErrFlag = true;
            }


            if (ddlcategory.SelectedValue == "1")
            {
                Stafflabour = "S";
            }
            else if (ddlcategory.SelectedValue == "2")
            {
                Stafflabour = "L";
            }


            dtGet = objdata.GetEmpDetails_SP(TxtExistSearch.Text, Stafflabour, SessionCcode, SessionLcode);

            if (dtGet.Rows.Count > 0)
            {
                txtempNo.Text = dtGet.Rows[0]["EmpNo"].ToString();
                txtempname.Text = dtGet.Rows[0]["EmpName"].ToString();
                ddldepartment.SelectedValue = dtGet.Rows[0]["Department"].ToString();
                ddemployeetype.SelectedValue = dtGet.Rows[0]["EmployeeType"].ToString();
                txtbase.Text = dtGet.Rows[0]["Base"].ToString();
                txtpfsal.Text = dtGet.Rows[0]["PFS"].ToString();
                txtpfnumber.Text = dtGet.Rows[0]["PFnumber"].ToString();
                txtesic.Text = dtGet.Rows[0]["ESICnumber"].ToString();

                if (dtGet.Rows[0]["EligiblePF"].ToString() == "1")
                {
                    rbPF.SelectedValue = "1";
                }
                else
                {
                    rbPF.SelectedValue = "2";
                }


                if (dtGet.Rows[0]["ElgibleESI"].ToString() == "1")
                {
                    rbtnesi.SelectedValue = "1";
                }
                else
                {
                    rbtnesi.SelectedValue = "2";
                }
                if (dtGet.Rows[0]["Wagestype"].ToString() == "1")
                {
                    rbtnotherwages.SelectedValue = "1";
                }
                else if (dtGet.Rows[0]["Wagestype"].ToString() == "2")
                {
                    rbtnotherwages.SelectedValue = "2";
                }
                else
                {
                    rbtnotherwages.SelectedValue = "3";

                    
                }
               


            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Exisisting Number Not Matching');", true);
            
            }
        }
        catch (Exception ex)
        { 
        
        }
    }
    protected void ddemployeetype_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void ddldepartment_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void TxtExistSearch_TextChanged(object sender, EventArgs e)
    {

    }
    protected void txtempNo_TextChanged(object sender, EventArgs e)
    {

    }
    protected void rbtnotherwages_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void rbPF_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (rbPF.SelectedValue == "1")
        {
            txtpfnumber.Enabled = true;
        }
        else if (rbPF.SelectedValue == "2")
        {
            txtpfnumber.Enabled = false;
        }
    }
    protected void rbtnesi_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (rbtnesi.SelectedValue == "1")
        {
            txtesic.Enabled = true;
        }
        else
        {
            txtesic.Enabled = false;
            txtesic.Text = "";
        }
    }
    protected void ddlcategory_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void btnsave_Click(object sender, EventArgs e)
    {
        try
        {
            bool ErrFlag = false;
            bool Update = false;
            string Stafflabour = "";
            string Quer = "";
            DataTable dt_Value = new DataTable();


            if ((ddlcategory.SelectedValue == "") || (ddlcategory.SelectedValue == "0"))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Category Properly');", true);
                ErrFlag = true;
            }

            else if (TxtExistSearch.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Exisisting Number');", true);
                ErrFlag = true;
            }

            if (ddlcategory.SelectedValue == "1")
            {
                Stafflabour = "S";
            }
            else if (ddlcategory.SelectedValue == "2")
            {
                Stafflabour = "L";
            }

            Quer = "select EmpNo from EmployeeDetails where BiometricID='" + TxtExistSearch.Text + "'";
            SqlCommand cmd = new SqlCommand(Quer, con);
            DataTable dt_1 = new DataTable();
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            //DataSet ds1 = new DataSet();
            con.Open();
            sda.Fill(dt_1);
            con.Close();

            if (dt_1.Rows.Count > 0) 
            {

                string Quer_UptEmp = "Update EmployeeDetails set Department='" + ddldepartment.SelectedValue + "',EmployeeType='" + ddemployeetype.SelectedValue + "',StafforLabor='" + Stafflabour + "' where BiometricID='" + Convert.ToInt32(TxtExistSearch.Text) + "'";


                con.Open();
                SqlCommand cmd_verify1 = new SqlCommand(Quer_UptEmp, con);
                cmd_verify1.ExecuteNonQuery();
                con.Close();

                string Query_UpdOff = "Update Officialprofile set EligiblePF='" + rbPF.SelectedValue + "',PFnumber='" + txtpfnumber.Text + "',ElgibleESI='" + rbtnesi.SelectedValue + "',ESICnumber='" + txtesic.Text + "',Wagestype='" + rbtnotherwages.SelectedValue + "' where EmpNo='" + dt_1.Rows[0]["EmpNo"].ToString() + "'";
                con.Open();
                SqlCommand cmd_UpdOff = new SqlCommand(Query_UpdOff, con);
                cmd_UpdOff.ExecuteNonQuery();
                con.Close();




                string Query_Salary = "Update SalaryMaster set Base='" + txtbase.Text + "',PFS='" + txtpfsal.Text + "' where EmpNo='" + dt_1.Rows[0]["EmpNo"].ToString() + "'";
                SqlCommand cmd_Salary = new SqlCommand(Query_Salary,con);
                con.Open();
                cmd_Salary.ExecuteNonQuery();
                con.Close();

                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('SuccessFully Updated..,');", true);

                txtpfnumber.Text = "";
                txtpfsal.Text = "";
                txtempname.Text = "";
                txtempNo.Text = "";
                txtesic.Text = "";
                TxtExistSearch.Text = "";






                //dt_Value = objdata.UpdateEmpType(Stafflabour, ddemployeetype.SelectedValue, ddldepartment.SelectedValue, rbtnesi.SelectedValue, txtesic.Text, rbPF.SelectedValue, txtpfnumber.Text, rbtnotherwages.SelectedValue, txtbase.Text, txtpfsal.Text, TxtExistSearch.Text, SessionCcode, SessionLcode);
                //Update = true;
                //if (Update == true)
                //{
                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Employee Updated Successfully..,');", true);
                //    ErrFlag = true;
                //}
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Employee Not Find..,');", true);
            }
            


          



        }
        catch (Exception ex)
        {
            Response.Write("Error :" + ex.Message);
        }

    }
    protected void btncamcel_Click(object sender, EventArgs e)
    {
        txtpfnumber.Text = "";
        txtpfsal.Text = "";
        txtempname.Text = "";
        txtempNo.Text = "";
        txtesic.Text = "";
        TxtExistSearch.Text = "";

    }
}
