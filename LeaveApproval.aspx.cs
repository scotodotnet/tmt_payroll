﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class LeaveApproval : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            FillApplyLeaveForGridview();
            FillApplyLeaveForGridviewForStatus();
            FillMonth();
            //DateTime Month = DateTime.Now;
            //for (int i = 0; i < 12; i++)
            //{
            //    ddlmonth.Items.Add(Month.ToString("MMMM"));
            //    Month = Month.AddMonths(1);
            //}
        }

    }
    public void FillMonth()
    {
        DataTable dtmonth = new DataTable();
        dtmonth = objdata.FillMonth();
        ddlmonth.DataSource = dtmonth;
        ddlmonth.DataTextField = "MonthName";
        ddlmonth.DataValueField = "MothId";
        ddlmonth.DataBind();
    }
    public void FillApplyLeaveForGridview()
    {
        DataTable dt = new DataTable();
        dt = objdata.FillApplyForGridView();
        GvLeaveApproval.DataSource = dt;
        GvLeaveApproval.DataBind();
    }

    public void FillApplyLeaveForGridviewForStatus()
    {
       
    }
    protected void rbtnApprovalstatus_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (rbtnApprovalstatus.SelectedValue == "1")
        {
            PanelApprovalLeave.Visible = true;
            PanelApprovalLeaveButton.Visible = true;
            PanelStatus.Visible = false;
            PanelStatusButton.Visible = false;
        }
        else if (rbtnApprovalstatus.SelectedValue == "2")
        {
            PanelApprovalLeave.Visible = false;
            PanelApprovalLeaveButton.Visible = false;
            PanelStatus.Visible = true;
            PanelStatusButton.Visible = true;
        }
    }
    protected void btnapproval_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        TextBox lblgvnoofleave1 = (TextBox)GvLeaveApproval.FindControl("txtnoofleaves");

        if (lblgvnoofleave1.Text == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter No of Leave Approval');", true);
            ErrFlag = false;
        }
        if (!ErrFlag)
        {
            for (int i = 0; i < GvLeaveApproval.Rows.Count; i++)
            {
                CheckBox check_box = (CheckBox)GvLeaveApproval.Rows[i].FindControl("chkclear");
                if (check_box != null)
                {
                    if (check_box.Checked)
                    {
                        Label lblgvempno = (Label)GvLeaveApproval.Rows[i].FindControl("lblempno");
                        Label lblgvleavecode = (Label)GvLeaveApproval.Rows[i].FindControl("lblleavecode");
                        TextBox lblgvnoofleave = (TextBox)GvLeaveApproval.Rows[i].FindControl("txtnoofleaves");
                        string sid = lblgvempno.Text;
                        string Status = "Y";
                        string Leavecd = lblgvleavecode.Text;
                        string Noofleave = lblgvnoofleave.Text;
                        objdata.UpdateForApplyLeave(sid, Status, Leavecd, Noofleave);
                        FillApplyLeaveForGridview();
                    }
                }
            }

            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Leave Approval Update Successfully...');", true);


        }
        
    }
    protected void btnrejected_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        TextBox lblgvnoofleave1 = (TextBox)GvLeaveApproval.FindControl("txtnoofleaves");
        if (lblgvnoofleave1.Text == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter No of Leave Rejected');", true);
            ErrFlag = false;
        }
        if (!ErrFlag)
        {
            for (int i = 0; i < GvLeaveApproval.Rows.Count; i++)
            {
                CheckBox check_box = (CheckBox)GvLeaveApproval.Rows[i].FindControl("chkclear");
                if (check_box != null)
                {
                    if (check_box.Checked)
                    {
                        Label lblgvempno = (Label)GvLeaveApproval.Rows[i].FindControl("lblempno");
                        Label lblgvleavecode = (Label)GvLeaveApproval.Rows[i].FindControl("lblleavecode");
                        TextBox lblgvnoofleave = (TextBox)GvLeaveApproval.Rows[i].FindControl("txtnoofleaves");
                        string sid = lblgvempno.Text;
                        string Status = "R";
                        string Leavecd = lblgvleavecode.Text;
                        string Noofleave = lblgvnoofleave.Text;
                        objdata.UpdateForApplyLeave(sid, Status, Leavecd, Noofleave);
                        FillApplyLeaveForGridview();
                    }
                }
            }

            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Leave Approval Update Successfully...');", true);
        }
    }

    protected void btnsearch_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        // string mont = ddlmonth.SelectedValue;

        if (ddlmonth.SelectedValue == "0")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select Month');", true);
        }
        if (!ErrFlag)
        {
            DataTable dt = new DataTable();
            dt = objdata.FillApplyForGridViewForStatus(ddlmonth.SelectedItem.Text);
            gvApplyLeaveStatus.DataSource = dt;
            gvApplyLeaveStatus.DataBind();
        }
    }
}
