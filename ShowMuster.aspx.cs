﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class ShowMuster : System.Web.UI.Page
{
    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    ReportDocument rd = new ReportDocument();
    BALDataAccess objdata = new BALDataAccess();
    SqlConnection con;
    string str_month;
    string str_yr;
    string str_cate;
    string str_dept;
    string SessionCcode;
    string SessionLcode;
    string SessionAdmin;
    static string CmpName;
    static string Cmpaddress;
    string Query = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        SqlConnection con = new SqlConnection(constr);
        SessionAdmin = Session["Isadmin"].ToString();
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        string ss = Session["UserId"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();

        str_cate = Request.QueryString["Cate"].ToString();
        str_dept = Request.QueryString["Depat"].ToString();
        str_month = Request.QueryString["Month"].ToString();
        str_yr = Request.QueryString["Financialyear"].ToString();
        DataTable dt = new DataTable();
        dt = objdata.Company_retrive(SessionCcode, SessionLcode);
        if (dt.Rows.Count > 0)
        {
            CmpName = dt.Rows[0]["Cname"].ToString();
            Cmpaddress = (dt.Rows[0]["Address1"].ToString() + ", " + dt.Rows[0]["Address2"].ToString() + ", " + dt.Rows[0]["Location"].ToString() + "-" + dt.Rows[0]["Pincode"].ToString());
        }
        string Month_year = (str_month + "-" + str_yr).ToString();
        if (str_dept == "0")
        {
            if (SessionAdmin == "1")
            {
                Query = "Select ExisistingCode,EmpName,FatherName,PSAdd1,PSAdd2,PSTaluk,PSDistrict from Employeedetails where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and  " +
                        " StafforLabor='" + str_cate + "' and ActivateMode='Y'";
            }
            else
            {
                Query = "Select ExisistingCode,EmpName,FatherName,PSAdd1,PSAdd2,PSTaluk,PSDistrict from Employeedetails where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and  " +
                        " StafforLabor='" + str_cate + "' and ActivateMode='Y'";
            }
        }
        else
        {
            if (SessionAdmin == "1")
            {
                Query = "Select ExisistingCode,EmpName,FatherName,PSAdd1,PSAdd2,PSTaluk,PSDistrict from Employeedetails where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and  " +
                        " Department='" + str_dept + "' and StafforLabor='" + str_cate + "' and ActivateMode='Y'";
            }
            else
            {
                Query = "Select ExisistingCode,EmpName,FatherName,PSAdd1,PSAdd2,PSTaluk,PSDistrict from Employeedetails where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and  " +
                        " Department='" + str_dept + "' and StafforLabor='" + str_cate + "' and ActivateMode='Y'";
            }
        }
        //query = "";
        SqlCommand cmd = new SqlCommand(Query, con);
        SqlDataAdapter sda = new SqlDataAdapter(cmd);
        DataSet ds1 = new DataSet();
        con.Open();
        sda.Fill(ds1);
        con.Close();
        if (ds1.Tables[0].Rows.Count > 0)
        {
            rd.Load(Server.MapPath("Musted.rpt"));
            rd.SetDataSource(ds1.Tables[0]);
            rd.DataDefinition.FormulaFields["MonthYear"].Text = "'" + Month_year + "'";
            CrystalReportViewer1.ReportSource = rd;
            CrystalReportViewer1.RefreshReport();
            CrystalReportViewer1.DataBind();
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('No Data Found');", true);

        }
    }
}
