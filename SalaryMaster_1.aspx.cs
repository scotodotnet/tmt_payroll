﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Payroll;
using Payroll.Configuration;
using Payroll.Data;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class SalaryMaster_1 : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    string staffLabour;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    protected void Page_Load(object sender, EventArgs e)
    {
        
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        string ss = Session["UserId"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        lblComany.Text = SessionCcode + " - " + SessionLcode;
        if (!IsPostBack)
        {
            //Department();
            FoodDetails();
            category();
            int currentYear = Utility.GetFinancialYear;
            for (int i = 0; i < 10; i++)
            {
                ddFinance.Items.Add(new ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                //  ddlShowYear.Items.Add(new ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                currentYear = currentYear - 1;
            }
        }
        lblusername.Text = Session["Usernmdisplay"].ToString();
    }
    public void category()
    {
        DataTable dtcate = new DataTable();
        dtcate = objdata.DropDownCategory();
        ddlcategory.DataSource = dtcate;
        ddlcategory.DataTextField = "CategoryName";
        ddlcategory.DataValueField = "CategoryCd";
        ddlcategory.DataBind();
    }
    private void FoodDetails()
    {
        DataTable dtfood = new DataTable();
        dtfood = objdata.DropDownFood();
        ddlFood.DataSource = dtfood;
        ddlFood.DataTextField = "FoodType";
        ddlFood.DataValueField = "ID";
        ddlFood.DataBind();
    }
    public void clr()
    {
        btnclick.Enabled = true;
        btnSave.Enabled = true;
        btnreset.Enabled = true;
        btncancel.Enabled = true;
        btnexitingSearch.Enabled = true;
        txtexistingNo.Text = "";
        txtBasic.Text = "0";
        txthrapercentage.Text = "0";
        txtHRAamt.Text = "0";
        txtconveyanceper.Text = "0";
        txtconveyanceAmt.Text = "0";
        txtmedicalPercentage.Text = "0";
        txtmedicalAmt.Text = "0";
        txtall1percentage.Text = "0";
        txtallowance1Amt.Text = "0";
        txtAllowance2percentage.Text = "0";
        txtAllowavce2Amt.Text = "0";
        txtAllowance3percentage.Text = "0";
        txtAllowance3Amt.Text = "0";
        txtnet.Text = "0";
        txtdeduction1.Text = "0";
        txtdeduction2.Text = "0";
        txtdeduction3.Text = "0";
        txtLabourBasic.Text = "0";
        txtmessDeductionLabour.Text = "0";
        txtHostel.Text = "0";
        txtdeductionAmt1.Text = "0";
        txtdeductionAmt2.Text = "0";
        txtdeductionAmt3.Text = "0";
        txtdeduction1percentage.Text = "0";
        txtdeduction2percentage.Text = "0";
        txtdeduction3Precentge.Text = "0";
        txtlabourallowance3.Text = "0";
        txtlabourallow1.Text = "0";
        txtlabourallow2.Text = "0";
        //txtDailySalary.Text = "0";
        //rbtnprofile.SelectedValue = "0";
        panelError.Visible = false;
        lblError.Text = "";
        panelsuccess.Visible = false;
        lbloutput.Text = "";
    }
    protected void ddlcategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        string staffLabour_temp;
        DataTable dtempty = new DataTable();
        ddlDepartment.DataSource = dtempty;
        ddlDepartment.DataBind();
        if (ddlcategory.SelectedValue == "1")
        {
            staffLabour_temp = "S";
        }
        else if (ddlcategory.SelectedValue == "2")
        {
            staffLabour_temp = "L";
        }
        else
        {
            staffLabour_temp = "0";
        }
        DataTable dtDip = new DataTable();
        dtDip = objdata.DropDownDepartment_category(staffLabour_temp);
        if (dtDip.Rows.Count > 1)
        {
            ddlDepartment.DataSource = dtDip;
            ddlDepartment.DataTextField = "DepartmentNm";
            ddlDepartment.DataValueField = "DepartmentCd";
            ddlDepartment.DataBind();
        }
        else
        {
            DataTable dt = new DataTable();
            ddlDepartment.DataSource = dt;
            ddlDepartment.DataBind();
        }
        PanelEmp.Visible = false;
        PanelExistNo.Visible = false;
        panelStaff1.Visible = false;
        Panelsaff2.Visible = false;
        //panelsstaff3.Visible = false;
        panelstaff4.Visible = false;
        Panelstaff5.Visible = false;
        panelstaff6.Visible = false;
        Panelstaff7.Visible = false;
        Panelstaff8.Visible = false;
        Panelstaff10.Visible = false;
        PanelStaff18.Visible = false;
        PanelStaff9.Visible = false;
        PanelStaff110.Visible = false;
        PanelWagesLabour.Visible = false;
        PanelDailyLabour.Visible = false;
        PanelDailyLabour.Visible = false;
        panelMonthlyLabour1.Visible = false;
        panelMonthlyLabour2.Visible = false;
        panelMonthlyLabour3.Visible = false;
        panelMonthlyLabour4.Visible = false;
        PanelLabourMonthlyAllowace.Visible = false;
        clr();
    }
    protected void ddlDepartment_SelectedIndexChanged(object sender, EventArgs e)
    {
        PanelExistNo.Visible = false;
        PanelEmp.Visible = false;
        panelStaff1.Visible = false;
        Panelsaff2.Visible = false;
        //panelsstaff3.Visible = false;
        panelstaff4.Visible = false;
        Panelstaff5.Visible = false;
        panelstaff6.Visible = false;
        Panelstaff7.Visible = false;
        Panelstaff8.Visible = false;
        Panelstaff10.Visible = false;
        PanelStaff18.Visible = false;
        PanelStaff9.Visible = false;
        PanelStaff110.Visible = false;
        PanelWagesLabour.Visible = false;
        PanelDailyLabour.Visible = false;
        PanelDailyLabour.Visible = false;
        panelMonthlyLabour1.Visible = false;
        panelMonthlyLabour2.Visible = false;
        panelMonthlyLabour3.Visible = false;
        panelMonthlyLabour4.Visible = false;
        PanelLabourMonthlyAllowace.Visible = false;
        clr();
    }
    protected void btnclick_Click(object sender, EventArgs e)
    {
        if (ddlcategory.SelectedValue == "1")
        {
            PanelEmp.Visible = true;
            PanelExistNo.Visible = true;
            panelStaff1.Visible = true;
            Panelsaff2.Visible = true;
            //panelsstaff3.Visible = true;
            panelstaff4.Visible = true;
            Panelstaff5.Visible = true;
            panelstaff6.Visible = true;
            Panelstaff7.Visible = true;
            Panelstaff8.Visible = true;
            Panelstaff10.Visible = true;
            PanelStaff18.Visible = true;
            PanelStaff9.Visible = true;
            PanelStaff110.Visible = true;
            PanelWagesLabour.Visible = false;
            PanelDailyLabour.Visible = false;
            PanelWagesLabour.Visible = false;
            PanelDailyLabour.Visible = false;
            panelMonthlyLabour1.Visible = false;
            panelMonthlyLabour2.Visible = false;
            panelMonthlyLabour3.Visible = false;
            panelMonthlyLabour4.Visible = false;
            PanelLabourMonthlyAllowace.Visible = false;
            clr();
        }
        else if (ddlcategory.SelectedValue == "2")
        {
            PanelEmp.Visible = true;
            PanelExistNo.Visible = true;
            panelStaff1.Visible = false;
            Panelsaff2.Visible = false;
            //panelsstaff3.Visible = false;
            panelstaff4.Visible = false;
            Panelstaff5.Visible = false;
            panelstaff6.Visible = false;
            Panelstaff7.Visible = false;
            Panelstaff8.Visible = false;
            Panelstaff10.Visible = false;
            PanelStaff18.Visible = false;
            PanelStaff9.Visible = false;
            PanelStaff110.Visible = false;
            PanelWagesLabour.Visible = true;
            PanelDailyLabour.Visible = false;
            PanelDailyLabour.Visible = false;
            panelMonthlyLabour1.Visible = false;
            panelMonthlyLabour2.Visible = false;
            panelMonthlyLabour3.Visible = false;
            panelMonthlyLabour4.Visible = false;
            PanelLabourMonthlyAllowace.Visible = false;

            clr();
        }
        if (ddlcategory.SelectedValue == "1")
        {
            bool ErrFlag = false;
            //panelError.Visible = true;
            if (ddlcategory.SelectedValue == "1")
            {
                staffLabour = "S";
            }
            else if (ddlcategory.SelectedValue == "2")
            {
                staffLabour = "L";
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Category...!');", true);
                //System.Windows.Forms.MessageBox.Show("Select the Category", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                //lblError.Text = "Select the Category";
                ErrFlag = true;
            }
            if (!ErrFlag)
            {


                DataTable dtempcode = new DataTable();
                if (SessionAdmin == "1")
                {
                    dtempcode = objdata.EmployeeLoadSalaryMaster(staffLabour, ddlDepartment.SelectedValue,SessionCcode,SessionLcode);
                }
                else
                {
                    dtempcode = objdata.EmployeeLoadSalaryMaster_user(staffLabour, ddlDepartment.SelectedValue,SessionCcode,SessionLcode);
                }

                ddlempNo.DataSource = dtempcode;
                DataRow dr = dtempcode.NewRow();
                dr["EmpNo"] = ddlDepartment.SelectedItem.Text;
                dr["EmpName"] = ddlDepartment.SelectedItem.Text;
                dtempcode.Rows.InsertAt(dr, 0);

                ddlempNo.DataTextField = "EmpNo";

                ddlempNo.DataValueField = "EmpNo";
                ddlempNo.DataBind();
                ddlEmpName.DataSource = dtempcode;
                ddlEmpName.DataTextField = "EmpName";
                ddlEmpName.DataValueField = "EmpNo";
                ddlEmpName.DataBind();
                if (dtempcode.Rows.Count > 0)
                {
                    PanelEmp.Visible = true;
                }
                else
                {
                    PanelEmp.Visible = false;
                }
            }
            clr();
        }
        else if (ddlcategory.SelectedValue == "2")
        {
            staffLabour = "L";
            PanelWagesLabour.Visible = true;
            DataTable dtempcode = new DataTable();
            dtempcode = objdata.EmployeeLoadSalaryMaster(staffLabour, ddlDepartment.SelectedValue, SessionCcode, SessionLcode);
            ddlempNo.DataSource = dtempcode;

            DataRow dr = dtempcode.NewRow();
            dr["EmpNo"] = ddlDepartment.SelectedItem.Text;
            dr["EmpName"] = ddlDepartment.SelectedItem.Text;
            dtempcode.Rows.InsertAt(dr, 0);

            ddlempNo.DataTextField = "EmpNo";
            ddlempNo.DataValueField = "EmpNo";
            ddlempNo.DataBind();
            ddlEmpName.DataSource = dtempcode;
            ddlEmpName.DataTextField = "EmpName";
            ddlEmpName.DataValueField = "EmpNo";
            ddlEmpName.DataBind();
            if (dtempcode.Rows.Count > 0)
            {
                PanelEmp.Visible = true;
            }
            else
            {
                PanelEmp.Visible = false;
            }
        }
    }
    protected void btnexitingSearch_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        //panelError.Visible = true;
        if (ddlcategory.SelectedValue == "0")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select Category Staff or Labour');", true);
            //System.Windows.Forms.MessageBox.Show("Select the Category", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            //lblError.Text = "Select the Category.";
            ErrFlag = true;
        }
        else if (ddlDepartment.SelectedValue == "0")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select Department');", true);
            //System.Windows.Forms.MessageBox.Show("Select the Department", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            //lblError.Text = "Select Department";
            ErrFlag = true;
        }
        else if (txtexistingNo.Text == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Existing Number');", true);
            //System.Windows.Forms.MessageBox.Show("Enter the Existing Number", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            //lblError.Text = "Enter the Existing Number";
            ErrFlag = true;
        }
        if (!ErrFlag)
        {
            if (ddlcategory.SelectedValue == "1")
            {
                #region Staff Search
                string Cate = "S";
                DataTable dtempcode = new DataTable();             
                dtempcode = objdata.SalaryMasterLabour_ForSearchingExisiting(ddlDepartment.SelectedValue, txtexistingNo.Text, Cate, SessionCcode, SessionLcode, SessionAdmin);
                if (dtempcode.Rows.Count > 0)
                {

                    //DataRow dr = dtempcode.NewRow();
                    //dr["EmpNo"] = ddlDepartment.SelectedItem.Text;
                    //dr["EmpName"] = ddlDepartment.SelectedItem.Text;
                    //dtempcode.Rows.InsertAt(dr, 0);
                    //ddlempNo.DataSource = dtempcode;
                    //ddlempNo.DataTextField = "EmpNo";
                    //ddlempNo.DataValueField = "EmpNo";
                    //ddlempNo.DataBind();
                    ddlempNo.SelectedValue = dtempcode.Rows[0]["EmpNo"].ToString();
                    ddlEmpName.DataSource = dtempcode;
                    ddlEmpName.DataTextField = "EmpName";
                    ddlEmpName.DataValueField = "EmpNo";
                    ddlEmpName.DataBind();
                    lblError.Text = "";
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Employee not found ');", true);
                    //lblError.Text = "Employee not Found";
                    ErrFlag = true;
                    clr();
                    staffLabour = "S";
                    if (SessionAdmin == "1")
                    {
                        dtempcode = objdata.EmployeeLoadSalaryMaster(staffLabour, ddlDepartment.SelectedValue, SessionCcode,SessionLcode);
                    }
                    else
                    {
                        dtempcode = objdata.EmployeeLoadSalaryMaster_user(staffLabour, ddlDepartment.SelectedValue,SessionCcode,SessionLcode);
                    }

                    ddlempNo.DataSource = dtempcode;
                    DataRow dr = dtempcode.NewRow();
                    dr["EmpNo"] = ddlDepartment.SelectedItem.Text;
                    dr["EmpName"] = ddlDepartment.SelectedItem.Text;
                    dtempcode.Rows.InsertAt(dr, 0);

                    ddlempNo.DataTextField = "EmpNo";

                    ddlempNo.DataValueField = "EmpNo";
                    ddlempNo.DataBind();
                    ddlEmpName.DataSource = dtempcode;
                    ddlEmpName.DataTextField = "EmpName";
                    ddlEmpName.DataValueField = "EmpNo";
                    ddlEmpName.DataBind();

                }

                if (!ErrFlag)
                {
                    string empCodeverify = objdata.SalaryMasterVerify(ddlempNo.SelectedValue,SessionCcode,SessionLcode);
                    if (empCodeverify == ddlempNo.SelectedValue)
                    {
                        if (ddlcategory.SelectedValue == "1")
                        {
                            DataTable dtempLoad = new DataTable();
                            dtempLoad = objdata.SalaryMasterEmpLoad(ddlempNo.SelectedValue,SessionCcode,SessionLcode);
                            if (dtempLoad.Rows.Count > 0)
                            {
                                lblError.Text = "";
                                txtBasic.Text = dtempLoad.Rows[0]["BasicSalary"].ToString();
                                txthrapercentage.Text = dtempLoad.Rows[0]["HRAPer"].ToString();
                                txtHRAamt.Text = dtempLoad.Rows[0]["HRAamt"].ToString();
                                txtconveyanceper.Text = dtempLoad.Rows[0]["ConveyancePer"].ToString();
                                txtconveyanceAmt.Text = dtempLoad.Rows[0]["ConveyanceAmt"].ToString();
                                //txtccaPercentage.Text = dtempLoad.Rows[0]["CCAper"].ToString();
                                //txtCCAamt.Text = dtempLoad.Rows[0]["CCAamt"].ToString();
                                txtmedicalPercentage.Text = dtempLoad.Rows[0]["Medicalper"].ToString();
                                txtmedicalAmt.Text = dtempLoad.Rows[0]["Medicalamt"].ToString();
                                txtall1percentage.Text = dtempLoad.Rows[0]["Allowance1per"].ToString();
                                txtallowance1Amt.Text = dtempLoad.Rows[0]["Alllowance1amt"].ToString();
                                txtAllowance2percentage.Text = dtempLoad.Rows[0]["Allowance2per"].ToString();
                                txtAllowavce2Amt.Text = dtempLoad.Rows[0]["Allowance2amt"].ToString();
                                txtAllowance3percentage.Text = dtempLoad.Rows[0]["Allowance3per"].ToString();
                                txtAllowance3Amt.Text = dtempLoad.Rows[0]["Allowance3amt"].ToString();
                                txtdeduction1percentage.Text = dtempLoad.Rows[0]["Deduction1per"].ToString();
                                txtdeduction2percentage.Text = dtempLoad.Rows[0]["Deduction2per"].ToString();
                                txtdeduction3Precentge.Text = dtempLoad.Rows[0]["Deduction3per"].ToString();
                                txtdeductionAmt1.Text = dtempLoad.Rows[0]["Deduction1amt"].ToString();
                                txtdeductionAmt2.Text = dtempLoad.Rows[0]["Deduction2amt"].ToString();
                                txtdeductionAmt3.Text = dtempLoad.Rows[0]["Deduction3amt"].ToString();
                                txtnet.Text = dtempLoad.Rows[0]["NetPay"].ToString();
                                txtmessDeductionLabour.Text = dtempLoad.Rows[0]["Messdeduction"].ToString();
                                txtHostel.Text = dtempLoad.Rows[0]["hosteldeduction"].ToString();
                                txtdeduction1.Text = dtempLoad.Rows[0]["Deduction1"].ToString();
                                txtdeduction2.Text = dtempLoad.Rows[0]["Deduction2"].ToString();
                                txtdeduction3.Text = dtempLoad.Rows[0]["Deduction3"].ToString();
                                ddlFood.SelectedValue = dtempLoad.Rows[0]["FoodType"].ToString();
                                txtLabourBasic.Text = dtempLoad.Rows[0]["DailySalary"].ToString();
                                txtlabourallow1.Text = dtempLoad.Rows[0]["LabourAllowace1"].ToString();
                                txtlabourallow2.Text = dtempLoad.Rows[0]["LabourAllowance2"].ToString();
                                txtlabourallowance3.Text = dtempLoad.Rows[0]["LabourAllowance3"].ToString();
                                if ((dtempLoad.Rows[0]["DailySalary"].ToString() == "") || (dtempLoad.Rows[0]["DailySalary"].ToString() == null))
                                {
                                    txtDailySalary.Text = "0";
                                }
                                else
                                {
                                    txtDailySalary.Text = dtempLoad.Rows[0]["DailySalary"].ToString();
                                }
                                //if(dtempLoad.Rows[0]["ProfileType"].ToString() == "1")
                                //{
                                //    rbtnprofile.SelectedValue = "1";
                                //}
                                //else
                                //{
                                //    rbtnprofile.SelectedValue = "2";
                                //}
                                lblError.Text = "";
                            }
                            DataTable dtprofile = new DataTable();
                            dtprofile = objdata.SalaryMasterProfile(ddlempNo.SelectedValue,SessionCcode, SessionLcode);
                            if (dtprofile.Rows.Count > 0)
                            {
                                txtexistingNo.Text = dtprofile.Rows[0]["ExisistingCode"].ToString();
                                if (dtprofile.Rows[0]["ProfileType"].ToString() == "1")
                                {
                                    //rbtnprofile.SelectedValue = "1";
                                    Profilelbl.Text = "Probationary period";
                                }
                                else
                                {
                                    //rbtnprofile.SelectedValue = "2";
                                    Profilelbl.Text = "Conformed emplooyee";
                                }
                            }

                        }
                    }

                }
            }
                #endregion
            else if (ddlcategory.SelectedValue == "2")
            {
                string Cate = "L";

                DataTable dtempcodeLa = new DataTable();
                if (SessionAdmin == "1")
                {
                    dtempcodeLa = objdata.SalaryMasterLabour_ForSearchingExisiting(ddlDepartment.SelectedValue, txtexistingNo.Text, Cate,SessionCcode,SessionLcode, SessionAdmin);
                }
                else
                {
                    dtempcodeLa = objdata.SalaryMasterLabour_ForSearchingExisiting_user(ddlDepartment.SelectedValue, txtexistingNo.Text, Cate,SessionCcode,SessionLcode);
                }
                ddlempNo.DataSource = dtempcodeLa;
                if (dtempcodeLa.Rows.Count > 0)
            //    {
                    //ddlempNo.DataTextField = "EmpNo";
                    //ddlempNo.DataValueField = "EmpNo";
                    //ddlempNo.DataBind();
                    ddlempNo.SelectedValue = dtempcodeLa.Rows[0]["EmpNo"].ToString();
                    ddlEmpName.DataSource = dtempcodeLa;
                    ddlEmpName.DataTextField = "EmpName";
                    ddlEmpName.DataValueField = "EmpNo";
                    ddlEmpName.DataBind();
                    if (ddlcategory.SelectedValue == "2")
                    {
                        string wage_type;
                        wage_type = objdata.Wages_SP(ddlempNo.SelectedValue, SessionCcode, SessionLcode);
                        if (wage_type == "2")
                        {
                            rbtnWages.SelectedValue = "2";
                            panelMonthlyLabour1.Visible = true;
                            panelMonthlyLabour2.Visible = true;
                            panelMonthlyLabour3.Visible = true;
                            panelMonthlyLabour4.Visible = true;
                            lblLabourDailyWages.Visible = true;
                            lblLabourBasic.Visible = true;
                            lblLabourDailyWages.Visible = false;
                            PanelLabourMonthlyAllowace.Visible = true;


                        }
                        else if (wage_type == "1")
                        {
                            rbtnWages.SelectedValue = "1";
                            panelMonthlyLabour1.Visible = true;
                            panelMonthlyLabour2.Visible = true;
                            panelMonthlyLabour3.Visible = true;
                            panelMonthlyLabour4.Visible = true;
                            lblLabourDailyWages.Visible = true;
                            lblLabourDailyWages.Visible = true;
                            lblLabourBasic.Visible = false;
                        }
                        else if (wage_type == "3")
                        {
                            rbtnWages.SelectedValue = "3";
                            panelMonthlyLabour1.Visible = true;
                            panelMonthlyLabour2.Visible = true;
                            panelMonthlyLabour3.Visible = true;
                            panelMonthlyLabour4.Visible = true;
                            lblLabourDailyWages.Visible = true;
                            lblLabourDailyWages.Visible = true;
                            lblLabourBasic.Visible = false;
                            PanelLabourMonthlyAllowace.Visible = true;
                        }
                    }
                }


                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Employee not Found ');", true);
                    //lblError.Text = "Employee not Found";
                //System.Windows.Forms.MessageBox.Show("Employee Not Found", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    ErrFlag = true;
                    clr();
                    DataTable dtempcode = new DataTable();
                    staffLabour = "L";
                    if (SessionAdmin == "1")
                    {
                        dtempcode = objdata.EmployeeLoadSalaryMaster(staffLabour, ddlDepartment.SelectedValue,SessionCcode,SessionLcode);
                    }
                    else
                    {
                        dtempcode = objdata.EmployeeLoadSalaryMaster_user(staffLabour, ddlDepartment.SelectedValue,SessionCcode,SessionLcode);
                    }

                    ddlempNo.DataSource = dtempcode;
                    DataRow dr = dtempcode.NewRow();
                    dr["EmpNo"] = ddlDepartment.SelectedItem.Text;
                    dr["EmpName"] = ddlDepartment.SelectedItem.Text;
                    dtempcode.Rows.InsertAt(dr, 0);

                    ddlempNo.DataTextField = "EmpNo";

                    ddlempNo.DataValueField = "EmpNo";
                    ddlempNo.DataBind();
                    ddlEmpName.DataSource = dtempcode;
                    ddlEmpName.DataTextField = "EmpName";
                    ddlEmpName.DataValueField = "EmpNo";
                    ddlEmpName.DataBind();
                }
                if (!ErrFlag)
                {
                    DataTable dtempLoad = new DataTable();
                    dtempLoad = objdata.SalaryMasterEmpLoad_ForLabour(ddlempNo.SelectedValue,SessionCcode,SessionLcode);
                    if (dtempLoad.Rows.Count > 0)
                    {
                        lblError.Text = "";
                        txtHostel.Text = dtempLoad.Rows[0]["hosteldeduction"].ToString();
                        txtmessDeductionLabour.Text = dtempLoad.Rows[0]["Messdeduction"].ToString();
                        txtdeduction1.Text = dtempLoad.Rows[0]["Deduction1"].ToString();
                        txtdeduction2.Text = dtempLoad.Rows[0]["Deduction2"].ToString();
                        txtdeduction3.Text = dtempLoad.Rows[0]["Deduction3"].ToString();
                        ddlFood.SelectedValue = dtempLoad.Rows[0]["ID"].ToString();
                        txtLabourBasic.Text = dtempLoad.Rows[0]["DailySalary"].ToString();
                        txtlabourallow1.Text = dtempLoad.Rows[0]["LabourAllowace1"].ToString();
                        txtlabourallow2.Text = dtempLoad.Rows[0]["LabourAllowance2"].ToString();
                        txtlabourallowance3.Text = dtempLoad.Rows[0]["LabourAllowance3"].ToString();
                        if ((dtempLoad.Rows[0]["DailySalary"].ToString() == "") || (dtempLoad.Rows[0]["DailySalary"].ToString() == null))
                        {
                            txtDailySalary.Text = "0";
                        }
                        else
                        {
                            txtDailySalary.Text = dtempLoad.Rows[0]["DailySalary"].ToString();
                        }
                        if (dtempLoad.Rows[0]["Wagestype"].ToString() == "2")
                        {
                            rbtnWages.SelectedValue = "2";
                        }
                        else if (dtempLoad.Rows[0]["Wagestype"].ToString() == "1")
                        {
                            rbtnWages.SelectedValue = "1";
                        }
                        else if (dtempLoad.Rows[0]["Wagestype"].ToString() == "3")
                        {
                            rbtnWages.SelectedValue = "3";
                        }
                        //if(dtempLoad.Rows[0]["ProfileType"].ToString() == "1")
                        //{
                        //    rbtnprofile.SelectedValue = "1";
                        //}
                        //else
                        //{
                        //    rbtnprofile.SelectedValue = "2";
                        //}
                    }
                    DataTable dtprofile = new DataTable();
                    dtprofile = objdata.SalaryMasterProfile(ddlempNo.SelectedValue,SessionCcode,SessionLcode);
                    if (dtprofile.Rows.Count > 0)
                    {
                        txtexistingNo.Text = dtprofile.Rows[0]["ExisistingCode"].ToString();
                        if (dtprofile.Rows[0]["ProfileType"].ToString() == "1")
                        {
                            //rbtnprofile.SelectedValue = "1";
                            Profilelbl.Text = "Probationary period";
                        }
                        else
                        {
                            //rbtnprofile.SelectedValue = "2";
                            Profilelbl.Text = "Conformed emplooyee";
                        }
                    }
                }
            }
            else
            {
                DataTable dtprofile = new DataTable();
                dtprofile = objdata.SalaryMasterProfile(ddlempNo.SelectedValue,SessionCcode,SessionLcode);
                if (dtprofile.Rows.Count > 0)
                {
                    txtexistingNo.Text = dtprofile.Rows[0]["ExisistingCode"].ToString();
                    if (dtprofile.Rows[0]["ProfileType"].ToString() == "1")
                    {
                        //rbtnprofile.SelectedValue = "1";
                        Profilelbl.Text = "Probationary period";
                    }
                    else
                    {
                        //rbtnprofile.SelectedValue = "2";
                        Profilelbl.Text = "Conformed emplooyee";
                    }
                }
            }


        //}
    }
    protected void ddlempNo_SelectedIndexChanged(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        if (ddlempNo.SelectedValue == "0")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Employee Code....!');", true);
            //System.Windows.Forms.MessageBox.Show("Select the Employee Code", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            //lblError.Text = "Select the Employee Code";
            ErrFlag = true;
        }
        if (!ErrFlag)
        {
            DataTable dtempName = new DataTable();
            dtempName = objdata.BonusEmployeeName(ddlempNo.SelectedValue,SessionCcode,SessionLcode);
            ddlEmpName.DataSource = dtempName;
            //DataRow dr = dtempName.NewRow();
            //dr["EmpNo"] = ddlDepartment.SelectedItem.Text;
            //dr["EmpName"] = ddlDepartment.SelectedItem.Text;
            //dtempName.Rows.InsertAt(dr, 0);
            ddlEmpName.DataTextField = "EmpName";
            ddlEmpName.DataValueField = "EmpNo";
            ddlEmpName.DataBind();
            clr();
            string empCodeverify = objdata.SalaryMasterVerify(ddlempNo.SelectedValue, SessionCcode,SessionLcode);
            if (empCodeverify == ddlempNo.SelectedValue)
            {
                if (ddlcategory.SelectedValue == "1")
                {
                    DataTable dtempLoad = new DataTable();
                    dtempLoad = objdata.SalaryMasterEmpLoad(ddlempNo.SelectedValue,SessionCcode, SessionLcode);
                    if (dtempLoad.Rows.Count > 0)
                    {
                        lblError.Text = "";
                        txtexistingNo.Text = dtempLoad.Rows[0]["ExisistingCode"].ToString();
                        txtBasic.Text = dtempLoad.Rows[0]["BasicSalary"].ToString();
                        Profilelbl.Text = dtempLoad.Rows[0]["ProfileType"].ToString();
                        txthrapercentage.Text = dtempLoad.Rows[0]["HRAPer"].ToString();
                        txtHRAamt.Text = dtempLoad.Rows[0]["HRAamt"].ToString();
                        txtconveyanceper.Text = dtempLoad.Rows[0]["ConveyancePer"].ToString();
                        txtconveyanceAmt.Text = dtempLoad.Rows[0]["ConveyanceAmt"].ToString();
                        //txtccaPercentage.Text = dtempLoad.Rows[0]["CCAper"].ToString();
                        //txtCCAamt.Text = dtempLoad.Rows[0]["CCAamt"].ToString();
                        txtmedicalPercentage.Text = dtempLoad.Rows[0]["Medicalper"].ToString();
                        txtmedicalAmt.Text = dtempLoad.Rows[0]["Medicalamt"].ToString();
                        txtall1percentage.Text = dtempLoad.Rows[0]["Allowance1per"].ToString();
                        txtallowance1Amt.Text = dtempLoad.Rows[0]["Alllowance1amt"].ToString();
                        txtAllowance2percentage.Text = dtempLoad.Rows[0]["Allowance2per"].ToString();
                        txtAllowavce2Amt.Text = dtempLoad.Rows[0]["Allowance2amt"].ToString();
                        txtAllowance3percentage.Text = dtempLoad.Rows[0]["Allowance3per"].ToString();
                        txtAllowance3Amt.Text = dtempLoad.Rows[0]["Allowance3amt"].ToString();
                        txtdeduction1percentage.Text = dtempLoad.Rows[0]["Deduction1per"].ToString();
                        txtdeduction2percentage.Text = dtempLoad.Rows[0]["Deduction2per"].ToString();
                        txtdeduction3Precentge.Text = dtempLoad.Rows[0]["Deduction3per"].ToString();
                        txtdeductionAmt1.Text = dtempLoad.Rows[0]["Deduction1amt"].ToString();
                        txtdeductionAmt2.Text = dtempLoad.Rows[0]["Deduction2amt"].ToString();
                        txtdeductionAmt3.Text = dtempLoad.Rows[0]["Deduction3amt"].ToString();
                        txtnet.Text = dtempLoad.Rows[0]["NetPay"].ToString();
                        txtmessDeductionLabour.Text = dtempLoad.Rows[0]["Messdeduction"].ToString();
                        txtHostel.Text = dtempLoad.Rows[0]["hosteldeduction"].ToString();
                        txtdeduction1.Text = dtempLoad.Rows[0]["Deduction1"].ToString();
                        txtdeduction2.Text = dtempLoad.Rows[0]["Deduction2"].ToString();
                        txtdeduction3.Text = dtempLoad.Rows[0]["Deduction3"].ToString();
                        ddlFood.SelectedValue = dtempLoad.Rows[0]["FoodType"].ToString();
                        txtLabourBasic.Text = dtempLoad.Rows[0]["DailySalary"].ToString();
                        txtlabourallow1.Text = dtempLoad.Rows[0]["LabourAllowace1"].ToString();
                        txtlabourallow2.Text = dtempLoad.Rows[0]["LabourAllowance2"].ToString();
                        txtlabourallowance3.Text = dtempLoad.Rows[0]["LabourAllowance3"].ToString();


                        if ((dtempLoad.Rows[0]["DailySalary"].ToString() == "") || (dtempLoad.Rows[0]["DailySalary"].ToString() == null))
                        {
                            txtDailySalary.Text = "0";
                        }
                        else
                        {
                            txtDailySalary.Text = dtempLoad.Rows[0]["DailySalary"].ToString();
                        }
                        //if(dtempLoad.Rows[0]["ProfileType"].ToString() == "1")
                        //{
                        //    rbtnprofile.SelectedValue = "1";
                        //}
                        //else
                        //{
                        //    rbtnprofile.SelectedValue = "2";
                        //}
                    }
                }
                else if (ddlcategory.SelectedValue == "2")
                {
                    DataTable dtempLoad = new DataTable();
                    dtempLoad = objdata.SalaryMasterEmpLoad_ForLabour(ddlempNo.SelectedValue,SessionCcode,SessionLcode);
                    if (dtempLoad.Rows.Count > 0)
                    {
                        lblError.Text = "";
                        txtexistingNo.Text = dtempLoad.Rows[0]["ExisistingCode"].ToString();
                        txtmessDeductionLabour.Text = dtempLoad.Rows[0]["Messdeduction"].ToString();
                        txtHostel.Text = dtempLoad.Rows[0]["hosteldeduction"].ToString();
                        txtdeduction1.Text = dtempLoad.Rows[0]["Deduction1"].ToString();
                        txtdeduction2.Text = dtempLoad.Rows[0]["Deduction2"].ToString();
                        txtdeduction3.Text = dtempLoad.Rows[0]["Deduction3"].ToString();
                        ddlFood.SelectedValue = dtempLoad.Rows[0]["ID"].ToString();
                        txtLabourBasic.Text = dtempLoad.Rows[0]["DailySalary"].ToString();
                        txtlabourallow1.Text = dtempLoad.Rows[0]["LabourAllowace1"].ToString();
                        txtlabourallow2.Text = dtempLoad.Rows[0]["LabourAllowance2"].ToString();
                        txtlabourallowance3.Text = dtempLoad.Rows[0]["LabourAllowance3"].ToString();
                        if (dtempLoad.Rows[0]["Wagestype"].ToString() == "2")
                        {
                            rbtnWages.SelectedValue = "2";
                        }
                        else if (dtempLoad.Rows[0]["Wagestype"].ToString() == "1")
                        {
                            rbtnWages.SelectedValue = "1";
                        }
                        else if (dtempLoad.Rows[0]["Wagestype"].ToString() == "3")
                        {
                            rbtnWages.SelectedValue = "3";
                        }
                        //if(dtempLoad.Rows[0]["ProfileType"].ToString() == "1")
                        //{
                        //    rbtnprofile.SelectedValue = "1";
                        //}
                        //else
                        //{
                        //    rbtnprofile.SelectedValue = "2";
                        //}
                    }
                    else
                    {

                    }

                }
            }
            else
            {
                clr();

            }
            DataTable dtprofile = new DataTable();
            dtprofile = objdata.SalaryMasterProfile(ddlempNo.SelectedValue,SessionCcode,SessionLcode);
            if (dtprofile.Rows.Count > 0)
            {
                lblError.Text = "";
                txtexistingNo.Text = dtprofile.Rows[0]["ExisistingCode"].ToString();
                if (dtprofile.Rows[0]["ProfileType"].ToString() == "1")
                {
                    //rbtnprofile.SelectedValue = "1";
                    Profilelbl.Text = "Probationary period";
                }
                else
                {
                    //rbtnprofile.SelectedValue = "2";
                    Profilelbl.Text = "Conformed emplooyee";
                }
            }
            if (ddlcategory.SelectedValue == "2")
            {
                string wage_type;
                wage_type = objdata.Wages_SP(ddlempNo.SelectedValue,SessionCcode,SessionLcode);
                if (wage_type == "2")
                {
                    lblError.Text = "";
                    rbtnWages.SelectedValue = "2";
                    panelMonthlyLabour1.Visible = true;
                    panelMonthlyLabour2.Visible = true;
                    panelMonthlyLabour3.Visible = true;
                    panelMonthlyLabour4.Visible = true;
                    lblLabourDailyWages.Visible = true;
                    lblLabourBasic.Visible = true;
                    lblLabourDailyWages.Visible = false;
                    PanelLabourMonthlyAllowace.Visible = true;

                }
                else if (wage_type == "1")
                {
                    rbtnWages.SelectedValue = "1";
                    panelMonthlyLabour1.Visible = true;
                    panelMonthlyLabour2.Visible = true;
                    panelMonthlyLabour3.Visible = true;
                    panelMonthlyLabour4.Visible = true;
                    lblLabourDailyWages.Visible = true;
                    lblLabourDailyWages.Visible = true;
                    lblLabourBasic.Visible = false;
                    PanelLabourMonthlyAllowace.Visible = true;
                }
            }

        }
    }
    protected void ddlEmpName_SelectedIndexChanged(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        //panelError.Visible = true;
        if (ddlEmpName.SelectedValue == "0")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Employee Code....!');", true);
            //System.Windows.Forms.MessageBox.Show("Select the Employee Code", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            //lblError.Text = "Select the Employee";
            ErrFlag = true;
        }
        if (!ErrFlag)
        {
            lblError.Text = "";
            DataTable dtemp2 = new DataTable();
            dtemp2 = objdata.BonusEmployeeName(ddlEmpName.SelectedValue,SessionCcode,SessionLcode);

            ddlempNo.DataSource = dtemp2;
            ddlempNo.DataTextField = "EmpNo";
            ddlempNo.DataValueField = "EmpNo";
            ddlempNo.DataBind();
            clr();
            string empCodeverify = objdata.SalaryMasterVerify(ddlempNo.SelectedValue,SessionCcode,SessionLcode);
            if (empCodeverify == ddlempNo.SelectedValue)
            {
                lblError.Text = "";
                if (ddlcategory.SelectedValue == "1")
                {
                    DataTable dtempLoad = new DataTable();
                    dtempLoad = objdata.SalaryMasterEmpLoad(ddlempNo.SelectedValue,SessionCcode,SessionLcode);
                    if (dtempLoad.Rows.Count > 0)
                    {
                        txtexistingNo.Text = dtempLoad.Rows[0]["ExisistingCode"].ToString();
                        txtBasic.Text = dtempLoad.Rows[0]["BasicSalary"].ToString();
                        Profilelbl.Text = dtempLoad.Rows[0]["ProfileType"].ToString();
                        txthrapercentage.Text = dtempLoad.Rows[0]["HRAPer"].ToString();
                        txtHRAamt.Text = dtempLoad.Rows[0]["HRAamt"].ToString();
                        txtconveyanceper.Text = dtempLoad.Rows[0]["ConveyancePer"].ToString();
                        txtconveyanceAmt.Text = dtempLoad.Rows[0]["ConveyanceAmt"].ToString();
                        //txtccaPercentage.Text = dtempLoad.Rows[0]["CCAper"].ToString();
                        //txtCCAamt.Text = dtempLoad.Rows[0]["CCAamt"].ToString();
                        txtmedicalPercentage.Text = dtempLoad.Rows[0]["Medicalper"].ToString();
                        txtmedicalAmt.Text = dtempLoad.Rows[0]["Medicalamt"].ToString();
                        txtall1percentage.Text = dtempLoad.Rows[0]["Allowance1per"].ToString();
                        txtallowance1Amt.Text = dtempLoad.Rows[0]["Alllowance1amt"].ToString();
                        txtAllowance2percentage.Text = dtempLoad.Rows[0]["Allowance2per"].ToString();
                        txtAllowavce2Amt.Text = dtempLoad.Rows[0]["Allowance2amt"].ToString();
                        txtAllowance3percentage.Text = dtempLoad.Rows[0]["Allowance3per"].ToString();
                        txtAllowance3Amt.Text = dtempLoad.Rows[0]["Allowance3amt"].ToString();
                        txtdeduction1percentage.Text = dtempLoad.Rows[0]["Deduction1per"].ToString();
                        txtdeduction2percentage.Text = dtempLoad.Rows[0]["Deduction2per"].ToString();
                        txtdeduction3Precentge.Text = dtempLoad.Rows[0]["Deduction3per"].ToString();
                        txtdeductionAmt1.Text = dtempLoad.Rows[0]["Deduction1amt"].ToString();
                        txtdeductionAmt2.Text = dtempLoad.Rows[0]["Deduction2amt"].ToString();
                        txtdeductionAmt3.Text = dtempLoad.Rows[0]["Deduction3amt"].ToString();
                        txtnet.Text = dtempLoad.Rows[0]["NetPay"].ToString();
                        txtmessDeductionLabour.Text = dtempLoad.Rows[0]["Messdeduction"].ToString();
                        txtHostel.Text = dtempLoad.Rows[0]["hosteldeduction"].ToString();
                        txtdeduction1.Text = dtempLoad.Rows[0]["Deduction1"].ToString();
                        txtdeduction2.Text = dtempLoad.Rows[0]["Deduction2"].ToString();
                        txtdeduction3.Text = dtempLoad.Rows[0]["Deduction3"].ToString();
                        ddlFood.SelectedValue = dtempLoad.Rows[0]["FoodType"].ToString();
                        txtLabourBasic.Text = dtempLoad.Rows[0]["DailySalary"].ToString();
                        txtlabourallow1.Text = dtempLoad.Rows[0]["LabourAllowace1"].ToString();
                        txtlabourallow2.Text = dtempLoad.Rows[0]["LabourAllowance2"].ToString();
                        txtlabourallowance3.Text = dtempLoad.Rows[0]["LabourAllowance3"].ToString();


                        if ((dtempLoad.Rows[0]["DailySalary"].ToString() == "") || (dtempLoad.Rows[0]["DailySalary"].ToString() == null))
                        {
                            txtDailySalary.Text = "0";
                        }
                        else
                        {
                            txtDailySalary.Text = dtempLoad.Rows[0]["DailySalary"].ToString();
                        }
                    }
                }
                else if (ddlcategory.SelectedValue == "2")
                {

                    DataTable dtempLoad = new DataTable();
                    //WorkStopped
                    dtempLoad = objdata.SalaryMasterEmpLoad_ForLabour(ddlempNo.SelectedValue,SessionCcode,SessionLcode);
                    if (dtempLoad.Rows.Count > 0)
                    {
                        lblError.Text = "";
                        txtexistingNo.Text = dtempLoad.Rows[0]["ExisistingCode"].ToString();
                        txtmessDeductionLabour.Text = dtempLoad.Rows[0]["Messdeduction"].ToString();
                        txtHostel.Text = dtempLoad.Rows[0]["hosteldeduction"].ToString();
                        txtdeduction1.Text = dtempLoad.Rows[0]["Deduction1"].ToString();
                        txtdeduction2.Text = dtempLoad.Rows[0]["Deduction2"].ToString();
                        txtdeduction3.Text = dtempLoad.Rows[0]["Deduction3"].ToString();
                        ddlFood.SelectedValue = dtempLoad.Rows[0]["ID"].ToString();
                        txtLabourBasic.Text = dtempLoad.Rows[0]["DailySalary"].ToString();
                        txtlabourallow1.Text = dtempLoad.Rows[0]["LabourAllowace1"].ToString();
                        txtlabourallow2.Text = dtempLoad.Rows[0]["LabourAllowance2"].ToString();
                        txtlabourallowance3.Text = dtempLoad.Rows[0]["LabourAllowance3"].ToString();
                        if ((dtempLoad.Rows[0]["DailySalary"].ToString() == "") || (dtempLoad.Rows[0]["DailySalary"].ToString() == null))
                        {
                            txtDailySalary.Text = "0";
                        }
                        else
                        {
                            txtDailySalary.Text = dtempLoad.Rows[0]["DailySalary"].ToString();
                        }
                        if (dtempLoad.Rows[0]["Wagestype"].ToString() == "2")
                        {
                            rbtnWages.SelectedValue = "2";
                        }
                        else if (dtempLoad.Rows[0]["Wagestype"].ToString() == "1")
                        {
                            rbtnWages.SelectedValue = "1";
                        }
                        else if (dtempLoad.Rows[0]["Wagestype"].ToString() == "3")
                        {
                            rbtnWages.SelectedValue = "3";
                        }
                        //if(dtempLoad.Rows[0]["ProfileType"].ToString() == "1")
                        //{
                        //    rbtnprofile.SelectedValue = "1";
                        //}
                        //else
                        //{
                        //    rbtnprofile.SelectedValue = "2";
                        //}
                    }

                }

            }
            else
            {
                clr();
            }
            DataTable dtprofile = new DataTable();
            dtprofile = objdata.SalaryMasterProfile(ddlempNo.SelectedValue,SessionCcode,SessionLcode);
            if (dtprofile.Rows.Count > 0)
            {
                lblError.Text = "";
                txtexistingNo.Text = dtprofile.Rows[0]["ExisistingCode"].ToString();
                if (dtprofile.Rows[0]["ProfileType"].ToString() == "1")
                {
                    //rbtnprofile.SelectedValue = "1";
                    Profilelbl.Text = "Probationary period";
                }
                else
                {
                    //rbtnprofile.SelectedValue = "2";
                    Profilelbl.Text = "Conformed emplooyee";
                }
            }
            if (ddlcategory.SelectedValue == "2")
            {
                lblError.Text = "";
                string wage_type;
                wage_type = objdata.Wages_SP(ddlEmpName.SelectedValue,SessionCcode, SessionLcode);
                if (wage_type == "2")
                {

                    rbtnWages.SelectedValue = "2";
                    panelMonthlyLabour1.Visible = true;
                    panelMonthlyLabour2.Visible = true;
                    panelMonthlyLabour3.Visible = true;
                    panelMonthlyLabour4.Visible = true;
                    lblLabourDailyWages.Visible = true;
                    lblLabourBasic.Visible = true;
                    lblLabourDailyWages.Visible = false;
                    PanelLabourMonthlyAllowace.Visible = true;

                }
                else if (wage_type == "1")
                {
                    rbtnWages.SelectedValue = "1";
                    panelMonthlyLabour1.Visible = true;
                    panelMonthlyLabour2.Visible = true;
                    panelMonthlyLabour3.Visible = true;
                    panelMonthlyLabour4.Visible = true;
                    lblLabourDailyWages.Visible = true;
                    lblLabourDailyWages.Visible = true;
                    lblLabourBasic.Visible = false;
                    PanelLabourMonthlyAllowace.Visible = true;
                }
            }

        }
    }
    protected void txtDailySalary_TextChanged(object sender, EventArgs e)
    {

    }
    protected void rbtnWages_SelectedIndexChanged(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        //panelError.Visible = true;
        if (ddlDepartment.SelectedValue == "0")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Department....!');", true);
            //System.Windows.Forms.MessageBox.Show("Select the Department", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            //lblError.Text = "Select the Department";
            ErrFlag = true;
        }
        if (!ErrFlag)
        {
            if (rbtnWages.SelectedValue == "1")
            {
                staffLabour = "L";

                DataTable dtempcode = new DataTable();
                //work stopped
                dtempcode = objdata.SalaryMasterLabour_Load(ddlDepartment.SelectedValue, rbtnWages.SelectedValue,SessionCcode,SessionLcode);
                ddlempNo.DataSource = dtempcode;
                DataRow dr = dtempcode.NewRow();
                dr["EmpNo"] = ddlDepartment.SelectedItem.Text;
                dr["EmpName"] = ddlDepartment.SelectedItem.Text;
                dtempcode.Rows.InsertAt(dr, 0);
                ddlempNo.DataTextField = "EmpNo";
                ddlempNo.DataValueField = "EmpNo";
                ddlempNo.DataBind();
                ddlEmpName.DataSource = dtempcode;
                ddlEmpName.DataTextField = "EmpName";
                ddlEmpName.DataValueField = "EmpNo";
                ddlEmpName.DataBind();
                if (dtempcode.Rows.Count > 0)
                {
                    PanelEmp.Visible = true;
                    PanelWagesLabour.Visible = true;
                    PanelDailyLabour.Visible = false;
                    panelStaff1.Visible = false;
                    Panelsaff2.Visible = false;
                    //panelsstaff3.Visible = false;
                    panelstaff4.Visible = false;
                    Panelstaff5.Visible = false;
                    panelstaff6.Visible = false;
                    Panelstaff7.Visible = false;
                    Panelstaff8.Visible = false;
                    Panelstaff10.Visible = false;
                    panelMonthlyLabour1.Visible = true;
                    panelMonthlyLabour2.Visible = true;
                    panelMonthlyLabour3.Visible = true;
                    panelMonthlyLabour4.Visible = true;
                    lblLabourDailyWages.Visible = true;
                    lblLabourBasic.Visible = false;
                    PanelLabourMonthlyAllowace.Visible = false;

                }
                else
                {
                    PanelEmp.Visible = false;
                    PanelEmp.Visible = false;
                    panelStaff1.Visible = false;
                    Panelsaff2.Visible = false;
                    //panelsstaff3.Visible = false;
                    panelstaff4.Visible = false;
                    Panelstaff5.Visible = false;
                    panelstaff6.Visible = false;
                    Panelstaff7.Visible = false;
                    Panelstaff8.Visible = false;
                    Panelstaff10.Visible = false;
                    PanelWagesLabour.Visible = true;
                    PanelDailyLabour.Visible = false;
                    panelMonthlyLabour1.Visible = false;
                    panelMonthlyLabour2.Visible = false;
                    panelMonthlyLabour3.Visible = false;
                    panelMonthlyLabour4.Visible = false;
                    PanelLabourMonthlyAllowace.Visible = false;

                }
                clr();
            }
            else if (rbtnWages.SelectedValue == "2")
            {
                staffLabour = "L";

                DataTable dtempcode = new DataTable();
                dtempcode = objdata.SalaryMasterLabour_Load(ddlDepartment.SelectedValue, rbtnWages.SelectedValue,SessionCcode, SessionLcode);
                ddlempNo.DataSource = dtempcode;
                DataRow dr = dtempcode.NewRow();
                dr["EmpNo"] = ddlDepartment.SelectedItem.Text;
                dr["EmpName"] = ddlDepartment.SelectedItem.Text;
                dtempcode.Rows.InsertAt(dr, 0);

                ddlempNo.DataTextField = "EmpNo";
                ddlempNo.DataValueField = "EmpNo";
                ddlempNo.DataBind();
                ddlEmpName.DataSource = dtempcode;
                ddlEmpName.DataTextField = "EmpName";
                ddlEmpName.DataValueField = "EmpNo";
                ddlEmpName.DataBind();
                if (dtempcode.Rows.Count > 0)
                {
                    PanelEmp.Visible = true;
                    PanelDailyLabour.Visible = false;
                    panelStaff1.Visible = false;
                    Panelsaff2.Visible = false;
                    //panelsstaff3.Visible = false;
                    panelstaff4.Visible = false;
                    Panelstaff5.Visible = false;
                    panelstaff6.Visible = false;
                    Panelstaff7.Visible = false;
                    Panelstaff8.Visible = false;
                    Panelstaff10.Visible = false;
                    PanelWagesLabour.Visible = true;
                    panelMonthlyLabour1.Visible = true;
                    panelMonthlyLabour2.Visible = true;
                    panelMonthlyLabour3.Visible = true;
                    panelMonthlyLabour4.Visible = true;
                    lblLabourDailyWages.Visible = false;
                    lblLabourBasic.Visible = true;
                    PanelLabourMonthlyAllowace.Visible = true;
                }
                else
                {
                    PanelEmp.Visible = false;
                    PanelDailyLabour.Visible = false;
                    panelStaff1.Visible = false;
                    Panelsaff2.Visible = false;
                    //panelsstaff3.Visible = false;
                    panelstaff4.Visible = false;
                    Panelstaff5.Visible = false;
                    panelstaff6.Visible = false;
                    Panelstaff7.Visible = false;
                    Panelstaff8.Visible = false;
                    Panelstaff10.Visible = false;
                    PanelWagesLabour.Visible = true;
                    panelMonthlyLabour1.Visible = false;
                    panelMonthlyLabour2.Visible = false;
                    panelMonthlyLabour3.Visible = false;
                    panelMonthlyLabour4.Visible = false;
                    PanelLabourMonthlyAllowace.Visible = false;
                }
                clr();
            }


        }
    }
    protected void btnCalculation_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        //panelError.Visible = true;
        string Total = "", GrdTotal = "";
        if ((txtBasic.Text == "") || (txtBasic.Text == null) || (txtBasic.Text == "0"))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Basic Salary....!');", true);
            //System.Windows.Forms.MessageBox.Show("Enter the Basic Salary", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            //lblError.Text = "Enter the Basic Salary";
            ErrFlag = true;
        }
        else if ((txthrapercentage.Text == "") || (txthrapercentage.Text == null))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the HRA %....!');", true);
            //System.Windows.Forms.MessageBox.Show("Enter the HRA %", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            //lblError.Text = "Enter the HRA %";
            ErrFlag = true;
        }
        //else if ((txtHRAamt.Text == "") || (txtHRAamt.Text == null))
        //{
        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Data Properly....!');", true);
        //    ErrFlag = true;
        //}
        else if ((txtconveyanceper.Text == "") || (txtconveyanceper.Text == null))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Conveyance %....!');", true);
            //System.Windows.Forms.MessageBox.Show("Enter the Conveyance %", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            //lblError.Text = "Enter the Conveyance %";
            ErrFlag = true;
        }
        //else if ((txtconveyanceAmt.Text == "") || (txtconveyanceAmt.Text == null))
        //{
        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Data Properly....!');", true);
        //    ErrFlag = true;
        //}
        //else if ((txtccaPercentage.Text == "") || (txtccaPercentage.Text == null))
        //{
        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the CCA %....!');", true);
        //    ErrFlag = true;
        //}
        //else if ((txtCCAamt.Text == "") || (txtCCAamt.Text == null))
        //{
        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Data Properly....!');", true);
        //    ErrFlag = true;
        //}
        else if ((txtmedicalPercentage.Text == "") || (txtmedicalPercentage.Text == null))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Medical Reimburesement %....!');", true);
            //System.Windows.Forms.MessageBox.Show("Enter the Medical Reimburessement", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            //lblError.Text = "Enter the Medical Reimburesement %";
            ErrFlag = true;
        }
        //else if ((txtmedicalAmt.Text == "") || (txtmedicalAmt.Text == null))
        //{
        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Data Properly....!');", true);
        //    ErrFlag = true;
        //}
        else if ((txtall1percentage.Text == "") || (txtall1percentage.Text == null))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Allowance 1 %...!');", true);
            //System.Windows.Forms.MessageBox.Show("Enter the Allowance 1 %", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            //lblError.Text = "Enter the Allowance 1 %";
            ErrFlag = true;
        }
        //else if ((txtallowance1Amt.Text == "") || (txtallowance1Amt.Text == null))
        //{
        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Data Properly....!');", true);
        //    ErrFlag = true;
        //}
        else if ((txtAllowance2percentage.Text == "") || (txtAllowance2percentage.Text == null))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Allowance 2 %....!');", true);
            //System.Windows.Forms.MessageBox.Show("Enter the Allowance 2", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            //lblError.Text = "Enter the Allowance 2 %";
            ErrFlag = true;
        }
        //else if ((txtAllowavce2Amt.Text == "") || (txtAllowavce2Amt.Text == null))
        //{
        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Data Properly....!');", true);
        //    ErrFlag = true;
        //}
        else if ((txtAllowance3percentage.Text == "") || (txtAllowance3percentage.Text == null))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Allowance 3 %....!');", true);
            //System.Windows.Forms.MessageBox.Show("Enter the Allowance 3 %", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            //lblError.Text = "Enter the Allowance 3 %";
            ErrFlag = true;
        }

        //else if ((txtAllowance3Amt.Text == "") || (txtAllowance3Amt.Text == null))
        //{
        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Data Properly....!');", true);
        //    ErrFlag = true;
        //}
        else if ((txtdeduction1percentage.Text == "") || (txtdeduction1percentage.Text == null))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Deduction 1 %....!');", true);
            //System.Windows.Forms.MessageBox.Show("Enter the Deduction 1 %", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            //lblError.Text = "Enter the Deduction 1 %";
            ErrFlag = true;
        }
        else if ((txtdeduction2percentage.Text == "") || (txtdeduction2percentage.Text == null))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Deduction 2 %....!');", true);
            //System.Windows.Forms.MessageBox.Show("Enter the Deduction 2 %", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            //lblError.Text = "Enter the Deduction 2 %";
            ErrFlag = true;
        }
        else if ((txtdeduction3Precentge.Text == "") || (txtdeduction3Precentge.Text == null))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Deduction 3 %....!');", true);
            //System.Windows.Forms.MessageBox.Show("Enter the Deduction 3 %", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            //lblError.Text = "Enter the Deduction 3 %";
            ErrFlag = true;
        }
        if (!ErrFlag)
        {
            txtHRAamt.Text = ((Convert.ToDecimal(txtBasic.Text.Trim()) / 100) * Convert.ToDecimal(txthrapercentage.Text)).ToString();
            txtconveyanceAmt.Text = ((Convert.ToDecimal(txtBasic.Text.Trim()) / 100) * Convert.ToDecimal(txtconveyanceper.Text)).ToString();
            //txtCCAamt.Text = ((Convert.ToDecimal(txtBasic.Text.Trim()) / 100) * Convert.ToDecimal(txtccaPercentage.Text)).ToString();
            txtmedicalAmt.Text = ((Convert.ToDecimal(txtBasic.Text.Trim()) / 100) * Convert.ToDecimal(txtmedicalPercentage.Text)).ToString();
            txtallowance1Amt.Text = ((Convert.ToDecimal(txtBasic.Text.Trim()) / 100) * Convert.ToDecimal(txtall1percentage.Text)).ToString();
            txtAllowavce2Amt.Text = ((Convert.ToDecimal(txtBasic.Text.Trim()) / 100) * Convert.ToDecimal(txtAllowance2percentage.Text)).ToString();
            txtAllowance3Amt.Text = ((Convert.ToDecimal(txtBasic.Text.Trim()) / 100) * Convert.ToDecimal(txtAllowance3percentage.Text)).ToString();
            txtdeductionAmt1.Text = ((Convert.ToDecimal(txtBasic.Text.Trim()) / 100) * Convert.ToDecimal(txtdeduction1percentage.Text)).ToString();
            txtdeductionAmt2.Text = ((Convert.ToDecimal(txtBasic.Text.Trim()) / 100) * Convert.ToDecimal(txtdeduction2percentage.Text)).ToString();
            txtdeductionAmt3.Text = ((Convert.ToDecimal(txtBasic.Text.Trim()) / 100) * Convert.ToDecimal(txtdeduction3Precentge.Text)).ToString();
            Total = (Convert.ToDecimal(txtBasic.Text) + Convert.ToDecimal(txtHRAamt.Text) + Convert.ToDecimal(txtconveyanceAmt.Text) + Convert.ToDecimal(txtmedicalAmt.Text) + Convert.ToDecimal(txtallowance1Amt.Text) + Convert.ToDecimal(txtAllowavce2Amt.Text) + Convert.ToDecimal(txtAllowance3Amt.Text)).ToString();
            GrdTotal = (Convert.ToDecimal(txtdeductionAmt1.Text) + Convert.ToDecimal(txtdeductionAmt2.Text) + Convert.ToDecimal(txtdeductionAmt3.Text)).ToString();
            txtnet.Text = (Convert.ToDecimal(Total) - Convert.ToDecimal(GrdTotal)).ToString();
            lblError.Text = "";
        }
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        bool Insert = false;
        bool Update = false;
        //panelError.Visible = true;
        //Work Stopped
        string contactsearch = objdata.SearchEmployee_save_SP(ddlempNo.SelectedValue, SessionCcode, SessionLcode);
        if (ddlcategory.SelectedValue == "2")
        {
            if ((txtLabourBasic.Text == "") || (txtLabourBasic.Text == null) || (txtLabourBasic.Text == "0.00") || (txtLabourBasic.Text == "0"))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Basic Salary....!');", true);
                //System.Windows.Forms.MessageBox.Show("Enter the Basic Salary", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                //lblError.Text = "Enter the Basic Salary";
                ErrFlag = true;
            }
            else if ((txtmessDeductionLabour.Text == "") || (txtmessDeductionLabour.Text == null))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Mess Amount....!');", true);
                //System.Windows.Forms.MessageBox.Show("Enter the Mess Amount", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                //lblError.Text = "Enter the Mess Amount";
                ErrFlag = true;
            }
            else if ((txtHostel.Text == "") || (txtHostel.Text == null))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Hostel Amount....!');", true);
                //System.Windows.Forms.MessageBox.Show("Enter the Hostel Amount", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                //lblError.Text = "Enter the Hostel Amount";
                ErrFlag = true;
            }
            else if ((txtdeduction1.Text == "") || (txtdeduction1.Text == null))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Deduction 1  Amount....!');", true);
                //System.Windows.Forms.MessageBox.Show("Enter the Deduction 1 Amount", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                //lblError.Text = "Enter the Deduction 2 Amount";
                ErrFlag = true;
            }
            else if ((txtdeduction2.Text == "") || (txtdeduction2.Text == null))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Deduction 2 Amount....!');", true);
                //System.Windows.Forms.MessageBox.Show("Enter the Deduction 2 Amount", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                //lblError.Text = "Enter the Deduction 2 Amount";
                ErrFlag = true;
            }
            else if ((txtdeduction3.Text == "") || (txtdeduction3.Text == null))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Deduction 3 Amount....!');", true);
                //System.Windows.Forms.MessageBox.Show("Enter the Deduction 3 Amount", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                //lblError.Text = "Enter the Deduction 3 Amount";
                ErrFlag = true;
            }
            else if (contactsearch != ddlempNo.SelectedValue)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Employee Properly...!');", true);
                //System.Windows.Forms.MessageBox.Show("Enter the Employee properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                //lblError.Text = "Enter the Employee Properly";
                ErrFlag = true;
            }
            if (!ErrFlag)
            {
                SalaryMasterClass objsal = new SalaryMasterClass();
                objsal.EmpNo = ddlempNo.SelectedValue;
                //objsal.ProfileType = rbtnprofile.SelectedValue;
                objsal.Basic = txtBasic.Text;
                objsal.hraper = txthrapercentage.Text;
                objsal.hraAmt = txtHRAamt.Text;
                objsal.conveyanceper = txtconveyanceper.Text;
                objsal.conveyanceamt = txtconveyanceAmt.Text;
                //objsal.CCAper = txtccaPercentage.Text;
                //objsal.CCaamt = txtCCAamt.Text;
                objsal.medicalper = txtmedicalPercentage.Text;
                objsal.medicalamt = txtmedicalAmt.Text;
                objsal.Allowance1per = txtall1percentage.Text;
                objsal.Allowance1Amt = txtallowance1Amt.Text;
                objsal.Allowance2per = txtAllowance2percentage.Text;
                objsal.Allowance2Amt = txtAllowavce2Amt.Text;
                objsal.Allowance3per = txtAllowance3percentage.Text;
                objsal.Allowance3Amt = txtAllowance3Amt.Text;
                objsal.StaffDeduction1Per = txtdeduction1percentage.Text;
                objsal.StaffDeduction2Per = txtdeduction2percentage.Text;
                objsal.StaffDeduction3Per = txtdeduction3Precentge.Text;
                objsal.StaffDecution1 = txtdeductionAmt1.Text;
                objsal.StaffDecution2 = txtdeductionAmt2.Text;
                objsal.StaffDecution3 = txtdeductionAmt3.Text;
                objsal.netPay = txtnet.Text;
                objsal.dailySalary = txtLabourBasic.Text;
                //   objsal.username = Session["UserId"].ToString();
                objsal.Finance = ddFinance.SelectedValue;
                objsal.FoodType = ddlFood.SelectedValue;
                objsal.mess = txtmessDeductionLabour.Text;
                objsal.Hostel = txtHostel.Text;
                objsal.deduction1 = txtdeduction1.Text;
                objsal.deduction2 = txtdeduction2.Text;
                objsal.deduction3 = txtdeduction3.Text;
                objsal.LabourAllowance1 = txtlabourallow1.Text;
                objsal.LabourAllowance2 = txtlabourallow2.Text;
                objsal.LabourAllowance3 = txtlabourallowance3.Text;
                objsal.Ccode = SessionCcode;
                objsal.Lcode = SessionLcode;
                string empCodeverify = objdata.SalaryMasterVerify(ddlempNo.SelectedValue,SessionCcode,SessionLcode);
                if (empCodeverify == ddlempNo.SelectedValue)
                {
                    objdata.SalaryMaster_Update(objsal);
                    Update = true;
                }
                else
                {
                    objdata.SalaryMaster_Insert(objsal);
                    Insert = true;
                }
                if (Insert == true)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Saved Successfully....!');", true);
                    //System.Windows.Forms.MessageBox.Show("Saved Successfully", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    lblError.Text = "";
                    btnclick.Enabled = false;
                    btnSave.Enabled = false;
                    btnreset.Enabled = false;
                    btncancel.Enabled = false;
                    btnexitingSearch.Enabled = false;
                    panelError.Visible = false;
                    //panelsuccess.Visible = true;
                    //lbloutput.Text = "Saved Successfully";
                    btnok.Focus();
                }
                if (Update == true)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Updated Successfully....!');", true);
                    //System.Windows.Forms.MessageBox.Show("Updated Successfully", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    lblError.Text = "";
                    btnclick.Enabled = false;
                    btnSave.Enabled = false;
                    btnreset.Enabled = false;
                    btncancel.Enabled = false;
                    btnexitingSearch.Enabled = false;
                    panelError.Visible = false;
                    //panelsuccess.Visible = true;
                    //lbloutput.Text = "Updated Successfully";
                    //btnok.Focus();
                }

            }
        }
        else
        {
            if ((txtBasic.Text == "") || (txtBasic.Text == null) || (txtBasic.Text == "0"))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Basic Salary....!');", true);
                //System.Windows.Forms.MessageBox.Show("Enter the Basic Salary", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                //lblError.Text = "Enter the Basic Salary";
                ErrFlag = true;
            }
            else if ((txthrapercentage.Text == "") || (txthrapercentage.Text == null))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the HRA %....!');", true);
                //System.Windows.Forms.MessageBox.Show("Enter the HRA %", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                //lblError.Text = "Enter the HRA %";
                ErrFlag = true;
            }
            else if ((txtHRAamt.Text == "") || (txtHRAamt.Text == null))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Data Properly....!');", true);
                //System.Windows.Forms.MessageBox.Show("Enter the Data Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                //lblError.Text = "Enter the HRA Amount Properly";
                ErrFlag = true;
            }
            else if ((txtconveyanceper.Text == "") || (txtconveyanceper.Text == null))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Conveyance %....!');", true);
                //System.Windows.Forms.MessageBox.Show("Enter the Conveyance %", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                //lblError.Text = "Enter the Conveyance %";
                ErrFlag = true;
            }
            else if ((txtconveyanceAmt.Text == "") || (txtconveyanceAmt.Text == null))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Data Properly....!');", true);
                //System.Windows.Forms.MessageBox.Show("Enter the Data Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                //lblError.Text = "Enter the Conveyance Amount Properly";
                ErrFlag = true;
            }
            else if (contactsearch != ddlempNo.SelectedValue)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Employee Properly...!');", true);
                //System.Windows.Forms.MessageBox.Show("Select the Employee properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                //lblError.Text = "Select the Employee ID";
                ErrFlag = true;
            }
            //else if ((txtccaPercentage.Text == "") || (txtccaPercentage.Text == null))
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the CCA %....!');", true);
            //    ErrFlag = true;
            //}
            //else if ((txtCCAamt.Text == "") || (txtCCAamt.Text == null))
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Data Properly....!');", true);
            //    ErrFlag = true;
            //}
            else if ((txtmedicalPercentage.Text == "") || (txtmedicalPercentage.Text == null))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Medical Reimburesement %....!');", true);
                //System.Windows.Forms.MessageBox.Show("Enter the Medical reimburesement %", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                //lblError.Text = "Enter Medical Reimburessement %";
                ErrFlag = true;
            }
            else if ((txtmedicalAmt.Text == "") || (txtmedicalAmt.Text == null))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Data Properly....!');", true);
                //System.Windows.Forms.MessageBox.Show("Enter the Data Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                //lblError.Text = "Enter the Medical Reimburesement Amount Properly";
                ErrFlag = true;
            }
            else if ((txtall1percentage.Text == "") || (txtall1percentage.Text == null))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Allowance 1 %...!');", true);
                //System.Windows.Forms.MessageBox.Show("Enter the Allowance 1 %", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                //lblError.Text = "Enter the Allowance 1 %";
                ErrFlag = true;
            }
            else if ((txtallowance1Amt.Text == "") || (txtallowance1Amt.Text == null))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Data Properly....!');", true);
                //System.Windows.Forms.MessageBox.Show("Enter the Data properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                //lblError.Text = "Enter the Allowance 1 Amount Properly";
                ErrFlag = true;
            }
            else if ((txtAllowance2percentage.Text == "") || (txtAllowance2percentage.Text == null))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Allowance 2 %....!');", true);
                //System.Windows.Forms.MessageBox.Show("Enter the Allowance 2 %", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                //lblError.Text = "Enter the Allowance 2 %";
                ErrFlag = true;
            }
            else if ((txtAllowavce2Amt.Text == "") || (txtAllowavce2Amt.Text == null))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Data Properly....!');", true);
                //System.Windows.Forms.MessageBox.Show("Enter the Data Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                //lblError.Text = "Enter the Allowance 2 Amount Properly";
                ErrFlag = true;
            }
            else if ((txtAllowance3percentage.Text == "") || (txtAllowance3percentage.Text == null))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Allowance 3 %....!');", true);
                //System.Windows.Forms.MessageBox.Show("Enter the Allowance 3 %", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                //lblError.Text = "Enter the Allowance 3 %";
                ErrFlag = true;
            }
            else if ((txtAllowance3Amt.Text == "") || (txtAllowance3Amt.Text == null))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Data Properly....!');", true);
                //System.Windows.Forms.MessageBox.Show("", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                //lblError.Text = "Enter the Data Properly";
                ErrFlag = true;
            }
            else if (PanelEmp.Visible == false)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Employee....!');", true);
                //System.Windows.Forms.MessageBox.Show("Select the Employee", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                //lblError.Text = "Select the Employee";
                ErrFlag = true;
            }
            else if ((txtnet.Text == "") || (txtnet.Text == null))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Click the Calculation Button....!');", true);
                //System.Windows.Forms.MessageBox.Show("Click the Calculation", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                //lblError.Text = "Click the Calculation Button";
                ErrFlag = true;
            }
            //else if (rbtnprofile.SelectedValue == "")
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Profile Type....!');", true);
            //    ErrFlag = true;
            //}
            if (!ErrFlag)
            {
                SalaryMasterClass objsal = new SalaryMasterClass();
                objsal.EmpNo = ddlempNo.SelectedValue;
                //objsal.ProfileType = rbtnprofile.SelectedValue;
                objsal.Basic = txtBasic.Text;
                objsal.hraper = txthrapercentage.Text;
                objsal.hraAmt = txtHRAamt.Text;
                objsal.conveyanceper = txtconveyanceper.Text;
                objsal.conveyanceamt = txtconveyanceAmt.Text;
                //objsal.CCAper = txtccaPercentage.Text;
                //objsal.CCaamt = txtCCAamt.Text;
                objsal.medicalper = txtmedicalPercentage.Text;
                objsal.medicalamt = txtmedicalAmt.Text;
                objsal.Allowance1per = txtall1percentage.Text;
                objsal.Allowance1Amt = txtallowance1Amt.Text;
                objsal.Allowance2per = txtAllowance2percentage.Text;
                objsal.Allowance2Amt = txtAllowavce2Amt.Text;
                objsal.Allowance3per = txtAllowance3percentage.Text;
                objsal.Allowance3Amt = txtAllowance3Amt.Text;
                objsal.StaffDeduction1Per = txtdeduction1percentage.Text;
                objsal.StaffDeduction2Per = txtdeduction2percentage.Text;
                objsal.StaffDeduction3Per = txtdeduction3Precentge.Text;
                objsal.StaffDecution1 = txtdeductionAmt1.Text;
                objsal.StaffDecution2 = txtdeductionAmt2.Text;
                objsal.StaffDecution3 = txtdeductionAmt3.Text;
                objsal.netPay = txtnet.Text;
                objsal.dailySalary = txtLabourBasic.Text;
                objsal.username = Session["UserId"].ToString();
                objsal.Finance = ddFinance.SelectedValue;
                objsal.FoodType = "0";
                objsal.mess = txtmessDeductionLabour.Text;
                objsal.Hostel = txtHostel.Text;
                objsal.deduction1 = txtdeduction1.Text;
                objsal.deduction2 = txtdeduction2.Text;
                objsal.deduction3 = txtdeduction3.Text;
                objsal.LabourAllowance1 = txtlabourallow1.Text;
                objsal.LabourAllowance2 = txtlabourallow2.Text;
                objsal.LabourAllowance3 = txtlabourallowance3.Text;
                objsal.Ccode = SessionCcode;
                objsal.Lcode = SessionLcode;
                string empCodeverify = objdata.SalaryMasterVerify(ddlempNo.SelectedValue, SessionCcode, SessionLcode);
                if (empCodeverify == ddlempNo.SelectedValue)
                {
                    objdata.SalaryMaster_Update(objsal);
                    Update = true;
                }
                else
                {
                    objdata.SalaryMaster_Insert(objsal);
                    Insert = true;
                }
                if (Insert == true)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Saved Successfully....!');", true);
                    //System.Windows.Forms.MessageBox.Show("Saved Successfully", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    //lblError.Text = "";
                    //panelError.Visible = false;
                    ////panelsuccess.Visible = true;
                    //btnclick.Enabled = false;
                    //btnSave.Enabled = false;
                    //btnreset.Enabled = false;
                    //btncancel.Enabled = false;
                    //btnexitingSearch.Enabled = false;
                    ////lbloutput.Text = "Saved Successfully";
                    ////btnok.Focus();
                }
                if (Update == true)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Updated Successfully....!');", true);
                    //System.Windows.Forms.MessageBox.Show("Updated Successfully", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    //lblError.Text = "";
                    //panelError.Visible = false;
                    ////panelsuccess.Visible = true;
                    //btnclick.Enabled = false;
                    //btnSave.Enabled = false;
                    //btnreset.Enabled = false;
                    //btncancel.Enabled = false;
                    //btnexitingSearch.Enabled = false;
                    ////lbloutput.Text = "Updated Successfully";
                    ////btnok.Focus();
                }

            }
        }
    }
    protected void btnreset_Click(object sender, EventArgs e)
    {
        clr();
    }
    protected void btncancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("SalaryMaster.aspx");
    }
    protected void btnok_Click(object sender, EventArgs e)
    {
        clr();
    }
    protected void ddFinance_SelectedIndexChanged(object sender, EventArgs e)
    {
        lblError.Text = "";
    }
}
