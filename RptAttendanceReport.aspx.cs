﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Payroll;
using Payroll.Data;
using Payroll.Configuration;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html;
using iTextSharp.text.html.simpleparser;


using Altius.BusinessAccessLayer.BALDataAccess;

public partial class RptAttendanceReport_1 : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    string SessionAdmin;
    string Stafflabour;
    DateTime FromDate;
    DateTime ToDate;
    string SessionCcode;
    string SessionLcode;
    protected void Page_Load(object sender, EventArgs e)
    {
        
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionAdmin = Session["Isadmin"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        string ss = Session["UserId"].ToString();
        if (!IsPostBack)
        {
            //Department();
            DropDwonCategory();
            int currentYear = Utility.GetFinancialYear;
            for (int i = 0; i < 10; i++)
            {
                ddlFinaYear.Items.Add(new System.Web.UI.WebControls.ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                //  ddlShowYear.Items.Add(new ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                currentYear = currentYear - 1;
            }
        }
        lblComany.Text = SessionCcode + " - " + SessionLcode;
        lblusername.Text = Session["Usernmdisplay"].ToString();

    }
    public void DropDwonCategory()
    {
        DataTable dtcate = new DataTable();
        dtcate = objdata.DropDownCategory();
        ddlcategory.DataSource = dtcate;
        ddlcategory.DataTextField = "CategoryName";
        ddlcategory.DataValueField = "CategoryCd";
        ddlcategory.DataBind();
    }
    protected void rbtmonthyera_SelectedIndexChanged(object sender, EventArgs e)
    {

        if (ddlemptype.SelectedValue == "1")
        {
            if (ddlrpttype.SelectedValue == "1")
            {
                PanelSingleEmployee.Visible = true;


            }
            else if (ddlrpttype.SelectedValue == "2")
            {
                PanelSingleEmployee.Visible = true;

            }
        }
        else if (ddlemptype.SelectedValue == "2")
        {
            if (ddlrpttype.SelectedValue == "1")
            {
                PanelSingleEmployee.Visible = false;

            }
            else if (ddlrpttype.SelectedValue == "2")
            {
                PanelSingleEmployee.Visible = false;

            }
        }
    }
    protected void rbtnrpttype_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlemptype.SelectedValue == "1")
        {
            if (ddlrpttype.SelectedValue == "1")
            {
                PanelSingleEmployee.Visible = true;

                GvAttendanceRpt.DataSource = null;
                GvAttendanceRpt.DataBind();
            }
            else if (ddlrpttype.SelectedValue == "2")
            {
                PanelSingleEmployee.Visible = true;

                GvAttendanceRpt.DataSource = null;
                GvAttendanceRpt.DataBind();

            }
            GvAttendanceRpt.DataSource = null;
            GvAttendanceRpt.DataBind();
        }
        else if (ddlemptype.SelectedValue == "2")
        {
            PanelSingleEmployee.Visible = false;
            GvAttendanceRpt.DataSource = null;
            GvAttendanceRpt.DataBind();
        }
    }
    public void Department()
    {
        DataTable dtDip = new DataTable();
        dtDip = objdata.DropDownDepartment();
        ddldepartment.DataSource = dtDip;
        ddldepartment.DataTextField = "DepartmentNm";
        ddldepartment.DataValueField = "DepartmentCd";
        ddldepartment.DataBind();
    }

    protected void ddldepartment_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable dtempty = new DataTable();
        ddlempno.DataSource = dtempty;
        ddlempno.DataBind();
        ddlempname.DataSource = dtempty;
        ddlempname.DataBind();
        GvAttendanceRpt.DataSource = dtempty;
        GvAttendanceRpt.DataBind();
        txtfromdate.Text = "";
        txttodate.Text = "";
    }
    protected void ddlempno_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable dtemp = new DataTable();
        dtemp = objdata.DropDownEmployeeName(ddlempno.SelectedValue, SessionCcode, SessionLcode);
        if (dtemp.Rows.Count > 0)
        {
            ddlempname.DataSource = dtemp;
            ddlempname.DataTextField = "EmpName";
            ddlempname.DataValueField = "EmpNo";
            ddlempname.DataBind();
        }
    }
    protected void ddlempname_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable dtemp = new DataTable();
        dtemp = objdata.DropDownEmployeeNumber(ddlempname.SelectedItem.Text, SessionCcode, SessionLcode);
        if (dtemp.Rows.Count > 0)
        {
            ddlempno.DataSource = dtemp;
            ddlempno.DataTextField = "EmpNo";
            ddlempno.DataValueField = "EmpNo";
            ddlempno.DataBind();
        }
    }
    protected void btnreport_Click(object sender, EventArgs e)
    {
        try
        {
            bool ErrFlag = false;
            if (ddlemptype.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select Report Type');", true);
                //System.Windows.Forms.MessageBox.Show("Select Report Type", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            else if (ddlemptype.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select Report Type');", true);
                //System.Windows.Forms.MessageBox.Show("Select Report Type", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            else if (ddlrpttype.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select Month or Year');", true);
                //System.Windows.Forms.MessageBox.Show("Select Month or Year", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            else if (ddlrpttype.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select Month or Year');", true);
                //System.Windows.Forms.MessageBox.Show("Select Month or Year", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            else if (ddlcategory.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select Employee Category');", true);
                //System.Windows.Forms.MessageBox.Show("Select Employee Category", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            else if (ddlcategory.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select Employee Category');", true);
                //System.Windows.Forms.MessageBox.Show("Select Employee Category", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            if (ddlrpttype.SelectedValue == "1")
            {
                if (txtfromdate.Text.Trim() == null)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the From Date');", true);
                    //System.Windows.Forms.MessageBox.Show("Enter the From Date", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    ErrFlag = true;
                }
                else if (txttodate.Text.Trim() == null)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the To Date');", true);
                    //System.Windows.Forms.MessageBox.Show("Enter the To Date", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    ErrFlag = true;
                }
            }
            if (!ErrFlag)
            {
                string EmpNo = ddlempno.SelectedValue;
                if (ddlrpttype.SelectedValue == "1")
                {
                    FromDate = DateTime.ParseExact(txtfromdate.Text, "dd-MM-yyyy", null);
                    ToDate = DateTime.ParseExact(txttodate.Text, "dd-MM-yyyy", null);
                }
                string FinaYear = ddlFinaYear.SelectedValue;
                string Dept = ddldepartment.SelectedValue;
                if (ddlemptype.SelectedValue == "1")
                {
                    if (ddlcategory.SelectedValue == "1")
                    {
                        if (ddlrpttype.SelectedValue == "1")
                        {
                            DataTable dtsinglmonth = new DataTable();
                            EmpNo = ddlempno.SelectedValue;

                            FinaYear = ddlFinaYear.SelectedValue;
                            Dept = ddldepartment.SelectedValue;
                            if (SessionAdmin == "1")
                            {
                                dtsinglmonth = objdata.RptAttendanceSingleEmpForMonth(EmpNo, FromDate, ToDate, FinaYear, Dept, SessionCcode, SessionLcode);
                            }
                            else
                            {
                                dtsinglmonth = objdata.RptAttendanceSingleEmpForMonth_user(EmpNo, FromDate, ToDate, FinaYear, Dept, SessionCcode, SessionLcode);
                            }
                            GvAttendanceRpt.DataSource = dtsinglmonth;
                            GvAttendanceRpt.DataBind();
                        }
                        else if (ddlrpttype.SelectedValue == "2")
                        {
                            EmpNo = ddlempno.SelectedValue;
                            FinaYear = ddlFinaYear.SelectedValue;
                            Dept = ddldepartment.SelectedValue;
                            DataTable dtsinglYear = new DataTable();
                            if (SessionAdmin == "1")
                            {
                                dtsinglYear = objdata.RptAttendanceSingleEmpForYear(EmpNo, FinaYear, Dept, SessionCcode, SessionLcode);
                            }
                            else
                            {
                                dtsinglYear = objdata.RptAttendanceSingleEmpForYear_user(EmpNo, FinaYear, Dept, SessionCcode, SessionLcode);
                            }
                            GvAttendanceRpt.DataSource = dtsinglYear;
                            GvAttendanceRpt.DataBind();
                        }


                    }
                    else if (ddlcategory.SelectedValue == "2")
                    {
                        if (ddlrpttype.SelectedValue == "1")
                        {
                            DataTable dtsinglmonth = new DataTable();
                            string Cate = "L";
                            EmpNo = ddlempno.SelectedValue;

                            FinaYear = ddlFinaYear.SelectedValue;
                            Dept = ddldepartment.SelectedValue;
                            if (SessionAdmin == "1")
                            {
                                dtsinglmonth = objdata.RptAttendanceSingleLabourForMonth(EmpNo, FromDate, ToDate, FinaYear, Dept, Cate, SessionCcode, SessionLcode);
                            }
                            else
                            {
                                dtsinglmonth = objdata.RptAttendanceSingleLabourForMonth_user(EmpNo, FromDate, ToDate, FinaYear, Dept, Cate, SessionCcode, SessionLcode);
                            }
                            GvAttendanceRpt.DataSource = dtsinglmonth;
                            GvAttendanceRpt.DataBind();
                        }
                        else if (ddlrpttype.SelectedValue == "2")
                        {
                            EmpNo = ddlempno.SelectedValue;
                            FinaYear = ddlFinaYear.SelectedValue;
                            Dept = ddldepartment.SelectedValue;
                            string Cate = "L";
                            DataTable dtsinglYear = new DataTable();
                            if (SessionAdmin == "1")
                            {
                                dtsinglYear = objdata.RptAttendanceSingleLabourForYear(EmpNo, FinaYear, Dept, Cate, SessionCcode, SessionLcode);
                            }
                            else
                            {
                                dtsinglYear = objdata.RptAttendanceSingleLabourForYear_user(EmpNo, FinaYear, Dept, Cate, SessionCcode, SessionLcode);
                            }
                            GvAttendanceRpt.DataSource = dtsinglYear;
                            GvAttendanceRpt.DataBind();
                        }
                    }
                }
                else if (ddlemptype.SelectedValue == "2")
                {
                    if (ddlcategory.SelectedValue == "1")
                    {
                        if (ddlrpttype.SelectedValue == "1")
                        {
                            DataTable dtAllMonth = new DataTable();


                            FinaYear = ddlFinaYear.SelectedValue;
                            Dept = ddldepartment.SelectedValue;
                            if (SessionAdmin == "1")
                            {
                                dtAllMonth = objdata.RptAttendanceAllEmpForMonth(FromDate, ToDate, FinaYear, Dept, SessionCcode, SessionLcode);
                            }
                            else
                            {
                                dtAllMonth = objdata.RptAttendanceAllEmpForMonth_user(FromDate, ToDate, FinaYear, Dept, SessionCcode, SessionLcode);
                            }
                            GvAttendanceRpt.DataSource = dtAllMonth;
                            GvAttendanceRpt.DataBind();
                        }
                        else if (ddlrpttype.SelectedValue == "2")
                        {
                            DataTable dtAllYear = new DataTable();

                            FinaYear = ddlFinaYear.SelectedValue;
                            Dept = ddldepartment.SelectedValue;
                            if (SessionAdmin == "1")
                            {
                                dtAllYear = objdata.RptAttendanceAllEmpForYear(FinaYear, Dept, SessionCcode, SessionLcode);
                            }
                            else
                            {
                                dtAllYear = objdata.RptAttendanceAllEmpForYear_user(FinaYear, Dept, SessionCcode, SessionLcode);
                            }
                            GvAttendanceRpt.DataSource = dtAllYear;
                            GvAttendanceRpt.DataBind();
                        }
                    }
                    else if (ddlcategory.SelectedValue == "2")
                    {
                        if (ddlrpttype.SelectedValue == "1")
                        {
                            DataTable dtsinglmonth = new DataTable();
                            string Cate = "L";

                            FinaYear = ddlFinaYear.SelectedValue;
                            Dept = ddldepartment.SelectedValue;
                            if (SessionAdmin == "1")
                            {
                                dtsinglmonth = objdata.RptAttendanceAllLabourForMonth(FromDate, ToDate, FinaYear, Dept, Cate, SessionCcode, SessionLcode);
                            }
                            else
                            {
                                dtsinglmonth = objdata.RptAttendanceAllLabourForMonth_user(FromDate, ToDate, FinaYear, Dept, Cate, SessionCcode, SessionLcode);
                            }
                            GvAttendanceRpt.DataSource = dtsinglmonth;
                            GvAttendanceRpt.DataBind();
                        }
                        else if (ddlrpttype.SelectedValue == "2")
                        {
                            EmpNo = ddlempno.SelectedValue;
                            FinaYear = ddlFinaYear.SelectedValue;
                            Dept = ddldepartment.SelectedValue;
                            string Cate = "L";
                            DataTable dtsinglYear = new DataTable();
                            if (SessionAdmin == "1")
                            {
                                dtsinglYear = objdata.RptAttendanceAllLabourForYear(FinaYear, Dept, Cate, SessionCcode, SessionLcode);
                            }
                            else
                            {
                                dtsinglYear = objdata.RptAttendanceAllLabourForYear_user(FinaYear, Dept, Cate, SessionCcode, SessionLcode);
                            }
                            GvAttendanceRpt.DataSource = dtsinglYear;
                            GvAttendanceRpt.DataBind();
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Date time is not properly Recognised');", true);
            //System.Windows.Forms.MessageBox.Show("Date time is not properly Recognised", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
        }

    }
    protected void btnexport_Click(object sender, EventArgs e)
    {
        if (ddlexport.SelectedValue == "1")
        {
            //GridViewExportUtil.Export("Reportsearch.pdf", gvreportserarch);
            string attachment = "attachment; filename=AttendanceReport.xls";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/ms-excel";
            StringWriter stw = new StringWriter();
            HtmlTextWriter htextw = new HtmlTextWriter(stw);
            GvAttendanceRpt.RenderControl(htextw);
            Response.Write(stw.ToString());
            Response.End();
        }
        else if (ddlexport.SelectedValue == "2")
        {
            string attachment = "attachment; filename=AttendanceReport.pdf";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/pdf";
            StringWriter stw = new StringWriter();
            HtmlTextWriter htextw = new HtmlTextWriter(stw);
            GvAttendanceRpt.RenderControl(htextw);
            Document document = new Document(PageSize.A4, 10f, 10f, 10f, 10f);


            PdfWriter.GetInstance(document, Response.OutputStream);
            document.Open();
            StringReader str = new StringReader(stw.ToString());
            HTMLWorker htmlworker = new HTMLWorker(document);
            htmlworker.Parse(str);
            document.Close();
            Response.Write(document);
            Response.End();


        }
        else if (ddlexport.SelectedValue == "3")
        {
            string attachment = "attachment; filename=AttendanceReport.doc";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/ms-excel";
            StringWriter stw = new StringWriter();
            HtmlTextWriter htextw = new HtmlTextWriter(stw);
            GvAttendanceRpt.RenderControl(htextw);
            Response.Write(stw.ToString());
            Response.End();
        }
    }
    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }
    protected void ddlcategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable dtempty = new DataTable();
        ddldepartment.DataSource = dtempty;
        ddldepartment.DataBind();
        ddlempno.DataSource = dtempty;
        ddlempno.DataBind();
        ddlempname.DataSource = dtempty;
        ddlempname.DataBind();
        GvAttendanceRpt.DataSource = dtempty;
        GvAttendanceRpt.DataBind();
        txtfromdate.Text = "";
        txttodate.Text = "";
        if (ddlcategory.SelectedValue == "1")
        {
            Stafflabour = "S";
        }
        else if (ddlcategory.SelectedValue == "2")
        {
            Stafflabour = "L";
        }
        else
        {
            Stafflabour = "0";
        }
        DataTable dtDip = new DataTable();
        dtDip = objdata.DropDownDepartment_category(Stafflabour);
        if (dtDip.Rows.Count > 1)
        {
            ddldepartment.DataSource = dtDip;
            ddldepartment.DataTextField = "DepartmentNm";
            ddldepartment.DataValueField = "DepartmentCd";
            ddldepartment.DataBind();
        }
        else
        {
            DataTable dt = new DataTable();
            ddldepartment.DataSource = dtempty;
            ddldepartment.DataBind();
        }
    }
    protected void btnClick_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        string Staff = "";
        DataTable dtempty = new DataTable();
        ddlempno.DataSource = dtempty;
        ddlempno.DataBind();
        ddlempname.DataSource = dtempty;
        ddlempname.DataBind();
        GvAttendanceRpt.DataSource = dtempty;
        GvAttendanceRpt.DataBind();
        txtfromdate.Text = "";
        txttodate.Text = "";
        if (ddlcategory.SelectedValue == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Employee Number');", true);
            //System.Windows.Forms.MessageBox.Show("Enter Employee Number", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            ErrFlag = true;
        }
        if (!ErrFlag)
        {
            if (ddlcategory.SelectedValue == "1")
            {
                Staff = "S";
            }
            else if (ddlcategory.SelectedValue == "2")
            {
                Staff = "L";
            }
            DataTable dtemp = new DataTable();
            string dd = ddldepartment.SelectedValue;
            if (SessionAdmin == "1")
            {
                dtemp = objdata.EmployeeNoandName(Staff, dd, SessionCcode, SessionLcode);
            }
            else
            {
                dtemp = objdata.EmployeeNoandName_user(Staff, dd, SessionCcode, SessionLcode);
            }
            if (dtemp.Rows.Count > 0)
            {
                ddlempno.DataSource = dtemp;
                DataRow dr = dtemp.NewRow();
                dr["EmpNo"] = ddldepartment.SelectedItem.Text;
                dr["EmpName"] = ddldepartment.SelectedItem.Text;
                dtemp.Rows.InsertAt(dr, 0);
                ddlempno.DataTextField = "EmpNo";
                ddlempno.DataValueField = "EmpNo";

                ddlempno.DataBind();

                ddlempname.DataSource = dtemp;
                ddlempname.DataTextField = "EmpName";
                ddlempname.DataValueField = "EmpNo";
                ddlempname.DataBind();
            }
        }
    }
    protected void ddlemptype_SelectedIndexChanged(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        DataTable dtempty = new DataTable();
        GvAttendanceRpt.DataSource = dtempty;
        GvAttendanceRpt.DataBind();
        ddlrpttype.SelectedValue = "0";
        txtfromdate.Text = "";
        txttodate.Text = "";
        if (ddlcategory.SelectedValue == "0")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Category');", true);
            //System.Windows.Forms.MessageBox.Show("Select the Category", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            ErrFlag = true;
        }
        else if (ddldepartment.SelectedValue == "0")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Department');", true);
            //System.Windows.Forms.MessageBox.Show("Select the Department", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            ErrFlag = true;
        }
        else if (ddlemptype.SelectedValue == "0")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Employee Type');", true);
            //System.Windows.Forms.MessageBox.Show("Select the Employee Type", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            ErrFlag = true;
        }
        if (!ErrFlag)
        {

            if (ddlemptype.SelectedValue == "1")
            {
                PanelSingleEmployee.Visible = false;
                PanelFromMonth.Visible = false;

            }
            else if (ddlemptype.SelectedValue == "2")
            {
                PanelSingleEmployee.Visible = false;
                PanelFromMonth.Visible = false;
            }
        }
    }
    protected void ddlrpttype_SelectedIndexChanged(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        DataTable dtempty = new DataTable();
        GvAttendanceRpt.DataSource = dtempty;
        GvAttendanceRpt.DataBind();
        //ddlrpttype.SelectedValue = "0";
        txtfromdate.Text = "";
        txttodate.Text = "";
        if (ddlcategory.SelectedValue == "0")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Category');", true);
            //System.Windows.Forms.MessageBox.Show("Select the Category", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            ErrFlag = true;
        }
        else if (ddldepartment.SelectedValue == "0")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Department');", true);
            //System.Windows.Forms.MessageBox.Show("Select the Department", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            ErrFlag = true;
        }
        else if (ddlemptype.SelectedValue == "0")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Employee Type');", true);
            //System.Windows.Forms.MessageBox.Show("Select the Employee Type", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            ErrFlag = true;
        }
        if (!ErrFlag)
        {
            if (ddlrpttype.SelectedValue == "1")
            {
                if (ddlemptype.SelectedValue == "1")
                {
                    PanelSingleEmployee.Visible = true;
                    PanelFromMonth.Visible = true;
                }
                else if (ddlemptype.SelectedValue == "2")
                {
                    PanelSingleEmployee.Visible = false;
                    PanelFromMonth.Visible = true;
                }
            }
            else if (ddlrpttype.SelectedValue == "2")
            {
                if (ddlemptype.SelectedValue == "1")
                {
                    PanelSingleEmployee.Visible = true;
                    PanelFromMonth.Visible = false;
                }
                else if (ddlemptype.SelectedValue == "2")
                {
                    PanelSingleEmployee.Visible = false;
                    PanelFromMonth.Visible = false;
                }
            }
        }

    }
    protected void btnEmp_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptEmpDownload.aspx");
    }
    protected void btnatt_Click(object sender, EventArgs e)
    {
        Response.Redirect("AttenanceDownload.aspx");
    }
    protected void btnLeave_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptLeaveSample.aspx");
    }
    protected void btnOT_Click(object sender, EventArgs e)
    {
        Response.Redirect("OTDownload.aspx");
    }
    protected void btnSal_Click(object sender, EventArgs e)
    {
        Response.Redirect("FrmDeductionDownload.aspx");
    }

}
