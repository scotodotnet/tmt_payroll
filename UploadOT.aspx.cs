﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Configuration;
using System.Data.SqlClient;
using System.Data.OleDb;
using Payroll;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class UploadOT : System.Web.UI.Page
{
    string SessionAdmin;
    string DepartmentCode;
    string Name_Upload;
    string Name_Upload1;
    BALDataAccess objdata = new BALDataAccess();
    Overtimeclass objot = new Overtimeclass();
    string SessionCcode;
    string SessionLcode;
    DateTime dfrom;
    DateTime dtto;
    DateTime MyDate1;
    DateTime MyDate2;
    //string DepartmentCode;
    protected void Page_Load(object sender, EventArgs e)
    {
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        if (SessionAdmin == "2")
        {
            Response.Redirect("EmployeeRegistration.aspx");

        }
        lblComany.Text = SessionCcode + " - " + SessionLcode;
        SessionAdmin = Session["Isadmin"].ToString();
        if (!IsPostBack)
        {
            Months_load();
            int currentYear = Utility.GetFinancialYear;
            for (int i = 0; i < 10; i++)
            {
                ddlfinance.Items.Add(new ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                //  ddlShowYear.Items.Add(new ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                currentYear = currentYear - 1;
            }
        }
        lblusername.Text = Session["Usernmdisplay"].ToString();
    }
    public void Months_load()
    {
        DataTable dt = new DataTable();
        dt = objdata.months_load();
        ddlMonths.DataSource = dt;
        ddlMonths.DataTextField = "Months";
        ddlMonths.DataValueField = "Months";
        ddlMonths.DataBind();
    }
    protected void btnUpload_Click(object sender, EventArgs e)
    {
        try
        {
            bool ErrFlag = false;
            if (txtfrom.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the From Date');", true);
                ErrFlag = true;
            }
            else if (txtTo.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the To Date');", true);
                ErrFlag = true;
            }
            if (!ErrFlag)
            {
                dfrom = Convert.ToDateTime(txtfrom.Text);
                dtto = Convert.ToDateTime(txtTo.Text);
                MyDate1 = DateTime.ParseExact(txtfrom.Text, "dd-MM-yyyy", null);
                MyDate2 = DateTime.ParseExact(txtTo.Text, "dd-MM-yyyy", null);
                if (dfrom > dtto)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the From Date...');", true);
                    txtfrom.Text = null;
                    txtTo.Text = null;
                    ErrFlag = true;
                }
            }
            
            if (!ErrFlag)
            {
                if (FileUpload.HasFile)
            {
                FileUpload.SaveAs(Server.MapPath("Upload/" + FileUpload.FileName));

            }
                string connectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + Server.MapPath("Upload/" + FileUpload.FileName) + ";" + "Extended Properties=Excel 8.0;";
                    OleDbConnection sSourceConnection = new OleDbConnection(connectionString);
                    DataTable dts = new DataTable();
                    using (sSourceConnection)
                    {
                        sSourceConnection.Open();

                        OleDbCommand command = new OleDbCommand("Select * FROM [Sheet1$];", sSourceConnection);
                        sSourceConnection.Close();

                        using (OleDbCommand cmd = sSourceConnection.CreateCommand())
                        {
                            command.CommandText = "Select * FROM [Sheet1$];";
                            sSourceConnection.Open();
                        }
                        using (OleDbDataReader dr = command.ExecuteReader())
                        {

                            if (dr.HasRows)
                            {

                            }

                        }
                        OleDbDataAdapter objDataAdapter = new OleDbDataAdapter();
                        objDataAdapter.SelectCommand = command;
                        DataSet ds = new DataSet();

                        objDataAdapter.Fill(ds);
                        DataTable dt = new DataTable();
                        dt = ds.Tables[0];
                        for (int i = 0; i < dt.Columns.Count; i++)
                        {
                            dt.Columns[i].ColumnName = dt.Columns[i].ColumnName.Replace(" ", string.Empty).ToString();

                        }
                        string constr = ConfigurationManager.AppSettings["ConnectionString"];
                        for (int j = 0; j < dt.Rows.Count; j++)
                        {
                            SqlConnection cn = new SqlConnection(constr);
                            string Department = dt.Rows[j][0].ToString();
                            string MachineID = dt.Rows[j][1].ToString();
                            string EmpNo = dt.Rows[j][2].ToString();
                            string ExistingCode = dt.Rows[j][3].ToString();
                            string EmpName = dt.Rows[j][4].ToString();
                            string HRsalary = dt.Rows[0][5].ToString();
                            string NoHrs = dt.Rows[0][6].ToString();
                            //string netAmount = dt.Rows[0][5].ToString();
                            if (Department == "")
                            {
                                j = j + 2;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Department. The Row Number is " + j + "');", true);
                                //System.Windows.Forms.MessageBox.Show("Enter the Department. The Row number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                ErrFlag = true;
                            }
                            else if (MachineID == "")
                            {
                                j = j + 2;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Machine ID. The Row Number is " + j + "');", true);
                                //System.Windows.Forms.MessageBox.Show("Enter the Machine ID. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                ErrFlag = true;
                            }
                            else if (EmpNo == "")
                            {
                                j = j + 2;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Employee Number. The Row Number is " + j + "');", true);
                                //System.Windows.Forms.MessageBox.Show("Enter the Employee Number. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                ErrFlag = true;
                            }
                            else if (ExistingCode == "")
                            {
                                j = j + 2;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Existing Code. The Row Number is " + j + "');", true);
                                //System.Windows.Forms.MessageBox.Show("Enter the Existing Code. The Roe Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                ErrFlag = true;
                            }
                            else if (EmpName == "")
                            {
                                j = j + 2;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Name. The Row Number is " + j + "');", true);
                                //System.Windows.Forms.MessageBox.Show("Enter the Name. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                ErrFlag = true;
                            }
                            else if (HRsalary == "")
                            {
                                j = j + 2;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Hr Salary. The Row Number is " + j + "');", true);
                                //System.Windows.Forms.MessageBox.Show("Enter the Employee Number. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                ErrFlag = true;
                            }
                            else if (NoHrs == "")
                            {
                                j = j + 2;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the No of Hrs. The Row Number is " + j + "');", true);
                                //System.Windows.Forms.MessageBox.Show("Enter the Employee Number. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                ErrFlag = true;
                            }
                            //else if (netAmount == "")
                            //{
                            //    j = j + 2;
                            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the No of Net Amount. The Row Number is " + j + "');", true);
                            //    //System.Windows.Forms.MessageBox.Show("Enter the Employee Number. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                            //    ErrFlag = true;
                            //}
                            cn.Open();
                            string qry_dpt = "Select DepartmentNm from MstDepartment where DepartmentNm = '" + Department + "'";
                            SqlCommand cmd_dpt = new SqlCommand(qry_dpt, cn);
                            SqlDataReader sdr_dpt = cmd_dpt.ExecuteReader();
                            if (sdr_dpt.HasRows)
                            {
                            }
                            else
                            {
                                j = j + 2;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Department name not found in the department Details. The Row Number is " + j + "');", true);
                                //System.Windows.Forms.MessageBox.Show("Department Name not Found in Department Details. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                ErrFlag = true;
                            }
                            cn.Close();
                            cn.Open();
                            string qry_dpt1 = "Select ED.Department from EmployeeDetails ED inner Join MstDepartment MSt on Mst.DepartmentCd=ED.Department where DepartmentNm = '" + Department + "' and ED.EmpNo='" + EmpNo + "'";
                            SqlCommand cmd_dpt1 = new SqlCommand(qry_dpt1, cn);
                            SqlDataReader sdr_dpt1 = cmd_dpt1.ExecuteReader();
                            if (sdr_dpt1.HasRows)
                            {
                            }
                            else
                            {
                                j = j + 2;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Department name not found in the department Details. The Row Number is " + j + "');", true);
                                //System.Windows.Forms.MessageBox.Show("Department Name not Found in Department Details. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                ErrFlag = true;
                            }
                            cn.Close();
                            //cn.Open();
                            //string Qry_SalaryType = "Select EmpNo from Officialprofile where WagesType ='" + rbsalary.SelectedValue + "'";
                            //SqlCommand cmd_wages = new SqlCommand(Qry_SalaryType, cn);
                            //SqlDataReader sdr_wages = cmd_wages.ExecuteReader();
                            //if (sdr_wages.HasRows)
                            //{
                            //}
                            //else
                            //{
                            //    j = j + 2;
                            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('This " + EmpNo + " is Wages Type is incorrect.');", true);
                            //    //System.Windows.Forms.MessageBox.Show("Department Name not Found in Department Details. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                            //    ErrFlag = true;
                            //}
                            //cn.Close();
                            cn.Open();
                            string qry_empNo = "Select EmpNo from EmployeeDetails where EmpNo= '" + EmpNo + "' and MachineNo = '" + MachineID + "' and ExisistingCode = '" + ExistingCode + "' and EmpName = '" + EmpName + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                            SqlCommand cmd_Emp = new SqlCommand(qry_empNo, cn);
                            SqlDataReader sdr_Emp = cmd_Emp.ExecuteReader();
                            if (sdr_Emp.HasRows)
                            {
                            }
                            else
                            {
                                j = j + 2;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Employee Details Properly. The Row Number is " + j + "');", true);
                                //System.Windows.Forms.MessageBox.Show("Enter the Employee Details Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                ErrFlag = true;
                            }
                            cn.Close();
                            cn.Open();
                            string qry_OT = "Select Eligibleforovertime from Officialprofile  where EmpNo= '" + EmpNo + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and Eligibleforovertime = '1'";
                            SqlCommand cmd_OT = new SqlCommand(qry_OT, cn);
                            SqlDataReader sdr_OT = cmd_OT.ExecuteReader();
                            if (sdr_OT.HasRows)
                            {
                            }
                            else
                            {
                                j = j + 2;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Department name not found in the department Details. The Row Number is " + j + "');", true);
                                //System.Windows.Forms.MessageBox.Show("Department Name not Found in Department Details. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                ErrFlag = true;
                            }
                            cn.Close();
                        }
                        if (!ErrFlag)
                        {
                            for (int i = 0; i < dt.Rows.Count; i++)
                            {
                                SqlConnection cn = new SqlConnection(constr);
                                cn.Open();                                
                                string qry_dpt1 = "Select DepartmentCd from MstDepartment where DepartmentNm = '" + dt.Rows[i][0].ToString() + "'";
                                SqlCommand cmd_dpt1 = new SqlCommand(qry_dpt1, cn);
                                DepartmentCode = Convert.ToString(cmd_dpt1.ExecuteScalar());
                                //string qry_emp = "Select EmpNo from SalaryDetails where EmpNo='" + dt.Rows[i][2].ToString() + "' and " +
                                //                 "Month='" + ddlMonths.Text + "' and FinancialYear='" + ddlfinance.SelectedValue + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'" +
                                //                 " and Process_Mode='1' and convert(datetime,TODate,105) >= convert(datetime,'" + txtfrom.Text + "',105)";
                                ////string qry_emp = "Select EmpNo from AttenanceDetails where EmpNo = '" + dt.Rows[i][2].ToString() + "' and Months = '" + ddlMonths.Text + "' and FinancialYear = '" + ddlfinance.SelectedValue + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                                //SqlCommand cmd_verify = new SqlCommand(qry_emp, cn);
                                //Name_Upload = Convert.ToString(cmd_verify.ExecuteScalar());
                                //string qry_emp1 = "Select EmpNo from OverTime where EmpNo = '" + dt.Rows[i][2].ToString() + "' and Months = '" + ddlMonths.Text + "' and FinancialYear = '" + ddlfinance.SelectedValue + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and convert(datetime,TODate,105) >= convert(datetime,'" + txtfrom.Text + "',105)";
                                //SqlCommand cmd_verify1 = new SqlCommand(qry_emp1, cn);
                                //Name_Upload1 = Convert.ToString(cmd_verify1.ExecuteScalar());
                                objot.Months = ddlMonths.SelectedValue;
                                objot.Finance = ddlfinance.SelectedValue;
                                objot.EmpNo = dt.Rows[i][2].ToString();
                                objot.EmpName = dt.Rows[i][4].ToString();
                                objot.department = DepartmentCode;
                                objot.hrsalary = dt.Rows[i][5].ToString();
                                objot.NHrs = dt.Rows[i][6].ToString();

                                //sabapathy
                                //Get Employee Type
                                //string qry_emptype = "Select EmployeeType from EmployeeDetails Where EmpNo='" + dt.Rows[i][2].ToString() + "' And Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                                //SqlCommand cmd_emptype_verify = new SqlCommand(qry_emptype, cn);
                                //string Employee_Type_Check = "";
                                //Employee_Type_Check = Convert.ToString(cmd_emptype_verify.ExecuteScalar());
                                //if (Employee_Type_Check == "3")
                                //{
                                //    objot.Netamt = (Convert.ToDecimal("10".ToString()) * Convert.ToDecimal(dt.Rows[i][6].ToString())).ToString();
                                //}
                                //else if (Employee_Type_Check == "2")
                                //{
                                //    objot.Netamt = (Convert.ToDecimal("25".ToString()) * Convert.ToDecimal(dt.Rows[i][6].ToString())).ToString();
                                //}
                                //else
                                //{
                                //    objot.Netamt = (Convert.ToDecimal(dt.Rows[i][5].ToString()) * Convert.ToDecimal(dt.Rows[i][6].ToString())).ToString();
                                //}
                                //sabapathy end

                                objot.Netamt = (Convert.ToDecimal(dt.Rows[i][5].ToString()) * Convert.ToDecimal(dt.Rows[i][6].ToString())).ToString();

                                //objot.Netamt = (Convert.ToDecimal(dt.Rows[i][5].ToString()) * Convert.ToDecimal(dt.Rows[i][6].ToString())).ToString();
                                objot.Netamt = Math.Round(Convert.ToDecimal(objot.Netamt), 0, MidpointRounding.AwayFromZero).ToString();
                                objot.chkmanual = "1";
                                cn.Close();
                                string Emp_verify = objdata.Overtime_verify(dt.Rows[i][2].ToString(), DepartmentCode, ddlMonths.SelectedValue, ddlfinance.SelectedValue, SessionCcode, SessionLcode, MyDate1, MyDate2);
                                if (Name_Upload == dt.Rows[i][2].ToString())
                                {
                                }
                                else
                                {
                                    if (Emp_verify == dt.Rows[i][2].ToString())
                                    {
                                        string del = "Delete from OverTime where EmpNo = '" + dt.Rows[i][2].ToString() + "' and Month = '" + ddlMonths.Text + "' and Financialyr = '" + ddlfinance.SelectedValue + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and convert(datetime,FromDate,105) = convert(datetime,'" + txtfrom.Text + "',105) and convert(datetime,ToDate,105) = convert(datetime,'" + txtTo.Text + "',105)";
                                        cn.Open();
                                        SqlCommand cmd_del = new SqlCommand(del, cn);
                                        cmd_del.ExecuteNonQuery();
                                        cn.Close();                
                                        objdata.OverTime_insert(objot, SessionCcode, SessionLcode, MyDate1, MyDate2);
                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Uploaded Successfully...');", true);
                                    }
                                    else
                                    {
                                        objdata.OverTime_insert(objot, SessionCcode, SessionLcode, MyDate1, MyDate2);
                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Uploaded Successfully...');", true);
                                        //sSourceConnection.Close();
                                    }
                                }
                            }
                        }
                    }
            }
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Please Upload Correct File Format...');", true);
        }
    }
    protected void btnEmp_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptEmpDownload.aspx");
    }
    protected void btnatt_Click(object sender, EventArgs e)
    {
        Response.Redirect("AttenanceDownload.aspx");
    }
    protected void btnLeave_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptLeaveSample.aspx");
    }
    protected void btnOT_Click(object sender, EventArgs e)
    {
        Response.Redirect("OTDownload.aspx");
    }
    protected void btnSal_Click(object sender, EventArgs e)
    {
        Response.Redirect("FrmDeductionDownload.aspx");
    }
    protected void txtTo_TextChanged(object sender, EventArgs e)
    {

    }
}
