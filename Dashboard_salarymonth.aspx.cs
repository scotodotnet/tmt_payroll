﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Payroll;
using Payroll.Configuration;
using Payroll.Data;
using System.IO;

using Altius.BusinessAccessLayer.BALDataAccess;

using System.Web.UI.DataVisualization.Charting;
using System.Data;
using System.Data.SqlClient;

public partial class Dashboard_salarymonth : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    bool dd = false;
    string SessionAdmin;
    bool SearchErr = false;
    string Stafflabour;
    string SessionCcode;
    string SessionLcode;
    string Datetime_ser;
    static int month;
    int yr;
    DateTime mydate;
    //string Stafflabour;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionAdmin = Session["Isadmin"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        lblComany.Text = SessionCcode + " - " + SessionLcode;
        string ss = Session["UserId"].ToString();
        if (!IsPostBack)
        {
            //DropDwonCategory();
            Months_load();
            int currentYear = Utility.GetFinancialYear;
            for (int i = 0; i < 10; i++)
            {
                ddlFinance.Items.Add(new System.Web.UI.WebControls.ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                //  ddlShowYear.Items.Add(new ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                currentYear = currentYear - 1;
            }
            SettingServerDate();
            Chart_Call();
            ddlRptType.Items.Add("---Select---");
            ddlRptType.Items.Add("Month");
            ddlRptType.Items.Add("Year");
        }
        
    }
    public void DropDwonCategory()
    {
        //DataTable dtcate = new DataTable();
        //dtcate = objdata.DropDownCategory();
        //ddlcategory.DataSource = dtcate;
        //ddlcategory.DataTextField = "CategoryName";
        //ddlcategory.DataValueField = "CategoryCd";
        //ddlcategory.DataBind();
    }
    public void Months_load()
    {
        DataTable dt = new DataTable();
        dt = objdata.months_load();
        ddlMonths.DataSource = dt;
        ddlMonths.DataTextField = "Months";
        ddlMonths.DataValueField = "Months";
        ddlMonths.DataBind();
    }
    
    protected void ddldept_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void ddlMonths_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            bool ErrFlag = false;
            DataTable dt = new DataTable();
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            if (ddlRptType.SelectedValue == "Month")
            {

                if (ddlMonths.SelectedIndex == 0)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select the Months');", true);
                    ErrFlag = true;
                }
            }
            if (!ErrFlag)
            {
                if (!ErrFlag)
                {
                    Chart1.Legends.Clear();
                    Chart2.Legends.Clear();
                    Chart3.Legends.Clear();
                    string constr = ConfigurationManager.AppSettings["ConnectionString"];
                    SqlConnection con = new SqlConnection(constr);
                    //string qry = "Select SD.EmpNo,SD.NetPay,ED.EmpName from SalaryDetails SD inner Join EmployeeDetails ED on ED.EmpNo=SD.EmpNo where SD.Ccode='" + SessionCcode + "' and SD.Lcode='" + SessionLcode + "' and SD.Month='" + ddlMonths.SelectedValue + "' and SD.FinancialYear='" + ddlFinance.SelectedValue + "' and ED.StafforLabor='" + Stafflabour + "'";
                    string qry = "";
                    if (ddlMonths.Enabled == true)
                    {
                        qry = "Select sum(SD.NetPay) as NetPay,MD.DepartmentNm from " +
                            "SalaryDetails SD,EmployeeDetails ED,MstDepartment MD where " +
                            "SD.EmpNo=ED.EmpNo And ED.Department=DepartmentCd and SD.Month='" + ddlMonths.SelectedValue + "'" +
                            "and SD.FinancialYear='" + ddlFinance.SelectedValue + "' and SD.Ccode='" + SessionCcode + "' " +
                            "and SD.Lcode='" + SessionLcode + "' group by ED.Department, MD.DepartmentNm";
                    }
                    else
                    {
                        qry = "Select sum(SD.NetPay) as NetPay,MD.DepartmentNm from " +
                            "SalaryDetails SD,EmployeeDetails ED,MstDepartment MD where " +
                            "SD.EmpNo=ED.EmpNo And ED.Department=DepartmentCd " +
                            "and SD.FinancialYear='" + ddlFinance.SelectedValue + "' and SD.Ccode='" + SessionCcode + "' " +
                            "and SD.Lcode='" + SessionLcode + "' group by ED.Department, MD.DepartmentNm";
                    }
                    SqlCommand cmd = new SqlCommand(qry, con);
                    da.SelectCommand = cmd;
                    con.Open();
                    da.Fill(ds);
                    con.Close();
                    dt = ds.Tables[0];
                    Chart1.DataSource = dt;
                    Chart1.Series["Series1"].XValueMember = "DepartmentNm";
                    Chart1.Series["Series1"].YValueMembers = "NetPay";

                    this.Chart1.Series[0]["PieLabelStyle"] = "Outside";
                    this.Chart1.Series[0].BorderWidth = 1;
                    this.Chart1.Series[0].BorderColor = System.Drawing.Color.FromArgb(26, 59, 105);
                    this.Chart1.Legends.Add("Legend1");
                    this.Chart1.Legends[0].Enabled = true;
                    this.Chart1.Legends[0].Docking = Docking.Bottom;
                    this.Chart1.Legends[0].Alignment = System.Drawing.StringAlignment.Center;
                    this.Chart1.Series[0].LegendText = "#PERCENT{P0}";
                    this.Chart1.DataManipulator.Sort(PointSortOrder.Descending, Chart2.Series[0]);
                    Chart1.DataBind();

                    //Chart2.DataSource = dt;
                    //Chart2.Series["Series1"].XValueMember = "DepartmentNm";
                    //Chart2.Series["Series1"].YValueMembers = "NetPay";
                    //Chart2.DataBind();
                    //ResponseHelper.Redirect("ViewReport.aspx?Cate=" + Stafflabour + "&Depat=" + ddldept.SelectedValue + "&Months=" + ddlMonths.SelectedValue + "&yr=" + ddlFinance.SelectedValue, "_blank", "");

                    //Get High Value
                    SqlDataReader dr;
                    double high_Val = 0;
                    string qry1 = "";
                    if (ddlMonths.Enabled == true)
                    {
                        qry1 = "Select sum(SD.NetPay) as NetPay,MD.DepartmentNm from " +
                                    "SalaryDetails SD,EmployeeDetails ED,MstDepartment MD where " +
                                    "SD.EmpNo=ED.EmpNo And ED.Department=DepartmentCd and SD.Month='" + ddlMonths.SelectedValue + "'" +
                                    "and SD.FinancialYear='" + ddlFinance.SelectedValue + "' and SD.Ccode='" + SessionCcode + "' " +
                                    "and SD.Lcode='" + SessionLcode + "' group by ED.Department, MD.DepartmentNm order by NetPay Desc";
                    }
                    else
                    {
                        qry1 = "Select sum(SD.NetPay) as NetPay,MD.DepartmentNm from " +
                                    "SalaryDetails SD,EmployeeDetails ED,MstDepartment MD where " +
                                    "SD.EmpNo=ED.EmpNo And ED.Department=DepartmentCd " +
                                    "and SD.FinancialYear='" + ddlFinance.SelectedValue + "' and SD.Ccode='" + SessionCcode + "' " +
                                    "and SD.Lcode='" + SessionLcode + "' group by ED.Department, MD.DepartmentNm order by NetPay Desc";
                    }
                    
                    cmd = new SqlCommand(qry1, con);
                    con.Open();
                    dr = cmd.ExecuteReader();
                    if (dr.HasRows)
                    {
                        dr.Read();
                        high_Val = Convert.ToDouble(dr["NetPay"]);
                    }
                    dr.Close();
                    con.Close();
                    // High Value End


                    //Bar Chart Start
                    if (ddlMonths.Enabled == true)
                    {
                        qry = "Select sum((SD.NetPay/" + high_Val + ")*100) as NetPay,MD.DepartmentNm from " +
                                    "SalaryDetails SD,EmployeeDetails ED,MstDepartment MD where " +
                                    "SD.EmpNo=ED.EmpNo And ED.Department=DepartmentCd and SD.Month='" + ddlMonths.SelectedValue + "'" +
                                    "and SD.FinancialYear='" + ddlFinance.SelectedValue + "' and SD.Ccode='" + SessionCcode + "' " +
                                    "and SD.Lcode='" + SessionLcode + "' group by ED.Department, MD.DepartmentNm";
                    }
                    else
                    {
                        qry = "Select sum((SD.NetPay/" + high_Val + ")*100) as NetPay,MD.DepartmentNm from " +
                                    "SalaryDetails SD,EmployeeDetails ED,MstDepartment MD where " +
                                    "SD.EmpNo=ED.EmpNo And ED.Department=DepartmentCd " +
                                    "and SD.FinancialYear='" + ddlFinance.SelectedValue + "' and SD.Ccode='" + SessionCcode + "' " +
                                    "and SD.Lcode='" + SessionLcode + "' group by ED.Department, MD.DepartmentNm";
                    }
                    
                    SqlCommand cmd1 = new SqlCommand(qry, con);
                    ds.Clear();
                    da.SelectCommand = cmd1;
                    con.Open();
                    da.Fill(ds);
                    con.Close();
                    //dt.Clear();
                    dt = ds.Tables[0];

                    Chart2.DataSource = dt;
                    Chart2.Series["Series1"].XValueMember = "DepartmentNm";
                    Chart2.Series["Series1"].YValueMembers = "NetPay";
                    //Chart2.Series[0].IsValueShownAsLabel = true;

                    Chart2.ChartAreas[0].RecalculateAxesScale();
                    Chart2.ChartAreas[0].AxisY.Minimum = 0;
                    Chart2.ChartAreas[0].AxisY.Maximum = 100;

                    //Chart2.ChartAreas[0].AxisY.LabelStyle.Format = "{#'%'}";
                    //Chart2.Series[0].IsValueShownAsLabel = true;
                    //Chart2.Series[0].Label = "#PERCENT{P}";

                    Chart2.DataBind();

                    //Line Chart Start
                    Chart3.DataSource = dt;
                    Chart3.Series["Series1"].XValueMember = "DepartmentNm";
                    Chart3.Series["Series1"].YValueMembers = "NetPay";
                    //Chart3.Series[0].IsValueShownAsLabel = true;

                    Chart3.ChartAreas[0].RecalculateAxesScale();
                    Chart3.ChartAreas[0].AxisY.Minimum = 0;
                    Chart3.ChartAreas[0].AxisY.Maximum = 100;

                    //Chart3.ChartAreas[0].AxisY.LabelStyle.Format = "{#'%'}";
                    //Chart3.Series[0].IsValueShownAsLabel = true;
                    //Chart3.Series[0].Label = "#PERCENT{P}";

                    Chart3.DataBind();
                    //Line Chart End

                }
            }
        }
        catch (Exception ex)
        {
        }
    }
    public void SettingServerDate()
    {
        Datetime_ser = objdata.ServerDate();
        //txtDate.Text = Datetime_ser;
    }
    public void Chart_Call()
    {

        DataTable dt = new DataTable();
        DataSet ds = new DataSet();
        SqlDataAdapter da = new SqlDataAdapter();
        SqlDataReader dr;
        mydate = Convert.ToDateTime(Datetime_ser).AddMonths(-1);
        month = Convert.ToInt32(mydate.Month.ToString());

        if (month >= 4)
        {
            yr = Convert.ToInt32(mydate.Year.ToString());
        }
        else
        {
            yr = Convert.ToInt32(mydate.Year.ToString());
            yr = yr - 1;
        }
        #region Month
        string Mont = "";
        switch (month)
        {
            case 1:
                Mont = "January";
                break;

            case 2:
                Mont = "February";
                break;
            case 3:
                Mont = "March";
                break;
            case 4:
                Mont = "April";
                break;
            case 5:
                Mont = "May";
                break;
            case 6:
                Mont = "June";
                break;
            case 7:
                Mont = "July";
                break;
            case 8:
                Mont = "August";
                break;
            case 9:
                Mont = "September";
                break;
            case 10:
                Mont = "October";
                break;
            case 11:
                Mont = "November";
                break;
            case 12:
                Mont = "December";
                break;
            default:
                break;
        }
        #endregion
        string constr = ConfigurationManager.AppSettings["ConnectionString"];
        SqlConnection con = new SqlConnection(constr);
        string qry = "Select sum(SD.NetPay) as NetPay,MD.DepartmentNm from " +
                        "SalaryDetails SD,EmployeeDetails ED,MstDepartment MD where " +
                        "SD.EmpNo=ED.EmpNo And ED.Department=DepartmentCd and SD.Month='" + Mont + "'" +
                        "and SD.FinancialYear='" + yr + "' and SD.Ccode='" + SessionCcode + "' " +
                        "and SD.Lcode='" + SessionLcode + "' group by ED.Department, MD.DepartmentNm";
        SqlCommand cmd = new SqlCommand(qry, con);
        da.SelectCommand = cmd;
        con.Open();
        da.Fill(ds);
        con.Close();
        dt = ds.Tables[0];
        Chart1.Legends.Clear();
        Chart2.Legends.Clear();
        Chart3.Legends.Clear();

        Chart1.DataSource = dt;
        Chart1.Series["Series1"].XValueMember = "DepartmentNm";
        Chart1.Series["Series1"].YValueMembers = "NetPay";

        this.Chart1.Series[0]["PieLabelStyle"] = "Outside";
        this.Chart1.Series[0].BorderWidth = 2;
        this.Chart1.Series[0].BorderColor = System.Drawing.Color.FromArgb(26, 59, 105);
        this.Chart1.Legends.Add("Legend1");
        this.Chart1.Legends[0].Enabled = true;
        this.Chart1.Legends[0].Docking = Docking.Bottom;
        this.Chart1.Legends[0].Alignment = System.Drawing.StringAlignment.Center;
        this.Chart1.Series[0].LegendText = "#PERCENT{P0}";
        this.Chart1.DataManipulator.Sort(PointSortOrder.Descending, Chart1.Series[0]);
        Chart1.DataBind();


        //Get High Value
        double high_Val = 0;
        string qry1 = "Select sum(SD.NetPay) as NetPay,MD.DepartmentNm from " +
                        "SalaryDetails SD,EmployeeDetails ED,MstDepartment MD where " +
                        "SD.EmpNo=ED.EmpNo And ED.Department=DepartmentCd and SD.Month='" + Mont + "'" +
                        "and SD.FinancialYear='" + yr + "' and SD.Ccode='" + SessionCcode + "' " +
                        "and SD.Lcode='" + SessionLcode + "' group by ED.Department, MD.DepartmentNm order by NetPay Desc";
        cmd = new SqlCommand(qry1, con);
        con.Open();
        dr = cmd.ExecuteReader();
        if (dr.HasRows)
        {
            dr.Read();
            high_Val = Convert.ToDouble(dr["NetPay"]);
        }
        dr.Close();
        con.Close();
        // High Value End


        //Bar Chart Start
        qry = "Select sum((SD.NetPay/" + high_Val + ")*100) as NetPay,MD.DepartmentNm from " +
                        "SalaryDetails SD,EmployeeDetails ED,MstDepartment MD where " +
                        "SD.EmpNo=ED.EmpNo And ED.Department=DepartmentCd and SD.Month='" + Mont + "'" +
                        "and SD.FinancialYear='" + yr + "' and SD.Ccode='" + SessionCcode + "' " +
                        "and SD.Lcode='" + SessionLcode + "' group by ED.Department, MD.DepartmentNm";
        SqlCommand cmd1 = new SqlCommand(qry, con);
        ds.Clear();
        da.SelectCommand = cmd1;
        con.Open();
        da.Fill(ds);
        con.Close();
        //dt.Clear();
        dt = ds.Tables[0];

        Chart2.DataSource = dt;
        Chart2.Series["Series1"].XValueMember = "DepartmentNm";
        Chart2.Series["Series1"].YValueMembers = "NetPay";
        //Chart2.Series[0].IsValueShownAsLabel = true;

        Chart2.ChartAreas[0].RecalculateAxesScale();
        Chart2.ChartAreas[0].AxisY.Minimum = 0;
        Chart2.ChartAreas[0].AxisY.Maximum = 100;

        //Chart2.ChartAreas[0].AxisY.LabelStyle.Format = "{#'%'}";
        //Chart2.Series[0].IsValueShownAsLabel = true;
        //Chart2.Series[0].Label = "#PERCENT{P}";
        
        Chart2.DataBind();

        //Line Chart Start
        Chart3.DataSource = dt;
        Chart3.Series["Series1"].XValueMember = "DepartmentNm";
        Chart3.Series["Series1"].YValueMembers = "NetPay";
        //Chart3.Series[0].IsValueShownAsLabel = true;

        Chart3.ChartAreas[0].RecalculateAxesScale();
        Chart3.ChartAreas[0].AxisY.Minimum = 0;
        Chart3.ChartAreas[0].AxisY.Maximum = 100;

        //Chart3.ChartAreas[0].AxisY.LabelStyle.Format = "{#'%'}";
        //Chart3.Series[0].IsValueShownAsLabel = true;
        //Chart3.Series[0].Label = "#PERCENT{P}";

        Chart3.DataBind();
        //Line Chart End

    }
    protected void btnEmp_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptEmpDownload.aspx");
    }
    protected void btnatt_Click(object sender, EventArgs e)
    {
        Response.Redirect("AttenanceDownload.aspx");
    }
    protected void btnLeave_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptLeaveSample.aspx");
    }
    protected void btnOT_Click(object sender, EventArgs e)
    {
        Response.Redirect("OTDownload.aspx");
    }
    protected void btncontract_Click(object sender, EventArgs e)
    {
        Response.Redirect("ContractRPT.aspx");
    }
    protected void ddlRptType_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlRptType.SelectedValue == "Year")
        {
            ddlMonths.SelectedIndex = 0;
            ddlMonths.Enabled = false;
        }
        else
        {
            ddlMonths.SelectedIndex = 0;
            ddlMonths.Enabled = true;
        }
    }
    protected void btnSal_Click(object sender, EventArgs e)
    {
        Response.Redirect("FrmDeductionDownload.aspx");
    }
}
