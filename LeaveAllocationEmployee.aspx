﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="LeaveAllocationEmployee.aspx.cs" Inherits="LeaveAllocationEmployee_1" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
    <head id="Head1" runat="server">
        <title>Payroll Management Systems</title>
        <link href="new_css/main.css" rel="stylesheet" type="text/css" />
        <link href="new_css/colors/dark_blue.css" rel="stylesheet" type="text/css" />
        <link href="new_css/superfish.css" rel="stylesheet" type="text/css" />

        <script src="new_js/jquery-1.4.2.min.js" type="text/javascript"></script>
        <script src="new_js/jquery.hoverintent.minified.js" type="text/javascript"></script>
        <script src="new_js/superfish.js" type="text/javascript"></script>
        <script src="new_js/jquery.tools.min.js" type="text/javascript"></script>
        <%--<script src="new_js/jquery.simplemodal.js" type="text/javascript"></script>--%>
        <script src="new_js/jquery.notifybar.js" type="text/javascript"></script>
        <script src="new_js/jquery.tipsy.js" type="text/javascript"></script>
        <script src="new_js/enhance.js" type="text/javascript"></script>
        <script src="new_js/excanvas.js" type="text/javascript"></script>
        <script src="new_js/visualize.jquery.js" type="text/javascript"></script>
        <script src="new_js/jquery.collapsible.js" type="text/javascript"></script>
        <script src="new_js/jquery.autosuggest.packed.js" type="text/javascript"></script>
        <script src="new_js/platinum-admin.js" type="text/javascript"></script>
        <style>
            .sf-navbar li ul 
            {
	            width: 1100px; /*IE6 soils itself without this*/
	            margin: 0px auto;
	            margin-left:-20em;	            
            }
        </style>
        <script type="text/javascript">
        function SelectheaderCheckboxes(cbSelectAll) {

            var gvcheck = document.getElementById('DataGridView1');
            var i;
            //Condition to check header checkbox selected or not if that is true checked all checkboxes
            if (cbSelectAll.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    var inputs = gvcheck.rows[i].getElementsByTagName('input');
                    inputs[0].checked = true;
                }
            }
            //if condition fails uncheck all checkboxes in gridview
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    var inputs = gvcheck.rows[i].getElementsByTagName('input');
                    inputs[0].checked = false;
                }
            }
        }
        //function to check header checkbox based on child checkboxes condition
        function Selectchildcheckboxes(header) {
            var ck = header;
            var count = 0;
            var gvcheck = document.getElementById('DataGridView1');
            var headerchk = document.getElementById(header);
            var rowcount = gvcheck.rows.length;
            //By using this for loop we will count how many checkboxes has checked
            for (i = 1; i < gvcheck.rows.length; i++) {
                var inputs = gvcheck.rows[i].getElementsByTagName('input');
                if (inputs[0].checked) {
                    count++;
                }
            }
            //Condition to check all the checkboxes selected or not
            if (count == rowcount - 1) {
                headerchk.checked = true;
            }
            else {
                headerchk.checked = false;
            }
        }
    </script>
<script type ="text/jscript" >
        function GoBack() {
            window.history.forward();
        }
   </script>
    </head>
    <body onload="GoBack();">
        <form id="form1" runat="server">
            <cc1:ToolkitScriptManager runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true"
                ID="ScriptManager1" EnablePartialRendering="true">
            </cc1:ToolkitScriptManager>
            <div id="header">
                <div id="header-top">
                    <div id="logo">
	                    <h1>&nbsp;</h1>
	                    <span id="slogan">&nbsp;</span> 
	                </div>
                    <!-- end logo -->
                    <div id="login-info">
                        <img id="profile" alt="profile" src="./new_images/icons/user.png" />
                        <p id="top"><span id="name"><asp:Label ID="lblusername" runat="server"></asp:Label></span><br />
                        <span id="links">
                        <%--<a id="message-link" href="#" title="click to see the messages!">
                        <span id="message-count">1</span> new message. </a>--%></span></p>
                        <div>
                            <asp:Label ID="lblComany" runat="server" Text="" Font-Bold="true"></asp:Label>
                        </div>
	                    <div id="messages-box">
		                    <h4>Messages</h4>
	                        <h5><a id="new-message" href="#">New Message</a></h5>
	                        <hr />
	                        <ul id="messages">
		                        <li class="new"><a href="#">
		                            <img alt="user" src="./new_images/icons/sender.png" />
		                            <span class="sender">John Doe</span>
		                            <span class="description">Hi this is just a description...</span>
		                            <span class="date">20 minutes ago...</span> </a>
		                        </li>
		                        <li><a href="#">
		                            <img alt="user" src="./new_images/icons/sender.png" />
		                            <span class="sender">John Doe</span>
		                            <span class="description">Hi this is just a description...</span>
		                            <span class="date">24 minutes ago...</span> </a>
		                        </li>
		                        <li><a href="#">
		                            <img alt="user" src="./new_images/icons/sender.png" />
		                            <span class="sender">John Doe</span>
		                            <span class="description">Hi this is just a description...</span>
		                            <span class="date">50 minutes ago...</span> </a>
		                        </li>
		                        <li id="last"><a id="all-messages" href="#">See all messages!
		                            <span id="unreaded">8 unreaded.</span> </a>
		                        </li>
		                    </ul>
		                    <br class="clear" />
	                    </div><!-- end messages-box -->
	                    <a id="power" href="Default.aspx" title="Logout">logout</a>
                    </div>
                    <!-- end login -->
                    <div id="nav">
	                    <ul class="sf-menu sf-navbar">
	                        <li><a id="dashboard" href="Dashboard.aspx">Dashboard</a>
	                            <ul>
		                            <li>&nbsp;
		                            </li>
	                            </ul>
	                        </li>
	                        <li class="current"><a id="Employee" href="EmployeeRegistration.aspx">Employee</a>
	                            <ul>
		                            <li><a href="EmployeeRegistration.aspx">Employee Registration</a> </li>					                    
		                            <li><a href="OfficalProfileDetails.aspx">Official Profile</a></li>
		                            <li><a href="SalaryMaster.aspx">Salary Master</a></li>
		                            <li  class="current"><a href="LeaveAllocationEmployee.aspx">Leave Allocation</a></li>
		                            <li><a href="ApplyForLeave.aspx">Apply Leave</a></li>						                    
		                            <li><a href="SalaryRegistration.aspx">Salary Details</a></li>
		                            <li><a href="SalaryAdvance.aspx">Salary Advance</a></li>
		                             <li><a href="Settlement.aspx">Settlement</a></li>
		                            <li><a href="EmployeeDeactiveMode.aspx">Employee Re-Activate</a></li>					                    
	                            </ul>
	                        </li>
	                         <li><a id="Reports" href="RptDepartmentwiseEmployee.aspx">Reports</a>
	                            <ul>
		                            <li><a href="RptDepartmentwiseEmployee.aspx">Employee Details</a></li>
		                            <li><a href="RptAttendanceReport.aspx">Attendance Report</a></li>
		                            <li><a href="RptProbationperiod.aspx">Probationary Period</a></li>
		                            <li><a href="ContractBreak.aspx">Contract Details</a></li>
		                            <li><a href="RptResign.aspx">Resignation Reports</a></li>
		                            <li><a href="PaySlipGen.aspx">Payslip</a></li>
		                            <%--<li><a href="RptSalary.aspx">Salary Details</a></li>--%>
	                            </ul>
	                        </li>
	                        <li><a id="OT" href="OverTime.aspx">OT</a>
	                            <ul>
	                                <li><a href="OverTime.aspx">OT</a></li>
	                            </ul>
	                        </li>
	                        <li><a id="Master" href="MstEmployeeType.aspx">Masters</a>
	                            <ul>
		                            <li><a href="MstEmployeeType.aspx">Employee Type</a></li>  
		                            <li><a href="MstLeave.aspx">Leave</a></li>
		                            <li><a href="MstProbationPeriod.aspx">Probationary Period</a></li>
		                            <li><a href="UserRegistration.aspx">User</a></li>
		                            <li><a href="MSTPFESI.aspx">PF</a></li>
		                            <li><a href="MstBank.aspx">Bank</a></li>
		                            <li><a href="MstDepartment.aspx">Department</a></li>
		                            <li><a href="MstInsuranceCompany.aspx">Insurance</a></li>
		                            <li><a href="MstQualification.aspx">Qualification</a></li>
	                            </ul>
	                        </li>
	                        <li><a id="Bonus" href="BonusForAll.aspx">Bonus</a>
	                            <ul>
		                            <li><a href="BonusForAll.aspx">Bonus</a></li>
		                            <li><a href="StandardBonus.aspx">Group Bonus</a></li>					                    
	                            </ul>
	                        </li>
	                        <li><a id="ESI_PF" href="PFForm5.aspx">ESI & PF</a>
	                            <ul>
		                            <li><a href="PFForm5.aspx">PF Form 5</a></li>
		                            <li><a href="PFDownload.aspx">PF Download</a></li>
		                            <li><a href="FOrm10.aspx">Form 10</a></li>
		                            <li><a href="PFForm3Anew.aspx">PF Form 3A</a></li>
		                            <li><a href="PFform6A.aspx">PF Form 6A</a></li>
		                            <li><a href="ESIForm7.aspx">ESI Form 7</a></li>
	                            </ul>
	                        </li>
	                        <li><a id="Upload" href="AttenanceUpload.aspx">Upload</a>
	                            <ul>
		                            <li><a href="AttenanceUpload.aspx">Attenance Upload</a></li>
		                            <li><a href="UploadEmployee.aspx">Employee Upload</a></li>	
	                            </ul>
	                        </li>
                        </ul>
                    </div>
                    <!-- end div#nav -->
                </div>
                <!-- end div#header-top -->
                <div class="breadCrumb">
                    <ul>
                        <li class="first"><a href="#">Home</a> </li>
                        <li><a href="#">Users</a> </li>
                        <li><a href="#">Roles</a> </li>
                        <li class="last">Leave Allocation</li>
                    </ul>
                </div>
                <!-- end div#breadCrumb -->
                <%--<div id="search-box">
                    <form id="searchform" action="" method="get">
                        <fieldset class="search"><span>Search</span>
                        <input class="box" type="text" />
                        <button class="btn" title="Submit Search">Search</button></fieldset>
                    </form>
                </div>--%>
                <!-- end div#search-box -->
            </div>
            <!-- end header -->
            <div id="page-wrap">
                <div id="right-sidebar">
                    <div class="innerdiv">
	                    <h2 class="head">Comments</h2>
	                    <div class="innercontent clear">
		                    <h4>Recent Comments</h4>
		                    <ul id="comments" class="tooltip-enabled">
		                        <li><a href="#" title="this comment is from John Doe">
		                            <img alt="user" src="./new_images/icons/sender.png" />
		                            <span class="sender">John Doe</span>
		                            <span class="description">Hey i liked your theme!</span>
		                            <span class="date">20 minutes ago...</span> </a>
		                            <span class="floatright"><a title="Aprrove this comment" href="#">Approve</a> | 
		                            <a title="Delete this comment"  href="#">Delete</a>
		                            </span>
		                            <br class="clear" />
		                        </li>
		                        <li><a href="#" title="again this guy!">
		                            <img alt="user" src="./new_images/icons/sender.png" />
		                            <span class="sender">John Doe</span>
		                            <span class="description">Hi, i need some help.</span>
		                            <span class="date">24 minutes ago...</span> </a>
		                            <span class="floatright"><a title="Aprrove this comment"  href="#">Approve</a> | 
		                            <a title="Delete this comment"  href="#">Delete</a>
		                            </span>
		                            <br class="clear" />
		                        </li>
		                        <li><a href="#" title="is he a spammer?">
		                            <img alt="user" src="./new_images/icons/sender.png" />
		                            <span class="sender">John Doe</span>
		                            <span class="description">Thank you for your help.</span>
		                            <span class="date">50 minutes ago...</span> </a>
		                            <span class="floatright"><a title="Aprrove this comment"  href="#">Approve</a> | 
		                            <a title="Delete this comment"  href="#">Delete</a>
		                            </span>
		                            <br class="clear" />
		                        </li>
		                        <li class="last"><a class="all-messages" title="See all comments awaiting confirmation." href="#">See all comments!
		                            <span class="unreaded">8 awaiting confirm.</span> </a>
		                        </li>
		                    </ul>
	                    </div>
                    </div>
                    <div class="clear"></div>                    
                </div>
	            <!-- end right-sidebar -->
	            <asp:UpdatePanel ID="UpdatePanel1" runat="server" >
                    <ContentTemplate>
                        <div id="main-content">
                            <%--<div>
                                <h3 style="text-align:center;">Leave Allocation</h3><br />
                                <div class="clear"></div>
                            </div>--%>
                            <br /><br /><br /><br /><br />
                            <div class="clear_body">
                                <div class="innerdiv">
                                    <div class="innercontent">
                                        <!-- tab "panes" -->
                                        <div>
                                            <table class="full">
                                                <thead>
				                                    <tr id="PanelRegister" runat="server">
					                                    <th colspan="4" style="text-align:center;"><h5>Leave Allocation Registeration</h5></th>
				                                    </tr>
				                                    <tr id="PanelEditRegister" runat="server" visible="false">                                                       
                                                        <th colspan="4" style="text-align:center;"><h5>Leave Allocation Update</h5></th>
                                                    </tr>
				                                </thead>
				                                <tbody class="tooltip-enabled">
				                                <asp:Panel ID="panelError" runat="server" Visible="false">
			                                        <tr align="center" >
                                                        
                                                            <td colspan="4" align="center">
                                                                <asp:Label ID="lblError" runat="server" Font-Bold="true" Font-Size="16px" ForeColor="Red" ></asp:Label>
                                                            </td>
                                                            
                                                        </tr>	
                                                </asp:Panel>
                                                <asp:Panel ID="panelsuccess" runat="server" Visible="false" BorderColor="Black">
		                                        		    <tr style="background-color:Olive">
		                                        		        <td align="left" colspan="4">
		                                        		            <asp:Label ID="lblhd" runat="server" Text="Altius InfoSystems" ForeColor="White" Font-Bold="true" Font-Size="14px" ></asp:Label>
		                                        		        </td>
		                                        		    </tr>
		                                        		    <tr align="center">
		                                        		        <td colspan="4">
		                                        		            <asp:Label ID="lbloutput" runat="server" Font-Bold="true" Font-Size="16px"></asp:Label>
		                                        		           
		                                        		        </td>
		                                        		    </tr>
		                                        		    <tr align="center">
		                                        		        <td colspan="4">
		                                        		            <asp:Button ID="btnok" runat="server" Text="Ok" onclick="btnok_Click" Width="75" Height="28" CssClass="button green" />
		                                        		        </td>
		                                        		    </tr>
		                                        	</asp:Panel>		    
				                                    <tr>
				                                        <td><asp:Label ID="lblcategory" runat="server" Text="Category" Font-Bold="true"></asp:Label></td>
				                                        <td>
				                                            <asp:DropDownList ID="ddlcategory" runat="server" Width="100" Height="25" 
                                                                AutoPostBack="true" onselectedindexchanged="ddlcategory_SelectedIndexChanged" >
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td><asp:Label ID="lblDepartment" runat="server" Text="Department" Font-Bold="true"></asp:Label></td>
                                                        <td valign="top">
                                                            <asp:DropDownList ID="ddlDepartment" runat="server" Width="215" Height="25" 
                                                                AutoPostBack="true" 
                                                                onselectedindexchanged="ddlDepartment_SelectedIndexChanged1" >
                                                            </asp:DropDownList>
                                                            <asp:Button ID="btnclick" runat="server" Text="Click" onclick="btnclick_Click" CssClass="button green" Height="28" Width="70"/>
                                                        </td>
				                                    </tr>
				                                    <%--<tr id="PanelButtons" runat="server" Visible="false">
                                                        <td>
                                                            <asp:Button ID="btnIndividual" runat="server" Text="Individual" OnClick="btnIndividual_Click" 
                                                                Width="100" Height="28" Visible="false" CssClass="button green"/>
                                                        </td>
                                                        <td colspan="3">
                                                            <asp:Button ID="btnall" runat="server" Text="All" OnClick="btnall_Click"  
                                                                Width="75" Height="28" Visible="false" CssClass="button green"/>
                                                        </td>
                                                    </tr>--%>
                                                    <tr id="PanelGrouporIndividual" runat="server" Visible="false">
                                                        <td><asp:Label ID="lblexistingno" runat="server" Text="Existing No" Font-Bold="true"></asp:Label></td>
                                                        <td><asp:TextBox ID="txtexistingno" runat="server" ></asp:TextBox></td>
                                                        <td><asp:Label ID="lblempNo" runat="server" Text="Employee No" Font-Bold="true"></asp:Label></td>
                                                        <td>
                                                            <asp:TextBox ID="txtEmpNo" runat="server"></asp:TextBox>
                                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR4" runat="server" 
                                                                FilterMode="ValidChars" FilterType="UppercaseLetters,LowercaseLetters,Numbers,Custom" 
                                                                TargetControlID="txtEmpNo"  ValidChars="">
                                                            </cc1:FilteredTextBoxExtender>                                                        
                                                            <asp:Button ID="btnsearch" runat="server" Text="Search" OnClick="btnsearch_Click"  
                                                                Width="75" Height="28" CssClass="button green"/>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td><asp:Label ID="lblfinancialyr" runat="server" Text="Financial Year" Font-Bold="true"></asp:Label></td>
                                                        <td><asp:DropDownList ID ="ddlFinaYear" runat="server" Height="25" Width="100"></asp:DropDownList></td>
                                                        <td>
                                                            <asp:CheckBox ID="chkcarry" runat="server" Text="Carry Forward" Checked="true" />
                                                        </td>
                                                        <td id="PanelFinacialYear" runat="server">&nbsp;
                                                            <%--<asp:Button ID="btninitializeyr" runat="server" Text="Initialize Financial Year" 
                                                                Width="200" Height="28" onclick="btninitializeyr_Click" Visible="false" CssClass="button greeen"/>--%>
                                                                
                                                        </td>
                                                        
                                                    </tr>
                                                    <tr id="PanelGrid" runat="server" Visible="false">
                                                        <td colspan="4">
                                                            <asp:GridView ID="DataGridView1" runat="server" AutoGenerateColumns="False" OnRowCancelingEdit="DataGridView1_RowCancelingEdit"
                                                                OnRowEditing="DataGridView1_RowEditing" 
                                                                OnRowUpdating="DataGridView1_RowUpdating" Width="100%" BackColor="#E7E7E7">
                                                                <Columns>
                                                                    <asp:TemplateField HeaderText="Staff Id">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblStaffID" runat="server" Text='<%# Eval("EmpNo") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>                                                                    
                                                                    <asp:TemplateField HeaderText="Staff Name">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblStaffName" runat="server" Text='<%# Eval("EmpName") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Designation">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lbldesignation" runat="server" Text='<%# Eval("Designation") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>
                                                                            <asp:CheckBox ID="chkselectall" runat="server" onclick="javascript:SelectheaderCheckboxes(this);" />
                                                                            All
                                                                        </HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:CheckBox ID="chkClear" runat="server" />
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                            </asp:GridView>
                                                        </td>
                                                    </tr>
                                                    <tr id="PanelReterviewRecod" runat="server" visible="false">
                                                        <td colspan="4">
                                                            <asp:Button ID="btnreterivew" runat="server" Text ="Retrive" 
                                                                Width="78" Height="28" onclick="btnreterivew_Click" CssClass="button green"/>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="lblleavedays" runat="server" Text="Eligible Leave For Year" 
                                                                Font-Bold="true" Width="150">
                                                            </asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtLeaveAllocation" runat="server" Width="85" > </asp:TextBox>
                                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR2" runat="server" FilterMode="ValidChars"
                                                                FilterType="Numbers,Custom" TargetControlID="txtLeaveAllocation" ValidChars="">
                                                            </cc1:FilteredTextBoxExtender>
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="Label1" runat="server" Text="Eligible Leave Days / Month" 
                                                                Font-Bold="True" Width="150px"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtleaveallocationMonth" runat="server" Width="200"> </asp:TextBox>
                                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR1" runat="server" FilterMode="ValidChars"
                                                                FilterType="Numbers,Custom" TargetControlID="txtleaveallocationMonth" ValidChars="">
                                                            </cc1:FilteredTextBoxExtender>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td><asp:Label ID="lbldate" runat="server" Text="Eligible From Date" 
                                                                Font-Bold="True" Width="100px"></asp:Label></td>
                                                        <td>
                                                            <asp:TextBox ID="txtdate" runat="server" Width="200" ></asp:TextBox>
                                                            <cc1:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtdate" 
                                                                Format ="dd-MM-yyyy" CssClass ="orange" Enabled="true">
                                                            </cc1:CalendarExtender>
                                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR11" runat="server"
                                                                FilterMode="ValidChars" FilterType="Numbers,Custom"
                                                                TargetControlID="txtdate" ValidChars="0123456789/- ">
                                                            </cc1:FilteredTextBoxExtender>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td colspan="3">&nbsp;</td>
                                                    </tr>
                                                    <tr align="center">
                                                        <td colspan="4">
                                                            <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click"  
                                                                Width="75" Height="28" Font-Bold="true"  CssClass="button green" />
                                                            <asp:Button ID="btnEdit" runat="server" Text="Edit"  Width="75" Height="28" 
                                                                onclick="btnEdit_Click" Font-Bold="true" CssClass="button green"/>
                                                            <asp:Button ID="btnreset" runat="server" Text="Reset"  Width="75" Height="28" 
                                                                Font-Bold="true" onclick="btnreset_Click" CssClass="button green"/>
                                                            <asp:Button ID="btncancel" runat="server" Text="Cancel"   Width="75" Height="28" 
                                                                onclick="btncancel_Click" Font-Bold="true" CssClass="button green"/>
                                                        </td>
                                                    </tr>
				                                </tbody>
                                            </table>
                                        </div>
                                        <!-- end tab "panes" -->
                                    </div>
                                    <!-- end innercontent -->
                                </div>
                                <!-- end innerdiv -->
                            </div>
                            <!-- end clear_body -->
                        </div>
                        <!-- end main-content -->
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            <!-- end page-wrap -->
            <div id="footer">
                <ul>
                    <li>Copyright &copy; 2012. All rights reserved.</li>
                    <li>Powered by <a href="http://www.altius.co.in" target="_blank">Altius Infosystems</a></li>
                </ul>
            </div>
            <!-- end footer -->
        </form>
    </body>
</html>
