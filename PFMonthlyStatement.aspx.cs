﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class PFMonthlyStatement : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    string Mont = "";
    DateTime m;
    int d = 0;
    string Year = "";
    string SessionAdmin;
    protected void Page_Load(object sender, EventArgs e)
    {
        SessionAdmin = Session["Isadmin"].ToString();
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        string ss = Session["UserId"].ToString();
        txtvpf.Text = "0";
    }

    public void Month()
    {

        m = Convert.ToDateTime(txtpfdate.Text);
        d = m.Month;
        int mon = m.Month;
        int Ye = m.Year;
        Year = Convert.ToString(Ye);


        switch (d)
        {
            case 1:
                Mont = "January";
                break;

            case 2:
                Mont = "February";
                break;
            case 3:
                Mont = "March";
                break;
            case 4:
                Mont = "April";
                break;
            case 5:
                Mont = "May";
                break;
            case 6:
                Mont = "June";
                break;
            case 7:
                Mont = "July";
                break;
            case 8:
                Mont = "August";
                break;
            case 9:
                Mont = "September";
                break;
            case 10:
                Mont = "October";
                break;
            case 11:
                Mont = "November";
                break;
            case 12:
                Mont = "December";
                break;
            default:
                break;
        }
    }

    protected void btnsearch_Click(object sender, EventArgs e)
    {
        try
        {
            if (txtempno.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Your Employee Registration Number..!');", true);
            }
            else if (txtpfdate.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Please Select Date..!');", true);
            }
            else
            {
                Month();
                DataTable dt = new DataTable();
                dt = objdata.PFMonthlyStatement(txtempno.Text, Mont, Year);
                txtempname.Text = dt.Rows[0]["EmpName"].ToString();
                txtpfnumber.Text = dt.Rows[0]["PFNumber"].ToString();
                txtworkeddays.Text = dt.Rows[0]["TotalWorkingDays"].ToString();
                txtearnedwages.Text = dt.Rows[0]["BasicandDA"].ToString();
                txtpf.Text = dt.Rows[0]["ProvidentFund"].ToString();

                double Basic1 = Convert.ToInt32(txtearnedwages.Text);

                double EPS = Basic1 * 8.33 / 100;
                double EPF = Basic1 * 3.67 / 100;
                string strEPS = Convert.ToString(EPS);
                string strEPF = Convert.ToString(EPF);
                txteps.Text = strEPS;
                txtepf.Text = strEPF;
            }
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Server Error Contact Admin..!');", true);
        }




    }
    protected void btnsave_Click(object sender, EventArgs e)
    {
        try
        {
            bool ErrFlag = false;
            if (txtempno.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Your Employee Registration Number..!');", true);
            }
            else
            {
                Month();
                PFMonthlySatementClass objPFM = new PFMonthlySatementClass();
                objPFM.EmpNo = txtempno.Text;
                objPFM.EmpName = txtempname.Text;
                objPFM.PFNumber = txtpfnumber.Text;
                objPFM.WorkedDays = txtworkeddays.Text;
                objPFM.Basic = txtearnedwages.Text;
                objPFM.PF = txtpf.Text;
                objPFM.VPF = txtvpf.Text;
                objPFM.EPS = txteps.Text;
                objPFM.EPF = txtepf.Text;
                DateTime PFDate;
                PFDate = DateTime.ParseExact(txtpfdate.Text, "dd-MM-yyyy", null);
                ErrFlag = true;
                if (ErrFlag)
                {
                    objdata.InsertPFFormonthlyStatement(objPFM, Mont, Year, PFDate);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Your Reocrd Registered Successfully..!');", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Your Correct Value Formate..!');", true);
                }
            }
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Server Error Contact Admin....!');", true);
        }



    }
}
