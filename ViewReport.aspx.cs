﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class ViewReport : System.Web.UI.Page
{
    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    ReportDocument rd = new ReportDocument();
    BALDataAccess objdata = new BALDataAccess();
    SqlConnection con;
    string str_month;
    string str_yr;
    string str_cate;
    string str_dept;
    string SessionCcode;
    string SessionLcode;
    string SessionAdmin;
    static string CmpName;
    static string Cmpaddress;
    string query;
    string fromdate;
    string EmployeeType;
    string ToDate;
    string salaryType;
    string Year_Str;
    string MonthAndYear;
    protected void Page_Load(object sender, EventArgs e)
    {
        //ReportDocument rd = new ReportDocument();
        SqlConnection con = new SqlConnection(constr);
        SessionAdmin = Session["Isadmin"].ToString();
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        string ss = Session["UserId"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();

        str_cate = Request.QueryString["Cate"].ToString();
        str_dept = Request.QueryString["Depat"].ToString();
        str_month = Request.QueryString["Months"].ToString();
        str_yr = Request.QueryString["yr"].ToString();
        fromdate = Request.QueryString["fromdate"].ToString();
        ToDate = Request.QueryString["ToDate"].ToString();
        EmployeeType = Request.QueryString["EmpType"].ToString();
       
        salaryType = Request.QueryString["Salary"].ToString();

        MonthAndYear = str_month + " " + str_yr;

        DataTable dt = new DataTable();
        dt = objdata.Company_retrive(SessionCcode, SessionLcode);
        if (dt.Rows.Count > 0)
        {
            CmpName = dt.Rows[0]["Cname"].ToString();
            Cmpaddress = (dt.Rows[0]["Address1"].ToString() + ", " + dt.Rows[0]["Address2"].ToString() + ", " + dt.Rows[0]["Location"].ToString() + "-" + dt.Rows[0]["Pincode"].ToString());
        }
        if ((EmployeeType == "1") || (EmployeeType == "2") || (EmployeeType == "4") || (EmployeeType == "6") || (EmployeeType == "7"))
        {
            query = "Select SalDet.EmpNo,EmpDet.ExisistingCode,MstDpt.DepartmentNm,EmpDet.EmpName,SalDet.WorkedDays, " +
                           " sum(SM.Base + SM.HRA + SM.FDA) as Base,SalDet.BasicandDA,(SalDet.TotalBasic) as TotGrossPay,SalDet.HRA,SalDet.FDA,SalDet.Deduction1, SalDet.Deduction2,SalDet.Deduction3, SalDet.Deduction4, SalDet.Deduction5, " +
                           " Sum(SpinningAmt + allowances1 + BasicandDA) as Total_Stipend,(SalDet.PfSalary) as PF,SalDet.ESI,SalDet.GrossEarnings,SalDet.allowances1,SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.allowances5," +
                           " CAST(EmpDet.ExisistingCode AS int) as OrderToken,SalDet.Advance,  " +
                           " SalDet.withpay,SalDet.Availabledays,SalDet.TotalDeductions as TotDed, " +
                             //" SalDet.Deduction1,SalDet.Deduction2,SalDet.Deduction3,SalDet.Deduction4, " +
                           " SalDet.TotalDeductions,SalDet.OverTime,(round(SalDet.Netpay/5,0)*5) as Val,SalDet.FineAmt, " +
                           " SalDet.NetPay from EmployeeDetails " +
                           " EmpDet inner Join SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo " +
                           " inner Join MstDepartment as MstDpt on MstDpt.DepartmentCd = EmpDet.Department " +
                           " inner Join OfficialProfile OP on OP.EmpNo=SalDet.EmpNo " +
                           " inner Join SalaryMaster SM on SM.EmpNo=SalDet.EmpNo where SalDet.Month='" + str_month + "' " +
                           " AND SalDet.FinancialYear='" + str_yr + "' and EmpDet.StafforLabor='" + str_cate + "' and " +
                           " EmpDet.EmployeeType='" + EmployeeType + "' and EmpDet.Ccode='" + SessionCcode + "' and " +
                           " EmpDet.Lcode='" + SessionLcode + "' and OP.WagesType='" + salaryType + "' " +
                           " group by SalDet.EmpNo,EmpDet.ExisistingCode,EmpDet.InsOuts,MstDpt.DepartmentNm,EmpDet.EmpName,SalDet.WorkedDays,SalDet.CL, " +
                           " SM.Base,SalDet.SpinningAmt,SalDet.BasicandDA,SalDet.PfSalary,SalDet.ESI, " +
                           " SalDet.Advance,SalDet.allowances1,SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.allowances5,SalDet.Deduction3, SalDet.Deduction4, SalDet.Deduction5, " +
                           " SalDet.allowances5,SalDet.Deduction1,SalDet.Deduction2,SalDet.Deduction3,SalDet.Deduction4,SalDet.withpay,SalDet.Availabledays, " +
                           " SalDet.Deduction5,SalDet.TotalDeductions,OverTime,SalDet.NetPay,SalDet.GrossEarnings,SalDet.TotalBasic,SalDet.HRA,SalDet.FDA,SalDet.Advance,SalDet.FineAmt Order by OrderToken Asc";

            //if (salaryType == "2")
            //{
            //    query = "Select SalDet.EmpNo,EmpDet.ExisistingCode,EmpDet.EmpName,EmpDet.Designation,EmpDet.Department,SalDet.Totalworkingdays,SalDet.NFh,SalDet.BasicandDA,SalDet.FDA,SalDet.VDA,SalDet.HRA," +
            //                         " SalDet.Advance,SalDet.Stamp,SalDet.Unioncg,SalDet.Words,SalDet.WorkedDays,SalDet.Month,SalDet.Year,SalDet.LeaveDays," +
            //                         " SalDet.SalaryDate,SalDet.allowances1,SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.allowances5,SalDet.Deduction1,SalDet.Deduction2,SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5,SalDet.Losspay,SalDet.NetPay,SalDet.TotalDeductions,SalDet.GrossEarnings,SalDet.OverTime," +
            //                         " MstDpt.DepartmentNm,SalDet.ProvidentFund,SalDet.ESI,SalDet.HRA,SalDet.Weekoff,SalDet.Fbasic,SalDet.FFDA,SalDet.FHRA," +
            //                         " SalDet.WorkedDays,convert(varchar,OP.Dateofjoining,105) as Dateofjoining,OP.PFnumber,OP.ESICnumber from EmployeeDetails EmpDet inner Join SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo" +
            //                         " inner Join MstDepartment as MstDpt on MstDpt.DepartmentCd = EmpDet.Department inner Join inner Join OfficialProfile OP on OP.EmpNo=SalDet.EmpNo where " +
            //                         "SalDet.Month='" + str_month + "' AND SalDet.FinancialYear='" + str_yr + "' and EmpDet.StafforLabor='" + str_cate + "' and " +
            //                        " EmpDet.Ccode='" + SessionCcode + "' and EmpDet.Lcode='" + SessionLcode + "' and OP.WagesType='" + salaryType + "'";
            //}
            //else
            //{
            //    query = "Select SalDet.EmpNo,EmpDet.ExisistingCode,EmpDet.EmpName,EmpDet.Designation,EmpDet.Department,SalDet.Totalworkingdays,SalDet.NFh,SalDet.BasicandDA,SalDet.FDA,SalDet.VDA,SalDet.HRA," +
            //                         " SalDet.Advance,SalDet.Stamp,SalDet.Unioncg,SalDet.Words,SalDet.WorkedDays,SalDet.Month,SalDet.Year,SalDet.LeaveDays," +
            //                         " SalDet.SalaryDate,SalDet.allowances1,SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.allowances5,SalDet.Deduction1,SalDet.Deduction2,SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5,SalDet.Losspay,SalDet.NetPay,SalDet.TotalDeductions,SalDet.GrossEarnings,SalDet.OverTime," +
            //                         " MstDpt.DepartmentNm,SalDet.ProvidentFund,SalDet.ESI,SalDet.HRA,SalDet.Weekoff,SalDet.Fbasic,SalDet.FFDA,SalDet.FHRA," +
            //                         " SalDet.WorkedDays,convert(varchar,OP.Dateofjoining,105) as Dateofjoining,OP.PFnumber,OP.ESICnumber from EmployeeDetails EmpDet inner Join SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo" +
            //                         " inner Join MstDepartment as MstDpt on MstDpt.DepartmentCd = EmpDet.Department inner Join inner Join OfficialProfile OP on OP.EmpNo=SalDet.EmpNo where " +
            //                         "SalDet.Month='" + str_month + "' AND SalDet.FinancialYear='" + str_yr + "' and EmpDet.StafforLabor='" + str_cate + "' and SalDet.FromDate = convert(datetime,'" + fromdate + "', 105) and SalDet.ToDate = Convert(Datetime,'" + ToDate + "', 105) and " +
            //                        " EmpDet.Ccode='" + SessionCcode + "' and EmpDet.Lcode='" + SessionLcode + "' and OP.WagesType='" + salaryType + "'";
            //}
        }
        else
        {

            query = "Select SalDet.EmpNo,EmpDet.ExisistingCode,MstDpt.DepartmentNm,EmpDet.EmpName,SalDet.WorkedDays, " +
                       " sum(SM.Base + SM.HRA + SM.FDA) as Base,SalDet.BasicandDA,(SalDet.TotalBasic) as TotGrossPay,SalDet.HRA,SalDet.FDA,SalDet.Deduction1, SalDet.Deduction2,SalDet.Deduction3, SalDet.Deduction4, SalDet.Deduction5, " +
                       " Sum(SpinningAmt + allowances1 + BasicandDA) as Total_Stipend,(SalDet.PfSalary) as PF,SalDet.ESI,(SalDet.GrossEarnings) as BasicRAI,SalDet.allowances1,SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.allowances5," +
                       " CAST(EmpDet.ExisistingCode AS int) as OrderToken,SalDet.Advance,  " +
                       " SalDet.withpay,SalDet.Availabledays,SalDet.TotalDeductions as TotDed,(round(SalDet.Netpay/5,0)*5) as Val, " +
                //" SalDet.Deduction1,SalDet.Deduction2,SalDet.Deduction3,SalDet.Deduction4, " +
                       " SalDet.TotalDeductions,SalDet.OverTime,SalDet.FineAmt, " +
                       " SalDet.NetPay from EmployeeDetails " +
                       " EmpDet inner Join SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo " +
                       " inner Join MstDepartment as MstDpt on MstDpt.DepartmentCd = EmpDet.Department " +
                       " inner Join OfficialProfile OP on OP.EmpNo=SalDet.EmpNo " +
                       " inner Join SalaryMaster SM on SM.EmpNo=SalDet.EmpNo where SalDet.Month='" + str_month + "' " +
                       " AND SalDet.FinancialYear='" + str_yr + "' and EmpDet.StafforLabor='" + str_cate + "' and " +
                       " EmpDet.EmployeeType='" + EmployeeType + "' and EmpDet.Ccode='" + SessionCcode + "' and " +
                       " EmpDet.Lcode='" + SessionLcode + "' and OP.WagesType='" + salaryType + "' " +
                       " group by SalDet.EmpNo,EmpDet.ExisistingCode,EmpDet.InsOuts,MstDpt.DepartmentNm,EmpDet.EmpName,SalDet.WorkedDays,SalDet.CL, " +
                       " SM.Base,SalDet.SpinningAmt,SalDet.BasicandDA,SalDet.PfSalary,SalDet.ESI, " +
                       " SalDet.Advance,SalDet.allowances1,SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.allowances5,SalDet.Deduction3, SalDet.Deduction4, SalDet.Deduction5, " +
                       " SalDet.allowances5,SalDet.Deduction1,SalDet.Deduction2,SalDet.Deduction3,SalDet.Deduction4,SalDet.withpay,SalDet.Availabledays, " +
                       " SalDet.Deduction5,SalDet.TotalDeductions,OverTime,SalDet.NetPay,SalDet.GrossEarnings,SalDet.TotalBasic,SalDet.HRA,SalDet.FDA,SalDet.Advance,SalDet.FineAmt Order by OrderToken Asc";


            //if (salaryType == "2")
            //{
            //    query = "Select SalDet.EmpNo,EmpDet.ExisistingCode,EmpDet.EmpName,EmpDet.Designation,EmpDet.Department,SalDet.Totalworkingdays,SalDet.NFh,SalDet.BasicandDA,SalDet.FDA,SalDet.VDA,SalDet.HRA," +
            //                         " SalDet.Advance,SalDet.Stamp,SalDet.Unioncg,SalDet.Words,SalDet.WorkedDays,SalDet.Month,SalDet.Year,SalDet.LeaveDays," +
            //                         " SalDet.SalaryDate,SalDet.allowances1,SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.allowances5,SalDet.Deduction1,SalDet.Deduction2,SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5,SalDet.Losspay,SalDet.NetPay,SalDet.TotalDeductions,SalDet.GrossEarnings,SalDet.OverTime," +
            //                         " MstDpt.DepartmentNm,SalDet.ProvidentFund,SalDet.ESI,SalDet.HRA,SalDet.Weekoff,SalDet.Fbasic,SalDet.FFDA,SalDet.FHRA," +
            //                         " SalDet.WorkedDays,convert(varchar,OP.Dateofjoining,105) as Dateofjoining,OP.PFnumber,OP.ESICnumber from EmployeeDetails EmpDet inner Join SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo" +
            //                         "  inner Join MstDepartment as MstDpt on MstDpt.DepartmentCd = EmpDet.Department inner Join OfficialProfile OP on OP.EmpNo=SalDet.EmpNo where " +
            //                         "SalDet.Month='" + str_month + "' AND SalDet.FinancialYear='" + str_yr + "' and EmpDet.StafforLabor='" + str_cate + "' and " +
            //                        " EmpDet.EmployeeType='" + str_dept + "' and EmpDet.Ccode='" + SessionCcode + "' and EmpDet.Lcode='" + SessionLcode + "' and OP.WagesType='" + salaryType + "'";
            //}
            //else
            //{
            //    query = "Select SalDet.EmpNo,EmpDet.ExisistingCode,EmpDet.EmpName,EmpDet.Designation,EmpDet.Department,SalDet.Totalworkingdays,SalDet.NFh,SalDet.BasicandDA,SalDet.FDA,SalDet.VDA,SalDet.HRA," +
            //                         " SalDet.Advance,SalDet.Stamp,SalDet.Unioncg,SalDet.Words,SalDet.WorkedDays,SalDet.Month,SalDet.Year,SalDet.LeaveDays," +
            //                         " SalDet.SalaryDate,SalDet.allowances1,SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.allowances5,SalDet.Deduction1,SalDet.Deduction2,SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5,SalDet.Losspay,SalDet.NetPay,SalDet.TotalDeductions,SalDet.GrossEarnings,SalDet.OverTime," +
            //                         " MstDpt.DepartmentNm,SalDet.ProvidentFund,SalDet.ESI,SalDet.HRA,SalDet.Weekoff,SalDet.Fbasic,SalDet.FFDA,SalDet.FHRA," +
            //                         " SalDet.WorkedDays,convert(varchar,OP.Dateofjoining,105) as Dateofjoining,OP.PFnumber,OP.ESICnumber from EmployeeDetails EmpDet inner Join SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo" +
            //                         "  inner Join MstDepartment as MstDpt on MstDpt.DepartmentCd = EmpDet.Department inner Join OfficialProfile OP on OP.EmpNo=SalDet.EmpNo where " +
            //                         "SalDet.Month='" + str_month + "' AND SalDet.FinancialYear='" + str_yr + "' and EmpDet.StafforLabor='" + str_cate + "' and SalDet.FromDate = convert(datetime,'" + fromdate + "', 105) and SalDet.ToDate = Convert(Datetime,'" + ToDate + "', 105) and " +
            //                        " EmpDet.EmployeeType='" + str_dept + "' and EmpDet.Ccode='" + SessionCcode + "' and EmpDet.Lcode='" + SessionLcode + "' and OP.WagesType='" + salaryType + "'";
            //}
         
        }
        SqlCommand cmd = new SqlCommand(query, con);
        SqlDataAdapter sda = new SqlDataAdapter(cmd);
        DataSet ds1 = new DataSet();
        con.Open();
        sda.Fill(ds1);
        DataTable dt_1 = new DataTable();
        sda.Fill(dt_1);
        con.Close();

        if ((EmployeeType == "1") || (EmployeeType == "2") || (EmployeeType == "4") || (EmployeeType == "6") || (EmployeeType == "7"))
        {
            if (ds1.Tables[0].Rows.Count != 0)
            {

                rd.Load(Server.MapPath("Payslip/ESIPF.rpt"));
                rd.SetDataSource(ds1.Tables[0]);

                rd.DataDefinition.FormulaFields["CompanyName"].Text = "'" + CmpName + "'";
                rd.DataDefinition.FormulaFields["Unit"].Text = "'" + SessionLcode + "'";
                rd.DataDefinition.FormulaFields["Month"].Text = "'" + MonthAndYear + "'";
                rd.DataDefinition.FormulaFields["CompanyAddress1"].Text = "'" + Cmpaddress + "'";

                rd.DataDefinition.FormulaFields["ReportHead"].Text = "'" + "STAFF - DETAILS" + "'";

                CrystalReportViewer1.ReportSource = rd;
                CrystalReportViewer1.RefreshReport();
                CrystalReportViewer1.DataBind();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('No Data Found');", true);
            }
        }
        else
        {
            if (ds1.Tables[0].Rows.Count != 0)
            {

                rd.Load(Server.MapPath("Payslip/NONPF.rpt"));
                rd.SetDataSource(ds1.Tables[0]);

                rd.DataDefinition.FormulaFields["CompanyName"].Text = "'" + CmpName + "'";
                rd.DataDefinition.FormulaFields["Unit"].Text = "'" + SessionLcode + "'";
                rd.DataDefinition.FormulaFields["Month"].Text = "'" + MonthAndYear + "'";
                rd.DataDefinition.FormulaFields["CompanyAddress1"].Text = "'" + Cmpaddress + "'";

                rd.DataDefinition.FormulaFields["ReportHead"].Text = "'" + "STAFF - DETAILS" + "'";

                CrystalReportViewer1.ReportSource = rd;
                CrystalReportViewer1.RefreshReport();
                CrystalReportViewer1.DataBind();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('No Data Found');", true);
            }
        }

       



        //if (ds1.Tables[0].Rows.Count > 0)
        //{
        //    //for (int i = 0; i < dt_1.Rows.Count; i++)
        //    //{
        //    //    //Label lblEmpNo = (Label)gvsal.FindControl("lblExist");
        //    //    string qry = "Update SalaryDetails set Process_Mode='" + 1 + "' where Month='" + str_month + "' and Year='" + str_yr + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and ExisistingCode='" + dt_1.Rows[i]["ExisistingCode"].ToString() + "'";
        //    //    SqlCommand cmd1 = new SqlCommand(qry, con);
        //    //    con.Open();
        //    //    cmd1.ExecuteNonQuery();
        //    //    con.Close();
        //    //}

        //    //if (str_dept == "3" && salaryType == "2")
        //    //{
        //    //    rd.Load(Server.MapPath("PaySlip1.rpt")); //rd.Load(Server.MapPath("PaySlip_Permanent.rpt"));
        //    //}
        //    //else
        //    //{
        //    //    rd.Load(Server.MapPath("PaySlip1.rpt"));
        //    //}


        //    //rd.SetDataSource(ds1.Tables[0]);
        //    //rd.DataDefinition.FormulaFields["CompanyName"].Text = "'" + CmpName + "'";
        //    //rd.DataDefinition.FormulaFields["Unit"].Text = "'" + SessionLcode + "'";
        //    //rd.DataDefinition.FormulaFields["CompanyAddress1"].Text = "'" + Cmpaddress + "'";
        //    //CrystalReportViewer1.ReportSource = rd;
        //    //CrystalReportViewer1.RefreshReport();
        //    //CrystalReportViewer1.DataBind();


        //    //ReportDocument rd = new ReportDocument();
        ////    ExportOptions CrExportOptions;
            
        ////    DiskFileDestinationOptions CrDiskFileDestinationOptions = new DiskFileDestinationOptions();
        ////ExcelFormatOptions CrFormatTypeOptions = new ExcelFormatOptions();
        ////CrDiskFileDestinationOptions.DiskFileName = "D:\\payslip_1.xls";
        ////CrExportOptions = rd.ExportOptions;
        ////CrExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
        ////CrExportOptions.ExportFormatType = ExportFormatType.Excel;
        ////CrExportOptions.DestinationOptions = CrDiskFileDestinationOptions;
        ////CrExportOptions.FormatOptions = CrFormatTypeOptions;
        ////rd.Export();
        //}
        //else
        //{
        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('No Data Found');", true);

        //}
    }
}
