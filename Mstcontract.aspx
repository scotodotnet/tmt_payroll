<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Mstcontract.aspx.cs" Inherits="Mstcontract" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
    <head id="Head1" runat="server">
        <title>Payroll Management Systems</title>
        <link href="new_css/main.css" rel="stylesheet" type="text/css" />
        <link href="new_css/colors/dark_blue.css" rel="stylesheet" type="text/css" />
        <link href="new_css/superfish.css" rel="stylesheet" type="text/css" />

        <script src="new_js/jquery-1.4.2.min.js" type="text/javascript"></script>
        <script src="new_js/jquery.hoverintent.minified.js" type="text/javascript"></script>
        <script src="new_js/superfish.js" type="text/javascript"></script>
        <script src="new_js/jquery.tools.min.js" type="text/javascript"></script>
        <%--<script src="new_js/jquery.simplemodal.js" type="text/javascript"></script>--%>
        <script src="new_js/jquery.notifybar.js" type="text/javascript"></script>
        <script src="new_js/jquery.tipsy.js" type="text/javascript"></script>
        <script src="new_js/enhance.js" type="text/javascript"></script>
        <script src="new_js/excanvas.js" type="text/javascript"></script>
        <script src="new_js/visualize.jquery.js" type="text/javascript"></script>
        <script src="new_js/jquery.collapsible.js" type="text/javascript"></script>
        <script src="new_js/jquery.autosuggest.packed.js" type="text/javascript"></script>
        <script src="new_js/platinum-admin.js" type="text/javascript"></script>
        <script type ="text/jscript" >
            function GoBack() {
                window.history.forward();
            }
            function showConfirm() 
            {
                if (confirm("Are you Sure want to Delete ?") == true) 
                {
                    //__doPostBack('<%= this.cmdOverriteGrade.ClientID %>', "true");
                    var clickButton = document.getElementById("<%= cmdOverriteGrade.ClientID %>");
                    clickButton.click();
                }
                return false;
            }
        </script>
        <style>
            .sf-navbar li ul 
            {
	            width: 1100px; /*IE6 soils itself without this*/
	            margin: 0px auto;
	            margin-left:-20em;	            
            }
        </style>
    </head>
    <body onload="GoBack();">
        <form id="form1" runat="server">
            <cc1:ToolkitScriptManager runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true"
                ID="ScriptManager1" EnablePartialRendering="true">
            </cc1:ToolkitScriptManager>
            <div id="header">
                <div id="header-top">
                    <div id="logo">
	                    <h1>&nbsp;</h1>
	                    <span id="slogan">&nbsp;</span> 
	                </div>
                    <!-- end logo -->
                    <div id="login-info">
                        <img id="profile" alt="profile" src="./new_images/icons/user.png" />
                        <p id="top"><span id="name"><asp:Label ID="lblusername" runat="server"></asp:Label></span><br />
                        <span id="links">
                        <%--<a id="message-link" href="#" title="click to see the messages!">
                        <span id="message-count">1</span> new message. </a>--%></span></p>
                        <div>
                            <asp:Label ID="lblComany" runat="server" Text="" Font-Bold="true"></asp:Label>
                        </div>
	                    <div id="messages-box">
		                    <h4>Messages</h4>
	                        <h5><a id="new-message" href="#">New Message</a></h5>
	                        <hr />
	                        <ul id="messages">
		                        <li class="new"><a href="#">
		                            <img alt="user" src="./new_images/icons/sender.png" />
		                            <span class="sender">John Doe</span>
		                            <span class="description">Hi this is just a description...</span>
		                            <span class="date">20 minutes ago...</span> </a>
		                        </li>
		                        <li><a href="#">
		                            <img alt="user" src="./new_images/icons/sender.png" />
		                            <span class="sender">John Doe</span>
		                            <span class="description">Hi this is just a description...</span>
		                            <span class="date">24 minutes ago...</span> </a>
		                        </li>
		                        <li><a href="#">
		                            <img alt="user" src="./new_images/icons/sender.png" />
		                            <span class="sender">John Doe</span>
		                            <span class="description">Hi this is just a description...</span>
		                            <span class="date">50 minutes ago...</span> </a>
		                        </li>
		                        <li id="last"><a id="all-messages" href="#">See all messages!
		                            <span id="unreaded">8 unreaded.</span> </a>
		                        </li>
		                    </ul>
		                    <br class="clear" />
	                    </div><!-- end messages-box -->
	                    <a id="power" href="Default.aspx" title="Logout">logout</a>
                    </div>
                    <!-- end login -->
                    <div id="nav">
	                    <ul class="sf-menu sf-navbar">
	                        <li><a id="dashboard" href="Dashboard.aspx">Dashboard</a>
	                            <ul>
		                            <li>&nbsp;
		                            </li>
	                            </ul>
	                        </li>
	                        <li><a id="Employee" href="EmployeeRegistration.aspx">Employee</a>
	                            <ul>
		                            <li><a href="EmployeeRegistration.aspx">Employee</a> </li>					                    
		                            <li><a href="OfficalProfileDetails.aspx">Profile</a></li>
		                            <li><a href="SalaryMaster.aspx">Salary Master</a></li>
		                            <%--<li><a href="LeaveAllocationEmployee.aspx">Leave Allocation</a></li>--%>
		                            <%--<li><a href="ApplyForLeave.aspx">Apply Leave</a></li>--%>						                    
		                            <li><a href="SalaryRegistration.aspx">Salary Details</a></li>
		                            <li><a href="SalaryAdvance.aspx">Advance</a></li>
		                             <li><a href="Settlement.aspx">Settlement</a></li>
		                            <li><a href="EmployeeDeactiveMode.aspx">Employee Re-Activate</a></li>				                    
	                            </ul>
	                        </li>
	                        <li><a id="Reports" href="RptDepartmentwiseEmployee.aspx">Reports</a>
	                            <ul>
		                            <li><a href="RptDepartmentwiseEmployee.aspx">Employee Details</a></li>
		                            <%--<li><a href="RptAttendanceReport.aspx">Attendance Report</a></li>--%>
		                            <%--<li><a href="RptProbationperiod.aspx">Probationary Period</a></li>
		                            <li><a href="ContractBreak.aspx">Contract Details</a></li>--%>
		                            <li><a href="RptResign.aspx">Resignation Reports</a></li>
		                            <li><a href="PaySlipGen.aspx">Payslip</a></li>
		                            <%--<li><a href="RptSalary.aspx">Salary Details</a></li>--%>
		                            <%--<li><a href="SalarySummary.aspx">Salary Summary</a></li>--%>
	                                <%--<li><a href="Incentive.aspx">Incentive Details</a></li>--%>
	                                <li><a href="RptDepartmentSalaryAbstract.aspx">Salary Abstract</a></li>
	                            </ul>
	                        </li>
	                        <li><a id="OT" href="OverTime.aspx">OT</a>
	                            <ul>
	                                <li><a href="OverTime.aspx">OT</a></li>
	                                <li><a href="RptOT.aspx">OT Report</a></li>
	                            </ul>
	                        </li>
	                        <li class="current"><a id="Master" href="MstEmployeeType.aspx">Masters</a>
	                            <ul>
		                            <li><a href="MstEmployeeType.aspx">Employee Type</a></li>  
		                            <%--<li><a href="MstLeave.aspx">Leave</a></li>
		                            <li><a href="MstProbationPeriod.aspx">Probationary Period</a></li>--%>
		                            <li><a href="UserRegistration.aspx">User</a></li>
		                            <li><a href="MSTPFESI.aspx">PF</a></li>
		                            <li><a href="MstBank.aspx">Bank</a></li>
		                            <li><a href="MstDepartment.aspx">Department</a></li>
		                            <li><a href="FrmSalarySlab.aspx">Salary Slab</a></li>
		                            <%--<li><a href="MstInsuranceCompany.aspx">Insurance</a></li>--%>
		                            <%--<li><a href="MstQualification.aspx">Qualification</a></li>--%>
		                            <li class="current"><a href="Mstcontract.aspx">Contract Master</a></li>
	                            </ul>
	                        </li>
	                        <li><a id="Bonus" href="BonusForAll.aspx">Bonus</a>
	                            <ul>
		                            <li><a href="BonusForAll.aspx">Bonus</a></li>
		                            <li><a href="StandardBonus.aspx">Group Bonus</a></li>	
		                            <li><a href="RptBonus.aspx">Bonus Report</a></li>				                    
	                            </ul>
	                        </li>
	                        <li><a id="ESI_PF" href="PFDownload.aspx">ESI & PF</a>
	                            <ul>
		                            <%--<li><a href="PFForm5.aspx">PF Form 5</a></li>--%>
		                            <li><a href="PFDownload.aspx">PF Download</a></li>
		                            <li><a href="ESIDownload.aspx">ESI Download</a></li>
		                            <li><a href="PFForm3Anew.aspx">PF Form 3A</a></li>
		                            <li><a href="PFform6A.aspx">PF Form 6A</a></li>
		                            <%--<li><a href="FOrm10.aspx">Form 10</a></li>
		                            <li><a href="PFForm3Anew.aspx">PF Form 3A</a></li>
		                            <li><a href="PFform6A.aspx">PF Form 6A</a></li>
		                            <li><a href="ESIForm7.aspx">ESI Form 7</a></li>--%>
	                            </ul>
	                        </li>
	                        <li><a id="Upload" href="UploadEmployee.aspx">Upload</a>
	                            <ul>
	                            <li><a href="UploadEmployee.aspx">Employee Upload</a></li>
		                            <li><a href="AttenanceUpload.aspx">Attenance Upload</a></li>
		                            	<li><a href="UploadOT.aspx">OT Upload</a></li>
		                            	<%--<li><a href="LoadEmpmusters.aspx">Employee Master</a></li>--%>
	                            </ul>
	                        </li>
                        </ul>
                    </div>
                    <!-- end div#nav -->
                </div>
                <!-- end div#header-top -->
                <div class="breadCrumb">
                    <ul>
                        <li class="first"><a href="#">Home</a> </li>
                        <li><a href="#">Users</a> </li>
                        <li><a href="#">Roles</a> </li>
                        <li class="last">Contract Master </li>
                    </ul>
                </div>
                <!-- end div#breadCrumb -->
                <%--<div id="search-box">
                    <form id="searchform" action="" method="get">
                        <fieldset class="search"><span>Search</span>
                        <input class="box" type="text" />
                        <button class="btn" title="Submit Search">Search</button></fieldset>
                    </form>
                </div>--%>
                <!-- end div#search-box -->
            </div>
            <!-- end header -->
            <div id="page-wrap">
                <div id="right-sidebar">
                    <div class="innerdiv">
	                    <h2 class="head">DownLoad Format</h2>
	                    <div class="innercontent clear">
		                    <h4>DownLoad Format</h4>
		                    <ul id="comments" class="tooltip-enabled">
		                    <asp:Button ID="btncontract" runat="server" Text="Contract Report" Width="200" 
                                    CssClass="button green" onclick="btncontract_Click" />
		                        <asp:Button ID="btnEmp" runat="server" Text="Employee Download" Width="200" 
                                    CssClass="button green" onclick="btnEmp_Click" />
		                        <asp:Button ID="btnatt" runat="server" Text="Attenance Download" Width="200" 
                                    CssClass="button green" onclick="btnatt_Click"/>
		                        <asp:Button ID="btnLeave" runat="server" Text="Leave Download" Width="200" 
                                    CssClass="button green" OnClick="btnLeave_Click" />
                                    <asp:Button ID="btnOT" runat="server" Text="OT Download" Width="200" 
                                    CssClass="button green" onclick="btnOT_Click" />
                                    <asp:Button ID="btnSal" runat="server" Text="Salary Download" Width="200" 
                                    CssClass="button green" onclick="btnSal_Click" />
		                        
		                    </ul>
		                    <%--<img src="new_images/icons/right_img.jpg" alt="" width="230"/>--%>
	                    </div>
                    </div>
                    <div class="clear"></div>                    
                </div>
	                <!-- end right-sidebar -->                
	                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
	                    <ContentTemplate>
	                        <div id="main-content">
                                <br /><br /><br /><br /> 
	                            <div class="clear_body">
                                    <div class="innerdiv">
                                        <div class="innercontent">
                                            <!-- tab "panes" -->				                
                                                <div>
                                                    <table class="full">
                                                        <thead>
                                                            <tr align="center">
                                                                <th colspan="4" style="text-align:center;"><h4>CONTRACT MASTER</h4></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr align="center">
                                                                <td colspan="4">
                                                                    <asp:RadioButtonList ID="rbtype" runat="server" AutoPostBack ="true" 
                                                                        RepeatColumns="3" Font-Bold="true" 
                                                                        onselectedindexchanged="rbtype_SelectedIndexChanged" >
                                                                        <asp:ListItem Text="Years" Value="1" Selected="False" ></asp:ListItem>
                                                                        <asp:ListItem Text="Months" Value="2" Selected="False"></asp:ListItem>
                                                                        <asp:ListItem Text="Days" Value="3" Selected="False" ></asp:ListItem>
                                                                    </asp:RadioButtonList>                  
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="lblName" runat="server" Text="Contract Name" Font-Bold="true" ></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txtname" runat="server" ></asp:TextBox>
                                                                    <cc1:FilteredTextBoxExtender ID="filter1" runat="server" FilterMode="ValidChars" FilterType="Custom" TargetControlID="txtname" ValidChars="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789- " ></cc1:FilteredTextBoxExtender>
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="Label1" runat="server" Text="Not Eligible Months" Font-Bold="true" ></asp:Label>
                                                                </td>
                                                                <td colspan="1">
                                                                    <asp:TextBox ID="txtnotEligible" runat="server" Width="50" ></asp:TextBox>                                                                    
                                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterMode="ValidChars" FilterType="Custom" TargetControlID="txtnotEligible" ValidChars="0123456789" ></cc1:FilteredTextBoxExtender>
                                                                    <asp:Label ID="Label2" runat="server" Text="Not Eligible Days" Font-Bold="true" Visible="false" ></asp:Label>
                                                                    <asp:TextBox ID="txtNotEDays" runat="server" Text="0" Visible="false" ></asp:TextBox>                                                                    
                                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterMode="ValidChars" FilterType="Custom" TargetControlID="txtNotEDays" ValidChars="0123456789." ></cc1:FilteredTextBoxExtender>
                                                                </td>
                                                            </tr> 
                                                            <asp:Panel ID="panelyear" runat="server" Visible="false" >                                                        
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="lblyr" runat="server" Text="No of years" Font-Bold="true" ></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txtyr" runat="server" MaxLength="2" ></asp:TextBox>
                                                                    <cc1:FilteredTextBoxExtender ID="Filter2" runat="server" FilterMode="ValidChars" FilterType="Custom" TargetControlID="txtyr" ValidChars="0123456789" ></cc1:FilteredTextBoxExtender>
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="lblyearDays" runat="server" Text="No of Days" Font-Bold="true" ></asp:Label>
                                                                </td>
                                                                <td colspan="1">
                                                                    <asp:TextBox ID="txtyrDays" runat="server" ></asp:TextBox>
                                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterMode="ValidChars" FilterType="Custom" TargetControlID="txtyrDays" ValidChars="0123456789" ></cc1:FilteredTextBoxExtender>
                                                                </td>                                                                
                                                            </tr>
                                                            </asp:Panel>
                                                            <asp:Panel ID="panelMonths" runat="server" Visible="false" >
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="lblMonth" runat="server" Text="No of Months" Font-Bold="true" ></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txtMonth" runat="server" MaxLength="2" ></asp:TextBox>
                                                                    <cc1:FilteredTextBoxExtender ID="Filter3" runat="server" FilterMode="ValidChars" FilterType="Custom" TargetControlID="txtMonth" ValidChars="0123456789" ></cc1:FilteredTextBoxExtender>
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="lbldaysperMpnth" runat="server" Text="No of Days per Month" Font-Bold="true" ></asp:Label>
                                                                </td>
                                                                <td  colspan="1">
                                                                    <asp:TextBox ID="txtdaysperMonth" runat="server" ></asp:TextBox>
                                                                    <cc1:FilteredTextBoxExtender ID="Filter4" runat="server" FilterMode="ValidChars" FilterType="Custom" TargetControlID="txtdaysperMonth" ValidChars="0123456789" ></cc1:FilteredTextBoxExtender>
                                                                </td>
                                                            </tr>
                                                            </asp:Panel>
                                                            <asp:Panel ID="PanelDays" runat="server" Visible="false" >
                                                            
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="lblDays" runat="server" Text="No of Days" Font-Bold="true" ></asp:Label>
                                                                    </td>
                                                                    <td colspan="3">
                                                                        <asp:TextBox ID="txtdays" runat="server" MaxLength="5" ></asp:TextBox>
                                                                        <cc1:FilteredTextBoxExtender ID="Filter5" runat="server" FilterMode="ValidChars" FilterType="Custom" TargetControlID="txtdays" ValidChars="0123456789" ></cc1:FilteredTextBoxExtender>
                                                                    </td>
                                                                </tr>
                                                            </asp:Panel>
                                                            <tr align="center">
                                                                <td colspan="4">
                                                                    <asp:Button ID="btnMont" runat="server" Text="Save" CssClass="button green" 
                                                                        onclick="btnMont_Click" />
                                                                    <asp:Button ID="btnclear" runat="server" Text="Clear" CssClass="button green" 
                                                                        onclick="btnclear_Click" />
                                                                </td>
                                                            </tr>
                                                            <tr align="center">
                                                                <td colspan="4">
                                                                    <asp:GridView ID="gvcontract" runat="server" AutoGenerateColumns="false" 
                                                                        onrowcancelingedit="gvcontract_RowCancelingEdit" 
                                                                        onrowdeleting="gvcontract_RowDeleting" 
                                                                        onrowediting="gvcontract_RowEditing" 
                                                                        onselectedindexchanged="gvcontract_SelectedIndexChanged" >
                                                                        <Columns>
                                                                            <asp:TemplateField>
                                                                                <HeaderTemplate>Contract Code</HeaderTemplate>
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="lblconCd" runat="server" Text='<%# Eval("ContractCode") %>'></asp:Label>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField>
                                                                                <HeaderTemplate>Contract Name</HeaderTemplate>
                                                                                <ItemTemplate>
                                                                                <asp:Label ID="lblconName" runat="server" Text='<%# Eval("ContractName") %>'></asp:Label>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField>
                                                                                <HeaderTemplate>Contract Type</HeaderTemplate>
                                                                                <ItemTemplate>
                                                                                <asp:Label ID="lblConType" runat="server" Text='<%# Eval("ContractTyp") %>'></asp:Label>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                             <asp:CommandField ButtonType="Button" CancelText="Cancel" 
                                                                                ControlStyle-CssClass="button green" DeleteText="Delete" EditText="Edit" 
                                                                                ShowDeleteButton="true" ShowEditButton="true" UpdateText="Update" />
                                                                        </Columns>
                                                                    </asp:GridView>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                 <%--Delete Button Code--%>
                                <asp:Button ID="cmdOverriteGrade" runat="server" style="display:none;" OnClick="cmdOverriteGrade_Click"/> 
                                                       
	                    </ContentTemplate>
	                </asp:UpdatePanel>
            </div>
            <!-- end page-wrap -->
            <div id="footer">
                <ul>
                    <li>Copyright &copy; 2012. All rights reserved.</li>
                    <li>Powered by <a href="http://www.altius.co.in" target="_blank">Altius Infosystems</a></li>
                </ul>
            </div>
            <!-- end footer -->
        </form>
    </body>
</html>

