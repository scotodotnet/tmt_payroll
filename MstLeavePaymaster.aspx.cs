﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class MstLeavePaymaster : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    string SessionAdmin;
    protected void Page_Load(object sender, EventArgs e)
    {
        SessionAdmin = Session["Isadmin"].ToString();
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        string ss = Session["UserId"].ToString();
        if (!IsPostBack)
        {
            DataTable dt = new DataTable();
            dt = objdata.MstLeavePayMasterDisplay();
            txtleaveid.Text = dt.Rows[0]["LeaveID"].ToString();
            txtleavepaystaff.Text = dt.Rows[0]["LeavePaydayStaff"].ToString();
        }
    }
    protected void btnsave_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        string leaveid=txtleaveid.Text;
        int dd = leaveid.Length;
        
        if (txtleaveid.Text == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Leave ID');", true);
            ErrFlag = false;
        }
        else if (txtleavepaystaff.Text == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Leave in Days');", true);
            ErrFlag = false;
        }
        if (!ErrFlag)
        {
            objdata.UpdateLeaveDaysMaster(txtleaveid.Text, txtleavepaystaff.Text);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Your Update Successfully...');", true);
        }
    }
}
