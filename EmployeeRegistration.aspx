<%@ Page Language="C#" AutoEventWireup="true" CodeFile="EmployeeRegistration.aspx.cs" Inherits="EmployeeRegistration_1" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
    <head runat="server">
        <title>Payroll Management Systems</title>
        <link href="new_css/main.css" rel="stylesheet" type="text/css" />
        <link href="new_css/colors/dark_blue.css" rel="stylesheet" type="text/css" />
        <link href="new_css/superfish.css" rel="stylesheet" type="text/css" />

        <script src="new_js/jquery-1.4.2.min.js" type="text/javascript"></script>
        <script src="new_js/jquery.hoverintent.minified.js" type="text/javascript"></script>
        <script src="new_js/superfish.js" type="text/javascript"></script>
        <script src="new_js/jquery.tools.min.js" type="text/javascript"></script>
        <script src="new_js/jquery.simplemodal.js" type="text/javascript"></script>
        
        <script src="new_js/jquery.notifybar.js" type="text/javascript"></script>
        <script src="new_js/jquery.tipsy.js" type="text/javascript"></script>
        <script src="new_js/enhance.js" type="text/javascript"></script>
        <script src="new_js/excanvas.js" type="text/javascript"></script>
        <script src="new_js/visualize.jquery.js" type="text/javascript"></script>
        <script src="new_js/jquery.collapsible.js" type="text/javascript"></script>
        <script src="new_js/jquery.autosuggest.packed.js" type="text/javascript"></script>
        <script src="new_js/platinum-admin.js" type="text/javascript"></script>
        <%--<script type ="text/jscript" >
        function GoBack() {
            window.history.forward();
        }
   </script>--%>
   <script type="text/javascript">
        function Showalert() {
        alert('Call JavaScript function from codebehind');
        }
        </script>
        <style>
            .sf-navbar li ul 
            {
	            width: 1100px; /*IE6 soils itself without this*/
	            margin: 0px auto;
	            margin-left:-20em;	            
            }
        </style>
    </head>
    <body>
        <form id="form1" runat="server">
            <cc1:ToolkitScriptManager runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true"
                ID="ScriptManager1" EnablePartialRendering="true">
            </cc1:ToolkitScriptManager>
            <div id="header">
                <div id="header-top">
                    <div id="logo">
	                    <h1>&nbsp;</h1>
	                    <span id="slogan">&nbsp;</span> 
	                </div>
                    <!-- end logo -->
                    <div id="login-info">
                        <img id="profile" alt="profile" src="./new_images/icons/user.png" />
                        <p id="top"><span id="name"><asp:Label ID="lblusername" runat="server"></asp:Label></span><br />
                        <span id="links">
                        <%--<a id="message-link" href="#" title="click to see the messages!">
                        <span id="message-count">1</span> new message. </a>--%></span></p>
                        <div>
                            <asp:Label ID="lblComany" runat="server" Text="" Font-Bold="true"></asp:Label>
                        </div>
	                    <div id="messages-box">
		                    <h4>Messages</h4>
	                        <h5><a id="new-message" href="#">New Message</a></h5>
	                        <hr />
	                        <ul id="messages">
		                        <li class="new"><a href="#">
		                            <img alt="user" src="./new_images/icons/sender.png" />
		                            <span class="sender">John Doe</span>
		                            <span class="description">Hi this is just a description...</span>
		                            <span class="date">20 minutes ago...</span> </a>
		                        </li>
		                        <li><a href="#">
		                            <img alt="user" src="./new_images/icons/sender.png" />
		                            <span class="sender">John Doe</span>
		                            <span class="description">Hi this is just a description...</span>
		                            <span class="date">24 minutes ago...</span> </a>
		                        </li>
		                        <li><a href="#">
		                            <img alt="user" src="./new_images/icons/sender.png" />
		                            <span class="sender">John Doe</span>
		                            <span class="description">Hi this is just a description...</span>
		                            <span class="date">50 minutes ago...</span> </a>
		                        </li>
		                        <li id="last"><a id="all-messages" href="#">See all messages!
		                            <span id="unreaded">8 unreaded.</span> </a>
		                        </li>
		                    </ul>
		                    <br class="clear" />
	                    </div><!-- end messages-box -->
	                    <a id="power" href="default.aspx" title="Logout">logout</a>
                    </div>
                    <!-- end login -->
                    <div id="nav">
	                    <ul class="sf-menu sf-navbar">
	                        <li><a id="dashboard" href="Dashboard.aspx">Dashboard</a>
	                            <ul>
		                            <li>&nbsp;
		                            </li>
	                            </ul>
	                        </li>
	                        <li class="current"><a id="Employee" href="EmployeeRegistration.aspx">Employee</a>
	                            <ul>
		                            <li><a href="EmployeeRegistration.aspx">Employee </a> </li>					                    
		                            <li><a href="OfficalProfileDetails.aspx">Profile</a></li>
		                            <li><a href="SalaryMaster.aspx">Salary Master</a></li>
		                            <%--<li><a href="LeaveAllocationEmployee.aspx">Leave Allocation</a></li>--%>
		                            <%--<li><a href="ApplyForLeave.aspx">Apply Leave</a></li>--%>						                    
		                            <li><a href="SalaryRegistration.aspx">Salary Details</a></li>
		                            <li><a href="SalaryAdvance.aspx">Advance</a></li>
		                             <li><a href="Settlement.aspx">Settlement</a></li>
		                            <li><a href="EmployeeDeactiveMode.aspx">Employee Re-Activate</a></li>    				                    
	                            </ul>
	                        </li>
	                        <li><a id="Reports" href="RptDepartmentwiseEmployee.aspx">Reports</a>
	                            <ul>
		                            <li><a href="RptDepartmentwiseEmployee.aspx">Employee Details</a></li>
		                            <%--<li><a href="RptAttendanceReport.aspx">Attendance Report</a></li>--%>
		                            <%--<li><a href="RptProbationperiod.aspx">Probationary Period</a></li>
		                            <li><a href="ContractBreak.aspx">Contract Details</a></li>--%>
		                            <li><a href="RptResign.aspx">Resignation Reports</a></li>
		                            <li><a href="PaySlipGen.aspx">Payslip</a></li>
		                            <%--<li><a href="RptSalary.aspx">Salary Details</a></li>--%>
		                            <%--<li><a href="SalarySummary.aspx">Salary Summary</a></li>--%>
	                                <%--<li><a href="Incentive.aspx">Incentive Details</a></li>--%>
	                                <li><a href="RptDepartmentSalaryAbstract.aspx">Salary Abstract</a></li>
	                            </ul>
	                        </li>
	                        <li><a id="OT" href="OverTime.aspx">OT</a>
	                            <ul>
	                                <li><a href="OverTime.aspx">OT</a></li>
	                                <li><a href="RptOT.aspx">OT Report</a></li>
	                            </ul>
	                        </li>
	                        <li><a id="Master" href="MstEmployeeType.aspx">Masters</a>
	                            <ul>
		                            <li><a href="MstEmployeeType.aspx">Employee Type</a></li>
		                            <li><a href="UserRegistration.aspx">User</a></li>
		                            <li><a href="MSTPFESI.aspx">PF</a></li>
		                            <li><a href="MstBank.aspx">Bank</a></li>
		                            <li><a href="MstDepartment.aspx">Department</a></li>
		                            <li><a href="MstDAfix.aspx">DA Arrears FIX</a></li>
	                            </ul>
	                        </li>
	                        <li><a id="Bonus" href="BonusForAll.aspx">Bonus</a>
	                            <ul>
		                            <li><a href="BonusForAll.aspx">Bonus</a></li>
		                            <li><a href="StandardBonus.aspx">Group Bonus</a></li>
		                            <li><a href="RptBonus.aspx">Bonus Report</a></li>					                    
	                            </ul>
	                        </li>
	                        <li><a id="ESI_PF" href="PFDownload.aspx">ESI & PF</a>
	                            <ul>
		                            <%--<li><a href="PFForm5.aspx">PF Form 5</a></li>--%>
		                            <li><a href="PFDownload.aspx">PF Download</a></li>
		                            <li><a href="ESIDownload.aspx">ESI Download</a></li>
		                            <li><a href="PFForm3Anew.aspx">PF Form 3A</a></li>
		                            <li><a href="PFform6A.aspx">PF Form 6A</a></li>
		                            <%--<li><a href="FOrm10.aspx">Form 10</a></li>
		                            <li><a href="PFForm3Anew.aspx">PF Form 3A</a></li>
		                            <li><a href="PFform6A.aspx">PF Form 6A</a></li>
		                            <li><a href="ESIForm7.aspx">ESI Form 7</a></li>--%>
	                            </ul>
	                        </li>
	                        <li><a id="Upload" href="UploadEmployee.aspx">Upload</a>
	                            <ul>
	                            <li><a href="UploadEmployee.aspx">Employee Upload</a></li>
		                            <li><a href="AttenanceUpload.aspx">Attenance Upload</a></li>
		                            <li><a href="UploadOT.aspx">OT Upload</a>/li>
		                            <li><a href="SalaryUpload_Demo.aspx">Salary Upload</a></li>
		                            	<%--<li><a href="LoadEmpmusters.aspx">Employee Master</a></li>--%>
	                            </ul>
	                        </li>
	                        
	                        
                        </ul>
                    </div>
                    <!-- end div#nav -->
                </div>
                <!-- end div#header-top -->
                <div class="breadCrumb">
                    <ul>
                        <li class="first"><a href="#">Home</a> </li>
                        <li><a href="#">Users</a> </li>
                        <li><a href="#">Roles</a> </li>
                        <li class="last">Employee Registration </li>
                    </ul>
                </div>
                <!-- end div#breadCrumb -->
                <%--<div id="search-box">
                    <form id="searchform" action="" method="get">
                        <fieldset class="search"><span>Search</span>
                        <input class="box" type="text" />
                        <button class="btn" title="Submit Search">Search</button></fieldset>
                    </form>
                </div>--%>
                <!-- end div#search-box -->
            </div>
            <!-- end header -->
            <div id="page-wrap">
                <div id="right-sidebar">
                    <div class="innerdiv">
	                    <h2 class="head">DownLoad Format</h2>
	                    <div class="innercontent clear">
		                    <h4>DownLoad Format</h4>
		                    <ul id="comments" class="tooltip-enabled">
		                    <asp:Button ID="btncontract" runat="server" Text="Contract Report" Width="200" 
                                    CssClass="button green" onclick="btncontract_Click" />
		                        <asp:Button ID="btnEmp" runat="server" Text="Employee Download" Width="200" 
                                    CssClass="button green" onclick="btnEmp_Click" />
		                        <asp:Button ID="btnatt" runat="server" Text="Attenance Download" Width="200" 
                                    CssClass="button green" onclick="btnatt_Click"/>
		                        <asp:Button ID="btnLeave" runat="server" Text="Leave Download" Width="200" 
                                    CssClass="button green" OnClick="btnLeave_Click" />
                                    <asp:Button ID="btnOT" runat="server" Text="OT Download" Width="200" 
                                    CssClass="button green" onclick="btnOT_Click" />
                                    <asp:Button ID="btnSal" runat="server" Text="Salary Download" Width="200" 
                                    CssClass="button green" onclick="btnSal_Click" />
		                        
		                    </ul>
		                    <%--<img src="new_images/icons/right_img.jpg" alt="" width="230"/>--%>
	                    </div>
                    </div>
                    <div class="clear"></div>                    
                </div>
	            <!-- end right-sidebar -->
	            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
	                <Triggers>
	                    <asp:PostBackTrigger ControlID="btnDownload" />
	                </Triggers>
                    <ContentTemplate>
                        <div id="main-content">                    
                            <div>
                                
                                <table width="100%" style="border:solid 1px #000;">
                                <thead>
                                <tr>
                                    <th colspan="3" style="text-align:center;"><h5>Employee Registeration</h5></th>
                                </tr>
                                </thead>
                                <tbody>
                                
                                    <tr>
                                        <td style="text-align:right;"><asp:Label ID="lblcategory" runat="server" Text="Category" Font-Bold="true"></asp:Label></td>
                                        <td><asp:DropDownList ID="ddlcategory" runat="server" Width="150"
                                                        onselectedindexchanged="ddlcategory_SelectedIndexChanged" 
                                                        AutoPostBack="True" Height="22"></asp:DropDownList>
                                        </td>
                                        <td style="text-align:left;">
                                            <asp:Button ID="btnclick" runat="server" Text="Click" Width="78" onclick="btnclick_Click" 
                                                CssClass="button green"/>
                                                <asp:Button ID="btnDownLoad" runat="server" Text="Download Employee Details" Width="160" 
                                                                     CssClass="button green" onclick="btnDownLoad_Click" />
                                        </td>
                                    </tr>
                                    
                                </table>	
                                <div class="clear"></div>
                            </div>
                            <div class="clear_body">
                                <div class="innerdiv">
	                                <div class="innercontent">
		                                <!-- tab "panes" -->				                
			                            <div>
			                            <table class="full">
			                            <asp:Panel ID="panelError" runat="server" Visible="false">
			                                <tr align="center" >
                                                
                                                    <td colspan="4" align="center">
                                                        <asp:Label ID="lblError" runat="server" Font-Bold="true" Font-Size="16px" ForeColor="Red" ></asp:Label>
                                                    </td>
                                                    
                                                </tr>	
                                        </asp:Panel>
                                            
			                            </table>
			                            
			                                <asp:Panel ID="Panelstafflabor" runat="server" Visible="false">
				                                <table class="full">
				                                    <thead>
				                                        <tr>
					                                        <th colspan="4" style="text-align:center;"><h5>Personal Details</h5></th>                       
				                                        </tr>
				                                    
				                                    <tbody class="tooltip-enabled">
				                                    
				                                        <tr id="PanelEmployeeSearch" runat="server" visible="false">
				                                            <td>
				                                                <asp:Label ID="lblEmpNo" runat="server" Text="Employee No" Font-Bold="true"></asp:Label>
				                                                <asp:TextBox ID="txtempNo" runat="server" ontextchanged="txtempNo_TextChanged" Width="100"></asp:TextBox>
				                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR4" runat="server" FilterMode="ValidChars"
                                                                    FilterType="UppercaseLetters,LowercaseLetters,Custom" TargetControlID="txtempNo"
                                                                    ValidChars="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789">
                                                                </cc1:FilteredTextBoxExtender>
                                                                <%--<asp:DropDownList ID="ddlEmpNo" runat="server" Width="180" Height="25" 
                                                                    TabIndex="26" AutoPostBack="true" 
                                                                    onselectedindexchanged="ddlEmpNo_SelectedIndexChanged"></asp:DropDownList>--%>
                                                                
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblExistsearch" runat="server" Text="Existing No" Font-Bold="true" ></asp:Label>
                                                                <asp:TextBox ID="TxtExistSearch" runat="server" 
                                                                        ontextchanged="TxtExistSearch_TextChanged" Width="70"></asp:TextBox>
                                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR10" runat="server" FilterMode="ValidChars"
                                                                FilterType="UppercaseLetters,LowercaseLetters,Custom" TargetControlID="TxtExistSearch"
                                                                ValidChars="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"></cc1:FilteredTextBoxExtender>
                                                                <asp:Button ID="btnsearch" runat="server" Text="Search" OnClick="btnsearch_Click" CausesValidation="false" 
				                                                    CssClass="button green"/>
				                                                    
				                                                    <%--<asp:DropDownList ID="ddlExist" runat=server Width="180" Height="25" 
                                                                    TabIndex="26" AutoPostBack="true" 
                                                                    onselectedindexchanged="ddlExist_SelectedIndexChanged" ></asp:DropDownList>--%>
				                                                    
				                                            </td>
				                                            <td colspan="2" style="text-align:left;">
				                                                <asp:Label ID="lblregistrationNo" runat="server" Text="Registration No" Font-Bold="true" Visible="false" ></asp:Label>
                                                                <asp:Label ID="RegistrationNo" runat="server" Text="" Font-Bold="true" Visible="false" ></asp:Label>
                                                            </td>                                            
				                                        </tr>
		                                        		<asp:Panel ID="panelsuccess" runat="server" Visible="false" BorderColor="Black">
		                                        		    <tr style="background-color:Olive">
		                                        		        <td align="left" colspan="4">
		                                        		            <asp:Label ID="lblhd" runat="server" Text="Altius InfoSystems" ForeColor="White" Font-Bold="true" Font-Size="14px" ></asp:Label>
		                                        		        </td>
		                                        		    </tr>
		                                        		    <tr align="center">
		                                        		        <td colspan="4">
		                                        		            <asp:Label ID="lbloutput" runat="server" Font-Bold="true" Font-Size="16px"></asp:Label>
		                                        		           
		                                        		        </td>
		                                        		    </tr>
		                                        		    <tr align="center">
		                                        		        <td colspan="4">
		                                        		            <asp:Button ID="btnok" runat="server" Text="Ok" onclick="btnok_Click" Width="75" Height="28" CssClass="button green" />
		                                        		            
		                                        		        </td>
		                                        		        
		                                        		    </tr>
		                                        		</asp:Panel>		                                
				                                        <tr>
					                                        <td><asp:Label ID="lblempname" runat="server" Text="Employee Name" Font-Bold="true"></asp:Label></td>
					                                        <td>
					                                            <asp:TextBox ID="txtempname" runat="server" TabIndex="1" Width="200"></asp:TextBox>
					                                            <cc1:FilteredTextBoxExtender ID="FilterTextboxExtenderempname" runat="server" FilterMode="ValidChars"
                                                                    FilterType="UppercaseLetters,LowercaseLetters,Custom" TargetControlID="txtempname"
                                                                    ValidChars=". ">
                                                                </cc1:FilteredTextBoxExtender>
                                                                <asp:RequiredFieldValidator ID="Req_txtEmpaname" runat="server" ControlToValidate="txtempname" ErrorMessage="*">
                                                                *
                                                                </asp:RequiredFieldValidator>
					                                        </td>
					                                        <td><asp:Label ID="lblempfname" runat="server" Text="Father / Husband Name" Font-Bold="true"></asp:Label></td>
					                                        <td>
					                                            <asp:TextBox ID="txtempfname" runat="server" TabIndex="2" Width="200"></asp:TextBox>
                                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR_FatherName" runat="server"
                                                                    FilterMode="ValidChars" FilterType="UppercaseLetters,LowercaseLetters,Custom"
                                                                    TargetControlID="txtempfname" ValidChars=". ">
                                                                </cc1:FilteredTextBoxExtender>
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtempfname">*</asp:RequiredFieldValidator>
					                                        </td>
				                                        </tr>
				                                        <tr>
				                                            <td><asp:Label ID="lblgender" runat="server" Text="Gender" Font-Bold="true"></asp:Label></td>
				                                            <td>
				                                                <asp:RadioButtonList ID="rbtngender" runat="server" RepeatColumns="2" 
                                                                    TabIndex="3" Width="210" 
                                                                    onselectedindexchanged="rbtngender_SelectedIndexChanged">
                                                                    <asp:ListItem Selected="True" Text="Male" Value="1"></asp:ListItem>
                                                                    <asp:ListItem Selected="False" Text="Female" Value="2"></asp:ListItem>
                                                                </asp:RadioButtonList>
				                                            </td>
				                                            <td><asp:Label ID="lbldob" runat="server" Text="Date of Birth" Font-Bold="true"></asp:Label></td>
				                                            <td>
				                                                <asp:TextBox ID="txtdob" runat="server" Width="200" MaxLength="10" TabIndex="4"></asp:TextBox>
                                                                <cc1:CalendarExtender ID="Caleder_txtdob" runat="server" TargetControlID="txtdob"
                                                                    Format="dd-MM-yyyy" CssClass="orange">
                                                                </cc1:CalendarExtender>
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtdob">*</asp:RequiredFieldValidator>
				                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR11" runat="server"
                                                                    FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                    TargetControlID="txtdob" ValidChars="0123456789/-">
                                                                </cc1:FilteredTextBoxExtender>
				                                            </td>
				                                        </tr>
				                                        <tr>
				                                            <td>
				                                                <asp:Label ID="lblMrtialstatus" runat="server" Text="Martial Status" Font-Bold="true"></asp:Label>
				                                            </td>
				                                            <td>
				                                                <asp:RadioButtonList ID="Rbmartial" runat="server" RepeatColumns="2">
				                                                <asp:ListItem Selected="True" Text="Unmarried" Value="U"></asp:ListItem>
				                                                <asp:ListItem Selected="False" Text="Married" Value="M" ></asp:ListItem>
				                                                </asp:RadioButtonList>
				                                            </td>
				                                            <td>
				                                                <asp:Label ID="lblinitial" runat="server" Text="Father/Husband" Font-Bold="true" ></asp:Label>
				                                            </td>
				                                            <td>
				                                                <asp:RadioButtonList ID="Rbfather" runat="server" RepeatColumns="2" >
				                                                    <asp:ListItem Selected="True" Text="Father" Value="F" ></asp:ListItem>
				                                                    <asp:ListItem Selected="False" Text="Husband" Value="S"></asp:ListItem>
				                                                </asp:RadioButtonList>
				                                            </td>
				                                        </tr>
				                                        <tr>
				                                            <td><asp:Label ID="lblPSadd1" runat="server" Text="Address1" Font-Bold="true"></asp:Label></td>
				                                            <td>
				                                                <asp:TextBox ID="txtPSAdd1" runat="server" TabIndex="5" Width="200"></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtPSAdd1">*</asp:RequiredFieldValidator>
				                                            </td>
				                                            <td><asp:Label ID="lblPSDistrict" runat="server" Text="District" Font-Bold="true"></asp:Label></td>
				                                            <td>
				                                                <asp:DropDownList ID="ddlPSDistrict" runat="server" Width="215" Height="25" AutoPostBack="True"
                                                                    OnSelectedIndexChanged="ddlPSDistrict_SelectedIndexChanged" TabIndex="8" Visible="false">
                                                                </asp:DropDownList>
                                                                <asp:TextBox ID="txtDistrict" runat="server" ></asp:TextBox>
                                                                 <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender13" runat="server" TargetControlID="txtDistrict"
                                                                    FilterMode="ValidChars" FilterType="UppercaseLetters,LowercaseLetters,Custom" ValidChars="">
                                                                </cc1:FilteredTextBoxExtender>
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlPSDistrict"
                                                                    InitialValue="0" ErrorMessage="*">*</asp:RequiredFieldValidator>
				                                            </td>
				                                        </tr>
				                                        <tr>
				                                            <td><asp:Label ID="lblPSAdd2" runat="server" Text="Address2" Font-Bold="true"></asp:Label></td>
				                                            <td><asp:TextBox ID="txtPSAdd2" runat="server" TabIndex="6" Width="200"></asp:TextBox></td>
				                                            <td><asp:Label ID="lblPSTaluk" runat="server" Text="Taluk" Font-Bold="true"></asp:Label></td>
				                                            <td>
				                                                <asp:DropDownList ID="ddlPSTaluk" runat="server" Width="215" Height="25" TabIndex="9" Visible="false">
                                                                </asp:DropDownList>
                                                                <asp:TextBox ID="txtTaluk" runat="server" ></asp:TextBox>
                                                                 <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender12" runat="server" TargetControlID="txtTaluk"
                                                                    FilterMode="ValidChars" FilterType="UppercaseLetters,LowercaseLetters,Custom" ValidChars="">
                                                                </cc1:FilteredTextBoxExtender>
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="ddlPSTaluk"
                                                                    InitialValue="0" ErrorMessage="*">*</asp:RequiredFieldValidator>
				                                            </td>
				                                        </tr>
				                                        <tr>
				                                            <td>&nbsp;</td>
                                                            <td>&nbsp;</td>
                                                            <td><asp:Label ID="lblpsstate" runat="server" Text="State" Font-Bold="true"></asp:Label></td>
                                                            <td>
                                                                <asp:TextBox ID="txtpsstate" runat="server" Text="Tamilnadu" Width="200"></asp:TextBox>
                                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" TargetControlID="txtpsstate"
                                                                    FilterMode="ValidChars" FilterType="UppercaseLetters,LowercaseLetters,Custom" ValidChars="">
                                                                </cc1:FilteredTextBoxExtender>
                                                                   <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="txtpsstate"
                                                                     ErrorMessage="*">*</asp:RequiredFieldValidator>
                                                            </td>                                                    
				                                        </tr>
				                                        <tr>
				                                            <td><asp:Label ID="lblphone" runat="server" Text="Phone" Font-Bold="true"></asp:Label></td>
				                                            <td>
				                                                <asp:TextBox ID="txtphone" runat="server" MaxLength="5" TabIndex="19" 
                                                                    Width="50" ontextchanged="txtphone_TextChanged" AutoPostBack="True"></asp:TextBox>
                                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" runat="server" TargetControlID="txtphone"
                                                                    FilterMode="ValidChars" FilterType="Custom" ValidChars="1234567890"></cc1:FilteredTextBoxExtender>
                                                                <asp:Label ID="lblPhonesplit" runat="server" Text="-" Font-Bold="true"></asp:Label>
                                                                <asp:TextBox ID="txtPhone1" runat="server" MaxLength="10" TabIndex="20" Width="122" ></asp:TextBox>
                                                                <cc1:FilteredTextBoxExtender ID="FilterTextboxexteder" runat="server" TargetControlID="txtphone1"
                                                                    FilterMode="ValidChars" FilterType="Custom" ValidChars="1234567890">
                                                                </cc1:FilteredTextBoxExtender>
				                                            </td>
				                                            <td><asp:Label ID="lblmobile" runat="server" Text="Mobile" Font-Bold="true"></asp:Label></td>
				                                            <td>
				                                                <asp:TextBox ID="txtMobileCode" runat="server" Text="+91" Width="40" TabIndex="21" 
                                                                    MaxLength="3" ontextchanged="txtMobileCode_TextChanged"></asp:TextBox>
                                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender9" runat="server" TargetControlID="txtMobileCode"
                                                                    FilterMode="ValidChars" FilterType="Custom" ValidChars="0123456789">
                                                                </cc1:FilteredTextBoxExtender>
                                                                <asp:TextBox ID="txtmobile" runat="server" MaxLength="10" TabIndex="22" Width="142"></asp:TextBox>
                                                                <cc1:FilteredTextBoxExtender ID="FilterTextboxMobile" runat="server" TargetControlID="txtmobile"
                                                                    FilterMode="ValidChars" FilterType="Custom" ValidChars="0123456789">
                                                                </cc1:FilteredTextBoxExtender>
				                                            </td>
				                                        </tr>
				                                        <tr>
				                                            <td><asp:Label ID="lblppno" runat="server" Text="Passport No" Font-Bold="true"></asp:Label></td>
				                                            <td>
				                                                <asp:TextBox ID="txtppno" runat="server" CssClass="text" TabIndex="23" Width="200" MaxLength="20"></asp:TextBox>
                                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR1" TargetControlID="txtppno"
                                                                    runat="server" FilterMode="ValidChars" FilterType="UppercaseLetters,LowercaseLetters,Numbers,Custom"
                                                                    ValidChars="">
                                                                </cc1:FilteredTextBoxExtender>
				                                            </td>
				                                            <td><asp:Label ID="lbldrivingno" runat="server" Text="Driving License No" Font-Bold="true"></asp:Label></td>                      
				                                            <td>
				                                                <asp:TextBox ID="txtdrivingno" runat="server" CssClass="text" TabIndex="24" Width="200" MaxLength="20"></asp:TextBox>
                                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR2" TargetControlID="txtdrivingno"
                                                                    runat="server" FilterMode="ValidChars" FilterType="UppercaseLetters,LowercaseLetters,Numbers,Custom"
                                                                    ValidChars="-">
                                                                </cc1:FilteredTextBoxExtender>
				                                            </td>
				                                        </tr>
					                                </tbody>
					                                <thead>
				                                        <tr>
					                                        <th colspan="4" style="text-align:center;"><h5>Education Details</h5></th>
				                                        </tr>
				                                    </thead>
				                                    <tbody>
				                                        <tr id="PanelSelectEducated" runat="server" visible="false" align="center">
				                                            <td colspan="4">
				                                                <asp:RadioButtonList ID="rbtneducateduneducated" runat="server" Width="180" RepeatColumns="2"
                                                                    AutoPostBack="true" TabIndex="25" OnSelectedIndexChanged="rbtneducateduneducated_SelectedIndexChanged">
                                                                    <asp:ListItem Text="Educated" Value="1" Selected="False"></asp:ListItem>
                                                                    <asp:ListItem Text="UnEducated" Value="2" Selected="True"></asp:ListItem>
                                                                </asp:RadioButtonList>
				                                            </td>
				                                        </tr>
				                                        <tr id="PanelEducated" runat="server" visible="false">
				                                            <td><asp:Label ID="lblqualif" runat="server" Text="Qualification" Font-Bold="true"></asp:Label></td>
				                                            <td>
				                                                <asp:DropDownList ID="ddlqualification" runat="server" Width="215" Height="25" TabIndex="26">
                                                                </asp:DropDownList>
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="ddlqualification"
                                                                    InitialValue="0" ErrorMessage="*">*</asp:RequiredFieldValidator>
				                                            </td>
				                                            <td><asp:Label ID="lblunive" runat="server" Text="Qualification" Font-Bold="true"></asp:Label></td>
				                                            <td>
				                                                <asp:TextBox ID="txtuniversity" runat="server" TabIndex="27" Width="200"></asp:TextBox>
                                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR3" TargetControlID="txtuniversity" runat="server"
                                                                    FilterMode="ValidChars" FilterType="UppercaseLetters,LowercaseLetters,Custom"
                                                                    ValidChars="+12890/ ">
                                                                </cc1:FilteredTextBoxExtender>
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ControlToValidate="txtuniversity"
                                                                     ErrorMessage="*">*</asp:RequiredFieldValidator>
                                                            </td>
				                                        </tr>
				                                        <tr id="Paneluneducated" runat="server" visible="false">
				                                            <td>
				                                                <asp:Label ID="lblclassnm" runat="server" Text="Secondary/Higher Secondary" Font-Bold="true"></asp:Label>
				                                            </td>
				                                            <td>
				                                                <asp:TextBox ID="txteducation" runat="server" TabIndex="28" Width="200"></asp:TextBox>
                                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR5" TargetControlID="txteducation"
                                                                    runat="server" FilterMode="ValidChars" FilterType="UppercaseLetters,LowercaseLetters,Custom"
                                                                    ValidChars="">
                                                                </cc1:FilteredTextBoxExtender>
				                                            </td>
				                                            <td><asp:Label ID="lblscholl" runat="server" Text="School/College" Font-Bold="true"></asp:Label></td>
				                                            <td>
				                                                <asp:TextBox ID="txtscholl" runat="server" TabIndex="29" Width="200"></asp:TextBox>
                                                                 <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR6" TargetControlID="txtscholl"
                                                                    runat="server" FilterMode="ValidChars" FilterType="UppercaseLetters,LowercaseLetters,Custom"
                                                                    ValidChars="">
                                                                </cc1:FilteredTextBoxExtender>
				                                            </td>
				                                        </tr>
				                                    </tbody>
				                                    <thead>
				                                        <tr>
					                                        <th colspan="4" style="text-align:center;"><h5>Company Details</h5></th>
				                                        </tr>
				                                    </thead>
				                                    <tbody>
				                                        <tr>
				                                            <td><asp:Label ID="lblcomdept" runat="server" Text="Department" Font-Bold="true"></asp:Label></td>
				                                            <td>
				                                                <asp:DropDownList ID="ddldepartment" runat="server" Width="215" Height="25" TabIndex="31">
                                                                </asp:DropDownList>
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ControlToValidate="ddldepartment"
                                                                    InitialValue="0" ErrorMessage="*">*</asp:RequiredFieldValidator>
				                                            </td>
				                                            <td><asp:Label ID="lbldesignation" runat="server" Text="Designation" Font-Bold="true"></asp:Label></td>
				                                            <td>
				                                                <asp:TextBox ID="txtdesignation" runat="server" TabIndex="32" Width="200"></asp:TextBox>
                                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR_Designation" runat="server"
                                                                    FilterMode="ValidChars" FilterType="UppercaseLetters,LowercaseLetters,Custom"
                                                                    TargetControlID="txtdesignation" ValidChars=" ">
                                                                </cc1:FilteredTextBoxExtender>
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ControlToValidate="txtdesignation"
                                                                     ErrorMessage="*">*</asp:RequiredFieldValidator>
				                                            </td>
				                                        </tr>
				                                        <tr>
				                                            <td><asp:Label ID="lblemptype" runat="server" Text="Employee Type" Font-Bold="true"></asp:Label></td>
                                                            <td>
                                                                <asp:DropDownList ID="ddemployeetype" runat="server" Width="215" Height="25" AutoPostBack="true"
                                                                    TabIndex="35" onselectedindexchanged="ddemployeetype_SelectedIndexChanged">
                                                                </asp:DropDownList>
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="ddemployeetype"
                                                                    InitialValue="0" ErrorMessage="*">*</asp:RequiredFieldValidator>
                                                            </td>                                                   
                                                            <td><asp:Label ID="lblexisiting" runat="server" Text="Exisiting No" Font-Bold="true"></asp:Label></td>
                                                            <td>
                                                                <asp:TextBox ID="txtexistiongno" runat="server" CssClass="text" MaxLength="10" TabIndex="38"
                                                                    Width="200"></asp:TextBox>
                                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender_ExisistingCode" runat="server"
                                                                    TargetControlID="txtexistiongno" FilterMode="ValidChars" FilterType="UppercaseLetters,LowercaseLetters,Numbers,Custom"                                                           ValidChars="0123456789+- ">
                                                                </cc1:FilteredTextBoxExtender>
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ControlToValidate="txtexistiongno"
                                                                     ErrorMessage="*">*</asp:RequiredFieldValidator>
                                                            </td>
				                                        </tr>
				                                        <tr id="panelcontract" runat="server" visible="false">
				                                            <td>
				                                                <asp:Label ID="lblContract" runat="server" Text="Contract Type" Font-Bold="true" ></asp:Label>
				                                            </td>
				                                            <td>
				                                                <asp:DropDownList ID="ddlContract" runat="server"  Width="215" Height="25" 
                                                                    onselectedindexchanged="ddlContract_SelectedIndexChanged" ></asp:DropDownList>
				                                            </td>
				                                            <td></td>
				                                            <td></td>
				                                        </tr>
				                                        <tr id="PanelLabourType" runat="server" visible="false">
				                                            <td><asp:Label ID="lbllabourtype" runat="server" Text="Labour Type" Font-Bold="true"></asp:Label></td>
                                                            <td><asp:DropDownList ID="ddllabourtype" runat="server" Width="215" Height="25"></asp:DropDownList></td>
                                                            <td><asp:Label ID="lblprobationpd" runat="server" Text="Probation Period" Font-Bold="true"></asp:Label></td>
                                                            <td><asp:DropDownList ID="ddlprobationpd" runat="server" Width="215" Height="25"></asp:DropDownList></td>
				                                        </tr>
				                                        <tr id="PanelLabourContarctType" runat="server" visible="false">
				                                            <td><asp:Label ID="lblcontracttype" runat="server" Text="Contract Type" Font-Bold="true"></asp:Label></td>
                                                            <td>
                                                                <asp:RadioButtonList ID="rbtncontratctype" runat="server" RepeatColumns="2">
                                                                    <asp:ListItem Text="Days" Value="1"></asp:ListItem>
                                                                    <asp:ListItem Text="Year" Value="2"></asp:ListItem>
                                                                </asp:RadioButtonList></td>
                                                            <td><asp:Label ID="lblclosingdate" runat ="server" Text="Closing Date" Font-Bold="true"></asp:Label></td>
                                                            <td><asp:TextBox ID="txtclosingdate" runat="server" Enabled="false" ></asp:TextBox></td>
				                                        </tr>
				                                        <tr>
				                                            <td>
				                                                <table>
				                                                    <tr>
				                                                        <td><asp:Label ID="lblnonpfgrade" runat="server" Text="On Role" Font-Bold="true"></asp:Label></td>
				                                                        <td><asp:CheckBox ID="chkboxnonpfgrade" runat="server" oncheckedchanged="chkboxnonpfgrade_CheckedChanged" /></td>
				                                                    </tr>
				                                                </table>
				                                            </td>                                                            
                                                            <td>
                                                                <table>
                                                                    <tr>
                                                                        
                                                                        <td><asp:Label ID="lblInsOut" runat="server" Text="Insider / Outsider" Font-Bold="true"></asp:Label></td>
                                                                        <td><asp:CheckBox ID="chkInsOuts" runat="server" /></td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                                <td>
                                                                    <asp:Label ID="lblbio" runat="server" Text="Bio-Metric ID" Font-Bold="true" ></asp:Label>
                                                                </td>
                                                                <td colspan="1">
                                                                <asp:TextBox ID="txtbio" runat="server" ></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ControlToValidate="txtbio"
                                                                     ErrorMessage="*">*</asp:RequiredFieldValidator>
                                                                     <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender14" runat="server"
                                                                    TargetControlID="txtbio" FilterMode="ValidChars" FilterType="Numbers,Custom" ValidChars="0123456789"></cc1:FilteredTextBoxExtender>
                                                                </td>
                                                        </tr>
                                                        <tr align="center">
                                                            <td><asp:Label ID="Label1" runat="server" Text=" * Mandatory Field" Font-Bold="true" ForeColor="Red" EnableTheming ="true"></asp:Label></td>
                                                            <td colspan="3">
                                                                <asp:Button ID="btnsave" runat="server" Text="Save" OnClick="btnsave_Click" TabIndex="39" Font-Bold="true" CssClass="button green" Width="70"/>
                                                                <asp:Button ID="btnedit" runat="server" Text="Edit" TabIndex="40" CausesValidation="false" OnClick="btnedit_Click" Font-Bold="true" CssClass="button green" Width="70"/>
                                                                <asp:Button ID="btnreset" runat="server" Text="Reset" TabIndex ="41" Font-Bold="true"  CausesValidation="false" onclick="btnreset_Click" CssClass="button green" Width="70"/>
                                                                <asp:Button ID="btncancel" runat="server" Text="Cancel" OnClick="btncancel_Click" CausesValidation="false" Font-Bold="true" CssClass="button green" Width="70"/>
                                                            </td>
                                                        </tr>
                                                        <tr id="Panelload" runat="server" visible="false">
                                                            <td colspan="4">
                                                                <asp:GridView ID="gvdownload" runat="server" AutoGenerateColumns="false" 
                                                                    onselectedindexchanged="gvdownload_SelectedIndexChanged">
                                                                    <Columns>
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate>
                                                                                EMP_NO
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblEmpNo" runat="server" Text='<%# Eval("EmpNo") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate>
                                                                                TOKEN nO
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblExist" runat="server" Text='<%# Eval("ExisistingCode") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate>
                                                                                MEMBER
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="Name" runat="server" Text='<%# Eval("EmpName") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate>
                                                                                GENDER
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblgender" runat="server" Text='<%# Eval("Gender") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate>
                                                                                FATHER&#39;S NAME
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblfather" runat="server" Text='<%# Eval("FatherName") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate>
                                                                                DISABLED
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="DISABLED" runat="server" Text="N"></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate>
                                                                                INTL WORKER
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="INTLWORKER" runat="server" Text="N"></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate>
                                                                                DOB
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="DOB" runat="server" Text='<%# Eval("DOB") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate>
                                                                                DOJ
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="DOJ" runat="server" Text='<%# Eval("DOJ") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate>
                                                                                MOBILE
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="MOBILE" runat="server" Text='<%# Eval("Mobile") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate>
                                                                                E-MAIL ID
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="EMAILID" runat="server" Text=""></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate>
                                                                                PAST SERVICE
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblpast" runat="server" Text=""></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate>
                                                                                H FLAG EPF
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="LBLHFLAG" runat="server" Text="N"></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate>
                                                                                H FLAG EPS
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblHFLAGEPS" runat="server" Text="N"></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate>
                                                                                BANK A/C
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblacc" runat="server" Text='<%# Eval("BankACCNo") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate>
                                                                                BANK NAME
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="bankName" runat="server" Text='<%# Eval("BankName") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate>
                                                                                BANK BR
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblbranch" runat="server" Text='<%# Eval("Branchname") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate>
                                                                                ADDRESS I
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lbladdress" runat="server" Text='<%#Eval("PSAdd1") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate>
                                                                                ADDRESS 2
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblAddress1" runat="server" Text='<%# Eval("PSAdd2") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate>
                                                                                ADDRESS CITY
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="city" runat="server" Text='<%# Eval("PSTaluk") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate>
                                                                                ADDRESS DISTRICT
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="District" runat="server" Text='<%# Eval("PSDistrict") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate>
                                                                                ADDRESS STATE
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="state" runat="server" Text='<%# Eval("PSState") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate>
                                                                                ADRESS PIN
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblpin" runat="server" Text=""></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate>
                                                                                MARITAL STATUS
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblstatus" runat="server" Text='<%# Eval("MartialStatus") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate>
                                                                                FATHER OR HUSBAND FLAG
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblfather" runat="server" Text='<%# Eval("Initial") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </td>
                                                        </tr>
				                                    </tbody>
				                                </table>
				                            </asp:Panel>
			                            </div>					        
			                            <!-- end tab pan -->
			                        </div>					    
			                        <!-- end inner content -->
		                        </div>				    
                                <!-- end innerdiv -->
                            </div>
                            <!-- end clear_body -->                                
                        </div>
                        <!-- end main-content -->
                    </ContentTemplate>           
                </asp:UpdatePanel>                
            </div>
            <!-- end page-wrap -->
            <div id="footer">
                <ul>
                    <li>Copyright &copy; 2012. All rights reserved.</li>
                    <li>Powered by <a href="http://www.altius.co.in" target="_blank">Altius Infosystems</a></li>
                </ul>
            </div>
            <!-- end footer -->
        </form>
    </body>
</html>
