﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Payroll;
using Payroll.Data;
using Payroll.Configuration;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html;
using iTextSharp.text.html.simpleparser;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using Altius.BusinessAccessLayer.BALDataAccess;
public partial class AttedanceSummary : System.Web.UI.Page
{
    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    SqlConnection con;
    BALDataAccess objdata = new BALDataAccess();
    DateTime TransDate;
    string SessionAdmin;
    string Stafflabour;
    string SessionCcode;
    string SessionLcode;
    string NetBase;
    string NetFDA;
    string NetVDA;
    string NetHRA;
    string Nettotal;
    string NetPFEarnings;
    string NetPF;
    string NetESI;
    string NetUnion;
    string NetAdvance;
    string NetAll1;
    string NetAll2;
    string NetAll3;
    string NetAll4;
    string NetAll5;
    string NetDed1;
    string NetDed2;
    string NetDed3;
    string NetDed4;
    string NetDed5;
    string HomeDays;
    string Halfnight;
    string FullNight;
    string DayIncentive;
    string Spinning;
    string ThreeSided;
    string NetLOP;
    string NetStamp;
    string NetTotalDeduction;
    string NetOT;
    string NetAmt;
    string Network;
    string totNFh;
    string totweekoff;
    string totCL;
    string totwork;
    string Roundoff;
    DateTime MyDate;
    DateTime MyDate1;
    DateTime MyDate2;
    static decimal AdvAmt;
    static string ID;
    static string Adv_id = "";
    static string Dec_mont = "0";
    static string Adv_BalanceAmt = "0";
    static string Adv_due = "0";
    static string Increment_mont = "0";
    static decimal val;
    static string EmployeeDays = "0";
    string MyMonth;
    static string cl = "0";
    string TempDate;
    string Slap_Query = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        con = new SqlConnection(constr);
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionAdmin = Session["Isadmin"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        lblComany.Text = SessionCcode + " - " + SessionLcode;
        lblusername.Text = Session["Usernmdisplay"].ToString();
        string ss = Session["UserId"].ToString();
        if (!IsPostBack)
        {
            DropDwonCategory();
           
            
           
        }
    }
    public void DropDwonCategory()
    {
        DataTable dtcate = new DataTable();
        dtcate = objdata.DropDownCategory();
        ddlcategory.DataSource = dtcate;
        ddlcategory.DataTextField = "CategoryName";
        ddlcategory.DataValueField = "CategoryCd";
        ddlcategory.DataBind();
    }
  
    public void EmployeeType()
    {
        DataTable dtemp = new DataTable();
        dtemp = objdata.EmployeeDropDown_no(ddlcategory.SelectedValue);
        ddldept.DataSource = dtemp;
        ddldept.DataTextField = "EmpType";
        ddldept.DataValueField = "EmpTypeCd";
        ddldept.DataBind();
    }
    protected void ddlcategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable dtempty = new DataTable();
        ddldept.DataSource = dtempty;
        ddldept.DataBind();

        if (ddlcategory.SelectedValue == "1")
        {
            Stafflabour = "S";
        }
        else if (ddlcategory.SelectedValue == "2")
        {
            Stafflabour = "L";
        }
        else
        {
            Stafflabour = "0";
        }
        EmployeeType();
    }
    protected void ddldept_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {

        try
        {
            bool ErrFlag = false;
            string CmpName = "";
            string Cmpaddress = "";
            int YR = 0;
            string StaffLabour = "";

            DateTime dfrom = Convert.ToDateTime(txtfrom.Text);
            DateTime dtto = Convert.ToDateTime(txtTo.Text);
            MyDate1 = DateTime.ParseExact(txtfrom.Text, "dd-MM-yyyy", null);
            MyDate2 = DateTime.ParseExact(txtTo.Text, "dd-MM-yyyy", null);

            int GetDate = ((MyDate2.Year - MyDate1.Year) * 12) + MyDate2.Month - MyDate1.Month;
            int MonthCnt = MyDate1.Month;
            int GetData = 0;


            int ValuesGet = MonthCnt + GetDate;
            string mon = "";
            for (int i = MonthCnt; i <= ValuesGet; i++)
            {
                GetData = i;
                if (GetData > 12)
                {
                    GetData = GetData - 12;
                }
                switch (GetData)
                {

                    case 1:
                        mon = "January";
                        break;
                    case 2:
                        mon = "February";
                        break;
                    case 3:
                        mon = "March";
                        break;
                    case 4:
                        mon = "April";
                        break;
                    case 5:
                        mon = "May";
                        break;
                    case 6:
                        mon = "June";
                        break;
                    case 7:
                        mon = "July";
                        break;
                    case 8:
                        mon = "August";
                        break;
                    case 9:
                        mon = "September";
                        break;
                    case 10:
                        mon = "October";
                        break;
                    case 11:
                        mon = "November";
                        break;
                    case 12:
                        mon = "December";
                        break;
                    default:
                        break;
                }
                string monthCal = "";
                monthCal += mon;
 
            }


            if (ddlcategory.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select the Category');", true);
                ErrFlag = true;
            }
            if (!ErrFlag)
            {
                if (ddlcategory.SelectedValue == "1")
                {
                    StaffLabour = "S";
                }
                else if (ddlcategory.SelectedValue == "2")
                {
                    StaffLabour = "L";
                }
                string constr = ConfigurationManager.AppSettings["ConnectionString"];
                SqlConnection con = new SqlConnection(constr);

                string query = "select ExisistingCode,EmpName,DepartmentNm,ISNULL([April],0) as April,ISNULL([May],0) as May, ";
                query = query + "ISNULL([June],0) as June,ISNULL([July],0) as July, ISNULL([August],0) as August, ";
                query = query + "ISNULL([September],0) as September,ISNULL([October],0) as October,ISNULL([November],0) as November, ";
                query = query + "ISNULL([December],0) as December,ISNULL([January],0) as January,ISNULL([February],0) as February, ";
                query = query + "ISNULL([March],0) as March, ";
                query = query + "(ISNULL([April],0)+ISNULL([May],0)+ISNULL([June],0)+ISNULL([July],0)+ISNULL([August],0)+ ";
                query = query + "ISNULL([September],0)+ISNULL([October],0)+ISNULL([November],0)+ISNULL([December],0)+ ";
                query = query + "ISNULL([January],0)+ISNULL([February],0)+ISNULL([March],0)) ";
                query = query + "as Total_Days from ( ";
                query = query + "select ED.ExisistingCode,ED.EmpName,MD.DepartmentNm,(Days + ThreeSided) Days, ";
                query = query + "Months from AttenanceDetails AD ";
                query = query + "inner join EmployeeDetails ED on AD.EmpNo=ED.EmpNo ";
                query = query + "inner join MstDepartment MD on MD.DepartmentCd=ED.Department ";
                               
                query = query + "where FromDate>=convert(datetime,'" + txtfrom.Text + "',103) and ToDate<=convert(datetime,'" + txtTo.Text + "',103) ";
                query = query + "and ED.StafforLabor='" + StaffLabour + "' and  ED.EmployeeType='" + ddldept.SelectedValue + "'";
                //query = query +  "ToDate<=convert(datetime,'" + txtTo.Text + "',103) and ED.StafforLabor='" + StaffLabour + "' and ED.EmployeeType='" + EmployeeType + "' ";
                query = query + "group by Months,ED.ExisistingCode,Days,ED.EmpName,MD.DepartmentNm,ThreeSided ";
                query = query +  ") AS ppv ";
                query = query +  "pivot(max(Days) for Months in ([April],[May],[June],[July],[August],[September],[October],[November],[December],[January],[February],[March])) ppt order by cast(ExisistingCode as decimal(18,2)) asc ";

                SqlCommand cmd = new SqlCommand(query, con);
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataSet ds1 = new DataSet();
                con.Open();
                sda.Fill(ds1);
                DataTable dt_1 = new DataTable();
                sda.Fill(dt_1);
                con.Close();



                gvReport.HeaderStyle.Font.Bold = true;
                gvReport.DataSource = dt_1;
                gvReport.DataBind();
                string attachment = "attachment;filename=AttendanceSummary.xls";
                Response.ClearContent();
                Response.AddHeader("content-disposition", attachment);
                Response.ContentType = "application/ms-excel";

                System.IO.StringWriter stw = new System.IO.StringWriter();
                HtmlTextWriter htextw = new HtmlTextWriter(stw);
                gvReport.RenderControl(htextw);
                Response.Write("<table>");
                Response.Write("<tr Font-Bold='true' align='center'>");
                Response.Write("<td font-Bold='true' colspan='12'>");
                Response.Write("<a style=\"font-weight:bold\">" + SessionCcode + "</a>");
                Response.Write("--");
                Response.Write("<a style=\"font-weight:bold\">" + SessionLcode + "</a>");
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("<tr Font-Bold='true' align='center'>");
                Response.Write("<td font-Bold='true' colspan='12'>");
                Response.Write("<a style=\"font-weight:bold\"> ATTENDANCE SUMMARY &nbsp;&nbsp;&nbsp;</a>");
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("<tr Font-Bold='true' align='center'>");
                Response.Write("<td colspan='12'>");

                Response.Write("<a style=\"font-weight:bold\"> FROM  -" + txtfrom.Text + "</a>");
                Response.Write("&nbsp;&nbsp;&nbsp;");
                Response.Write("<a style=\"font-weight:bold\"> TO -" + txtTo.Text + "</a>");
                Response.Write("</td>");
                Response.Write("</tr>");

                Response.Write("</table>");
                Response.Write(stw.ToString());
                Response.End();
                Response.Clear();




                

                //ResponseHelper.Redirect("VeiwReportNew.aspx?Cate=" + Stafflabour + "&Depat=" + ddldept.SelectedValue + "&fromdate=" + txtfrom.Text + "&ToDate=" + txtTo.Text + "&Salary=" + rbsalary.SelectedValue + "&EmpType=" + ddldept.SelectedValue.ToString() + "&PFTypePost=" + "&Left_Emp=" + "&Report_Type=Payslip", "_blank", "");
            }
            
        }
        catch (Exception Ex)
        { 
        
        }
        //try
        //{
        //    bool ErrFlag = false;
        //    string CmpName = "";
        //    string Cmpaddress = "";
        //    int YR = 0;
        //    if (ddlMonths.SelectedValue == "January")
        //    {
        //        YR = Convert.ToInt32(ddlFinance.SelectedValue);
        //        YR = YR + 1;
        //    }
        //    else if (ddlMonths.SelectedValue == "February")
        //    {
        //        YR = Convert.ToInt32(ddlFinance.SelectedValue);
        //        YR = YR + 1;
        //    }
        //    else if (ddlMonths.SelectedValue == "March")
        //    {
        //        YR = Convert.ToInt32(ddlFinance.SelectedValue);
        //        YR = YR + 1;
        //    }
        //    else
        //    {
        //        YR = Convert.ToInt32(ddlFinance.SelectedValue);
        //    }

        //    if (ddlcategory.SelectedValue == "0")
        //    {
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select the Category');", true);
        //        ErrFlag = true;
        //    }
        //    //else if ((txtEmployeeType.SelectedValue == "") || (txtEmployeeType.SelectedValue == "0"))
        //    //{
        //    //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select the Employee Type');", true);
        //    //    ErrFlag = true;
        //    //}

        //    else if ((ddlMonths.SelectedValue == "0") || (ddlMonths.SelectedValue == ""))
        //    {
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select the Months');", true);
        //        ErrFlag = true;
        //    }


        //    if (!ErrFlag)
        //    {
        //        //if (ddldept.SelectedValue == "0")
        //        //{
        //        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select the Department Properly');", true);
        //        //    ErrFlag = true;
        //        //}
        //        if (!ErrFlag)
        //        {
        //            if (ddlcategory.SelectedValue == "1")
        //            {
        //                Stafflabour = "S";
        //            }
        //            else if (ddlcategory.SelectedValue == "2")
        //            {
        //                Stafflabour = "L";
        //            }
        //            string constr = ConfigurationManager.AppSettings["ConnectionString"];
        //            SqlConnection con = new SqlConnection(constr);
        //            //foreach (GridViewRow gvsal in gvSalary.Rows)
        //            //{
        //            //    Label lblEmpNo = (Label)gvsal.FindControl("lblExist");
        //            //    string qry = "Update SalaryDetails set Process_Mode='" + 1 + "' where Month='" + ddlMonths.SelectedValue + "' and FinancialYear='" + ddlFinance.SelectedValue + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and ExisistingCode='" + lblEmpNo.Text + "'";
        //            //    SqlCommand cmd = new SqlCommand(qry, con);
        //            //    con.Open();
        //            //    cmd.ExecuteNonQuery();
        //            //    con.Close();
        //            ////}
        //            //string Emp_ESI_Code = "";//ddESI.SelectedValue.ToString();

        //            //string Payslip_Format_Type = rdbPayslipFormat.SelectedValue.ToString();

        //            //string Str_PFType = RdbPFNonPF.SelectedValue.ToString();
        //            //string Str_ChkLeft = "";
        //            //Str_ChkLeft = ChkLeft.SelectedValue.ToString();
        //            //if (ChkLeft.Checked == true)
        //            //{
        //            //    Str_ChkLeft = "1";
        //            //}
        //            //else { Str_ChkLeft = "0"; }

        //            ResponseHelper.Redirect("ViewReport.aspx?Cate=" + Stafflabour + "&Depat=" + ddldept.SelectedValue + "&Months=" + ddlMonths.SelectedValue + "&yr=" + ddlFinance.SelectedValue + "&fromdate=" + txtfrom.Text + "&ToDate=" + txtTo.Text + "&Salary=" + rbsalary.SelectedValue + "&EmpType=" + ddldept.SelectedValue.ToString() + "&PFTypePost=" + "&Left_Emp=" + "&Report_Type=Payslip", "_blank", "");

        //        }
        //    }
        //}
        //catch (Exception ex)
        //{
        //}

        //try
        //{
        //    bool ErrFlag = false;
        //    string CmpName = "";
        //    string Cmpaddress = "";
        //    int YR = 0;
        //    if (ddlMonths.SelectedValue == "January")
        //    {
        //        YR = Convert.ToInt32(ddlFinance.SelectedValue);
        //        YR = YR + 1;
        //    }
        //    else if (ddlMonths.SelectedValue == "February")
        //    {
        //        YR = Convert.ToInt32(ddlFinance.SelectedValue);
        //        YR = YR + 1;
        //    }
        //    else if (ddlMonths.SelectedValue == "March")
        //    {
        //        YR = Convert.ToInt32(ddlFinance.SelectedValue);
        //        YR = YR + 1;
        //    }
        //    else
        //    {
        //        YR = Convert.ToInt32(ddlFinance.SelectedValue);
        //    }

        //    if (ddlcategory.SelectedValue == "0")
        //    {
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select the Category');", true);
        //        ErrFlag = true;
        //    }
        //    else if ((ddldept.SelectedValue == "") || (ddldept.SelectedValue == "0"))
        //    {
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select the Employee Type');", true);
        //        ErrFlag = true;
        //    }
        //    else if ((ddlMonths.SelectedValue == "0") || (ddlMonths.SelectedValue == ""))
        //    {
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select the Months');", true);
        //        ErrFlag = true;
        //    }
        //    if (!ErrFlag)
        //    {
        //        if (ddldept.SelectedValue == "0")
        //        {
        //            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select the Department Properly');", true);
        //            ErrFlag = true;
        //        }
        //        if (!ErrFlag)
        //        {
        //            if (ddlcategory.SelectedValue == "1")
        //            {
        //                Stafflabour = "S";
        //            }
        //            else if (ddlcategory.SelectedValue == "2")
        //            {
        //                Stafflabour = "L";
        //            }
        //            string constr = ConfigurationManager.AppSettings["ConnectionString"];
        //            SqlConnection con = new SqlConnection(constr);
        //            foreach (GridViewRow gvsal in gvSalary.Rows)
        //            {
        //                Label lblEmpNo = (Label)gvsal.FindControl("lblExist");
        //                string qry = "Update SalaryDetails set Process_Mode='" + 1 + "' where Month='" + ddlMonths.SelectedValue + "' and FinancialYear='" + ddlFinance.SelectedValue + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and ExisistingCode='" + lblEmpNo.Text + "'";
        //                SqlCommand cmd = new SqlCommand(qry, con);
        //                con.Open();
        //                cmd.ExecuteNonQuery();
        //                con.Close();
        //            }

        //            ResponseHelper.Redirect("ViewReport.aspx?Cate=" + Stafflabour + "&Depat=" + ddldept.SelectedValue + "&Months=" + ddlMonths.SelectedValue + "&yr=" + ddlFinance.SelectedValue + "&fromdate=" + txtfrom.Text + "&ToDate=" + txtTo.Text + "&Salary=" + rbsalary.SelectedValue, "_blank", "");



        //        }
        //    }
        //}
        //catch (Exception ex)
        //{
        //}
    }
    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }
    protected void btnEmp_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptEmpDownload.aspx");
    }
    protected void btnatt_Click(object sender, EventArgs e)
    {
        Response.Redirect("AttenanceDownload.aspx");
    }
    protected void btnLeave_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptLeaveSample.aspx");
    }
    protected void btnOT_Click(object sender, EventArgs e)
    {
        Response.Redirect("OTDownload.aspx");
    }


  
    protected void gvSalary_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void rbsalary_SelectedIndexChanged(object sender, EventArgs e)
    {
        //if (rbsalary.SelectedValue == "2")
        //{
        //    pnlMonth.Visible = false;
        //}
        //else if (rbsalary.SelectedValue == "1")
        //{
        //    pnlMonth.Visible = true;
        //}
        //else if (rbsalary.SelectedValue == "3")
        //{
        //    pnlMonth.Visible = true;
        //}
        //else
        //{
        //    pnlMonth.Visible = false;
        //}
    }
    protected void txtfrom_TextChanged(object sender, EventArgs e)
    {

    }
    protected void txtTo_TextChanged(object sender, EventArgs e)
    {

    }
    protected void btncontract_Click(object sender, EventArgs e)
    {
        Response.Redirect("ContractRPT.aspx");
    }
    
}
