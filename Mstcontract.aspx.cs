﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Data.SqlClient;
using System.Configuration;


public partial class Mstcontract : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    ContractClass objcon = new ContractClass();
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    static string Cont_code = "";
    static string lblprobation_Common = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        string ss = Session["UserId"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();
        if (!IsPostBack)
        {
            Retrive_load();
            
        }
        SessionAdmin = Session["Isadmin"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        lblusername.Text = Session["Usernmdisplay"].ToString();
        lblComany.Text = SessionCcode + " - " + SessionLcode;
    }
    protected void btnEmp_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptEmpDownload.aspx");
    }
    protected void btnatt_Click(object sender, EventArgs e)
    {
        Response.Redirect("AttenanceDownload.aspx");
    }
    protected void btnLeave_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptLeaveSample.aspx");
    }
    protected void btnOT_Click(object sender, EventArgs e)
    {
        Response.Redirect("OTDownload.aspx");
    }
    protected void cmdOverriteGrade_Click(object sender, EventArgs e)
    {
        try
        {
            string constr = ConfigurationManager.AppSettings["ConnectionString"];
            SqlConnection con = new SqlConnection(constr);
            //Label lblbank = (Label)gvBank.Rows[e.RowIndex].FindControl("lblBankcd");
            string qry = "Delete from ContractMaster where ContractCode = '" + lblprobation_Common + "'";
            SqlCommand cmd = new SqlCommand(qry, con);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Deleted Successfully');", true);
            //System.Windows.Forms.MessageBox.Show("User Deleted", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            clr();

        }
        catch (Exception ex)
        {
        }
    }
    protected void rbtype_SelectedIndexChanged(object sender, EventArgs e)
    {
        txtname.Enabled = true;
        txtname.Text = "";
        txtyr.Text = "";
        txtMonth.Text = "";
        txtdaysperMonth.Text = "";
        txtdays.Text = "";
        txtyrDays.Text = "";
        txtnotEligible.Text = "";
        txtNotEDays.Text = "";
        Retrive_load();
        if (rbtype.SelectedValue == "1")
        {
            panelyear.Visible = true;
            panelMonths.Visible = false;
            PanelDays.Visible = false;
            
        }
        else if (rbtype.SelectedValue == "2")
        {
            panelyear.Visible = false;
            panelMonths.Visible = true;
            PanelDays.Visible = false;
            
        }
        else if (rbtype.SelectedValue == "3")
        {
            panelyear.Visible = false;
            panelMonths.Visible = false;
            PanelDays.Visible = true;
            
        }
    }
    protected void gvcontract_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {

    }
    protected void gvcontract_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        if (SessionAdmin == "1")
        {
            string constr = ConfigurationManager.AppSettings["ConnectionString"];
            SqlConnection con = new SqlConnection(constr);
            Label lbluser = (Label)gvcontract.Rows[e.RowIndex].FindControl("lblconCd");
            lblprobation_Common = lbluser.Text;
            string qry = "select ContractType from EmployeeDetails where ContractType='" + lbluser.Text + "'";
            //SqlCommand cmd = new SqlCommand(qry, con);
            SqlDataAdapter sda = new SqlDataAdapter(qry, con);
            DataTable dt = new DataTable();
            con.Open();
            sda.Fill(dt);
            con.Close();
            if (dt.Rows.Count > 0)
            {
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "script", "showConfirm();", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Row cannot be Deleted....');", true);
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Row cannot be Deleted....');", true);
        }
    }
    protected void gvcontract_RowEditing(object sender, GridViewEditEventArgs e)
    {
        Label lbledit = (Label)gvcontract.Rows[e.NewEditIndex].FindControl("lblconCd");
        DataTable dt = new DataTable();
        dt = objdata.Contract_Edit(lbledit.Text);
        if (dt.Rows.Count > 0)
        {
            Cont_code = lbledit.Text;
            txtname.Text = "";
            txtyr.Text = "";
            txtMonth.Text = "";
            txtdaysperMonth.Text = "";
            txtdays.Text = "";
            txtyrDays.Text = "";
            txtname.Enabled = false;
            rbtype.SelectedValue = dt.Rows[0]["ContractType"].ToString();
            txtname.Text = dt.Rows[0]["ContractName"].ToString();
            txtNotEDays.Text = dt.Rows[0]["NotEDays"].ToString();
            txtnotEligible.Text = dt.Rows[0]["NotEMonths"].ToString();
            if (dt.Rows[0]["ContractType"].ToString() == "1")
            {
                panelyear.Visible = true;
                panelMonths.Visible = false;
                PanelDays.Visible = false;
                txtyr.Text = dt.Rows[0]["ContractYear"].ToString();
                txtyrDays.Text = dt.Rows[0]["YrDays"].ToString();
            }
            else if (dt.Rows[0]["ContractType"].ToString() == "2")
            {
                panelyear.Visible = false;
                panelMonths.Visible = true;
                PanelDays.Visible = false;
                txtMonth.Text = dt.Rows[0]["ContractMonth"].ToString();
                txtdaysperMonth.Text = dt.Rows[0]["FixedDays"].ToString();
            }
            else if (dt.Rows[0]["ContractType"].ToString() == "3")
            {
                panelyear.Visible = false;
                panelMonths.Visible = false;
                PanelDays.Visible = true;
                txtdays.Text = dt.Rows[0]["ContractDays"].ToString();
            }
        }
    }
    protected void btnclear_Click(object sender, EventArgs e)
    {
        clr();
    }
    public void clr()
    {
        rbtype.SelectedIndex = -1;
        panelyear.Visible = false;
        panelMonths.Visible = false;
        PanelDays.Visible = false;
        txtname.Enabled = true;
        txtname.Text = "";
        txtyr.Text = "";
        txtMonth.Text = "";
        txtdaysperMonth.Text = "";
        txtdays.Text = "";
        txtyrDays.Text = "";
        txtnotEligible.Text = "";
        txtNotEDays.Text = "0";
        Retrive_load();
    }
    public void Retrive_load()
    {
        DataTable dt_empty = new DataTable();
        DataTable dt = new DataTable();
        gvcontract.DataSource = dt_empty;
        gvcontract.DataBind();
        dt = objdata.Contract_retrive();
        gvcontract.DataSource = dt;
        gvcontract.DataBind();
    }
    protected void btnMont_Click(object sender, EventArgs e)
    {
        try
        {
            bool ErrFlag = false;
            bool SaveFlag = false;
            if (rbtype.SelectedValue == "1")
            {
                if (txtname.Text.Trim() == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Contract Name....');", true);
                    ErrFlag = true;
                }
                else if (txtyr.Text.Trim() == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the years....');", true);
                    ErrFlag = true;
                }
                else if (txtyrDays.Text.Trim() == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Days....');", true);
                    ErrFlag = true;
                }
                else if (txtyr.Text.Trim() != "")
                {
                    if (Convert.ToInt32(txtyr.Text) == 0)
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the years Properly....');", true);
                        ErrFlag = true;
                    }
                    else if (Convert.ToInt32(txtyrDays.Text) == 0)
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Days Properly....');", true);
                        ErrFlag = true;
                    }
                }
            }
            else if (rbtype.SelectedValue == "2")
            {
                if (txtname.Text.Trim() == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Contract Name....');", true);
                    ErrFlag = true;
                }
                else if (txtMonth.Text.Trim() == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Months....');", true);
                    ErrFlag = true;
                }
                else if (txtdaysperMonth.Text.Trim() == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Days per Month....');", true);
                    ErrFlag = true;
                }
                else if (txtMonth.Text.Trim() != "")
                {
                    if (Convert.ToInt32(txtMonth.Text) == 0)
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Months Properly....');", true);
                        ErrFlag = true;
                    }
                }
                else if (txtdaysperMonth.Text.Trim() != "")
                {
                    if (Convert.ToInt32(txtdaysperMonth.Text) == 0)
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Days per Month Properly....');", true);
                        ErrFlag = true;
                    }
                }
            }
            else if (rbtype.SelectedValue == "3")
            {
                if (txtname.Text.Trim() == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Contract Name....');", true);
                    ErrFlag = true;
                }
                else if (txtdays.Text.Trim() == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Days....');", true);
                    ErrFlag = true;
                }
                else if (txtnotEligible.Text.Trim() == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the not Eligible Months....');", true);
                    ErrFlag = true;
                }
                else if (txtNotEDays.Text.Trim() == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Not Eligible Days per Month....');", true);
                    ErrFlag = true;
                }
                else if (txtdays.Text.Trim() != "")
                {
                    if (Convert.ToInt32(txtdays.Text) == 0)
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Days Properly....');", true);
                        ErrFlag = true;
                    }
                }
            }
            if (!ErrFlag)
            {
                if (txtname.Enabled == false)
                {
                    if (rbtype.SelectedValue == "1")
                    {
                        objcon.ContractName = txtname.Text.Trim();
                        objcon.ContractType = "1";
                        objcon.ContractYear = txtyr.Text.Trim();
                        objcon.ContractMonth = "0";
                        objcon.ContractDays = "0";
                        objcon.FixedDays = "0";
                        objcon.NotEDays = txtNotEDays.Text;
                        objcon.NotEMonths = txtnotEligible.Text;
                        objcon.YrDays = txtyrDays.Text.Trim();
                        objcon.PlannedDays = (Convert.ToInt32(txtyrDays.Text) * Convert.ToInt32(txtyr.Text)).ToString();

                    }
                    else if (rbtype.SelectedValue == "2")
                    {
                        objcon.ContractName = txtname.Text.Trim();
                        objcon.ContractType = "2";
                        objcon.ContractYear = "0";
                        objcon.ContractMonth = txtMonth.Text.Trim();
                        objcon.ContractDays = "0";
                        objcon.FixedDays = txtdaysperMonth.Text.Trim();
                        objcon.YrDays = "0";
                        objcon.NotEDays = txtNotEDays.Text;
                        objcon.NotEMonths = txtnotEligible.Text;
                        objcon.PlannedDays = (Convert.ToInt32(txtdaysperMonth.Text) * Convert.ToInt32(txtMonth.Text)).ToString();
                    }
                    else if (rbtype.SelectedValue == "3")
                    {
                        objcon.ContractName = txtname.Text.Trim();
                        objcon.ContractType = "3";
                        objcon.ContractYear = "0";
                        objcon.ContractMonth = "0";
                        objcon.ContractDays = txtdays.Text.Trim();
                        objcon.FixedDays = "0";
                        objcon.YrDays = "0";
                        objcon.NotEDays = txtNotEDays.Text;
                        objcon.NotEMonths = txtnotEligible.Text;
                        objcon.PlannedDays = txtdays.Text.Trim();
                    }
                    objdata.Contract_Update_sp(objcon, Cont_code);
                    SaveFlag = true;
                    if (SaveFlag == true)
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Updated Successfully....');", true);
                        clr();

                    }
                }
                else
                {
                    string Cname = objdata.Contract_verify(txtname.Text.Trim());
                    if (Cname != "")
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Contract Name Already Exist....');", true);
                        ErrFlag = true;
                    }
                    if (!ErrFlag)
                    {
                        if (rbtype.SelectedValue == "1")
                        {
                            objcon.ContractName = txtname.Text.Trim();
                            objcon.ContractType = "1";
                            objcon.ContractYear = txtyr.Text.Trim();
                            objcon.ContractMonth = "0";
                            objcon.ContractDays = "0";
                            objcon.FixedDays = "0";
                            objcon.NotEDays = txtNotEDays.Text;
                            objcon.NotEMonths = txtnotEligible.Text;
                            objcon.YrDays = txtyrDays.Text.Trim();
                            objcon.PlannedDays = (Convert.ToInt32(txtyrDays.Text) * Convert.ToInt32(txtyr.Text)).ToString();
                        }
                        else if (rbtype.SelectedValue == "2")
                        {
                            objcon.ContractName = txtname.Text.Trim();
                            objcon.ContractType = "2";
                            objcon.ContractYear = "0";
                            objcon.ContractMonth = txtMonth.Text.Trim();
                            objcon.ContractDays = "0";
                            objcon.FixedDays = txtdaysperMonth.Text.Trim();
                            objcon.YrDays = "0";
                            objcon.NotEDays = txtNotEDays.Text;
                            objcon.NotEMonths = txtnotEligible.Text;
                            objcon.PlannedDays = (Convert.ToInt32(txtdaysperMonth.Text) * Convert.ToInt32(txtMonth.Text)).ToString();
                        }
                        else if (rbtype.SelectedValue == "3")
                        {
                            objcon.ContractName = txtname.Text.Trim();
                            objcon.ContractType = "3";
                            objcon.ContractYear = "0";
                            objcon.ContractMonth = "0";
                            objcon.ContractDays = txtdays.Text.Trim();
                            objcon.FixedDays = "0";
                            objcon.YrDays = "0";
                            objcon.NotEDays = txtNotEDays.Text;
                            objcon.NotEMonths = txtnotEligible.Text;
                            objcon.PlannedDays = txtdays.Text.Trim();
                        }
                        objdata.contract_insert_sp(objcon);
                        SaveFlag = true;
                        if (SaveFlag == true)
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Saved Successfully....');", true);
                            
                            clr();
                            
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Contact Admin....');", true);
        }
    }
    protected void gvcontract_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void btncontract_Click(object sender, EventArgs e)
    {
        Response.Redirect("ContractRPT.aspx");
    }
    protected void btnSal_Click(object sender, EventArgs e)
    {
        Response.Redirect("FrmDeductionDownload.aspx");
    }
}
