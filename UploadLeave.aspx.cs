﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Configuration;
using System.Data.SqlClient;
using System.Data.OleDb;
using Payroll;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class UploadLeave_1 : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    int tempExcess;
    int LeavdaysCount;
    string LeaveDaysCount1;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (SessionAdmin == "2")
        {
            Response.Redirect("EmployeeRegistration.aspx");
        }

        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionAdmin = Session["Isadmin"].ToString();
        if (!IsPostBack)
        {

            int currentYear = Utility.GetFinancialYear;
            for (int i = 0; i < 10; i++)
            {
                ddlFinaYear.Items.Add(new ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                //  ddlShowYear.Items.Add(new ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                currentYear = currentYear - 1;
            }
        }
        lblusername.Text = Session["Usernmdisplay"].ToString();
        string ss = Session["UserId"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        lblComany.Text = SessionCcode + " - " + SessionLcode;
        lblComany.Text = SessionCcode + " - " + SessionLcode;
    }
    protected void btnUpload_Click(object sender, EventArgs e)
    {
        //panelError.Visible = true;
        bool ErrFlag = false;
        string tempfromDate = "";
        string temptodate = "";
        DateTime DfromDate;
        DateTime DtoDate;
        DateTime ApprovedDate;
        ApprovedDate = new DateTime();
        DfromDate = new DateTime();
        DtoDate = new DateTime();
        string LeaveCode = "";
        string AllocatedLeave = "";
        string noofLeave = "";
        string ExcessLeave = "";
        string Maternalleave = "";

        bool CheckFlag = false;
        try
        {
            //string txt = Convert.ToString(fileUpload.FileName);
            if (FileUpload.HasFile)
            {
                FileUpload.SaveAs(Server.MapPath("Upload/" + FileUpload.FileName));
            }
            //string mypath = "'C:/Users/RaguramVasanth/Documents/Work/Working/Nov06/Payroll03_dec_New/UploadLeave/Leave.xls";
            //fileUpload.SaveAs(Server.MapPath("Upload/" + mypath));
            string connectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + Server.MapPath("Upload/" + FileUpload.FileName) + ";" + "Extended Properties=Excel 8.0;";

            OleDbConnection sSourceConnection = new OleDbConnection(connectionString);
            DataTable dts = new DataTable();

            using (sSourceConnection)
            {
                sSourceConnection.Open();

                OleDbCommand command = new OleDbCommand("Select EmpNo,LeaveType,NumberofLeave,TotalWorkingDays,FromDate,ToDate,Month,Year,Approvededby,Approveddate FROM [Sheet1$];", sSourceConnection);
                sSourceConnection.Close();


                using (OleDbCommand cmd = sSourceConnection.CreateCommand())
                {
                    command.CommandText = "Select EmpNo,LeaveType,NumberofLeave,TotalWorkingDays,FromDate,ToDate,Month,Year,Approvededby,Approveddate FROM [Sheet1$];";
                    sSourceConnection.Open();
                }
                using (OleDbDataReader dr = command.ExecuteReader())
                {

                    if (dr.HasRows)
                    {

                    }

                }
                OleDbDataAdapter objDataAdapter = new OleDbDataAdapter();
                objDataAdapter.SelectCommand = command;
                DataSet ds = new DataSet();

                objDataAdapter.Fill(ds);
                DataTable dt = new DataTable();
                dt = ds.Tables[0];
                for (int i = 0; i < dt.Columns.Count; i++)
                {
                    dt.Columns[i].ColumnName = dt.Columns[i].ColumnName.Replace(" ", string.Empty).ToString();

                }
                string constr = ConfigurationManager.AppSettings["ConnectionString"];
                for (int j = 0; j < dt.Rows.Count; j++)
                {
                    SqlConnection cn = new SqlConnection(constr);
                    string EmpNo = dt.Rows[j]["EmpNo"].ToString();
                    string LeaveType = dt.Rows[j]["LeaveType"].ToString();
                    string NumberofLeave = dt.Rows[j]["NumberofLeave"].ToString();
                    string TotalWorkingDays = dt.Rows[j]["TotalWorkingDays"].ToString();
                    string FromDate = dt.Rows[j]["FromDate"].ToString();
                    string ToDate = dt.Rows[j]["ToDate"].ToString();
                    string Month = dt.Rows[j]["Month"].ToString();
                    string Year = dt.Rows[j]["Year"].ToString();

                    string Approvededby = dt.Rows[j]["Approvededby"].ToString();
                    string Approveddate = dt.Rows[j]["Approveddate"].ToString();

                    if (EmpNo == "")
                    {
                        j = j + 2;
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Employee Number. The Row Number is " + j + "');", true);
                        //System.Windows.Forms.MessageBox.Show("Enter the Employee Number. The Roe Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                        //lblError.Text = "Enter the Employee Number. The Row No is " + j;
                        ErrFlag = true;
                    }
                    else if (LeaveType == "")
                    {
                        j = j + 2;
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter LeaveType Number. The Row Number is " + j + " ');", true);
                        //System.Windows.Forms.MessageBox.Show("Enter the LeaveType Number. The Row Number is" + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                        //lblError.Text = "Enter the Leave Type. The Row Number is " + j;
                        ErrFlag = true;
                    }
                    else if (NumberofLeave == "")
                    {
                        j = j + 2;
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Number of Leave. The Row Number is " + j + "');", true);
                        //System.Windows.Forms.MessageBox.Show("Enter the Number of Leave. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                        //lblError.Text = "Enter the No of leave. The Row No is " + j;
                        ErrFlag = true;

                    }
                    else if (Convert.ToInt32(NumberofLeave) == 0)
                    {
                        j = j + 2;
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Number of Leave. The Row Number is " + j + "');", true);
                        //System.Windows.Forms.MessageBox.Show("Enter the Number of Leave. The Row is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                        //lblError.Text = "Enter the No of Leave. The Row No is " + j;
                        ErrFlag = true;

                    }

                    else if (TotalWorkingDays == "")
                    {
                        j = j + 2;
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Total WorkingDays. The Row Number is " + j + "');", true);
                        //System.Windows.Forms.MessageBox.Show("Enter the Total Working Days. The Row no is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                        //lblError.Text = "Enter the Total Working Days. the row No is " + j;
                        ErrFlag = true;
                    }
                    else if (Convert.ToInt32(TotalWorkingDays) == 0)
                    {
                        j = j + 2;
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Total WorkingDays. The Row Number is " + j + "');", true);
                        //System.Windows.Forms.MessageBox.Show("Enter the Total Working Days. the Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                        //lblError.Text = "Enter the Total Working Days. the Row No is " + j;
                        ErrFlag = true;
                    }
                    else if (FromDate == "")
                    {
                        j = j + 2;
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter From Date. The Row Number is " + j + "');", true);
                        //System.Windows.Forms.MessageBox.Show("Enter the From Date", "AltiusInfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                        //lblError.Text = "Enter The From Date. The Row No is " + j;
                        ErrFlag = true;
                    }
                    else if (ToDate == "")
                    {
                        j = j + 2;
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter To Date. The Row Number is " + j + "');", true);
                        //System.Windows.Forms.MessageBox.Show("Enter the From Date", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                        //lblError.Text = "Enter To Date. The Row No is " + j;
                        ErrFlag = true;
                    }

                    else if (Month == "")
                    {
                        j = j + 2;
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Month. The Row Number is " + j + "');", true);
                        //System.Windows.Forms.MessageBox.Show("Enter the Month. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                        //lblError.Text = "Enter the Month. The Row No is " + j;
                        ErrFlag = true;
                    }
                    else if (Year == "")
                    {
                        j = j + 2;
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Year. The Row Number is " + j + "');", true);
                        //System.Windows.Forms.MessageBox.Show("Enter the Year. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                        //lblError.Text = "Enter the Year. The Row Number is " + j;
                        ErrFlag = true;
                    }

                    //else if (ExcessLeave == "")
                    //{
                    //    j = j + 2;
                    //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Excess Leave. The Row Number is " + j + "');", true);
                    //    ErrFlag = true;
                    //}

                    else if (Approvededby == "")
                    {
                        j = j + 2;
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Approveded by. The Row Number is " + j + "');", true);
                        //System.Windows.Forms.MessageBox.Show("Enter the Approved By. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                        //lblError.Text = "Enter the Approved By " + j;
                        ErrFlag = true;
                    }
                    else if (Approveddate == "")
                    {
                        j = j + 2;
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Approved Date. The Row Number is " + j + "');", true);
                        //System.Windows.Forms.MessageBox.Show("Entered the Approved Date. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                        //lblError.Text = "Enter the Approved Date. The Row No is j" + j;
                        ErrFlag = true;
                    }
                    tempfromDate = Convert.ToDateTime(FromDate).ToShortDateString();
                    temptodate = Convert.ToDateTime(ToDate).ToShortDateString();
                    DfromDate = DateTime.ParseExact(tempfromDate, "dd/MM/yyyy", null);
                    DtoDate = DateTime.ParseExact(temptodate, "dd/MM/yyyy", null);
                    if (DfromDate > DtoDate)
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Year is incorrect Enter Correct Value. The Row Number is " + j + "');", true);
                        //System.Windows.Forms.MessageBox.Show("Year is Incorrect. Enter the Correct Value. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                        //lblError.Text = "Year is Incorrect. Enter the Correct Value. The Row No is " + j;
                        ErrFlag = true;
                    }

                    cn.Open();
                    string Query = "Select LeaveType from MstLeave where LeaveType='" + LeaveType + "'";
                    SqlCommand cmd = new SqlCommand(Query, cn);
                    SqlDataReader sdr = cmd.ExecuteReader();
                    if (sdr.HasRows)
                    {


                    }
                    else
                    {
                        j = j + 2;
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('This Leave Type Not Availabe in Leave type Master.The Row Number is " + j + " ');", true);
                        //System.Windows.Forms.MessageBox.Show("This Leave Type Not Available in Leave Type Master. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                        //lblError.Text = "This Leave Type Not Available in Leave Type Maste. The Row No is " + j;
                        ErrFlag = true;

                    }
                    cn.Close();
                    cn.Open();
                    string Query1 = "Select EmpNo from EmployeeDetails where EmpNo='" + EmpNo + "' and Status = 'O'";
                    SqlCommand cmd1 = new SqlCommand(Query1, cn);
                    SqlDataReader sdr1 = cmd1.ExecuteReader();
                    if (sdr1.HasRows)
                    {


                    }
                    else
                    {
                        j = j + 2;
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('This Employee Number not Available.The Row Number is " + j + " ');", true);
                        //System.Windows.Forms.MessageBox.Show("This Employee Number not Available. The Row number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                        //lblError.Text = "The Employee Number not Avsilable. The Row Number is " + j;
                        ErrFlag = true;

                    }
                    cn.Close();


                    DataTable dtchek = new DataTable();
                    dtchek = objdata.AllReadyApplyForLeaveDate(EmpNo, DfromDate, DtoDate, ddlFinaYear.SelectedValue, SessionCcode, SessionLcode);
                    if (dtchek.Rows.Count > 0)
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('You All Ready Apply to Leave From " + dtchek.Rows[0]["FromDate"].ToString() + " To " + dtchek.Rows[0]["ToDate"].ToString() + " ');", true);
                        //System.Windows.Forms.MessageBox.Show("You All Ready Apply to Leave From " + dtchek.Rows[0]["FromDate"].ToString() + " To " + dtchek.Rows[0]["ToDate"].ToString(), "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                        //lblError.Text = "You already Applied to Leave From " + dtchek.Rows[0]["FromDate"].ToString() + " To " + dtchek.Rows[0]["ToDate"].ToString();
                        ErrFlag = true;
                    }
                    string qry_gender = "select case Gender when '1' then 'male' else 'female' end as Gender from EmployeeDetails where EmpNo='" + EmpNo + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                    SqlCommand cmd_g = new SqlCommand(qry_gender, cn);
                    cn.Open();
                    string gen = Convert.ToString(cmd_g.ExecuteScalar());
                    cn.Close();
                    if (gen == "male")
                    {
                        if (LeaveType == "Maternity Leave")
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('You cannot Apply Maternity leave for this Employee " + EmpNo + " ');", true);
                            //System.Windows.Forms.MessageBox.Show("You cannot Apply Maternity Leave for this Employee " + EmpNo , "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                            //lblError.Text = "You cannot Apply Maternity Leave for this Employee " + EmpNo;
                            ErrFlag = true;
                        }
                    }

                }



                if (!ErrFlag)
                {

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        DateTime EnteredDate;
                        SqlConnection cn = new SqlConnection(constr);
                        cn.Open();
                        string Query_leavetype = "Select LeaveCd from MstLeave where LeaveType='" + dt.Rows[i]["LeaveType"].ToString() + "'";
                        SqlCommand cmd_LT = new SqlCommand(Query_leavetype, cn);
                        LeaveCode = Convert.ToString(cmd_LT.ExecuteScalar());

                        string QueryAllocated = "Select AllocatedLeave from LeaveAllocation where EmpNo = '" + dt.Rows[i]["EmpNo"].ToString() + "'";
                        SqlCommand cmd_AL = new SqlCommand(QueryAllocated, cn);
                        AllocatedLeave = Convert.ToString(cmd_AL.ExecuteScalar());
                        if ((AllocatedLeave == "") || (AllocatedLeave == null))
                        {
                            AllocatedLeave = "0";
                        }
                        string Query_Old = "Select Sum(NumberofLeave) as NumberofLeave from LeaveDetails where EmpNo = '" + dt.Rows[i]["EmpNo"].ToString() + "' and FinancialYear = '" + ddlFinaYear.SelectedValue + "' and LeaveType != '3'";
                        SqlCommand cmd_old = new SqlCommand(Query_Old, cn);
                        noofLeave = Convert.ToString(cmd_old.ExecuteScalar());
                        if ((noofLeave == "") || (noofLeave == null))
                        {
                            noofLeave = "0";
                        }
                        tempExcess = (Convert.ToInt32(AllocatedLeave) - Convert.ToInt32(noofLeave));
                        if (tempExcess < 0)
                        {
                            ExcessLeave = Convert.ToString((tempExcess * (-1)));

                        }
                        else
                        {
                            ExcessLeave = "0";
                        }
                        if (dt.Rows[i]["LeaveType"].ToString() == "Maternity Leave")
                        {
                            Maternalleave = "Y";
                        }
                        else
                        {
                            Maternalleave = "N";
                        }
                        string Fdate = Convert.ToDateTime(dt.Rows[i]["FromDate"].ToString()).ToShortDateString();

                        string Tdate = Convert.ToDateTime(dt.Rows[i]["ToDate"].ToString()).ToShortDateString();

                        DfromDate = DateTime.ParseExact(Fdate, "dd/MM/yyyy", null);
                        DtoDate = DateTime.ParseExact(Tdate, "dd/MM/yyyy", null);
                        LeavdaysCount = Convert.ToInt32((Convert.ToDateTime(DtoDate) - Convert.ToDateTime(DfromDate)).Days);
                        LeavdaysCount = LeavdaysCount + 1;
                        DataTable dtleave = new DataTable();
                        dtleave = objdata.AlreadyLeaveDateLoad(dt.Rows[i]["EmpNo"].ToString(), ddlFinaYear.SelectedValue, SessionCcode, SessionLcode);
                        if (dtleave.Rows.Count > 0)
                        {
                            for (int k = 0; k < dtleave.Rows.Count; k++)
                            {
                                tempfromDate = dtleave.Rows[k]["FromDate"].ToString();
                                temptodate = dtleave.Rows[k]["ToDate"].ToString();
                                for (DateTime m = Convert.ToDateTime(tempfromDate); m <= Convert.ToDateTime(temptodate); )
                                {
                                    if (m == DfromDate || (m == DtoDate))
                                    {
                                        CheckFlag = true;
                                    }
                                    else
                                    {

                                    }
                                    m = m.AddDays(1);
                                }

                            }


                            LeavdaysCount = Convert.ToInt32((Convert.ToDateTime(DtoDate) - Convert.ToDateTime(DfromDate)).Days);
                            LeavdaysCount = LeavdaysCount + 1;
                            if (CheckFlag == false)
                            {
                                string temp_approved = Convert.ToDateTime(dt.Rows[i]["Approveddate"].ToString()).ToShortDateString();
                                ApprovedDate = DateTime.ParseExact(temp_approved, "dd/MM/yyyy", null);
                                string Leavecode_temp = objdata.LeaveCode();
                                objdata.ApplyForLeave(dt.Rows[i]["EmpNo"].ToString(), LeaveCode, LeavdaysCount.ToString(), dt.Rows[i]["TotalWorkingDays"].ToString(), Leavecode_temp, DfromDate, DtoDate, dt.Rows[i]["Month"].ToString(), dt.Rows[i]["Year"].ToString(), AllocatedLeave, noofLeave, ExcessLeave, dt.Rows[i]["Approvededby"].ToString(), ApprovedDate, ddlFinaYear.SelectedValue, Maternalleave, SessionCcode, SessionLcode);
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert(' Upload Successfully...');", true);
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert(' Upload Correct Format...');", true);
                            }

                        }
                        else
                        {
                            LeavdaysCount = Convert.ToInt32((Convert.ToDateTime(DtoDate) - Convert.ToDateTime(DfromDate)).Days);
                            LeavdaysCount = LeavdaysCount + 1;
                            if (CheckFlag == false)
                            {
                                string temp_approved = Convert.ToDateTime(dt.Rows[i]["Approveddate"].ToString()).ToShortDateString();
                                ApprovedDate = DateTime.ParseExact(temp_approved, "dd/MM/yyyy", null);
                                string Leavecode_temp = objdata.LeaveCode();
                                objdata.ApplyForLeave(dt.Rows[i]["EmpNo"].ToString(), LeaveCode, LeavdaysCount.ToString(), dt.Rows[i]["TotalWorkingDays"].ToString(), Leavecode_temp, DfromDate, DtoDate, dt.Rows[i]["Month"].ToString(), dt.Rows[i]["Year"].ToString(), AllocatedLeave, noofLeave, ExcessLeave, dt.Rows[i]["Approvededby"].ToString(), ApprovedDate, ddlFinaYear.SelectedValue, Maternalleave, SessionCcode, SessionLcode);
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert(' Upload Successfully...');", true);
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert(' Upload Correct Format...');", true);
                            }
                        }
                    }

                    //System.Windows.Forms.MessageBox.Show("Uploaded Successfully", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    //panelsuccess.Visible = true;
                    lblError.Text = "";
                    //lbloutput.Text = "Uploaded Successfully";
                    btnUpload.Enabled = false;
                    FileUpload.Enabled = false;
                    //txtsheetname.Text = " ";
                    //Response.Redirect("UploadItems.aspx");

                }
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Your File Not Upload...');", true);
                //System.Windows.Forms.MessageBox.Show("Your File Not Upload", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);

            }
            //}
        }
        catch (Exception ex)
        {
            //Response.Write(ex.ToString());
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Please Upload Correct File Format...');", true);
            //System.Windows.Forms.MessageBox.Show("Please Upload Correct File Format", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            //lblError.Text = "Please Upload Correct Format.";
        }
    }
    protected void btnok_Click(object sender, EventArgs e)
    {
        FileUpload.Enabled = true;
        btnUpload.Enabled = true;
        panelsuccess.Visible = false;
        lbloutput.Text = "";
        lblError.Text = "";
    }
    protected void btnEmp_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptEmpDownload.aspx");
    }
    protected void btnatt_Click(object sender, EventArgs e)
    {
        Response.Redirect("AttenanceDownload.aspx");
    }
    protected void btnLeave_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptLeaveSample.aspx");
    }
    protected void btnOT_Click(object sender, EventArgs e)
    {
        Response.Redirect("OTDownload.aspx");
    }
    protected void btnSal_Click(object sender, EventArgs e)
    {
        Response.Redirect("FrmDeductionDownload.aspx");
    }
}
