﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Payroll;
using Payroll.Configuration;
using Payroll.Data;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class SPMSalaryMaster : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    string staffLabour;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    static string EmpType;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        string ss = Session["UserId"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        lblComany.Text = SessionCcode + " - " + SessionLcode;
        if (!IsPostBack)
        {
            //Department();
            //FoodDetails();
            category();
            clr();
            int currentYear = Utility.GetFinancialYear;
            for (int i = 0; i < 10; i++)
            {
                ddFinance.Items.Add(new ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                //  ddlShowYear.Items.Add(new ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                currentYear = currentYear - 1;
            }
        }
        lblusername.Text = Session["Usernmdisplay"].ToString();
    }
    public void category()
    {
        DataTable dtcate = new DataTable();
        dtcate = objdata.DropDownCategory();
        ddlcategory.DataSource = dtcate;
        ddlcategory.DataTextField = "CategoryName";
        ddlcategory.DataValueField = "CategoryCd";
        ddlcategory.DataBind();
    }
    public void clr()
    {
        rbsalary.SelectedIndex = -1;
        txtbase.Text = "0";
        txtFDA.Text = "0";
        txthra.Text = "0";
        //txttotal.Text = "0";
        txtUnion.Text = "0";
        txtVDA.Text = "0";
        txtWBasic.Text = "0";
        txtWAll1.Text = "0";
        txtWAll2.Text = "0";
        txtWded1.Text = "0";
        txtWded2.Text = "0";
        txtSBasic.Text = "0";
        txtSall1.Text = "0";
        txtSall2.Text = "0";
        txtSded1.Text = "0";
        txtSded2.Text = "0";
        txtOBasic.Text = "0";
        txtOUnion.Text = "0";
        txtOAll1.Text = "0";
        txtOAll2.Text = "0";
        txtOded1.Text = "0";
        txtOded2.Text = "0";
    }
    protected void ddlcategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        clr();
        string staffLabour_temp;
        DataTable dtempty = new DataTable();
        ddlDepartment.DataSource = dtempty;
        ddlDepartment.DataBind();
        ddlEmpName.DataSource = dtempty;
        ddlEmpName.DataBind();
        ddlempNo.DataSource = dtempty;
        ddlempNo.DataBind();
        ddToken.DataSource = dtempty;
        ddToken.DataBind();
        if (ddlcategory.SelectedValue == "1")
        {
            staffLabour_temp = "S";
        }
        else if (ddlcategory.SelectedValue == "2")
        {
            staffLabour_temp = "L";
        }
        else
        {
            staffLabour_temp = "0";
        }
        DataTable dtDip = new DataTable();
        dtDip = objdata.DropDownDepartment_category(staffLabour_temp);
        if (dtDip.Rows.Count > 1)
        {
            ddlDepartment.DataSource = dtDip;
            ddlDepartment.DataTextField = "DepartmentNm";
            ddlDepartment.DataValueField = "DepartmentCd";
            ddlDepartment.DataBind();
        }
        else
        {
            DataTable dt = new DataTable();
            ddlDepartment.DataSource = dt;
            ddlDepartment.DataBind();
        }
        PanelSpecial.Visible = false;
        Panelall.Visible = false;
        PanelStaff.Visible = false;
        PanelDaily.Visible = false;

    }
    protected void ddlDepartment_SelectedIndexChanged(object sender, EventArgs e)
    {
        PanelSpecial.Visible = false;
        Panelall.Visible = false;
        PanelStaff.Visible = false;
        PanelDaily.Visible = false;
        DataTable dtempty = new DataTable();
        ddlEmpName.DataSource = dtempty;
        ddlEmpName.DataBind();
        ddlempNo.DataSource = dtempty;
        ddlempNo.DataBind();
        ddToken.DataSource = dtempty;
        ddToken.DataBind();
    }
    protected void btnclick_Click(object sender, EventArgs e)
    {
        PanelSpecial.Visible = false;
        Panelall.Visible = false;
        PanelStaff.Visible = false;
        PanelDaily.Visible = false;
        bool ErrFlag = false;
        if (ddlDepartment.SelectedValue == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Department...!');", true);
            ErrFlag = true;
        }
        else if (ddlDepartment.SelectedValue == "0")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Department...!');", true);
            ErrFlag = true;
        }
        if (!ErrFlag)
        {
            if (ddlcategory.SelectedValue == "1")
            {

                //panelError.Visible = true;
                if (ddlcategory.SelectedValue == "1")
                {
                    staffLabour = "S";
                }
                else if (ddlcategory.SelectedValue == "2")
                {
                    staffLabour = "L";
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Category...!');", true);
                    //System.Windows.Forms.MessageBox.Show("Select the Category", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    //lblError.Text = "Select the Category";
                    ErrFlag = true;
                }
                if (!ErrFlag)
                {


                    DataTable dtempcode = new DataTable();
                    if (SessionAdmin == "1")
                    {
                        dtempcode = objdata.EmployeeLoadSalaryMaster(staffLabour, ddlDepartment.SelectedValue, SessionCcode, SessionLcode);
                    }
                    else
                    {
                        dtempcode = objdata.EmployeeLoadSalaryMaster_user(staffLabour, ddlDepartment.SelectedValue, SessionCcode, SessionLcode);
                    }

                    ddlempNo.DataSource = dtempcode;
                    DataRow dr = dtempcode.NewRow();
                    dr["EmpNo"] = ddlDepartment.SelectedItem.Text;
                    dr["EmpName"] = ddlDepartment.SelectedItem.Text;
                    dr["ExisistingCode"] = ddlDepartment.SelectedItem.Text;
                    dtempcode.Rows.InsertAt(dr, 0);
                    
                    
                    ddlempNo.DataTextField = "EmpNo";

                    ddlempNo.DataValueField = "EmpNo";
                    ddlempNo.DataBind();
                    ddlEmpName.DataSource = dtempcode;
                    ddlEmpName.DataTextField = "EmpName";
                    ddlEmpName.DataValueField = "EmpNo";
                    ddlEmpName.DataBind();

                    ddToken.DataSource = dtempcode;
                    ddToken.DataTextField = "ExisistingCode";
                    ddToken.DataValueField = "ExisistingCode";
                    ddToken.DataBind();
                    //PanelStaff.Visible = true;
                }
                clr();
            }
            else if (ddlcategory.SelectedValue == "2")
            {
                staffLabour = "L";

                DataTable dtempcode = new DataTable();
                dtempcode = objdata.EmployeeLoadSalaryMaster(staffLabour, ddlDepartment.SelectedValue, SessionCcode, SessionLcode);
                //EmpType = dtempcode.Rows[0]["EmployeeType"].ToString();
               
                ddlempNo.DataSource = dtempcode;

                DataRow dr = dtempcode.NewRow();
                dr["EmpNo"] = ddlDepartment.SelectedItem.Text;
                dr["EmpName"] = ddlDepartment.SelectedItem.Text;
                dr["ExisistingCode"] = ddlDepartment.SelectedItem.Text;
                dtempcode.Rows.InsertAt(dr, 0);

                ddlempNo.DataTextField = "EmpNo";
                ddlempNo.DataValueField = "EmpNo";
                ddlempNo.DataBind();
                ddlEmpName.DataSource = dtempcode;
                ddlEmpName.DataTextField = "EmpName";
                ddlEmpName.DataValueField = "EmpNo";
                ddlEmpName.DataBind();

                ddToken.DataSource = dtempcode;
                ddToken.DataTextField = "ExisistingCode";
                ddToken.DataValueField = "ExisistingCode";
                ddToken.DataBind();

            }
        }
    }

    protected void ddFinance_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void btnexitingSearch_Click(object sender, EventArgs e)
    {
        try
        {
            bool ErrFlag = false;
            clr();
            if (ddlcategory.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select Category Staff or Labour');", true);

                ErrFlag = true;
            }
            else if (ddlDepartment.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select Department');", true);
                //System.Windows.Forms.MessageBox.Show("Select the Department", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                //lblError.Text = "Select Department";
                ErrFlag = true;
            }
            //else if (txtexistingNo.Text == "")
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Existing Number');", true);
            //    //System.Windows.Forms.MessageBox.Show("Enter the Existing Number", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            //    //lblError.Text = "Enter the Existing Number";
            //    ErrFlag = true;
            //}
            if (!ErrFlag)
            {
                DataTable dtempcode = new DataTable();
                string Cate = "";
                if (ddlcategory.SelectedValue == "1")
                {
                    Cate = "S";
                }
                else if (ddlcategory.SelectedValue == "2")
                {
                    Cate = "L";
                }

                dtempcode = objdata.Salary_ExistNo(ddlDepartment.SelectedValue, txtexistingNo.Text, Cate, SessionCcode, SessionLcode, SessionAdmin);
                EmpType = dtempcode.Rows[0]["EmployeeType"].ToString();

                if (EmpType == "1")
                {
                    //PanelSpecial.Visible = true;
                    PanelStaff.Visible = true;
                    //PanelDaily.Visible = false;
                    //Panelall.Visible = false;
                }

                else
                {
                    PanelStaff.Visible = true;
                    //PanelDaily.Visible = false;
                    //Panelall.Visible = false;
                    //PanelSpecial.Visible = false;
                }
                if (dtempcode.Rows.Count > 0)
                {
                    ddlempNo.SelectedValue = dtempcode.Rows[0]["EmpNo"].ToString();
                    ddToken.SelectedValue = dtempcode.Rows[0]["ExisistingCode"].ToString();
                    ddlEmpName.DataSource = dtempcode;
                    ddlEmpName.DataTextField = "EmpName";
                    ddlEmpName.DataValueField = "EmpNo";
                    ddlEmpName.DataBind();
                    if (dtempcode.Rows[0]["Wagestype"].ToString() == "1")
                    {
                        rbsalary.SelectedValue = "1";
                    }
                    else if (dtempcode.Rows[0]["Wagestype"].ToString() == "2")
                    {
                        rbsalary.SelectedValue = "2";
                    }
                    else if (dtempcode.Rows[0]["Wagestype"].ToString() == "3")
                    {
                        rbsalary.SelectedValue = "3";
                    }
                    string empCodeverify = objdata.SalaryMasterVerify(ddlempNo.SelectedValue, SessionCcode, SessionLcode);
                    if (empCodeverify == ddlempNo.SelectedValue)
                    {
                        DataTable dtempLoad = new DataTable();
                        dtempLoad = objdata.SalaryMasterEmpLoad(ddlempNo.SelectedValue, SessionCcode, SessionLcode);
                        if (dtempLoad.Rows.Count > 0)
                        {
                            if (EmpType == "1")
                            {
                                txtexistingNo.Text = dtempLoad.Rows[0]["ExisistingCode"].ToString();
                                txtSBasic.Text = dtempLoad.Rows[0]["Base"].ToString();
                                txtStaffTA.Text = dtempLoad.Rows[0]["FDA"].ToString();
                                txtVDA.Text = dtempLoad.Rows[0]["VDA"].ToString();
                                txtStaffHRA.Text = dtempLoad.Rows[0]["HRA"].ToString();

                                txtSall1.Text = dtempLoad.Rows[0]["Alllowance1amt"].ToString();
                                txtSall2.Text = dtempLoad.Rows[0]["Allowance2amt"].ToString();
                                txtSded1.Text = dtempLoad.Rows[0]["Deduction1"].ToString();
                                txtSded2.Text = dtempLoad.Rows[0]["Deduction2"].ToString();
                                txtPF.Text = dtempLoad.Rows[0]["PFS"].ToString();

                                //txtSBasic.Text = "0";
                                //txtSall1.Text = "0";
                                //txtSall2.Text = "0";
                                //txtSded1.Text = "0";
                                //txtSded2.Text = "0";
                                //txtPF.Text = "0";
                            }
                            else
                            {
                                txtSBasic.Text = dtempLoad.Rows[0]["Base"].ToString();
                                txtStaffHRA.Text = "0.00";
                                txtStaffTA.Text = "0.00";

                                txtSall1.Text = dtempLoad.Rows[0]["Alllowance1amt"].ToString();
                                txtSall2.Text = dtempLoad.Rows[0]["Allowance2amt"].ToString();
                                txtSded1.Text = dtempLoad.Rows[0]["Deduction1"].ToString();
                                txtSded2.Text = dtempLoad.Rows[0]["Deduction2"].ToString();
                                txtPF.Text = dtempLoad.Rows[0]["PFS"].ToString();
                            }


                        }
                        else
                        {
                            txtbase.Text = "0";
                            txtFDA.Text = "0";
                            txthra.Text = "0";
                            txtVDA.Text = "0";
                            //txttotal.Text = "0";
                            txtUnion.Text = "0";
                            txAllowance1.Text = "0";
                            txtAllowance2.Text = "0";
                            txtDeduction1.Text = "0";
                            txtDeduction2.Text = "0";
                            txtWBasic.Text = "0";
                            txtWAll1.Text = "0";
                            txtWAll2.Text = "0";
                            txtWded1.Text = "0";
                            txtWded2.Text = "0";
                            txtSBasic.Text = "0";
                            txtSall1.Text = "0";
                            txtSall2.Text = "0";
                            txtSded1.Text = "0";
                            txtSded2.Text = "0";
                            txtOBasic.Text = "0";
                            txtOUnion.Text = "0";
                            txtOAll1.Text = "0";
                            txtOAll2.Text = "0";
                            txtOded1.Text = "0";
                            txtOded2.Text = "0";
                            txtPF.Text = "0";

                        }

                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Employee Data Not Found');", true);
                    clr();
                }
            }
        }
        catch (Exception ex)
        {
        }
    }
    protected void ddlempNo_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            bool ErrFlag = false;
            if (ddlempNo.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Employee Code....!');", true);
                ErrFlag = true;
            }
            if (!ErrFlag)
            {
                DataTable dtempName = new DataTable();
                dtempName = objdata.BonusEmployeeName(ddlempNo.SelectedValue, SessionCcode, SessionLcode);
                ddlEmpName.DataSource = dtempName;
                //DataRow dr = dtempName.NewRow();
                //dr["EmpNo"] = ddlDepartment.SelectedItem.Text;
                //dr["EmpName"] = ddlDepartment.SelectedItem.Text;
                //dtempName.Rows.InsertAt(dr, 0);
                ddlEmpName.DataTextField = "EmpName";
                ddlEmpName.DataValueField = "EmpNo";
                ddlEmpName.DataBind();
                ddToken.DataSource = dtempName;
                ddToken.DataTextField = "ExisistingCode";
                ddToken.DataValueField = "ExisistingCode";
                ddToken.DataBind();
                txtexistingNo.Text = dtempName.Rows[0]["ExisistingCode"].ToString();
                clr();
                string empCodeverify = objdata.SalaryMasterVerify(ddlempNo.SelectedValue, SessionCcode, SessionLcode);
                DataTable dtempLoad = new DataTable();
                dtempLoad = objdata.SalaryMasterEmpLoad(ddlempNo.SelectedValue, SessionCcode, SessionLcode);
                EmpType = dtempName.Rows[0]["EmployeeType"].ToString();
                if (dtempName.Rows[0]["Wagestype"].ToString() == "1")
                {
                    rbsalary.SelectedValue = "1";
                }
                else if (dtempName.Rows[0]["Wagestype"].ToString() == "2")
                {
                    rbsalary.SelectedValue = "2";
                }
                else if (dtempName.Rows[0]["Wagestype"].ToString() == "3")
                {
                    rbsalary.SelectedValue = "3";
                }
                if (EmpType == "1")
                {
                    PanelStaff.Visible = false;
                    //PanelDaily.Visible = false;
                    Panelall.Visible = true;
                    //PanelSpecial.Visible = false;
                }
                else if (EmpType == "2")
                {
                    PanelStaff.Visible = true;
                    //PanelDaily.Visible = false;
                    Panelall.Visible = false;
                    //PanelSpecial.Visible = false;
                }
                else if (EmpType == "3")
                {
                    PanelStaff.Visible = false;
                    //PanelDaily.Visible = true;
                    Panelall.Visible = true;
                    //PanelSpecial.Visible = false;
                }
                else if (EmpType == "4")
                {
                    //PanelSpecial.Visible = true;
                    PanelStaff.Visible = true;
                    //PanelDaily.Visible = false;
                    Panelall.Visible = false;
                }
                else
                {
                    PanelStaff.Visible = true;
                    //PanelDaily.Visible = false;
                    Panelall.Visible = false;
                    //PanelSpecial.Visible = false;
                }
                if (empCodeverify == ddlempNo.SelectedValue)
                {
                    
                    
                    if (dtempLoad.Rows.Count > 0)
                    {
                        if (EmpType == "1" || EmpType == "3")
                        {
                            txtexistingNo.Text = dtempLoad.Rows[0]["ExisistingCode"].ToString();
                            txtbase.Text = dtempLoad.Rows[0]["Base"].ToString();
                            txtFDA.Text = dtempLoad.Rows[0]["FDA"].ToString();
                            txtVDA.Text = dtempLoad.Rows[0]["VDA"].ToString();
                            txthra.Text = dtempLoad.Rows[0]["HRA"].ToString();

                            txAllowance1.Text = dtempLoad.Rows[0]["Alllowance1amt"].ToString();
                            txtAllowance2.Text = dtempLoad.Rows[0]["Allowance2amt"].ToString();
                            txtDeduction1.Text = dtempLoad.Rows[0]["Deduction1"].ToString();
                            txtDeduction2.Text = dtempLoad.Rows[0]["Deduction2"].ToString();
                            txtUnion.Text = dtempLoad.Rows[0]["PFS"].ToString();

                            txtSBasic.Text = "0";
                            txtSall1.Text = "0";
                            txtSall2.Text = "0";
                            txtSded1.Text = "0";
                            txtSded2.Text = "0";
                            txtPF.Text = "0";
                        }
                        else
                        {
                            txtSBasic.Text = dtempLoad.Rows[0]["Base"].ToString();
                            txtSall1.Text = dtempLoad.Rows[0]["Alllowance1amt"].ToString();
                            txtSall2.Text = dtempLoad.Rows[0]["Allowance2amt"].ToString();
                            txtSded1.Text = dtempLoad.Rows[0]["Deduction1"].ToString();
                            txtSded2.Text = dtempLoad.Rows[0]["Deduction2"].ToString();
                            txtPF.Text = dtempLoad.Rows[0]["PFS"].ToString();
                            txtexistingNo.Text = dtempLoad.Rows[0]["ExisistingCode"].ToString();
                        }
                        //if (EmpType == "1")
                        //{
                        //    txtSBasic.Text = dtempLoad.Rows[0]["Base"].ToString();
                        //    txtSall1.Text = dtempLoad.Rows[0]["Alllowance1amt"].ToString();
                        //    txtSall2.Text = dtempLoad.Rows[0]["Allowance2amt"].ToString();
                        //    txtSded1.Text = dtempLoad.Rows[0]["Deduction1"].ToString();
                        //    txtSded2.Text = dtempLoad.Rows[0]["Deduction2"].ToString();
                        //}
                        //else if (EmpType == "2")
                        //{
                        //    txtbase.Text = dtempLoad.Rows[0]["Base"].ToString();
                        //    txtFDA.Text = dtempLoad.Rows[0]["FDA"].ToString();
                        //    txthra.Text = dtempLoad.Rows[0]["HRA"].ToString();
                        //    txtVDA.Text = dtempLoad.Rows[0]["VDA"].ToString();
                        //    //txttotal.Text = dtempLoad.Rows[0]["Total"].ToString();
                        //    txtUnion.Text = dtempLoad.Rows[0]["Unioncharges"].ToString();
                        //    txAllowance1.Text = dtempLoad.Rows[0]["Alllowance1amt"].ToString();
                        //    txtAllowance2.Text = dtempLoad.Rows[0]["Allowance2amt"].ToString();
                        //    txtDeduction1.Text = dtempLoad.Rows[0]["Deduction1"].ToString();
                        //    txtDeduction2.Text = dtempLoad.Rows[0]["Deduction2"].ToString();
                        //}
                        //else if (EmpType == "3")
                        //{
                        //    txtWBasic.Text = dtempLoad.Rows[0]["Base"].ToString();
                        //    txtWAll1.Text = dtempLoad.Rows[0]["Alllowance1amt"].ToString();
                        //    txtWAll2.Text = dtempLoad.Rows[0]["Allowance2amt"].ToString();
                        //    txtWded1.Text = dtempLoad.Rows[0]["Deduction1"].ToString();
                        //    txtWded2.Text = dtempLoad.Rows[0]["Deduction2"].ToString();
                        //}
                        //else if (EmpType == "4")
                        //{
                        //    txtOBasic.Text = dtempLoad.Rows[0]["Base"].ToString();
                        //    txtOUnion.Text = dtempLoad.Rows[0]["Unioncharges"].ToString();
                        //    txtOAll1.Text = dtempLoad.Rows[0]["Alllowance1amt"].ToString();
                        //    txtOAll2.Text = dtempLoad.Rows[0]["Allowance2amt"].ToString();
                        //    txtOded1.Text = dtempLoad.Rows[0]["Deduction1"].ToString();
                        //    txtOded2.Text = dtempLoad.Rows[0]["Deduction2"].ToString();
                        //}
                        //else
                        //{
                        //    txtbase.Text = dtempLoad.Rows[0]["Base"].ToString();
                        //    txtFDA.Text = dtempLoad.Rows[0]["FDA"].ToString();
                        //    txthra.Text = dtempLoad.Rows[0]["HRA"].ToString();
                        //    txtVDA.Text = dtempLoad.Rows[0]["VDA"].ToString();
                        //    //txttotal.Text = dtempLoad.Rows[0]["Total"].ToString();
                        //    txtUnion.Text = dtempLoad.Rows[0]["Unioncharges"].ToString();
                        //    txAllowance1.Text = dtempLoad.Rows[0]["Alllowance1amt"].ToString();
                        //    txtAllowance2.Text = dtempLoad.Rows[0]["Allowance2amt"].ToString();
                        //    txtDeduction1.Text = dtempLoad.Rows[0]["Deduction1"].ToString();
                        //    txtDeduction2.Text = dtempLoad.Rows[0]["Deduction2"].ToString();
                        //}
                    }
                    else
                    {
                        txtbase.Text = "0";
                        txtFDA.Text = "0";
                        txthra.Text = "0";
                        txtVDA.Text = "0";
                        //txttotal.Text = "0";
                        txtUnion.Text = "0";
                        txAllowance1.Text = "0";
                        txtAllowance2.Text = "0";
                        txtDeduction1.Text = "0";
                        txtDeduction2.Text = "0";
                        txtWBasic.Text = "0";
                        txtWAll1.Text = "0";
                        txtWAll2.Text = "0";
                        txtWded1.Text = "0";
                        txtWded2.Text = "0";
                        txtSBasic.Text = "0";
                        txtSall1.Text = "0";
                        txtSall2.Text = "0";
                        txtSded1.Text = "0";
                        txtSded2.Text = "0";
                        txtOBasic.Text = "0";
                        txtOUnion.Text = "0";
                        txtOAll1.Text = "0";
                        txtOAll2.Text = "0";
                        txtOded1.Text = "0";
                        txtOded2.Text = "0";
                        txtPF.Text = "0";
                    }
                }

            }

        }
        catch (Exception ex)
        {
        }


    }
    protected void ddlEmpName_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            bool ErrFlag = false;
            if (ddlEmpName.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Employee Code....!');", true);
                ErrFlag = true;
            }
            if (!ErrFlag)
            {
                DataTable dtemp2 = new DataTable();
                dtemp2 = objdata.BonusEmployeeName(ddlEmpName.SelectedValue, SessionCcode, SessionLcode);

                ddlempNo.DataSource = dtemp2;
                ddlempNo.DataTextField = "EmpNo";
                ddlempNo.DataValueField = "EmpNo";
                ddlempNo.DataBind();
                txtexistingNo.Text = dtemp2.Rows[0]["ExisistingCode"].ToString();
                ddToken.DataSource = dtemp2;
                ddToken.DataTextField = "ExisistingCode";
                ddToken.DataValueField = "ExisistingCode";
                ddToken.DataBind();
                clr();
                string empCodeverify = objdata.SalaryMasterVerify(ddlempNo.SelectedValue, SessionCcode, SessionLcode);
                DataTable dtempLoad = new DataTable();
                dtempLoad = objdata.SalaryMasterEmpLoad(ddlempNo.SelectedValue, SessionCcode, SessionLcode);
                EmpType = dtemp2.Rows[0]["EmployeeType"].ToString();
                if (dtemp2.Rows[0]["Wagestype"].ToString() == "1")
                {
                    rbsalary.SelectedValue = "1";
                }
                else if (dtemp2.Rows[0]["Wagestype"].ToString() == "2")
                {
                    rbsalary.SelectedValue = "2";
                }
                else if (dtemp2.Rows[0]["Wagestype"].ToString() == "3")
                {
                    rbsalary.SelectedValue = "3";
                }
                if (EmpType == "1")
                {
                    PanelStaff.Visible = false;
                    //PanelDaily.Visible = false;
                    Panelall.Visible = true;
                    //PanelSpecial.Visible = false;
                }
                else if (EmpType == "2")
                {
                    PanelStaff.Visible = true;
                    //PanelDaily.Visible = false;
                    Panelall.Visible = false;
                    //PanelSpecial.Visible = false;
                }
                else if (EmpType == "3")
                {
                    PanelStaff.Visible = false;
                    //PanelDaily.Visible = true;
                    Panelall.Visible = true;
                    //PanelSpecial.Visible = false;
                }
                else if (EmpType == "4")
                {
                    //PanelSpecial.Visible = true;
                    PanelStaff.Visible = true;
                    //PanelDaily.Visible = false;
                    Panelall.Visible = false;
                }
                else
                {
                    PanelStaff.Visible = true;
                    //PanelDaily.Visible = false;
                    Panelall.Visible = false;
                    //PanelSpecial.Visible = false;
                }
                if (empCodeverify == ddlempNo.SelectedValue)
                {
                    
                    if (dtempLoad.Rows.Count > 0)
                    {
                        if (EmpType == "1" || EmpType == "3")
                        {
                            txtexistingNo.Text = dtempLoad.Rows[0]["ExisistingCode"].ToString();
                            txtbase.Text = dtempLoad.Rows[0]["Base"].ToString();
                            txtFDA.Text = dtempLoad.Rows[0]["FDA"].ToString();
                            txtVDA.Text = dtempLoad.Rows[0]["VDA"].ToString();
                            txthra.Text = dtempLoad.Rows[0]["HRA"].ToString();

                            txAllowance1.Text = dtempLoad.Rows[0]["Alllowance1amt"].ToString();
                            txtAllowance2.Text = dtempLoad.Rows[0]["Allowance2amt"].ToString();
                            txtDeduction1.Text = dtempLoad.Rows[0]["Deduction1"].ToString();
                            txtDeduction2.Text = dtempLoad.Rows[0]["Deduction2"].ToString();
                            txtUnion.Text = dtempLoad.Rows[0]["PFS"].ToString();

                            txtSBasic.Text = "0";
                            txtSall1.Text = "0";
                            txtSall2.Text = "0";
                            txtSded1.Text = "0";
                            txtSded2.Text = "0";
                            txtPF.Text = "0";
                        }
                        else
                        {
                            txtexistingNo.Text = dtempLoad.Rows[0]["ExisistingCode"].ToString();
                            txtSBasic.Text = dtempLoad.Rows[0]["Base"].ToString();
                            txtSall1.Text = dtempLoad.Rows[0]["Alllowance1amt"].ToString();
                            txtSall2.Text = dtempLoad.Rows[0]["Allowance2amt"].ToString();
                            txtSded1.Text = dtempLoad.Rows[0]["Deduction1"].ToString();
                            txtSded2.Text = dtempLoad.Rows[0]["Deduction2"].ToString();
                            txtPF.Text = dtempLoad.Rows[0]["PFS"].ToString();
                        }
                        //Profilelbl.Text = dtempLoad.Rows[0]["ProfileType"].ToString();
                        //if (EmpType == "1")
                        //{
                        //    txtSBasic.Text = dtempLoad.Rows[0]["Base"].ToString();
                        //    txtSall1.Text = dtempLoad.Rows[0]["Alllowance1amt"].ToString();
                        //    txtSall2.Text = dtempLoad.Rows[0]["Allowance2amt"].ToString();
                        //    txtSded1.Text = dtempLoad.Rows[0]["Deduction1"].ToString();
                        //    txtSded2.Text = dtempLoad.Rows[0]["Deduction2"].ToString();
                        //}
                        //else if (EmpType == "2")
                        //{
                        //    txtbase.Text = dtempLoad.Rows[0]["Base"].ToString();
                        //    txtFDA.Text = dtempLoad.Rows[0]["FDA"].ToString();
                        //    txthra.Text = dtempLoad.Rows[0]["HRA"].ToString();
                        //    txtVDA.Text = dtempLoad.Rows[0]["VDA"].ToString();
                        //    //txttotal.Text = dtempLoad.Rows[0]["Total"].ToString();
                        //    txtUnion.Text = dtempLoad.Rows[0]["Unioncharges"].ToString();
                        //    txAllowance1.Text = dtempLoad.Rows[0]["Alllowance1amt"].ToString();
                        //    txtAllowance2.Text = dtempLoad.Rows[0]["Allowance2amt"].ToString();
                        //    txtDeduction1.Text = dtempLoad.Rows[0]["Deduction1"].ToString();
                        //    txtDeduction2.Text = dtempLoad.Rows[0]["Deduction2"].ToString();
                        //}
                        //else if (EmpType == "3")
                        //{
                        //    txtWBasic.Text = dtempLoad.Rows[0]["Base"].ToString();
                        //    txtWAll1.Text = dtempLoad.Rows[0]["Alllowance1amt"].ToString();
                        //    txtWAll2.Text = dtempLoad.Rows[0]["Allowance2amt"].ToString();
                        //    txtWded1.Text = dtempLoad.Rows[0]["Deduction1"].ToString();
                        //    txtWded2.Text = dtempLoad.Rows[0]["Deduction2"].ToString();
                        //}
                        //else if (EmpType == "4")
                        //{
                        //    txtOBasic.Text = dtempLoad.Rows[0]["Base"].ToString();
                        //    txtOUnion.Text = dtempLoad.Rows[0]["Unioncharges"].ToString();
                        //    txtOAll1.Text = dtempLoad.Rows[0]["Alllowance1amt"].ToString();
                        //    txtOAll2.Text = dtempLoad.Rows[0]["Allowance2amt"].ToString();
                        //    txtOded1.Text = dtempLoad.Rows[0]["Deduction1"].ToString();
                        //    txtOded2.Text = dtempLoad.Rows[0]["Deduction2"].ToString();
                        //}
                        //else
                        //{
                        //    txtbase.Text = dtempLoad.Rows[0]["Base"].ToString();
                        //    txtFDA.Text = dtempLoad.Rows[0]["FDA"].ToString();
                        //    txthra.Text = dtempLoad.Rows[0]["HRA"].ToString();
                        //    txtVDA.Text = dtempLoad.Rows[0]["VDA"].ToString();
                        //    //txttotal.Text = dtempLoad.Rows[0]["Total"].ToString();
                        //    txtUnion.Text = dtempLoad.Rows[0]["Unioncharges"].ToString();
                        //    txAllowance1.Text = dtempLoad.Rows[0]["Alllowance1amt"].ToString();
                        //    txtAllowance2.Text = dtempLoad.Rows[0]["Allowance2amt"].ToString();
                        //    txtDeduction1.Text = dtempLoad.Rows[0]["Deduction1"].ToString();
                        //    txtDeduction2.Text = dtempLoad.Rows[0]["Deduction2"].ToString();
                        //}
                    }
                    else
                    {
                        txtbase.Text = "0";
                        txtFDA.Text = "0";
                        txthra.Text = "0";
                        txtVDA.Text = "0";
                        //txttotal.Text = "0";
                        txtUnion.Text = "0";
                        txAllowance1.Text = "0";
                        txtAllowance2.Text = "0";
                        txtDeduction1.Text = "0";
                        txtDeduction2.Text = "0";
                        txtWBasic.Text = "0";
                        txtWAll1.Text = "0";
                        txtWAll2.Text = "0";
                        txtWded1.Text = "0";
                        txtWded2.Text = "0";
                        txtSBasic.Text = "0";
                        txtSall1.Text = "0";
                        txtSall2.Text = "0";
                        txtSded1.Text = "0";
                        txtSded2.Text = "0";
                        txtOBasic.Text = "0";
                        txtOUnion.Text = "0";
                        txtOAll1.Text = "0";
                        txtOAll2.Text = "0";
                        txtOded1.Text = "0";
                        txtOded2.Text = "0";
                        txtPF.Text = "0";
                    }
                }
            }
        }
        catch (Exception ex)
        {
        }
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            bool ErrFlag = false;
            bool Update = false;
            bool Insert = false;
         
            if (EmpType == "1") //STAFF SALARY MASTER
            {
                if (txtSBasic.Text.Trim() == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Basic Salary....!');", true);
                    ErrFlag = true;
                }
                else if (Convert.ToDecimal(txtSBasic.Text) == 0)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Basic Salary Properly....!');", true);
                    ErrFlag = true;
                }
                if (!ErrFlag)
                {
                    SalaryMasterClass objStaffSal = new SalaryMasterClass();

                    objStaffSal.EmpNo = ddlempNo.SelectedValue;
                    objStaffSal.Ccode = SessionCcode;
                    objStaffSal.Lcode = SessionLcode;
                    objStaffSal.Base = txtSBasic.Text;
                    objStaffSal.HRA = txtStaffHRA.Text;
                    objStaffSal.FDA = txtStaffTA.Text;
                    objStaffSal.VDA = txtVDA.Text;
                    objStaffSal.union = "0";
                    objStaffSal.total = "0";
                    objStaffSal.PFsalary = txtPF.Text;
                    objStaffSal.Allowance1Amt = txtSall1.Text;
                    objStaffSal.Allowance2Amt = txtSall2.Text;
                    objStaffSal.deduction1 = txtSded1.Text;
                    objStaffSal.deduction2 = txtSded2.Text;
                    objStaffSal.EmpType = EmpType;

                    string StaffEmpCodeVerify = objdata.SalaryMasterVerify(ddlempNo.SelectedValue, SessionCcode, SessionLcode);
                    if (StaffEmpCodeVerify == ddlempNo.SelectedValue)
                    {
                        objdata.SalaryMaster_Update(objStaffSal);
                        Update = true;
                    }
                    else
                    {
                        objdata.SalaryMaster_Insert(objStaffSal);
                        Insert = true;
                    }
                    if (Insert == true)
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Saved Successfully....!');", true);

                    }
                    if (Update == true)
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Updated Successfully....!');", true);

                    }
                }
            }
         
            else
            {
                if (txtSBasic.Text.Trim() == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Basic Salary....!');", true);
                    ErrFlag = true;
                }
                else if (Convert.ToDecimal(txtSBasic.Text) == 0)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Basic Salary Properly....!');", true);
                    ErrFlag = true;
                }
                else if (txtSall1.Text.Trim() == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Allowance 1 Amount....!');", true);
                    ErrFlag = true;
                }
                else if (txtSall2.Text.Trim() == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Allowance 2 Amount....!');", true);
                    ErrFlag = true;
                }
                else if (txtSded1.Text.Trim() == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Deduction 1 Amount....!');", true);
                    ErrFlag = true;
                }
                else if (txtSded2.Text.Trim() == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Deduction 2 Amount....!');", true);
                    ErrFlag = true;
                }
                else if (txtPF.Text.Trim() == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the PF Salary....!');", true);
                    ErrFlag = true;
                }
                if (!ErrFlag)
                {
                    SalaryMasterClass objsal = new SalaryMasterClass();




                    objsal.EmpNo = ddlempNo.SelectedValue;
                    objsal.Ccode = SessionCcode;
                    objsal.Lcode = SessionLcode;
                    objsal.Base = txtSBasic.Text;
                    objsal.HRA = txtStaffHRA.Text;
                    objsal.FDA = txtStaffTA.Text;
                    objsal.VDA = txtVDA.Text;
                    objsal.union = "0";
                    objsal.total = "0";
                    objsal.PFsalary = txtPF.Text;
                    objsal.Allowance1Amt = txtSall1.Text;
                    objsal.Allowance2Amt = txtSall2.Text;
                    objsal.deduction1 = txtSded1.Text;
                    objsal.deduction2 = txtSded2.Text;
                    objsal.EmpType = EmpType;

                    string empCodeverify = objdata.SalaryMasterVerify(ddlempNo.SelectedValue, SessionCcode, SessionLcode);
                    if (empCodeverify == ddlempNo.SelectedValue)
                    {
                        objdata.SalaryMaster_Update(objsal);
                        Update = true;
                    }
                    else
                    {
                        objdata.SalaryMaster_Insert(objsal);
                        Insert = true;
                    }
                    if (Insert == true)
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Saved Successfully....!');", true);

                    }
                    if (Update == true)
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Updated Successfully....!');", true);

                    }
                }
            }
          
        }
        catch (Exception ex)
        {
        }
    }
    protected void btnreset_Click(object sender, EventArgs e)
    {
        clr();
    }
    protected void btncancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("SalaryMaster.aspx");
    }
    protected void btnCal_Click(object sender, EventArgs e)
    {
        //try
        //{
        //    bool ErrFlag = false;
        //    if (txtbase.Text.Trim() == "")
        //    {
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Basic Salary....!');", true);
        //        ErrFlag = true;
        //    }
        //    else if (txtFDA.Text.Trim() == "")
        //    {
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the FDA....!');", true);
        //        ErrFlag = true;
        //    }
        //    else if (txthra.Text.Trim() == "")
        //    {
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the HRA....!');", true);
        //        ErrFlag = true;
        //    }

        //    else if (txtVDA.Text.Trim() == "")
        //    {
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the VDA....!');", true);
        //        ErrFlag = true;
        //    }
        //    //else if (txtUnion.Text.Trim() == "")
        //    //{
        //    //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Union Charges....!');", true);
        //    //    ErrFlag = true;
        //    //}
        //    else if (Convert.ToDecimal(txtbase.Text.Trim()) == 0)
        //    {
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Basic Salary....!');", true);
        //        ErrFlag = true;
        //    }
        //    if (!ErrFlag)
        //    {
        //        txttotal.Text = (Convert.ToDecimal(txtbase.Text.Trim()) + Convert.ToDecimal(txtFDA.Text.Trim()) + Convert.ToDecimal(txtVDA.Text.Trim()) + Convert.ToDecimal(txthra.Text.Trim())).ToString();
        //        txttotal.Text = (Math.Round(Convert.ToDecimal(txttotal.Text), 4, MidpointRounding.ToEven)).ToString();

        //    }
        //}
        //catch (Exception ex)
        //{
        //}

    }
    protected void btnEmp_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptEmpDownload.aspx");
    }
    protected void btnatt_Click(object sender, EventArgs e)
    {
        Response.Redirect("AttenanceDownload.aspx");
    }
    protected void btnLeave_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptLeaveSample.aspx");
    }
    protected void btnOT_Click(object sender, EventArgs e)
    {
        Response.Redirect("OTDownload.aspx");
    }
    protected void btncontract_Click(object sender, EventArgs e)
    {
        Response.Redirect("ContractRPT.aspx");
    }
    protected void ddToken_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            bool ErrFlag = false;
            clr();
            if (ddlcategory.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select Category Staff or Labour');", true);

                ErrFlag = true;
            }
            else if (ddlDepartment.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select Department');", true);
                //System.Windows.Forms.MessageBox.Show("Select the Department", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                //lblError.Text = "Select Department";
                ErrFlag = true;
            }
            //else if (txtexistingNo.Text == "")
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Existing Number');", true);
            //    //System.Windows.Forms.MessageBox.Show("Enter the Existing Number", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            //    //lblError.Text = "Enter the Existing Number";
            //    ErrFlag = true;
            //}
            if (!ErrFlag)
            {
                DataTable dtempcode = new DataTable();
                string Cate = "";
                if (ddlcategory.SelectedValue == "1")
                {
                    Cate = "S";
                }
                else if (ddlcategory.SelectedValue == "2")
                {
                    Cate = "L";
                }

                dtempcode = objdata.Salary_ExistNo(ddlDepartment.SelectedValue, ddToken.SelectedValue, Cate, SessionCcode, SessionLcode, SessionAdmin);
                EmpType = dtempcode.Rows[0]["EmployeeType"].ToString();

                if (EmpType == "1")
                {
                    //PanelSpecial.Visible = true;
                    PanelStaff.Visible = true;
                    //PanelDaily.Visible = false;
                    //Panelall.Visible = false;
                }
            
                else
                {
                    PanelStaff.Visible = true;
                    //PanelDaily.Visible = false;
                    //Panelall.Visible = false;
                    //PanelSpecial.Visible = false;
                }
                if (dtempcode.Rows.Count > 0)
                {
                    ddlempNo.SelectedValue = dtempcode.Rows[0]["EmpNo"].ToString();
                    ddlEmpName.DataSource = dtempcode;
                    ddlEmpName.DataTextField = "EmpName";
                    ddlEmpName.DataValueField = "EmpNo";
                    ddlEmpName.DataBind();
                    if (dtempcode.Rows[0]["Wagestype"].ToString() == "1")
                    {
                        rbsalary.SelectedValue = "1";
                    }
                    else if (dtempcode.Rows[0]["Wagestype"].ToString() == "2")
                    {
                        rbsalary.SelectedValue = "2";
                    }
                    else if (dtempcode.Rows[0]["Wagestype"].ToString() == "3")
                    {
                        rbsalary.SelectedValue = "3";
                    }
                    string empCodeverify = objdata.SalaryMasterVerify(ddlempNo.SelectedValue, SessionCcode, SessionLcode);
                    if (empCodeverify == ddlempNo.SelectedValue)
                    {
                        DataTable dtempLoad = new DataTable();
                        dtempLoad = objdata.SalaryMasterEmpLoad(ddlempNo.SelectedValue, SessionCcode, SessionLcode);
                        if (dtempLoad.Rows.Count > 0)
                        {
                            if (EmpType == "1")
                            {
                                txtexistingNo.Text = dtempLoad.Rows[0]["ExisistingCode"].ToString();
                                txtSBasic.Text = dtempLoad.Rows[0]["Base"].ToString();
                                txtStaffTA.Text = dtempLoad.Rows[0]["FDA"].ToString();
                                txtVDA.Text = dtempLoad.Rows[0]["VDA"].ToString();
                                txtStaffHRA.Text = dtempLoad.Rows[0]["HRA"].ToString();

                                txtSall1.Text = dtempLoad.Rows[0]["Alllowance1amt"].ToString();
                                txtSall2.Text = dtempLoad.Rows[0]["Allowance2amt"].ToString();
                                txtSded1.Text = dtempLoad.Rows[0]["Deduction1"].ToString();
                                txtSded2.Text = dtempLoad.Rows[0]["Deduction2"].ToString();
                                txtPF.Text = dtempLoad.Rows[0]["PFS"].ToString();

                                //txtSBasic.Text = "0";
                                //txtSall1.Text = "0";
                                //txtSall2.Text = "0";
                                //txtSded1.Text = "0";
                                //txtSded2.Text = "0";
                                //txtPF.Text = "0";
                            }
                            else
                            {
                                txtSBasic.Text = dtempLoad.Rows[0]["Base"].ToString();
                                txtStaffHRA.Text = "0.00";
                                txtStaffTA.Text = "0.00";

                                txtSall1.Text = dtempLoad.Rows[0]["Alllowance1amt"].ToString();
                                txtSall2.Text = dtempLoad.Rows[0]["Allowance2amt"].ToString();
                                txtSded1.Text = dtempLoad.Rows[0]["Deduction1"].ToString();
                                txtSded2.Text = dtempLoad.Rows[0]["Deduction2"].ToString();
                                txtPF.Text = dtempLoad.Rows[0]["PFS"].ToString();
                            }
                     

                        }
                        else
                        {
                            txtbase.Text = "0";
                            txtFDA.Text = "0";
                            txthra.Text = "0";
                            txtVDA.Text = "0";
                            //txttotal.Text = "0";
                            txtUnion.Text = "0";
                            txAllowance1.Text = "0";
                            txtAllowance2.Text = "0";
                            txtDeduction1.Text = "0";
                            txtDeduction2.Text = "0";
                            txtWBasic.Text = "0";
                            txtWAll1.Text = "0";
                            txtWAll2.Text = "0";
                            txtWded1.Text = "0";
                            txtWded2.Text = "0";
                            txtSBasic.Text = "0";
                            txtSall1.Text = "0";
                            txtSall2.Text = "0";
                            txtSded1.Text = "0";
                            txtSded2.Text = "0";
                            txtOBasic.Text = "0";
                            txtOUnion.Text = "0";
                            txtOAll1.Text = "0";
                            txtOAll2.Text = "0";
                            txtOded1.Text = "0";
                            txtOded2.Text = "0";
                            txtPF.Text = "0";

                        }

                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Employee Data Not Found');", true);
                    clr();
                }
            }
        }
        catch (Exception ex)
        {
        }
    }
    protected void btnSal_Click(object sender, EventArgs e)
    {
        Response.Redirect("FrmDeductionDownload.aspx");
    }
}
