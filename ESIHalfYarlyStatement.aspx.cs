﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class ESIMonthlyStatement : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    string Mont = "";
    DateTime m;
    int d = 0;
    string Year = "";
    string SessionAdmin;
    protected void Page_Load(object sender, EventArgs e)
    {
        SessionAdmin = Session["Isadmin"].ToString();
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        string ss = Session["UserId"].ToString();
    }
    public void Month()
    {

        m = Convert.ToDateTime(txtpfdate.Text);
        d = m.Month;
        int mon = m.Month;
        int Ye = m.Year;
        Year = Convert.ToString(Ye);


        switch (d)
        {
            case 1:
                Mont = "January";
                break;

            case 2:
                Mont = "February";
                break;
            case 3:
                Mont = "March";
                break;
            case 4:
                Mont = "April";
                break;
            case 5:
                Mont = "May";
                break;
            case 6:
                Mont = "June";
                break;
            case 7:
                Mont = "July";
                break;
            case 8:
                Mont = "August";
                break;
            case 9:
                Mont = "September";
                break;
            case 10:
                Mont = "October";
                break;
            case 11:
                Mont = "November";
                break;
            case 12:
                Mont = "December";
                break;
            default:
                break;
        }
    }
    protected void btnsearch_Click(object sender, EventArgs e)
    {
        try
        {
            if (txtempno.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Your Employee Registration Number..!');", true);
            }
            else if (txtpfdate.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Please Select Date..!');", true);
            }
            else
            {
                Month();
                DataTable dt = new DataTable();
                dt = objdata.ESIMonthlyStatement(txtempno.Text, Mont, Year);
                if (dt.Rows.Count > 0)
                {
                    txtesinumber.Text = dt.Rows[0]["ESICnumber"].ToString();
                    txtamount.Text = dt.Rows[0]["ESI"].ToString();
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Your Data Not Available');", true);
                }
            }
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Server Error Contact Admin..!');", true);
        }

    }
    protected void btnsave_Click(object sender, EventArgs e)
    {
        try
        {
            bool ErrFlag = false;
            if (txtempno.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Your Employee Registration Number..!');", true);
            }
            else if (txtpfdate.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Please Select Your PF Date..!');", true);
            }
            else if (txtchallendate.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Please Select Chalan Date..!');", true);
            }
            else if (txtbankname.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Your Bank Name...!');", true);
            }
            else if (txtbranchname.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Your Branch Name...!');", true);
            }
            else if (txtbranchoff.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Your ESI Office Name...!');", true);
            }


            else
            {
                Month();
                ESIMonthlyStatementClass objESI = new ESIMonthlyStatementClass();
                objESI.EmpNo = txtempno.Text;
                objESI.ESINumber = txtesinumber.Text;
                objESI.ESIAmount = txtamount.Text;
                objESI.BankNm = txtbankname.Text;
                objESI.Branch = txtbranchname.Text;
                objESI.OfficeNm = txtbranchoff.Text;

                DateTime ESIDate;
                ESIDate = DateTime.ParseExact(txtpfdate.Text, "dd-MM-yyyy", null);
                DateTime ChallenDate;
                ChallenDate = DateTime.ParseExact(txtchallendate.Text, "dd-MM-yyyy", null);
                ErrFlag = true;
                if (ErrFlag)
                {
                    objdata.InsertESIMonthlyStatement(objESI, Mont, Year, ESIDate, ChallenDate);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Your Reocrd Registered Successfully..!');", true);
                    Clear();
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Your Correct Value Formate..!');", true);
                }
            }
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Server Error Contact Admin....!');", true);
        }
    }
    public void Clear()
    {
        txtamount.Text = "";
        txtbankname.Text = "";
        txtbranchname.Text = "";
        txtbranchoff.Text = "";
        txtchallendate.Text = "";
        txtempno.Text = "";
        txtesinumber.Text = "";
        txtpfdate.Text = "";
    }
    protected void btnclear_Click(object sender, EventArgs e)
    {
        Clear();
    }
}
