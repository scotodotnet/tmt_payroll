<%@ Page Language="C#" AutoEventWireup="true" CodeFile="OfficalProfileDetails.aspx.cs" Inherits="OfficalProfileDetails_1" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
    <head id="Head1" runat="server">
        <title>Payroll Management Systems</title>
        <link href="new_css/main.css" rel="stylesheet" type="text/css" />
        <link href="new_css/colors/dark_blue.css" rel="stylesheet" type="text/css" />
        <link href="new_css/superfish.css" rel="stylesheet" type="text/css" />

        <script src="new_js/jquery-1.4.2.min.js" type="text/javascript"></script>
        <script src="new_js/jquery.hoverintent.minified.js" type="text/javascript"></script>
        <script src="new_js/superfish.js" type="text/javascript"></script>
        <script src="new_js/jquery.tools.min.js" type="text/javascript"></script>
        <%--<script src="new_js/jquery.simplemodal.js" type="text/javascript"></script>--%>
        <script src="new_js/jquery.notifybar.js" type="text/javascript"></script>
        <script src="new_js/jquery.tipsy.js" type="text/javascript"></script>
        <script src="new_js/enhance.js" type="text/javascript"></script>
        <script src="new_js/excanvas.js" type="text/javascript"></script>
        <script src="new_js/visualize.jquery.js" type="text/javascript"></script>
        <script src="new_js/jquery.collapsible.js" type="text/javascript"></script>
        <script src="new_js/jquery.autosuggest.packed.js" type="text/javascript"></script>
        <script src="new_js/platinum-admin.js" type="text/javascript"></script>
        <script language="javascript" type="text/javascript">

            function SelectSingleRadiobutton(rbtnstatus)
             {
                var rdBtn = document.getElementById(rbtnstatus);
                var rdBtnList = document.getElementsByTagName("input");
                   for (i = 0; i < rdBtnList.length; i++) 
                   {
                        if (rdBtnList[i].type == "radio" && rdBtnList[i].id != rdBtn.id)
                        {
                        rdBtnList[i].checked = false;
                        }
                  }
            }
  </script>
  <script type ="text/jscript" >
        function GoBack() {
            window.history.forward();
        }
   </script>
        <style>
            .sf-navbar li ul 
            {
	            width: 1100px; /*IE6 soils itself without this*/
	            margin: 0px auto;
	            margin-left:-20em;	            
            }
        </style>
    </head>
    <body  onload="GoBack();">
        <form id="form1" runat="server">
            <cc1:ToolkitScriptManager runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true"
                ID="ScriptManager1" EnablePartialRendering="true">
            </cc1:ToolkitScriptManager>
            <div id="header">
                <div id="header-top">
                    <div id="logo">
	                    <h1>&nbsp;</h1>
	                    <span id="slogan">&nbsp;</span> 
	                </div>
                    <!-- end logo -->
                    <div id="login-info">
                        <img id="profile" alt="profile" src="./new_images/icons/user.png" />
                        <p id="top"><span id="name"><asp:Label ID="lblusername" runat="server"></asp:Label></span><br />
                        <span id="links">
                        <%--<a id="message-link" href="#" title="click to see the messages!">
                        <span id="message-count">1</span> new message. </a>--%></span></p>
                        <div>
                            <asp:Label ID="lblComany" runat="server" Text="" Font-Bold="true"></asp:Label>
                        </div>
	                    <div id="messages-box">
		                    <h4>Messages</h4>
	                        <h5><a id="new-message" href="#">New Message</a></h5>
	                        <hr />
	                        <ul id="messages">
		                        <li class="new"><a href="#">
		                            <img alt="user" src="./new_images/icons/sender.png" />
		                            <span class="sender">John Doe</span>
		                            <span class="description">Hi this is just a description...</span>
		                            <span class="date">20 minutes ago...</span> </a>
		                        </li>
		                        <li><a href="#">
		                            <img alt="user" src="./new_images/icons/sender.png" />
		                            <span class="sender">John Doe</span>
		                            <span class="description">Hi this is just a description...</span>
		                            <span class="date">24 minutes ago...</span> </a>
		                        </li>
		                        <li><a href="#">
		                            <img alt="user" src="./new_images/icons/sender.png" />
		                            <span class="sender">John Doe</span>
		                            <span class="description">Hi this is just a description...</span>
		                            <span class="date">50 minutes ago...</span> </a>
		                        </li>
		                        <li id="last"><a id="all-messages" href="#">See all messages!
		                            <span id="unreaded">8 unreaded.</span> </a>
		                        </li>
		                    </ul>
		                    <br class="clear" />
	                    </div><!-- end messages-box -->
	                    <a id="power" href="default.aspx" title="Logout">logout</a>
                    </div>
                    <!-- end login -->
                    <div id="nav">
	                    <ul class="sf-menu sf-navbar">
	                        <li><a id="dashboard" href="Dashboard.aspx">Dashboard</a>
	                            <ul>
		                            <li>&nbsp;
		                            </li>
	                            </ul>
	                        </li>
	                        <li class="current"><a id="Employee" href="EmployeeRegistration.aspx">Employee</a>
	                            <ul>
		                            <li><a href="EmployeeRegistration.aspx">Employee</a> </li>					                    
		                            <li class="current"><a href="OfficalProfileDetails.aspx">Profile</a></li>
		                            <li><a href="SalaryMaster.aspx">Salary Master</a></li>
		                            <%--<li><a href="LeaveAllocationEmployee.aspx">Leave Allocation</a></li>--%>
		                            <%--<li><a href="ApplyForLeave.aspx">Apply Leave</a></li>--%>						                    
		                            <li><a href="SalaryRegistration.aspx">Salary Details</a></li>
		                            <li><a href="SalaryAdvance.aspx">Advance</a></li>
		                             <li><a href="Settlement.aspx">Settlement</a></li>
		                            <li><a href="EmployeeDeactiveMode.aspx">Employee Re-Activate</a></li>
		                             				                    
	                            </ul>
	                        </li>
	                         <li><a id="Reports" href="RptDepartmentwiseEmployee.aspx">Reports</a>
	                            <ul>
		                            <li><a href="RptDepartmentwiseEmployee.aspx">Employee Details</a></li>
		                            <%--<li><a href="RptAttendanceReport.aspx">Attendance Report</a></li>--%>
		                            <%--<li><a href="RptProbationperiod.aspx">Probationary Period</a></li>
		                            <li><a href="ContractBreak.aspx">Contract Details</a></li>--%>
		                            <li><a href="RptResign.aspx">Resignation Reports</a></li>
		                            <li><a href="PaySlipGen.aspx">Payslip</a></li>
		                            <%--<li><a href="RptSalary.aspx">Salary Details</a></li>--%>
		                            <%--<li><a href="SalarySummary.aspx">Salary Summary</a></li>--%>
	                                <%--<li><a href="Incentive.aspx">Incentive Details</a></li>--%>
	                                <li><a href="RptDepartmentSalaryAbstract.aspx">Salary Abstract</a></li>
	                            </ul>
	                        </li>
	                        <li><a id="OT" href="OverTime.aspx">OT</a>
	                            <ul>
	                                <li><a href="OverTime.aspx">OT</a></li>
	                                <li><a href="RptOT.aspx">OT Report</a></li>
	                            </ul>
	                        </li>
	                        <li><a id="Master" href="MstEmployeeType.aspx">Masters</a>
	                            <ul>
		                            <li><a href="MstEmployeeType.aspx">Employee Type</a></li>
		                            <li><a href="UserRegistration.aspx">User</a></li>
		                            <li><a href="MSTPFESI.aspx">PF</a></li>
		                            <li><a href="MstBank.aspx">Bank</a></li>
		                            <li><a href="MstDepartment.aspx">Department</a></li>
		                            <li><a href="MstDAfix.aspx">DA Arrears FIX</a></li>
	                            </ul>
	                        </li>
	                        <li><a id="Bonus" href="BonusForAll.aspx">Bonus</a>
	                            <ul>
		                            <li><a href="BonusForAll.aspx">Bonus</a></li>
		                            <li><a href="StandardBonus.aspx">Group Bonus</a></li>
		                            <li><a href="RptBonus.aspx">Bonus Report</a></li>					                    
	                            </ul>
	                        </li>
	                        <li><a id="ESI_PF" href="PFForm5.aspx">ESI & PF</a>
	                            <ul>
		                            <%--<li><a href="PFForm5.aspx">PF Form 5</a></li>--%>
		                            <li><a href="PFDownload.aspx">PF Download</a></li>
		                            <li><a href="ESIDownload.aspx">ESI Download</a></li>
		                            <li><a href="PFForm3Anew.aspx">PF Form 3A</a></li>
		                            <li><a href="PFform6A.aspx">PF Form 6A</a></li>
		                            <%--<li><a href="FOrm10.aspx">Form 10</a></li>
		                            <li><a href="PFForm3Anew.aspx">PF Form 3A</a></li>
		                            <li><a href="PFform6A.aspx">PF Form 6A</a></li>
		                            <li><a href="ESIForm7.aspx">ESI Form 7</a></li>--%>
	                            </ul>
	                        </li>
	                        <li><a id="Upload" href="UploadEmployee.aspx">Upload</a>
	                            <ul>
	                                <li><a href="UploadEmployee.aspx">Employee Upload</a></li>
		                            <li><a href="AttenanceUpload.aspx">Attenance Upload</a></li>
		                            	<li><a href="UploadOT.aspx">OT Upload</a></li>
		                            	<%--<li><a href="LoadEmpmusters.aspx">Employee Master</a></li>--%>
	                            </ul>
	                        </li>
                        </ul>
                    </div>
                    <!-- end div#nav -->
                </div>
                <!-- end div#header-top -->
                <div class="breadCrumb">
                    <ul>
                        <li class="first"><a href="#">Home</a> </li>
                        <li><a href="#">Users</a> </li>
                        <li><a href="#">Roles</a> </li>
                        <li class="last">Official profile</li>
                    </ul>
                </div>
                <!-- end div#breadCrumb -->
                <%--<div id="search-box">
                    <form id="searchform" action="" method="get">
                        <fieldset class="search"><span>Search</span>
                        <input class="box" type="text" />
                        <button class="btn" title="Submit Search">Search</button></fieldset>
                    </form>
                </div>--%>
                <!-- end div#search-box -->
            </div>
            <!-- end header -->
            <div id="page-wrap">
                <div id="right-sidebar">
                    <div class="innerdiv">
	                    <h2 class="head">DownLoad Format</h2>
	                    <div class="innercontent clear">
		                    <h4>DownLoad Format</h4>
		                    <ul id="comments" class="tooltip-enabled">
		                    <asp:Button ID="btncontract" runat="server" Text="Contract Report" Width="200" 
                                    CssClass="button green" onclick="btncontract_Click" />
		                        <asp:Button ID="btnEmp" runat="server" Text="Employee Download" Width="200" 
                                    CssClass="button green" onclick="btnEmp_Click" />
		                        <asp:Button ID="btnatt" runat="server" Text="Attenance Download" Width="200" 
                                    CssClass="button green" onclick="btnatt_Click"/>
		                        <asp:Button ID="btnLeave" runat="server" Text="Leave Download" Width="200" 
                                    CssClass="button green" OnClick="btnLeave_Click" />
                                    <asp:Button ID="btnOT" runat="server" Text="OT Download" Width="200" 
                                    CssClass="button green" onclick="btnOT_Click" />
                                    <asp:Button ID="btnSal" runat="server" Text="Salary Download" Width="200" 
                                    CssClass="button green" onclick="btnSal_Click" />
		                        
		                    </ul>
		                    <%--<img src="new_images/icons/right_img.jpg" alt="" width="230"/>--%>
	                    </div>
                    </div>
                    <div class="clear"></div>                    
                </div>
	            <!-- end right-sidebar -->
	            
	            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <div id="main-content">
                        <br /><br /><br /><br /><br />  
	                        <div class="clear_body">
                                <div class="innerdiv">
                                    <div class="innercontent">
                                        <!-- tab "panes" -->				                
                                        <div>
                                            <table class="full">
                                                <thead>
                                                    <tr align="center">
                                                        <th colspan="4" style="text-align:center;"><h4>OFFICIAL PROFILE</h4></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr >
                                                        <td><asp:Label ID="lblCategory" runat="server" Text="Category" Font-Bold="true"></asp:Label></td>
                                                        <td><asp:DropDownList ID="ddlcategory" runat="server" Width="100" Height="25" 
                                                                onselectedindexchanged="ddlcategory_SelectedIndexChanged" AutoPostBack="true" ></asp:DropDownList></td>
                                                        <td><asp:Label ID="lbldepatment" runat="server" Text="Department" Font-Bold="true"></asp:Label></td>
                                                        <td><asp:DropDownList ID="ddldepartment" runat="server" Width="140" Height="25" 
                                                                onselectedindexchanged="ddldepartment_SelectedIndexChanged" AutoPostBack="true" ></asp:DropDownList>
                                                        <asp:Button ID="btnclick" runat="server" Text="Click" Width="78" Height="25" 
                                                                onclick="btnclick_Click" CssClass="button green"/>
                                                        </td>
                                                    </tr>
                                                    <asp:Panel ID="panelError" runat="server" Visible="false">
			                                        <tr align="center" >
                                                
                                                        <td colspan="4" align="center">
                                                            <asp:Label ID="lblError" runat="server" Font-Bold="true" Font-Size="16px" ForeColor="Red" ></asp:Label>
                                                        </td>
                                                    
                                                    </tr>	
                                                    </asp:Panel>
                                                    <asp:Panel ID="panelsuccess" runat="server" Visible="false" BorderColor="Black">
		                                        		    <tr style="background-color:Olive">
		                                        		        <td align="left" colspan="4">
		                                        		            <asp:Label ID="lblhd" runat="server" Text="Altius InfoSystems" ForeColor="White" Font-Bold="true" Font-Size="14px" ></asp:Label>
		                                        		        </td>
		                                        		    </tr>
		                                        		    <tr align="center">
		                                        		        <td colspan="4">
		                                        		            <asp:Label ID="lbloutput" runat="server" Font-Bold="true" Font-Size="16px"></asp:Label>
		                                        		           
		                                        		        </td>
		                                        		    </tr>
		                                        		    <tr align="center">
		                                        		        <td colspan="4">
		                                        		            <asp:Button ID="btnok" runat="server" Text="Ok" onclick="btnok_Click" Width="75" Height="28" CssClass="button green" />
		                                        		        </td>
		                                        		    </tr>
		                                        		</asp:Panel>		    
                                            
                                            <tr id="panelgrid" runat="server" visible="false">
                                                <td colspan="4"><asp:GridView ID="GvOfficialProfile" runat="server"  AutoGenerateColumns="false" onpageindexchanging="PageIndexChanging">
                                                            <Columns>
                                                                <asp:TemplateField>
                                                                <HeaderTemplate>Sl No</HeaderTemplate>
                                                                <ItemTemplate><%# Container.DataItemIndex + 1   %></ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField>
                                                                    <HeaderTemplate>Token No</HeaderTemplate>
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblToken" runat="server" Text='<%# Eval("ExisistingCode") %>'></asp:Label>       
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField>
                                                                <HeaderTemplate> Emp Number</HeaderTemplate>
                                                                <ItemTemplate>
                                                                <asp:Label ID="lblempno" runat="server" Text='<%# Eval("EmpNo") %>' ></asp:Label>
                                                                </ItemTemplate>
                                                                </asp:TemplateField>
                                                                  <asp:TemplateField>
                                                                <HeaderTemplate>Emp Name</HeaderTemplate>
                                                                <ItemTemplate>
                                                                <asp:Label ID="lblEmpName" runat="server" Text='<%# Eval("EmpName") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                </asp:TemplateField>
                                                                  <asp:TemplateField>
                                                                <HeaderTemplate>Gender</HeaderTemplate>
                                                                <ItemTemplate>
                                                                <asp:Label ID="lblGender" runat="server" Text='<%# Eval("Gender") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                </asp:TemplateField>
                                                                 
                                                                   <asp:TemplateField>
                                                                <HeaderTemplate>Department Name</HeaderTemplate>
                                                                <ItemTemplate>
                                                                <asp:Label ID="lbldepartmentnm" runat="server" Text='<%# Eval("DepartmentNm") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                </asp:TemplateField>
                                                                   <asp:TemplateField>
                                                                <HeaderTemplate>Designation</HeaderTemplate>
                                                                <ItemTemplate>
                                                                <asp:Label ID="lbldesignation" runat="server" Text='<%# Eval("Designation") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField>
                                                                <HeaderTemplate>Category</HeaderTemplate>
                                                                <ItemTemplate>
                                                                <asp:Label ID="lblstaff" runat="server" Text='<%# Eval("Staff") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField Visible="false">
                                                                <HeaderTemplate>Contract Type</HeaderTemplate>
                                                                <ItemTemplate>
                                                                <asp:Label ID="lblcontracttype" runat="server" Text='<%# Eval("LabourType") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField>
                                                                <HeaderTemplate>Select</HeaderTemplate>
                                                                <ItemTemplate>
                                                                <asp:RadioButton ID="rbtnstatus" runat="server"  OnClick="javascript:SelectSingleRadiobutton(this.id);" />
                                                                </ItemTemplate>
                                                                </asp:TemplateField>
                                                            
                                                            </Columns>
                                                        </asp:GridView></td>
                                                    </tr>
                                                    <tr id="PanelOtherUserForGrid" runat="server" visible="false">
                                                        <td colspan="4"><asp:GridView ID="GvOtherUsers" runat="server"  AutoGenerateColumns="false" AllowPaging="false" PageSize="10"   onpageindexchanging="PageIndexChanging">
                                                            <Columns>
                                                            <asp:TemplateField>
                                                            <HeaderTemplate>Sl No</HeaderTemplate>
                                                            <ItemTemplate><%# Container.DataItemIndex + 1   %></ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                    <HeaderTemplate>Token No</HeaderTemplate>
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblToken" runat="server" Text='<%# Eval("ExisistingCode") %>'></asp:Label>       
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            <asp:TemplateField>
                                                            <HeaderTemplate>Employee Number</HeaderTemplate>
                                                            <ItemTemplate>
                                                            <asp:Label ID="lblempno" runat="server" Text='<%# Eval("EmpNo") %>' ></asp:Label>
                                                            </ItemTemplate>
                                                            </asp:TemplateField>
                                                              <asp:TemplateField>
                                                            <HeaderTemplate>Emp Name</HeaderTemplate>
                                                            <ItemTemplate>
                                                            <asp:Label ID="lblEmpName" runat="server" Text='<%# Eval("EmpName") %>'></asp:Label>
                                                            </ItemTemplate>
                                                            </asp:TemplateField>
                                                              <asp:TemplateField>
                                                            <HeaderTemplate>Gender</HeaderTemplate>
                                                            <ItemTemplate>
                                                            <asp:Label ID="lblGender" runat="server" Text='<%# Eval("Gender") %>'></asp:Label>
                                                            </ItemTemplate>
                                                            </asp:TemplateField>
                                                             
                                                               <asp:TemplateField>
                                                            <HeaderTemplate>Department Name</HeaderTemplate>
                                                            <ItemTemplate>
                                                            <asp:Label ID="lbldepartmentnm" runat="server" Text='<%# Eval("DepartmentNm") %>'></asp:Label>
                                                            </ItemTemplate>
                                                            </asp:TemplateField>
                                                               <asp:TemplateField>
                                                            <HeaderTemplate>Designation</HeaderTemplate>
                                                            <ItemTemplate>
                                                            <asp:Label ID="lbldesignation" runat="server" Text='<%# Eval("Designation") %>'></asp:Label>
                                                            </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                            <HeaderTemplate>Category</HeaderTemplate>
                                                            <ItemTemplate>
                                                            <asp:Label ID="lblstaff" runat="server" Text='<%# Eval("Staff") %>'></asp:Label>
                                                            </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField Visible="false">
                                                            <HeaderTemplate>Contract Type</HeaderTemplate>
                                                            <ItemTemplate>
                                                            <asp:Label ID="lblcontracttype" runat="server" Text='<%# Eval("LabourType") %>'></asp:Label>
                                                            </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                            <HeaderTemplate>Select</HeaderTemplate>
                                                            <ItemTemplate>
                                                            <asp:RadioButton ID="rbtnstatus" runat="server"  OnClick="javascript:SelectSingleRadiobutton(this.id);" />
                                                            </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                        </Columns>
                                                        </asp:GridView></td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="4"><asp:Button ID="btnadd" runat="server" Text="Add" onclick="btnadd_Click" CssClass="button green" /></td>
                                                    </tr>
                                                </tbody>
	                                        </table>
	                                        
	                                        <asp:Panel ID="PanelOfficialProfiel" runat="server">
	                                            <table class="full">
                                                    <thead>
                                                        <tr align="center">
                                                            <%--<th colspan="4" style="text-align:center;"><h4>Official Profile</h4></th>--%>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr id="PanelAdminUser" runat="server" visible="true" >
                                                            <td><asp:Label ID="lblempno" runat="server" Text="Employee No" Font-Bold="true"  ></asp:Label></td>
                                                            <td><asp:TextBox ID="txtempno" runat="server" CssClass="text" Enabled="false"></asp:TextBox></td>
                                                            <td><asp:Label ID="lblempname" runat="server" Text="Employee Name : " Font-Bold="true" ></asp:Label></td>
                                                            <td><asp:Label ID="lblempname1" runat="server" Font-Bold="true"></asp:Label></td>
                                                            <td></td>
                                                        </tr>
                                                        <tr id="PanelOtherUser" runat="server" visible="false" >
                                                            <td><asp:Label ID="Label1" runat="server" Text="Employee No" Font-Bold="true"  ></asp:Label></td>
                                                            <td><asp:TextBox ID="txtOthEmpNo" runat="server" CssClass="text" Enabled="false"></asp:TextBox></td>
                                                            <td><asp:Label ID="Label2" runat="server" Text="Employee Name : " Font-Bold="true" ></asp:Label></td>
                                                            <td><asp:Label ID="lblOthEmpName" runat="server" Font-Bold="true"></asp:Label></td>
                                                            <td></td>
                                                        </tr>
                                                        <%--<tr>
                                                            <td colspan="5"><asp:Image ID="Image1" runat="server" ImageUrl="~/Images/citybg.gif" Width="700px" Height="5px" /></td>
                                                            </tr>--%>
                                                            <tr id="PanelLabour" runat="server" visible="false">
                                                            <td><asp:Label ID="lbllabourty" runat="server" Text="Labour Type" Font-Bold="true"></asp:Label></td>
                                                            <td><asp:TextBox ID="txtlabourtype" runat="server" Enabled="false"  ></asp:TextBox></td>
                                                            <td>
                                                            <asp:Panel  id="PanelContarctTypeLabel" runat="server" visible="false" >
                                                            <asp:Label ID="lblcontratctyp" runat="server" Text="Contract Type" Font-Bold="true"></asp:Label>
                                                            </asp:Panel>
                                                            </td>
                                                            <td colspan="2">
                                                            <asp:Panel ID="PanelContarctTypeRadio" runat="server" visible="false" >
                                                            <asp:RadioButtonList ID="rbtncontract" runat="server" RepeatColumns="2">
                                                            <asp:ListItem Text="Days" Value="1"></asp:ListItem>
                                                            <asp:ListItem Text="Month" Value="2"></asp:ListItem>
                                                            </asp:RadioButtonList>
                                                            </asp:Panel>
                                                            </td>
                                                        </tr>
                                                        <tr id="PanelOthers" runat="server" visible="false">
                                                            <td><asp:Label ID="lblothers" runat="server" Text="Wages" Font-Bold="true"></asp:Label></td>
                                                            <td colspan="4"><asp:RadioButtonList ID="rbtnotherwages" runat="server" 
                                                                    RepeatColumns="3" Font-Bold="true" Width="340px" 
                                                                    onselectedindexchanged="rbtnotherwages_SelectedIndexChanged">
                                                                    <asp:ListItem Text ="Weekly Wages" Value="1"></asp:ListItem>
                                                                    <asp:ListItem Text="Monthly Wages" Value="2" ></asp:ListItem>
                                                                    <asp:ListItem Text="Bi-Monthly Wages" Value="3"></asp:ListItem>
                                                            </asp:RadioButtonList></td>        
                                                        </tr>
                                                        <tr>
                                                            <td><asp:Label ID="lbldobjoin" runat="server" Text="Date Of Joining" Font-Bold="true" ></asp:Label></td>
                                                            <td><asp:TextBox ID="txtdobjoin" runat="server" ontextchanged="txtdobjoin_TextChanged" AutoPostBack="true" ></asp:TextBox>
                                                            <cc1:CalendarExtender ID="Caleder_txtdob" runat="server" TargetControlID="txtdobjoin"  CssClass="orange" Format ="dd-MM-yyyy"  ></cc1:CalendarExtender>
                                                             <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR11" runat="server"
                                                                                                                    FilterMode="ValidChars" FilterType="Numbers,Custom"
                                                                                                                    TargetControlID="txtdobjoin" ValidChars="0123456789/- ">
                                                                                                                </cc1:FilteredTextBoxExtender></td>                                                         
                                                            <td><asp:Label ID="lblprobperiod" runat="server" Text="Probation Period" Font-Bold="true"></asp:Label></td>
                                                            <td colspan="2"><asp:DropDownList ID="ddlprobation" runat="server" Width="150" Height="25"  ></asp:DropDownList></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="5">
                                                                <asp:CheckBox ID="chkpf" runat="server" Text="Special Salary for PF Calculation" Font-Bold="true" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td><asp:Label ID="lblprofileType" runat="server" Text="Profile Type" Font-Bold="true" ></asp:Label></td>
                                                            <td colspan="4"><asp:RadioButtonList ID="rbtnProfie" runat="server" 
                                                                    RepeatColumns="2" AutoPostBack="true" 
                                                                    onselectedindexchanged="rbtnProfie_SelectedIndexChanged" >
                                                            <asp:ListItem Text="Probationary Period" Value="1" Selected="True" ></asp:ListItem>
                                                            <asp:ListItem Text="Confirmation Period" Value="2" ></asp:ListItem>
                                                            </asp:RadioButtonList></td>
                                                        </tr>
                                                        <tr id="panelContract" runat="server" visible="false">
                                                            <td><asp:Label ID="lblContract" runat="server" Text="Contract Period" Font-Bold="true"></asp:Label></td>
                                                            <td colspan="4"><asp:DropDownList ID="ddlcontract" runat="server" AutoPostBack="true" Width="180" Height="25" Enabled="false" ></asp:DropDownList></td>
                                                            <%--<td>
                                                                <asp:Label ID="lblconDate" runat="server" Text="Contract Starting Date" Font-Bold="true" ></asp:Label>
                                                            </td>--%>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                            <asp:Label ID="lblElgiblePF" runat="server" Text="Elgible for PF" Font-Bold="true" ></asp:Label>
                                                            </td>
                                                            <td>
                                                            <asp:RadioButtonList ID="rbPF" runat="server" AutoPostBack="true" 
                                                                    RepeatColumns ="2" onselectedindexchanged="rbPF_SelectedIndexChanged" >
                                                            <asp:ListItem Text="Yes" Value="1" Selected="True" ></asp:ListItem>
                                                            <asp:ListItem Text="No" Value="2"></asp:ListItem>
                                                            </asp:RadioButtonList>
                                                            </td>
                                                            <td>
                                                                    <asp:Label ID="Label3" runat="server" Text="UAN No" Font-Bold="true" ></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtUAN" runat="server"></asp:TextBox>
                                                                    </td>
                                                        </tr>
                                                        <tr>
                                                            <td><asp:Label ID="lblconfirmationdate" runat="server" Text="Confirmation Date" Font-Bold="true"></asp:Label></td>
                                                            <td ><asp:TextBox ID="txtconfirmationdate" runat="server" ontextchanged="txtconfirmationdate_TextChanged" Enabled="false"  ></asp:TextBox>
                                                                <cc1:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtconfirmationdate" Format ="dd-MM-yyyy" CssClass="orange" Enabled="true"    ></cc1:CalendarExtender>
                                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR6" runat="server"
                                                                                                                    FilterMode="ValidChars" FilterType="Numbers,Custom"
                                                                                                                    TargetControlID="txtconfirmationdate" ValidChars="0123456789/- ">
                                                                                                                </cc1:FilteredTextBoxExtender>
                                                            </td>
                                                             <td><asp:Label ID="pfnumber" runat="server" Text="PF Number" Font-Bold="true"></asp:Label></td>
                                                            <td colspan="2"><asp:TextBox ID="txtpfnumber" runat="server" Enabled="false" ></asp:TextBox>
                                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR_Designation" runat="server" FilterMode="ValidChars" FilterType="UppercaseLetters,LowercaseLetters,Numbers,Custom"
                                                                                                                    TargetControlID="txtpfnumber" ValidChars=" /">
                                                                                                                </cc1:FilteredTextBoxExtender></td>                                             
                                                        </tr>
                                                        <tr>
                                                            <td><asp:Label ID="lblpannumber" runat="server" Text="PAN Number" Font-Bold="true"></asp:Label></td>
                                                            <td><asp:TextBox ID="txtpannumber" runat="server" ></asp:TextBox>
                                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR1" runat="server" FilterMode="ValidChars" FilterType="UppercaseLetters,LowercaseLetters,Numbers,Custom"
                                                                                                                    TargetControlID="txtpannumber" ValidChars=" ">
                                                                                                                </cc1:FilteredTextBoxExtender>
                                                            </td>
                                                            <td><asp:Label ID="lblFinance" runat="server" Text="Financial Period" Font-Bold="true" ></asp:Label></td>
                                                            <td colspan="2"><asp:DropDownList ID="ddlFinancialPeriod" runat="server" Width="180" Height="25" Enabled=false ></asp:DropDownList></td>                                                           
                                                        </tr>
                                                        <tr>
                                                            <td><asp:Label ID="lblESI" runat="server" Text="Eligible for ESI" Font-Bold="true" ></asp:Label></td>
                                                            <td><asp:RadioButtonList ID="rbtnesi" runat="server" RepeatColumns="2" Width="180" 
                                                                    AutoPostBack="true" onselectedindexchanged="rbtnesi_SelectedIndexChanged">
                                                            <asp:ListItem Text="Yes" Value="1"></asp:ListItem>
                                                            <asp:ListItem Text="No" Value="2" Selected="True"></asp:ListItem>
                                                            </asp:RadioButtonList></td>
                                                            <td><asp:Label ID="lblesic" runat="server" Text="ESI Number" Font-Bold="true"></asp:Label></td>
                                                            <td colspan="2"><asp:TextBox ID="txtesic" runat="server" Enabled="false" ></asp:TextBox>
                                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR2" runat="server" FilterMode="ValidChars" FilterType="UppercaseLetters,LowercaseLetters,Numbers,Custom"
                                                                                                                    TargetControlID="txtesic" ValidChars=" /">
                                                                                                                </cc1:FilteredTextBoxExtender></td>
                                                            
                                                        </tr>
                                                        <tr>
                                                            <td><asp:Label ID="lblinsuracmpnm" runat="server" Text="Insurance Company Name" Font-Bold="true"></asp:Label></td>
                                                            <td><asp:DropDownList ID="ddlInsurance" runat="server" Width="150" Height="25" ></asp:DropDownList></td>
                                                            <td><asp:Label ID="lblinsuranceno" runat="server" Text="Insurance Number" Font-Bold="true"></asp:Label></td>
                                                            <td colspan="2"><asp:TextBox ID="txtinsuranceno" runat="server"></asp:TextBox>
                                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR3" runat="server"
                                                                                                                    FilterMode="ValidChars" FilterType="UppercaseLetters,LowercaseLetters,Numbers,Custom"
                                                                                                                    TargetControlID="txtinsuranceno" ValidChars=" ">
                                                                                                                </cc1:FilteredTextBoxExtender></td>                                                            
                                                        </tr>
                                                         <tr>
                                                            <td><asp:Label ID="lblsalarythroug"  runat="server" Text="Salary Through" Font-Bold="true" TabIndex="4"></asp:Label></td>
                                                            <td><asp:RadioButtonList ID="rbtnsalarythroug" runat="server" RepeatColumns="2" 
                                                                    Width="180" Height="28" AutoPostBack="true" 
                                                                    onselectedindexchanged="rbtnsalarythroug_SelectedIndexChanged">
                                                            <asp:ListItem  Text="Cash" Value="1" Selected="True"></asp:ListItem>
                                                            <asp:ListItem  Text="Bank" Value="2"></asp:ListItem>
                                                            </asp:RadioButtonList></td>
                                                            <td><asp:Label ID="lblbankacno" runat="server" Text="Bank Account Number" Font-Bold="true" ></asp:Label></td>
                                                            <td colspan="2"><asp:TextBox ID="txtbankacno" runat="server" CssClass="text" TabIndex="6" Enabled="false"></asp:TextBox>
                                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender18" runat="server" TargetControlID="txtbankacno" FilterMode="ValidChars" FilterType="Numbers,Custom" ValidChars=""> </cc1:FilteredTextBoxExtender></td>
                                                        </tr>
                                                        <tr>
                                                             <td><asp:Label ID="lblbankname" runat="server" Text="Bank Name" Font-Bold="true"></asp:Label></td>
                                                            <td><asp:DropDownList ID="ddlbankname" runat="server" CssClass="text" Width="180" Height="25" Enabled="false"></asp:DropDownList></td>
                                                            <td><asp:Label ID="lblbranch" runat="server" Text="Branch Name" Font-Bold="true"></asp:Label></td>
                                                            <td colspan="2"><asp:TextBox ID="txtbranch" runat="server" CssClass="text" Enabled="false" ></asp:TextBox>
                                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender19" runat="server" TargetControlID="txtbranch" FilterMode="ValidChars" FilterType="UppercaseLetters,LowercaseLetters,Custom" ValidChars=""> </cc1:FilteredTextBoxExtender></td>                                                          
                                                        </tr>
                                                        <tr id="PanelReportAuthrity" runat="server" visible="false">
                                                            <td><asp:Label ID="lblreportingauthritynm" runat="server" Text="Reporting Authority Name" Font-Bold="true"></asp:Label></td>
                                                            <td ><asp:TextBox ID="txtauthoritynm" runat="server" ></asp:TextBox>
                                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR4" runat="server"
                                                                                                                    FilterMode="ValidChars" FilterType="UppercaseLetters,LowercaseLetters,Custom"
                                                                                                                    TargetControlID="txtauthoritynm" ValidChars=" ">
                                                                                                                </cc1:FilteredTextBoxExtender></td>
                                                            <td><asp:Label ID="lblovertime" runat="server" Text="Eligible For Over Time" Font-Bold="true"></asp:Label></td>
                                                            <td colspan="2" ><asp:RadioButtonList ID="rbtnovertime" runat="server" RepeatColumns="2">
                                                            <asp:ListItem Text="Yes" Value="1" Selected="True"></asp:ListItem>
                                                            <asp:ListItem Text="No" Value="2" Selected="False"></asp:ListItem>
                                                            </asp:RadioButtonList></td>
                                                        </tr>
                                                        <tr>
                                                           
                                                          <td>
                                                              <asp:Label ID="lblMess" runat="server" Text="Eligible Mess" Font-Bold="true" Visible="false"></asp:Label>
                                                          </td>
                                                          <td>
                                                           <asp:RadioButtonList ID="rbtnMess" runat="server" RepeatDirection="Horizontal" Visible="false">
                                                            <asp:ListItem Value="1" Text="Yes"></asp:ListItem>
                                                            <asp:ListItem Value="2" Text="No"></asp:ListItem>
                                                            </asp:RadioButtonList>
                                                          </td>
                                                          <td>
                                                              <asp:Label ID="lblIncentive" runat="server" Text="Eligible Incentive" Font-Bold="true"></asp:Label>
                                                          </td>
                                                          <td>
                                                              <asp:RadioButtonList ID="rbtIncentive" runat="server" RepeatDirection="Horizontal">
                                                              <asp:ListItem Value="1" Text="Yes"></asp:ListItem>
                                                              <asp:ListItem Value="2" Text="No"></asp:ListItem>
                                                              </asp:RadioButtonList>
                                                          </td>
                                                        </tr>
                                                         <tr>
                                                            <td><asp:Label ID="lblBasicSalary" runat="server" Text="Basic Salary" Font-Bold="true" Visible="false" ></asp:Label></td>
                                                            <td colspan="4"><asp:TextBox ID="txtbasicSalary" runat="server" Visible="false" Text="0" ></asp:TextBox>
                                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR5" runat="server"
                                                            FilterMode="ValidChars" FilterType="Numbers,Custom"
                                                            TargetControlID="txtbasicSalary" ValidChars=" ">
                                                            </cc1:FilteredTextBoxExtender></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="5" align="center" >
                                                            <asp:Button ID="btnsave" runat ="server" Text="Save" onclick="btnsave_Click" Font-Bold="true" Width="75" Height="28" CssClass="button green" />
                                                            <asp:Button ID="btnEdit" runat="server" Text="Edit" onclick="btnEdit_Click" Font-Bold="true" Width="75" Height="28" CssClass="button green" />
                                                            <asp:Button ID="btnreset" runat="server" Text="Reset"  Font-Bold="true" Width="75" 
                                                                    Height="28" onclick="btnreset_Click" CssClass="button green" />
                                                            <asp:Button ID="btncancel" runat="server" Text="Cancel" OnClick="btncancel_Click" Font-Bold="true" Width="75" Height="28" CssClass="button green" />
                                                            </td>
                                                        </tr>
                                                    </tbody>
	                                            </table>
	                                        </asp:Panel>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>  
                    </ContentTemplate>
                </asp:UpdatePanel>          
            </div>
            <!-- end page-wrap -->
            <div id="footer">
                <ul>
                    <li>Copyright &copy; 2012. All rights reserved.</li>
                    <li>Powered by <a href="http://www.altius.co.in" target="_blank">Altius Infosystems</a></li>
                </ul>
            </div>
            <!-- end footer -->
        </form>
    </body>
</html>
