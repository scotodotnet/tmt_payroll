﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;
using Payroll;
using System.Drawing;
using iTextSharp.text.pdf;
using iTextSharp.text.html;
using iTextSharp.text.html.simpleparser;
using Altius.BusinessAccessLayer.BALDataAccess;
using Excel = Microsoft.Office.Interop.Excel;
using System.Reflection;
using System.Runtime.InteropServices;

public partial class PFform6A_1 : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    string SessionAdmin;
    string Stafflabour;
    string SessionCcode;
    string SessionLcode;
    string CBNO = "";
    string sum_pfsal;
    string sum_pf1;
    string sum_epf;
    string sum_eps;
    string Establishment;
    string sum_pf2;
    protected void Page_Load(object sender, EventArgs e)
    {
        
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        string ss = Session["UserId"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        //SessionAdmin = Session["Isadmin"].ToString();
        if (!IsPostBack)
        {
            DropDwonCategory();
            int currentYear = Utility.GetFinancialYear;
            for (int i = 0; i < 10; i++)
            {
                ddlfinance.Items.Add(new ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                //  ddlShowYear.Items.Add(new ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                currentYear = currentYear - 1;
            }
        }
        lblusername.Text = Session["Usernmdisplay"].ToString();
        lblComany.Text = SessionCcode + " - " + SessionLcode;
    }
    public void DropDwonCategory()
    {
        //DataTable dtcate = new DataTable();
        //dtcate = objdata.DropDownCategory();
        //ddlCategory.DataSource = dtcate;
        //ddlCategory.DataTextField = "CategoryName";
        //ddlCategory.DataValueField = "CategoryCd";
        //ddlCategory.DataBind();
    }
    protected void ddlCategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        //DataTable dtempty = new DataTable();
        //ddldpt.DataSource = dtempty;
        //ddldpt.DataBind();
        //gvform6A.DataSource = dtempty;
        //gvform6A.DataBind();
        //if (ddlCategory.SelectedValue == "1")
        //{
        //    Stafflabour = "S";
        //}
        //else if (ddlCategory.SelectedValue == "2")
        //{
        //    Stafflabour = "L";
        //}
        //else
        //{
        //    Stafflabour = "0";
        //}
        //DataTable dtDip = new DataTable();
        //dtDip = objdata.EmployeeDropDown_no(ddlCategory.SelectedValue);
        //if (dtDip.Rows.Count > 1)
        //{
        //    ddldpt.DataSource = dtDip;
        //    ddldpt.DataTextField = "EmpType";
        //    ddldpt.DataValueField = "EmpTypeCd";
        //    ddldpt.DataBind();
        //}
        //else
        //{
        //    DataTable dt = new DataTable();
        //    ddldpt.DataSource = dtempty;
        //    ddldpt.DataBind();
        //}
    }
    protected void ddldpt_SelectedIndexChanged(object sender, EventArgs e)
    {
        //DataTable dtempty = new DataTable();
        //gvform6A.DataSource = dtempty;
        //gvform6A.DataBind();
    }
    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }
    protected void btnEmp_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptEmpDownload.aspx");
    }
    protected void btnatt_Click(object sender, EventArgs e)
    {
        Response.Redirect("AttenanceDownload.aspx");
    }
    protected void btnLeave_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptLeaveSample.aspx");
    }
    protected void btnOT_Click(object sender, EventArgs e)
    {
        Response.Redirect("OTDownload.aspx");
    }
    public void ReleaseComObject(object reference)
    {
        try
        {
            while (System.Runtime.InteropServices.Marshal.ReleaseComObject(reference) <= 0)
            {
            }
        }
        catch
        {
        }
    }

    protected void btnExport_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        bool download = false;
        CBNO = objdata.Establishment(SessionCcode, SessionLcode);



        //if (ddlCategory.SelectedValue == "0")
        //{
        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Category.');", true);

        //    //System.Windows.Forms.MessageBox.Show("Select the Category", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
        //    ErrFlag = true;
        //}
        //else if (ddlfinance.SelectedValue == "")
        //{
        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Department.');", true);

        //    //System.Windows.Forms.MessageBox.Show("Select the department", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
        //    ErrFlag = true;
        //}
        //else if (ddlfinance.SelectedValue == "0")
        //{
        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Department.');", true);

        //    //System.Windows.Forms.MessageBox.Show("Select the department", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
        //    ErrFlag = true;
        //}
        if (!ErrFlag)
        {
            //DataTable dt_emp = new DataTable();
            //dt_emp = objdata.Form6A_load(SessionCcode, SessionLcode, ddlfinance.SelectedValue);
            //if (dt_emp.Rows.Count > 0)
            //{
            //    gvform6A.DataSource = dt_emp;
            //    gvform6A.DataBind();
            //    for (int i = 0; i < dt_emp.Rows.Count; i++)
            //    {
            //        sum_pfsal = (Convert.ToDecimal(sum_pfsal) + Convert.ToDecimal(dt_emp.Rows[i]["pfsalary"].ToString())).ToString();
            //        sum_pf1 = (Convert.ToDecimal(sum_pf1) + Convert.ToDecimal(dt_emp.Rows[i]["ProvidentFund"].ToString())).ToString();
            //        sum_epf = (Convert.ToDecimal(sum_epf) + Convert.ToDecimal(dt_emp.Rows[i]["amt1"].ToString())).ToString();
            //        sum_eps = (Convert.ToDecimal(sum_eps) + Convert.ToDecimal(dt_emp.Rows[i]["amt2"].ToString())).ToString();

            //    }
            //    string attachment = "attachment;filename=Form6A" + ddlfinance.SelectedValue + ".xls";
            //    Response.ClearContent();
            //    Response.AddHeader("content-disposition", attachment);
            //    Response.ContentType = "application/ms-excel";

            //    StringWriter stw = new StringWriter();
            //    HtmlTextWriter htextw = new HtmlTextWriter(stw);
            //    gvform6A.RenderControl(htextw);
            //    Response.Write("<table>");
            //    //Line 1
            //    Response.Write("<tr align='center'>");
            //    Response.Write("<td colspan='14'>");
            //    Response.Write("(For Unexempted Estalishment Only)");
            //    Response.Write("</td>");
            //    Response.Write("</tr>");

            //    //Line 2
            //    Response.Write("<tr align='center' style='font-weight: bold'>");
            //    Response.Write("<td colspan='14'>");
            //    Response.Write("FORM 6-A, (revised)");
            //    Response.Write("</td>");
            //    Response.Write("</tr>");

            //    //Line 3
            //    Response.Write("<tr align='center'>");
            //    Response.Write("<td colspan='14'>");
            //    Response.Write("(Paragraph 43) and");
            //    Response.Write("</td>");
            //    Response.Write("</tr>");

            //    //Line 4
            //    Response.Write("<tr align='center' style='font-weight: bold'>");
            //    Response.Write("<td colspan='14'>");
            //    Response.Write("The Employess's Pension Suheme,1995");
            //    Response.Write("</td>");
            //    Response.Write("</tr>");

            //    //Line 5
            //    Response.Write("<tr align='center'>");
            //    Response.Write("<td colspan='14'>");
            //    Response.Write("[Paragraph 19]");
            //    Response.Write("</td>");
            //    Response.Write("</tr>");

            //    //Line 6
            //    Response.Write("<tr align='center'>");
            //    Response.Write("<td colspan='14'>");
            //    Response.Write("");
            //    Response.Write("</td>");
            //    Response.Write("</tr>");

            //    //Line 7
            //    Response.Write("<tr>");
            //    Response.Write("<td colspan='8'>");
            //    Response.Write("Statement of Contribution for the Currency Period 1st April " + ddlfinance.SelectedValue + " to March " + (Convert.ToInt32(ddlfinance.SelectedValue) + 1).ToString());
            //    Response.Write("</td>");

            //    //Response.Write("<td>");
            //    //Response.Write("</td>");

            //    //Response.Write("<td>");
            //    //Response.Write("</td>");

            //    //Response.Write("<td>");
            //    //Response.Write("</td>");

            //    //Response.Write("<td>");
            //    //Response.Write("</td>");

            //    //Response.Write("<td>");
            //    //Response.Write("</td>");

            //    //Response.Write("<td>");
            //    //Response.Write("</td>");

            //    //Response.Write("<td>");
            //    //Response.Write("</td>");

            //    Response.Write("<td colspan='6'>");
            //    Response.Write("Statuary rate of Contribution :    12 %");
            //    Response.Write("</td>");
            //    Response.Write("</tr>");  

            //    //Line 8
            //    Response.Write("<tr align='center'>");
            //    Response.Write("<td>");
            //    Response.Write("");
            //    Response.Write("</td>");
            //    Response.Write("</tr>");

            //    //Line 9
            //    Response.Write("<tr>");
            //    Response.Write("<td>");
            //    Response.Write("Name & Address of the Estt");
            //    Response.Write("</td>");

            //    Response.Write("<td>");
            //    Response.Write("</td>");

            //    Response.Write("<td>");
            //    Response.Write("</td>");

            //    Response.Write("<td>");
            //    Response.Write("</td>");

            //    Response.Write("<td>");
            //    Response.Write("</td>");

            //    Response.Write("<td>");
            //    Response.Write("</td>");

            //    Response.Write("<td>");
            //    Response.Write("</td>");

            //    Response.Write("<td>");
            //    Response.Write("</td>");

            //    Response.Write("<td colspan='6'>");
            //    Response.Write("No.of Members Voluntarily Subscribing at Higher rate");
            //    Response.Write("</td>");
            //    Response.Write("</tr>");  

            //    //Line 10
            //    Response.Write("<tr align='center'>");
            //    Response.Write("<td>");
            //    Response.Write("");
            //    Response.Write("</td>");
            //    Response.Write("</tr>");

            //    //Line 11
            //    Response.Write("<tr>");
            //    Response.Write("<td colspan='14'>");
            //    Response.Write("Code No.of Establishment " + CBNO);
            //    Response.Write("</td>");
            //    Response.Write("</tr>");

            //    //Line 12
            //    Response.Write("<tr align='center'>");
            //    Response.Write("<td>");
            //    Response.Write("SI No.");
            //    Response.Write("</td>");


            //    Response.Write("<td>");
            //    Response.Write("Account No");
            //    Response.Write("</td>");

            //    Response.Write("<td>");
            //    Response.Write("Name of the Members (in Block Letters)");
            //    Response.Write("</td>");

            //    Response.Write("<td>");
            //    Response.Write("Wages retaining allowances(if Any) and D.A.including cash value of food concession paid during the currency period");
            //    Response.Write("</td>");

            //    Response.Write("<td colspan='2'>");
            //    Response.Write("Amount of Worker's contribution deducted from the wages");
            //    Response.Write("</td>");

            //    Response.Write("<td colspan='3'>");
            //    Response.Write("Employer's Contribution");
            //    Response.Write("</td>");

            //    Response.Write("<td >");
            //    Response.Write("Refund of advance");
            //    Response.Write("</td>");

            //    Response.Write("<td >");
            //    Response.Write("Rate of higher voluntary contribution (if any)");
            //    Response.Write("</td>");

            //    Response.Write("<td >");
            //    Response.Write("EDLI Contribution ");
            //    Response.Write("</td>");

            //    Response.Write("<td >");
            //    Response.Write("Remarks");
            //    Response.Write("</td>");

            //    Response.Write("</tr>");


            //    Response.Write("</table>");
            //    Response.Write(stw.ToString());
            //    Response.Write("<table>");
            //    Response.Write("<tr align='center' style='font-weight: bold'>");
            //    Response.Write("<td>");
            //    Response.Write("</td>");

            //    Response.Write("<td>");
            //    Response.Write("</td>");

            //    Response.Write("<td>");
            //    Response.Write("Total");
            //    Response.Write("</td>");

            //    Response.Write("<td>");
            //    Response.Write(sum_pfsal);
            //    Response.Write("</td>");

            //    Response.Write("<td>");
            //    Response.Write(sum_pf1);
            //    Response.Write("</td>");

            //    Response.Write("<td>");
            //    Response.Write(sum_pf1);
            //    Response.Write("</td>");

            //    Response.Write("<td>");
            //    Response.Write(sum_epf);
            //    Response.Write("</td>");

            //    Response.Write("<td>");
            //    Response.Write(sum_eps);
            //    Response.Write("</td>");

            //    Response.Write("<td>");
            //    Response.Write(sum_pf1);
            //    Response.Write("</td>");

            //    Response.Write("<td>");                
            //    Response.Write("</td>");

            //    Response.Write("<td>");
            //    Response.Write("</td>");

            //    Response.Write("<td>");
            //    Response.Write("</td>");

            //    Response.Write("<td>");
            //    Response.Write("</td>");

            //    Response.Write("</tr>");
            //    Response.Write("</table>");
            //    //gvDownload.RenderControl(htextw);
            //    //Response.Write("Contract Details");

            //    Response.End();
            //    Response.Clear();
            //}
            //if (ddlCategory.SelectedValue == "1")
            //{
            //    Stafflabour = "S";
            //}
            //else if (ddlCategory.SelectedValue == "2")
            //{
            //    Stafflabour = "L";
            //}
            //DataTable dt = new DataTable();
            //if (SessionAdmin == "1")
            //{
            //    dt = objdata.Form6A_New_SP(Stafflabour, ddldpt.SelectedValue, ddlfinance.SelectedValue);
            //}
            //else
            //{
            //    dt = objdata.Form6A_New_SP_user(Stafflabour, ddldpt.SelectedValue, ddlfinance.SelectedValue);
            //}
            Excel.Application xlapp;
            Excel.Workbook xlWorkBook;
            Excel.Worksheet xlWorkSheet;
            object misValue = System.Reflection.Missing.Value;
            Excel.Range chartrange;

            //gvform6A.DataSource = dt;
            //gvform6A.DataBind();
            //if (dt.Rows.Count > 0)
            {
                //for (int i = 0; i < dt.Rows.Count; i++)
                //{

                xlapp = new Excel.ApplicationClass();
                xlWorkBook = xlapp.Workbooks.Add(misValue);
                xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
                chartrange = xlWorkSheet.get_Range("A1", "M1");
                xlWorkSheet.Cells[1, 1] = "(For Unexempted Estalishment Only)";
                chartrange.Merge(true);
                chartrange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;

                chartrange = xlWorkSheet.get_Range("A2", "M2");
                xlWorkSheet.Cells[2, 1] = "FORM 6-A, (revised)";
                chartrange.Merge(true);
                chartrange.Font.Bold = true;
                chartrange.Font.Size = "14";
                chartrange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;

                chartrange = xlWorkSheet.get_Range("A3", "M3");
                xlWorkSheet.Cells[3, 1] = "The Employees' Provident Funds Scheme,1952";
                chartrange.Merge(true);
                chartrange.Font.Bold = true;
                chartrange.Font.Size = "12";
                chartrange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;

                chartrange = xlWorkSheet.get_Range("A4", "M4");
                xlWorkSheet.Cells[4, 1] = "(Paragraph 43) and";
                chartrange.Merge(true);
                //chartrange.Font.Bold = true;
                //chartrange.Font.Size = "12";
                chartrange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;

                chartrange = xlWorkSheet.get_Range("A5", "M5");
                xlWorkSheet.Cells[5, 1] = "The Employess's Pension Suheme,1995";
                chartrange.Merge(true);
                chartrange.Font.Bold = true;
                chartrange.Font.Size = "12";
                chartrange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;

                chartrange = xlWorkSheet.get_Range("A6", "M6");
                xlWorkSheet.Cells[6, 1] = "[Paragraph 19]";
                chartrange.Merge(true);
                //chartrange.Font.Bold = true;
                //chartrange.Font.Size = "12";
                chartrange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;

                chartrange = xlWorkSheet.get_Range("A7", "I7");
                xlWorkSheet.Cells[7, 1] = "Statement of Contribution for the Currency Period 1st April " + ddlfinance.SelectedValue + " to March " + (Convert.ToInt32(ddlfinance.SelectedValue) + 1).ToString();
                chartrange.Merge(true);
                //chartrange.Font.Bold = true;
                //chartrange.Font.Size = "12";
                //chartrange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;

                chartrange = xlWorkSheet.get_Range("J7", "M7");
                xlWorkSheet.Cells[7, 10] = "Statuary rate of Contribution : 12%";
                chartrange.Merge(true);
                //chartrange.Font.Bold = true;
                //chartrange.Font.Size = "12";
                //chartrange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;

                chartrange = xlWorkSheet.get_Range("A9", "C9");
                xlWorkSheet.Cells[9, 1] = "Name & Address of the Estt";
                chartrange.Merge(true);
                //chartrange.Font.Bold = true;
                //chartrange.Font.Size = "12";
                //chartrange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;

                chartrange = xlWorkSheet.get_Range("G9", "J9");
                xlWorkSheet.Cells[9, 7] = "No.of Members Voluntarily Subscribing ";
                chartrange.Merge(true);
                //chartrange.Font.Bold = true;
                //chartrange.Font.Size = "12";
                //chartrange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;

                chartrange = xlWorkSheet.get_Range("G11", "J11");
                xlWorkSheet.Cells[10, 7] = "at Higher rate";
                chartrange.Merge(true);
                //chartrange.Font.Bold = true;
                //chartrange.Font.Size = "12";
                //chartrange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;

                chartrange = xlWorkSheet.get_Range("A12", "M12");
                xlWorkSheet.Cells[11, 1] = "Code No.of Establishment " + CBNO;
                chartrange.Merge(true);
                //chartrange.Font.Bold = true;
                //chartrange.Font.Size = "12";
                //chartrange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;

                chartrange = xlWorkSheet.get_Range("A13", "A13");
                xlWorkSheet.Cells[13, 1] = "Si. No";
                chartrange.Merge(true);
                //chartrange.Font.Bold = true;
                //chartrange.Font.Size = "12";
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                chartrange = xlWorkSheet.get_Range("B13", "B13");
                xlWorkSheet.Cells[13, 2] = "Account No";
                chartrange.Merge(true);
                //chartrange.Font.Bold = true;
                //chartrange.Font.Size = "12";
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                chartrange = xlWorkSheet.get_Range("C13", "C13");
                xlWorkSheet.Cells[13, 3] = "Name of Members (in Block Letters)";
                chartrange.WrapText = true;
                //chartrange.Font.Bold = true;
                //chartrange.Font.Size = "12";
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                chartrange = xlWorkSheet.get_Range("D13", "D13");
                xlWorkSheet.Cells[13, 4] = "Wages retaining allowances(if Any) and D.A.including cash value of food concession paid during the currency period";
                chartrange.WrapText = true;
                //chartrange.Font.Bold = true;
                //chartrange.Font.Size = "12";
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                chartrange = xlWorkSheet.get_Range("E13", "F13");
                xlWorkSheet.Cells[13, 5] = "Amount of Worker's contribution deducted from the wages";
                chartrange.Merge(true);
                chartrange.WrapText = true;
                //chartrange.Font.Bold = true;
                //chartrange.Font.Size = "12";
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                chartrange = xlWorkSheet.get_Range("G13", "I13");
                xlWorkSheet.Cells[13, 7] = "Employer's Contribution";
                chartrange.Merge(true);
                chartrange.WrapText = true;
                //chartrange.Font.Bold = true;
                //chartrange.Font.Size = "12";
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                chartrange = xlWorkSheet.get_Range("J13", "J13");
                xlWorkSheet.Cells[13, 10] = "Refund of Advance";
                chartrange.Merge(true);
                chartrange.WrapText = true;
                //chartrange.Font.Bold = true;
                //chartrange.Font.Size = "12";
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                chartrange = xlWorkSheet.get_Range("K13", "M13");
                xlWorkSheet.Cells[13, 11] = "Rate of higher voluntary contribution (if any)";
                chartrange.Merge(true);
                chartrange.WrapText = true;
                //chartrange.Font.Bold = true;
                //chartrange.Font.Size = "12";
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                chartrange = xlWorkSheet.get_Range("L13", "L13");
                xlWorkSheet.Cells[13, 12] = "EDLI Contribution ";
                chartrange.Merge(true);
                chartrange.WrapText = true;
                //chartrange.Font.Bold = true;
                //chartrange.Font.Size = "12";
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                chartrange = xlWorkSheet.get_Range("M13", "M13");
                xlWorkSheet.Cells[13, 13] = "Remarks";
                chartrange.Merge(true);
                chartrange.WrapText = true;
                //chartrange.Font.Bold = true;
                //chartrange.Font.Size = "12";
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                chartrange = xlWorkSheet.get_Range("A14", "A14");
                xlWorkSheet.Cells[14, 1] = "1";
                chartrange.Merge(true);
                chartrange.WrapText = true;
                //chartrange.Font.Bold = true;
                //chartrange.Font.Size = "12";
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                chartrange = xlWorkSheet.get_Range("B14", "B14");
                xlWorkSheet.Cells[14, 2] = "2";
                chartrange.Merge(true);
                chartrange.WrapText = true;
                //chartrange.Font.Bold = true;
                //chartrange.Font.Size = "12";
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                chartrange = xlWorkSheet.get_Range("C14", "C14");
                xlWorkSheet.Cells[14, 3] = "3";
                chartrange.Merge(true);
                chartrange.WrapText = true;
                //chartrange.Font.Bold = true;
                //chartrange.Font.Size = "12";
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                chartrange = xlWorkSheet.get_Range("D14", "D14");
                xlWorkSheet.Cells[14, 4] = "4";
                chartrange.Merge(true);
                chartrange.WrapText = true;
                //chartrange.Font.Bold = true;
                //chartrange.Font.Size = "12";
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                chartrange = xlWorkSheet.get_Range("E14", "F14");
                xlWorkSheet.Cells[14, 5] = "5";
                chartrange.Merge(true);
                chartrange.WrapText = true;
                //chartrange.Font.Bold = true;
                //chartrange.Font.Size = "12";
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                chartrange = xlWorkSheet.get_Range("G14", "I14");
                xlWorkSheet.Cells[14, 7] = "6";
                chartrange.Merge(true);
                chartrange.WrapText = true;
                //chartrange.Font.Bold = true;
                //chartrange.Font.Size = "12";
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                chartrange = xlWorkSheet.get_Range("J14", "J14");
                xlWorkSheet.Cells[14, 10] = "7";
                chartrange.Merge(true);
                chartrange.WrapText = true;
                //chartrange.Font.Bold = true;
                //chartrange.Font.Size = "12";
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                chartrange = xlWorkSheet.get_Range("K14", "K14");
                xlWorkSheet.Cells[14, 11] = "8";
                chartrange.Merge(true);
                chartrange.WrapText = true;
                //chartrange.Font.Bold = true;
                //chartrange.Font.Size = "12";
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                chartrange = xlWorkSheet.get_Range("L14", "L14");
                xlWorkSheet.Cells[14, 12] = "9";
                chartrange.Merge(true);
                chartrange.WrapText = true;
                //chartrange.Font.Bold = true;
                //chartrange.Font.Size = "12";
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                chartrange = xlWorkSheet.get_Range("M14", "M14");
                xlWorkSheet.Cells[14, 13] = "10";
                chartrange.Merge(true);
                chartrange.WrapText = true;
                //chartrange.Font.Bold = true;
                //chartrange.Font.Size = "12";
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                chartrange = xlWorkSheet.get_Range("E15", "E15");
                xlWorkSheet.Cells[15, 5] = "EPF";
                chartrange.Merge(true);
                chartrange.WrapText = true;
                //chartrange.Font.Bold = true;
                //chartrange.Font.Size = "12";
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                chartrange = xlWorkSheet.get_Range("F15", "F15");
                xlWorkSheet.Cells[15, 6] = "Total";
                chartrange.Merge(true);
                chartrange.WrapText = true;
                //chartrange.Font.Bold = true;
                //chartrange.Font.Size = "12";
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                chartrange = xlWorkSheet.get_Range("G15", "G15");
                xlWorkSheet.Cells[15, 7] = "EPF 3.67%";
                chartrange.Merge(true);
                chartrange.WrapText = true;
                //chartrange.Font.Bold = true;
                //chartrange.Font.Size = "12";
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                chartrange = xlWorkSheet.get_Range("H15", "H15");
                xlWorkSheet.Cells[15, 8] = "PC 8.33";
                chartrange.Merge(true);
                chartrange.WrapText = true;
                //chartrange.Font.Bold = true;
                //chartrange.Font.Size = "12";
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                chartrange = xlWorkSheet.get_Range("I15", "I15");
                xlWorkSheet.Cells[15, 9] = "Total";
                chartrange.Merge(true);
                chartrange.WrapText = true;
                //chartrange.Font.Bold = true;
                //chartrange.Font.Size = "12";
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                chartrange = xlWorkSheet.get_Range("J15", "J15");
                xlWorkSheet.Cells[15, 10] = "";
                chartrange.Merge(true);
                chartrange.WrapText = true;
                //chartrange.Font.Bold = true;
                //chartrange.Font.Size = "12";
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                chartrange = xlWorkSheet.get_Range("K15", "L15");
                xlWorkSheet.Cells[15, 11] = "";
                chartrange.Merge(true);
                chartrange.WrapText = true;
                //chartrange.Font.Bold = true;
                //chartrange.Font.Size = "12";
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                chartrange = xlWorkSheet.get_Range("L15", "L15");
                xlWorkSheet.Cells[15, 12] = "";
                chartrange.Merge(true);
                chartrange.WrapText = true;
                //chartrange.Font.Bold = true;
                //chartrange.Font.Size = "12";
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                chartrange = xlWorkSheet.get_Range("M15", "M15");
                xlWorkSheet.Cells[15, 13] = "";
                chartrange.Merge(true);
                chartrange.WrapText = true;
                //chartrange.Font.Bold = true;
                //chartrange.Font.Size = "12";
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                int val1 = 16;
                int val2 = 1;
                sum_pfsal = "0";
                sum_pf1 = "0";
                sum_epf = "0";
                sum_eps = "0";
                sum_pf2 = "0";
                DataTable dt_emp = new DataTable();
                //if (ddlCategory.SelectedValue == "1")
                //{
                //    Stafflabour = "S";
                //}
                //else
                //{
                //    Stafflabour = "L";
                //}
                string Finance = ("01-03-" + (ddlfinance.SelectedValue)).ToString();
                string Finance1 = ("28-02-" + (Convert.ToInt32(ddlfinance.SelectedValue) + 1).ToString()).ToString();
                dt_emp = objdata.Form6A_load(SessionCcode, SessionLcode, Finance, Finance1);
                for (int i = 0; i < dt_emp.Rows.Count; i++)
                {
                    chartrange = xlWorkSheet.get_Range("A" + val1.ToString(), "A" + val1.ToString());
                    xlWorkSheet.Cells[val1, 1] = val2;
                    chartrange.Merge(true);
                    chartrange.WrapText = true;
                    //chartrange.Font.Bold = true;
                    //chartrange.Font.Size = "12";
                    chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                    chartrange = xlWorkSheet.get_Range("B" + val1.ToString(), "B" + val1.ToString());
                    xlWorkSheet.Cells[val1, 2] = dt_emp.Rows[i]["PFnumber"].ToString();
                    chartrange.Merge(true);
                    chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                    chartrange = xlWorkSheet.get_Range("C" + val1.ToString(), "C" + val1.ToString());
                    xlWorkSheet.Cells[val1, 3] = dt_emp.Rows[i]["Empname"].ToString();
                    chartrange.Merge(true);
                    chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                    chartrange = xlWorkSheet.get_Range("D" + val1.ToString(), "D" + val1.ToString());
                    xlWorkSheet.Cells[val1, 4] = dt_emp.Rows[i]["pfsalary"].ToString();
                    chartrange.Merge(true);
                    chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                    chartrange = xlWorkSheet.get_Range("E" + val1.ToString(), "E" + val1.ToString());
                    xlWorkSheet.Cells[val1, 5] = dt_emp.Rows[i]["ProvidentFund"].ToString();
                    chartrange.Merge(true);
                    chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                    chartrange = xlWorkSheet.get_Range("F" + val1.ToString(), "F" + val1.ToString());
                    xlWorkSheet.Cells[val1, 6] = dt_emp.Rows[i]["ProvidentFund"].ToString();
                    chartrange.Merge(true);
                    chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                    chartrange = xlWorkSheet.get_Range("G" + val1.ToString(), "G" + val1.ToString());
                    xlWorkSheet.Cells[val1, 7] = dt_emp.Rows[i]["amt1"].ToString();
                    chartrange.Merge(true);
                    chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                    chartrange = xlWorkSheet.get_Range("H" + val1.ToString(), "H" + val1.ToString());
                    xlWorkSheet.Cells[val1, 8] = dt_emp.Rows[i]["amt2"].ToString();
                    chartrange.Merge(true);
                    chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                    chartrange = xlWorkSheet.get_Range("I" + val1.ToString(), "I" + val1.ToString());
                    xlWorkSheet.Cells[val1, 9] = dt_emp.Rows[i]["EMp_PF"].ToString();
                    chartrange.Merge(true);
                    chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                    chartrange = xlWorkSheet.get_Range("J" + val1.ToString(), "J" + val1.ToString());
                    xlWorkSheet.Cells[val1, 10] = "";
                    chartrange.Merge(true);
                    chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                    chartrange = xlWorkSheet.get_Range("K" + val1.ToString(), "K" + val1.ToString());
                    xlWorkSheet.Cells[val1, 11] = "";
                    chartrange.Merge(true);
                    chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                    chartrange = xlWorkSheet.get_Range("L" + val1.ToString(), "L" + val1.ToString());
                    xlWorkSheet.Cells[val1, 12] = "";
                    chartrange.Merge(true);
                    chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                    chartrange = xlWorkSheet.get_Range("M" + val1.ToString(), "M" + val1.ToString());
                    xlWorkSheet.Cells[val1, 13] = "";
                    chartrange.Merge(true);
                    chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                    sum_pfsal = (Convert.ToDecimal(sum_pfsal) + Convert.ToDecimal(dt_emp.Rows[i]["pfsalary"].ToString())).ToString();
                    sum_pf1 = (Convert.ToDecimal(sum_pf1) + Convert.ToDecimal(dt_emp.Rows[i]["ProvidentFund"].ToString())).ToString();
                    sum_epf = (Convert.ToDecimal(sum_epf) + Convert.ToDecimal(dt_emp.Rows[i]["amt1"].ToString())).ToString();
                    sum_eps = (Convert.ToDecimal(sum_eps) + Convert.ToDecimal(dt_emp.Rows[i]["amt2"].ToString())).ToString();
                    sum_pf2 = (Convert.ToDecimal(sum_pf2) + Convert.ToDecimal(dt_emp.Rows[i]["EMp_PF"].ToString())).ToString();
                    val1 = val1 + 1;
                    val2 = val2 + 1;

                }
                chartrange = xlWorkSheet.get_Range("A" + val1.ToString(), "A" + val1.ToString());
                xlWorkSheet.Cells[val1, 1] = "";
                chartrange.Merge(true);
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                chartrange = xlWorkSheet.get_Range("B" + val1.ToString(), "B" + val1.ToString());
                xlWorkSheet.Cells[val1, 2] = "";
                chartrange.Merge(true);
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                chartrange = xlWorkSheet.get_Range("C" + val1.ToString(), "C" + val1.ToString());
                xlWorkSheet.Cells[val1, 3] = "Total";
                chartrange.Merge(true);
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                chartrange = xlWorkSheet.get_Range("D" + val1.ToString(), "D" + val1.ToString());
                xlWorkSheet.Cells[val1, 4] = sum_pfsal;
                chartrange.Merge(true);
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                chartrange = xlWorkSheet.get_Range("E" + val1.ToString(), "E" + val1.ToString());
                xlWorkSheet.Cells[val1, 5] = sum_pf1;
                chartrange.Merge(true);
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                chartrange = xlWorkSheet.get_Range("F" + val1.ToString(), "F" + val1.ToString());
                xlWorkSheet.Cells[val1, 6] = sum_pf1;
                chartrange.Merge(true);
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                chartrange = xlWorkSheet.get_Range("G" + val1.ToString(), "G" + val1.ToString());
                xlWorkSheet.Cells[val1, 7] = sum_epf;
                chartrange.Merge(true);
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                chartrange = xlWorkSheet.get_Range("H" + val1.ToString(), "H" + val1.ToString());
                xlWorkSheet.Cells[val1, 8] = sum_eps;
                chartrange.Merge(true);
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                chartrange = xlWorkSheet.get_Range("I" + val1.ToString(), "I" + val1.ToString());
                xlWorkSheet.Cells[val1, 9] = sum_pf2;
                chartrange.Merge(true);
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                chartrange = xlWorkSheet.get_Range("J" + val1.ToString(), "J" + val1.ToString());
                xlWorkSheet.Cells[val1, 10] = "";
                chartrange.Merge(true);
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                chartrange = xlWorkSheet.get_Range("K" + val1.ToString(), "K" + val1.ToString());
                xlWorkSheet.Cells[val1, 11] = "";
                chartrange.Merge(true);
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                chartrange = xlWorkSheet.get_Range("L" + val1.ToString(), "L" + val1.ToString());
                xlWorkSheet.Cells[val1, 12] = "";
                chartrange.Merge(true);
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                chartrange = xlWorkSheet.get_Range("M" + val1.ToString(), "M" + val1.ToString());
                xlWorkSheet.Cells[val1, 13] = "";
                chartrange.Merge(true);
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                //string newpath = Server.MapPath("C:");
                xlWorkBook.SaveAs("Form6A.xls", Excel.XlFileFormat.xlWorkbookNormal, misValue, misValue, misValue, misValue, Excel.XlSaveAsAccessMode.xlExclusive, misValue, misValue, misValue, misValue, misValue);
                xlWorkBook.Close(true, misValue, misValue);
                xlapp.Quit();
                download = true;

                Marshal.ReleaseComObject(xlapp);
                Marshal.ReleaseComObject(xlWorkBook);
                Marshal.ReleaseComObject(xlWorkSheet);
            }
            if (download == true)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Saved Successfully in Documents in the name of Form6A.xls');", true);
            }

            //}


        }
    }
    protected void btncontract_Click(object sender, EventArgs e)
    {
        Response.Redirect("ContractRPT.aspx");
    }
    protected void btnSal_Click(object sender, EventArgs e)
    {
        Response.Redirect("FrmDeductionDownload.aspx");
    }

}
