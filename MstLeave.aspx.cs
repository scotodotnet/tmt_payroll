﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Configuration;
using Altius.BusinessAccessLayer.BALDataAccess;
public partial class MstLeave_1 : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    MastersClass objMas = new MastersClass();
    string SessionAdmin;
    static string lblprobation_Common;
    string SessionCcode;
    string SessionLcode;
    protected void Page_Load(object sender, EventArgs e)
    {
        
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        string ss = Session["UserId"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();
        if (!IsPostBack)
        {
            MstLeaveDisplay();
        }
        lblComany.Text = SessionCcode + " - " + SessionLcode;
        lblusername.Text = Session["Usernmdisplay"].ToString();
    }
    protected void btnsave_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        if (txtleavecd.Text == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Leave Type Code');", true);
            //System.Windows.Forms.MessageBox.Show("Enter the leave type Code", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            ErrFlag = true;
        }
        else if (txtleavetype.Text == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Type of Leave');", true);
            //System.Windows.Forms.MessageBox.Show("Enter the Leave type", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            ErrFlag = true;
        }
        string constr = ConfigurationManager.AppSettings["ConnectionString"];
        SqlConnection con = new SqlConnection(constr);
        SqlCommand cmd = new SqlCommand("Select LeaveType from MstLeave Where LeaveType='" + txtleavetype.Text + "'", con);
        con.Open();
        SqlDataReader sdr = cmd.ExecuteReader();
        if (sdr.HasRows)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('The Leave Type All Ready Registered');", true);
            //System.Windows.Forms.MessageBox.Show("Leave type Already Registered", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            ErrFlag = true;
        }
        else
        {

        }
        con.Close();
        if (!ErrFlag)
        {
            string leaveid = objdata.Leaveid_check(txtleavecd.Text);
            string leavename = objdata.Leavename_check(txtleavetype.Text);
            if (leaveid == txtleavecd.Text)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('The Leave Type Id AlReady Registered');", true);
                //System.Windows.Forms.MessageBox.Show("Leave type ID Already registered", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            if (leavename.ToLower() == txtleavetype.Text.ToLower())
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('The Leave Type All Ready Registered');", true);
                //System.Windows.Forms.MessageBox.Show("Leave type Already registered", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            if (!ErrFlag)
            {
                objMas.LeaveCd = txtleavecd.Text;
                objMas.LeaveType = txtleavetype.Text;

                objdata.MstLeave(objMas);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Your Leave Type Save Successfully...');", true);
                //System.Windows.Forms.MessageBox.Show("Your Leave Type Saved Successfully", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                txtleavecd.Text = "";
                txtleavetype.Text = "";
                MstLeaveDisplay();
            }
        }
 
    }
    public void MstLeaveDisplay()
    {
        DataTable dt = new DataTable();
        dt = objdata.MstLeaveDisplay();
        gvQualification.DataSource = dt;
        gvQualification.DataBind();
    }
    protected void GvLeave_Editing(object sender, GridViewEditEventArgs e)
    {
        gvQualification.EditIndex = e.NewEditIndex;
        MstLeaveDisplay();
    }
    protected void GvLeave_Cancel(object sender, GridViewCancelEditEventArgs e)
    {
        gvQualification.EditIndex = -1;
        MstLeaveDisplay();
    }
    protected void GvLeave_Updating(object sender, GridViewUpdateEventArgs e)
    {
        try
        {
            bool ErrFlag = false;
            TextBox txtleavety1 = (TextBox)gvQualification.Rows[e.RowIndex].FindControl("txtleavetype");
            if (txtleavety1.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Type of Leave');", true);
                //System.Windows.Forms.MessageBox.Show("Enter the type of leave", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            if (!ErrFlag)
            {
                Label lblleavecd1 = (Label)gvQualification.Rows[e.RowIndex].FindControl("lbllaevecd");
                string leavename = objdata.Leavename_check(txtleavety1.Text);
                if (leavename.ToLower() == txtleavety1.Text.ToLower())
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('The Leave Type All Ready Registered');", true);
                    //System.Windows.Forms.MessageBox.Show("leave Type Already Registered", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    ErrFlag = true;
                }
                if (!ErrFlag)
                {
                    objMas.LeaveCd = lblleavecd1.Text;
                    objMas.LeaveType = txtleavety1.Text;
                    objdata.UpdateLeave(objMas);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Leave Type Update Successfully...');", true);
                    //System.Windows.Forms.MessageBox.Show("Leave type Updated Successfully", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    gvQualification.EditIndex = -1;
                    MstLeaveDisplay();
                }
            }
        }
        catch (Exception ex)
        {

        }
    }
    protected void gvQualification_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        bool ErrFlag = false;
        string constr = ConfigurationManager.AppSettings["ConnectionString"];
        Label lblid = (Label)gvQualification.Rows[e.RowIndex].FindControl("lbllaevecd");
        string leaveid = objdata.Leave_delete(lblid.Text);
        lblprobation_Common = lblid.Text;
        if (leaveid == lblid.Text)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Id Cannot be Deleted...');", true);
            //System.Windows.Forms.MessageBox.Show("it Cannot be Deleted", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            ErrFlag = true;
        }
        if (!ErrFlag)
        {
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "script", "showConfirm();", true);
            //SqlConnection con = new SqlConnection(constr);
            //System.Windows.Forms.DialogResult dres = System.Windows.Forms.MessageBox.Show("Are you Sure want to Delete ?", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.YesNo, System.Windows.Forms.MessageBoxIcon.Warning);
            //if (dres.ToString() == "Yes")
            //{

            //    string qry = "Delete from MstLeave where LeaveCd = '" + lblid.Text + "'";
            //    SqlCommand cmd = new SqlCommand(qry, con);
            //    con.Open();
            //    cmd.ExecuteNonQuery();
            //    con.Close();
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Deleted Successfully...');", true);
            //    System.Windows.Forms.MessageBox.Show("Deleted Successfully", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            //    MstLeaveDisplay();
            //}
        }

    }

    protected void cmdOverriteGrade_Click(object sender, EventArgs e)
    {
        try
        {
            string constr = ConfigurationManager.AppSettings["ConnectionString"];
            SqlConnection con = new SqlConnection(constr);
            //Label lblbank = (Label)gvBank.Rows[e.RowIndex].FindControl("lblBankcd");
            string qry = "Delete from MstLeave where LeaveCd = '" + lblprobation_Common + "'";
            SqlCommand cmd = new SqlCommand(qry, con);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Deleted Successfully...');", true);
            //System.Windows.Forms.MessageBox.Show("Deleted Successfully", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            MstLeaveDisplay();

        }
        catch (Exception ex)
        {
        }
    }

    protected void btncancel_Click(object sender, EventArgs e)
    {
        txtleavecd.Text = "";
        txtleavetype.Text = "";
        MstLeaveDisplay();
    }
}
