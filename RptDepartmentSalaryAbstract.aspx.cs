﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using Payroll;
using Payroll.Data;
using Payroll.Configuration;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html;
using iTextSharp.text.html.simpleparser;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;

public partial class RptDepartmentSalaryAbstract : System.Web.UI.Page
{
    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    SqlConnection con;
    BALDataAccess objdata = new BALDataAccess();

    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    DateTime MyDate;
    DateTime MyDate1;
    DateTime MyDate2;

    protected void Page_Load(object sender, EventArgs e)
    {
        con = new SqlConnection(constr);
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionAdmin = Session["Isadmin"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        string ss = Session["UserId"].ToString();
        if (!IsPostBack)
        {
            //DropDwonCategory();
            alldropdownadd();
        }
        lblComany.Text = SessionCcode + " - " + SessionLcode;
        lblusername.Text = Session["Usernmdisplay"].ToString();
    }

    public void alldropdownadd()
    {

        //Financial Year Add
        int currentYear = Utility.GetFinancialYear;
        txtFinancial_Year.Items.Add("----Select----");
        txtFrom_Month.Items.Add("----Select----");
        for (int i = 0; i <= 11; i++)
        {
            txtFinancial_Year.Items.Add(new System.Web.UI.WebControls.ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
            currentYear = currentYear - 1;
        }
        txtFinancial_Year.SelectedIndex = 1;
        //Month Add
        System.Globalization.DateTimeFormatInfo mfi = new
        System.Globalization.DateTimeFormatInfo();
        string strMonthName = "";
        for (int i = 3; i <= 11; i++)
        {
            //strMonthName = mfi.GetAbbreviatedMonthName(i + 1).ToString();
            strMonthName = mfi.GetMonthName(i + 1).ToString();
            txtFrom_Month.Items.Add(strMonthName);
        }
        for (int i = 0; i <= 2; i++)
        {
            strMonthName = mfi.GetMonthName(i + 1).ToString();
            txtFrom_Month.Items.Add(strMonthName);
        }
    }

    protected void txtFrom_Month_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void txtFinancial_Year_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void ddlrptReport_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void btnexport_Click(object sender, EventArgs e)
    {
        //All Header Details Get
        string CmpName = "";
        string Cmpaddress = "";
        string From_Month_DB = "";
        string Department_ID = "";
        string NetPay_Count = "";
        bool ErrFlag = false;
        string query = "";
        DataTable NetPay_DT = new DataTable();
        //if (rbsalary.SelectedValue == "2")
        //{
        //    if (txtFrom_Month.SelectedValue == "" || txtFrom_Month.SelectedValue == "----Select----")
        //    {
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Month Properly');", true);
        //        ErrFlag = true;
        //    }
        //}
        //else
        //{
        //    if (txtfrom.Text.Trim() == "")
        //    {
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Month Properly');", true);
        //        ErrFlag = true;
        //    }
        //    else if (txtTo.Text.Trim() == "")
        //    {
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Month Properly');", true);
        //        ErrFlag = true;
        //    }
        //    if (!ErrFlag)
        //    {
        //        DateTime dfrom = Convert.ToDateTime(txtfrom.Text);
        //        DateTime dtto = Convert.ToDateTime(txtTo.Text);
        //        MyDate1 = DateTime.ParseExact(txtfrom.Text, "dd-MM-yyyy", null);
        //        MyDate2 = DateTime.ParseExact(txtTo.Text, "dd-MM-yyyy", null);
        //        if (dtto < dfrom)
        //        {
        //            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the From Date...');", true);
        //            txtfrom.Text = null;
        //            txtTo.Text = null;
        //            ErrFlag = true;
        //        }
        //    }
        //}

        int YR = 0;
        if (!ErrFlag)
        {
            if (txtFrom_Month.SelectedValue == "January")
            {
                YR = Convert.ToInt32(txtFinancial_Year.SelectedValue);
                YR = YR + 1;
            }
            else if (txtFrom_Month.SelectedValue == "February")
            {
                YR = Convert.ToInt32(txtFinancial_Year.SelectedValue);
                YR = YR + 1;
            }
            else if (txtFrom_Month.SelectedValue == "March")
            {
                YR = Convert.ToInt32(txtFinancial_Year.SelectedValue);
                YR = YR + 1;
            }
            else
            {
                YR = Convert.ToInt32(txtFinancial_Year.SelectedValue);
            }

            if (txtFrom_Month.SelectedValue != "0") { From_Month_DB = txtFrom_Month.SelectedItem.Text.ToString(); }

            int Fin_Year_DB = 0;
            string[] Fin_Year_Split = txtFinancial_Year.SelectedItem.Text.Split('-');
            Fin_Year_DB = Convert.ToInt32(Fin_Year_Split[0].ToString());

            query = "Select Distinct EmpDet.Department,MstDpt.DepartmentNm from EmployeeDetails EmpDet" +
                    " inner Join MstDepartment as MstDpt on MstDpt.DepartmentCd = EmpDet.Department" +
                    " inner Join SalaryDetails SalDet on SalDet.EmpNo=EmpDet.EmpNo where EmpDet.Ccode='" + SessionCcode + "'" +
                    " and EmpDet.Lcode='" + SessionLcode + "'" +
                    " and SalDet.Month='" + From_Month_DB + "' and SalDet.FinancialYear='" + Fin_Year_DB + "'" +
                    " Order by MstDpt.DepartmentNm Asc";

            SqlCommand cmd = new SqlCommand(query, con);
            DataTable Department_Dt = new DataTable();
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            con.Open();
            sda.Fill(Department_Dt);
            con.Close();

            //Excel Write
            string attachment = "attachment; filename=SalaryAbstractDetails.xls";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/ms-excel";

            DataTable dt = new DataTable();
            dt = objdata.Company_retrive(SessionCcode, SessionLcode);
            if (dt.Rows.Count > 0)
            {
                CmpName = dt.Rows[0]["Cname"].ToString();
                Cmpaddress = (dt.Rows[0]["Address1"].ToString() + ", " + dt.Rows[0]["Address2"].ToString() + ", " + dt.Rows[0]["Location"].ToString() + "-" + dt.Rows[0]["Pincode"].ToString());
            }

            StringWriter stw = new StringWriter();
            HtmlTextWriter htextw = new HtmlTextWriter(stw);
            //griddept.RenderControl(htextw);

            Response.Write("<table>");
            Response.Write("<tr align='Center'>");
            Response.Write("<td colspan='8' style='font-size:16.0pt;font-weight:bold;text-decoration:underline;''>");
            Response.Write("SRI VENKATALAKSHMI SPINNERS PVT LTD");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("<tr align='Center'>");
            Response.Write("<td colspan='8' style='font-size:13.0pt;font-weight:bold;text-decoration:underline;''>");
            Response.Write("" + SessionLcode + "");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("<tr align='Center'>");
            Response.Write("<td colspan='8' style='font-size:12.0pt;font-weight:bold;text-decoration:underline;''>");
            Response.Write("" + Cmpaddress + "");
            Response.Write("</td>");
            Response.Write("</tr>");

            
            Response.Write("<tr align='center'>");
            Response.Write("<td colspan='8' style='font-size:11.0pt;font-weight:bold;text-decoration:underline;''>");
            Response.Write("SALARY ABSTRACT THE MONTH OF " + txtFrom_Month.SelectedValue.ToUpper().ToString() + " - " + YR);
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("<tr align='center'><td colspan='8'></td></tr>");
            
            Response.Write("</table>");

            //Report Column Heading Add
            Response.Write("<table border='1' style='font-weight:bold;'><tr align='center' style='vertical-align: middle;'>");
            Response.Write("<td rowspan='2'>S.NO</td><td rowspan='2'>DEPARTMENT</td><td colspan='1'>STAFFS</td>");

            Response.Write("<td colspan='5'>LABOUR</td><td rowspan='2'>GRAND TOTAL</td>");
            
            Response.Write("</tr><tr align='center' style='vertical-align: middle;'>");
            Response.Write("<td>GENERAL</td><td>CASUAL WORKER</td><td>PERMANENT</td><td>SCHEME</td>");
            Response.Write("<td>NON SCHEME</td><td>OT AMOUNT</td>");
            Response.Write("</tr></table>");


            Int32 Grand_Tot_Start;
            Int32 Grand_Tot_End;
            Grand_Tot_Start = 7;
            for (int i = 0; i < Department_Dt.Rows.Count; i++)
            {
                NetPay_Count = "";
                Response.Write("<table border='1'><tr>");
                Response.Write("<td>" + (Convert.ToDecimal(i) + Convert.ToDecimal(1)).ToString() + "</td>");
                Response.Write("<td align='center'>" + Department_Dt.Rows[i]["DepartmentNm"].ToString() + "</td>");
                Department_ID = Department_Dt.Rows[i]["Department"].ToString();

                //General Staff Sum Total
                query = "Select sum(SalDet.NetPay) as NetPay from SalaryDetails SalDet,EmployeeDetails EmpDet,OfficialProfile OP where EmpDet.EmpNo=SalDet.EmpNo and OP.EmpNo=SalDet.EmpNo" +
                " and SalDet.Month='" + From_Month_DB + "' and SalDet.FinancialYear='" + Fin_Year_DB + "' and EmpDet.StafforLabor='S'" +
                " and EmpDet.Ccode='" + SessionCcode + "' and EmpDet.Lcode='" + SessionLcode + "'" +
                " and EmpDet.Department='" + Department_ID + "' and EmpDet.EmployeeType='1'";
                NetPay_DT = objdata.RptEmployeeMultipleDetails(query);
                if (NetPay_DT.Rows[0][0].ToString() != "" && NetPay_DT.Rows[0][0].ToString() != "0.00") { NetPay_Count = NetPay_DT.Rows[0][0].ToString(); }
                Response.Write("<td>" + NetPay_Count + "</td>"); NetPay_Count = "";

                //Labour Casual Worker Sum Total
                query = "Select sum(SalDet.NetPay) as NetPay from SalaryDetails SalDet,EmployeeDetails EmpDet,OfficialProfile OP where EmpDet.EmpNo=SalDet.EmpNo and OP.EmpNo=SalDet.EmpNo" +
                " and SalDet.Month='" + From_Month_DB + "' and SalDet.FinancialYear='" + Fin_Year_DB + "' and EmpDet.StafforLabor='L'" +
                " and EmpDet.Ccode='" + SessionCcode + "' and EmpDet.Lcode='" + SessionLcode + "'" +
                " and EmpDet.Department='" + Department_ID + "' and EmpDet.EmployeeType='2'";
                NetPay_DT = objdata.RptEmployeeMultipleDetails(query);
                if (NetPay_DT.Rows[0][0].ToString() != "" && NetPay_DT.Rows[0][0].ToString() != "0.00") { NetPay_Count = NetPay_DT.Rows[0][0].ToString(); }
                Response.Write("<td>" + NetPay_Count + "</td>"); NetPay_Count = "";

                //Labour Permanent Sum Total
                query = "Select sum(SalDet.NetPay) as NetPay from SalaryDetails SalDet,EmployeeDetails EmpDet,OfficialProfile OP where EmpDet.EmpNo=SalDet.EmpNo and OP.EmpNo=SalDet.EmpNo" +
                " and SalDet.Month='" + From_Month_DB + "' and SalDet.FinancialYear='" + Fin_Year_DB + "' and EmpDet.StafforLabor='L'" +
                " and EmpDet.Ccode='" + SessionCcode + "' and EmpDet.Lcode='" + SessionLcode + "'" +
                " and EmpDet.Department='" + Department_ID + "' and EmpDet.EmployeeType='3'";
                NetPay_DT = objdata.RptEmployeeMultipleDetails(query);
                if (NetPay_DT.Rows[0][0].ToString() != "" && NetPay_DT.Rows[0][0].ToString() != "0.00") { NetPay_Count = NetPay_DT.Rows[0][0].ToString(); }
                Response.Write("<td>" + NetPay_Count + "</td>"); NetPay_Count = "";

                //Labour Scheme Sum Total
                query = "Select sum(SalDet.NetPay) as NetPay from SalaryDetails SalDet,EmployeeDetails EmpDet,OfficialProfile OP where EmpDet.EmpNo=SalDet.EmpNo and OP.EmpNo=SalDet.EmpNo" +
                " and SalDet.Month='" + From_Month_DB + "' and SalDet.FinancialYear='" + Fin_Year_DB + "' and EmpDet.StafforLabor='L'" +
                " and EmpDet.Ccode='" + SessionCcode + "' and EmpDet.Lcode='" + SessionLcode + "'" +
                " and EmpDet.Department='" + Department_ID + "' and EmpDet.EmployeeType='4'";
                NetPay_DT = objdata.RptEmployeeMultipleDetails(query);
                if (NetPay_DT.Rows[0][0].ToString() != "" && NetPay_DT.Rows[0][0].ToString() != "0.00") { NetPay_Count = NetPay_DT.Rows[0][0].ToString(); }
                Response.Write("<td>" + NetPay_Count + "</td>"); NetPay_Count = "";

                //Labour Non Scheme Sum Total
                query = "Select sum(SalDet.NetPay) as NetPay from SalaryDetails SalDet,EmployeeDetails EmpDet,OfficialProfile OP where EmpDet.EmpNo=SalDet.EmpNo and OP.EmpNo=SalDet.EmpNo" +
                " and SalDet.Month='" + From_Month_DB + "' and SalDet.FinancialYear='" + Fin_Year_DB + "' and EmpDet.StafforLabor='L'" +
                " and EmpDet.Ccode='" + SessionCcode + "' and EmpDet.Lcode='" + SessionLcode + "'" +
                " and EmpDet.Department='" + Department_ID + "' and EmpDet.EmployeeType='5'";
                NetPay_DT = objdata.RptEmployeeMultipleDetails(query);
                if (NetPay_DT.Rows[0][0].ToString() != "" && NetPay_DT.Rows[0][0].ToString() != "0.00") { NetPay_Count = NetPay_DT.Rows[0][0].ToString(); }
                Response.Write("<td>" + NetPay_Count + "</td>"); NetPay_Count = "";

                //OT Amount Calculate
                query = "Select SUM(OT.netAmount) AS OverTime from OverTime OT,EmployeeDetails ED where OT.EmpNo=ED.EmpNo and OT.Ccode='" + SessionCcode + "'" +
                " and OT.Lcode='" + SessionLcode + "' and OT.Month='" + From_Month_DB + "' and OT.Financialyr='" + Fin_Year_DB + "' and ED.Ccode='" + SessionCcode + "'" +
                " and ED.Lcode='" + SessionLcode + "' and ED.Department='" + Department_ID + "'";
                NetPay_DT = objdata.RptEmployeeMultipleDetails(query);
                if (NetPay_DT.Rows[0][0].ToString() != "" && NetPay_DT.Rows[0][0].ToString() != "0.00") { NetPay_Count = NetPay_DT.Rows[0][0].ToString(); }
                Response.Write("<td>" + NetPay_Count + "</td>"); NetPay_Count = "";

                //Grand Total Amount
                Grand_Tot_Start++;
                Response.Write("<td>=sum(C" + Grand_Tot_Start.ToString() + ":H" + Grand_Tot_Start.ToString() + ")</td>");
                //Response.Write("<td>=sum(C" + Grand_Tot_Start.ToString() + ":N" + Grand_Tot_Start.ToString() + ")</td>");

                Response.Write("</tr></table>");

            }
            //Final Total Amount
            if (Department_Dt.Rows.Count.ToString() != "0")
            {
                Grand_Tot_End = Grand_Tot_Start;
                Response.Write("<table border='1'><tr>");
                Response.Write("<td colspan='2' align='right'>TOTAL AMOUNT</td>");

                Response.Write("<td>=sum(C8:C" + Grand_Tot_End.ToString() + ")</td>");
                Response.Write("<td>=sum(D8:D" + Grand_Tot_End.ToString() + ")</td>");
                Response.Write("<td>=sum(E8:E" + Grand_Tot_End.ToString() + ")</td>");
                Response.Write("<td>=sum(F8:F" + Grand_Tot_End.ToString() + ")</td>");
                Response.Write("<td>=sum(G8:G" + Grand_Tot_End.ToString() + ")</td>");
                Response.Write("<td>=sum(H8:H" + Grand_Tot_End.ToString() + ")</td>");
                Response.Write("<td>=sum(I8:I" + Grand_Tot_End.ToString() + ")</td>");
                //Response.Write("<td>=sum(Q8:Q" + Grand_Tot_End.ToString() + ")</td>");

                Response.Write("</tr></table>");
            }
            Response.End();
            Response.Clear();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Downloaded Successfully');", true);
        }
    }

    protected void btnEmp_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptEmpDownload.aspx");
    }
    protected void btnatt_Click(object sender, EventArgs e)
    {
        Response.Redirect("AttenanceDownload.aspx");
    }
    protected void btnLeave_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptLeaveSample.aspx");
    }
    protected void btnOT_Click(object sender, EventArgs e)
    {
        Response.Redirect("OTDownload.aspx");
    }
    protected void btncontract_Click(object sender, EventArgs e)
    {
        Response.Redirect("ContractRPT.aspx");
    }
    protected void btnSal_Click(object sender, EventArgs e)
    {
        Response.Redirect("FrmDeductionDownload.aspx");
    }
    
    protected void txtfrom_TextChanged(object sender, EventArgs e)
    {

    }
    protected void txtTo_TextChanged(object sender, EventArgs e)
    {

    }
}
