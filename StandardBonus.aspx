<%@ Page Language="C#" AutoEventWireup="true" CodeFile="StandardBonus.aspx.cs" Inherits="GroupBonus" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
    <head id="Head1" runat="server">
        <title>Payroll Management Systems</title>
        <link href="new_css/main.css" rel="stylesheet" type="text/css" />
        <link href="new_css/colors/dark_blue.css" rel="stylesheet" type="text/css" />
        <link href="new_css/superfish.css" rel="stylesheet" type="text/css" />

        <script src="new_js/jquery-1.4.2.min.js" type="text/javascript"></script>
        <script src="new_js/jquery.hoverintent.minified.js" type="text/javascript"></script>
        <script src="new_js/superfish.js" type="text/javascript"></script>
        <script src="new_js/jquery.tools.min.js" type="text/javascript"></script>
        <%--<script src="new_js/jquery.simplemodal.js" type="text/javascript"></script>--%>
        <script src="new_js/jquery.notifybar.js" type="text/javascript"></script>
        <script src="new_js/jquery.tipsy.js" type="text/javascript"></script>
        <script src="new_js/enhance.js" type="text/javascript"></script>
        <script src="new_js/excanvas.js" type="text/javascript"></script>
        <script src="new_js/visualize.jquery.js" type="text/javascript"></script>
        <script src="new_js/jquery.collapsible.js" type="text/javascript"></script>
        <script src="new_js/jquery.autosuggest.packed.js" type="text/javascript"></script>
        <script src="new_js/platinum-admin.js" type="text/javascript"></script>
        <script src="new_js/platinum-admin.js" type="text/javascript"></script>
        <script type="text/javascript">
        function SelectheaderCheckboxes_staff(cbSelectAll) {

            var gvcheck = document.getElementById('GridviewStaff');
            var i;
            //Condition to check header checkbox selected or not if that is true checked all checkboxes
            if (cbSelectAll.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    var inputs = gvcheck.rows[i].getElementsByTagName('input');
                    inputs[0].checked = true;
                }
            }
            //if condition fails uncheck all checkboxes in gridview
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    var inputs = gvcheck.rows[i].getElementsByTagName('input');
                    inputs[0].checked = false;
                }
            }
        }
        //function to check header checkbox based on child checkboxes condition
        function Selectchildcheckboxes_staff(header) {
            var ck = header;
            var count = 0;
            var gvcheck = document.getElementById('GridviewStaff');
            var headerchk = document.getElementById(header);
            var rowcount = gvcheck.rows.length;
            //By using this for loop we will count how many checkboxes has checked
            for (i = 1; i < gvcheck.rows.length; i++) {
                var inputs = gvcheck.rows[i].getElementsByTagName('input');
                if (inputs[0].checked) {
                    count++;
                }
            }
            //Condition to check all the checkboxes selected or not
            if (count == rowcount - 1) {
                headerchk.checked = true;
            }
            else {
                headerchk.checked = false;
            }
        }
    </script>
        
        
        <style>
            
        
            .sf-navbar li ul 
            {
	            width: 1100px; /*IE6 soils itself without this*/
	            margin: 0px auto;
	            margin-left:-20em;	            
            }
        </style>
    </head>
    <body>
        <form id="form1" runat="server">
            <cc1:ToolkitScriptManager runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true"
                ID="ScriptManager1" EnablePartialRendering="true">
            </cc1:ToolkitScriptManager>
            <div id="header">
                <div id="header-top">
                    <div id="logo">
	                    <h1>&nbsp;</h1>
	                    <span id="slogan">&nbsp;</span> 
	                </div>
                    <!-- end logo -->
                    <div id="login-info">
                        <img id="profile" alt="profile" src="./new_images/icons/user.png" />
                        <p id="top"><span id="name"><asp:Label ID="lblusername" runat="server"></asp:Label></span><br />
                        <span id="links">
                        <%--<a id="message-link" href="#" title="click to see the messages!">
                        <span id="message-count">1</span> new message. </a>--%></span></p>
                        <div>
                            <asp:Label ID="lblComany" runat="server" Text="" Font-Bold="true"></asp:Label>
                        </div>
	                    <div id="messages-box">
		                    <h4>Messages</h4>
	                        <h5><a id="new-message" href="#">New Message</a></h5>
	                        <hr />
	                        <ul id="messages">
		                        <li class="new"><a href="#">
		                            <img alt="user" src="./new_images/icons/sender.png" />
		                            <span class="sender">John Doe</span>
		                            <span class="description">Hi this is just a description...</span>
		                            <span class="date">20 minutes ago...</span> </a>
		                        </li>
		                        <li><a href="#">
		                            <img alt="user" src="./new_images/icons/sender.png" />
		                            <span class="sender">John Doe</span>
		                            <span class="description">Hi this is just a description...</span>
		                            <span class="date">24 minutes ago...</span> </a>
		                        </li>
		                        <li><a href="#">
		                            <img alt="user" src="./new_images/icons/sender.png" />
		                            <span class="sender">John Doe</span>
		                            <span class="description">Hi this is just a description...</span>
		                            <span class="date">50 minutes ago...</span> </a>
		                        </li>
		                        <li id="last"><a id="all-messages" href="#">See all messages!
		                            <span id="unreaded">8 unreaded.</span> </a>
		                        </li>
		                    </ul>
		                    <br class="clear" />
	                    </div><!-- end messages-box -->
	                    <a id="power" href="Default.aspx" title="Logout">logout</a>
                    </div>
                    <!-- end login -->
                    <div id="nav">
	                    <ul class="sf-menu sf-navbar">
	                        <li><a id="dashboard" href="Dashboard.aspx">Dashboard</a>
	                            <ul>
		                            <li>&nbsp;
		                            </li>
	                            </ul>
	                        </li>
	                        <li><a id="Employee" href="EmployeeRegistration.aspx">Employee</a>
	                            <ul>
		                            <li><a href="EmployeeRegistration.aspx">Employee</a> </li>					                    
		                            <li><a href="OfficalProfileDetails.aspx">Profile</a></li>
		                            <li><a href="SalaryMaster.aspx">Salary Master</a></li>
		                            <%--<li><a href="LeaveAllocationEmployee.aspx">Leave Allocation</a></li>--%>
		                            <%--<li><a href="ApplyForLeave.aspx">Apply Leave</a></li>--%>					                    
		                            <li><a href="SalaryRegistration.aspx">Salary Details</a></li>
		                            <li><a href="SalaryAdvance.aspx">Advance</a></li>
		                            <li><a href="Settlement.aspx">Settlement</a></li>
		                            <li><a href="EmployeeDeactiveMode.aspx">Employee Re-Activate</a></li>
		                            					                    
	                            </ul>
	                        </li>
	                        <li><a id="Reports" href="RptDepartmentwiseEmployee.aspx">Reports</a>
	                            <ul>
		                            <li><a href="RptDepartmentwiseEmployee.aspx">Employee Details</a></li>
		                            <%--<li><a href="RptAttendanceReport.aspx">Attendance Report</a></li>--%>
		                            <%--<li><a href="RptProbationperiod.aspx">Probationary Period</a></li>
		                            <li><a href="ContractBreak.aspx">Contract Details</a></li>--%>
		                            <li><a href="RptResign.aspx">Resignation Reports</a></li>
		                            <li><a href="PaySlipGen.aspx">Payslip</a></li>
		                            <%--<li><a href="RptSalary.aspx">Salary Details</a></li>--%>
		                            <%--<li><a href="SalarySummary.aspx">Salary Summary</a></li>--%>
	                                <li><a href="Incentive.aspx">Incentive Details</a></li>
	                                <li><a href="RptDepartmentSalaryAbstract.aspx">Salary Abstract</a></li>
	                            </ul>
	                        </li>
	                        <li><a id="OT" href="OverTime.aspx">OT</a>
	                            <ul>
	                                <li><a href="OverTime.aspx">OT</a></li>
	                                <li><a href="RptOT.aspx">OT Report</a></li>
	                            </ul>
	                        </li>
	                        <li><a id="Master" href="MstEmployeeType.aspx">Masters</a>
	                            <ul>
		                            <li><a href="MstEmployeeType.aspx">Employee Type</a></li>
		                            <li><a href="UserRegistration.aspx">User</a></li>
		                            <li><a href="MSTPFESI.aspx">PF</a></li>
		                            <li><a href="MstBank.aspx">Bank</a></li>
		                            <li><a href="MstDepartment.aspx">Department</a></li>
		                            <li><a href="MstDAfix.aspx">DA Arrears FIX</a></li>
	                            </ul>
	                        </li>
	                        <li class="current"><a id="Bonus" href="BonusForAll.aspx">Bonus</a>
	                            <ul>
		                            <li><a href="BonusForAll.aspx">Bonus</a></li>
		                            <li class="current"><a href="StandardBonus.aspx">Group Bonus</a></li>
		                            <li><a href="RptBonus.aspx">Bonus Report</a></li>					                    
	                            </ul>
	                        </li>
	                        <li><a id="ESI_PF" href="PFDownload.aspx">ESI & PF</a>
	                            <ul>
		                            <%--<li><a href="PFForm5.aspx">PF Form 5</a></li>--%>
		                            <li><a href="PFDownload.aspx">PF Download</a></li>
		                            <li><a href="ESIDownload.aspx">ESI Download</a></li>
		                            <li><a href="PFForm3Anew.aspx">PF Form 3A</a></li>
		                            <li><a href="PFform6A.aspx">PF Form 6A</a></li>
		                            <%--<li><a href="FOrm10.aspx">Form 10</a></li>
		                            <li><a href="PFForm3Anew.aspx">PF Form 3A</a></li>
		                            <li><a href="PFform6A.aspx">PF Form 6A</a></li>
		                            <li><a href="ESIForm7.aspx">ESI Form 7</a></li>--%>
	                            </ul>
	                        </li>
	                        <li><a id="Upload" href="UploadEmployee.aspx">Upload</a>
	                            <ul>
	                            <li><a href="UploadEmployee.aspx">Employee Upload</a></li>
		                            <li><a href="AttenanceUpload.aspx">Attenance Upload</a></li>
		                            	<li><a href="UploadOT.aspx">OT Upload</a></li>
		                            	<%--<li><a href="LoadEmpmusters.aspx">Employee Master</a></li>--%>
	                            </ul>
	                        </li>
                        </ul>
                    </div>
                    <!-- end div#nav -->
                </div>
                <!-- end div#header-top -->
                <div class="breadCrumb">
                    <ul>
                        <li class="first"><a href="#">Home</a> </li>
                        <li><a href="#">Users</a> </li>
                        <li><a href="#">Roles</a> </li>
                        <li class="last">Group Bonus</li>
                    </ul>
                </div>
                <!-- end div#breadCrumb -->
                <%--<div id="search-box">
                    <form id="searchform" action="" method="get">
                        <fieldset class="search"><span>Search</span>
                        <input class="box" type="text" />
                        <button class="btn" title="Submit Search">Search</button></fieldset>
                    </form>
                </div>--%>
                <!-- end div#search-box -->
            </div>
            <!-- end header -->
            <div id="page-wrap">
                <div id="right-sidebar">
                   <div class="innerdiv">
	                    <h2 class="head">DownLoad Format</h2>
	                    <div class="innercontent clear">
		                    <h4>DownLoad Format</h4>
		                    <ul id="comments" class="tooltip-enabled">
		                    <asp:Button ID="btncontract" runat="server" Text="Contract Report" Width="200" 
                                    CssClass="button green" onclick="btncontract_Click" />
		                        <asp:Button ID="btnEmp" runat="server" Text="Employee Download" Width="200" 
                                    CssClass="button green" onclick="btnEmp_Click" />
		                        <asp:Button ID="btnatt" runat="server" Text="Attenance Download" Width="200" 
                                    CssClass="button green" onclick="btnatt_Click"/>
		                        <asp:Button ID="btnLeave" runat="server" Text="Leave Download" Width="200" 
                                    CssClass="button green" OnClick="btnLeave_Click" />
                                    <asp:Button ID="btnOT" runat="server" Text="OT Download" Width="200" 
                                    CssClass="button green" onclick="btnOT_Click" />
		                        <asp:Button ID="btnSal" runat="server" Text="Salary Download" Width="200" 
                                    CssClass="button green" onclick="btnSal_Click" />
		                    </ul>
		                    <%--<img src="new_images/icons/right_img.jpg" alt="" width="230"/>--%>
	                    </div>
                    </div>
                    <div class="clear"></div>                    
                </div>
	            <!-- end right-sidebar --> 
	            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
	                    <ContentTemplate>
	                        <div id="main-content">
                                <br /><br /><br /><br /> 
	                            <div class="clear_body">
                                    <div class="innerdiv">
                                        <div class="innercontent">
                                            <!-- tab "panes" -->				                
                                                <div>
                                                    <table class="full" >
                                                         <thead>
                                                            <tr align="center">
                                                                <th colspan="5" style="text-align:center;"><h4>GROUP BONUS</h4></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td><asp:Label ID="lblCategory" runat="server" Text="Category" Font-Bold="true" ></asp:Label></td>
                                                                <td><asp:DropDownList ID="ddlcategory" runat="server" Width="100" Height="25" 
                                                                        AutoPostBack="true" onselectedindexchanged="ddlcategory_SelectedIndexChanged" ></asp:DropDownList></td>
                                                                <td><asp:Label ID="lbldepartment" runat="server" Text="Department" Font-Bold="true" ></asp:Label></td>
                                                                <td><asp:DropDownList ID="ddldepartment" runat="server" 
                                                                        AutoPostBack="true" TabIndex="2" Width="180" Height="25" 
                                                                        onselectedindexchanged="ddldepartment_SelectedIndexChanged" ></asp:DropDownList></td>
                                                                        <td><asp:Button ID="btnclick" runat="server" Text="Click" Width="78" 
                                                                                Height="28" onclick="btnclick_Click" CssClass="button green" /></td>
                                                            </tr>
                                                            
                                                            <tr>
                                                                <td><asp:Label ID="lblFinance" runat="server" Text="Financial Year" Font-Bold="true" ></asp:Label></td>
                                                                <td colspan="4"><asp:DropDownList ID="ddFinance" runat="server" Width="180" 
                                                                        Height="25" onselectedindexchanged="ddFinance_SelectedIndexChanged" ></asp:DropDownList></td>
                                                                </tr>
                                                                <asp:Panel ID="pnl" runat="server" Visible="false" >
                                                                <tr>
                                                                
                                                                    <td colspan="5">
                                                                    <asp:CheckBox ID="chkbasic" runat="server" Text="Basic" AutoPostBack="true" 
                                                                            Font-Bold="true" Width="140" Font-Size="10" 
                                                                             />
                                                                    <asp:CheckBox ID="chkHRA" runat="server" Text="HRA" AutoPostBack="true" 
                                                                            Font-Bold="true" Width="140" Font-Size="10" 
                                                                             />
                                                                    <asp:CheckBox ID="chkFDA" runat="server" Text="FDA" AutoPostBack="true" 
                                                                            Font-Bold="true" Width="140" Font-Size="10" 
                                                                             />
                                                                    <asp:CheckBox ID="chkVDA" runat="server" Text="VDA" AutoPostBack="true" 
                                                                            Font-Bold="true" Width="140" Font-Size="10" 
                                                                             />
                                                                    <asp:CheckBox ID="chkAllowance1" runat="server" Text="Allowance 1" 
                                                                            AutoPostBack="true" Font-Bold="true"  Width="140" Font-Size="10" 
                                                                            />
                                                                    <asp:CheckBox ID="chkAllowance2" runat="server" Text="Allowance 2" 
                                                                            AutoPostBack="true" Font-Bold="true" Width="140" Font-Size="10" 
                                                                            />
                                                                    </td>
                                                            </tr>
                                                            <tr >
                                                                    <td colspan="5">
                                                                        <asp:CheckBox ID="chkDeduction1" runat="server" Text="Deduction1" 
                                                                            AutoPostBack="true" Font-Bold="true" Width="140" Font-Size="10" 
                                                                             />
                                                                        <asp:CheckBox ID="chkdeduction2" runat="server" Text="Deduction2" 
                                                                            AutoPostBack="true" Font-Bold="true" Width="140" Font-Size="10" 
                                                                             />
                                                                    </td>        
                                                            </tr>
                                                            </asp:Panel>
                                                            <tr>
                                                                            <td><asp:Label ID="lblpercentage" runat="server" Text="Percentage" Font-Bold="true" ></asp:Label> </td>
                                                                            <td><asp:TextBox ID="txtpercentage" runat="server" Text="0" 
                                                                                    ontextchanged="txtpercentage_TextChanged" ></asp:TextBox>
                                                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR8" runat="server" FilterMode="ValidChars" FilterType="Numbers,Custom" TargetControlID="txtpercentage" ValidChars=".">
                                                                                                                                </cc1:FilteredTextBoxExtender></td>
                                                                            <td colspan="3"><asp:Button ID="btnpercentagecal" runat="server" Text="Percentage" 
                                                                                    Width="100" Height="28" Font-Bold="true" CssClass="button green" 
                                                                                    onclick="btnpercentagecal_Click" /></td>
                                                                        </tr>
                                                                            <tr id="panelgridStaff" runat="server" visible="true">
                                                                <td colspan="5">
                                                                    <asp:GridView ID="GridviewStaff" runat="server" AutoGenerateColumns="false" AllowPaging="true" PageSize="10">
                                                                        <Columns>
                                                                        <asp:TemplateField>
                                                                        <HeaderTemplate>Sl No</HeaderTemplate>
                                                                        <ItemTemplate><%# Container.DataItemIndex + 1   %></ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField>
                                                                        <HeaderTemplate> Emp Number</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                        <asp:Label ID="lblempno" runat="server" Text='<%# Eval("EmpNo") %>' ></asp:Label>
                                                                        </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField>
                                                                        <HeaderTemplate>Existing No</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                        <asp:Label ID="lblextNo" runat="server" Text='<%# Eval("ExisistingCode") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                          <asp:TemplateField>
                                                                        <HeaderTemplate>Emp Name</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                        <asp:Label ID="lblEmpName" runat="server" Text='<%# Eval("EmpName") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                       <%-- <asp:TemplateField Visible="false">
                                                                        <HeaderTemplate>Basic Salary</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                        <asp:Label ID="lblbasic" runat="Server" Text='<%# Eval("Base") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        
                                                                        <asp:TemplateField Visible="false">
                                                                        <HeaderTemplate>HRA</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                        <asp:Label ID="lblHRA" runat="server" Text='<%# Eval("HRA") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        
                                                                        <asp:TemplateField Visible="false">
                                                                        <HeaderTemplate>VDA</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                        <asp:Label ID="VDA" runat="Server" Text='<%# Eval("VDA") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        
                                                                        <asp:TemplateField Visible="false">
                                                                        <HeaderTemplate>FDA</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                        <asp:Label ID="FDA" runat="server" Text='<%# Eval("FDA") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        
                                                                        <asp:TemplateField Visible="false">
                                                                        <HeaderTemplate>Allowance 1</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                        <asp:Label ID="lblAllowancw1" runat="server" Text='<%# Eval("Alllowance1amt") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField Visible="false">
                                                                        <HeaderTemplate>Allownce 2</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                        <asp:Label ID="lblAllowancw2" runat="server" Text='<%# Eval("Allowance2amt") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField Visible="false">
                                                                        
                                                                        <HeaderTemplate>deduction 1</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                        <asp:Label ID="lblded1" runat="Server" Text='<%# Eval("Deduction1") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField Visible="false">
                                                                        <HeaderTemplate>deduction 2</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                        <asp:Label ID="lblded2" runat="Server" Text='<%# Eval("Deduction2") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        </asp:TemplateField>--%>
                                                                        
                                                                        <asp:TemplateField>
                                                                        <HeaderTemplate>DOJ</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                        <asp:Label ID="lblDOJ" runat="server" Text='<%# Eval("DOJ") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField>
                                                                        <HeaderTemplate>No of Days</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                        <asp:Label ID="lblDays" runat="server" Text=<%# Eval("Days") %>>></asp:Label>
                                                                        </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField>
                                                                        <HeaderTemplate>Total Wages</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                        <asp:Label ID="lblNetAmount" runat="server" Text=""></asp:Label>
                                                                        </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField>
                                                                        <HeaderTemplate>Bonus %</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                        <asp:Label ID="lblBonuspercentage" runat="server" Text=""></asp:Label>
                                                                        </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField>
                                                                        <HeaderTemplate>Bonus Amount/Month</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                        <asp:Label ID="lblBonusAmount" runat="server" Text=""></asp:Label>
                                                                        </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        
                                                                        <asp:TemplateField>
                                                                        <HeaderTemplate>Total Bonus</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                        <asp:Label ID="lblBonus" runat="server" Text=""></asp:Label>
                                                                        </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField>
                                                                        <HeaderTemplate>
                                                                        <asp:CheckBox ID="chkselectall" runat="server" onclick="javascript:SelectheaderCheckboxes_staff(this);" />
                                                                            All
                                                                        </HeaderTemplate>
                                                                        <ItemTemplate>
                                                                        <asp:CheckBox ID="chkClear" runat="server" />
                                                                        </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        </Columns>
                                                                    </asp:GridView>
                                                                </td>
                                                            </tr>
                                                             <tr id="PanelStaff3" runat="server">
                                                                <td align="center" colspan="5"><asp:Button ID="btnstaffSave" runat="server" 
                                                                        Text="Save" onclick="btnstaffSave_Click" Font-Bold="true" Width="75" Height="28" CssClass="button green" />
                                                                <asp:Button ID="btnCancel" runat="server" Text="Cancel" onclick="btnCancel_Click" Font-Bold="true" Width="75" Height="28" CssClass="button green" /> </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>               
            </div>
            <!-- end page-wrap -->
            <div id="footer">
                <ul>
                    <li>Copyright &copy; 2012. All rights reserved.</li>
                    <li>Powered by <a href="http://www.altius.co.in" target="_blank">Altius Infosystems</a></li>
                </ul>
            </div>
            <!-- end footer -->
        </form>
    </body>
</html>
