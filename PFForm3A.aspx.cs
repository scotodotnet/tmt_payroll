﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

using Altius.BusinessAccessLayer.BALDataAccess;

public partial class PFForm3A : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    PFForm3AClass objPF3A = new PFForm3AClass();
    string Mont = "";
    DateTime m;
    int d = 0;
    string Year = "";
    string SessionAdmin;
    protected void Page_Load(object sender, EventArgs e)
    {
        SessionAdmin = Session["Isadmin"].ToString();
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        string ss = Session["UserId"].ToString();
    }
    public void Month()
    {

        m = Convert.ToDateTime(txtpfdate.Text);
        d = m.Month;
        int mon = m.Month;
        int Ye = m.Year;
        Year = Convert.ToString(Ye);


        switch (d)
        {
            case 1:
                Mont = "January";
                break;

            case 2:
                Mont = "February";
                break;
            case 3:
                Mont = "March";
                break;
            case 4:
                Mont = "April";
                break;
            case 5:
                Mont = "May";
                break;
            case 6:
                Mont = "June";
                break;
            case 7:
                Mont = "July";
                break;
            case 8:
                Mont = "August";
                break;
            case 9:
                Mont = "September";
                break;
            case 10:
                Mont = "October";
                break;
            case 11:
                Mont = "November";
                break;
            case 12:
                Mont = "December";
                break;
            default:
                break;
        }
    }
    protected void btnsearch_Click(object sender, EventArgs e)
    {

        try
        {
            if (txtempno.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Your Employee Registration Number..!');", true);
            }
            else if (txtpfdate.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Please Select Date..!');", true);
            }
            else
            {
                Month();
                DataTable dt = new DataTable();
                dt = objdata.PFMonthlyStatement(txtempno.Text, Mont, Year);
                txtpfnumber.Text = dt.Rows[0]["PFNumber"].ToString();
                txtamountofwages.Text = dt.Rows[0]["BasicandDA"].ToString();
                txtepf.Text = dt.Rows[0]["ProvidentFund"].ToString();

                double Basic1 = Convert.ToInt32(txtamountofwages.Text);
                double PensonFund = Basic1 * 8.33 / 100;
                double EPFDiff = Convert.ToInt32(txtepf.Text) - PensonFund;
                string strEPFDiff = Convert.ToString(EPFDiff);

                string strPensFund = Convert.ToString(PensonFund);

                txtpensionfund.Text = strPensFund;
                txtepfdiff.Text = strEPFDiff;
            }
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Server Error Contact Admin..!');", true);
        }

    }
    protected void btnsave_Click(object sender, EventArgs e)
    {
        try
        {
            bool ErrFlag = false;
            if (txtempno.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Your Employee Registration Number..!');", true);
            }
            else if (txtpfdate.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Please Select Your PF Date..!');", true);
            }
            else if (txtrefundadd.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Your Refund Advance..!');", true);
            }
            else if (txtnofdaynon.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Your No of Days Non Contribution Days...!');", true);
            }

            else
            {
                Month();
                PFMonthlySatementClass objPFM = new PFMonthlySatementClass();
                objPF3A.EmpNo = txtempno.Text;
                objPF3A.PFNumber = txtpfnumber.Text;
                objPF3A.AmountWages = txtamountofwages.Text;
                objPF3A.EPF = txtepf.Text;
                objPF3A.RateofHiger = txtrateofvolun.Text;
                objPF3A.EPFDiff = txtepfdiff.Text;
                objPF3A.PensonFund = txtpensionfund.Text;
                objPF3A.RefundofAdd = txtrefundadd.Text;
                objPF3A.NonContribution = txtnofdaynon.Text;
                objPF3A.CompCdnumber = txtcompnayCd.Text;
                objPF3A.CompAdd1 = txtcompadd1.Text;
                objPF3A.CompAdd2 = txtcompanyadd2.Text;
                objPF3A.Location = txtlocation.Text;

                DateTime PFDate;
                PFDate = DateTime.ParseExact(txtpfdate.Text, "dd-MM-yyyy", null);
                ErrFlag = true;
                if (ErrFlag)
                {
                    objdata.InsertPFFor3AStatement(objPF3A, Mont, Year, PFDate);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Your Reocrd Registered Successfully..!');", true);
                    clear();
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Your Correct Value Formate..!');", true);
                }
            }
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Server Error Contact Admin....!');", true);
        }
    }

    void clear()
    {
        txtempno.Text = "";
        txtpfnumber.Text = "";
        txtamountofwages.Text = "";
        txtepf.Text = "";
        txtrateofvolun.Text = "";
        txtepfdiff.Text = "";
        txtpensionfund.Text = "";
        txtrefundadd.Text = "";
        txtnofdaynon.Text = "";
        txtcompnayCd.Text = "";
        txtcompadd1.Text = "";
        txtcompanyadd2.Text = "";
        txtlocation.Text = "";

    }
    protected void btnclear_Click(object sender, EventArgs e)
    {
        clear();
    }
}
