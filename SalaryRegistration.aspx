<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SalaryRegistration.aspx.cs" Inherits="SPMSalaryRegistration" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
    <head id="Head1" runat="server">
        <title>Payroll Management Systems</title>
        <link href="new_css/main.css" rel="stylesheet" type="text/css" />
        <link href="new_css/colors/dark_blue.css" rel="stylesheet" type="text/css" />
        <link href="new_css/superfish.css" rel="stylesheet" type="text/css" />

        <script src="new_js/jquery-1.4.2.min.js" type="text/javascript"></script>
        <script src="new_js/jquery.hoverintent.minified.js" type="text/javascript"></script>
        <script src="new_js/superfish.js" type="text/javascript"></script>
        <script src="new_js/jquery.tools.min.js" type="text/javascript"></script>
        <%--<script src="new_js/jquery.simplemodal.js" type="text/javascript"></script>--%>
        <script src="new_js/jquery.notifybar.js" type="text/javascript"></script>
        <script src="new_js/jquery.tipsy.js" type="text/javascript"></script>
        <script src="new_js/enhance.js" type="text/javascript"></script>
        <script src="new_js/excanvas.js" type="text/javascript"></script>
        <script src="new_js/visualize.jquery.js" type="text/javascript"></script>
        <script src="new_js/jquery.collapsible.js" type="text/javascript"></script>
        <script src="new_js/jquery.autosuggest.packed.js" type="text/javascript"></script>
        <script src="new_js/platinum-admin.js" type="text/javascript"></script>
        <style>
            .sf-navbar li ul 
            {
	            width: 1100px; /*IE6 soils itself without this*/
	            margin: 0px auto;
	            margin-left:-20em;	            
            }
        </style>
    </head>
    <body>
        <form id="form1" runat="server">
            <cc1:ToolkitScriptManager runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true"
                ID="ScriptManager1" EnablePartialRendering="true">
            </cc1:ToolkitScriptManager>
            <div id="header">
                <div id="header-top">
                    <div id="logo">
	                    <h1>&nbsp;</h1>
	                    <span id="slogan">&nbsp;</span> 
	                </div>
                    <!-- end logo -->
                    <div id="login-info">
                        <img id="profile" alt="profile" src="./new_images/icons/user.png" />
                        <p id="top"><span id="name"><asp:Label ID="lblusername" runat="server"></asp:Label></span><br />
                        <span id="links">
                        <%--<a id="message-link" href="#" title="click to see the messages!">
                        <span id="message-count">1</span> new message. </a>--%></span></p>
                        <div>
                            <asp:Label ID="lblComany" runat="server" Text="" Font-Bold="true"></asp:Label>
                        </div>
	                    <div id="messages-box">
		                    <h4>Messages</h4>
	                        <h5><a id="new-message" href="#">New Message</a></h5>
	                        <hr />
	                        <ul id="messages">
		                        <li class="new"><a href="#">
		                            <img alt="user" src="./new_images/icons/sender.png" />
		                            <span class="sender">John Doe</span>
		                            <span class="description">Hi this is just a description...</span>
		                            <span class="date">20 minutes ago...</span> </a>
		                        </li>
		                        <li><a href="#">
		                            <img alt="user" src="./new_images/icons/sender.png" />
		                            <span class="sender">John Doe</span>
		                            <span class="description">Hi this is just a description...</span>
		                            <span class="date">24 minutes ago...</span> </a>
		                        </li>
		                        <li><a href="#">
		                            <img alt="user" src="./new_images/icons/sender.png" />
		                            <span class="sender">John Doe</span>
		                            <span class="description">Hi this is just a description...</span>
		                            <span class="date">50 minutes ago...</span> </a>
		                        </li>
		                        <li id="last"><a id="all-messages" href="#">See all messages!
		                            <span id="unreaded">8 unreaded.</span> </a>
		                        </li>
		                    </ul>
		                    <br class="clear" />
	                    </div><!-- end messages-box -->
	                    <a id="power" href="Default.aspx" title="Logout">logout</a>
                    </div>
                    <!-- end login -->
                    <div id="nav">
	                    <ul class="sf-menu sf-navbar">
	                        <li><a id="dashboard" href="Dashboard.aspx">Dashboard</a>
	                            <ul>
		                            <li>&nbsp;
		                            </li>
	                            </ul>
	                        </li>
	                        <li class="current"><a id="Employee" href="EmployeeRegistration.aspx">Employee</a>
	                            <ul>
		                            <li><a href="EmployeeRegistration.aspx">Employee</a> </li>					                    
		                            <li><a href="OfficalProfileDetails.aspx">Profile</a></li>
		                            <li><a href="SalaryMaster.aspx">Salary Master</a></li>
		                            <%--<li><a href="LeaveAllocationEmployee.aspx">Leave Allocation</a></li>--%>
		                            <%--<li><a href="ApplyForLeave.aspx">Apply Leave</a></li>--%>						                    
		                            <li class="current"><a href="SalaryRegistration.aspx">Salary Details</a></li>
		                            <li><a href="SalaryAdvance.aspx">Advance</a></li>
		                             <li><a href="Settlement.aspx">Settlement</a></li>
		                            <li><a href="EmployeeDeactiveMode.aspx">Employee Re-Activate</a></li>
		                            
		                             				                    
	                            </ul>
	                        </li>
	                         <li><a id="Reports" href="RptDepartmentwiseEmployee.aspx">Reports</a>
	                            <ul>
		                            <li><a href="RptDepartmentwiseEmployee.aspx">Employee Details</a></li>
		                            <%--<li><a href="RptAttendanceReport.aspx">Attendance Report</a></li>--%>
		                            <%--<li><a href="RptProbationperiod.aspx">Probationary Period</a></li>
		                            <li><a href="ContractBreak.aspx">Contract Details</a></li>--%>
		                            <li><a href="RptResign.aspx">Resignation Reports</a></li>
		                            <li><a href="PaySlipGen.aspx">Payslip</a></li>
		                            <%--<li><a href="RptSalary.aspx">Salary Details</a></li>--%>
		                            <%--<li><a href="SalarySummary.aspx">Salary Summary</a></li>--%>
	                                <li><a href="Incentive.aspx">Incentive Details</a></li>
	                                <li><a href="RptDepartmentSalaryAbstract.aspx">Salary Abstract</a></li>
	                            </ul>
	                        </li>
	                        <li><a id="OT" href="OverTime.aspx">OT</a>
	                            <ul>
	                                <li><a href="OverTime.aspx">OT</a></li>
	                                <li><a href="RptOT.aspx">OT Report</a></li>
	                            </ul>
	                        </li>
	                        <li><a id="Master" href="MstEmployeeType.aspx">Masters</a>
	                            <ul>
		                            <li><a href="MstEmployeeType.aspx">Employee Type</a></li>
		                            <li><a href="UserRegistration.aspx">User</a></li>
		                            <li><a href="MSTPFESI.aspx">PF</a></li>
		                            <li><a href="MstBank.aspx">Bank</a></li>
		                            <li><a href="MstDepartment.aspx">Department</a></li>
		                            <li><a href="MstDAfix.aspx">DA Arrears FIX</a></li>
	                            </ul>
	                        </li>
	                        <li><a id="Bonus" href="BonusForAll.aspx">Bonus</a>
	                            <ul>
		                            <li><a href="BonusForAll.aspx">Bonus</a></li>
		                            <li><a href="StandardBonus.aspx">Group Bonus</a></li>
		                            <li><a href="RptBonus.aspx">Bonus Report</a></li>					                    
	                            </ul>
	                        </li>
	                        <li><a id="ESI_PF" href="PFDownload.aspx">ESI & PF</a>
	                            <ul>
		                            <%--<li><a href="PFForm5.aspx">PF Form 5</a></li>--%>
		                            <li><a href="PFDownload.aspx">PF Download</a></li>
		                            <li><a href="ESIDownload.aspx">ESI Download</a></li>
		                            <li><a href="PFForm3Anew.aspx">PF Form 3A</a></li>
		                            <li><a href="PFform6A.aspx">PF Form 6A</a></li>
		                            <%--<li><a href="FOrm10.aspx">Form 10</a></li>
		                            <li><a href="PFForm3Anew.aspx">PF Form 3A</a></li>
		                            <li><a href="PFform6A.aspx">PF Form 6A</a></li>
		                            <li><a href="ESIForm7.aspx">ESI Form 7</a></li>--%>
	                            </ul>
	                        </li>
	                        <li><a id="Upload" href="UploadEmployee.aspx">Upload</a>
	                            <ul>
	                            <li><a href="UploadEmployee.aspx">Employee Upload</a></li>
		                            <li><a href="AttenanceUpload.aspx">Attenance Upload</a></li>
		                            	<li><a href="UploadOT.aspx">OT Upload</a></li>
		                            	<%--<li><a href="LoadEmpmusters.aspx">Employee Master</a></li>  --%>
	                            </ul>
	                        </li>
                        </ul>
                    </div>
                    <!-- end div#nav -->
                </div>
                <!-- end div#header-top -->
                <div class="breadCrumb">
                    <ul>
                        <li class="first"><a href="#">Home</a> </li>
                        <li><a href="#">Users</a> </li>
                        <li><a href="#">Roles</a> </li>
                        <li class="last">Salary Details</li>
                    </ul>
                </div>
                <!-- end div#breadCrumb -->
                <%--<div id="search-box">
                    <form id="searchform" action="" method="get">
                        <fieldset class="search"><span>Search</span>
                        <input class="box" type="text" />
                        <button class="btn" title="Submit Search">Search</button></fieldset>
                    </form>
                </div>--%>
                <!-- end div#search-box -->
            </div>
            <!-- end header -->
            <div id="page-wrap">
                <div id="right-sidebar">
                    <div class="innerdiv">
	                    <h2 class="head">DownLoad Format</h2>
	                    <div class="innercontent clear">
		                    <h4>DownLoad Format</h4>
		                    <ul id="comments" class="tooltip-enabled">
		                    <asp:Button ID="btncontract" runat="server" Text="Contract Report" Width="200" 
                                    CssClass="button green" onclick="btncontract_Click" />
		                        <asp:Button ID="btnEmp" runat="server" Text="Employee Download" Width="200" 
                                    CssClass="button green" onclick="btnEmp_Click" />
		                        <asp:Button ID="btnatt" runat="server" Text="Attenance Download" Width="200" 
                                    CssClass="button green" onclick="btnatt_Click"/>
		                        <asp:Button ID="btnLeave" runat="server" Text="Leave Download" Width="200" 
                                    CssClass="button green" OnClick="btnLeave_Click" />
                                    <asp:Button ID="btnOT" runat="server" Text="OT Download" Width="200" 
                                    CssClass="button green" onclick="btnOT_Click" />
		                        <asp:Button ID="btnSal" runat="server" Text="Salary Download" Width="200" 
                                    CssClass="button green" onclick="btnSal_Click" />
		                    </ul>
		                    <%--<img src="new_images/icons/right_img.jpg" alt="" width="230"/>--%>
	                    </div>
                    </div>
                    <div class="clear"></div>                    
                </div>
	            <!-- end right-sidebar -->
	            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
	                    <ContentTemplate>
	                        <div id="main-content">
                                <br /><br /><br /><br /> 
	                            <div class="clear_body">
                                    <div class="innerdiv">
                                        <div class="innercontent">
                                            <!-- tab "panes" -->				                
                                                <div>
                                                    <table  class="full">
                                                        <thead>
                                                            <tr align="center">
                                                                <th colspan="4" style="text-align:center;"><h4>SALARY DETAILS</h4></th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <tr align="center">
                                                                <td colspan="4">
                                                                    <asp:Label ID="lblWagesType" runat="server" Text="Wages Type" Font-Bold="true" ></asp:Label>
                                                                    <asp:RadioButtonList ID="rbsalary" runat="server" AutoPostBack="true" 
                                                                        RepeatColumns="3" onselectedindexchanged="rbsalary_SelectedIndexChanged">
                                                                    <asp:ListItem Text ="Weekly Wages" Value="1"></asp:ListItem>
                                                                    <asp:ListItem Text="Monthly Wages" Value="2" ></asp:ListItem>                                                                                                                                       
                                                                    <asp:ListItem Text="Bi-Monthly Wages" Value="3"></asp:ListItem>
                                                                    </asp:RadioButtonList>
                                                                </td>
                                                            </tr>
                                                                <tr>
                                                                    <td><asp:Label ID="lblcategory" runat="server" Text="Category" Font-Bold="true" ></asp:Label></td>
                                                                    <td><asp:DropDownList ID="ddlcategory" runat="server" Width="100" Height="25" 
                                                                    onselectedindexchanged="ddlcategory_SelectedIndexChanged" AutoPostBack="true" ></asp:DropDownList></td>
                                                                    <td><asp:Label ID="lbldepartment" runat="server" Text="Department" Font-Bold="true" ></asp:Label></td>                                                                
                                                                    <td><asp:DropDownList ID="ddldepartment" runat="server" Width="180" Height="25" 
                                                                    onselectedindexchanged="ddldepartment_SelectedIndexChanged" TabIndex="1" AutoPostBack="true" ></asp:DropDownList>
                                                                    <asp:Button ID="btnclick" runat="server" Text="Click" Width="78" 
                                                                    Height="28" onclick="btnclick_Click" CssClass="button green" /></td>                                                                            
                                                                </tr>
                                                                <tr>
                                                                        <td><asp:Label ID="lblexisitingno" runat="server" Text="Existing Number" Font-Bold="true"></asp:Label></td>
                                                                        <td><asp:TextBox ID="txtexistingNo" runat="server" ontextchanged="txtexistingNo_TextChanged" Visible="false" ></asp:TextBox>
                                                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender_ExisistingCode" runat="server"
                                                                        TargetControlID="txtexistingNo" FilterMode="ValidChars" FilterType="UppercaseLetters,LowercaseLetters,Numbers,Custom" ValidChars="0123456789+- "></cc1:FilteredTextBoxExtender>
                                                                        <asp:DropDownList ID="ddToken" runat="server" AutoPostBack="true" Width="150" 
                                                                        Height="25" onselectedindexchanged="ddToken_SelectedIndexChanged"
                                                                         ></asp:DropDownList>
                                                                        </td>                                                               
                                                                        <td colspan="2"><asp:Button ID="btnexitingSearch" runat="server" Text="Search" Width="78" Height="28" CssClass="button green" Visible="false" onclick="btnexitingSearch_Click" /></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td><asp:Label ID="lblempno" runat="server" Text="Employee No" Font-Bold="true"></asp:Label></td>  
                                                                        <td><asp:DropDownList ID="ddlempCodeStaff" runat="server" AutoPostBack="true" Width="180" Height="25" onselectedindexchanged="ddlempCode1_SelectedIndexChanged" TabIndex="1" ></asp:DropDownList></td>
                                                                        <td><asp:Label ID="lblempname" runat="server" Text="Employee Name" Font-Bold="true"></asp:Label></td>
                                                                        <td><asp:DropDownList ID="ddlempNameStaff" runat="server" AutoPostBack="true" Width="180" Height="25" 
                                                                        onselectedindexchanged="ddlEmpName1_SelectedIndexChanged" TabIndex="2" ></asp:DropDownList></td>
                                                                    </tr>
                                                                    <tr id="PanelMonths" runat="server" visible="false">
                                                                        <td><asp:Label ID="Label15" runat="server" Text="lblMonths" Font-Bold="true" Visible="true" ></asp:Label></td>  
                                                                        <td colspan="3">
                                                                            <asp:DropDownList ID="ddlMonths" runat="server" AutoPostBack="true" Width="180" 
                                                                                Height="25" onselectedindexchanged="ddlMonths_SelectedIndexChanged">
                                                                        </asp:DropDownList>
                                                </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="lblfrom" runat="server" Text="From Date" Font-Bold="true"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="txtfrom" runat="server" CssClass="text" AutoPostBack="True" TabIndex="3" ></asp:TextBox>
                                                                            <cc1:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtfrom" Format ="dd-MM-yyyy" CssClass ="orange" Enabled="true"></cc1:CalendarExtender>
                                                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR38" runat="server" FilterMode="ValidChars" FilterType="Numbers,Custom" TargetControlID="txtfrom" ValidChars="0123456789/- "></cc1:FilteredTextBoxExtender>
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="lblToDate" runat="server" Text="To Date" Font-Bold="true"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="txtTo" runat="server" CssClass="text" AutoPostBack="True" 
                                                                                TabIndex="3" ontextchanged="txtTo_TextChanged" ></asp:TextBox>
                                                                            <cc1:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtTo" Format ="dd-MM-yyyy" CssClass ="orange" Enabled="true"></cc1:CalendarExtender>
                                                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR39" runat="server" FilterMode="ValidChars" FilterType="Numbers,Custom" TargetControlID="txtTo" ValidChars="0123456789/- "></cc1:FilteredTextBoxExtender>
                                                                        </td>
                                                                        
                                                                    </tr>
                                                                    <tr>
                                                                        <td><asp:Label ID="lblsalarday" runat="server" Text="Salary Date" Font-Bold="true"></asp:Label></td>
                                                                        <td><asp:TextBox ID="txtsalaryday" runat="server" CssClass="text" AutoPostBack="True" ontextchanged="txtsalaryday_TextChanged" TabIndex="3" ></asp:TextBox>
                                                                        <cc1:CalendarExtender ID="Caleder_txtdob" runat="server" TargetControlID="txtsalaryday" Format ="dd-MM-yyyy" CssClass ="orange" Enabled="true"></cc1:CalendarExtender>
                                                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR9" runat="server" FilterMode="ValidChars" FilterType="Numbers,Custom" TargetControlID="txtsalaryday" ValidChars="0123456789/- "></cc1:FilteredTextBoxExtender></td>
                                                                        <td><asp:Label ID="lblsalarythroug"  runat="server" Text="Salary Through" Font-Bold="true" TabIndex="4" Visible="false"></asp:Label></td>
                                                                        <td colspan="1"><asp:RadioButtonList ID="rbtnsalarythroug" runat="server" RepeatColumns="2" Enabled="false" onselectedindexchanged="rbtnsalarythroug_SelectedIndexChanged" AutoPostBack="true" Visible="false">
                                                                        <asp:ListItem Selected="False" Text="Cash" Value="1"></asp:ListItem>
                                                                        <asp:ListItem Selected="False" Text="Bank" Value="2"></asp:ListItem>
                                                                        </asp:RadioButtonList></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td><asp:Label ID="lblfaltrate" runat="server" Text="Financial Year" Font-Bold="true" ></asp:Label></td>
                                                                        <td><asp:DropDownList ID="ddlFinaYear" runat="server" Width="180" Height="25" 
                                                                                onselectedindexchanged="ddlFinaYear_SelectedIndexChanged" ></asp:DropDownList></td>
                                                                        <td><asp:Label ID="lblEmpDays" runat="server" Text="Total Working Days" Font-Bold="true" ></asp:Label></td>
                                                                        <td colspan="1"><asp:TextBox ID="txtwork" runat="server" CssClass="text" Enabled="false" ></asp:TextBox>
                                                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR33" runat="server" FilterMode="ValidChars" FilterType="Numbers,Custom" TargetControlID="txtwork" ValidChars="0123456789."></cc1:FilteredTextBoxExtender>
                                                                        <%--<asp:Label ID="lblNFH" runat="server" Text="NFh" Font-Bold="true" Visible="false" ></asp:Label>--%>
                                                                        <%--<asp:TextBox ID="txtNFh" runat="server" Text="" Enabled="false" CssClass="text" Width="30" Visible="false" ></asp:TextBox>--%>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="lblNFH" runat="server" Text="NFH Days" Font-Bold="true" Visible="true" ></asp:Label>
                                                                        
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="txtNFh" runat="server" Text="" Enabled="false" CssClass="text" Width="30" Visible="true" ></asp:TextBox>
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="lblCl" runat="server" Text="CL" Font-Bold="true" ></asp:Label>
                                                                        </td>
                                                                        <td colspan="1">
                                                                            <asp:TextBox ID="txtcl" runat="server" Text="0" Enabled="false" CssClass="text" Width="30" ></asp:TextBox>
                                                                            <asp:Label ID="lblweek" runat="server" Text="Week off Days" Font-Bold="true" ></asp:Label>
                                                                            <asp:TextBox ID="txtweekoff" runat="server" Enabled="false" CssClass="text" Width="30" ></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td><asp:Label ID="lblnoofworking" runat="server" Text="Employee Working Days" Font-Bold="true"  ></asp:Label></td>
                                                                        <td><asp:TextBox ID="txtempdays" runat="server" CssClass="text" Enabled="false" 
                                                                                ontextchanged="txtempdays_TextChanged" ></asp:TextBox>
                                                                         
                                                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR34" runat="server" FilterMode="ValidChars" FilterType="Numbers,Custom" TargetControlID="txtempdays" ValidChars="0123456789."></cc1:FilteredTextBoxExtender></td>
                                                                        <td><asp:Label ID="lblattendawithpay" runat="server" Text="Absent Days" Font-Bold="true" ></asp:Label></td>
                                                                        <td colspan="1"><asp:TextBox ID="txtlossofpay" runat="server" Enabled="false" CssClass="text" 
                                                                                ontextchanged="txtlossofpay_TextChanged" ></asp:TextBox></td>
                                                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR35" runat="server" FilterMode="ValidChars" FilterType="Numbers,Custom" TargetControlID="txtlossofpay" ValidChars="0123456789."></cc1:FilteredTextBoxExtender></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td><asp:Label ID="Label14" runat="server" Text="Home Town Days" Font-Bold="true"  ></asp:Label></td>
                                                                         <td><asp:TextBox ID="txthome" runat="server" CssClass="text" Enabled="false" 
                                                                                ontextchanged="txtempdays_TextChanged" ></asp:TextBox>
                                                                                <td><asp:Label ID="lblrest" runat="server" Text="Rest Days" Font-Bold="true"></asp:Label></td>
                                                                                <td colspan="1">
                                                                                    <asp:TextBox ID="txtrest" runat="server" Text="" Enabled="false"></asp:TextBox>
                                                                                </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="2" align="center"><asp:Label ID="lblearnings" runat="server" Text="EARNINGS" Font-Bold="true"></asp:Label></td>
                                                                        <td colspan="2" align="center"><asp:Label ID="lbldeductions" runat="server" Text="DEDUCTIONS" Font-Bold="true"></asp:Label></td>
                                                                    </tr>
                                                                    <asp:Panel ID="PanelPermenant" runat="server" Visible="false" >
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="lblBasic" runat="server" Text="Basic Salary" Font-Bold="true" ></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="txtbasic" runat="server" Text="0" Enabled="false" 
                                                                                 ></asp:TextBox>
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="lblPF" runat="server" Text="PF Amount" Font-Bold="true" ></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="txtpf" runat="server" Text="0" Enabled="false" ></asp:TextBox>
                                                                        </td>                                                                                                    
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="lblFDA" runat="server" Text="FDA Amount" Font-Bold="true" ></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="txtFDA" runat="server" Text="0" Enabled="false" ></asp:TextBox>
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="lblESI" runat="server" Text="ESI Amount" Font-Bold="true" ></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="txtESI" runat="server" Text="0" Enabled="false" ></asp:TextBox>
                                                                        </td>
                                                                        
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="lblVDA" runat="server" Text="VDA Amount" Font-Bold="true" ></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="txtVDA" runat="server" Text="0" Enabled="false" ></asp:TextBox>
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="lblAdvance" runat="server" Text="Advance Amount" Font-Bold="true" ></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="txtAdvance" runat="server" Text="0" Enabled="false" 
                                                                                ontextchanged="txtAdvance_TextChanged" AutoPostBack="true" ></asp:TextBox>
                                                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR4" runat="server" FilterMode="ValidChars" FilterType="Numbers,Custom" TargetControlID="txtAdvance" ValidChars="0123456789."></cc1:FilteredTextBoxExtender>
                                                                            <asp:Button ID="btnEdit" runat="server" Text="Edit" CssClass="button green" 
                                                                                onclick="btnEdit_Click" />
                                                                        </td>
                                                                        
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="lblHRA" runat="server" Text="HRA Amount" Font-Bold="true"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="txtHRA" runat="server" Text="0" Enabled="false" ></asp:TextBox>
                                                                        </td>   
                                                                        <td>
                                                                            <asp:Label ID="lblStamp" runat="server" Text="Stamp Charge" Font-Bold="true" ></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="txtStamp" runat="server" Text="0" 
                                                                                ontextchanged="txtStamp_TextChanged" AutoPostBack="true" ></asp:TextBox>
                                                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR1" runat="server" FilterMode="ValidChars" FilterType="Numbers,Custom" TargetControlID="txtStamp" ValidChars="0123456789."></cc1:FilteredTextBoxExtender>
                                                                        </td>                                                                      
                                                                    </tr>
                                                                    <tr>
                                                                    <td>
                                                                            <asp:Label ID="lblOT" runat="server" Text="OT Amount" Font-Bold="true" ></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="txtOT" runat="server" Text="0" Enabled="false" ></asp:TextBox>
                                                                        </td>
                                                                          
                                                                          <td>
                                                                            <asp:Label ID="lblunion" runat="server" Text="Union Charges" Font-Bold="true" ></asp:Label>
                                                                            
                                                                          </td>
                                                                          <td>
                                                                            <asp:TextBox ID="txtunion" runat="server" Text="0" 
                                                                                  ontextchanged="txtunion_TextChanged" AutoPostBack="true" ></asp:TextBox>
                                                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR5" runat="server" FilterMode="ValidChars" FilterType="Numbers,Custom" TargetControlID="txtunion" ValidChars="0123456789."></cc1:FilteredTextBoxExtender>
                                                                          </td>
                                                                          
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="lblAllowance1" runat="server" Text="Allowance 1" Font-Bold="true" ></asp:Label>
                                                                          </td>
                                                                          <td>
                                                                            <asp:TextBox ID="txtAllowance" runat="server" Text="0" 
                                                                                  ontextchanged="txtAllowance_TextChanged" AutoPostBack="true" 
                                                                                  style="height: 22px" ></asp:TextBox>
                                                                                  <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR2" runat="server" FilterMode="ValidChars" FilterType="Numbers,Custom" TargetControlID="txtAllowance" ValidChars="0123456789."></cc1:FilteredTextBoxExtender>
                                                                          </td>
                                                                        <td>
                                                                            <asp:Label ID="lbldeduction" runat="server" Text="Deduction 1" Font-Bold="true" ></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="txtdeduction" runat="server" Text="0" 
                                                                                ontextchanged="txtdeduction_TextChanged" AutoPostBack="true" ></asp:TextBox>
                                                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR3" runat="server" FilterMode="ValidChars" FilterType="Numbers,Custom" TargetControlID="txtdeduction" ValidChars="0123456789."></cc1:FilteredTextBoxExtender>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="lblAllowance2" runat="server" Text="Allowance 2" Font-Bold="true"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="txtAllowance2" runat="server" Text="0" AutoPostBack="true" 
                                                                                ontextchanged="txtAllowance2_TextChanged" ></asp:TextBox>
                                                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR6" runat="server" FilterMode="ValidChars" FilterType="Numbers,Custom" TargetControlID="txtAllowance2" ValidChars="0123456789."></cc1:FilteredTextBoxExtender>
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="lblDeduction2" runat="server" Text="Deduction 2" Font-Bold="true" ></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="txtdeduction2" runat="server" Text="0" 
                                                                                ontextchanged="txtdeduction2_TextChanged" AutoPostBack="true" ></asp:TextBox>
                                                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR7" runat="server" FilterMode="ValidChars" FilterType="Numbers,Custom" TargetControlID="txtdeduction2" ValidChars="0123456789."></cc1:FilteredTextBoxExtender>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                                                                                                   
                                                                        <td>
                                                                            <asp:Label ID="Label1" runat="server" Text="Allowance 3" Font-Bold="true"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="txall3" runat="server" Text="0" Font-Bold="true" 
                                                                                AutoPostBack="true" ontextchanged="txall3_TextChanged" ></asp:TextBox>
                                                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR8" runat="server" FilterMode="ValidChars" FilterType="Numbers,Custom" TargetControlID="txall3" ValidChars="0123456789."></cc1:FilteredTextBoxExtender>
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="Label2" runat="server" Text="Deduction 3" Font-Bold="true" ></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="txtded3" runat="server" Text="0" 
                                                                                ontextchanged="txtded3_TextChanged" AutoPostBack="true" ></asp:TextBox>
                                                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR10" runat="server" FilterMode="ValidChars" FilterType="Numbers,Custom" TargetControlID="txtded3" ValidChars="0123456789."></cc1:FilteredTextBoxExtender>
                                                                        </td> 
                                                                    </tr>
                                                                    <tr>                                                                              
                                                                        <td>
                                                                            <asp:Label ID="Label3" runat="server" Text="Allowance 4" Font-Bold="true"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="txtsll4" runat="server" Text="0" Font-Bold="true" 
                                                                                ontextchanged="txtsll4_TextChanged" AutoPostBack="true" ></asp:TextBox>
                                                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR11" runat="server" FilterMode="ValidChars" FilterType="Numbers,Custom" TargetControlID="txtsll4" ValidChars="0123456789."></cc1:FilteredTextBoxExtender>
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="Label4" runat="server" Text="Deduction 4" Font-Bold="true" ></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="txtded4" runat="server" Text="0" AutoPostBack="true" 
                                                                                ontextchanged="txtded4_TextChanged" ></asp:TextBox>
                                                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR12" runat="server" FilterMode="ValidChars" FilterType="Numbers,Custom" TargetControlID="txtded4" ValidChars="0123456789."></cc1:FilteredTextBoxExtender>
                                                                        </td>
                                                                        
                                                                    </tr>
                                                                    </asp:Panel>
                                                                    <asp:Panel ID="PanelStaff" runat="server" Visible="false" >
                                                                        <tr>
                                                                            <td>
                                                                                <asp:Label ID="lblSBasic" runat="server" Text="Basic Salary" Font-Bold="true" ></asp:Label>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="txtSBasic" runat="server" Text="0" Enabled="false" ></asp:TextBox>
                                                                            </td>
                                                                        
                                                                            <td>
                                                                                <asp:Label ID="lblSPF" runat="server" Text="PF Amount" Font-Bold="true" ></asp:Label>
                                                                            </td>
                                                                            <td colspan="1">
                                                                                <asp:TextBox ID="txtSPF" runat="server" Text="0" Enabled="false" ></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:Label ID="lblSAll1" runat="server" Text="Allowance 1" Font-Bold="true" ></asp:Label>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="txtSAll1" runat="server" Text="0" AutoPostBack="true" 
                                                                                    ontextchanged="txtSAll1_TextChanged" Enabled="false" ></asp:TextBox>
                                                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR13" runat="server" FilterMode="ValidChars" FilterType="Numbers,Custom" TargetControlID="txtSAll1" ValidChars="0123456789."></cc1:FilteredTextBoxExtender>
                                                                            </td>
                                                                            <td>
                                                                                <asp:Label ID="lblSESI" runat="server" Text="ESI Amount" Font-Bold="true" ></asp:Label>
                                                                            </td>
                                                                            <td colspan="1">
                                                                                <asp:TextBox ID="txtSesi" runat="server" Text="0" Enabled="false"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:Label ID="lblSAll2" runat="server" Text="Allowance 2" Font-Bold="true" ></asp:Label>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="txtSAll2" runat="server" Text="0" AutoPostBack="true" 
                                                                                    ontextchanged="txtSAll2_TextChanged" Enabled="false" ></asp:TextBox>
                                                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR14" runat="server" FilterMode="ValidChars" FilterType="Numbers,Custom" TargetControlID="txtSAll2" ValidChars="0123456789."></cc1:FilteredTextBoxExtender>
                                                                            </td>
                                                                            <td>
                                                                                <asp:Label ID="lblSAdvance" runat="server" Text="Advance Amount" Font-Bold="true" ></asp:Label>
                                                                            </td>
                                                                            <td colspan="1">
                                                                                <asp:TextBox ID="txtSAdvance" runat="server" Text="0" Enabled="True" 
                                                                                    AutoPostBack="true" ontextchanged="txtSAdvance_TextChanged" ></asp:TextBox>
                                                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR30" runat="server" FilterMode="ValidChars" FilterType="Numbers,Custom" TargetControlID="txtSAdvance" ValidChars="0123456789."></cc1:FilteredTextBoxExtender>
                                                                                <asp:Button ID="btnSadvance" runat="server" Text="Edit" CssClass="button green" onclick="btnSadvance_Click" Visible="false"
                                                                                 />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:Label ID="lblSAll3" runat="server" Text="Allowance 3" Font-Bold="true" ></asp:Label>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="txtSAll3" runat="server" Text="0" AutoPostBack="true" 
                                                                                    ontextchanged="txtSAll3_TextChanged" ></asp:TextBox>
                                                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR15" runat="server" FilterMode="ValidChars" FilterType="Numbers,Custom" TargetControlID="txtSAll3" ValidChars="0123456789."></cc1:FilteredTextBoxExtender>
                                                                            </td>
                                                                            <td>
                                                                                <asp:Label ID="Label11" runat="server" Font-Bold="true" Text="LOP"></asp:Label>
                                                                            </td>
                                                                            <td colspan="1">
                                                                                <asp:TextBox ID="txtSLOP" runat="server" AutoPostBack="true" 
                                                                                    ontextchanged="txtSLOP_TextChanged" Text="0" Enabled="false"></asp:TextBox>
                                                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR32" runat="server" 
                                                                                    FilterMode="ValidChars" FilterType="Numbers,Custom" TargetControlID="txtSLOP" 
                                                                                    ValidChars="0123456789.">
                                                                                </cc1:FilteredTextBoxExtender>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:Label ID="Label5" runat="server" Text="Allowance 4" Font-Bold="true" ></asp:Label>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="txtSAll4" runat="server" Text="0" AutoPostBack="true" ontextchanged="txtSAll4_TextChanged" 
                                                                                     ></asp:TextBox>
                                                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR16" runat="server" FilterMode="ValidChars" FilterType="Numbers,Custom" TargetControlID="txtSAll4" ValidChars="0123456789."></cc1:FilteredTextBoxExtender>
                                                                            </td>
                                                                            <td>
                                                                                <asp:Label ID="Label6" runat="server" Text="Deduction 1" Font-Bold="true" ></asp:Label>
                                                                            </td>
                                                                            <td colspan="1">
                                                                                <asp:TextBox ID="txtSded1" runat="server" Text="0" 
                                                                                    AutoPostBack="true" ontextchanged="txtSded1_TextChanged" 
                                                                                    style="height: 22px" Enabled="false" ></asp:TextBox>
                                                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR17" runat="server" FilterMode="ValidChars" FilterType="Numbers,Custom" TargetControlID="txtSded1" ValidChars="0123456789."></cc1:FilteredTextBoxExtender>
                                                                            </td>
                                                                        </tr>
                                                                        <asp:Panel ID="Panelstamp" runat="server" Visible="false" >
                                                                        <tr>
                                                                            <td>
                                                                            </td>
                                                                            <td>
                                                                            </td>
                                                                            <td colspan="1">
                                                                                <asp:TextBox ID="txtSstamp" runat="server" Text="0" Enabled="true" Visible="false"
                                                                                    ontextchanged="txtSstamp_TextChanged" ></asp:TextBox>
                                                                                 <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR37" runat="server" 
                                                                                FilterMode="ValidChars" FilterType="Numbers,Custom" TargetControlID="txtSstamp" 
                                                                                ValidChars="0123456789.">
                                                                            </cc1:FilteredTextBoxExtender>
                                                                            </td>
                                                                            </tr>
                                                                
                                                                </asp:Panel>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="lblSAll5" runat="server" Text="Allowance 5" Font-Bold="true" ></asp:Label>
                                                                    </td>                                                                            
                                                                    <td>
                                                                        <asp:TextBox ID="txtSAll5" runat="server" AutoPostBack="true" 
                                                                            ontextchanged="txtSded2_TextChanged" Text="0" Enabled="true"></asp:TextBox>
                                                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR40" runat="server" 
                                                                            FilterMode="ValidChars" FilterType="Numbers,Custom" TargetControlID="txtSAll5" 
                                                                            ValidChars="0123456789.">
                                                                        </cc1:FilteredTextBoxExtender>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="lblSded2" runat="server" Font-Bold="true" Text="Deduction 2"></asp:Label>
                                                                    </td>
                                                                    <td colspan="1">
                                                                        <asp:TextBox ID="txtSded2" runat="server" AutoPostBack="true" 
                                                                            ontextchanged="txtSded2_TextChanged" Text="0" Enabled="false"></asp:TextBox>
                                                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR18" runat="server" 
                                                                            FilterMode="ValidChars" FilterType="Numbers,Custom" TargetControlID="txtSded2" 
                                                                            ValidChars="0123456789.">
                                                                        </cc1:FilteredTextBoxExtender>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                                <asp:Label ID="lblSOT" runat="server" Text="OT Amount" Font-Bold="true" ></asp:Label>
                                                                            </td>
                                                                            <td>
                                                                            <asp:TextBox ID="txtSOT" runat="server" Text="0.00" Enabled="false" ></asp:TextBox>
                                                                            </td>
                                                                    <td>
                                                                        <asp:Label ID="Label7" runat="server" Font-Bold="true" Text="Deduction 3"></asp:Label>
                                                                    </td>
                                                                    <td colspan="1">
                                                                        <asp:TextBox ID="txtSded3" runat="server" AutoPostBack="true" 
                                                                            ontextchanged="txtSded3_TextChanged" Text="0"></asp:TextBox>
                                                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR19" runat="server" 
                                                                            FilterMode="ValidChars" FilterType="Numbers,Custom" TargetControlID="txtSded3" 
                                                                            ValidChars="0123456789.">
                                                                        </cc1:FilteredTextBoxExtender>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                                <asp:Label ID="Label16" runat="server" Text="Day Incentive" Font-Bold="true" ></asp:Label>
                                                                            </td>
                                                                            <td>
                                                                            <asp:TextBox ID="txtDayIncentive" runat="server" Text="0.00" Enabled="false" ></asp:TextBox>
                                                                            </td>
                                                                    <td>
                                                                        <asp:Label ID="Label8" runat="server" Font-Bold="true" Text="Deduction 4"></asp:Label>
                                                                    </td>
                                                                    <td colspan="1">
                                                                        <asp:TextBox ID="txtSded4" runat="server" AutoPostBack="true" 
                                                                            ontextchanged="txtSded4_TextChanged" Text="0"></asp:TextBox>
                                                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR20" runat="server" 
                                                                            FilterMode="ValidChars" FilterType="Numbers,Custom" TargetControlID="txtSded4" 
                                                                            ValidChars="0123456789.">
                                                                        </cc1:FilteredTextBoxExtender>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                                <asp:Label ID="Label17" runat="server" Text="Spinning Amount" Font-Bold="true" ></asp:Label>
                                                                            </td>
                                                                            <td>
                                                                            <asp:TextBox ID="txtSpinning" runat="server" Text="0.00" Enabled="false" ></asp:TextBox>
                                                                            </td>
                                                                    <td>
                                                                        <asp:Label ID="Label13" runat="server" Font-Bold="true" Text="Deduction 5"></asp:Label>
                                                                    </td>
                                                                    <td colspan="1">
                                                                        <asp:TextBox ID="txtSded5" runat="server" AutoPostBack="true" 
                                                                            ontextchanged="txtSded4_TextChanged" Text="0"></asp:TextBox>
                                                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR41" runat="server" 
                                                                            FilterMode="ValidChars" FilterType="Numbers,Custom" TargetControlID="txtSded5" 
                                                                            ValidChars="0123456789.">
                                                                        </cc1:FilteredTextBoxExtender>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                                <asp:Label ID="Label18" runat="server" Text="O/N Amount" Font-Bold="true" ></asp:Label>
                                                                            </td>
                                                                            <td>
                                                                            <asp:TextBox ID="txtHalf" runat="server" Text="0.00" Enabled="false" ></asp:TextBox>
                                                                            </td>
                                                                            <td>
                                                                            </td>
                                                                            <td colspan="1">
                                                                            </td>
                                                                </tr>
                                                                <tr>
                                                                <td>
                                                                                <asp:Label ID="Label19" runat="server" Text="F/N Amount" Font-Bold="true" ></asp:Label>
                                                                            </td>
                                                                            <td>
                                                                            <asp:TextBox ID="txtFull" runat="server" Text="0.00" Enabled="false" ></asp:TextBox>
                                                                            </td>
                                                                            <td>
                                                                            </td>
                                                                            <td colspan="1">
                                                                            </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                <asp:Label ID="Label20" runat="server" Text="ThreeSided Amount" Font-Bold="true" ></asp:Label>
                                                                            </td>
                                                                            <td>
                                                                            <asp:TextBox ID="txtThreeSided" runat="server" Text="0.00" Enabled="false" ></asp:TextBox>
                                                                            </td>
                                                                            <td>
                                                                            </td>
                                                                            <td colspan="1">
                                                                            </td>
                                                                            </tr>
                                                                </asp:Panel>
                                                                <asp:Panel ID="PanelWorker" runat="server" Visible="false">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="lblWbasic" runat="server" Font-Bold="true" Text="Basic Salary"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="txtWBasic" runat="server" Enabled="false" Text="0"></asp:TextBox>
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="lblWPF" runat="server" Font-Bold="true" Text="PF Amount"></asp:Label>
                                                                        </td>
                                                                        <td colspan="1">
                                                                            <asp:TextBox ID="txtWpf" runat="server" Enabled="false" Text="0"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="lblWOT" runat="server" Font-Bold="true" Text="OT Amount"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="txtWot" runat="server" Enabled="false" Text="0"></asp:TextBox>
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="lblWESI" runat="server" Font-Bold="true" Text="ESI Amount"></asp:Label>
                                                                        </td>
                                                                        <td colspan="1">
                                                                            <asp:TextBox ID="txtWesi" runat="server" Enabled="false" Text="0"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="lblWAll1" runat="server" Font-Bold="true" Text="Allowance 1"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="txtWAll1" runat="server" AutoPostBack="true" 
                                                                                ontextchanged="txtWAll1_TextChanged" Text="0"></asp:TextBox>
                                                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR21" runat="server" 
                                                                                FilterMode="ValidChars" FilterType="Numbers,Custom" TargetControlID="txtWAll1" 
                                                                                ValidChars="0123456789.">
                                                                            </cc1:FilteredTextBoxExtender>
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="Label9" runat="server" Font-Bold="true" Text="Advance Amount"></asp:Label>
                                                                        </td>
                                                                        <td colspan="1">
                                                                            <asp:TextBox ID="txtWadvance" runat="server" AutoPostBack="true" 
                                                                                Enabled="false" ontextchanged="txtWadvance_TextChanged" Text="0"></asp:TextBox>
                                                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR31" runat="server" 
                                                                                FilterMode="ValidChars" FilterType="Numbers,Custom" 
                                                                                TargetControlID="txtWadvance" ValidChars="0123456789.">
                                                                            </cc1:FilteredTextBoxExtender>
                                                                            <asp:Button ID="btnWadvance" runat="server" CssClass="button green" 
                                                                                onclick="btnWadvance_Click" Text="Edit" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="lblWall2" runat="server" Font-Bold="true" Text="Allowance 2"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="txtWall2" runat="server" AutoPostBack="true" 
                                                                                ontextchanged="txtWall2_TextChanged" Text="0"></asp:TextBox>
                                                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR22" runat="server" 
                                                                                FilterMode="ValidChars" FilterType="Numbers,Custom" TargetControlID="txtWall2" 
                                                                                ValidChars="0123456789.">
                                                                            </cc1:FilteredTextBoxExtender>
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="lblWUnion" runat="server" Font-Bold="true" Text="Union Charges" Visible="false"></asp:Label>
                                                                        </td>
                                                                        <td colspan="1">
                                                                            <asp:TextBox ID="txtWUnion" runat="server" AutoPostBack="true" 
                                                                                ontextchanged="txtWUnion_TextChanged" Text="0"  Visible="false"></asp:TextBox>
                                                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR23" runat="server" 
                                                                                FilterMode="ValidChars" FilterType="Numbers,Custom" TargetControlID="txtWUnion" 
                                                                                ValidChars="0123456789.">
                                                                            </cc1:FilteredTextBoxExtender>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="lblWAll3" runat="server" Font-Bold="true" Text="Allowance 3"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="txtWall3" runat="server" AutoPostBack="true" 
                                                                                ontextchanged="txtWall3_TextChanged" Text="0"></asp:TextBox>
                                                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR24" runat="server" 
                                                                                FilterMode="ValidChars" FilterType="Numbers,Custom" TargetControlID="txtWall3" 
                                                                                ValidChars="0123456789.">
                                                                            </cc1:FilteredTextBoxExtender>
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="lblWded1" runat="server" Font-Bold="true" Text="Deduction 1"></asp:Label>
                                                                        </td>
                                                                        <td colspan="1">
                                                                            <asp:TextBox ID="txtWded1" runat="server" AutoPostBack="true" 
                                                                                ontextchanged="txtWded1_TextChanged" Text="0"></asp:TextBox>
                                                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR25" runat="server" 
                                                                                FilterMode="ValidChars" FilterType="Numbers,Custom" TargetControlID="txtWded1" 
                                                                                ValidChars="0123456789.">
                                                                            </cc1:FilteredTextBoxExtender>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="lblWAll4" runat="server" Font-Bold="true" Text="Allowance 4"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="txtWall4" runat="server" AutoPostBack="true" 
                                                                                ontextchanged="txtWall4_TextChanged" Text="0"></asp:TextBox>
                                                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR26" runat="server" 
                                                                                FilterMode="ValidChars" FilterType="Numbers,Custom" TargetControlID="txtWall4" 
                                                                                ValidChars="0123456789.">
                                                                            </cc1:FilteredTextBoxExtender>
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="lblWded2" runat="server" Font-Bold="true" Text="Deduction 2"></asp:Label>
                                                                        </td>
                                                                        <td colspan="1">
                                                                            <asp:TextBox ID="txtWded2" runat="server" AutoPostBack="true" 
                                                                                ontextchanged="txtWded2_TextChanged" Text="0"></asp:TextBox>
                                                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR27" runat="server" 
                                                                                FilterMode="ValidChars" FilterType="Numbers,Custom" TargetControlID="txtWded2" 
                                                                                ValidChars="0123456789.">
                                                                            </cc1:FilteredTextBoxExtender>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                        </td>
                                                                        <td>
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="lblWded3" runat="server" Font-Bold="true" Text="Deduction 3"></asp:Label>
                                                                        </td>
                                                                        <td colspan="1">
                                                                            <asp:TextBox ID="txtWDed3" runat="server" AutoPostBack="true" 
                                                                                ontextchanged="txtWDed3_TextChanged" Text="0"></asp:TextBox>
                                                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR28" runat="server" 
                                                                                FilterMode="ValidChars" FilterType="Numbers,Custom" TargetControlID="txtDed3" 
                                                                                ValidChars="0123456789.">
                                                                            </cc1:FilteredTextBoxExtender>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                        </td>
                                                                        <td>
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="Label10" runat="server" Font-Bold="true" Text="Deduction 4"></asp:Label>
                                                                        </td>
                                                                        <td colspan="1">
                                                                            <asp:TextBox ID="txtWded4" runat="server" AutoPostBack="true" 
                                                                                ontextchanged="txtWded4_TextChanged" Text="0"></asp:TextBox>
                                                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR29" runat="server" 
                                                                                FilterMode="ValidChars" FilterType="Numbers,Custom" TargetControlID="txtWded4" 
                                                                                ValidChars="0123456789.">
                                                                            </cc1:FilteredTextBoxExtender>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                        </td>
                                                                        <td>
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="Label12" runat="server" Font-Bold="true" Text="Stamp Charge"></asp:Label>
                                                                        </td>
                                                                        <td colspan="1">
                                                                            <asp:TextBox ID="txtWstamp" runat="server" AutoPostBack="true" 
                                                                                 Text="0" ontextchanged="txtWstamp_TextChanged" Enabled="true"></asp:TextBox>
                                                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR36" runat="server" 
                                                                                FilterMode="ValidChars" FilterType="Numbers,Custom" TargetControlID="txtWstamp" 
                                                                                ValidChars="0123456789.">
                                                                            </cc1:FilteredTextBoxExtender>
                                                                        </td>
                                                                    </tr>
                                                                </asp:Panel>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Button ID="btnCalculate" runat="server" CssClass="button green" 
                                                                            onclick="btnCalculate_Click" Text="Calculate" />
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="Earnings" runat="server" Font-Bold="true" 
                                                                            Text="Gross Earnings RS. "></asp:Label>
                                                                        <asp:Label ID="EarningsAmt" runat="server" Font-Bold="true" Text="0"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="Deductions" runat="server" Font-Bold="true" 
                                                                            Text="Gross Deductions RS. "></asp:Label>
                                                                        <asp:Label ID="DeductionAmt" runat="server" Font-Bold="true" Text="0"></asp:Label>
                                                                    </td>
                                                                    <td colspan="1">
                                                                    </td>
                                                                </tr>
                                                                <tr align="center">
                                                                    <td colspan="4">
                                                                        <asp:Label ID="lblNetpay" runat="server" Font-Bold="true" Text="Net Pay RS. "></asp:Label>
                                                                        <asp:Label ID="lblNetPayAmt" runat="server" Font-Bold="true" Text="0"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr align="center">
                                                                    <td colspan="4">
                                                                        <asp:Label ID="lbltext" runat="server" Font-Bold="true" Font-Size="14px" 
                                                                            Text="Zero Only"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="center" colspan="4">
                                                                    <asp:Button ID="btngroup" runat="server" Text="Group Salary" 
                                                                            CssClass="button green" Width="120" Height="25" onclick="btngroup_Click" Visible= "true" />
                                                                        <asp:Button ID="btnsave" runat="server" CssClass="button green" 
                                                                            Font-Bold="true" Height="25" onclick="btnsave_Click" Text="Save" Width="78" />
                                                                        <asp:Button ID="btnreset" runat="server" CssClass="button green" 
                                                                            Font-Bold="true" Height="25" onclick="btnreset_Click" Text="Reset" Width="78" />
                                                                        <asp:Button ID="btnpayslip" runat="server" CssClass="button green" 
                                                                            Font-Bold="true" Height="25" onclick="btnpayslip_Click" Text="Payslip" 
                                                                            Width="78" Visible="false" />
                                                                        <asp:Button ID="btncancel" runat="server" CssClass="button green" 
                                                                            Font-Bold="true" Height="25" onclick="btncancel_Click" Text="Cancel" 
                                                                            Width="78" />
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>                
            </div>
            <!-- end page-wrap -->
            <div id="footer">
                <ul>
                    <li>Copyright &copy; 2012. All rights reserved.</li>
                    <li>Powered by <a href="http://www.altius.co.in" target="_blank">Altius Infosystems</a></li>
                </ul>
            </div>
            <!-- end footer -->
        </form>
    </body>