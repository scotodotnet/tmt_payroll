﻿ using System;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using System.Security.Cryptography;
using System.Collections;
using System.Collections.Specialized;
using System.Text;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class _Default : System.Web.UI.Page 
{
    protected System.Resources.ResourceManager rm;
    protected string PayrollRegisterContentMeta, pageTitle;
    System.Globalization.CultureInfo culterInfo = new System.Globalization.CultureInfo("en-GB");
    BALDataAccess objdata = new BALDataAccess();
    protected void Page_Load(object sender, EventArgs e)
    {
        Session.Remove("UserId");
        if (!IsPostBack)
        {
            Load_company();
        }
    }
    public void Load_company()
    {
        DataTable dt = new DataTable();
        dt = objdata.Dropdown_Company();
        ddlCode.DataSource = dt;
        DataRow dr = dt.NewRow();
        dr["Ccode"] = "Company Name";
        dr["Cname"] = "Company Name";
        dt.Rows.InsertAt(dr, 0);
        ddlCode.DataTextField = "Cname";
        ddlCode.DataValueField = "Ccode";
        ddlCode.DataBind();
    }
    public void Load_Location()
    {

    }
    protected void btnsave_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        string Verification = objdata.Verification_verify();
        UserRegistrationClass objuser = new UserRegistrationClass();
        if (txtusername.Text.Trim() == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Enter the User Name ');", true);
            ErrFlag = true;
        }
        else if (txtpassword.Text.Trim() == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Enter the Password ');", true);
            ErrFlag = true;
        }

        if (!ErrFlag)
        {
            DataTable dt_v = new DataTable();
            if (txtusername.Text.Trim() == "Altius")
            {
                Session["UserId"] = txtusername.Text.Trim();
                objuser.UserCode = txtusername.Text.Trim();
                objuser.Password = s_hex_md5(txtpassword.Text.Trim());
                string pwd = s_hex_md5(txtpassword.Text);
                DataTable dt = new DataTable();
                dt = objdata.AltiusLogin(objuser);
                if (dt.Rows.Count > 0)
                {

                    if ((dt.Rows[0]["UserCode"].ToString().Trim() == txtusername.Text.Trim()) && (dt.Rows[0]["Password"].ToString().Trim() == pwd))
                    {
                        Session["Isadmin"] = dt.Rows[0]["IsAdmin"].ToString().Trim();
                        Session["Usernmdisplay"] = txtusername.Text.Trim();
                        Session["Ccode"] = ddlCode.SelectedValue;
                        Session["Lcode"] = ddlLocation.SelectedValue;
                        if (Session["Isadmin"].ToString() == "1")
                        {
                            Session["RoleCode"] = "1";
                        }
                        else
                        {
                            Session["RoleCode"] = "2";
                        }
                        Response.Redirect("Administrator.aspx");
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('User Name and password Incorrect ');", true);
                    }
                }
            }
            else
            {
                string date_1 = "";
                dt_v = objdata.Value_Verify();
                if (dt_v.Rows.Count > 0)
                {
                    date_1 = (22 + "-" + 06 + "-" + 2021).ToString();
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Please Update your Licence Key');", true);
                    return;
                }
                if (Verification.Trim() != "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Please Update your Licence Key');", true);
                    return;
                }
                string dtserver = objdata.ServerDate();
                if (Convert.ToDateTime(dtserver) < Convert.ToDateTime(date_1))
                {
                    if (txtusername.Text.Trim() == "")
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Enter the User Name ');", true);
                        ErrFlag = true;
                    }
                    else if (txtpassword.Text.Trim() == "")
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Enter the Password ');", true);
                        ErrFlag = true;
                    }
                    else if (ddlLocation.SelectedValue == "")
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select the Location ');", true);
                        ErrFlag = true;
                    }
                    if (!ErrFlag)
                    {
                        Session["UserId"] = txtusername.Text.Trim();
                        objuser.UserCode = txtusername.Text.Trim();
                        objuser.Password = s_hex_md5(txtpassword.Text.Trim());
                        string pwd = s_hex_md5(txtpassword.Text);
                        objuser.Ccode = ddlCode.SelectedValue;
                        objuser.Lcode = ddlLocation.SelectedValue;
                        DataTable dt = new DataTable();
                        dt = objdata.UserLogin(objuser);
                        if (dt.Rows.Count > 0)
                        {
                            if ((dt.Rows[0]["UserCode"].ToString().Trim() == txtusername.Text.Trim()) && (dt.Rows[0]["Password"].ToString().Trim() == pwd) && (dt.Rows[0]["CompCode"].ToString().Trim() == ddlCode.SelectedValue) && (dt.Rows[0]["LocationCode"].ToString().Trim() == ddlLocation.SelectedValue))
                            {
                                Session["Isadmin"] = dt.Rows[0]["IsAdmin"].ToString().Trim();
                                Session["Usernmdisplay"] = txtusername.Text.Trim();
                                Session["Ccode"] = dt.Rows[0]["CompCode"].ToString().Trim();
                                Session["Lcode"] = dt.Rows[0]["LocationCode"].ToString().Trim();
                                if (Session["Isadmin"].ToString() == "1")
                                {
                                    Session["RoleCode"] = "1";
                                }
                                else
                                {
                                    Session["RoleCode"] = "2";
                                }
                                Response.Redirect("Dashboard.aspx");
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('User Name and password Incorrect ');", true);
                        }
                    }
                }
                else
                {
                    //objdata.Insert_verificationValue();
                    //ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Please Update your Licence Key');", true);
                }

                //Session["UserId"] = txtusername.Text;
                //objuser.UserCode = txtusername.Text;
                //objuser.Password = s_hex_md5(txtpassword.Text);
                //objuser.Ccode = ddlCode.SelectedValue.Trim();
                //objuser.Lcode = ddlLocation.SelectedValue.Trim();
                //string pwd = s_hex_md5(txtpassword.Text);
                //string UserName = objdata.username(objuser);
                //string password = objdata.UserPassword(objuser);

                //if (UserName == txtusername.Text && password == pwd)
                //{

                //    string UserNameDis = objdata.usernameDisplay(objuser);
                //    string isadmin = objdata.isAdmin(txtusername.Text);
                //    string see = isadmin;
                //    int dd = Convert.ToInt32(see);
                //    Session["Isadmin"] = Convert.ToString(dd);
                //    Session["Usernmdisplay"] = UserNameDis;
                //    if (see.Trim() == "3")
                //    {
                //        Response.Redirect("Administrator.aspx");
                //    }
                //    else
                //    {
                //        if (UserName == "Admin")
                //        {
                //            Session["RoleCode"] = "1";
                //        }
                //        else
                //        {
                //            Session["RoleCode"] = "2";
                //        }
                //        Response.Redirect("EmployeeRegistration.aspx");
                //    }
                //}
                //else
                //{
                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('User Name and password Incorrect ');", true);
                //}
            }
        }

        //if (isadmin = "1")
        //{
 
        //}



    }

    public static String s_hex_md5(String originalPassword)
    {
        UTF8Encoding encoder = new UTF8Encoding();
        MD5 md5 = new MD5CryptoServiceProvider();

        Byte[] hashedbytes = md5.ComputeHash(encoder.GetBytes(originalPassword));
        return BitConverter.ToString(hashedbytes).Replace("-", "").ToLower();
    }
    protected void ddlLocation_SelectedIndexChanged(object sender, EventArgs e)
    {
        
       
    }
    protected void ddlCode_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable dtempty = new DataTable();
        ddlLocation.DataSource = dtempty;
        ddlLocation.DataBind();
        DataTable dt = new DataTable();
        dt = objdata.dropdown_loc(ddlCode.SelectedValue);
        ddlLocation.DataSource = dt;
        //DataRow dr = dt.NewRow();
        //dr["LCode"] = "Location";
        //dr["Location"] = "Location";
        //dt.Rows.InsertAt(dr, 0);
        ddlLocation.DataTextField = "Location";
        ddlLocation.DataValueField = "LCode";
        ddlLocation.DataBind();
    }
}
