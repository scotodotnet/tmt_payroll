﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SalaryMaster_1.aspx.cs" Inherits="SalaryMaster_1" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
    <head id="Head1" runat="server">
        <title>Payroll Management Systems</title>
        <link href="new_css/main.css" rel="stylesheet" type="text/css" />
        <link href="new_css/colors/dark_blue.css" rel="stylesheet" type="text/css" />
        <link href="new_css/superfish.css" rel="stylesheet" type="text/css" />

        <script src="new_js/jquery-1.4.2.min.js" type="text/javascript"></script>
        <script src="new_js/jquery.hoverintent.minified.js" type="text/javascript"></script>
        <script src="new_js/superfish.js" type="text/javascript"></script>
        <script src="new_js/jquery.tools.min.js" type="text/javascript"></script>
        <%--<script src="new_js/jquery.simplemodal.js" type="text/javascript"></script>--%>
        <script src="new_js/jquery.notifybar.js" type="text/javascript"></script>
        <script src="new_js/jquery.tipsy.js" type="text/javascript"></script>
        <script src="new_js/enhance.js" type="text/javascript"></script>
        <script src="new_js/excanvas.js" type="text/javascript"></script>
        <script src="new_js/visualize.jquery.js" type="text/javascript"></script>
        <script src="new_js/jquery.collapsible.js" type="text/javascript"></script>
        <script src="new_js/jquery.autosuggest.packed.js" type="text/javascript"></script>
        <script src="new_js/platinum-admin.js" type="text/javascript"></script>
        <script language="javascript" type="text/javascript">
            function SelectSingleRadiobutton(rbtnstatus)
             {
                var rdBtn = document.getElementById(rbtnstatus);
                var rdBtnList = document.getElementsByTagName("input");
                   for (i = 0; i < rdBtnList.length; i++) 
                   {
                        if (rdBtnList[i].type == "radio" && rdBtnList[i].id != rdBtn.id)
                        {
                        rdBtnList[i].checked = false;
                        }
                  }
            }
  </script>
  <script type ="text/jscript" >
        function GoBack() {
            window.history.forward();
        }
   </script>
        <style>
            .sf-navbar li ul 
            {
	            width: 1100px; /*IE6 soils itself without this*/
	            margin: 0px auto;
	            margin-left:-20em;	            
            }
        </style>
    </head>
    <body onload="GoBack();">
        <form id="form1" runat="server">
            <cc1:ToolkitScriptManager runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true"
                ID="ScriptManager1" EnablePartialRendering="true">
            </cc1:ToolkitScriptManager>
            <div id="header">
                <div id="header-top">
                    <div id="logo">
	                    <h1>&nbsp;</h1>
	                    <span id="slogan">&nbsp;</span> 
	                </div>
                    <!-- end logo -->
                    <div id="login-info">
                        <img id="profile" alt="profile" src="./new_images/icons/user.png" />
                        <p id="top"><span id="name"><asp:Label ID="lblusername" runat="server"></asp:Label></span><br />
                        <span id="links">
                        <%--<a id="message-link" href="#" title="click to see the messages!">
                        <span id="message-count">1</span> new message. </a>--%></span></p>
                        <div>
                            <asp:Label ID="lblComany" runat="server" Text="" Font-Bold="true"></asp:Label>
                        </div>

	                    <div id="messages-box">
		                    <h4>Messages</h4>
	                        <h5><a id="new-message" href="#">New Message</a></h5>
	                        <hr />
	                        <ul id="messages">
		                        <li class="new"><a href="#">
		                            <img alt="user" src="./new_images/icons/sender.png" />
		                            <span class="sender">John Doe</span>
		                            <span class="description">Hi this is just a description...</span>
		                            <span class="date">20 minutes ago...</span> </a>
		                        </li>
		                        <li><a href="#">
		                            <img alt="user" src="./new_images/icons/sender.png" />
		                            <span class="sender">John Doe</span>
		                            <span class="description">Hi this is just a description...</span>
		                            <span class="date">24 minutes ago...</span> </a>
		                        </li>
		                        <li><a href="#">
		                            <img alt="user" src="./new_images/icons/sender.png" />
		                            <span class="sender">John Doe</span>
		                            <span class="description">Hi this is just a description...</span>
		                            <span class="date">50 minutes ago...</span> </a>
		                        </li>
		                        <li id="last"><a id="all-messages" href="#">See all messages!
		                            <span id="unreaded">8 unreaded.</span> </a>
		                        </li>
		                    </ul>
		                    <br class="clear" />
	                    </div><!-- end messages-box -->
	                    <a id="power" href="default.aspx" title="Logout">logout</a>
                    </div>
                    <!-- end login -->
                    <div id="nav">
	                    <ul class="sf-menu sf-navbar">
	                        <li><a id="dashboard" href="Dashboard.aspx">Dashboard</a>
	                            <ul>
		                            <li>&nbsp;
		                            </li>
	                            </ul>
	                        </li>
	                        <li class="current"><a id="Employee" href="EmployeeRegistration.aspx">Employee</a>
	                            <ul>
		                            <li><a href="EmployeeRegistration.aspx">Employee Registration</a> </li>					                    
		                            <li><a href="OfficalProfileDetails.aspx">Official Profile</a></li>
		                            <li class="current"><a href="SalaryMaster_1.aspx">Salary Master</a></li>
		                            <li><a href="LeaveAllocationEmployee.aspx">Leave Allocation</a></li>
		                            <li><a href="ApplyForLeave.aspx">Apply Leave</a></li>						                    
		                            <li><a href="SalaryRegistration.aspx">Salary Details</a></li>
		                            <li><a href="SalaryAdvance.aspx">Salary Advance</a></li>
		                             <li><a href="Settlement.aspx">Settlement</a></li>
		                            <li><a href="EmployeeDeactiveMode.aspx">Employee Re-Activate</a></li>
		                             				                    
	                            </ul>
	                        </li>
	                        <li><a id="Bonus" href="BonusForAll.aspx">Bonus</a>
	                            <ul>
		                            <li><a href="BonusForAll.aspx">Bonus</a></li>
		                            <li><a href="StandardBonus.aspx">Group Bonus</a></li>					                    
	                            </ul>
	                        </li>
	                        <li><a id="ESI_PF" href="PFForm5.aspx">ESI & PF</a>
	                            <ul>
		                            <li><a href="PFForm5.aspx">PF Form 5</a></li>
		                            <li><a href="FOrm10.aspx">Form 10</a></li>
		                            <li><a href="PFForm3Anew.aspx">PF Form 3A</a></li>
		                            <li><a href="PFform6A.aspx">PF Form 6A</a></li>
		                            <li><a href="ESIForm7.aspx">ESI Form 7</a></li>
	                            </ul>
	                        </li>
	                        <li><a id="Master" href="MstEmployeeType.aspx">Masters</a>
	                            <ul>
		                            <li><a href="MstBank.aspx">Bank Master</a></li>
		                            <li><a href="MstDepartment.aspx">Department Master</a></li>
		                            <li><a href="MstEmployeeType.aspx">Employee Type Master</a></li>
		                            <li><a href="MstInsuranceCompany.aspx">Insurance Master</a></li>
		                            <li><a href="MstLeave.aspx">Leave Master</a></li>
		                            <li><a href="MstProbationPeriod.aspx">Probationary Period Master</a></li>
		                             <li><a href="MstQualification.aspx">Qualification Master</a></li>
		                              <li><a href="UserRegistration.aspx">User Master</a></li>
	                            </ul>
	                        </li>
	                         <li><a id="Reports" href="RptDepartmentwiseEmployee.aspx">Reports</a>
	                            <ul>
		                            <li><a href="ContractBreak.aspx">Contract Details</a></li>
		                            <li><a href="RptAttendanceReport.aspx">Attendance Report</a></li>
		                            <li><a href="RptDepartmentwiseEmployee.aspx">Employee Details</a></li>
		                            <li><a href="RptProbationperiod.aspx">Probationary Period</a></li>
		                            <li><a href="RptResign.aspx">Resignation Reports</a></li>
		                            <li><a href="PaySlipGen.aspx">Payslip</a></li>
		                            <%--<li><a href="RptSalary.aspx">Salary Details</a></li>--%>
	                            </ul>
	                        </li>
	                        <li><a id="Upload" href="AttenanceUpload.aspx">Upload</a>
	                            <ul>
		                            <li><a href="AttenanceUpload.aspx">Attenance Upload</a></li>
		                            <li><a href="UploadEmployee.aspx">Employee Upload</a></li>	
	                            </ul>
	                        </li>
	                        <li><a id="OT" href="OverTime.aspx">OT</a>
	                            <ul>
	                                <li><a href="OverTime.aspx">OT</a></li>
	                            </ul>
	                        </li>
                        </ul>
                    </div>
                    <!-- end div#nav -->
                </div>
                <!-- end div#header-top -->
                <div class="breadCrumb">
                    <ul>
                        <li class="first"><a href="#">Home</a> </li>
                        <li><a href="#">Users</a> </li>
                        <li><a href="#">Roles</a> </li>
                        <li class="last">Salary Master</li>
                    </ul>
                </div>
                <!-- end div#breadCrumb -->
                <%--<div id="search-box">
                    <form id="searchform" action="" method="get">
                        <fieldset class="search"><span>Search</span>
                        <input class="box" type="text" />
                        <button class="btn" title="Submit Search">Search</button></fieldset>
                    </form>
                </div>--%>
                <!-- end div#search-box -->
            </div>
            <!-- end header -->
            <div id="page-wrap">
                <div id="right-sidebar">
                    <div class="innerdiv">
	                    <h2 class="head">Comments</h2>
	                    <div class="innercontent clear">
		                    <h4>Recent Comments</h4>
		                    <ul id="comments" class="tooltip-enabled">
		                        <li><a href="#" title="this comment is from John Doe">
		                            <img alt="user" src="./new_images/icons/sender.png" />
		                            <span class="sender">John Doe</span>
		                            <span class="description">Hey i liked your theme!</span>
		                            <span class="date">20 minutes ago...</span> </a>
		                            <span class="floatright"><a title="Aprrove this comment" href="#">Approve</a> | 
		                            <a title="Delete this comment"  href="#">Delete</a>
		                            </span>
		                            <br class="clear" />
		                        </li>
		                        <li><a href="#" title="again this guy!">
		                            <img alt="user" src="./new_images/icons/sender.png" />
		                            <span class="sender">John Doe</span>
		                            <span class="description">Hi, i need some help.</span>
		                            <span class="date">24 minutes ago...</span> </a>
		                            <span class="floatright"><a title="Aprrove this comment"  href="#">Approve</a> | 
		                            <a title="Delete this comment"  href="#">Delete</a>
		                            </span>
		                            <br class="clear" />
		                        </li>
		                        <li><a href="#" title="is he a spammer?">
		                            <img alt="user" src="./new_images/icons/sender.png" />
		                            <span class="sender">John Doe</span>
		                            <span class="description">Thank you for your help.</span>
		                            <span class="date">50 minutes ago...</span> </a>
		                            <span class="floatright"><a title="Aprrove this comment"  href="#">Approve</a> | 
		                            <a title="Delete this comment"  href="#">Delete</a>
		                            </span>
		                            <br class="clear" />
		                        </li>
		                        <li class="last"><a class="all-messages" title="See all comments awaiting confirmation." href="#">See all comments!
		                            <span class="unreaded">8 awaiting confirm.</span> </a>
		                        </li>
		                    </ul>
	                    </div>
                    </div>
                    <div class="clear"></div>                    
                </div>
	            <!-- end right-sidebar -->    
	                <asp:UpdatePanel runat="server">
	                    <ContentTemplate>
	                        <div id="main-content">
                                <br /><br /><br /><br /> 
	                            <div class="clear_body">
                                    <div class="innerdiv">
                                        <div class="innercontent">
                                            <!-- tab "panes" -->				                
                                                <div>
                                                    <table class="full">
                                                        <thead>
                                                            <tr align="center">
                                                                <th colspan="4" style="text-align:center;"><h4>SALARY MASTER</h4></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        <asp:Panel ID="panelError" runat="server" Visible="false">
			                                                <tr align="center" >
                                                                
                                                                <td colspan="4" align="center">
                                                                    <asp:Label ID="lblError" runat="server" Font-Bold="true" Font-Size="16px" ForeColor="Red" ></asp:Label>
                                                                </td>
                                                                    
                                                            </tr>	
                                                        </asp:Panel>
                                                            <tr>
                                                                <td><asp:Label ID="label1" runat="server" Text="Category" Font-Bold="true" ></asp:Label></td>
                                                                <td><asp:DropDownList ID="ddlcategory" runat="server" Width="100" Height="25" 
                                                                        AutoPostBack="True" onselectedindexchanged="ddlcategory_SelectedIndexChanged"></asp:DropDownList></td>
                                                                
                                                               
                                                                <td><asp:Label ID="label2" runat="server" Text="Department" Font-Bold="true" ></asp:Label></td>
                                                                <td colspan="1"><asp:DropDownList ID="ddlDepartment" runat="server" Width="100" Height="25" TabIndex="2" 
                                                                onselectedindexchanged="ddlDepartment_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                                                <asp:Button ID="btnclick" runat="server" Text="Click" Width="78" Height="28" onclick="btnclick_Click" CssClass="button green" /></td>   
                                                            </tr>
                                                            <asp:Panel ID="panelsuccess" runat="server" Visible="false" BorderColor="Black">
		                                        		    <tr style="background-color:Olive">
		                                        		        <td align="left" colspan="4">
		                                        		            <asp:Label ID="lblhd" runat="server" Text="Altius InfoSystems" ForeColor="White" Font-Bold="true" Font-Size="14px" ></asp:Label>
		                                        		        </td>
		                                        		    </tr>
		                                        		    <tr align="center">
		                                        		        <td colspan="4">
		                                        		            <asp:Label ID="lbloutput" runat="server" Font-Bold="true" Font-Size="16px"></asp:Label>
		                                        		           
		                                        		        </td>
		                                        		    </tr>
		                                        		    <tr align="center">
		                                        		        <td colspan="4">
		                                        		            <asp:Button ID="btnok" runat="server" Text="Ok" onclick="btnok_Click" Width="75" Height="28" CssClass="button green" />
		                                        		        </td>
		                                        		    </tr>
		                                        		</asp:Panel>		            
                                                            <tr>
                                                                <td><asp:Label ID="lblFinance" runat="server" Text="Financial Year" Font-Bold="true" ></asp:Label></td>
                                                                <td colspan="3"><asp:DropDownList ID="ddFinance" runat="server" AutoPostBack="true" 
                                                                        Width="150" Height="25" 
                                                                        onselectedindexchanged="ddFinance_SelectedIndexChanged" ></asp:DropDownList></td>
                                                            </tr>
                                                            <tr id="PanelExistNo" runat="server" visible="false">
                                                                <td><asp:Label ID="lblexisitingno" runat="server" Text="Existing Number" Font-Bold="true"></asp:Label></td>
                                                                <td><asp:TextBox ID="txtexistingNo" runat="server" ></asp:TextBox>
                                                                 <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender_ExisistingCode" runat="server"
                                                                 TargetControlID="txtexistingNo" FilterMode="ValidChars" FilterType="UppercaseLetters,LowercaseLetters,Numbers,Custom"                                                           ValidChars="0123456789+- ">
                                                                 </cc1:FilteredTextBoxExtender></td>
                                                                 <td colspan="2"><asp:Button ID="btnexitingSearch" runat="server" Text="Search" Width="78" 
                                                                 Height="28" onclick="btnexitingSearch_Click" CssClass="button green" /></td>
                                                            </tr>
                                                            <tr id="PanelEmp" runat="server" visible="false">
                                                                <td><asp:Label ID="label3" runat="server" Text="Employee No" Font-Bold="true" ></asp:Label></td>
                                                                <td><asp:DropDownList ID="ddlempNo" runat="server" AutoPostBack="true" Width="150" 
                                                                        Height="25" TabIndex="3" 
                                                                        onselectedindexchanged="ddlempNo_SelectedIndexChanged" ></asp:DropDownList></td>
                                                                <td><asp:Label ID="label4" runat="server" Text="Employee Name" Font-Bold="true" ></asp:Label></td>
                                                                <td colspan="1"><asp:DropDownList ID="ddlEmpName" runat="server" 
                                                                        AutoPostBack="true" Width="150" Height="25" TabIndex="4" 
                                                                        onselectedindexchanged="ddlEmpName_SelectedIndexChanged" ></asp:DropDownList></td>
                                                            </tr>
                                                            <tr>
                                                                <td><asp:Label ID="label7" runat="server" Text="Profile Type" Font-Bold="true" ></asp:Label></td>
                                                                 <td colspan="3"><asp:Label ID="Profilelbl" runat="server" Font-Bold="true"></asp:Label></td>
                                                            </tr>
                                                            <tr id="PanelDailyLabour" runat="server" visible="false">
                                                                <td><asp:Label ID="lblLabourSalary" runat="server" Text="Daily Salary" Font-Bold="true" ></asp:Label></td>
                                                                <td colspan="3"><asp:TextBox ID="txtDailySalary" runat="server" 
                                                                        ontextchanged="txtDailySalary_TextChanged" ></asp:TextBox>
                                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR9" runat="server" FilterMode="ValidChars"
                                                                FilterType="Numbers,Custom" TargetControlID="txtDailySalary" ValidChars="."></cc1:FilteredTextBoxExtender></td>
                                                                
                                                            </tr>
                                                            <tr id="PanelWagesLabour" runat="server" visible="false">
                                                                <td><asp:Label ID="lblWages" runat="server" Text="Wages Type" Font-Bold="true" ></asp:Label>
                                                                    </td>
                                                                <td colspan="3">
                                                                <asp:RadioButtonList ID="rbtnWages" runat="server" 
                                                                AutoPostBack="true" RepeatColumns="3" 
                                                                onselectedindexchanged="rbtnWages_SelectedIndexChanged" Enabled="false" >
                                                                <asp:ListItem Text="Daily Wages" Value="1" ></asp:ListItem>
                                                                <asp:ListItem Text="Monthly Wages" Value="2" ></asp:ListItem>
                                                                <asp:ListItem Text="Weekly Wages" Value="3" ></asp:ListItem>
                                                                </asp:RadioButtonList></td>
                                                            </tr>
                                                            <tr id="panelStaff1" runat="server" visible="false">
                                                                <td></td>
                                                                
                                                                <td ><asp:Label ID="label6" runat="server" Text="Basic Salary" Font-Bold="true" ></asp:Label></td>
                                                                <td><asp:TextBox ID="txtBasic" runat="server" TabIndex="5" Text="0" ></asp:TextBox>
                                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR4" runat="server" FilterMode="ValidChars"
                                                                FilterType="Numbers,Custom" TargetControlID="txtBasic" ValidChars="."></cc1:FilteredTextBoxExtender>
                                                                </td>
                                                                <td></td>
                                                            </tr>
                                                            <tr id="Panelstaff10" runat="server" visible="false">
                                                                <td><asp:Label ID="label8" runat="server" Text="HRA %" Font-Bold="true" ></asp:Label></td>
                                                                <td><asp:TextBox ID="txthrapercentage" runat="server" TabIndex="6" Text="0" ></asp:TextBox>
                                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR1" runat="server" FilterMode="ValidChars"
                                                                FilterType="Numbers,Custom" TargetControlID="txthrapercentage" ValidChars="."></cc1:FilteredTextBoxExtender>
                                                                </td>
                                                                <td><asp:Label ID="label9" runat="server" Text="HRA Amount" Font-Bold="true" ></asp:Label></td>
                                                                <td colspan="1" ><asp:TextBox ID="txtHRAamt" runat="server" Enabled="false" Text="0" ></asp:TextBox></td>
                                                            </tr>
                                                            <tr id="Panelsaff2" runat="server" visible="false">
                                                                <td><asp:Label ID="label10" runat="server" Text="Conveyance %" Font-Bold="true" ></asp:Label></td>
                                                                <td><asp:TextBox ID="txtconveyanceper" runat="server" TabIndex="7" Text="0" ></asp:TextBox>
                                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR2" runat="server" FilterMode="ValidChars"
                                                                FilterType="Numbers,Custom" TargetControlID="txtconveyanceper" ValidChars="."></cc1:FilteredTextBoxExtender>
                                                                </td>
                                                                <td><asp:Label ID="label11" runat="server" Text="Conveyance Amount" Font-Bold="true" ></asp:Label></td>
                                                                <td colspan="1"><asp:TextBox ID="txtconveyanceAmt" runat="server" Enabled="false" Text="0" ></asp:TextBox></td>
                                                            </tr>
                                                            <tr id="panelstaff4" runat="server" visible="false">
                                                                <td><asp:Label ID="label14" runat="server" Text="Medical Reimburesement %" Font-Bold="true" ></asp:Label></td>
                                                                <td><asp:TextBox ID="txtmedicalPercentage" runat="server" TabIndex="9" Text="0"></asp:TextBox>
                                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR5" runat="server" FilterMode="ValidChars"
                                                                FilterType="Numbers,Custom" TargetControlID="txtmedicalPercentage" ValidChars="."></cc1:FilteredTextBoxExtender>
                                                                </td>
                                                                <td><asp:Label ID="label15" runat="server" Text="Medical Reimburesement" Font-Bold="true" ></asp:Label></td>
                                                                <td colspan="1"><asp:TextBox ID="txtmedicalAmt" runat="server" Enabled="false" Text="0"></asp:TextBox></td>
                                                            </tr>
                                                            <tr id="Panelstaff5" runat="server" visible="false">
                                                                <td><asp:Label ID="label16" runat="server" Text="Allowance1 %" Font-Bold="true" ></asp:Label></td>
                                                                <td><asp:TextBox ID="txtall1percentage" runat="server" TabIndex="10" Text="0" ></asp:TextBox>
                                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR6" runat="server" FilterMode="ValidChars"
                                                                FilterType="Numbers,Custom" TargetControlID="txtall1percentage" ValidChars="."></cc1:FilteredTextBoxExtender>
                                                                </td>
                                                                <td><asp:Label ID="label17" runat="server" Text="Allowance 1" Font-Bold="true" ></asp:Label></td>
                                                                <td colspan="1"><asp:TextBox ID="txtallowance1Amt" runat="server" Enabled="false" Text="0" ></asp:TextBox> </td>
                                                            </tr>
                                                            <tr id="panelstaff6" runat="server" visible="false">
                                                                <td><asp:Label ID="label18" runat="server" Font-Bold="true" Text="Allowance2 %"></asp:Label></td>
                                                                <td><asp:TextBox ID="txtAllowance2percentage" runat="server" TabIndex="11" Text="0"></asp:TextBox>
                                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR7" runat="server" FilterMode="ValidChars"
                                                                FilterType="Numbers,Custom" TargetControlID="txtAllowance2percentage" ValidChars="."></cc1:FilteredTextBoxExtender>
                                                                </td>
                                                                <td><asp:Label ID="label19" runat="server" Font-Bold="true" Text="Allowance 2"></asp:Label></td>
                                                                <td colspan="1"><asp:TextBox ID="txtAllowavce2Amt" runat="server" Enabled="false" Text="0"></asp:TextBox></td>
                                                            </tr>
                                                            <tr id="Panelstaff7" runat="server" visible="false">
                                                                <td><asp:Label ID="Label20" runat="server" Font-Bold="true" Text="Allowance3 %"></asp:Label>
                                                                    </td>
                                                                <td><asp:TextBox ID="txtAllowance3percentage" runat="server" TabIndex="12" Text="0"></asp:TextBox>
                                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR8" runat="server" FilterMode="ValidChars"
                                                                FilterType="Numbers,Custom" TargetControlID="txtAllowance3percentage" ValidChars="."></cc1:FilteredTextBoxExtender>
                                                                </td>
                                                                <td><asp:Label ID="label21" runat="server" Font-Bold="true" Text="Allowance 3"></asp:Label></td>
                                                                <td colspan="1"><asp:TextBox ID="txtAllowance3Amt" runat="server" Enabled="false" Text="0"></asp:TextBox></td>
                                                            </tr>
                                                             <tr id="PanelStaff18" runat="server" visible="false">
                                                                <td><asp:Label ID="Label24" runat="server" Font-Bold="true" Text="Deduction 1 %"></asp:Label></td>
                                                                <td><asp:TextBox ID="txtdeduction1percentage" runat="server" TabIndex="12" Text="0"></asp:TextBox>
                                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR18" runat="server" FilterMode="ValidChars"
                                                                FilterType="Numbers,Custom" TargetControlID="txtdeduction1percentage" ValidChars="."></cc1:FilteredTextBoxExtender>
                                                                </td>
                                                                <td><asp:Label ID="label25" runat="server" Font-Bold="true" Text="Deduction 1"></asp:Label></td>
                                                                <td colspan="1"><asp:TextBox ID="txtdeductionAmt1" runat="server" Enabled="false" Text="0"></asp:TextBox></td>
                                                            </tr>
                                                            <tr id="PanelStaff9" runat="server" visible="false">
                                                                <td><asp:Label ID="Label26" runat="server" Font-Bold="true" Text="Deduction 2 %"></asp:Label></td>
                                                                <td><asp:TextBox ID="txtdeduction2percentage" runat="server" TabIndex="12" Text="0"></asp:TextBox>
                                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR19" runat="server" FilterMode="ValidChars"
                                                                FilterType="Numbers,Custom" TargetControlID="txtdeduction2percentage" ValidChars="."></cc1:FilteredTextBoxExtender>
                                                                </td>
                                                                <td><asp:Label ID="label27" runat="server" Font-Bold="true" Text="Deduction 2"></asp:Label></td>
                                                                <td colspan="1"><asp:TextBox ID="txtdeductionAmt2" runat="server" Enabled="false" Text="0"></asp:TextBox></td>
                                                            </tr>
                                                            <tr id="PanelStaff110" runat="server" visible="false">
                                                                <td><asp:Label ID="Label28" runat="server" Font-Bold="true" Text="Deduction 3 %"></asp:Label></td>
                                                                <td><asp:TextBox ID="txtdeduction3Precentge" runat="server" TabIndex="12" Text="0"></asp:TextBox>
                                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR20" runat="server" FilterMode="ValidChars"
                                                                FilterType="Numbers,Custom" TargetControlID="txtdeduction3Precentge" ValidChars="."></cc1:FilteredTextBoxExtender>
                                                                </td>
                                                                <td><asp:Label ID="label29" runat="server" Font-Bold="true" Text="Deduction 3"></asp:Label></td>
                                                                <td colspan="1"><asp:TextBox ID="txtdeductionAmt3" runat="server" Enabled="false" Text="0"></asp:TextBox></td>
                                                            </tr>
                                                            <tr id="Panelstaff8" runat="server" visible="false" >
                                                                <td><asp:Button ID="btnCalculation" runat="server" Height="28" 
                                                                 onclick="btnCalculation_Click" Text="Calculation" Width="100" CssClass="button green" /></td>
                                                                <td></td>
                                                                <td><asp:Label ID="label22" runat="server" Font-Bold="true" Text="Net Pay"></asp:Label></td>
                                                                <td><asp:TextBox ID="txtnet" runat="server" Enabled="false" Text="0"></asp:TextBox></td>
                                                            </tr>
                                                            <tr id="panelMonthlyLabour1" runat="server" visible="false">
                                                                <td><asp:Label ID="lblLabourBasic" runat="server" Text="Basic Salary" Font-Bold="true" Visible="false" ></asp:Label>
                                                                <asp:Label ID="lblLabourDailyWages" runat="server" Text="Daily Salary" Font-Bold="true" Visible="false"></asp:Label>
                                                                </td>
                                                                <td><asp:TextBox ID="txtLabourBasic" runat="server" ></asp:TextBox></td>
                                                                <td><asp:Label ID="lblMessType" runat="server" Text="Mess Type" Font-Bold="true" ></asp:Label></td>
                                                                <td><asp:DropDownList ID="ddlFood" runat="server" AutoPostBack="true" Width="150" Height="25" ></asp:DropDownList></td>
                                                            </tr>
                                                            <tr id="panelMonthlyLabour2" runat="server" visible="false">
                                                                <td><asp:Label ID="lblmessdeduction" runat="server" Text="Mess Deduction" Font-Bold="true"></asp:Label></td>
                                                                <td><asp:TextBox ID="txtmessDeductionLabour" runat="server" Text="0" ></asp:TextBox>
                                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR14" runat="server" FilterMode="ValidChars"
                                                                FilterType="Numbers,Custom" TargetControlID="txtmessDeductionLabour" ValidChars="."></cc1:FilteredTextBoxExtender></td>
                                                                <td><asp:Label ID="lblHostelDeduction" runat="server" Text="Hostel Deduction" Font-Bold="true"></asp:Label></td>
                                                                <td><asp:TextBox ID="txtHostel" runat="server" Text="0"></asp:TextBox>
                                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR13" runat="server" FilterMode="ValidChars"
                                                                FilterType="Numbers,Custom" TargetControlID="txtHostel" ValidChars="."></cc1:FilteredTextBoxExtender></td>
                                                            </tr>
                                                            <tr id="PanelLabourMonthlyAllowace" runat="server" visible="false">
                                                                <td><asp:Label ID="lblallowance1" runat="server" Text="Allowance 1" Font-Bold="true"></asp:Label></td>
                                                                <td><asp:TextBox ID="txtlabourallow1" runat="server" Text="0" ></asp:TextBox>
                                                                  <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR16" runat="server" FilterMode="ValidChars"
                                                                FilterType="Numbers,Custom" TargetControlID="txtlabourallow1" ValidChars="."></cc1:FilteredTextBoxExtender>
                                                                </td>
                                                                <td><asp:Label ID="lbldeduction1" runat="server" Text="Deduction 1" Font-Bold="true" ></asp:Label></td>
                                                               <td><asp:TextBox ID="txtdeduction1" runat="server" Text="0"></asp:TextBox>
                                                               <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR12" runat="server" FilterMode="ValidChars"
                                                                FilterType="Numbers,Custom" TargetControlID="txtdeduction1" ValidChars="."></cc1:FilteredTextBoxExtender></td>                      
                                                            </tr>
                                                             <tr id="panelMonthlyLabour3" runat="server" visible="false">
                                                               <td><asp:Label ID="Label5" runat="server" Text="Allowance 2" Font-Bold="true"></asp:Label></td>
                                                                <td><asp:TextBox ID="txtlabourallow2" runat="server" Text="0" ></asp:TextBox>
                                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR15" runat="server" FilterMode="ValidChars"
                                                                FilterType="Numbers,Custom" TargetControlID="txtlabourallow2" ValidChars="."></cc1:FilteredTextBoxExtender>
                                                                </td>
                                                               <td><asp:Label ID="lblDeduction2" runat="server" Text="Deduction 2" Font-Bold="true"></asp:Label></td>
                                                               <td ><asp:TextBox ID="txtdeduction2" runat="server" Text="0" ></asp:TextBox>
                                                               <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR11" runat="server" FilterMode="ValidChars"
                                                                FilterType="Numbers,Custom" TargetControlID="txtdeduction2" ValidChars="."></cc1:FilteredTextBoxExtender></td>
                                                            </tr>
                                                            <tr id="panelMonthlyLabour4" runat="server" visible="false">
                                                               <td><asp:Label ID="Label23" runat="server" Text="Allowance 3" Font-Bold="true"></asp:Label></td>
                                                               <td><asp:TextBox ID="txtlabourallowance3" runat="server" Text="0" ></asp:TextBox>
                                                               <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR17" runat="server" FilterMode="ValidChars"
                                                                FilterType="Numbers,Custom" TargetControlID="txtlabourallowance3" ValidChars="."></cc1:FilteredTextBoxExtender>
                                                               </td>
                                                               <td><asp:Label ID="lblDdeduction3" runat="server" Text="Deduction 3" Font-Bold="true" ></asp:Label></td>
                                                               <td><asp:TextBox ID="txtdeduction3" runat="server" Text="0" ></asp:TextBox>
                                                               <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR10" runat="server" FilterMode="ValidChars"
                                                                FilterType="Numbers,Custom" TargetControlID="txtdeduction3" ValidChars="."></cc1:FilteredTextBoxExtender></td>
                                                            </tr>
                                                            <tr><td align="center" colspan="4">
                                                                <asp:Button ID="btnSave" runat="server" Height="28" onclick="btnSave_Click" Text="Save" Width="75" Font-Bold="true" CssClass="button green"/>
                                                                <asp:Button ID="btnreset" runat="server" Height="28"  Text="Reset" Width="75" Font-Bold="true"
                                                                       onclick="btnreset_Click" CssClass="button green" />
                                                                <asp:Button ID="btncancel" runat="server" Height="28" onclick="btncancel_Click" Text="Cancel" Width="75" Font-Bold="true" CssClass="button green"/></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            
	                    </ContentTemplate>
	                </asp:UpdatePanel>            
            </div>
            <!-- end page-wrap -->
            <div id="footer">
                <ul>
                    <li>Copyright &copy; 2012. All rights reserved.</li>
                    <li>Powered by <a href="http://www.altius.co.in" target="_blank">Altius Infosystems</a></li>
                </ul>
            </div>
            <!-- end footer -->
        </form>
    </body>
</html>

