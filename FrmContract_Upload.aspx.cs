﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Configuration;
using System.Data.SqlClient;
using System.Data.OleDb;
using Payroll;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class FrmContract_Upload : System.Web.UI.Page
{
    string SessionAdmin;
    string DepartmentCode;
    string Name_Upload;
    string Name_Upload1;
    BALDataAccess objdata = new BALDataAccess();
    SalaryMasterClass objsal = new SalaryMasterClass();
    string SessionCcode;
    string SessionLcode;
    DateTime dfrom;
    DateTime dtto;
    DateTime MyDate1;
    DateTime MyDate2;
    protected void Page_Load(object sender, EventArgs e)
    {
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }

        lblComany.Text = SessionCcode + " - " + SessionLcode;
        SessionAdmin = Session["Isadmin"].ToString();
        if ((SessionAdmin == "2") || (SessionAdmin == "1"))
        {
            Response.Redirect("EmployeeRegistration.aspx");
        }
    }
    protected void btnEmp_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptEmpDownload.aspx");
    }
    protected void btnatt_Click(object sender, EventArgs e)
    {
        Response.Redirect("AttenanceDownload.aspx");
    }
    protected void btnLeave_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptLeaveSample.aspx");
    }
    protected void btnOT_Click(object sender, EventArgs e)
    {
        Response.Redirect("OTDownload.aspx");
    }
    protected void btnSal_Click(object sender, EventArgs e)
    {
        Response.Redirect("FrmDeductionDownload.aspx");
    }
    protected void btnUpload_Click(object sender, EventArgs e)
    {
        try
        {
            bool ErrFlag = false;
            bool Update = false;
            if (!ErrFlag)
            {
                if (!ErrFlag)
                {
                    if (FileUpload.HasFile)
                    {
                        FileUpload.SaveAs(Server.MapPath("Upload/" + FileUpload.FileName));

                    }
                    string connectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + Server.MapPath("Upload/" + FileUpload.FileName) + ";" + "Extended Properties=Excel 8.0;";
                    OleDbConnection sSourceConnection = new OleDbConnection(connectionString);
                    DataTable dts = new DataTable();
                    using (sSourceConnection)
                    {
                        sSourceConnection.Open();

                        OleDbCommand command = new OleDbCommand("Select * FROM [Sheet1$];", sSourceConnection);
                        sSourceConnection.Close();

                        using (OleDbCommand cmd = sSourceConnection.CreateCommand())
                        {
                            command.CommandText = "Select * FROM [Sheet1$];";
                            sSourceConnection.Open();
                        }
                        using (OleDbDataReader dr = command.ExecuteReader())
                        {

                            if (dr.HasRows)
                            {

                            }

                        }
                        OleDbDataAdapter objDataAdapter = new OleDbDataAdapter();
                        objDataAdapter.SelectCommand = command;
                        DataSet ds = new DataSet();

                        objDataAdapter.Fill(ds);
                        DataTable dt = new DataTable();
                        dt = ds.Tables[0];
                        for (int i = 0; i < dt.Columns.Count; i++)
                        {
                            dt.Columns[i].ColumnName = dt.Columns[i].ColumnName.Replace(" ", string.Empty).ToString();

                        }
                        string constr = ConfigurationManager.AppSettings["ConnectionString"];
                        for (int j = 0; j < dt.Rows.Count; j++)
                        {
                            SqlConnection cn = new SqlConnection(constr);
                            //string Department = dt.Rows[j][0].ToString();
                            //string MachineID = dt.Rows[j][1].ToString();
                            string EmpNo = dt.Rows[j][0].ToString();
                            string ExistingCode = dt.Rows[j][1].ToString();
                            string EmpName = dt.Rows[j][2].ToString();                            
                            string MonthCount = dt.Rows[j][3].ToString();
                            string DecMonth = dt.Rows[j][4].ToString();
                            string BasicMonths = dt.Rows[j][5].ToString();
                            //string HRsalary = dt.Rows[0][5].ToString();
                            //string NoHrs = dt.Rows[0][6].ToString();
                            //string netAmount = dt.Rows[0][5].ToString();                        
                            if (EmpNo == "")
                            {
                                j = j + 2;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Employee Number. The Row Number is " + j + "');", true);
                                //System.Windows.Forms.MessageBox.Show("Enter the Employee Number. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                ErrFlag = true;
                            }
                            else if (ExistingCode == "")
                            {
                                j = j + 2;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Existing Code. The Row Number is " + j + "');", true);
                                //System.Windows.Forms.MessageBox.Show("Enter the Existing Code. The Roe Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                ErrFlag = true;
                            }
                            else if (EmpName == "")
                            {
                                j = j + 2;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Name. The Row Number is " + j + "');", true);
                                //System.Windows.Forms.MessageBox.Show("Enter the Name. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                ErrFlag = true;
                            }
                            else if (BasicMonths == "")
                            {
                                j = j + 2;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Basic Month. The Row Number is " + j + "');", true);
                                //System.Windows.Forms.MessageBox.Show("Enter the Employee Number. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                ErrFlag = true;
                            }
                            else if (MonthCount == "")
                            {
                                j = j + 2;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Month Count. The Row Number is " + j + "');", true);
                                //System.Windows.Forms.MessageBox.Show("Enter the Employee Number. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                ErrFlag = true;
                            }
                            else if (DecMonth == "")
                            {
                                j = j + 2;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Month Count. The Row Number is " + j + "');", true);
                                //System.Windows.Forms.MessageBox.Show("Enter the Employee Number. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                ErrFlag = true;
                            }
                            cn.Open();
                            string qry_empNo = "Select EmpNo from EmployeeDetails where EmpNo= '" + EmpNo + "' and ExisistingCode = '" + ExistingCode + "' and EmpName = '" + EmpName + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                            SqlCommand cmd_Emp = new SqlCommand(qry_empNo, cn);
                            SqlDataReader sdr_Emp = cmd_Emp.ExecuteReader();
                            if (sdr_Emp.HasRows)
                            {
                            }
                            else
                            {
                                j = j + 2;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Employee Details Properly. The Row Number is " + j + "');", true);
                                //System.Windows.Forms.MessageBox.Show("Enter the Employee Details Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                ErrFlag = true;
                            }
                            cn.Close();
                        }
                        if (!ErrFlag)
                        {
                            for (int i = 0; i < dt.Rows.Count; i++)
                            {
                                //SqlConnection cn = new SqlConnection(constr);
                                //cn.Open();

                                string empCodeverify = objdata.SalaryMasterVerify(dt.Rows[i][0].ToString(), SessionCcode, SessionLcode);
                                if (empCodeverify == dt.Rows[i][0].ToString().Trim())
                                {
                                    objdata.Contract_Upload_data(SessionCcode, SessionLcode, dt.Rows[i][0].ToString(), dt.Rows[i][3].ToString(), dt.Rows[i][4].ToString(), dt.Rows[i][5].ToString());
                                    //objdata.Upload_SalaryMaster(Ccode, Lcode, EmpNo, Basic, pf);
                                    Update = true;
                                }
                            }
                        }
                        if (Update == true)
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Finished Successfully');", true);
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Please Upload Correct File Format...');", true);
        }
    }
    protected void btnDownload_Click(object sender, EventArgs e)
    {

    }
}
