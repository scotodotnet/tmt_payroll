﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="RptEmpDownload.aspx.cs" Inherits="RptEmpDownload" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <tr id="Panelload" runat="server" visible="false">
            <td>
                <asp:GridView ID="gvEmp" runat="server" AutoGenerateColumns="false" >
                    <Columns>
                        <asp:TemplateField>
                            <HeaderTemplate>ExisistingCode</HeaderTemplate>
                            <ItemTemplate>
                            <asp:Label ID="ExisistingCode" runat="server" Text='<%# Eval("ExisistingCode") %>' ></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>EmpName</HeaderTemplate>
                            <ItemTemplate>
                            <asp:Label ID="EmpName" runat="server" Text='<%# Eval("EmpName") %>' ></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>FatherName</HeaderTemplate>
                            <ItemTemplate>
                            <asp:Label ID="FatherName" runat="server" Text='<%# Eval("FatherName") %>' ></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>Gender</HeaderTemplate>
                            <ItemTemplate>
                            <asp:Label ID="Gender" runat="server" Text='<%# Eval("Gender") %>' ></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>DOB</HeaderTemplate>
                            <ItemTemplate>
                            <asp:Label ID="DOB" runat="server" Text='<%# Eval("DOB") %>' ></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>PSAdd1</HeaderTemplate>
                            <ItemTemplate>
                            <asp:Label ID="PSAdd1" runat="server" Text='<%# Eval("PSAdd1") %>' ></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>PSAdd2</HeaderTemplate>
                            <ItemTemplate>
                            <asp:Label ID="PSAdd2" runat="server" Text='<%# Eval("PSAdd2") %>' ></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>PSDistrict</HeaderTemplate>
                            <ItemTemplate>
                            <asp:Label ID="PSDistrict" runat="server" Text='<%# Eval("PSDistrict") %>' ></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>PSTaluk</HeaderTemplate>
                            <ItemTemplate>
                            <asp:Label ID="PSTaluk" runat="server" Text='<%# Eval("PSTaluk") %>' ></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>PSState</HeaderTemplate>
                            <ItemTemplate>
                            <asp:Label ID="PSState" runat="server" Text='<%# Eval("PSState") %>' ></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>    
                        <asp:TemplateField>
                            <HeaderTemplate>Phone</HeaderTemplate>
                            <ItemTemplate>
                            <asp:Label ID="Phone" runat="server" Text='<%# Eval("Phone") %>' ></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>Mobile</HeaderTemplate>
                            <ItemTemplate>
                            <asp:Label ID="Mobile" runat="server" Text='<%# Eval("Mobile") %>' ></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>    
                        <asp:TemplateField>
                            <HeaderTemplate>PassportNo</HeaderTemplate>
                            <ItemTemplate>
                            <asp:Label ID="PassportNo" runat="server" Text='<%# Eval("PassportNo") %>' ></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>    
                        <asp:TemplateField>
                            <HeaderTemplate>DrivingLicenceNo</HeaderTemplate>
                            <ItemTemplate>
                            <asp:Label ID="DrivingLicenceNo" runat="server" Text='<%# Eval("DrivingLicenceNo") %>' ></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>    
                        <asp:TemplateField>
                            <HeaderTemplate>Qualification</HeaderTemplate>
                            <ItemTemplate>
                            <asp:Label ID="Qualification" runat="server" Text='<%# Eval("QualificationNm") %>' ></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>   
                        <asp:TemplateField>
                            <HeaderTemplate>University</HeaderTemplate>
                            <ItemTemplate>
                            <asp:Label ID="University" runat="server" Text='<%# Eval("University") %>' ></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>   
                        <asp:TemplateField>
                            <HeaderTemplate>Department</HeaderTemplate>
                            <ItemTemplate>
                            <asp:Label ID="Department" runat="server" Text='<%# Eval("DepartmentNm") %>' ></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>   
                        <asp:TemplateField>
                            <HeaderTemplate>Designation</HeaderTemplate>
                            <ItemTemplate>
                            <asp:Label ID="Designation" runat="server" Text='<%# Eval("Designation") %>' ></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>   
                        <asp:TemplateField>
                            <HeaderTemplate>EmployeeType</HeaderTemplate>
                            <ItemTemplate>
                            <asp:Label ID="EmployeeType" runat="server" Text='<%# Eval("EmpType") %>' ></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>   
                        <asp:TemplateField>
                            <HeaderTemplate>NonPFGrade</HeaderTemplate>
                            <ItemTemplate>
                            <asp:Label ID="NonPFGrade" runat="server" Text='<%# Eval("NonPFGrade") %>' ></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>  
                        <asp:TemplateField>
                            <HeaderTemplate>StafforLabor</HeaderTemplate>
                            <ItemTemplate>
                            <asp:Label ID="StafforLabor" runat="server" Text='<%# Eval("StafforLabor") %>' ></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>  
                        <asp:TemplateField>
                            <HeaderTemplate>Educated</HeaderTemplate>
                            <ItemTemplate>
                            <asp:Label ID="Educated" runat="server" Text='<%# Eval("Uneducated") %>' ></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>  
                        <asp:TemplateField>
                            <HeaderTemplate>MartialStatus</HeaderTemplate>
                            <ItemTemplate>
                            <asp:Label ID="MartialStatus" runat="server" Text='<%# Eval("MartialStatus") %>' ></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>  
                        <asp:TemplateField>
                            <HeaderTemplate>Initial</HeaderTemplate>
                            <ItemTemplate>
                            <asp:Label ID="Initial" runat="server" Text='<%# Eval("Initial") %>' ></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>         
                        <asp:TemplateField>
                            <HeaderTemplate>BiometricID</HeaderTemplate>
                            <ItemTemplate>
                            <asp:Label ID="contract" runat="server" Text='<%# Eval("BiometricID") %>' ></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>              
                    </Columns>
                </asp:GridView>
            </td>
            
        </tr>
    </div>
    </form>
</body>
</html>
