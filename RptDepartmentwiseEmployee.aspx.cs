﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;

using Payroll;
using Payroll.Data;
using Payroll.Configuration;

using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html;
using iTextSharp.text.html.simpleparser;


using Altius.BusinessAccessLayer.BALDataAccess;

public partial class RptDepartmentwiseEmployee_1 : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    string SessionAdmin;
    string Stafflabour;
    string SessionCcode;
    string SessionLcode;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionAdmin = Session["Isadmin"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        string ss = Session["UserId"].ToString();
        if (SessionAdmin == "1")
        {
            PanelOnrole.Visible = true;
        }
        else
        {
            PanelOnrole.Visible = false;
        }
        if (!IsPostBack)
        {
            DropDwonCategory();
        }
        lblComany.Text = SessionCcode + " - " + SessionLcode;
        lblusername.Text = Session["Usernmdisplay"].ToString();
    }
    public void DropDwonCategory()
    {
        DataTable dtcate = new DataTable();
        dtcate = objdata.DropDownCategory();
        ddlcategory.DataSource = dtcate;
        ddlcategory.DataTextField = "CategoryName";
        ddlcategory.DataValueField = "CategoryCd";
        ddlcategory.DataBind();
    }
    protected void ddldept_SelectedIndexchanged(object sender, EventArgs e)
    {

        //Clear Form
        Result_Panel.Visible = false;
        //rbtngender.SelectedIndex = 0;

    }



    protected void btnexport_Click(object sender, EventArgs e)
    {

        if (ddlrptReport.SelectedValue == "1")
        {

            string attachment = "attachment; filename=DepartmentwiseEmployee.xls";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/ms-excel";

            StringWriter stw = new StringWriter();
            HtmlTextWriter htextw = new HtmlTextWriter(stw);
            griddept.RenderControl(htextw);
            Response.Write("Employee Details");
            Response.Write(stw.ToString());

            Response.End();
        }
        else if (ddlrptReport.SelectedValue == "2")
        {

            string attachment = "attachment; filename=DepartmentwiseEmployee.pdf";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/pdf";
            StringWriter stw = new StringWriter();

            HtmlTextWriter htextw = new HtmlTextWriter(stw);
            griddept.RenderControl(htextw);

            Document document = new Document(PageSize.A4, 10f, 10f, 10f, 10f);


            PdfWriter.GetInstance(document, Response.OutputStream);
            document.Open();
            StringReader str = new StringReader(stw.ToString());
            HTMLWorker htmlworker = new HTMLWorker(document);
            htmlworker.Parse(str);
            document.Close();
            Response.Write(document);
            Response.End();
        }
        else if (ddlrptReport.SelectedValue == "3")
        {

            string attachment = "attachment; filename=DepartmentwiseEmployee.doc";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/ms-excel";
            StringWriter stw = new StringWriter();
            HtmlTextWriter htextw = new HtmlTextWriter(stw);

            griddept.RenderControl(htextw);
            Response.Write("Employee Details");
            Response.Write(stw.ToString());
            Response.End();
        }

    }
    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }
    protected void ddlcategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable dtempty = new DataTable();
        ddldept.DataSource = dtempty;
        ddldept.DataBind();
        griddept.DataSource = dtempty;
        griddept.DataBind();
        if (ddlcategory.SelectedValue == "1")
        {
            Stafflabour = "S";
        }
        else if (ddlcategory.SelectedValue == "2")
        {
            Stafflabour = "L";
        }
        else
        {
            Stafflabour = "0";
        }
        DataTable dtDip = new DataTable();
        dtDip = objdata.DropDownDepartment_category(Stafflabour);
        if (dtDip.Rows.Count > 1)
        {
            ddldept.DataSource = dtDip;
            ddldept.DataTextField = "DepartmentNm";
            ddldept.DataValueField = "DepartmentCd";
            ddldept.DataBind();
        }
        else
        {
            DataTable dt = new DataTable();
            ddldept.DataSource = dtempty;
            ddldept.DataBind();
        }
    }
    protected void btnClick_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        try
        {
            if (ddlcategory.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select Employee Category');", true);
                //System.Windows.Forms.MessageBox.Show("Select Employee Category", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            if (ddldept.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Department Name');", true);
                //System.Windows.Forms.MessageBox.Show("Select Employee Category", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            if (!ErrFlag)
            {
                Result_Panel.Visible = true;
                DataTable dt = new DataTable();
                if (ddlcategory.SelectedValue == "1")
                {
                    string Cate = "S";
                    if (SessionAdmin == "1")
                    {
                        dt = objdata.DepartwiseEmployee(ddldept.SelectedValue, rbtngender.SelectedValue, "Yes", Cate, SessionCcode, SessionLcode, rppfgrade.SelectedValue);
                    }
                    else
                    {
                        dt = objdata.DepartwiseEmployee(ddldept.SelectedValue, rbtngender.SelectedValue, "No", Cate, SessionCcode, SessionLcode, rppfgrade.SelectedValue);
                    }
                    griddept.DataSource = dt;
                    griddept.DataBind();
                }
                else if (ddlcategory.SelectedValue == "2")
                {
                    string Cate = "L";
                    if (SessionAdmin == "1")
                    {
                        dt = objdata.DepartwiseEmployee(ddldept.SelectedValue, rbtngender.SelectedValue, "Yes", Cate, SessionCcode, SessionLcode, rppfgrade.SelectedValue);
                    }
                    else
                    {
                        dt = objdata.DepartwiseEmployee(ddldept.SelectedValue, rbtngender.SelectedValue, "No", Cate, SessionCcode, SessionLcode, rppfgrade.SelectedValue);
                    }
                    griddept.DataSource = dt;
                    griddept.DataBind();
                }


            }
        }
        catch (Exception ex)
        { }
    }
    protected void rbtngender_SelectedIndexChanged(object sender, EventArgs e)
    {
        
    }
    protected void btnEmp_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptEmpDownload.aspx");
    }
    protected void btnatt_Click(object sender, EventArgs e)
    {
        Response.Redirect("AttenanceDownload.aspx");
    }
    protected void btnLeave_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptLeaveSample.aspx");
    }
    protected void btnOT_Click(object sender, EventArgs e)
    {
        Response.Redirect("OTDownload.aspx");
    }
    protected void btncontract_Click(object sender, EventArgs e)
    {
        Response.Redirect("ContractRPT.aspx");
    }
    protected void btnSal_Click(object sender, EventArgs e)
    {
        Response.Redirect("FrmDeductionDownload.aspx");
    }

}
