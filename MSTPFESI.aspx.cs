﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Payroll;
using Payroll.Configuration;
using Payroll.Data;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class MSTPFESI : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        string ss = Session["UserId"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        lblComany.Text = SessionCcode + " - " + SessionLcode;
        lblusername.Text = Session["Usernmdisplay"].ToString();
        if (!IsPostBack)
        {
            ESIPF_load();
        }
    }
    public void ESIPF_load()
    {
        DataTable dt = new DataTable();
        dt = objdata.ESI_PF_Load(SessionCcode, SessionLcode);
        if (dt.Rows.Count > 0)
        {
            txtpf.Text = dt.Rows[0]["PF_per"].ToString();
            txtESI.Text = dt.Rows[0]["ESI_per"].ToString();
            txtstaff.Text = dt.Rows[0]["StaffSalary"].ToString();
            txtstamp.Text = dt.Rows[0]["StampCg"].ToString();
            txtVDAPoint1.Text = dt.Rows[0]["VDA1"].ToString();
            txtVDAPoint2.Text = dt.Rows[0]["VDA2"].ToString();
            txtSpinning.Text = dt.Rows[0]["Spinning"].ToString();
            txtDayIncentive.Text = dt.Rows[0]["DayIncentive"].ToString();
            txtHalf.Text = dt.Rows[0]["halfNight"].ToString();
            txtFull.Text = dt.Rows[0]["FullNight"].ToString();
            txtThree.Text = dt.Rows[0]["ThreeSideAmt"].ToString();
            if (dt.Rows[0]["Union_val"].ToString() == "1")
            {
                chkUnion.Checked = true;
            }
            else
            {
                chkUnion.Checked = false;
            }
        }
        else
        {
            txtpf.Text = "";
            txtESI.Text = "";
            txtstaff.Text = "";
            txtSpinning.Text = "";
            txtDayIncentive.Text = "";
            txtHalf.Text = "";
            txtFull.Text = "";
            txtThree.Text = "";
        }
    }
    protected void btnclr_Click(object sender, EventArgs e)
    {
        ESIPF_load();
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            bool ErrFlag = false;
            if (txtpf.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the PF %');", true);
                ErrFlag = true;
            }
            else if (txtESI.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the ESI %');", true);
                ErrFlag = true;
            }
            else if (txtstaff.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Staff Salary');", true);
                ErrFlag = true;
            }
            else if (txtstamp.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Stamp Charge');", true);
                ErrFlag = true;
            }
            else if (txtVDAPoint1.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the VDA Point 1');", true);
                ErrFlag = true;
            }
            else if (txtVDAPoint2.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the VDA Point 2');", true);
                ErrFlag = true;
            }
            else if (txtSpinning.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Spinning Department Amount');", true);
                ErrFlag = true;
            }
            else if (txtDayIncentive.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Day Incentive Amount');", true);
                ErrFlag = true;
            }
            else if (txtHalf.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the O/N Shift Amount');", true);
                ErrFlag = true;
            }
            else if (txtFull.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the F/N Shift Amount');", true);
                ErrFlag = true;
            }
            else if (txtThree.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Three Side Amount');", true);
                ErrFlag = true;
            }
            if (!ErrFlag)
            {
                if (Convert.ToDecimal(txtpf.Text.Trim()) == 0)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the PF Properly');", true);
                    ErrFlag = true;
                }
                //else if (Convert.ToDecimal(txtESI.Text.Trim()) == 0)
                //{
                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the ESI Properly');", true);
                //    ErrFlag = true;
                //}
                else if (Convert.ToDecimal(txtstaff.Text.Trim()) == 0)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Staff Salary Properly');", true);
                    ErrFlag = true;
                }
                else if (Convert.ToDecimal(txtVDAPoint2.Text) > Convert.ToDecimal(txtVDAPoint1.Text))
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the VDA Points properly');", true);
                    ErrFlag = true;
                }
                if (!ErrFlag)
                {
                    string Union = "";
                    if (chkUnion.Checked == true)
                    {
                        Union = "1";
                    }
                    else
                    {
                        Union = "0";
                    }
                    string Db = objdata.ESIPF_verify(SessionCcode, SessionLcode);
                    if (Db.Trim() == "")
                    {
                        objdata.ESIPF_Insert(txtpf.Text.Trim(), txtESI.Text.Trim(), txtstaff.Text.Trim(), SessionCcode, SessionLcode, txtstamp.Text, txtVDAPoint1.Text, txtVDAPoint2.Text, Union, txtSpinning.Text.Trim(), txtHalf.Text.Trim(), txtFull.Text.Trim(), txtDayIncentive.Text.Trim(), txtThree.Text.Trim());
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Saved Successfully');", true);
                    }
                    else
                    {
                        objdata.ESIPF_Update(txtpf.Text.Trim(), txtESI.Text.Trim(), txtstaff.Text.Trim(), SessionCcode, SessionLcode, txtstamp.Text, txtVDAPoint1.Text, txtVDAPoint2.Text, Union, txtSpinning.Text.Trim(), txtHalf.Text.Trim(), txtFull.Text.Trim(), txtDayIncentive.Text.Trim(), txtThree.Text.Trim());
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Updated Successfully');", true);
                    }
                }
            }
        }
        catch (Exception ex)
        {
        }
    }
    protected void btnEmp_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptEmpDownload.aspx");
    }
    protected void btnatt_Click(object sender, EventArgs e)
    {
        Response.Redirect("AttenanceDownload.aspx");
    }
    protected void btnLeave_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptLeaveSample.aspx");
    }
    protected void btnOT_Click(object sender, EventArgs e)
    {
        Response.Redirect("OTDownload.aspx");
    }
    protected void chkUnion_CheckedChanged(object sender, EventArgs e)
    {

    }
    protected void btncontract_Click(object sender, EventArgs e)
    {
        Response.Redirect("ContractRPT.aspx");
    }
    protected void btnSal_Click(object sender, EventArgs e)
    {
        Response.Redirect("FrmDeductionDownload.aspx");
    }
}
