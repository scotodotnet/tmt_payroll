﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Payroll;
using Payroll.Configuration;
using Payroll.Data;
using Altius.BusinessAccessLayer.BALDataAccess;


public partial class LeaveAllocationForLabours : System.Web.UI.Page
{
    protected System.Resources.ResourceManager rm;
    protected string PayrollRegisterContentMeta, pageTitle;
    BALDataAccess objdata = new BALDataAccess();
    string stafforLabour;
    string SessionAdmin;
    protected void Page_Load(object sender, EventArgs e)
    {
        SessionAdmin = Session["Isadmin"].ToString();
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        string ss = Session["UserId"].ToString();
        if (Request.QueryString != null) 
        {
            //  PanelEditTextboxPanel.Visible = true;
        }
        //string ss = Session["UserId"].ToString();
        //if (Session["UserId"] == null)
        //{
        //    Response.Redirect("Default.aspx");
        //    Response.Write("Your session expired");
        //}
        if (!IsPostBack)
        {
            Department();
            // EmployeeDetails_Load();
        }
    }
    protected void rbtnlaburstaff_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (rbtnlaburstaff.SelectedValue == "2")
        {
            panelLabourDepartment.Visible = false;
            PanelGrid.Visible = false;
            PanelGrouporIndividual.Visible = false;
            PanelButtons.Visible = false;
        }
        else
        {

            panelLabourDepartment.Visible = true;
            PanelGrid.Visible = false;
            PanelGrouporIndividual.Visible = false;
            PanelButtons.Visible = false;

        }
    }
    public void Department()
    {
        DataTable dtDip = new DataTable();
        dtDip = objdata.DropDownDepartment();
        ddlDepartment.DataSource = dtDip;
        ddlDepartment.DataTextField = "DepartmentNm";
        ddlDepartment.DataValueField = "DepartmentCd";
        ddlDepartment.DataBind();
    }
    public void EmployeeDetails_Load()
    {

        if (rbtnlaburstaff.SelectedValue == "1")
        {
            stafforLabour = "S";
        }
        else if (rbtnlaburstaff.SelectedValue == "2")
        {
            stafforLabour = "L";
        }

        //DataTable dt1 = new DataTable();
        //dt1 = objdata.LeaveAllocationEmployee(stafforLabour, ddlDepartment.SelectedValue);
        //DataGridView1.DataSource = dt1;
        //DataGridView1.DataBind();
    }
    protected void ddlDepartment_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void ddlDepartment_TextChanged(object sender, EventArgs e)
    {
        if (ddlDepartment.SelectedValue == "0")
        {
            PanelGrid.Visible = false;
            PanelButtons.Visible = false;
            PanelGrouporIndividual.Visible = false;
        }
        else if (ddlDepartment.SelectedValue == "1")
        {
            PanelGrid.Visible = true;
            PanelButtons.Visible = true;
            EmployeeDetails_Load();
            PanelGrouporIndividual.Visible = false;
        }
        else if (ddlDepartment.SelectedValue == "2")
        {
            PanelGrid.Visible = true;
            PanelButtons.Visible = true;
            EmployeeDetails_Load();
            PanelGrouporIndividual.Visible = false;
        }
    }

    protected void DataGridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        DataGridView1.EditIndex = -1;
        EmployeeDetails_Load();
    }
    protected void DataGridView1_RowEditing(object sender, GridViewEditEventArgs e)
    {
        DataGridView1.EditIndex = e.NewEditIndex;
        EmployeeDetails_Load();
    }
    protected void DataGridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {

    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        if (ddlDepartment.Text == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Please Select Department...!');", true);
            ErrFlag = true;
        }
        else if (ddlDepartment.SelectedValue == "0")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Please Select Department...!');", true);
            ErrFlag = true;
        }
        else if (DataGridView1.Rows.Count <= 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Please Select Department...!');", true);
            ErrFlag = true;
        }
        if (!ErrFlag)
        {

        }

    }
    protected void btnIndividual_Click(object sender, EventArgs e)
    {
        PanelGrouporIndividual.Visible = true;
        PanelGrid.Visible = false;
    }
    protected void btnall_Click(object sender, EventArgs e)
    {
        PanelGrouporIndividual.Visible = false;
        PanelGrid.Visible = true;
        EmployeeDetails_Load();
    }
    protected void btnsearch_Click(object sender, EventArgs e)
    {
    //    bool ErrFlag = false;
    //    if (rbtnlaburstaff.SelectedValue == "1")
    //    {
    //        stafforLabour = "S";
    //    }
    //    else if (rbtnlaburstaff.SelectedValue == "2")
    //    {
    //        stafforLabour = "L";
    //    }
    //    string EmpVerify = objdata.EmployeeVerifyforLeaveAllocation(txtEmpNo.Text, ddlDepartment.SelectedValue, stafforLabour);
    //    string EmpVerify=
    //    //if(txtEmpNo.Text == "")
    //    //{
    //    //    //ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Enter the Employee No..!');", true);
    //    //    //ErrFlag = true;
    //    //}
    //    //if (!ErrFlag)
    //    //{
    //    //    PanelGrid.Visible = true;
    //    //    DataTable DT1 = new DataTable();
    //    //    DT1 = objdata.EmployeeVerifyforLeaveAllocation(txtEmpNo.Text, ddlDepartment.SelectedValue, stafforLabour);
    //    //    DataGridView1.DataSource = DT1;
    //    //    DataGridView1.DataBind();
    //    //}


    }
}