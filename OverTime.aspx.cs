﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Payroll;
using Payroll.Configuration;
using Payroll.Data;
using System.Data.SqlClient;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class OverTime_1 : System.Web.UI.Page
{
    protected System.Resources.ResourceManager rm;
    protected string PayrollRegisterContentMeta, pageTitle;
    BALDataAccess objdata = new BALDataAccess();
    Overtimeclass objot = new Overtimeclass();
    bool searchFlag = false;
    string Datetime_ser;
    DateTime mydate;
    //string Mont;
    //int Monthid;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    DateTime dfrom;
    DateTime dtto;
    DateTime MyDate1;
    DateTime MyDate2;
    protected void Page_Load(object sender, EventArgs e)
    {
        SessionAdmin = Session["Isadmin"].ToString();
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        string ss = Session["UserId"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        lblComany.Text = SessionCcode + " - " + SessionLcode;
        if (!IsPostBack)
        {
            int currentYear = Utility.GetFinancialYear;
            for (int i = 0; i < 10; i++)
            {
                ddlfinance.Items.Add(new ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                //  ddlShowYear.Items.Add(new ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                currentYear = currentYear - 1;
            }
            DropDownDepart();
            Months_load();
        }
        lblusername.Text = Session["Usernmdisplay"].ToString();
    }
    public void DropDownDepart()
    {
        DataTable dt = new DataTable();
        dt = objdata.Depart_Labour();
        ddldepartment.DataSource = dt;
        ddldepartment.DataTextField = "DepartmentNm";
        ddldepartment.DataValueField = "DepartmentCd";
        ddldepartment.DataBind();
    }
    public void Months_load()
    {
        DataTable dt = new DataTable();
        dt = objdata.months_load();
        ddlMonths.DataSource = dt;
        ddlMonths.DataTextField = "Months";
        ddlMonths.DataValueField = "Months";
        ddlMonths.DataBind();
    }
    private void clr()
    {
        DataTable Empty = new DataTable();
        txtfrom.Text = null;
        txtTo.Text = null;
        ddlEmpName.DataSource = Empty;
        ddlEmpName.DataBind();
        ddlEmpNo.DataSource = Empty;
        ddlEmpNo.DataBind();
        ddToken.DataSource = Empty;
        ddToken.DataBind();
        txtExist.Text = "";
        lblbasic1.Text = "";
        lblperday1.Text = "";
        lblhr1.Text = "";
        txthrs.Text = "";
        chkmanual.Checked = false;
        txtrate.Text = "";
        txtrate.Enabled = false;
        txtNet.Text = "";
    }
    protected void ddldepartment_SelectedIndexChanged(object sender, EventArgs e)
    {
        clr();
    }
    protected void btnClick_Click(object sender, EventArgs e)
    {
        clr();
        bool ErrFlag = false;
        if (rbsalary.SelectedValue == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Wages Type');", true);
            ErrFlag = true;
        }
        if (!ErrFlag)
        {
            DataTable dt = new DataTable();
            if (SessionAdmin == "1")
            {
                dt = objdata.OverTime_Load(ddldepartment.SelectedValue.ToString(), SessionAdmin, SessionCcode, SessionLcode, rbsalary.SelectedValue);
            }
            else
            {
                dt = objdata.OverTime_Load(ddldepartment.SelectedValue.ToString(), SessionAdmin, SessionCcode, SessionLcode, rbsalary.SelectedValue);
            }


            ddlEmpNo.DataSource = dt;
            DataRow dr = dt.NewRow();
            dr["EmpNo"] = ddldepartment.SelectedItem.Text;
            dr["EmpName"] = ddldepartment.SelectedItem.Text;
            dr["ExisistingCode"] = ddldepartment.SelectedItem.Text;
            dt.Rows.InsertAt(dr, 0);
            ddlEmpNo.DataTextField = "EmpNo";
            ddlEmpNo.DataValueField = "EmpNo";
            ddlEmpNo.DataBind();
            ddlEmpName.DataSource = dt;
            ddlEmpName.DataTextField = "EmpName";
            ddlEmpName.DataValueField = "EmpNo";
            ddlEmpName.DataBind();
            ddToken.DataSource = dt;
            ddToken.DataTextField = "ExisistingCode";
            ddToken.DataValueField = "ExisistingCode";
            ddToken.DataBind();
        }
    }
    protected void ddlEmpName_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            bool ErrFlag = false;
            DataTable dtpf = new DataTable();
            dtpf = objdata.Load_pf_details(SessionCcode, SessionLcode);
            if (ddldepartment.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Employee Department');", true);
                ErrFlag = true;
            }
            else if (ddlEmpName.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Employee Employee Name');", true);
                ErrFlag = true;
            }
            else if (ddlEmpName.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Employee Name');", true);
                ErrFlag = true;
            }
            if (!ErrFlag)
            {
                DataTable dt = new DataTable();
                dt = objdata.Overtime_Details(ddldepartment.SelectedValue, ddlEmpName.SelectedValue, SessionCcode, SessionLcode);
                if (dt.Rows.Count > 0)
                {
                    txtExist.Text = dt.Rows[0]["ExisistingCode"].ToString();
                    ddlEmpNo.SelectedValue = dt.Rows[0]["EmpNo"].ToString();
                    ddlEmpNo.DataSource = dt;
                    ddlEmpNo.DataTextField = "EmpNo";
                    ddlEmpNo.DataValueField = "EmpNo";
                    ddlEmpNo.DataBind();
                    ddToken.DataSource = dt;
                    ddToken.DataTextField = "ExisistingCode";
                    ddToken.DataValueField = "ExisistingCode";
                    ddToken.DataBind();
                    //ddToken.SelectedValue = dt.Rows[0]["ExisistingCode"].ToString();
                    //string salaryval = dt.Rows[0]["WagesType"].ToString();
                    string salaryval = dt.Rows[0]["WagesType"].ToString();
                    string EmpType = dt.Rows[0]["EmployeeType"].ToString();
                    //if (EmpType == "3")
                    //{
                        lblperday1.Text = dt.Rows[0]["DailySalary"].ToString();
                        lblhr1.Text = (Convert.ToDecimal(lblperday1.Text) / 8).ToString();
                        lblhr1.Text = Math.Round(Convert.ToDecimal(lblhr1.Text), 2, MidpointRounding.ToEven).ToString();
                    //}
                    //else if (EmpType == "4")
                    //{
                    //    lblperday1.Text = dt.Rows[0]["DailySalary"].ToString();
                    //    lblhr1.Text = (Convert.ToDecimal(lblperday1.Text) / 8).ToString();
                    //    lblhr1.Text = Math.Round(Convert.ToDecimal(lblhr1.Text), 2, MidpointRounding.ToEven).ToString();
                    //}
                    //else if (EmpType == "2")
                    //{
                    //    string Sum_1 = (Convert.ToDecimal(dt.Rows[0]["DailySalary"].ToString()) + Convert.ToDecimal(dt.Rows[0]["FDA"].ToString()) + Convert.ToDecimal(dt.Rows[0]["HRA"].ToString())).ToString();
                    //    string vda = (Convert.ToDecimal(dtpf.Rows[0]["VDA1"].ToString()) - Convert.ToDecimal(dtpf.Rows[0]["VDA2"].ToString())).ToString();

                    //    vda = (Convert.ToDecimal(vda) * Convert.ToDecimal(dt.Rows[0]["VDA"].ToString())).ToString();
                    //    Sum_1 = (Convert.ToDecimal(Sum_1) + Convert.ToDecimal(vda)).ToString();
                    //    lblperday1.Text = (Convert.ToDecimal(Sum_1) / 26).ToString();
                    //    lblperday1.Text = (Math.Round(Convert.ToDecimal(lblperday1.Text), 4, MidpointRounding.ToEven)).ToString();
                    //    lblhr1.Text = (Convert.ToDecimal(lblperday1.Text) / 8).ToString();
                    //    lblhr1.Text = Math.Round(Convert.ToDecimal(lblhr1.Text), 2, MidpointRounding.ToEven).ToString();
                    //}
                    //else
                    //{
                    //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('No OT for Staff');", true);

                    //}
                    //if (salaryval == "1")
                    //{
                    //    //lblbasic1.Text = dt.Rows[0]["DailySalary"].ToString();
                    //    lblperday1.Text = dt.Rows[0]["DailySalary"].ToString();
                    //    lblhr1.Text = (Convert.ToDecimal(lblperday1.Text) / 8).ToString();
                    //    lblhr1.Text = Math.Round(Convert.ToDecimal(lblhr1.Text), 2, MidpointRounding.ToEven).ToString();
                    //}
                    //else if (salaryval == "2")
                    //{
                    //    lblperday1.Text = dt.Rows[0]["DailySalary"].ToString();
                    //    lblhr1.Text = (Convert.ToDecimal(lblperday1.Text) / 8).ToString();
                    //    lblhr1.Text = Math.Round(Convert.ToDecimal(lblhr1.Text), 2, MidpointRounding.ToEven).ToString();
                    //}
                    //else if (salaryval == "3")
                    //{
                    //    lblperday1.Text = dt.Rows[0]["DailySalary"].ToString();
                    //    lblhr1.Text = (Convert.ToDecimal(lblperday1.Text) / 8).ToString();
                    //    lblhr1.Text = Math.Round(Convert.ToDecimal(lblhr1.Text), 2, MidpointRounding.ToEven).ToString();
                    //}

                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Employee Not Found');", true);
                }
                txtNet.Text = "";

            }
        }
        catch (Exception ex)
        {
        }
    }
    protected void ddlEmpNo_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            bool ErrFlag = false;

            if (ddldepartment.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Employee Department');", true);
                ErrFlag = true;
            }
            else if (ddlEmpNo.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Employee Employee Name');", true);
                ErrFlag = true;
            }
            else if (ddlEmpNo.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Employee Name');", true);
                ErrFlag = true;
            }
            if (!ErrFlag)
            {
                DataTable dtpf = new DataTable();
                dtpf = objdata.Load_pf_details(SessionCcode, SessionLcode);
                DataTable dt = new DataTable();
                dt = objdata.Overtime_Details(ddldepartment.SelectedValue, ddlEmpNo.SelectedValue, SessionCcode, SessionLcode);
                if (dt.Rows.Count > 0)
                {
                    //txtExist.Text = dt.Rows[0]["ExisistingCode"].ToString();
                    //ddlEmpName.SelectedValue = dt.Rows[0]["EmpNo"].ToString();
                    ddlEmpName.DataSource = dt;
                    ddlEmpName.DataTextField = "EmpName";
                    ddlEmpName.DataValueField = "EmpNo";
                    ddlEmpName.DataBind();
                    ddToken.DataSource = dt;
                    ddToken.DataTextField = "ExisistingCode";
                    ddToken.DataValueField = "ExisistingCode";
                    ddToken.DataBind();
                    //string salaryval = dt.Rows[0]["WagesType"].ToString();
                    string salaryval = dt.Rows[0]["WagesType"].ToString();
                    string EmpType = dt.Rows[0]["EmployeeType"].ToString();
                    //if (EmpType == "3")
                    //{
                        lblperday1.Text = dt.Rows[0]["DailySalary"].ToString();
                        lblhr1.Text = (Convert.ToDecimal(lblperday1.Text) / 8).ToString();
                        lblhr1.Text = Math.Round(Convert.ToDecimal(lblhr1.Text), 2, MidpointRounding.ToEven).ToString();
                    //}
                    //else if (EmpType == "4")
                    //{
                    //    lblperday1.Text = dt.Rows[0]["DailySalary"].ToString();
                    //    lblhr1.Text = (Convert.ToDecimal(lblperday1.Text) / 8).ToString();
                    //    lblhr1.Text = Math.Round(Convert.ToDecimal(lblhr1.Text), 2, MidpointRounding.ToEven).ToString();
                    //}
                    //else if (EmpType == "2")
                    //{
                    //    string Sum_1 = (Convert.ToDecimal(dt.Rows[0]["DailySalary"].ToString()) + Convert.ToDecimal(dt.Rows[0]["FDA"].ToString()) + Convert.ToDecimal(dt.Rows[0]["HRA"].ToString())).ToString();
                    //    string vda = (Convert.ToDecimal(dtpf.Rows[0]["VDA1"].ToString()) - Convert.ToDecimal(dtpf.Rows[0]["VDA2"].ToString())).ToString();

                    //    vda = (Convert.ToDecimal(vda) * Convert.ToDecimal(dt.Rows[0]["VDA"].ToString())).ToString();
                    //    Sum_1 = (Convert.ToDecimal(Sum_1) + Convert.ToDecimal(vda)).ToString();
                    //    lblperday1.Text = (Convert.ToDecimal(Sum_1) / 26).ToString();
                    //    lblperday1.Text = (Math.Round(Convert.ToDecimal(lblperday1.Text), 4, MidpointRounding.ToEven)).ToString();
                    //    lblhr1.Text = (Convert.ToDecimal(lblperday1.Text) / 8).ToString();
                    //    lblhr1.Text = Math.Round(Convert.ToDecimal(lblhr1.Text), 2, MidpointRounding.ToEven).ToString();
                    //}
                    //else
                    //{
                    //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('No OT for Staff');", true);

                    //}
                    //if (salaryval == "1")
                    //{
                    //    lblperday1.Text = dt.Rows[0]["DailySalary"].ToString();
                    //    lblhr1.Text = (Convert.ToDecimal(lblperday1.Text) / 8).ToString();
                    //    lblhr1.Text = Math.Round(Convert.ToDecimal(lblhr1.Text), 2, MidpointRounding.ToEven).ToString();
                    //}
                    //else if (salaryval == "2")
                    //{
                    //    lblperday1.Text = dt.Rows[0]["DailySalary"].ToString();
                    //    lblhr1.Text = (Convert.ToDecimal(lblperday1.Text) / 8).ToString();
                    //    lblhr1.Text = Math.Round(Convert.ToDecimal(lblhr1.Text), 2, MidpointRounding.ToEven).ToString();
                    //}
                    //else if (salaryval == "3")
                    //{
                    //    lblperday1.Text = dt.Rows[0]["DailySalary"].ToString();
                    //    lblhr1.Text = (Convert.ToDecimal(lblperday1.Text) / 8).ToString();
                    //    lblhr1.Text = Math.Round(Convert.ToDecimal(lblhr1.Text), 2, MidpointRounding.ToEven).ToString();
                    //}
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Employee Not Found');", true);
                }
                txtNet.Text = "";
            }
        }
        catch (Exception ex)
        {
        }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            bool ErrFlag = false;
            if (ddldepartment.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Employee Department');", true);
                ErrFlag = true;
            }
            else if (txtExist.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Employee Department');", true);
                ErrFlag = true;
            }
            if (!ErrFlag)
            {
                DataTable dtpf = new DataTable();
                dtpf = objdata.Load_pf_details(SessionCcode, SessionLcode);
                DataTable dt = new DataTable();
                dt = objdata.OverTime_Exist(ddldepartment.SelectedValue, txtExist.Text, SessionCcode, SessionLcode, SessionAdmin, rbsalary.SelectedValue);
                if (dt.Rows.Count > 0)
                {
                    //txtExist.Text = dt.Rows[0]["ExisistingCode"].ToString();
                    ddlEmpNo.SelectedItem.Text = dt.Rows[0]["EmpNo"].ToString();
                    ddlEmpName.SelectedItem.Text = dt.Rows[0]["EmpName"].ToString();
                    string salaryval = dt.Rows[0]["WagesType"].ToString();
                    string EmpType = dt.Rows[0]["EmployeeType"].ToString();
                    //if (EmpType == "3")
                    //{
                        lblperday1.Text = dt.Rows[0]["DailySalary"].ToString();
                        lblhr1.Text = (Convert.ToDecimal(lblperday1.Text) / 8).ToString();
                        lblhr1.Text = Math.Round(Convert.ToDecimal(lblhr1.Text), 2, MidpointRounding.ToEven).ToString();
                    //}
                    //else if (EmpType == "4")
                    //{
                    //    lblperday1.Text = dt.Rows[0]["DailySalary"].ToString();
                    //    lblhr1.Text = (Convert.ToDecimal(lblperday1.Text) / 8).ToString();
                    //    lblhr1.Text = Math.Round(Convert.ToDecimal(lblhr1.Text), 2, MidpointRounding.ToEven).ToString();
                    //}
                    //else if (EmpType == "2")
                    //{
                    //    string Sum_1 = (Convert.ToDecimal(dt.Rows[0]["DailySalary"].ToString()) + Convert.ToDecimal(dt.Rows[0]["FDA"].ToString()) + Convert.ToDecimal(dt.Rows[0]["HRA"].ToString())).ToString();
                    //    string vda = (Convert.ToDecimal(dtpf.Rows[0]["VDA1"].ToString()) - Convert.ToDecimal(dtpf.Rows[0]["VDA2"].ToString())).ToString();
                        
                    //    vda = (Convert.ToDecimal(vda) * Convert.ToDecimal(dt.Rows[0]["VDA"].ToString())).ToString();
                    //    Sum_1 = (Convert.ToDecimal(Sum_1) + Convert.ToDecimal(vda)).ToString();
                    //    lblperday1.Text = (Convert.ToDecimal(Sum_1) / 26).ToString();
                    //    lblperday1.Text = (Math.Round(Convert.ToDecimal(lblperday1.Text), 4, MidpointRounding.ToEven)).ToString();
                    //    lblhr1.Text = (Convert.ToDecimal(lblperday1.Text) / 8).ToString();
                    //    lblhr1.Text = Math.Round(Convert.ToDecimal(lblhr1.Text), 2, MidpointRounding.ToEven).ToString();
                    //}
                    //else
                    //{
                    //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('No OT for Staff');", true);

                    //}
                    //if (salaryval == "1")
                    //{
                        
                        
                    //}
                    //else if (salaryval == "2")
                    //{
                    //    lblperday1.Text = dt.Rows[0]["DailySalary"].ToString();
                    //    lblhr1.Text = (Convert.ToDecimal(lblperday1.Text) / 8).ToString();
                    //    lblhr1.Text = Math.Round(Convert.ToDecimal(lblhr1.Text), 2, MidpointRounding.ToEven).ToString();
                    //}
                    //else if (salaryval == "3")
                    //{
                    //    lblperday1.Text = dt.Rows[0]["DailySalary"].ToString();
                    //    lblhr1.Text = (Convert.ToDecimal(lblperday1.Text) / 8).ToString();
                    //    lblhr1.Text = Math.Round(Convert.ToDecimal(lblhr1.Text), 2, MidpointRounding.ToEven).ToString();
                    //}
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Employee Not Found');", true);
                }
                txtNet.Text = "";

            }
        }
        catch (Exception ex)
        {

        }
    }
    protected void txthrs_TextChanged(object sender, EventArgs e)
    {
        txtNet.Text = "";
    }
    protected void chkmanual_CheckedChanged(object sender, EventArgs e)
    {
        if (chkmanual.Checked == true)
        {
            txtrate.Enabled = true;
            txtrate.Text = "";
            txtNet.Text = "";
        }
        else
        {
            txtrate.Enabled = false;
            txtrate.Text = "";
            txtNet.Text = "";
        }
    }
    protected void txtrate_TextChanged(object sender, EventArgs e)
    {
        txtNet.Text = "";
    }
    protected void btncalculation_Click(object sender, EventArgs e)
    {
        try
        {
            bool ErrFlag = false;
            if (txthrs.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the No of Hours');", true);
                ErrFlag = true;
            }

            if (!ErrFlag)
            {
                if (chkmanual.Checked == true)
                {
                    if (txtrate.Text == "")
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Per Hour Salary');", true);
                        ErrFlag = true;
                    }
                    if (!ErrFlag)
                    {
                        txtNet.Text = (Convert.ToDecimal(txthrs.Text) * Convert.ToDecimal(txtrate.Text)).ToString();
                        txtNet.Text = Math.Round(Convert.ToDecimal(txtNet.Text), 0, MidpointRounding.AwayFromZero).ToString();
                    }
                }
                else
                {
                    if (lblhr1.Text.Trim() == "")
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Properly Employee');", true);
                        ErrFlag = true;
                    }
                    if (!ErrFlag)
                    {
                        txtNet.Text = (Convert.ToDecimal(txthrs.Text) * Convert.ToDecimal(lblhr1.Text)).ToString();
                        txtNet.Text = Math.Round(Convert.ToDecimal(txtNet.Text), 0, MidpointRounding.AwayFromZero).ToString();
                    }
                }
            }
        }
        catch
        {
        }
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            bool ErrFlag = false;
            bool SaveFlag = false;
            if (ddldepartment.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Department');", true);
                ErrFlag = true;
            }
            else if (ddldepartment.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Department');", true);
                ErrFlag = true;
            }
            else if (ddlMonths.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Month');", true);
                ErrFlag = true;
            }
            else if (ddlEmpName.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Employee Name');", true);
                ErrFlag = true;
            }
            else if (ddlEmpNo.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Employee No');", true);
                ErrFlag = true;
            }
            //else if (txtExist.Text.Trim() == "")
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Existing Code');", true);
            //    ErrFlag = true;
            //}
            else if (lblperday1.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Employee Details Properly');", true);
                ErrFlag = true;
            }
            else if (txtfrom.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the From Date');", true);
                ErrFlag = true;
            }
            else if (txtTo.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the To Date');", true);
                ErrFlag = true;
            }
            else if (chkmanual.Checked == true)
            {
                if (txtrate.Text.Trim() == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Per Hour Rate');", true);
                    ErrFlag = true;
                }
                else if (Convert.ToDecimal(txtrate.Text) == 0)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Per Hour Rate');", true);
                    ErrFlag = true;
                }
            }
            else if (txtNet.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Data Properly');", true);
                ErrFlag = true;
            }
            else if (ddlMonths.SelectedItem.Text == "-----Select----")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Months Properly');", true);
                ErrFlag = true;
            }
            if (!ErrFlag)
            {
                MyDate1 = DateTime.ParseExact(txtfrom.Text, "dd-MM-yyyy", null);
                MyDate2 = DateTime.ParseExact(txtTo.Text, "dd-MM-yyyy", null);
                string Exist_verify = objdata.OV_Exist_verify(ddlEmpNo.SelectedValue, ddldepartment.SelectedValue, SessionCcode, SessionLcode);
                string Emp_verify = objdata.Overtime_verify(ddlEmpNo.SelectedValue, ddldepartment.SelectedValue, ddlMonths.SelectedValue, ddlfinance.SelectedValue, SessionCcode, SessionLcode, MyDate1, MyDate2);
                if (Exist_verify.Trim() != txtExist.Text.Trim())
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Proper Employee Details');", true);
                    ErrFlag = true;
                }
                else if (Emp_verify.Trim() == ddlEmpNo.SelectedValue)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Data is already saved for this Month');", true);
                    ErrFlag = true;
                }
                if (!ErrFlag)
                {
                    objot.EmpNo = ddlEmpNo.SelectedValue;
                    objot.EmpName = ddlEmpName.SelectedItem.Text;
                    objot.department = ddldepartment.SelectedValue;
                    objot.Finance = ddlfinance.SelectedValue;
                    objot.Months = ddlMonths.SelectedValue;
                    objot.dailysalary = lblperday1.Text;
                    objot.NHrs = txthrs.Text.Trim();
                    if (chkmanual.Checked == true)
                    {
                        objot.chkmanual = "1";
                        objot.hrsalary = txtrate.Text.Trim();
                    }
                    else
                    {
                        objot.chkmanual = "0";
                        objot.hrsalary = lblhr1.Text.Trim();
                    }
                    objot.Netamt = txtNet.Text.Trim();
                    objot.Ccode = SessionCcode;
                    objot.Lcode = SessionLcode;
                   
                    objdata.OverTime_insert(objot, SessionCcode, SessionLcode, MyDate1, MyDate2);
                    SaveFlag = true;
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Data is not Saved');", true);
                }
                if (SaveFlag == true)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Data Saved Successfully');", true);
                    clr();
                }
            }
        }
        catch (Exception ex)
        {
        }
    }
    protected void btnclr_Click(object sender, EventArgs e)
    {
        clr();
    }
    protected void btnEmp_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptEmpDownload.aspx");
    }
    protected void btnatt_Click(object sender, EventArgs e)
    {
        Response.Redirect("AttenanceDownload.aspx");
    }
    protected void btnLeave_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptLeaveSample.aspx");
    }
    protected void btnOT_Click(object sender, EventArgs e)
    {
        Response.Redirect("OTDownload.aspx");
    }
    protected void txtTo_TextChanged(object sender, EventArgs e)
    {
        try
        {
            bool ErrFlag = false;
            if (txtfrom.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the From Date...');", true);
                ErrFlag = true;
            }

            if (!ErrFlag)
            {
                DateTime dfrom = Convert.ToDateTime(txtfrom.Text);
                DateTime dtto = Convert.ToDateTime(txtTo.Text);
                if (dfrom >= dtto)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Date Properly...');", true);
                    ErrFlag = true;
                    txtTo.Text = null;
                }
                if (!ErrFlag)
                {
                    MyDate1 = DateTime.ParseExact(txtfrom.Text, "dd-MM-yyyy", null);
                    MyDate2 = DateTime.ParseExact(txtTo.Text, "dd-MM-yyyy", null);
                    string Emp_verify = objdata.Overtime_verify(ddlEmpNo.SelectedValue, ddldepartment.SelectedValue, ddlMonths.SelectedValue, ddlfinance.SelectedValue, SessionCcode, SessionLcode, MyDate1, MyDate2);
                    if ((Emp_verify == null) || (Emp_verify == "")) 
                    {
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('OT Process has been already done for the selected Employee...');", true);
                        clr();
                    }
                }
            }
        }
        catch (Exception ex)
        {
        }
    }
    protected void rbsalary_SelectedIndexChanged(object sender, EventArgs e)
    {
        clr();
    }
    protected void txtfrom_TextChanged(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        txtTo.Text = null;
        int val_Month = Convert.ToDateTime(txtfrom.Text).Month;
        string Mont = "";
        switch (val_Month)
        {
            case 1:
                Mont = "January";
                break;

            case 2:
                Mont = "February";
                break;
            case 3:
                Mont = "March";
                break;
            case 4:
                Mont = "April";
                break;
            case 5:
                Mont = "May";
                break;
            case 6:
                Mont = "June";
                break;
            case 7:
                Mont = "July";
                break;
            case 8:
                Mont = "August";
                break;
            case 9:
                Mont = "September";
                break;
            case 10:
                Mont = "October";
                break;
            case 11:
                Mont = "November";
                break;
            case 12:
                Mont = "December";
                break;
            default:
                break;
        }
        if (Mont != ddlMonths.SelectedValue)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Date Properly...');", true);
            ErrFlag = true;
            txtfrom.Text = null;
        }
    }
    protected void btncontract_Click(object sender, EventArgs e)
    {
        Response.Redirect("ContractRPT.aspx");
    }
    protected void ddToken_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            bool ErrFlag = false;
            if (ddldepartment.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Employee Department');", true);
                ErrFlag = true;
            }
            //else if (txtExist.Text.Trim() == "")
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Employee Department');", true);
            //    ErrFlag = true;
            //}
            if (!ErrFlag)
            {
                DataTable dtpf = new DataTable();
                dtpf = objdata.Load_pf_details(SessionCcode, SessionLcode);
                DataTable dt = new DataTable();
                dt = objdata.OverTime_Exist(ddldepartment.SelectedValue, ddToken.SelectedValue, SessionCcode, SessionLcode, SessionAdmin, rbsalary.SelectedValue);
                if (dt.Rows.Count > 0)
                {
                    //txtExist.Text = dt.Rows[0]["ExisistingCode"].ToString();
                    //ddlEmpNo.SelectedItem.Text = dt.Rows[0]["EmpNo"].ToString();
                    //ddlEmpName.SelectedItem.Text = dt.Rows[0]["EmpName"].ToString();
                    ddlEmpNo.DataSource = dt;
                    ddlEmpNo.DataTextField = "EmpNo";
                    ddlEmpNo.DataValueField = "EmpNo";
                    ddlEmpNo.DataBind();
                    ddlEmpName.DataSource = dt;
                    ddlEmpName.DataTextField = "EmpName";
                    ddlEmpName.DataValueField = "EmpNo";
                    ddlEmpName.DataBind();
                    string salaryval = dt.Rows[0]["WagesType"].ToString();
                    string EmpType = dt.Rows[0]["EmployeeType"].ToString();
                    //if (EmpType == "3")
                    //{
                    lblperday1.Text = dt.Rows[0]["DailySalary"].ToString();
                    lblhr1.Text = (Convert.ToDecimal(lblperday1.Text) / 8).ToString();
                    lblhr1.Text = Math.Round(Convert.ToDecimal(lblhr1.Text), 2, MidpointRounding.ToEven).ToString();
                    //}
                    //else if (EmpType == "4")
                    //{
                    //    lblperday1.Text = dt.Rows[0]["DailySalary"].ToString();
                    //    lblhr1.Text = (Convert.ToDecimal(lblperday1.Text) / 8).ToString();
                    //    lblhr1.Text = Math.Round(Convert.ToDecimal(lblhr1.Text), 2, MidpointRounding.ToEven).ToString();
                    //}
                    //else if (EmpType == "2")
                    //{
                    //    string Sum_1 = (Convert.ToDecimal(dt.Rows[0]["DailySalary"].ToString()) + Convert.ToDecimal(dt.Rows[0]["FDA"].ToString()) + Convert.ToDecimal(dt.Rows[0]["HRA"].ToString())).ToString();
                    //    string vda = (Convert.ToDecimal(dtpf.Rows[0]["VDA1"].ToString()) - Convert.ToDecimal(dtpf.Rows[0]["VDA2"].ToString())).ToString();

                    //    vda = (Convert.ToDecimal(vda) * Convert.ToDecimal(dt.Rows[0]["VDA"].ToString())).ToString();
                    //    Sum_1 = (Convert.ToDecimal(Sum_1) + Convert.ToDecimal(vda)).ToString();
                    //    lblperday1.Text = (Convert.ToDecimal(Sum_1) / 26).ToString();
                    //    lblperday1.Text = (Math.Round(Convert.ToDecimal(lblperday1.Text), 4, MidpointRounding.ToEven)).ToString();
                    //    lblhr1.Text = (Convert.ToDecimal(lblperday1.Text) / 8).ToString();
                    //    lblhr1.Text = Math.Round(Convert.ToDecimal(lblhr1.Text), 2, MidpointRounding.ToEven).ToString();
                    //}
                    //else
                    //{
                    //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('No OT for Staff');", true);

                    //}
                    //if (salaryval == "1")
                    //{


                    //}
                    //else if (salaryval == "2")
                    //{
                    //    lblperday1.Text = dt.Rows[0]["DailySalary"].ToString();
                    //    lblhr1.Text = (Convert.ToDecimal(lblperday1.Text) / 8).ToString();
                    //    lblhr1.Text = Math.Round(Convert.ToDecimal(lblhr1.Text), 2, MidpointRounding.ToEven).ToString();
                    //}
                    //else if (salaryval == "3")
                    //{
                    //    lblperday1.Text = dt.Rows[0]["DailySalary"].ToString();
                    //    lblhr1.Text = (Convert.ToDecimal(lblperday1.Text) / 8).ToString();
                    //    lblhr1.Text = Math.Round(Convert.ToDecimal(lblhr1.Text), 2, MidpointRounding.ToEven).ToString();
                    //}
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Employee Not Found');", true);
                }
                txtNet.Text = "";

            }
        }
        catch (Exception ex)
        {

        }
    }
    protected void btnSal_Click(object sender, EventArgs e)
    {
        Response.Redirect("FrmDeductionDownload.aspx");
    }
}
