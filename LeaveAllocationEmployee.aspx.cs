﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Payroll;
using Payroll.Configuration;
using Payroll.Data;
using System.Data.SqlClient;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class LeaveAllocationEmployee_1 : System.Web.UI.Page
{
    protected System.Resources.ResourceManager rm;
    protected string PayrollRegisterContentMeta, pageTitle;
    BALDataAccess objdata = new BALDataAccess();
    string stafforLabour;
    static int d = 0;
    string SessionAdmin = "";
    string SessionCcode;
    string SessionLcode;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString != null)
        {
            //  PanelEditTextboxPanel.Visible = true;
        }
        
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        string ss = Session["UserId"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        lblComany.Text = SessionCcode + " - " + SessionLcode;
        if (!IsPostBack)
        {
            //Department();
            DropDwonCategory();
            int currentYear = Utility.GetFinancialYear;
            for (int i = 0; i < 10; i++)
            {
                ddlFinaYear.Items.Add(new ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                //  ddlShowYear.Items.Add(new ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                currentYear = currentYear - 1;
            }
            if (Request.QueryString["Edit"] != null)
            {
                PanelRegister.Visible = false;
                PanelEditRegister.Visible = true;
            }
        }
        lblusername.Text = Session["Usernmdisplay"].ToString();
    }

    public void DropDwonCategory()
    {
        DataTable dtcate = new DataTable();
        dtcate = objdata.DropDownCategory();
        ddlcategory.DataSource = dtcate;
        ddlcategory.DataTextField = "CategoryName";
        ddlcategory.DataValueField = "CategoryCd";
        ddlcategory.DataBind();
    }
    protected void rbtnlaburstaff_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlcategory.SelectedValue == "2")
        {

            PanelGrid.Visible = false;
            PanelGrouporIndividual.Visible = false;
            //PanelButtons.Visible = false;
            txtEmpNo.Text = "";
            txtLeaveAllocation.Text = "";
            txtleaveallocationMonth.Text = "";
        }
        else
        {


            PanelGrid.Visible = false;
            PanelGrouporIndividual.Visible = false;
            //PanelButtons.Visible = false;
            txtEmpNo.Text = "";
            txtLeaveAllocation.Text = "";
            txtleaveallocationMonth.Text = "";

        }
    }
    public void Department()
    {
        DataTable dtDip = new DataTable();
        dtDip = objdata.DropDownDepartment();
        ddlDepartment.DataSource = dtDip;
        ddlDepartment.DataTextField = "DepartmentNm";
        ddlDepartment.DataValueField = "DepartmentCd";
        ddlDepartment.DataBind();
    }
    public void EmployeeDetails_Load()
    {

        if (ddlcategory.SelectedValue == "1")
        {
            stafforLabour = "S";
        }
        else if (ddlcategory.SelectedValue == "2")
        {
            stafforLabour = "L";
        }
        if (Request.QueryString["Edit"] != null)
        {
            DataTable dt1 = new DataTable();
            if (SessionAdmin == "1")
            {
                dt1 = objdata.LeaveAllocationEmployeeForEdit(stafforLabour, ddlDepartment.SelectedValue, SessionCcode, SessionLcode);
            }
            else
            {
                dt1 = objdata.LeaveAllocationEmployeeForEdit_user(stafforLabour, ddlDepartment.SelectedValue, SessionCcode, SessionLcode);
            }
            DataGridView1.DataSource = dt1;
            DataGridView1.DataBind();
            PanelReterviewRecod.Visible = true;
        }
        else
        {
            DataTable dt1 = new DataTable();
            if (ddlcategory.SelectedValue == "1")
            {
                if (SessionAdmin == "1")
                {
                    dt1 = objdata.LeaveAllocationEmployee(stafforLabour, ddlDepartment.SelectedValue, SessionCcode, SessionLcode);
                }
                else if (SessionAdmin == "2")
                {
                    dt1 = objdata.LeaveAllocation_User(stafforLabour, ddlDepartment.SelectedValue, SessionCcode, SessionLcode);
                }
            }
            else
            {
                if (SessionAdmin == "1")
                {
                    dt1 = objdata.LeaveAllocationLabour(stafforLabour, ddlDepartment.SelectedValue, SessionCcode, SessionLcode);
                }
                else if (SessionAdmin == "2")
                {
                    dt1 = objdata.LeaveAllocationLabour_user(stafforLabour, ddlDepartment.SelectedValue, SessionCcode, SessionLcode);
                }
            }
            DataGridView1.DataSource = dt1;
            DataGridView1.DataBind();
            PanelReterviewRecod.Visible = false;
        }
    }
    protected void btnclick_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        txtdate.Text = "";
        //panelError.Visible = true;
        txtLeaveAllocation.Text = "";
        txtleaveallocationMonth.Text = "";
        if (ddlcategory.SelectedValue == "0")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select Category Staff or Labour!');", true);
            //System.Windows.Forms.MessageBox.Show("Select Category Staff or Labour!", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            //lblError.Text = "Select the Category";
            ErrFlag = true;
        }
        if (ddlDepartment.SelectedValue == "0")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select Department!');", true);
            //System.Windows.Forms.MessageBox.Show("Select Department!", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            //lblError.Text = "Select the Department";
            //lblError.Focus();
            ErrFlag = true;
        }
        if (!ErrFlag)
        {
            lblError.Text = "";
            PanelGrid.Visible = true;
            //PanelButtons.Visible = true;
            EmployeeDetails_Load();
            PanelGrouporIndividual.Visible = false;
            DataTable dt = new DataTable();
            dt = objdata.LeaveAllocation_default(ddlDepartment.SelectedValue, SessionCcode, SessionLcode);
            if (dt.Rows.Count > 0)
            {
                txtLeaveAllocation.Text = dt.Rows[0]["AllocatedLeave"].ToString();
                txtleaveallocationMonth.Text = dt.Rows[0]["Monthlyleave"].ToString();
            }
            else
            {
                txtLeaveAllocation.Text = "";
                txtleaveallocationMonth.Text = "";
            }
        }

    }
    protected void btnsearch_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        //panelError.Visible = true;
        if (ddlcategory.SelectedValue == "1")
        {
            stafforLabour = "S";
        }
        else if (ddlcategory.SelectedValue == "2")
        {
            stafforLabour = "L";
        }
        
        if (!ErrFlag)
        {
            lblError.Text = "";
            if (txtexistingno.Text != "")
            {
                PanelGrid.Visible = true;
                DataTable DT1 = new DataTable();
                DT1 = objdata.EmployeeVerifyforLeaveAllocationForExistingNo(txtexistingno.Text, ddlDepartment.SelectedValue, stafforLabour, SessionCcode, SessionLcode);
                DataGridView1.DataSource = DT1;
                DataGridView1.DataBind();
            }
            else if (txtEmpNo.Text != "")
            {
                PanelGrid.Visible = true;
                DataTable DT1 = new DataTable();
                DT1 = objdata.EmployeeVerifyforLeaveAllocation(txtEmpNo.Text, ddlDepartment.SelectedValue, stafforLabour, SessionCcode, SessionLcode);
                DataGridView1.DataSource = DT1;
                DataGridView1.DataBind();
            }
            else if (txtEmpNo.Text != "" && txtexistingno.Text != "")
            {

                PanelGrid.Visible = true;
                DataTable DT1 = new DataTable();
                DT1 = objdata.EmployeeVerifyforLeaveAllocExistingNoandEmpNo(txtexistingno.Text, txtEmpNo.Text, ddlDepartment.SelectedValue, stafforLabour, SessionCcode, SessionLcode);
                DataGridView1.DataSource = DT1;
                DataGridView1.DataBind();

            }
        }
    }
    //protected void ddlDepartment_TextChanged(object sender, EventArgs e)
    //{
    //    if (ddlDepartment.SelectedValue == "0")
    //    {
    //        PanelGrid.Visible = false;
    //        PanelButtons.Visible = false;
    //        PanelGrouporIndividual.Visible = false;
    //    }
    //    else
    //    {
    //        PanelGrid.Visible = true;
    //        PanelButtons.Visible = true;
    //        EmployeeDetails_Load();
    //        PanelGrouporIndividual.Visible = false;
    //    }

    //}

    protected void DataGridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        DataGridView1.EditIndex = -1;
        EmployeeDetails_Load();
    }
    protected void DataGridView1_RowEditing(object sender, GridViewEditEventArgs e)
    {
        DataGridView1.EditIndex = e.NewEditIndex;
        EmployeeDetails_Load();
    }
    protected void DataGridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {

    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            //panelError.Visible = true;
            LeaveAllocation objla = new LeaveAllocation();
            bool ErrFlag = false;
            string Months = "";
            ArrayList MonthArray = new ArrayList();
            bool Valuechk = false;
            string carry = "";
            if (chkcarry.Checked == true)
            {
                carry = "1";
            }
            else
            {
                carry = "0";
            }
            if (ddlDepartment.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Please Select Department...!');", true);
                //System.Windows.Forms.MessageBox.Show("Please Select Department...!", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                //lblError.Text = "Select the Department";
                ErrFlag = true;
            }
            else if (ddlDepartment.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Please Select Department...!');", true);
                //System.Windows.Forms.MessageBox.Show("Please Select Department...!", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                //lblError.Text = "Select the Department";
                ErrFlag = true;
            }
            else if (PanelGrid.Visible == false)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Please Select Department...!');", true);
                //System.Windows.Forms.MessageBox.Show("Please Select Department...!", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                //lblError.Text = "Select the Department";
                ErrFlag = true;
            }
            else if (DataGridView1.Rows.Count <= 0)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Please Select Department...!');", true);
                //System.Windows.Forms.MessageBox.Show("Please Select Department...!", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                //lblError.Text = "Select the Department";
                ErrFlag = true;
            }
            else if (txtLeaveAllocation.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Enter the Eligible Leave Days...!');", true);
                //System.Windows.Forms.MessageBox.Show("Enter the Eligible Leave Days...!", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                //lblError.Text = "Enter the Eligible Leave Days";
                ErrFlag = true;
            }
            else if (txtdate.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select Leave Allocation From Month');", true);
                //System.Windows.Forms.MessageBox.Show("Select Leave Allocation From Month", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                //lblError.Text = "Select the Leave Allocation from Month";
                ErrFlag = true;
            }
            if (!ErrFlag)
            {
                bool UpdateFlag = false;
                bool InsertFlag = false;
                foreach (GridViewRow gvrow in DataGridView1.Rows)
                {

                    CheckBox check_box = (CheckBox)gvrow.FindControl("chkClear");
                    if (check_box != null)
                    {
                        lblError.Text = "";
                        if (check_box.Checked)
                        {
                            Valuechk = true;
                            if (ddlcategory.SelectedValue == "1")
                            {
                                stafforLabour = "S";
                            }
                            else if (ddlcategory.SelectedValue == "2")
                            {
                                stafforLabour = "L";
                            }

                            Label lblstaffid = (Label)gvrow.FindControl("lblStaffID");
                            Label lbldesignation = (Label)gvrow.FindControl("lbldesignation");
                            //string EmpCode = objdata.EmployeeLeaveallocationupdate(lblstaffid.Text);

                            objla.EmpNo = lblstaffid.Text;
                            objla.Department = ddlDepartment.SelectedValue;
                            objla.LeaveDays = txtLeaveAllocation.Text;
                            objla.StafforLabour = stafforLabour;
                            objla.Designation = lbldesignation.Text;
                            objla.FinYear = ddlFinaYear.SelectedValue;
                            objla.LeaveAllForMonth = txtleaveallocationMonth.Text;
                            if (Request.QueryString["Edit"] != null)
                            {
                                //objdata.UpdateLeaveAllocation(objla);
                                //UpdateFlag = true;
                                //EmployeeDetails_Load();

                                DateTime m;
                                m = DateTime.ParseExact(txtdate.Text, "dd-MM-yyyy", null);
                                d = m.Month;
                                int mon = m.Month;
                                int yr = m.Year;
                                string[] Monthssqq = new string[20];

                                if (d == 1)
                                {
                                    MonthArray.Add("January"); MonthArray.Add("February"); MonthArray.Add("March");
                                }
                                else if (d == 2)
                                {
                                    MonthArray.Add("February"); MonthArray.Add("March");
                                }
                                else if (d == 3) { MonthArray.Add("March"); }
                                else if (d == 4)
                                {
                                    MonthArray.Add("April"); MonthArray.Add("May"); MonthArray.Add("June"); MonthArray.Add("July"); MonthArray.Add("August");
                                    MonthArray.Add("Septemeber"); MonthArray.Add("October"); MonthArray.Add("November"); MonthArray.Add("December");
                                    MonthArray.Add("January"); MonthArray.Add("February"); MonthArray.Add("March");
                                }
                                else if (d == 5)
                                {
                                    MonthArray.Add("May"); MonthArray.Add("June"); MonthArray.Add("July"); MonthArray.Add("August"); MonthArray.Add("Septemeber");
                                    MonthArray.Add("October"); MonthArray.Add("November"); MonthArray.Add("December"); MonthArray.Add("January"); MonthArray.Add("February"); MonthArray.Add("March");
                                }
                                else if (d == 6)
                                {
                                    MonthArray.Add("June"); MonthArray.Add("July"); MonthArray.Add("August"); MonthArray.Add("Septemeber"); MonthArray.Add("October");
                                    MonthArray.Add("November"); MonthArray.Add("December"); MonthArray.Add("January"); MonthArray.Add("February"); MonthArray.Add("March");
                                }
                                else if (d == 7) { MonthArray.Add("July"); MonthArray.Add("August"); MonthArray.Add("Septemeber"); MonthArray.Add("October"); MonthArray.Add("November"); MonthArray.Add("December"); MonthArray.Add("January"); MonthArray.Add("February"); MonthArray.Add("March"); }
                                else if (d == 8) { MonthArray.Add("August"); MonthArray.Add("Septemeber"); MonthArray.Add("October"); MonthArray.Add("November"); MonthArray.Add("December"); MonthArray.Add("January"); MonthArray.Add("February"); MonthArray.Add("March"); }
                                else if (d == 9) { MonthArray.Add("Septemeber"); MonthArray.Add("October"); MonthArray.Add("November"); MonthArray.Add("December"); MonthArray.Add("January"); MonthArray.Add("February"); MonthArray.Add("March"); }
                                else if (d == 10)
                                {
                                    MonthArray.Add("October"); MonthArray.Add("November"); MonthArray.Add("December");
                                    MonthArray.Add("January"); MonthArray.Add("February"); MonthArray.Add("March");
                                }

                                else if (d == 11) { MonthArray.Add("November"); MonthArray.Add("December"); MonthArray.Add("January"); MonthArray.Add("February"); MonthArray.Add("March"); }
                                else if (d == 12) { MonthArray.Add("December"); MonthArray.Add("January"); MonthArray.Add("February"); MonthArray.Add("March"); }

                                // ArrayList MonthArray = new ArrayList(Monthssqq.Length);
                                // MonthArray.AddRange(Monthssqq);
                                for (int i = 0; i < MonthArray.Count; i++)
                                {

                                    if (i == 0)
                                    {
                                        string constr = ConfigurationManager.AppSettings["ConnectionString"];

                                        string Updateqry = "Update LeaveAllocation set " + MonthArray[i] + " = " + txtleaveallocationMonth.Text + ", AllocatedLeave='" + txtLeaveAllocation.Text + "', CreateDate = convert(DateTime,'" + txtdate.Text + "', 105),Monthlyleave = '" + txtleaveallocationMonth.Text + "', Carry='" + carry + "' Where EmpNo='" + lblstaffid.Text + "' and FinancialYear='" + ddlFinaYear.SelectedValue + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode  + "'"; SqlConnection con = new SqlConnection(constr);
                                        //string Updateqry = "Update LeaveAllocation set " + MonthArray[i] + " = " + txtleaveallocationMonth.Text + " , MonthlyLeave='" + txtleaveallocationMonth.Text + "' Where EmpNo='" + lblstaffid.Text + "' and FinancialYear='" + ddlFinaYear.SelectedValue + "' ";
                                        SqlCommand cmd = new SqlCommand(Updateqry, con);
                                        con.Open();
                                        cmd.ExecuteNonQuery();
                                        UpdateFlag = true;
                                        con.Close();
                                    }
                                    else
                                    {
                                        string constr = ConfigurationManager.AppSettings["ConnectionString"];
                                        SqlConnection con = new SqlConnection(constr);
                                        string Updateqry = "Update LeaveAllocation set " + MonthArray[i] + " = " + txtleaveallocationMonth.Text + ", AllocatedLeave='" + txtLeaveAllocation.Text + "', CreateDate = convert(DateTime,'" + txtdate.Text + "', 105),Monthlyleave = '" + txtleaveallocationMonth.Text + "',Carry='" + carry + "' Where EmpNo='" + lblstaffid.Text + "' and FinancialYear='" + ddlFinaYear.SelectedValue + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                                        //string Updateqry = "Update LeaveAllocation set " + MonthArray[i] + " = " + txtleaveallocationMonth.Text + "  Where EmpNo='" + lblstaffid.Text + "' and FinancialYear='" + ddlFinaYear.SelectedValue + "' ";
                                        SqlCommand cmd = new SqlCommand(Updateqry, con);
                                        con.Open();
                                        cmd.ExecuteNonQuery();
                                        UpdateFlag = true;
                                        con.Close();
                                        string StatusUpdate = "Update EmployeeDetails set IsLeave='Y' Where EmpNo='" + lblstaffid.Text + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";

                                        SqlCommand cmdReg = new SqlCommand(StatusUpdate, con);
                                        con.Open();
                                        cmdReg.ExecuteNonQuery();
                                        con.Close();
                                    }
                                }

                            }
                            else
                            {
                                DateTime m;
                                m = DateTime.ParseExact(txtdate.Text, "dd-MM-yyyy", null);
                                d = m.Month;
                                int mon = m.Month;
                                int yr = m.Year;
                                string[] Monthssqq = new string[20];

                                if (d == 1)
                                {
                                    MonthArray.Add("January"); MonthArray.Add("February"); MonthArray.Add("March");
                                }
                                else if (d == 2)
                                {
                                    MonthArray.Add("February"); MonthArray.Add("March");
                                }
                                else if (d == 3) { MonthArray.Add("March"); }
                                else if (d == 4)
                                {
                                    MonthArray.Add("April"); MonthArray.Add("May"); MonthArray.Add("June"); MonthArray.Add("July"); MonthArray.Add("August");
                                    MonthArray.Add("Septemeber"); MonthArray.Add("October"); MonthArray.Add("November"); MonthArray.Add("December");
                                    MonthArray.Add("January"); MonthArray.Add("February"); MonthArray.Add("March");
                                }
                                else if (d == 5)
                                {
                                    MonthArray.Add("May"); MonthArray.Add("June"); MonthArray.Add("July"); MonthArray.Add("August"); MonthArray.Add("Septemeber");
                                    MonthArray.Add("October"); MonthArray.Add("November"); MonthArray.Add("December"); MonthArray.Add("January"); MonthArray.Add("February"); MonthArray.Add("March");
                                }
                                else if (d == 6)
                                {
                                    MonthArray.Add("June"); MonthArray.Add("July"); MonthArray.Add("August"); MonthArray.Add("Septemeber"); MonthArray.Add("October");
                                    MonthArray.Add("November"); MonthArray.Add("December"); MonthArray.Add("January"); MonthArray.Add("February"); MonthArray.Add("March");
                                }
                                else if (d == 7) { MonthArray.Add("July"); MonthArray.Add("August"); MonthArray.Add("Septemeber"); MonthArray.Add("October"); MonthArray.Add("November"); MonthArray.Add("December"); MonthArray.Add("January"); MonthArray.Add("February"); MonthArray.Add("March"); }
                                else if (d == 8) { MonthArray.Add("August"); MonthArray.Add("Septemeber"); MonthArray.Add("October"); MonthArray.Add("November"); MonthArray.Add("December"); MonthArray.Add("January"); MonthArray.Add("February"); MonthArray.Add("March"); }
                                else if (d == 9) { MonthArray.Add("Septemeber"); MonthArray.Add("October"); MonthArray.Add("November"); MonthArray.Add("December"); MonthArray.Add("January"); MonthArray.Add("February"); MonthArray.Add("March"); }
                                else if (d == 10)
                                {
                                    MonthArray.Add("October"); MonthArray.Add("November"); MonthArray.Add("December");
                                    MonthArray.Add("January"); MonthArray.Add("February"); MonthArray.Add("March");
                                }

                                else if (d == 11) { MonthArray.Add("November"); MonthArray.Add("December"); MonthArray.Add("January"); MonthArray.Add("February"); MonthArray.Add("March"); }
                                else if (d == 12) { MonthArray.Add("December"); MonthArray.Add("January"); MonthArray.Add("February"); MonthArray.Add("March"); }

                                // ArrayList MonthArray = new ArrayList(Monthssqq.Length);
                                // MonthArray.AddRange(Monthssqq);
                                for (int i = 0; i < MonthArray.Count; i++)
                                {

                                    if (i == 0)
                                    {
                                        //  objdata.InsertLeaveAllocation(objla);
                                        string constr = ConfigurationManager.AppSettings["ConnectionString"];
                                        SqlConnection con = new SqlConnection(constr);
                                        string InsertQry = "Insert into  LeaveAllocation (EmpNo,AllocatedLeave,Department,StafforLabour,FinancialYear,MonthlyLeave,CreateDate, " +
                                                           " " + MonthArray[i] + ",Ccode,Lcode,Carry ) Values('" + lblstaffid.Text + "','" + txtLeaveAllocation.Text + "', " +
                                                           " '" + ddlDepartment.SelectedValue + "', '" + stafforLabour + "','" + ddlFinaYear.SelectedValue + "','" + txtleaveallocationMonth.Text + "',convert(datetime,'" + txtdate.Text + "',105), " +
                                                           " '" + txtleaveallocationMonth.Text + "','" + SessionCcode + "','" + SessionLcode + "','" + carry + "')";
                                        SqlCommand cmd = new SqlCommand(InsertQry, con);
                                        con.Open();
                                        cmd.ExecuteNonQuery();
                                        InsertFlag = true;
                                        con.Close();
                                    }
                                    else
                                    {
                                        string constr = ConfigurationManager.AppSettings["ConnectionString"];
                                        SqlConnection con = new SqlConnection(constr);
                                        string Updateqry = "Update LeaveAllocation set " + MonthArray[i] + " = " + txtleaveallocationMonth.Text + ", AllocatedLeave='" + txtLeaveAllocation.Text + "', CreateDate = convert(DateTime,'" + txtdate.Text + "', 105),Carry='" + carry + "' Where EmpNo='" + lblstaffid.Text + "' and FinancialYear='" + ddlFinaYear.SelectedValue + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                                        SqlCommand cmd = new SqlCommand(Updateqry, con);
                                        con.Open();
                                        cmd.ExecuteNonQuery();
                                        InsertFlag = true;
                                        con.Close();
                                        string StatusUpdate = "Update EmployeeDetails set IsLeave='Y' Where EmpNo='" + lblstaffid.Text + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";

                                        SqlCommand cmdReg = new SqlCommand(StatusUpdate, con);
                                        con.Open();
                                        cmdReg.ExecuteNonQuery();
                                        con.Close();
                                    }
                                }

                                //objdata.InsertLeaveAllocation(objla);
                                //InsertFlag = true;
                                EmployeeDetails_Load();
                            }

                        }
                    }

                }
                if (InsertFlag == true)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Saved Successfully...!');", true);
                    //System.Windows.Forms.MessageBox.Show("Saved Successfully...!", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    lblError.Text = "";
                    ////lbloutput.Text = "Saved Successfully Successfully";
                    ////panelsuccess.Visible = true;
                    //btnclick.Enabled = false;
                    //btnsearch.Enabled = false;
                    //btnSave.Enabled = false;
                    //btnEdit.Enabled = false;
                    //btnreset.Enabled = false;
                    //btncancel.Enabled = false;
                    //btnreterivew.Enabled = false;
                    ////btnok.Focus();
                    txtLeaveAllocation.Text = "";
                    txtleaveallocationMonth.Text = "";
                    txtdate.Text = "";
                }
                if (UpdateFlag == true)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Updated Successfully...!');", true);
                    //System.Windows.Forms.MessageBox.Show("Updated Successfully...!", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    txtLeaveAllocation.Text = "";
                    txtleaveallocationMonth.Text = "";
                    txtdate.Text = "";
                    //lblError.Text = "";
                    ////lbloutput.Text = "Updated Successfully Successfully";
                    ////panelsuccess.Visible = true;
                    //btnclick.Enabled = false;
                    //btnsearch.Enabled = false;
                    //btnSave.Enabled = false;
                    //btnEdit.Enabled = false;
                    //btnreset.Enabled = false;
                    //btncancel.Enabled = false;
                    //btnreterivew.Enabled = false;
                    ////btnok.Focus();
                }
                if (Valuechk == false)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select the Employee...!');", true);
                    //System.Windows.Forms.MessageBox.Show("Select the Employee...!", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    //lblError.Text = "Select the Employee";
                }
            }
        }
        catch (Exception ex)
        { }
    }
    protected void btnIndividual_Click(object sender, EventArgs e)
    {
        PanelGrouporIndividual.Visible = true;
        PanelGrid.Visible = false;
        txtEmpNo.Text = "";
        txtLeaveAllocation.Text = "";
        txtleaveallocationMonth.Text = "";
    }
    protected void btnall_Click(object sender, EventArgs e)
    {
        PanelGrouporIndividual.Visible = false;
        PanelGrid.Visible = true;
        EmployeeDetails_Load();
        txtEmpNo.Text = "";
        txtLeaveAllocation.Text = "";
        txtleaveallocationMonth.Text = "";
    }

    protected void btnEdit_Click(object sender, EventArgs e)
    {
        Response.Redirect("LeaveAllocationEmployee.aspx?Edit=edit");
    }
    protected void ddlDepartment_SelectedIndexChanged(object sender, EventArgs e)
    {
        txtEmpNo.Text = "";
        txtLeaveAllocation.Text = "";
        txtleaveallocationMonth.Text = "";
    }
    protected void btncancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("LeaveAllocationEmployee.aspx");
    }
    protected void btnreset_Click(object sender, EventArgs e)
    {
        lblError.Text = "";
        txtEmpNo.Text = "";
        txtLeaveAllocation.Text = "";
        txtleaveallocationMonth.Text = "";
        chkcarry.Checked = true;
    }

    protected void btnreterivew_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        //panelError.Visible = true;
        foreach (GridViewRow gvrow in DataGridView1.Rows)
        {
            CheckBox check_box = (CheckBox)gvrow.FindControl("chkClear");
            if (check_box != null)
            {
                if (check_box.Checked)
                {
                    ErrFlag = true;
                }
            }
        }
        if (ErrFlag == true)
        {
            foreach (GridViewRow gvrow in DataGridView1.Rows)
            {

                CheckBox check_box = (CheckBox)gvrow.FindControl("chkClear");
                if (check_box != null)
                {
                    if (check_box.Checked)
                    {
                        Label lblstaffid = (Label)gvrow.FindControl("lblStaffID");
                        DataTable dtobjda = new DataTable();
                        dtobjda = objdata.EmployeeLeaveAllocForReterivew(lblstaffid.Text, SessionCcode, SessionLcode);
                        if (dtobjda.Rows.Count > 0)
                        {
                            txtLeaveAllocation.Text = dtobjda.Rows[0]["AllocatedLeave"].ToString();
                            txtleaveallocationMonth.Text = dtobjda.Rows[0]["Monthlyleave"].ToString();
                            txtdate.Text = dtobjda.Rows[0]["CreatedDate"].ToString();
                            if (dtobjda.Rows[0]["Carry"].ToString() == "1")
                            {
                                chkcarry.Checked = true;
                            }
                            else if (dtobjda.Rows[0]["Carry"].ToString() == "0")
                            {
                                chkcarry.Checked = false;
                            }
                        }

                    }
                }
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select the Employee...!');", true);
            
        }

    }
    protected void ddlcategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        lblError.Text = "";
        string Stafflabour_temp;
        //Clear();
        DataTable dempty = new DataTable();
        DataGridView1.DataSource = dempty;
        DataGridView1.DataBind();
        if (ddlcategory.SelectedValue == "1")
        {
            Stafflabour_temp = "S";
        }
        else if (ddlcategory.SelectedValue == "2")
        {
            Stafflabour_temp = "L";
        }
        else
        {
            Stafflabour_temp = "0";
        }
        DataTable dtDip = new DataTable();
        dtDip = objdata.DropDownDepartment_category(Stafflabour_temp);
        if (dtDip.Rows.Count > 1)
        {
            ddlDepartment.DataSource = dtDip;
            ddlDepartment.DataTextField = "DepartmentNm";
            ddlDepartment.DataValueField = "DepartmentCd";
            ddlDepartment.DataBind();
        }
        else
        {
            DataTable dt = new DataTable();
            ddlDepartment.DataSource = dt;
            ddlDepartment.DataBind();
        }
        txtdate.Text = "";
        txtLeaveAllocation.Text = "";
        txtleaveallocationMonth.Text = "";
    }
    protected void ddlDepartment_SelectedIndexChanged1(object sender, EventArgs e)
    {
        //Clear();
        lblError.Text = "";
        DataTable dempty = new DataTable();
        DataGridView1.DataSource = dempty;
        DataGridView1.DataBind();
        txtdate.Text = "";
        txtLeaveAllocation.Text = "";
        txtleaveallocationMonth.Text = "";

    }
    protected void btninitializeyr_Click(object sender, EventArgs e)
    {

    }
    protected void btnok_Click(object sender, EventArgs e)
    {
        panelsuccess.Visible = false;
        btnclick.Enabled = true;
        btnsearch.Enabled = true;
        btnSave.Enabled = true;
        btnEdit.Enabled = true;
        btnreset.Enabled = true;
        btncancel.Enabled = true;
        btnreterivew.Enabled = true;
    }
}
