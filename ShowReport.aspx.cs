﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Web.Security;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Specialized;


using Microsoft.Reporting.WebForms;

public partial class ShowReport : System.Web.UI.Page
{

    string SessionAdmin;
     protected void Page_Load(object sender, EventArgs e)
    {
        SessionAdmin = Session["Isadmin"].ToString();
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        string ss = Session["UserId"].ToString();
        if (!IsPostBack)
        {
            displayPayrollReport();
        }

    }


    private void displayPayrollReport()
    {
        
        string rpt;
        if (Request.QueryString["rpt"] != null)
        {
            rpt = Request.QueryString["rpt"].ToString();
        }
        else
        {

            rpt = ReportsValues.PaySlipForMonth;

            SalaryslipFormonthly_Report();
            //ReportsValues.ReportName;
           
        }

        switch (rpt)
        {
            case "rptpayslip": // 1 DCBill 
                {
                    SalaryslipFormonthly_Report(); break;
                }
            //case "dcbill_anx1_ds": // 2 DCBill Annexure 1 - for Day Scholar
            //    {
            //        DCBillAnnexure1_DS_Report(); break;
            //    }
            //case "dcbill_anx1_hs": // 3 DCBill Annexure 1 - for Hostel Scholar
            //    {
            //        DCBillAnnexure1_HS_Report(); break;
            //    }
            
          
            
            
           
            default:
                {
                    NoReports();
                    break;
                }
        }
    }

    private void NoReports()
    {
    }


    private void setReportCredentials(string rptName)
    {
        //PMSReports.ShowParameterPrompts = false;
        //PMSReports.ShowCredentialPrompts = false;
        //PMSReports.ProcessingMode = ProcessingMode.Remote; 
        ////ScholarReports.ServerReport.ReportServerCredentials = new ReportCredentials("SWOReports", "", "");
        //PMSReports.ServerReport.ReportServerUrl = new Uri(ConfigurationManager.AppSettings["reportsUrl"]);
        //PMSReports.ServerReport.ReportPath = ConfigurationManager.AppSettings["reportsDir"] + rptName;
        
    }

    private void SalaryslipFormonthly_Report()
    {
        try
        {
            //string EmpNo="Emp";
            //string Month = "July";
            
            //ReportParameter[] param = new ReportParameter[2];
            //param[0] = new ReportParameter("EmpNo", EmpNo);
            //param[1] = new ReportParameter("Month", Month);

            //setReportCredentials("rptSalaryslipformonth");
            //PMSReports.ServerReport.SetParameters(param);
            //PMSReports.ServerReport.Refresh();
        }
        catch (Exception ex)
        {
            int ErrCode = 0;
            //ErrCode = ScholarDBUtilities.SaveError(ex);
            //FF.Utilities.CreateMessageAlert(this.Page, "Contact Admin..!,Error Number is '" + ErrCode + "'", " strkey2");
        }
    }


}
