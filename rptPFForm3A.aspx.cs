﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class rptPFForm3A : System.Web.UI.Page
{
    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    ReportDocument rd = new ReportDocument();
    BALDataAccess objdata = new BALDataAccess();
    SqlConnection con;
    string Finance;
    string category;
    string StaffLabour;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string Finance1;
    string CmpName;
    string Cmpaddress;
    string EmpNo;
    protected void Page_Load(object sender, EventArgs e)
    {
        SqlConnection con = new SqlConnection(constr);
        SessionAdmin = Session["Isadmin"].ToString();
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        string ss = Session["UserId"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();

        Finance = Request.QueryString["yr"].ToString();
        category = Request.QueryString["Category"].ToString();
        EmpNo = Request.QueryString["EmpNo"].ToString();
        DataTable dt = new DataTable();
        dt = objdata.Company_retrive(SessionCcode, SessionLcode);
        if (dt.Rows.Count > 0)
        {
            CmpName = dt.Rows[0]["Cname"].ToString();
            Cmpaddress = (dt.Rows[0]["Address1"].ToString() + ", " + dt.Rows[0]["Address2"].ToString() + ", " + dt.Rows[0]["Location"].ToString() + "-" + dt.Rows[0]["Pincode"].ToString());
        }
        if (category == "1")
        {
            StaffLabour = "S";
        }
        else if (category == "2")
        {
            StaffLabour = "L";
        }
        string query = "Select ED.EmpNo,ED.EmpName,ED.FatherName,OP.PFnumber,SD.Month,SD.PfSalary,SD.FinancialYear,SD.Year,SD.ProvidentFund,Round(((SD.PfSalary * 3.67)/100),0) as A,Round((((SD.PfSalary) * 8.33)/100), 0) as B from " +
                        " EmployeeDetails ED inner Join OfficialProfile OP on OP.EmpNo=ED.EmpNo inner Join SalaryDetails SD on SD.EmpNo=ED.EmpNo " +
                        " where ED.EmpNo='" + EmpNo + "' and ED.StafforLabor='" + StaffLabour + "' and ED.Ccode='" + SessionCcode + "' and ED.Lcode='" + SessionLcode + "' and OP.Ccode='" + SessionCcode + "' and " +
                        " OP.Lcode='" + SessionLcode + "'and SD.Ccode='" + SessionCcode + "' and SD.Lcode='" + SessionLcode + "' and SD.FinancialYear='" + Finance + "' Order By SD.SalaryDate Asc";
        SqlCommand cmd = new SqlCommand(query, con);
        SqlDataAdapter sda = new SqlDataAdapter(cmd);
        DataSet ds1 = new DataSet();
        con.Open();
        sda.Fill(ds1);
        DataTable dt_1 = new DataTable();
        sda.Fill(dt_1);
        con.Close();
        if (ds1.Tables[0].Rows.Count > 0)
        {
            rd.Load(Server.MapPath("Form3A.rpt"));
            rd.SetDataSource(ds1.Tables[0]);
            rd.DataDefinition.FormulaFields["CompanyName"].Text = "'" + CmpName + "'";
            rd.DataDefinition.FormulaFields["Address"].Text = "'" + Cmpaddress + "'";
            Finance1 = (Convert.ToInt32(Finance) + 1).ToString();
            rd.DataDefinition.FormulaFields["MonthDetails"].Text = "'" + "1 st April " + Finance + " to 31 st March " + Finance1 + "'";
            
            CrystalReportViewer1.ReportSource = rd;
            CrystalReportViewer1.RefreshReport();
            CrystalReportViewer1.DataBind();
        }
    }
}
