﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;
using Payroll;
using System.Drawing;
using iTextSharp.text.pdf;
using iTextSharp.text.html;
using iTextSharp.text.html.simpleparser;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class PFForm5_1 : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    string SessionAdmin;
    string Stafflabour;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        string ss = Session["UserId"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();
        if (!IsPostBack)
        {
            DropDwonCategory();
            Months_load();
            int currentYear = Utility.GetFinancialYear;
            for (int i = 0; i < 10; i++)
            {
                ddlFinance.Items.Add(new ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                //  ddlShowYear.Items.Add(new ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                currentYear = currentYear - 1;
            }
        }
        lblusername.Text = Session["Usernmdisplay"].ToString();
    }
    public void DropDwonCategory()
    {
        DataTable dtcate = new DataTable();
        dtcate = objdata.DropDownCategory();
        ddlCategory.DataSource = dtcate;
        ddlCategory.DataTextField = "CategoryName";
        ddlCategory.DataValueField = "CategoryCd";
        ddlCategory.DataBind();
    }
    public void Months_load()
    {
        DataTable dt = new DataTable();
        dt = objdata.months_load();
        ddlMonths.DataSource = dt;
        ddlMonths.DataTextField = "Months";
        ddlMonths.DataValueField = "Months";
        ddlMonths.DataBind();
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        DataTable dtempty = new DataTable();
        gvForm5.DataSource = dtempty;
        gvForm5.DataBind();
        bool ErrFlag = false;
        bool download = false;
        if (ddlCategory.SelectedValue == "0")
        {
            //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "script", "showalert('Select the Category.');", true);
            ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.GetType(), "window", "alert('Select the Category.');",true);

            //System.Windows.Forms.MessageBox.Show("Select the Category", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            ErrFlag = true;
        }
        else if (ddldpt.SelectedValue == "0")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Department.');", true);

            //System.Windows.Forms.MessageBox.Show("Select the Department", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            ErrFlag = true;
        }
        else if (ddlMonths.SelectedValue == "0")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Months.');", true);

            //System.Windows.Forms.MessageBox.Show("Select the Months", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            ErrFlag = true;
        }
        if (!ErrFlag)
        {
            if (ddlCategory.SelectedValue == "1")
            {
                Stafflabour = "S";
            }
            else if (ddlCategory.SelectedValue == "2")
            {
                Stafflabour = "L";
            }
            DataTable dt = new DataTable();
            if (SessionAdmin == "1")
            {
                dt = objdata.PF_form5(ddldpt.SelectedValue, Stafflabour);
            }
            else
            {
                dt = objdata.PF_form5_user(ddldpt.SelectedValue, Stafflabour);

            }
            gvForm5.DataSource = dt;
            gvForm5.DataBind();
            if (gvForm5.Rows.Count > 0)
            {
                int yr = 0;
                if ((ddlMonths.SelectedValue == "January") || (ddlMonths.SelectedValue == "feburary") || (ddlMonths.SelectedValue == "March"))
                {
                    yr = Convert.ToInt32(ddlFinance.SelectedValue);
                    yr = yr + 1;
                }
                else
                {
                    yr = Convert.ToInt32(ddlFinance.SelectedValue);
                }
                string attachment = "attachment; filename=PFForm5.xls";
                Response.ClearContent();
                Response.AddHeader("content-disposition", attachment);
                Response.ContentType = "application/ms-excel";

                StringWriter stw = new StringWriter();
                HtmlTextWriter htextw = new HtmlTextWriter(stw);
                gvForm5.RenderControl(htextw);
                //HtmlTextWriter.Write("FORM 5");
                Response.Write("<table>");
                Response.Write("<tr align='Center'>");
                Response.Write("<td colspan='9'>");
                Response.Write("FORM 5");
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("<tr align='Center'>");
                Response.Write("<td colspan='9'>");
                Response.Write("THE EMPLOYEES PROVIDENT FUND SCHEME, 1952 (PARAGRAPH 36 [2] [a]) AND THE EMPLOYEES PENSION SCHEME 1995 (PARA 20 [4])");
                Response.Write("Returns of Employees qualifying for membership of the Employees Provident Fund, Employees Pension Fund & Employees deposit");
                Response.Write("linked insurance Fund for the first time during the month of " + ddlMonths.SelectedValue + "-" + ddlFinance.SelectedValue);
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("<tr>");
                Response.Write("<td colspan='9'>");
                Response.Write("To be sent to the Commissioner with Form 2 (EPS & EPS)");
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("<tr>");
                Response.Write("<td colspan='9'>");
                Response.Write("NAME & ADDRESS OF THE FACTORY :");
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("<tr>");
                Response.Write("<td colspan='9'>");
                Response.Write("CODE NUMBER OF FACTORY/ESTABLISHMENT : ");
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("<tr>");

                Response.Write("</tr>");
                Response.Write("</table>");
                Response.Write(stw.ToString());
                Response.Write("<table>");
                Response.Write("<tr>");

                Response.Write("</tr>");
                Response.Write("<tr>");

                Response.Write("</tr>");
                Response.Write("<tr align='right'>");
                Response.Write("<td colspan='9'>");
                Response.Write("Signature of the Employer or the other authorised officer and stamp of the Factory / Establishment ");
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("<tr align='left'>");
                Response.Write("<td colspan='9'>");
                Response.Write("Note : Please furnish the details of membership in remark column if the employee was a member of Empoyees' Provident Fund and Employees' Family Pension Scheme Before joining yourself/factory. i.e. Account No and/ or the name and particulars of Last Employer.");
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("</table>");
                download = true;
                Response.End();
                download = true;
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('No Data Found.');", true);

                //System.Windows.Forms.MessageBox.Show("No Data Found", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            }
            if (download == true)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Exported Successfully in the name PFForm5.xls');", true);

                //System.Windows.Forms.MessageBox.Show("Exported Successfully", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            }
        }
    }
    protected void ddlFinance_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void btnExport_Click(object sender, EventArgs e)
    {
        if (gvForm5.Rows.Count > 0)
        {
            int yr = Convert.ToInt32(ddlFinance.SelectedValue);

            string attachment = "attachment; filename=PFForm5.xls";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/ms-excel";

            StringWriter stw = new StringWriter();
            HtmlTextWriter htextw = new HtmlTextWriter(stw);
            gvForm5.RenderControl(htextw);
            //HtmlTextWriter.Write("FORM 5");
            Response.Write("<table>");
            Response.Write("<tr align='Center'>");
            Response.Write("<td colspan='8'>");
            Response.Write("FORM 5");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("<tr align='Center'>");
            Response.Write("<td colspan='8'>");
            Response.Write("THE EMPLOYEES PROVIDENT FUND SCHEME, 1952 (PARAGRAPH 36 [2] [a]) AND THE EMPLOYEES PENSION SCHEME 1995 (PARA 20 [4])");
            Response.Write("Returns of Employees qualifying for membership of the Employees Provident Fund, Employees Pension Fund & Employees deposit");
            Response.Write("linked insurance Fund for the first time during the month of " + ddlMonths.SelectedValue + "-" + ddlFinance.SelectedValue);
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("<tr>");
            Response.Write("<td colspan='8'>");
            Response.Write("To be sent to the Commissioner with Form 2 (EPS & EPS)");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("<tr>");
            Response.Write("<td colspan='8'>");
            Response.Write("NAME & ADDRESS OF THE FACTORY :");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("<tr>");
            Response.Write("<td colspan='8'>");
            Response.Write("CODE NUMBER OF FACTORY/ESTABLISHMENT :");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write(stw.ToString());
            Response.Write("</table>");
            Response.End();

        }
    }
    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }
    protected void ddlCategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable dtempty = new DataTable();
        ddldpt.DataSource = dtempty;
        ddldpt.DataBind();
        gvForm5.DataSource = dtempty;
        gvForm5.DataBind();
        if (ddlCategory.SelectedValue == "1")
        {
            Stafflabour = "S";
        }
        else if (ddlCategory.SelectedValue == "2")
        {
            Stafflabour = "L";
        }
        else
        {
            Stafflabour = "0";
        }
        DataTable dtDip = new DataTable();
        dtDip = objdata.DropDownDepartment_category(Stafflabour);
        if (dtDip.Rows.Count > 1)
        {
            ddldpt.DataSource = dtDip;
            ddldpt.DataTextField = "DepartmentNm";
            ddldpt.DataValueField = "DepartmentCd";
            ddldpt.DataBind();
        }
        else
        {
            DataTable dt = new DataTable();
            ddldpt.DataSource = dtempty;
            ddldpt.DataBind();
        }
    }
    protected void ddldpt_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable dtempty = new DataTable();
        gvForm5.DataSource = dtempty;
        gvForm5.DataBind();
    }
}
