﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Data.SqlClient;
using System.Configuration;



public partial class FrmSalarySlab : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    ContractClass objcon = new ContractClass();
    SalarySlab objslab = new SalarySlab();
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    static string Cont_code = "";
    static string lblprobation_Common = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        string ss = Session["UserId"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();
        if (!IsPostBack)
        {
            Contract_load();

        }
        SessionAdmin = Session["Isadmin"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        lblusername.Text = Session["Usernmdisplay"].ToString();
        lblComany.Text = SessionCcode + " - " + SessionLcode;
    }
    protected void btncontract_Click(object sender, EventArgs e)
    {
        Response.Redirect("ContractRPT.aspx");
    }
    protected void btnEmp_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptEmpDownload.aspx");
    }
    protected void btnatt_Click(object sender, EventArgs e)
    {
        Response.Redirect("AttenanceDownload.aspx");
    }
    protected void btnOT_Click(object sender, EventArgs e)
    {
        Response.Redirect("OTDownload.aspx");
    }
    protected void btnSal_Click(object sender, EventArgs e)
    {
        Response.Redirect("FrmDeductionDownload.aspx");
    }
    protected void btnLeave_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptLeaveSample.aspx");
    }
    private void Contract_load()
    {
        DataTable dt_empty = new DataTable();
        ddlscheme.DataSource = dt_empty;
        ddlscheme.DataBind();
        DataTable dt = new DataTable();
        dt = objdata.Contract_Scheme();
        ddlscheme.DataSource = dt;        
        ddlscheme.DataTextField = "ContractName";
        ddlscheme.DataValueField = "ContractCode";
        ddlscheme.DataBind();
    }
    private void SalarySlab_Load()
    {
        DataTable dt_Empty = new DataTable();
        gvScheme.DataSource = dt_Empty;
        gvScheme.DataBind();
        DataTable dt = new DataTable();
        dt = objdata.SalarySlab_Load(ddlscheme.SelectedValue);
        gvScheme.DataSource = dt;
        gvScheme.DataBind();
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            bool ErrFlag = false;
            bool SaveFlag = false;
            if (ddlscheme.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Contract Name....');", true);
                ErrFlag = true;
            }
            else if (txtSalary.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Salry Amount....');", true);
                ErrFlag = true;
            }
            else if (txtSlab.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Salry Amount....');", true);
                ErrFlag = true;
            }
            if (!ErrFlag)
            {
                string SalarySlab = objdata.Salaryslab_Verify(txtSlab.Text.Trim(), ddlscheme.SelectedValue);
                if (SalarySlab.Trim() == "")
                {
                    string ID_val = objdata.SalarySlab_ID(ddlscheme.SelectedValue);
                    int ID = 1;
                    if (ID_val == "")
                    {
                        ID = 1;
                    }
                    else
                    {
                        ID = (Convert.ToInt32(ID_val) + 1);
                    }
                    objslab.ID = ID.ToString();
                    objslab.SchmemeName = ddlscheme.SelectedValue;
                    objslab.SlabName = txtSlab.Text.Trim();
                    objslab.Amount = txtSalary.Text.Trim();
                    objdata.SalarySlab_Insert(objslab);
                    SaveFlag = true;
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Salary Slab already Exist....');", true);
                }
                if (SaveFlag == true)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Saved Successfully....');", true);
                    SalarySlab_Load();
                    txtSalary.Text = "";
                    txtSlab.Text = "";
                }
            }
        }
        catch (Exception ex)
        {
        }
    }
    protected void ddlscheme_SelectedIndexChanged(object sender, EventArgs e)
    {
        SalarySlab_Load();
    }
    protected void gvScheme_RowEditing(object sender, GridViewEditEventArgs e)
    {
        gvScheme.EditIndex = e.NewEditIndex;
        SalarySlab_Load();
    }
    protected void gvScheme_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void gvScheme_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        try
        {
            //objDep.CompanyCd = ddlcompany.SelectedValue;
            bool ErrFlag = false;
            TextBox txtAmt12 = (TextBox)gvScheme.Rows[e.RowIndex].FindControl("txtAmount1");
            if (txtAmt12.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Amount');", true);
                ErrFlag = true;
            }

            if (!ErrFlag)
            {
                Label lblID = (Label)gvScheme.Rows[e.RowIndex].FindControl("lblID");
                Label lblsalaryslab = (Label)gvScheme.Rows[e.RowIndex].FindControl("lblSlab");
                Label lblScheme = (Label)gvScheme.Rows[e.RowIndex].FindControl("lblScheme");
                //Label lblsalaryslab = (Label)gvScheme.Rows[e.RowIndex].FindControl("lbldepartmentcd");
                //string dptname = objdata.DepartmentName_check(txtdepartmentcd1.Text);                
                if (!ErrFlag)
                {
                    //objDep.DepartmentCd = lbldepartmentcd1.Text;
                    //objDep.DepartmentNm = txtdepartmentcd1.Text;

                    //objdata.UpdateMstDepartment(objDep);
                    objdata.SalarySlab_Update(ddlscheme.SelectedValue, txtAmt12.Text, lblID.Text, lblsalaryslab.Text);
                    gvScheme.EditIndex = -1;
                    SalarySlab_Load();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Amount Update Successfully');", true);
                    //System.Windows.Forms.MessageBox.Show("Department Name Updated Successfully", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                }
            }

        }
        catch (Exception ex)
        {

        }
    }
    protected void gvScheme_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        gvScheme.EditIndex = -1;
        SalarySlab_Load();
    }
    protected void btnClear_Click(object sender, EventArgs e)
    {
        txtSalary.Text = "";
        txtSlab.Text = "";
        SalarySlab_Load();
    }
}
