﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;
using Payroll;
using System.Drawing;
using iTextSharp.text.pdf;
using iTextSharp.text.html;
using iTextSharp.text.html.simpleparser;
using Altius.BusinessAccessLayer.BALDataAccess;
using Excel = Microsoft.Office.Interop.Excel;
using System.Reflection;
using System.Runtime.InteropServices;

public partial class PFForm3Anew_1 : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    string SessionAdmin;
    string Stafflabour;
    string path;
    string SessionCcode;
    string SessionLcode;
    int finance;
    decimal pfsal;
    decimal EPF;
    decimal EPF1;
    decimal A;
    decimal B;
    decimal EPF_total;
    protected void Page_Load(object sender, EventArgs e)
    {
        
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        string ss = Session["UserId"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        lblComany.Text = SessionCcode + " - " + SessionLcode;
        if (!IsPostBack)
        {
            DropDwonCategory();
            int currentYear = Utility.GetFinancialYear;
            for (int i = 0; i < 10; i++)
            {
                ddlfinance.Items.Add(new ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                //  ddlShowYear.Items.Add(new ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                currentYear = currentYear - 1;
            }
        }
        lblusername.Text = Session["Usernmdisplay"].ToString();
    }
    public void DropDwonCategory()
    {
        DataTable dtcate = new DataTable();
        dtcate = objdata.DropDownCategory();
        ddlCategory.DataSource = dtcate;
        ddlCategory.DataTextField = "CategoryName";
        ddlCategory.DataValueField = "CategoryCd";
        ddlCategory.DataBind();
    }

    protected void ddlCategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable dtempty = new DataTable();
        ddlEmpType.DataSource = dtempty;
        ddlEmpType.DataBind();
        //ddlEmpNo.DataSource = dtempty;
        //ddlEmpNo.DataBind();
        //ddlEmpname.DataSource = dtempty;
        //ddlEmpname.DataBind();

        if (ddlCategory.SelectedValue == "1")
        {
            Stafflabour = "S";
        }
        else if (ddlCategory.SelectedValue == "2")
        {
            Stafflabour = "L";
        }
        else
        {
            Stafflabour = "0";
        }
        EmployeeType();
    }
    public void EmployeeType()
    {
        DataTable dtemp = new DataTable();
        dtemp = objdata.EmployeeDropDown_no(ddlCategory.SelectedValue);
        ddlEmpType.DataSource = dtemp;
        ddlEmpType.DataTextField = "EmpType";
        ddlEmpType.DataValueField = "EmpTypeCd";
        ddlEmpType.DataBind();
    }
    protected void ddldpt_SelectedIndexChanged(object sender, EventArgs e)
    {
        //DataTable dtempty = new DataTable();
        //ddlEmpno.DataSource = dtempty;
        //ddlEmpno.DataBind();
        //ddlempname.DataSource = dtempty;
        //ddlempname.DataBind();
        //gvform3A.DataSource = dtempty;
        //gvform3A.DataBind();
    }
   
   
    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }

    protected void btnexport_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        bool saveFlag = false;
        //bool download = false;
        if (ddlCategory.SelectedValue == "0")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Category...!');", true);

            //System.Windows.Forms.MessageBox.Show("Select the Category", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            ErrFlag = true;
        }
        else if (ddlEmpType.SelectedValue == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Employee Type...!');", true);
            ErrFlag = true;
        }
        else if (ddlEmpType.SelectedValue == "0")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Employee Type...!');", true);
            ErrFlag = true;
        }
        //if (!ErrFlag)
        //{
        //    ResponseHelper.Redirect("rptPFForm3A.aspx?Category=" + ddlCategory.SelectedValue + "&yr=" + ddlfinance.SelectedValue + "&EmpNo=" + ddlEmpNo.SelectedValue, "_blank", "");
        //}
        //else if (ddldpt.SelectedValue == "0")
        //{
        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Department....!');", true);

        //    //System.Windows.Forms.MessageBox.Show("Select the Department", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
        //    ErrFlag = true;
        //}
        //else if (ddlEmpno.SelectedValue == "")
        //{
        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Employee Number....!');", true);

        //    //System.Windows.Forms.MessageBox.Show("Select the Employee Number", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
        //    ErrFlag = true;
        //}
        if (!ErrFlag)
        {
            if (ddlCategory.SelectedValue == "1")
            {
                Stafflabour = "S";
            }
            else if (ddlCategory.SelectedValue == "2")
            {
                Stafflabour = "L";
            }
            DataTable dt_EmpLoad = new DataTable();
            dt_EmpLoad = objdata.PF_EmpLoad(SessionCcode, SessionLcode, ddlEmpType.SelectedValue);
            for (int k = 0; k < dt_EmpLoad.Rows.Count; k++)
            {


                DataTable dt_val = new DataTable();
                dt_val = objdata.PF_Load(dt_EmpLoad.Rows[k]["EmpNo"].ToString(), Stafflabour, SessionCcode, SessionLcode, ddlfinance.SelectedValue);
                DataTable dt_march = new DataTable();
                string Finance = (Convert.ToInt32(ddlfinance.SelectedValue) - 1).ToString();
                dt_march = objdata.PF_Load1(dt_EmpLoad.Rows[k]["EmpNo"].ToString(), Stafflabour, SessionCcode, SessionLcode, Finance);

                if (dt_march.Rows.Count > 0)
                {
                    DataRow dr = dt_val.NewRow();
                    dr[0] = dt_march.Rows[0]["EmpNo"].ToString();
                    dr[1] = dt_march.Rows[0]["EmpName"].ToString();
                    dr[2] = dt_march.Rows[0]["FatherName"].ToString();
                    dr[3] = dt_march.Rows[0]["PFnumber"].ToString();
                    dr[4] = dt_march.Rows[0]["Month"].ToString();
                    dr[5] = dt_march.Rows[0]["PfSalary"].ToString();
                    dr[6] = dt_march.Rows[0]["FinancialYear"].ToString();
                    dr[7] = dt_march.Rows[0]["Year"].ToString();
                    dr[8] = dt_march.Rows[0]["ProvidentFund"].ToString();
                    dr[9] = dt_march.Rows[0]["A"].ToString();
                    dr[10] = dt_march.Rows[0]["B"].ToString();
                    dt_val.Rows.InsertAt(dr, 0);
                }

                DataTable dt_add = new DataTable();
                dt_add = objdata.Company_retrive(SessionCcode, SessionLcode);
                DataTable dt_emp = new DataTable();
                dt_emp = objdata.Form3A_Load(dt_EmpLoad.Rows[k]["EmpNo"].ToString(), SessionCcode, SessionLcode);

                DataTable dt = new DataTable();
                dt = objdata.Form3A_sp(dt_EmpLoad.Rows[k]["EmpNo"].ToString(), ddlfinance.SelectedValue);
                Excel.Application xlapp;
                Excel.Workbook xlWorkBook;
                Excel.Worksheet xlWorkSheet;
                object misValue = System.Reflection.Missing.Value;
                Excel.Range chartrange;

                xlapp = new Excel.ApplicationClass();
                xlWorkBook = xlapp.Workbooks.Add(misValue);
                xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
                //xlWorkSheet.get_Range("A1", "H1").Merge;
                xlWorkSheet.Cells[1, 1] = "(For Unexempted Establishments Only)";
                chartrange = xlWorkSheet.get_Range("A1", "H1");
                xlWorkSheet.get_Range("A1", "H1").Merge(true);
                //chartrange.Font.Bold = true;
                xlWorkSheet.Cells[1, 9] = "(Form 3 - A Revised)";
                chartrange = xlWorkSheet.get_Range("I1", "J1");
                xlWorkSheet.get_Range("I1", "J1").Merge(true);
                xlWorkSheet.Cells[2, 1] = "THE EMPLOYEES' PROVIDENT FUND SCHEME, 1952";
                chartrange = xlWorkSheet.get_Range("A2", "J2");
                xlWorkSheet.get_Range("A2", "J2").Merge(true);
                chartrange.Font.Size = "14";
                chartrange.Font.Bold = true;
                chartrange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
                xlWorkSheet.Cells[3, 1] = "(Paras 35 & 42 and ";
                chartrange = xlWorkSheet.get_Range("A3", "J3");
                xlWorkSheet.get_Range("A3", "J3").Merge(true);
                chartrange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;

                xlWorkSheet.Cells[4, 1] = "Employee's Pension Scheme 1995";
                chartrange = xlWorkSheet.get_Range("A4", "J4");
                xlWorkSheet.get_Range("A4", "J4").Merge(true);
                chartrange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;

                xlWorkSheet.Cells[5, 1] = "(Para 19))";
                chartrange = xlWorkSheet.get_Range("A5", "J5");
                xlWorkSheet.get_Range("A5", "J5").Merge(true);
                chartrange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;

                xlWorkSheet.Cells[6, 1] = "Contribution Card for the Currency Period From 1st April " + ddlfinance.SelectedValue + " to 21 March " + (Convert.ToDecimal(ddlfinance.SelectedValue) + 1).ToString();
                chartrange = xlWorkSheet.get_Range("A6", "J6");
                xlWorkSheet.get_Range("A6", "J6").Merge(true);
                //chartrange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;

                xlWorkSheet.Cells[7, 1] = "";

                xlWorkSheet.Cells[8, 1] = "1. Account No. TN/";
                chartrange = xlWorkSheet.get_Range("A8", "J8");
                xlWorkSheet.get_Range("A8", "B8").Merge(true);

                xlWorkSheet.Cells[8, 3] = "";

                xlWorkSheet.Cells[8, 4] = ": " + dt_emp.Rows[0]["PFnumber"].ToString();
                xlWorkSheet.Cells[8, 8] = "4. Name & Address of the Establishment";
                xlWorkSheet.get_Range("H8", "J8").Merge(true);
                xlWorkSheet.Cells[9, 8] = "M/s." + dt_add.Rows[0]["CName"].ToString();
                xlWorkSheet.get_Range("H9", "J9").Merge(true);
                chartrange = xlWorkSheet.get_Range("H9", "J9");

                chartrange.Font.Bold = true;

                xlWorkSheet.Cells[10, 8] = dt_add.Rows[0]["Address1"].ToString();
                xlWorkSheet.get_Range("H10", "J10").Merge(true);
                xlWorkSheet.Cells[11, 8] = dt_add.Rows[0]["Address2"].ToString();
                xlWorkSheet.get_Range("H11", "J11").Merge(true);
                xlWorkSheet.Cells[12, 8] = dt_add.Rows[0]["Location"].ToString() + "-" + dt_add.Rows[0]["Pincode"].ToString();
                xlWorkSheet.get_Range("H12", "J12").Merge(true);

                xlWorkSheet.Cells[11, 1] = "2. Name/ Surname";

                xlWorkSheet.Cells[11, 4] = ":" + dt_emp.Rows[0]["EmpName"].ToString();

                xlWorkSheet.Cells[13, 1] = "3. Father's/ Husband's Name";

                xlWorkSheet.Cells[13, 4] = ":" + dt_emp.Rows[0]["FatherName"].ToString();

                xlWorkSheet.Cells[12, 8] = "5. Statutory rate of contribution   12%";

                xlWorkSheet.Cells[13, 8] = "6. Voluntary higher rate of employees ";

                xlWorkSheet.Cells[14, 8] = "contrbution if any ………………………..";
                //xlWorkSheet.get_Range("A16", "A18").Rows.Merge(true);

                chartrange = xlWorkSheet.get_Range("A16", "A18");
                chartrange.Merge(true);
                xlWorkSheet.Cells[16, 1] = "Month";
                chartrange.WrapText = true;
                chartrange.Font.Bold = true;
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                xlWorkSheet.Cells[16, 2] = "Amount of Wages";
                chartrange = xlWorkSheet.get_Range("B16", "B18");
                xlWorkSheet.get_Range("B16", "B18").Merge(true);
                chartrange.WrapText = true;
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                chartrange = xlWorkSheet.get_Range("C16", "G16");
                xlWorkSheet.Cells[16, 3] = "Contribution";
                xlWorkSheet.get_Range("C16", "G16").Merge(true);
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                chartrange = xlWorkSheet.get_Range("C17", "D17");
                xlWorkSheet.Cells[17, 3] = "Worker's Share";
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                chartrange.Merge(true);
                chartrange = xlWorkSheet.get_Range("C18", "D18");
                xlWorkSheet.Cells[18, 3] = "EPF";
                xlWorkSheet.Cells[18, 4] = "Total";
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                chartrange = xlWorkSheet.get_Range("E17", "G17");
                xlWorkSheet.get_Range("E17", "G17").Merge(true);
                xlWorkSheet.Cells[17, 5] = "Employer's Share";
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                chartrange = xlWorkSheet.get_Range("E18", "G18");
                xlWorkSheet.Cells[18, 5] = "EPF";
                xlWorkSheet.Cells[18, 6] = "EPS";
                xlWorkSheet.Cells[18, 7] = "Total";
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                chartrange = xlWorkSheet.get_Range("H16", "H18");
                chartrange.WrapText = true;
                xlWorkSheet.Cells[16, 8] = "* Refund of Advance";
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                chartrange = xlWorkSheet.get_Range("I16", "I18");
                chartrange.WrapText = true;
                xlWorkSheet.Cells[16, 9] = "Break in membership reckonable Service From to";
                chartrange.Merge(true);
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                chartrange = xlWorkSheet.get_Range("J16", "J18");
                chartrange.WrapText = true;
                xlWorkSheet.Cells[16, 10] = "Remarks";
                chartrange.Merge(true);
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                chartrange = xlWorkSheet.get_Range("A19", "A19");
                xlWorkSheet.Cells[19, 1] = "1";
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                chartrange = xlWorkSheet.get_Range("B19", "B19");
                xlWorkSheet.Cells[19, 2] = "2";
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                chartrange = xlWorkSheet.get_Range("C19", "D19");
                xlWorkSheet.Cells[19, 3] = "3";
                chartrange.Merge(true);
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                chartrange = xlWorkSheet.get_Range("E19", "G19");
                xlWorkSheet.Cells[19, 5] = "4";
                chartrange.Merge(true);
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                chartrange = xlWorkSheet.get_Range("H19", "H19");
                xlWorkSheet.Cells[19, 8] = "5";
                chartrange.Merge(true);
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                chartrange = xlWorkSheet.get_Range("I19", "I19");
                xlWorkSheet.Cells[19, 9] = "6";
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                chartrange = xlWorkSheet.get_Range("J19", "J19");
                xlWorkSheet.Cells[19, 10] = "7";
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                //March Zero Value
                chartrange = xlWorkSheet.get_Range("A20", "A20");
                finance = (Convert.ToInt32(ddlfinance.SelectedValue));
                xlWorkSheet.Cells[20, 1] = "March " + finance.ToString();
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                chartrange = xlWorkSheet.get_Range("B20", "B20");
                xlWorkSheet.Cells[20, 2] = "0.00";
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                chartrange = xlWorkSheet.get_Range("C20", "C20");
                xlWorkSheet.Cells[20, 3] = "0";
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                chartrange = xlWorkSheet.get_Range("D20", "D20");
                xlWorkSheet.Cells[20, 4] = "0";
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                chartrange = xlWorkSheet.get_Range("E20", "E20");
                xlWorkSheet.Cells[20, 5] = "0";
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                chartrange = xlWorkSheet.get_Range("F20", "F20");
                xlWorkSheet.Cells[20, 6] = "0";
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                chartrange = xlWorkSheet.get_Range("G20", "G20");
                xlWorkSheet.Cells[20, 7] = "0";
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                chartrange = xlWorkSheet.get_Range("H20", "H20");
                xlWorkSheet.Cells[20, 8] = "";
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                chartrange = xlWorkSheet.get_Range("I20", "I20");
                xlWorkSheet.Cells[20, 9] = "";
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                chartrange = xlWorkSheet.get_Range("J20", "J20");
                xlWorkSheet.Cells[20, 10] = "";
                chartrange.WrapText = true;
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                //April NullValue
                chartrange = xlWorkSheet.get_Range("A21", "A21");
                xlWorkSheet.Cells[21, 1] = "April " + ddlfinance.SelectedValue;
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                chartrange = xlWorkSheet.get_Range("B21", "B21");
                xlWorkSheet.Cells[21, 2] = "0.00";
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                chartrange = xlWorkSheet.get_Range("C21", "C21");
                xlWorkSheet.Cells[21, 3] = "0";
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                chartrange = xlWorkSheet.get_Range("D21", "D21");
                xlWorkSheet.Cells[21, 4] = "0";
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                chartrange = xlWorkSheet.get_Range("E21", "E21");
                xlWorkSheet.Cells[21, 5] = "0";
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                chartrange = xlWorkSheet.get_Range("F21", "F21");
                xlWorkSheet.Cells[21, 6] = "0";
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                chartrange = xlWorkSheet.get_Range("G21", "G21");
                xlWorkSheet.Cells[21, 7] = "0";
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                chartrange = xlWorkSheet.get_Range("H21", "H21");
                xlWorkSheet.Cells[21, 8] = "";
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                chartrange = xlWorkSheet.get_Range("I21", "I21");
                xlWorkSheet.Cells[21, 9] = "";
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                chartrange = xlWorkSheet.get_Range("J21", "J21");
                xlWorkSheet.Cells[21, 10] = "a) Date of leaving Service ";
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                //May Zero value
                chartrange = xlWorkSheet.get_Range("A22", "A22");
                xlWorkSheet.Cells[22, 1] = "May " + ddlfinance.SelectedValue;
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                chartrange = xlWorkSheet.get_Range("B22", "B22");
                xlWorkSheet.Cells[22, 2] = "0.00";
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                chartrange = xlWorkSheet.get_Range("C22", "C22");
                xlWorkSheet.Cells[22, 3] = "0";
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                chartrange = xlWorkSheet.get_Range("D22", "D22");
                xlWorkSheet.Cells[22, 4] = "0";
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                chartrange = xlWorkSheet.get_Range("E22", "E22");
                xlWorkSheet.Cells[22, 5] = "0";
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                chartrange = xlWorkSheet.get_Range("F22", "F22");
                xlWorkSheet.Cells[22, 6] = "0";
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                chartrange = xlWorkSheet.get_Range("G22", "G22");
                xlWorkSheet.Cells[22, 7] = "0";
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                chartrange = xlWorkSheet.get_Range("H22", "H22");
                xlWorkSheet.Cells[22, 8] = "";
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                chartrange = xlWorkSheet.get_Range("I22", "I22");
                xlWorkSheet.Cells[22, 9] = "";
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                chartrange = xlWorkSheet.get_Range("J22", "J22");
                xlWorkSheet.Cells[22, 10] = "..........";
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                //June Zero Value
                chartrange = xlWorkSheet.get_Range("A23", "A23");
                xlWorkSheet.Cells[23, 1] = "June " + ddlfinance.SelectedValue;
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                chartrange = xlWorkSheet.get_Range("B23", "B23");
                xlWorkSheet.Cells[23, 2] = "0.00";
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                chartrange = xlWorkSheet.get_Range("C23", "C23");
                xlWorkSheet.Cells[23, 3] = "0";
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                chartrange = xlWorkSheet.get_Range("D23", "D23");
                xlWorkSheet.Cells[23, 4] = "0";
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                chartrange = xlWorkSheet.get_Range("E23", "E23");
                xlWorkSheet.Cells[23, 5] = "0";
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                chartrange = xlWorkSheet.get_Range("F23", "F23");
                xlWorkSheet.Cells[23, 6] = "0";
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                chartrange = xlWorkSheet.get_Range("G23", "G23");
                xlWorkSheet.Cells[23, 7] = "0";
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                chartrange = xlWorkSheet.get_Range("H23", "H23");
                xlWorkSheet.Cells[23, 8] = "";
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                chartrange = xlWorkSheet.get_Range("I23", "I23");
                xlWorkSheet.Cells[23, 9] = "";
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                chartrange = xlWorkSheet.get_Range("J23", "J23");
                xlWorkSheet.Cells[23, 10] = "b) Reason for leaving ";
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                //July Zero Value
                chartrange = xlWorkSheet.get_Range("A24", "A24");
                xlWorkSheet.Cells[24, 1] = "July " + ddlfinance.SelectedValue;
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                chartrange = xlWorkSheet.get_Range("B24", "B24");
                xlWorkSheet.Cells[24, 2] = "0.00";
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                chartrange = xlWorkSheet.get_Range("C24", "C24");
                xlWorkSheet.Cells[24, 3] = "0";
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                chartrange = xlWorkSheet.get_Range("D24", "D24");
                xlWorkSheet.Cells[24, 4] = "0";
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                chartrange = xlWorkSheet.get_Range("E24", "E24");
                xlWorkSheet.Cells[24, 5] = "0";
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                chartrange = xlWorkSheet.get_Range("F24", "F24");
                xlWorkSheet.Cells[24, 6] = "0";
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                chartrange = xlWorkSheet.get_Range("G24", "G24");
                xlWorkSheet.Cells[24, 7] = "0";
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                chartrange = xlWorkSheet.get_Range("H24", "H24");
                xlWorkSheet.Cells[24, 8] = "";
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                chartrange = xlWorkSheet.get_Range("I24", "I24");
                xlWorkSheet.Cells[24, 9] = "";
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                chartrange = xlWorkSheet.get_Range("J24", "J24");
                xlWorkSheet.Cells[24, 10] = "Service …………";
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                //August Zero Value
                chartrange = xlWorkSheet.get_Range("A25", "A25");
                xlWorkSheet.Cells[25, 1] = "August " + ddlfinance.SelectedValue;
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                chartrange = xlWorkSheet.get_Range("B25", "B25");
                xlWorkSheet.Cells[25, 2] = "0.00";
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                chartrange = xlWorkSheet.get_Range("C25", "C25");
                xlWorkSheet.Cells[25, 3] = "0";
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                chartrange = xlWorkSheet.get_Range("D25", "D25");
                xlWorkSheet.Cells[25, 4] = "0";
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                chartrange = xlWorkSheet.get_Range("E25", "E25");
                xlWorkSheet.Cells[25, 5] = "0";
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                chartrange = xlWorkSheet.get_Range("F25", "F25");
                xlWorkSheet.Cells[25, 6] = "0";
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                chartrange = xlWorkSheet.get_Range("G25", "G25");
                xlWorkSheet.Cells[25, 7] = "0";
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                chartrange = xlWorkSheet.get_Range("H25", "H25");
                xlWorkSheet.Cells[25, 8] = "";
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                chartrange = xlWorkSheet.get_Range("I25", "I25");
                xlWorkSheet.Cells[25, 9] = "";
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                chartrange = xlWorkSheet.get_Range("J25", "J25");
                xlWorkSheet.Cells[25, 10] = "C) Certified that the total ";
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                //September Zero Value
                chartrange = xlWorkSheet.get_Range("A26", "A26");
                xlWorkSheet.Cells[26, 1] = "September " + ddlfinance.SelectedValue;
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                chartrange = xlWorkSheet.get_Range("B26", "B26");
                xlWorkSheet.Cells[26, 2] = "0.00";
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                chartrange = xlWorkSheet.get_Range("C26", "C26");
                xlWorkSheet.Cells[26, 3] = "0";
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                chartrange = xlWorkSheet.get_Range("D26", "D26");
                xlWorkSheet.Cells[26, 4] = "0";
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                chartrange = xlWorkSheet.get_Range("E26", "E26");
                xlWorkSheet.Cells[26, 5] = "0";
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                chartrange = xlWorkSheet.get_Range("F26", "F26");
                xlWorkSheet.Cells[26, 6] = "0";
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                chartrange = xlWorkSheet.get_Range("G26", "G26");
                xlWorkSheet.Cells[26, 7] = "0";
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                chartrange = xlWorkSheet.get_Range("H26", "H26");
                xlWorkSheet.Cells[26, 8] = "";
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                chartrange = xlWorkSheet.get_Range("I26", "I26");
                xlWorkSheet.Cells[26, 9] = "";
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                chartrange = xlWorkSheet.get_Range("J26", "J26");
                xlWorkSheet.Cells[26, 10] = "amount of contribution ";
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                //October Zero Value
                chartrange = xlWorkSheet.get_Range("A27", "A27");
                xlWorkSheet.Cells[27, 1] = "October " + ddlfinance.SelectedValue;
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                chartrange = xlWorkSheet.get_Range("B27", "B27");
                xlWorkSheet.Cells[27, 2] = "0.00";
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                chartrange = xlWorkSheet.get_Range("C27", "C27");
                xlWorkSheet.Cells[27, 3] = "0";
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                chartrange = xlWorkSheet.get_Range("D27", "D27");
                xlWorkSheet.Cells[27, 4] = "0";
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                chartrange = xlWorkSheet.get_Range("E27", "E27");
                xlWorkSheet.Cells[27, 5] = "0";
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                chartrange = xlWorkSheet.get_Range("F27", "F27");
                xlWorkSheet.Cells[27, 6] = "0";
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                chartrange = xlWorkSheet.get_Range("G27", "G27");
                xlWorkSheet.Cells[27, 7] = "0";
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                chartrange = xlWorkSheet.get_Range("H27", "H27");
                xlWorkSheet.Cells[27, 8] = "";
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                chartrange = xlWorkSheet.get_Range("I27", "I27");
                xlWorkSheet.Cells[27, 9] = "";
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                chartrange = xlWorkSheet.get_Range("J27", "J27");
                xlWorkSheet.Cells[27, 10] = "indicated in this card i.e. ";
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                //November Zero Value
                chartrange = xlWorkSheet.get_Range("A28", "A28");
                xlWorkSheet.Cells[28, 1] = "November " + ddlfinance.SelectedValue;
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                chartrange = xlWorkSheet.get_Range("B28", "B28");
                xlWorkSheet.Cells[28, 2] = "0.00";
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                chartrange = xlWorkSheet.get_Range("C28", "C28");
                xlWorkSheet.Cells[28, 3] = "0";
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                chartrange = xlWorkSheet.get_Range("D28", "D28");
                xlWorkSheet.Cells[28, 4] = "0";
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                chartrange = xlWorkSheet.get_Range("E28", "E28");
                xlWorkSheet.Cells[28, 5] = "0";
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                chartrange = xlWorkSheet.get_Range("F28", "F28");
                xlWorkSheet.Cells[28, 6] = "0";
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                chartrange = xlWorkSheet.get_Range("G28", "G28");
                xlWorkSheet.Cells[28, 7] = "0";
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                chartrange = xlWorkSheet.get_Range("H28", "H28");
                xlWorkSheet.Cells[28, 8] = "";
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                chartrange = xlWorkSheet.get_Range("I28", "I28");
                xlWorkSheet.Cells[28, 9] = "";
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                chartrange = xlWorkSheet.get_Range("J28", "J28");
                xlWorkSheet.Cells[28, 10] = "Rs. …… Col 3 (c ) + ";
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                //December Zero value
                chartrange = xlWorkSheet.get_Range("A29", "A29");
                xlWorkSheet.Cells[29, 1] = "December " + ddlfinance.SelectedValue;
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                chartrange = xlWorkSheet.get_Range("B29", "B29");
                xlWorkSheet.Cells[29, 2] = "0.00";
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                chartrange = xlWorkSheet.get_Range("C29", "C29");
                xlWorkSheet.Cells[29, 3] = "0";
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                chartrange = xlWorkSheet.get_Range("D29", "D29");
                xlWorkSheet.Cells[29, 4] = "0";
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                chartrange = xlWorkSheet.get_Range("E29", "E29");
                xlWorkSheet.Cells[29, 5] = "0";
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                chartrange = xlWorkSheet.get_Range("F29", "F29");
                xlWorkSheet.Cells[29, 6] = "0";
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                chartrange = xlWorkSheet.get_Range("G29", "G29");
                xlWorkSheet.Cells[29, 7] = "0";
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                chartrange = xlWorkSheet.get_Range("H29", "H29");
                xlWorkSheet.Cells[29, 8] = "";
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                chartrange = xlWorkSheet.get_Range("I29", "I29");
                xlWorkSheet.Cells[29, 9] = "";
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                chartrange = xlWorkSheet.get_Range("J29", "J29");
                xlWorkSheet.Cells[29, 10] = "Col.4 (c ) has already been ";
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                //January Zero Value
                chartrange = xlWorkSheet.get_Range("A30", "A30");
                finance = (Convert.ToInt32(ddlfinance.SelectedValue) + 1);
                xlWorkSheet.Cells[30, 1] = "January " + finance.ToString();
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                chartrange = xlWorkSheet.get_Range("B30", "B30");
                xlWorkSheet.Cells[30, 2] = "0.00";
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                chartrange = xlWorkSheet.get_Range("C30", "C30");
                xlWorkSheet.Cells[30, 3] = "0";
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                chartrange = xlWorkSheet.get_Range("D30", "D30");
                xlWorkSheet.Cells[30, 4] = "0";
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                chartrange = xlWorkSheet.get_Range("E30", "E30");
                xlWorkSheet.Cells[30, 5] = "0";
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                chartrange = xlWorkSheet.get_Range("F30", "F30");
                xlWorkSheet.Cells[30, 6] = "0";
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                chartrange = xlWorkSheet.get_Range("G30", "G30");
                xlWorkSheet.Cells[30, 7] = "0";
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                chartrange = xlWorkSheet.get_Range("H30", "H30");
                xlWorkSheet.Cells[30, 8] = "";
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                chartrange = xlWorkSheet.get_Range("I30", "I30");
                xlWorkSheet.Cells[30, 9] = "";
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                chartrange = xlWorkSheet.get_Range("J30", "J30");
                xlWorkSheet.Cells[30, 10] = "remitted in full in E.P.F. ";
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                //February Zero value
                chartrange = xlWorkSheet.get_Range("A31", "A31");
                finance = (Convert.ToInt32(ddlfinance.SelectedValue) + 1);
                xlWorkSheet.Cells[31, 1] = "February " + finance.ToString();
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                chartrange = xlWorkSheet.get_Range("B31", "B31");
                xlWorkSheet.Cells[31, 2] = "0.00";
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                chartrange = xlWorkSheet.get_Range("C31", "C31");
                xlWorkSheet.Cells[31, 3] = "0";
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                chartrange = xlWorkSheet.get_Range("D31", "D31");
                xlWorkSheet.Cells[31, 4] = "0";
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                chartrange = xlWorkSheet.get_Range("E31", "E31");
                xlWorkSheet.Cells[31, 5] = "0";
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                chartrange = xlWorkSheet.get_Range("F31", "F31");
                xlWorkSheet.Cells[31, 6] = "0";
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                chartrange = xlWorkSheet.get_Range("G31", "G31");
                xlWorkSheet.Cells[31, 7] = "0";
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                chartrange = xlWorkSheet.get_Range("H31", "H31");
                xlWorkSheet.Cells[31, 8] = "";
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                chartrange = xlWorkSheet.get_Range("I31", "I31");
                xlWorkSheet.Cells[31, 9] = "";
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                chartrange = xlWorkSheet.get_Range("J31", "J31");
                xlWorkSheet.Cells[31, 10] = "A/c. No. 1 (Provident Fund Contributions A/c) and in Account No.10(Employees's Pension Fund Contribut";
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;




                for (int i = 0; i < dt_val.Rows.Count; i++)
                {
                    if (dt_val.Rows[i]["Month"].ToString() == "March")
                    {
                        chartrange = xlWorkSheet.get_Range("A20", "A20");
                        xlWorkSheet.Cells[20, 1] = dt_val.Rows[i]["Month"].ToString() + " " + dt_val.Rows[i]["Year"].ToString();
                        chartrange.EntireColumn.AutoFit();
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                        chartrange = xlWorkSheet.get_Range("B20", "B20");
                        xlWorkSheet.Cells[20, 2] = dt_val.Rows[i]["PfSalary"].ToString();
                        chartrange.EntireColumn.AutoFit();
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                        chartrange = xlWorkSheet.get_Range("C20", "C20");
                        xlWorkSheet.Cells[20, 3] = dt_val.Rows[i]["ProvidentFund"].ToString();
                        chartrange.EntireColumn.AutoFit();
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                        chartrange = xlWorkSheet.get_Range("D20", "D20");
                        xlWorkSheet.Cells[20, 4] = dt_val.Rows[i]["ProvidentFund"].ToString();
                        chartrange.EntireColumn.AutoFit();
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                        if (Math.Round(Convert.ToDecimal(dt_val.Rows[i]["ProvidentFund"].ToString()), 0, MidpointRounding.ToEven) > 780)
                        {

                            chartrange = xlWorkSheet.get_Range("E20", "E20");
                            xlWorkSheet.Cells[20, 5] = "238.55";
                            chartrange.EntireColumn.AutoFit();
                            chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                            chartrange = xlWorkSheet.get_Range("F20", "F20");
                            xlWorkSheet.Cells[20, 6] = "541.45";
                            chartrange.EntireColumn.AutoFit();
                            chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                            chartrange = xlWorkSheet.get_Range("G20", "G20");
                            xlWorkSheet.Cells[20, 7] = "780";
                            chartrange.EntireColumn.AutoFit();
                            chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                        }
                        else
                        {

                            chartrange = xlWorkSheet.get_Range("E20", "E20");
                            xlWorkSheet.Cells[20, 5] = dt_val.Rows[i]["A"].ToString();
                            chartrange.EntireColumn.AutoFit();
                            chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                            chartrange = xlWorkSheet.get_Range("F20", "F20");
                            xlWorkSheet.Cells[20, 6] = dt_val.Rows[i]["B"].ToString();
                            chartrange.EntireColumn.AutoFit();
                            chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                            chartrange = xlWorkSheet.get_Range("G20", "G20");
                            xlWorkSheet.Cells[20, 7] = dt_val.Rows[i]["ProvidentFund"].ToString();
                            chartrange.EntireColumn.AutoFit();
                            chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                        }

                        chartrange = xlWorkSheet.get_Range("H20", "H20");
                        xlWorkSheet.Cells[20, 8] = "";
                        chartrange.EntireColumn.AutoFit();
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                        chartrange = xlWorkSheet.get_Range("I20", "I20");
                        xlWorkSheet.Cells[20, 9] = "";
                        chartrange.EntireColumn.AutoFit();
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                        chartrange = xlWorkSheet.get_Range("J20", "J20");
                        xlWorkSheet.Cells[20, 10] = "";
                        chartrange.WrapText = true;
                        chartrange.EntireColumn.AutoFit();
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                    }
                    else if (dt_val.Rows[i]["Month"].ToString() == "April")
                    {
                        chartrange = xlWorkSheet.get_Range("A21", "A21");
                        xlWorkSheet.Cells[21, 1] = dt_val.Rows[i]["Month"].ToString() + " " + dt_val.Rows[i]["Year"].ToString();
                        chartrange.EntireColumn.AutoFit();
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                        chartrange = xlWorkSheet.get_Range("B21", "B21");
                        xlWorkSheet.Cells[21, 2] = dt_val.Rows[i]["PfSalary"].ToString();
                        chartrange.EntireColumn.AutoFit();
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                        chartrange = xlWorkSheet.get_Range("C21", "C21");
                        xlWorkSheet.Cells[21, 3] = dt_val.Rows[i]["ProvidentFund"].ToString();
                        chartrange.EntireColumn.AutoFit();
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                        chartrange = xlWorkSheet.get_Range("D21", "D21");
                        xlWorkSheet.Cells[21, 4] = dt_val.Rows[i]["ProvidentFund"].ToString();
                        chartrange.EntireColumn.AutoFit();
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                        if (Math.Round(Convert.ToDecimal(dt_val.Rows[i]["ProvidentFund"].ToString()), 0, MidpointRounding.ToEven) > 780)
                        {

                            chartrange = xlWorkSheet.get_Range("E21", "E21");
                            xlWorkSheet.Cells[21, 5] = "238.55";
                            chartrange.EntireColumn.AutoFit();
                            chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                            chartrange = xlWorkSheet.get_Range("F21", "F21");
                            xlWorkSheet.Cells[21, 6] = "541.45";
                            chartrange.EntireColumn.AutoFit();
                            chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                            chartrange = xlWorkSheet.get_Range("G21", "G21");
                            xlWorkSheet.Cells[21, 7] = "780";
                            chartrange.EntireColumn.AutoFit();
                        }
                        else
                        {

                            chartrange = xlWorkSheet.get_Range("E21", "E21");
                            xlWorkSheet.Cells[21, 5] = dt_val.Rows[i]["A"].ToString();
                            chartrange.EntireColumn.AutoFit();
                            chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                            chartrange = xlWorkSheet.get_Range("F21", "F21");
                            xlWorkSheet.Cells[21, 6] = dt_val.Rows[i]["B"].ToString();
                            chartrange.EntireColumn.AutoFit();
                            chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                            chartrange = xlWorkSheet.get_Range("G21", "G21");
                            xlWorkSheet.Cells[21, 7] = dt_val.Rows[i]["ProvidentFund"].ToString();
                            chartrange.EntireColumn.AutoFit();
                            chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                        }

                        chartrange = xlWorkSheet.get_Range("H21", "H21");
                        xlWorkSheet.Cells[21, 8] = "";
                        chartrange.EntireColumn.AutoFit();
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                        chartrange = xlWorkSheet.get_Range("I21", "I21");
                        xlWorkSheet.Cells[21, 9] = "";
                        chartrange.EntireColumn.AutoFit();
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                        chartrange = xlWorkSheet.get_Range("J21", "J21");
                        xlWorkSheet.Cells[21, 10] = "a) Date of leaving Service ";
                        chartrange.EntireColumn.AutoFit();
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                    }

                    else if (dt_val.Rows[i]["Month"].ToString() == "May")
                    {
                        chartrange = xlWorkSheet.get_Range("A22", "A22");
                        xlWorkSheet.Cells[22, 1] = dt_val.Rows[i]["Month"].ToString() + " " + dt_val.Rows[i]["Year"].ToString();
                        chartrange.EntireColumn.AutoFit();
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                        chartrange = xlWorkSheet.get_Range("B22", "B22");
                        xlWorkSheet.Cells[22, 2] = dt_val.Rows[i]["PfSalary"].ToString();
                        chartrange.EntireColumn.AutoFit();
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                        chartrange = xlWorkSheet.get_Range("C22", "C22");
                        xlWorkSheet.Cells[22, 3] = dt_val.Rows[i]["ProvidentFund"].ToString();
                        chartrange.EntireColumn.AutoFit();
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                        chartrange = xlWorkSheet.get_Range("D22", "D22");
                        xlWorkSheet.Cells[22, 4] = dt_val.Rows[i]["ProvidentFund"].ToString();
                        chartrange.EntireColumn.AutoFit();
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                        if (Math.Round(Convert.ToDecimal(dt_val.Rows[i]["ProvidentFund"].ToString()), 0, MidpointRounding.ToEven) > 780)
                        {

                            chartrange = xlWorkSheet.get_Range("E22", "E22");
                            xlWorkSheet.Cells[22, 5] = "238.45";
                            chartrange.EntireColumn.AutoFit();
                            chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                            chartrange = xlWorkSheet.get_Range("F22", "F22");
                            xlWorkSheet.Cells[22, 6] = "541.45";
                            chartrange.EntireColumn.AutoFit();
                            chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                            chartrange = xlWorkSheet.get_Range("G22", "G22");
                            xlWorkSheet.Cells[22, 7] = "780";
                            chartrange.EntireColumn.AutoFit();
                            chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                        }
                        else
                        {

                            chartrange = xlWorkSheet.get_Range("E22", "E22");
                            xlWorkSheet.Cells[22, 5] = dt_val.Rows[i]["A"].ToString();
                            chartrange.EntireColumn.AutoFit();
                            chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                            chartrange = xlWorkSheet.get_Range("F22", "F22");
                            xlWorkSheet.Cells[22, 6] = dt_val.Rows[i]["B"].ToString();
                            chartrange.EntireColumn.AutoFit();
                            chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                            chartrange = xlWorkSheet.get_Range("G22", "G22");
                            xlWorkSheet.Cells[22, 7] = dt_val.Rows[i]["ProvidentFund"].ToString();
                            chartrange.EntireColumn.AutoFit();
                            chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                        }


                        chartrange = xlWorkSheet.get_Range("H22", "H22");
                        xlWorkSheet.Cells[22, 8] = "";
                        chartrange.EntireColumn.AutoFit();
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                        chartrange = xlWorkSheet.get_Range("I22", "I22");
                        xlWorkSheet.Cells[22, 9] = "";
                        chartrange.EntireColumn.AutoFit();
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                        chartrange = xlWorkSheet.get_Range("J22", "J22");
                        xlWorkSheet.Cells[22, 10] = "…………";
                        chartrange.EntireColumn.AutoFit();
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                    }
                    else if (dt_val.Rows[i]["Month"].ToString() == "June")
                    {
                        chartrange = xlWorkSheet.get_Range("A23", "A23");
                        xlWorkSheet.Cells[23, 1] = dt_val.Rows[i]["Month"].ToString() + " " + dt_val.Rows[i]["Year"].ToString();
                        chartrange.EntireColumn.AutoFit();
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                        chartrange = xlWorkSheet.get_Range("B23", "B23");
                        xlWorkSheet.Cells[23, 2] = dt_val.Rows[i]["PfSalary"].ToString();
                        chartrange.EntireColumn.AutoFit();
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                        chartrange = xlWorkSheet.get_Range("C23", "C23");
                        xlWorkSheet.Cells[23, 3] = dt_val.Rows[i]["ProvidentFund"].ToString();
                        chartrange.EntireColumn.AutoFit();
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                        chartrange = xlWorkSheet.get_Range("D23", "D23");
                        xlWorkSheet.Cells[23, 4] = dt_val.Rows[i]["ProvidentFund"].ToString();
                        chartrange.EntireColumn.AutoFit();
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                        if (Math.Round(Convert.ToDecimal(dt_val.Rows[i]["ProvidentFund"].ToString()), 0, MidpointRounding.ToEven) > 780)
                        {


                            chartrange = xlWorkSheet.get_Range("E23", "E23");
                            xlWorkSheet.Cells[23, 5] = "238.55";
                            chartrange.EntireColumn.AutoFit();
                            chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                            chartrange = xlWorkSheet.get_Range("F23", "F23");
                            xlWorkSheet.Cells[23, 6] = "541.45";
                            chartrange.EntireColumn.AutoFit();
                            chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                            chartrange = xlWorkSheet.get_Range("G23", "G23");
                            xlWorkSheet.Cells[23, 7] = "780";
                            chartrange.EntireColumn.AutoFit();
                        }
                        else
                        {

                            chartrange = xlWorkSheet.get_Range("E23", "E23");
                            xlWorkSheet.Cells[23, 5] = dt_val.Rows[i]["A"].ToString();
                            chartrange.EntireColumn.AutoFit();
                            chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                            chartrange = xlWorkSheet.get_Range("F23", "F23");
                            xlWorkSheet.Cells[23, 6] = dt_val.Rows[i]["B"].ToString();
                            chartrange.EntireColumn.AutoFit();
                            chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                            chartrange = xlWorkSheet.get_Range("G23", "G23");
                            xlWorkSheet.Cells[23, 7] = dt_val.Rows[i]["ProvidentFund"].ToString();
                            chartrange.EntireColumn.AutoFit();
                            chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                        }

                        chartrange = xlWorkSheet.get_Range("H23", "H23");
                        xlWorkSheet.Cells[23, 8] = "";
                        chartrange.EntireColumn.AutoFit();
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                        chartrange = xlWorkSheet.get_Range("I23", "I23");
                        xlWorkSheet.Cells[23, 9] = "";
                        chartrange.EntireColumn.AutoFit();
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                        chartrange = xlWorkSheet.get_Range("J23", "J23");
                        xlWorkSheet.Cells[23, 10] = "b) Reason for leaving ";
                        chartrange.EntireColumn.AutoFit();
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                    }

                    else if (dt_val.Rows[i]["Month"].ToString() == "July")
                    {
                        chartrange = xlWorkSheet.get_Range("A24", "A24");
                        xlWorkSheet.Cells[24, 1] = dt_val.Rows[i]["Month"].ToString() + " " + dt_val.Rows[i]["Year"].ToString();
                        chartrange.EntireColumn.AutoFit();
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                        chartrange = xlWorkSheet.get_Range("B24", "B24");
                        xlWorkSheet.Cells[24, 2] = dt_val.Rows[i]["PfSalary"].ToString();
                        chartrange.EntireColumn.AutoFit();
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                        chartrange = xlWorkSheet.get_Range("C24", "C24");
                        xlWorkSheet.Cells[24, 3] = dt_val.Rows[i]["ProvidentFund"].ToString();
                        chartrange.EntireColumn.AutoFit();
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                        chartrange = xlWorkSheet.get_Range("D24", "D24");
                        xlWorkSheet.Cells[24, 4] = dt_val.Rows[i]["ProvidentFund"].ToString();
                        chartrange.EntireColumn.AutoFit();
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                        if (Math.Round(Convert.ToDecimal(dt_val.Rows[i]["ProvidentFund"].ToString()), 0, MidpointRounding.ToEven) > 780)
                        {

                            chartrange = xlWorkSheet.get_Range("E24", "E24");
                            xlWorkSheet.Cells[24, 5] = "238.55";
                            chartrange.EntireColumn.AutoFit();
                            chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                            chartrange = xlWorkSheet.get_Range("F24", "F24");
                            xlWorkSheet.Cells[24, 6] = "541.45";
                            chartrange.EntireColumn.AutoFit();
                            chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                            chartrange = xlWorkSheet.get_Range("G24", "G24");
                            xlWorkSheet.Cells[24, 7] = "780";
                            chartrange.EntireColumn.AutoFit();
                            chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                        }
                        else
                        {

                            chartrange = xlWorkSheet.get_Range("E24", "E24");
                            xlWorkSheet.Cells[24, 5] = dt_val.Rows[i]["A"].ToString();
                            chartrange.EntireColumn.AutoFit();
                            chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                            chartrange = xlWorkSheet.get_Range("F24", "F24");
                            xlWorkSheet.Cells[24, 6] = dt_val.Rows[i]["B"].ToString();
                            chartrange.EntireColumn.AutoFit();
                            chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                            chartrange = xlWorkSheet.get_Range("G24", "G24");
                            xlWorkSheet.Cells[24, 7] = dt_val.Rows[i]["ProvidentFund"].ToString();
                            chartrange.EntireColumn.AutoFit();
                            chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                        }

                        chartrange = xlWorkSheet.get_Range("H24", "H24");
                        xlWorkSheet.Cells[24, 8] = "";
                        chartrange.EntireColumn.AutoFit();
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                        chartrange = xlWorkSheet.get_Range("I24", "I24");
                        xlWorkSheet.Cells[24, 9] = "";
                        chartrange.EntireColumn.AutoFit();
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                        chartrange = xlWorkSheet.get_Range("J24", "J24");
                        xlWorkSheet.Cells[24, 10] = "Service ………….";
                        chartrange.EntireColumn.AutoFit();
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                    }

                    else if (dt_val.Rows[i]["Month"].ToString() == "August")
                    {
                        chartrange = xlWorkSheet.get_Range("A25", "A25");
                        xlWorkSheet.Cells[25, 1] = dt_val.Rows[i]["Month"].ToString() + " " + dt_val.Rows[i]["Year"].ToString();
                        chartrange.EntireColumn.AutoFit();
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                        chartrange = xlWorkSheet.get_Range("B25", "B25");
                        xlWorkSheet.Cells[25, 2] = dt_val.Rows[i]["PfSalary"].ToString();
                        chartrange.EntireColumn.AutoFit();
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                        chartrange = xlWorkSheet.get_Range("C25", "C25");
                        xlWorkSheet.Cells[25, 3] = dt_val.Rows[i]["ProvidentFund"].ToString();
                        chartrange.EntireColumn.AutoFit();
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                        chartrange = xlWorkSheet.get_Range("D25", "D25");
                        xlWorkSheet.Cells[25, 4] = dt_val.Rows[i]["ProvidentFund"].ToString();
                        chartrange.EntireColumn.AutoFit();
                        if (Math.Round(Convert.ToDecimal(dt_val.Rows[i]["ProvidentFund"].ToString()), 0, MidpointRounding.ToEven) > 780)
                        {
                            chartrange = xlWorkSheet.get_Range("E25", "E25");
                            xlWorkSheet.Cells[25, 5] = "238.55";
                            chartrange.EntireColumn.AutoFit();
                            chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                            chartrange = xlWorkSheet.get_Range("F25", "F25");
                            xlWorkSheet.Cells[25, 6] = "541.45";
                            chartrange.EntireColumn.AutoFit();
                            chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                            chartrange = xlWorkSheet.get_Range("G25", "G25");
                            xlWorkSheet.Cells[25, 7] = "780";
                            chartrange.EntireColumn.AutoFit();
                            chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                        }
                        else
                        {

                            chartrange = xlWorkSheet.get_Range("E25", "E25");
                            xlWorkSheet.Cells[25, 5] = dt_val.Rows[i]["A"].ToString();
                            chartrange.EntireColumn.AutoFit();
                            chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                            chartrange = xlWorkSheet.get_Range("F25", "F25");
                            xlWorkSheet.Cells[25, 6] = dt_val.Rows[i]["B"].ToString();
                            chartrange.EntireColumn.AutoFit();
                            chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                            chartrange = xlWorkSheet.get_Range("G25", "G25");
                            xlWorkSheet.Cells[25, 7] = dt_val.Rows[i]["ProvidentFund"].ToString();
                            chartrange.EntireColumn.AutoFit();
                            chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                        }



                        chartrange = xlWorkSheet.get_Range("H25", "H25");
                        xlWorkSheet.Cells[25, 8] = "";
                        chartrange.EntireColumn.AutoFit();
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                        chartrange = xlWorkSheet.get_Range("I25", "I25");
                        xlWorkSheet.Cells[25, 9] = "";
                        chartrange.EntireColumn.AutoFit();
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                        chartrange = xlWorkSheet.get_Range("J25", "J25");
                        xlWorkSheet.Cells[25, 10] = "C) Certified that the total ";
                        chartrange.EntireColumn.AutoFit();
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                    }

                    else if (dt_val.Rows[i]["Month"].ToString() == "September")
                    {
                        chartrange = xlWorkSheet.get_Range("A26", "A26");
                        xlWorkSheet.Cells[26, 1] = dt_val.Rows[i]["Month"].ToString() + " " + dt_val.Rows[i]["Year"].ToString();
                        chartrange.EntireColumn.AutoFit();
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                        chartrange = xlWorkSheet.get_Range("B26", "B26");
                        xlWorkSheet.Cells[26, 2] = dt_val.Rows[i]["PfSalary"].ToString();
                        chartrange.EntireColumn.AutoFit();
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                        chartrange = xlWorkSheet.get_Range("C26", "C26");
                        xlWorkSheet.Cells[26, 3] = dt_val.Rows[i]["ProvidentFund"].ToString();
                        chartrange.EntireColumn.AutoFit();
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                        chartrange = xlWorkSheet.get_Range("D26", "D26");
                        xlWorkSheet.Cells[26, 4] = dt_val.Rows[i]["ProvidentFund"].ToString();
                        chartrange.EntireColumn.AutoFit();
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                        if (Math.Round(Convert.ToDecimal(dt_val.Rows[i]["ProvidentFund"].ToString()), 0, MidpointRounding.ToEven) > 780)
                        {
                            chartrange = xlWorkSheet.get_Range("E26", "E26");
                            xlWorkSheet.Cells[26, 5] = "238.55";
                            chartrange.EntireColumn.AutoFit();
                            chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                            chartrange = xlWorkSheet.get_Range("F26", "F26");
                            xlWorkSheet.Cells[26, 6] = "541.45";
                            chartrange.EntireColumn.AutoFit();
                            chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                            chartrange = xlWorkSheet.get_Range("G26", "G26");
                            xlWorkSheet.Cells[26, 7] = "780";
                            chartrange.EntireColumn.AutoFit();
                            chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                        }
                        else
                        {
                            chartrange = xlWorkSheet.get_Range("E26", "E26");
                            xlWorkSheet.Cells[26, 5] = dt_val.Rows[i]["A"].ToString();
                            chartrange.EntireColumn.AutoFit();
                            chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                            chartrange = xlWorkSheet.get_Range("F26", "F26");
                            xlWorkSheet.Cells[26, 6] = dt_val.Rows[i]["B"].ToString();
                            chartrange.EntireColumn.AutoFit();
                            chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                            chartrange = xlWorkSheet.get_Range("G26", "G26");
                            xlWorkSheet.Cells[26, 7] = dt_val.Rows[i]["ProvidentFund"].ToString();
                            chartrange.EntireColumn.AutoFit();
                            chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                        }

                        chartrange = xlWorkSheet.get_Range("H26", "H26");
                        xlWorkSheet.Cells[26, 8] = "";
                        chartrange.EntireColumn.AutoFit();
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                        chartrange = xlWorkSheet.get_Range("I26", "I26");
                        xlWorkSheet.Cells[26, 9] = "";
                        chartrange.EntireColumn.AutoFit();
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                        chartrange = xlWorkSheet.get_Range("J26", "J26");
                        xlWorkSheet.Cells[26, 10] = "amount of contribution ";
                        chartrange.EntireColumn.AutoFit();
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                    }

                    else if (dt_val.Rows[i]["Month"].ToString() == "October")
                    {
                        chartrange = xlWorkSheet.get_Range("A27", "A27");
                        xlWorkSheet.Cells[27, 1] = dt_val.Rows[i]["Month"].ToString() + " " + dt_val.Rows[i]["Year"].ToString();
                        chartrange.EntireColumn.AutoFit();
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                        chartrange = xlWorkSheet.get_Range("B27", "B27");
                        xlWorkSheet.Cells[27, 2] = dt_val.Rows[i]["PfSalary"].ToString();
                        chartrange.EntireColumn.AutoFit();
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                        chartrange = xlWorkSheet.get_Range("C27", "C27");
                        xlWorkSheet.Cells[27, 3] = dt_val.Rows[i]["ProvidentFund"].ToString();
                        chartrange.EntireColumn.AutoFit();
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                        chartrange = xlWorkSheet.get_Range("D27", "D27");
                        xlWorkSheet.Cells[27, 4] = dt_val.Rows[i]["ProvidentFund"].ToString();
                        chartrange.EntireColumn.AutoFit();
                        if (Math.Round(Convert.ToDecimal(dt_val.Rows[i]["ProvidentFund"].ToString()), 0, MidpointRounding.ToEven) > 780)
                        {
                            chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                            chartrange = xlWorkSheet.get_Range("E27", "E27");
                            xlWorkSheet.Cells[27, 5] = "238.55";
                            chartrange.EntireColumn.AutoFit();
                            chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                            chartrange = xlWorkSheet.get_Range("F27", "F27");
                            xlWorkSheet.Cells[27, 6] = "541.45";
                            chartrange.EntireColumn.AutoFit();
                            chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                            chartrange = xlWorkSheet.get_Range("G27", "G27");
                            xlWorkSheet.Cells[27, 7] = "780";
                            chartrange.EntireColumn.AutoFit();
                            chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                        }
                        else
                        {
                            chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                            chartrange = xlWorkSheet.get_Range("E27", "E27");
                            xlWorkSheet.Cells[27, 5] = dt_val.Rows[i]["A"].ToString();
                            chartrange.EntireColumn.AutoFit();
                            chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                            chartrange = xlWorkSheet.get_Range("F27", "F27");
                            xlWorkSheet.Cells[27, 6] = dt_val.Rows[i]["B"].ToString();
                            chartrange.EntireColumn.AutoFit();
                            chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                            chartrange = xlWorkSheet.get_Range("G27", "G27");
                            xlWorkSheet.Cells[27, 7] = dt_val.Rows[i]["ProvidentFund"].ToString();
                            chartrange.EntireColumn.AutoFit();
                            chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                        }

                        chartrange = xlWorkSheet.get_Range("H27", "H27");
                        xlWorkSheet.Cells[27, 8] = "";
                        chartrange.EntireColumn.AutoFit();
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                        chartrange = xlWorkSheet.get_Range("I27", "I27");
                        xlWorkSheet.Cells[27, 9] = "";
                        chartrange.EntireColumn.AutoFit();
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                        chartrange = xlWorkSheet.get_Range("J27", "J27");
                        xlWorkSheet.Cells[27, 10] = "indicated in this card i.e. ";
                        chartrange.EntireColumn.AutoFit();
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                    }

                    else if (dt_val.Rows[i]["Month"].ToString() == "November")
                    {
                        chartrange = xlWorkSheet.get_Range("A28", "A28");
                        xlWorkSheet.Cells[28, 1] = dt_val.Rows[i]["Month"].ToString() + " " + dt_val.Rows[i]["Year"].ToString();
                        chartrange.EntireColumn.AutoFit();
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                        chartrange = xlWorkSheet.get_Range("B28", "B28");
                        xlWorkSheet.Cells[28, 2] = dt_val.Rows[i]["PfSalary"].ToString();
                        chartrange.EntireColumn.AutoFit();
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                        chartrange = xlWorkSheet.get_Range("C28", "C28");
                        xlWorkSheet.Cells[28, 3] = dt_val.Rows[i]["ProvidentFund"].ToString();
                        chartrange.EntireColumn.AutoFit();
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                        chartrange = xlWorkSheet.get_Range("D28", "D28");
                        xlWorkSheet.Cells[28, 4] = dt_val.Rows[i]["ProvidentFund"].ToString();
                        chartrange.EntireColumn.AutoFit();
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                        if (Math.Round(Convert.ToDecimal(dt_val.Rows[i]["ProvidentFund"].ToString()), 0, MidpointRounding.ToEven) > 780)
                        {
                            chartrange = xlWorkSheet.get_Range("E28", "E28");
                            xlWorkSheet.Cells[28, 5] = "238.55";
                            chartrange.EntireColumn.AutoFit();
                            chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                            chartrange = xlWorkSheet.get_Range("F28", "F28");
                            xlWorkSheet.Cells[28, 6] = "541.45";
                            chartrange.EntireColumn.AutoFit();
                            chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                            chartrange = xlWorkSheet.get_Range("G28", "G28");
                            xlWorkSheet.Cells[28, 7] = "780";
                            chartrange.EntireColumn.AutoFit();
                            chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                        }
                        else
                        {
                            chartrange = xlWorkSheet.get_Range("E28", "E28");
                            xlWorkSheet.Cells[28, 5] = dt_val.Rows[i]["A"].ToString();
                            chartrange.EntireColumn.AutoFit();
                            chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                            chartrange = xlWorkSheet.get_Range("F28", "F28");
                            xlWorkSheet.Cells[28, 6] = dt_val.Rows[i]["B"].ToString();
                            chartrange.EntireColumn.AutoFit();
                            chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                            chartrange = xlWorkSheet.get_Range("G28", "G28");
                            xlWorkSheet.Cells[28, 7] = dt_val.Rows[i]["ProvidentFund"].ToString();
                            chartrange.EntireColumn.AutoFit();
                            chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                        }

                        chartrange = xlWorkSheet.get_Range("H28", "H28");
                        xlWorkSheet.Cells[28, 8] = "";
                        chartrange.EntireColumn.AutoFit();
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                        chartrange = xlWorkSheet.get_Range("I28", "I28");
                        xlWorkSheet.Cells[28, 9] = "";
                        chartrange.EntireColumn.AutoFit();
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                        chartrange = xlWorkSheet.get_Range("J28", "J28");
                        xlWorkSheet.Cells[28, 10] = "Rs. …… Col 3 (c ) + ";
                        chartrange.EntireColumn.AutoFit();
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                    }

                    else if (dt_val.Rows[i]["Month"].ToString() == "December")
                    {
                        chartrange = xlWorkSheet.get_Range("A29", "A29");
                        xlWorkSheet.Cells[29, 1] = dt_val.Rows[i]["Month"].ToString() + " " + dt_val.Rows[i]["Year"].ToString();
                        chartrange.EntireColumn.AutoFit();
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                        chartrange = xlWorkSheet.get_Range("B29", "B29");
                        xlWorkSheet.Cells[29, 2] = dt_val.Rows[i]["PfSalary"].ToString();
                        chartrange.EntireColumn.AutoFit();
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                        chartrange = xlWorkSheet.get_Range("C29", "C29");
                        xlWorkSheet.Cells[29, 3] = dt_val.Rows[i]["ProvidentFund"].ToString();
                        chartrange.EntireColumn.AutoFit();
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                        chartrange = xlWorkSheet.get_Range("D29", "D29");
                        xlWorkSheet.Cells[29, 4] = dt_val.Rows[i]["ProvidentFund"].ToString();
                        chartrange.EntireColumn.AutoFit();
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                        if (Math.Round(Convert.ToDecimal(dt_val.Rows[i]["ProvidentFund"].ToString()), 0, MidpointRounding.ToEven) > 780)
                        {
                            chartrange = xlWorkSheet.get_Range("E29", "E29");
                            xlWorkSheet.Cells[29, 5] = "238.55";
                            chartrange.EntireColumn.AutoFit();
                            chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                            chartrange = xlWorkSheet.get_Range("F29", "F29");
                            xlWorkSheet.Cells[29, 6] = "541.45";
                            chartrange.EntireColumn.AutoFit();
                            chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                            chartrange = xlWorkSheet.get_Range("G29", "G29");
                            xlWorkSheet.Cells[29, 7] = "780";
                            chartrange.EntireColumn.AutoFit();
                            chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                        }
                        else
                        {
                            chartrange = xlWorkSheet.get_Range("E29", "E29");
                            xlWorkSheet.Cells[29, 5] = dt_val.Rows[i]["A"].ToString();
                            chartrange.EntireColumn.AutoFit();
                            chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                            chartrange = xlWorkSheet.get_Range("F29", "F29");
                            xlWorkSheet.Cells[29, 6] = dt_val.Rows[i]["B"].ToString();
                            chartrange.EntireColumn.AutoFit();
                            chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                            chartrange = xlWorkSheet.get_Range("G29", "G29");
                            xlWorkSheet.Cells[29, 7] = dt_val.Rows[i]["ProvidentFund"].ToString();
                            chartrange.EntireColumn.AutoFit();
                            chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                        }

                        chartrange = xlWorkSheet.get_Range("H29", "H29");
                        xlWorkSheet.Cells[29, 8] = "";
                        chartrange.EntireColumn.AutoFit();
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                        chartrange = xlWorkSheet.get_Range("I29", "I29");
                        xlWorkSheet.Cells[29, 9] = "";
                        chartrange.EntireColumn.AutoFit();
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                        chartrange = xlWorkSheet.get_Range("J29", "J29");
                        xlWorkSheet.Cells[29, 10] = "Col.4 (c ) has already been ";
                        chartrange.EntireColumn.AutoFit();
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                    }

                    else if (dt_val.Rows[i]["Month"].ToString() == "January")
                    {
                        chartrange = xlWorkSheet.get_Range("A30", "A30");
                        xlWorkSheet.Cells[30, 1] = dt_val.Rows[i]["Month"].ToString() + " " + dt_val.Rows[i]["Year"].ToString();
                        chartrange.EntireColumn.AutoFit();
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                        chartrange = xlWorkSheet.get_Range("B30", "B30");
                        xlWorkSheet.Cells[30, 2] = dt_val.Rows[i]["PfSalary"].ToString();
                        chartrange.EntireColumn.AutoFit();
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                        chartrange = xlWorkSheet.get_Range("C30", "C30");
                        xlWorkSheet.Cells[30, 3] = dt_val.Rows[i]["ProvidentFund"].ToString();
                        chartrange.EntireColumn.AutoFit();
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                        chartrange = xlWorkSheet.get_Range("D30", "D30");
                        xlWorkSheet.Cells[30, 4] = dt_val.Rows[i]["ProvidentFund"].ToString();
                        chartrange.EntireColumn.AutoFit();
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                        if (Math.Round(Convert.ToDecimal(dt_val.Rows[i]["ProvidentFund"].ToString()), 0, MidpointRounding.ToEven) > 780)
                        {
                            chartrange = xlWorkSheet.get_Range("E30", "E30");
                            xlWorkSheet.Cells[30, 5] = "238.55";
                            chartrange.EntireColumn.AutoFit();
                            chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                            chartrange = xlWorkSheet.get_Range("F30", "F30");
                            xlWorkSheet.Cells[30, 6] = "541.45";
                            chartrange.EntireColumn.AutoFit();
                            chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                            chartrange = xlWorkSheet.get_Range("G30", "G30");
                            xlWorkSheet.Cells[30, 7] = "780";
                            chartrange.EntireColumn.AutoFit();
                            chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                        }
                        else
                        {
                            chartrange = xlWorkSheet.get_Range("E30", "E30");
                            xlWorkSheet.Cells[30, 5] = dt_val.Rows[i]["A"].ToString();
                            chartrange.EntireColumn.AutoFit();
                            chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                            chartrange = xlWorkSheet.get_Range("F30", "F30");
                            xlWorkSheet.Cells[30, 6] = dt_val.Rows[i]["B"].ToString();
                            chartrange.EntireColumn.AutoFit();
                            chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                            chartrange = xlWorkSheet.get_Range("G30", "G30");
                            xlWorkSheet.Cells[30, 7] = dt_val.Rows[i]["ProvidentFund"].ToString();
                            chartrange.EntireColumn.AutoFit();
                            chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                        }

                        chartrange = xlWorkSheet.get_Range("H30", "H30");
                        xlWorkSheet.Cells[30, 8] = "";
                        chartrange.EntireColumn.AutoFit();
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                        chartrange = xlWorkSheet.get_Range("I30", "I30");
                        xlWorkSheet.Cells[30, 9] = "";
                        chartrange.EntireColumn.AutoFit();
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                        chartrange = xlWorkSheet.get_Range("J30", "J30");
                        xlWorkSheet.Cells[30, 10] = "remitted in full in E.P.F. ";
                        chartrange.EntireColumn.AutoFit();
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                    }

                    else if (dt_val.Rows[i]["Month"].ToString() == "February")
                    {
                        chartrange = xlWorkSheet.get_Range("A31", "A31");
                        xlWorkSheet.Cells[31, 1] = dt_val.Rows[i]["Month"].ToString() + " " + dt_val.Rows[i]["Year"].ToString();
                        chartrange.EntireColumn.AutoFit();
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                        chartrange = xlWorkSheet.get_Range("B31", "B31");
                        xlWorkSheet.Cells[31, 2] = dt_val.Rows[i]["PfSalary"].ToString();
                        chartrange.EntireColumn.AutoFit();
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                        chartrange = xlWorkSheet.get_Range("C31", "C31");
                        xlWorkSheet.Cells[31, 3] = dt_val.Rows[i]["ProvidentFund"].ToString();
                        chartrange.EntireColumn.AutoFit();
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                        chartrange = xlWorkSheet.get_Range("D31", "D31");
                        xlWorkSheet.Cells[31, 4] = dt_val.Rows[i]["ProvidentFund"].ToString();
                        chartrange.EntireColumn.AutoFit();
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                        if (Math.Round(Convert.ToDecimal(dt_val.Rows[i]["ProvidentFund"].ToString()), 0, MidpointRounding.ToEven) > 780)
                        {
                            chartrange = xlWorkSheet.get_Range("E31", "E31");
                            xlWorkSheet.Cells[31, 5] = "238.55";
                            chartrange.EntireColumn.AutoFit();
                            chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                            chartrange = xlWorkSheet.get_Range("F31", "F31");
                            xlWorkSheet.Cells[31, 6] = "541.45";
                            chartrange.EntireColumn.AutoFit();
                            chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                            chartrange = xlWorkSheet.get_Range("G31", "G31");
                            xlWorkSheet.Cells[31, 7] = "780";
                            chartrange.EntireColumn.AutoFit();
                            chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                        }
                        else
                        {
                            chartrange = xlWorkSheet.get_Range("E31", "E31");
                            xlWorkSheet.Cells[31, 5] = dt_val.Rows[i]["A"].ToString();
                            chartrange.EntireColumn.AutoFit();
                            chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                            chartrange = xlWorkSheet.get_Range("F31", "F31");
                            xlWorkSheet.Cells[31, 6] = dt_val.Rows[i]["B"].ToString();
                            chartrange.EntireColumn.AutoFit();
                            chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                            chartrange = xlWorkSheet.get_Range("G31", "G31");
                            xlWorkSheet.Cells[31, 7] = dt_val.Rows[i]["ProvidentFund"].ToString();
                            chartrange.EntireColumn.AutoFit();
                            chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                        }

                        chartrange = xlWorkSheet.get_Range("H31", "H31");
                        xlWorkSheet.Cells[31, 8] = "";
                        chartrange.EntireColumn.AutoFit();
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                        chartrange = xlWorkSheet.get_Range("I31", "I31");
                        xlWorkSheet.Cells[31, 9] = "";
                        chartrange.EntireColumn.AutoFit();
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                        chartrange = xlWorkSheet.get_Range("J31", "J31");
                        xlWorkSheet.Cells[31, 10] = "A/c. No. 1 (Provident Fund Contributions A/c) and in Account No.10(Employees's Pension Fund Contribut";
                        chartrange.EntireColumn.AutoFit();
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                    }



                }
                chartrange = xlWorkSheet.get_Range("A32", "A32");
                xlWorkSheet.Cells[32, 1] = "Total";
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                pfsal = 0;
                EPF = 0;
                A = 0;
                B = 0;
                EPF1 = 0;
                EPF_total = 0;
                for (int j = 0; j < dt_val.Rows.Count; j++)
                {

                    pfsal = pfsal + Convert.ToDecimal(dt_val.Rows[j]["PfSalary"].ToString());
                    EPF_total = EPF_total + Convert.ToDecimal(dt_val.Rows[j]["ProvidentFund"].ToString());
                    if (Math.Round(Convert.ToDecimal(dt_val.Rows[j]["ProvidentFund"].ToString()), 0, MidpointRounding.ToEven) > 780)
                    {
                        EPF1 = 780;
                        A = A + Convert.ToDecimal("238.55");
                        B = B + Convert.ToDecimal("541.45");
                        EPF = EPF + EPF1;
                    }
                    else
                    {
                        A = A + Convert.ToDecimal(dt_val.Rows[j]["A"].ToString());
                        B = B + Convert.ToDecimal(dt_val.Rows[j]["B"].ToString());
                        EPF = EPF + Convert.ToDecimal(dt_val.Rows[j]["ProvidentFund"].ToString());
                    }
                }
                chartrange = xlWorkSheet.get_Range("B32", "B32");
                xlWorkSheet.Cells[32, 2] = pfsal.ToString();
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                chartrange = xlWorkSheet.get_Range("C32", "C32");
                xlWorkSheet.Cells[32, 3] = EPF_total.ToString();
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                chartrange = xlWorkSheet.get_Range("D32", "D32");
                xlWorkSheet.Cells[32, 4] = EPF_total.ToString();
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                chartrange = xlWorkSheet.get_Range("E32", "E32");
                xlWorkSheet.Cells[32, 5] = Math.Round(A, 0, MidpointRounding.ToEven).ToString();
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                chartrange = xlWorkSheet.get_Range("F32", "F32");
                xlWorkSheet.Cells[32, 6] = Math.Round(B, 0, MidpointRounding.ToEven).ToString();
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                chartrange = xlWorkSheet.get_Range("G32", "G32");
                xlWorkSheet.Cells[32, 7] = Math.Round(EPF, 0, MidpointRounding.ToEven).ToString();
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                chartrange = xlWorkSheet.get_Range("H32", "H32");
                xlWorkSheet.Cells[32, 8] = "";
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                chartrange = xlWorkSheet.get_Range("I32", "I32");
                xlWorkSheet.Cells[32, 9] = "";
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                chartrange = xlWorkSheet.get_Range("J32", "J32");
                xlWorkSheet.Cells[32, 10] = "";
                chartrange.EntireColumn.AutoFit();
                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                chartrange = xlWorkSheet.get_Range("A33", "J33");
                xlWorkSheet.Cells[33, 1] = "Certifed that the difference between the total of the contributions shown under the columns(3) & (4) of the above table and arrived at on the total wages shown in column (2) at the prescribed rate is solely due to therounding off the contributions to the nearest rupee under the rules.";
                chartrange.WrapText = true;
                chartrange.EntireColumn.AutoFit();
                chartrange.Merge(true);
                //chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                chartrange = xlWorkSheet.get_Range("A34", "J34");
                xlWorkSheet.Cells[34, 1] = "";
                chartrange.EntireColumn.AutoFit();
                chartrange.Merge(true);
                //chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                chartrange = xlWorkSheet.get_Range("A35", "J35");
                xlWorkSheet.Cells[35, 1] = "";
                chartrange.EntireColumn.AutoFit();
                chartrange.Merge(true);
                //chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                chartrange = xlWorkSheet.get_Range("A36", "J36");
                xlWorkSheet.Cells[36, 1] = "* Contributions for the month of March paid in April";
                chartrange.EntireColumn.AutoFit();
                chartrange.Merge(true);
                //chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                chartrange = xlWorkSheet.get_Range("A39", "G39");
                xlWorkSheet.Cells[39, 1] = "Date :";
                chartrange.EntireColumn.AutoFit();
                chartrange.Merge(true);
                //chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                chartrange = xlWorkSheet.get_Range("H39", "J39");
                xlWorkSheet.Cells[39, 8] = "Signature of Employer";
                chartrange.EntireColumn.AutoFit();
                chartrange.Merge(true);
                //chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                chartrange = xlWorkSheet.get_Range("A40", "G40");
                xlWorkSheet.Cells[40, 1] = "Note :";
                chartrange.EntireColumn.AutoFit();
                chartrange.Merge(true);
                //chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                chartrange = xlWorkSheet.get_Range("H40", "J40");
                xlWorkSheet.Cells[40, 8] = "(Office Seal)";
                chartrange.EntireColumn.AutoFit();
                chartrange.Merge(true);
                //chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                chartrange = xlWorkSheet.get_Range("A41", "J41");
                xlWorkSheet.Cells[41, 1] = "In respect of the Form (3A) sent to the Regional Office during the course of the currency peiod for the purpose";
                chartrange.EntireColumn.AutoFit();
                chartrange.Merge(true);
                //chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                chartrange = xlWorkSheet.get_Range("A42", "J42");
                xlWorkSheet.Cells[42, 1] = "of final settlement of the accounts of the members who had left service details of date and reasons for leaving";
                chartrange.EntireColumn.AutoFit();
                chartrange.Merge(true);
                //chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                chartrange = xlWorkSheet.get_Range("A43", "J43");
                xlWorkSheet.Cells[43, 1] = "service and also a certificate as shown in the Remarks Colums should be added.";
                chartrange.EntireColumn.AutoFit();
                chartrange.Merge(true);
                //chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;


                xlWorkBook.SaveAs("Form3A-" + dt_EmpLoad.Rows[k]["EmpNo"].ToString() + ".xls", Excel.XlFileFormat.xlWorkbookNormal, misValue, misValue, misValue, misValue, Excel.XlSaveAsAccessMode.xlExclusive, misValue, misValue, misValue, misValue, misValue);
                xlWorkBook.Close(true, misValue, misValue);
                xlapp.Quit();
                saveFlag = true;

                Marshal.ReleaseComObject(xlapp);
                Marshal.ReleaseComObject(xlWorkBook);
                Marshal.ReleaseComObject(xlWorkSheet);
                //releaseObject(xlapp);
                //releaseObject(xlWorkBook);
                //releaseObject(xlWorkSheet);
            }
        }
        if (saveFlag == true)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Saved Successfully in Documents');", true);
        }



    }
    public void ReleaseComObject(object reference)
    {
        try
        {
            while (System.Runtime.InteropServices.Marshal.ReleaseComObject(reference) <= 0)
            {
            }
        }
        catch
        {
        }
    }    

    protected void ddlEmpType_SelectedIndexChanged(object sender, EventArgs e)
    {
        //DataTable dtempty = new DataTable();
        //ddlEmpNo.DataSource = dtempty;
        //ddlEmpNo.DataBind();
        //ddlEmpname.DataSource = dtempty;
        //ddlEmpname.DataBind();
        //DataTable dt = new DataTable();
        //dt = objdata.PF_EmpLoad(SessionCcode, SessionLcode, ddlEmpType.SelectedValue);
        //DataRow dr = dt.NewRow();
        //dr["EmpNo"] = ddlEmpType.SelectedItem.Text;
        //dr["EmpName"] = ddlEmpType.SelectedItem.Text;
        //dt.Rows.InsertAt(dr, 0);
        //ddlEmpNo.DataSource = dt;
        //ddlEmpNo.DataTextField = "EmpNo";
        //ddlEmpNo.DataValueField = "EmpNo";
        //ddlEmpNo.DataBind();
        //ddlEmpname.DataSource = dt;
        //ddlEmpname.DataTextField = "EmpName";
        //ddlEmpname.DataValueField = "EmpNo";
        //ddlEmpname.DataBind();
    }
    protected void ddlEmpNo_SelectedIndexChanged(object sender, EventArgs e)
    {
        //DataTable dtempName = new DataTable();
        //dtempName = objdata.BonusEmployeeName(ddlEmpNo.SelectedValue, SessionCcode, SessionLcode);
        //ddlEmpname.DataSource = dtempName;
        //ddlEmpname.DataTextField = "EmpName";
        //ddlEmpname.DataValueField = "EmpNo";
        //ddlEmpname.DataBind();
    }
    protected void ddlEmpname_SelectedIndexChanged(object sender, EventArgs e)
    {
        //DataTable dtempName = new DataTable();
        //dtempName = objdata.BonusEmployeeName(ddlEmpname.SelectedValue, SessionCcode, SessionLcode);
        //ddlEmpNo.DataSource = dtempName;
        //ddlEmpNo.DataTextField = "EmpNo";
        //ddlEmpNo.DataValueField = "EmpNo";
        //ddlEmpNo.DataBind();
    }
    protected void btnEmp_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptEmpDownload.aspx");
    }
    protected void btnatt_Click(object sender, EventArgs e)
    {
        Response.Redirect("AttenanceDownload.aspx");
    }
    protected void btnLeave_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptLeaveSample.aspx");
    }
    protected void btnOT_Click(object sender, EventArgs e)
    {
        Response.Redirect("OTDownload.aspx");
    }
    protected void btncontract_Click(object sender, EventArgs e)
    {
        Response.Redirect("ContractRPT.aspx");
    }
    protected void btnSal_Click(object sender, EventArgs e)
    {
        Response.Redirect("FrmDeductionDownload.aspx");
    }

}
