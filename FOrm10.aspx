﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="FOrm10.aspx.cs" Inherits="FOrm10_1" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
    <head id="Head1" runat="server">
        <title>Payroll Management Systems</title>
        <link href="new_css/main.css" rel="stylesheet" type="text/css" />
        <link href="new_css/colors/dark_blue.css" rel="stylesheet" type="text/css" />
        <link href="new_css/superfish.css" rel="stylesheet" type="text/css" />

        <script src="new_js/jquery-1.4.2.min.js" type="text/javascript"></script>
        <script src="new_js/jquery.hoverintent.minified.js" type="text/javascript"></script>
        <script src="new_js/superfish.js" type="text/javascript"></script>
        <script src="new_js/jquery.tools.min.js" type="text/javascript"></script>
        <%--<script src="new_js/jquery.simplemodal.js" type="text/javascript"></script>--%>
        <script src="new_js/jquery.notifybar.js" type="text/javascript"></script>
        <script src="new_js/jquery.tipsy.js" type="text/javascript"></script>
        <script src="new_js/enhance.js" type="text/javascript"></script>
        <script src="new_js/excanvas.js" type="text/javascript"></script>
        <script src="new_js/visualize.jquery.js" type="text/javascript"></script>
        <script src="new_js/jquery.collapsible.js" type="text/javascript"></script>
        <script src="new_js/jquery.autosuggest.packed.js" type="text/javascript"></script>
        <script src="new_js/platinum-admin.js" type="text/javascript"></script>
        <script type ="text/jscript" >
        function GoBack() {
            window.history.forward();
        }
   </script>
        <style>
            .sf-navbar li ul 
            {
	            width: 1100px; /*IE6 soils itself without this*/
	            margin: 0px auto;
	            margin-left:-20em;	            
            }
        </style>
    </head>
    <body onload="GoBack();">
        <form id="form1" runat="server">
            <cc1:ToolkitScriptManager runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true"
                ID="ScriptManager1" EnablePartialRendering="true">
            </cc1:ToolkitScriptManager>
            <div id="header">
                <div id="header-top">
                    <div id="logo">
	                    <h1>&nbsp;</h1>
	                    <span id="slogan">&nbsp;</span> 
	                </div>
                    <!-- end logo -->
                    <div id="login-info">
                        <img id="profile" alt="profile" src="./new_images/icons/user.png" />
                        <p id="top"><span id="name"><asp:Label ID="lblusername" runat="server"></asp:Label></span><br />
                        <span id="links">
                        <a id="message-link" href="#" title="click to see the messages!">
                        <span id="message-count">1</span> new message. </a></span></p>
	                    <div id="messages-box">
		                    <h4>Messages</h4>
	                        <h5><a id="new-message" href="#">New Message</a></h5>
	                        <hr />
	                        <ul id="messages">
		                        <li class="new"><a href="#">
		                            <img alt="user" src="./new_images/icons/sender.png" />
		                            <span class="sender">John Doe</span>
		                            <span class="description">Hi this is just a description...</span>
		                            <span class="date">20 minutes ago...</span> </a>
		                        </li>
		                        <li><a href="#">
		                            <img alt="user" src="./new_images/icons/sender.png" />
		                            <span class="sender">John Doe</span>
		                            <span class="description">Hi this is just a description...</span>
		                            <span class="date">24 minutes ago...</span> </a>
		                        </li>
		                        <li><a href="#">
		                            <img alt="user" src="./new_images/icons/sender.png" />
		                            <span class="sender">John Doe</span>
		                            <span class="description">Hi this is just a description...</span>
		                            <span class="date">50 minutes ago...</span> </a>
		                        </li>
		                        <li id="last"><a id="all-messages" href="#">See all messages!
		                            <span id="unreaded">8 unreaded.</span> </a>
		                        </li>
		                    </ul>
		                    <br class="clear" />
	                    </div><!-- end messages-box -->
	                    <a id="power" href="default.aspx" title="Logout">logout</a>
                    </div>
                    <!-- end login -->
                    <div id="nav">
	                    <ul class="sf-menu sf-navbar">
	                        <li><a id="dashboard" href="Dashboard.aspx">Dashboard</a>
	                            <ul>
		                            <li>&nbsp;
		                            </li>
	                            </ul>
	                        </li>
	                        <li><a id="Employee" href="EmployeeRegistration.aspx">Employee</a>
	                            <ul>
		                            <li><a href="EmployeeRegistration.aspx">Employee Registration</a> </li>					                    
		                            <li><a href="OfficalProfileDetails.aspx">Official Profile</a></li>
		                            <li><a href="SalaryMaster.aspx">Salary Master</a></li>
		                            <li><a href="LeaveAllocationEmployee.aspx">Leave Allocation</a></li>
		                            <li><a href="ApplyForLeave.aspx">Apply Leave</a></li>					                    
		                            <li><a href="SalaryRegistration.aspx">Salary Details</a></li>
		                            <li><a href="SalaryAdvance.aspx">Salary Advance</a></li>
		                            <li><a href="Settlement.aspx">Settlement</a></li>
		                            <li><a href="EmployeeDeactiveMode.aspx">Employee Re-Activate</a></li>
		                            					                    
	                            </ul>
	                        </li>
	                        <li><a id="Bonus" href="BonusForAll.aspx">Bonus</a>
	                            <ul>
		                            <li><a href="BonusForAll.aspx">Bonus</a></li>
		                            <li><a href="StandardBonus.aspx">Group Bonus</a></li>					                    
	                            </ul>
	                        </li>
	                        <li class="current"><a id="ESI_PF" href="PFForm5.aspx">ESI & PF</a>
	                            <ul>
		                            <li><a href="PFForm5.aspx">PF Form 5</a></li>
		                            <li class="current"><a href="FOrm10.aspx">Form 10</a></li>
		                            <li><a href="PFForm3Anew.aspx">PF Form 3A</a></li>
		                            <li><a href="PFform6A.aspx">PF Form 6A</a></li>
		                            <li><a href="ESIForm7.aspx">ESI Form 7</a></li>
	                            </ul>
	                        </li>
	                         <li><a id="Master" href="MstEmployeeType.aspx">Masters</a>
	                            <ul>
		                            <li><a href="MstBank.aspx">Bank Master</a></li>
		                            <li><a href="MstDepartment.aspx">Department Master</a></li>
		                            <li><a href="MstEmployeeType.aspx">Employee Type Master</a></li>
		                            <li><a href="MstInsuranceCompany.aspx">Insurance Master</a></li>
		                            <li><a href="MstLeave.aspx">Leave Master</a></li>
		                            <li><a href="MstProbationPeriod.aspx">Probationary Period Master</a></li>
		                             <li><a href="MstQualification.aspx">Qualification Master</a></li>
		                              <li><a href="UserRegistration.aspx">User Master</a></li>
	                            </ul>
	                        </li>
	                         <li><a id="Reports" href="RptDepartmentwiseEmployee.aspx">Reports</a>
	                            <ul>
		                            <li><a href="ContractBreak.aspx">Contract Details</a></li>
		                            <li><a href="RptAttendanceReport.aspx">Attendance Report</a></li>
		                            <li><a href="RptDepartmentwiseEmployee.aspx">Employee Details</a></li>
		                            <li><a href="RptProbationperiod.aspx">Probationary Period</a></li>
		                            <li><a href="RptResign.aspx">Resignation Reports</a></li>
		                            <li><a href="PaySlipGen.aspx">Payslip</a></li>
		                            <%--<li><a href="RptSalary.aspx">Salary Details</a></li>--%>
	                            </ul>
	                        </li>
	                        <li><a id="Upload" href="AttenanceUpload.aspx">Upload</a>
	                            <ul>
		                            <li><a href="AttenanceUpload.aspx">Attenance Upload</a></li>
		                            <li><a href="UploadEmployee.aspx">Employee Upload</a></li>	
	                            </ul>
	                        </li>
                        </ul>
                    </div>
                    <!-- end div#nav -->
                </div>
                <!-- end div#header-top -->
                <div class="breadCrumb">
                    <ul>
                        <li class="first"><a href="#">Home</a> </li>
                        <li><a href="#">Users</a> </li>
                        <li><a href="#">Roles</a> </li>
                        <li class="last">Form 10 </li>
                    </ul>
                </div>
                <!-- end div#breadCrumb -->
                <%--<div id="search-box">
                    <form id="searchform" action="" method="get">
                        <fieldset class="search"><span>Search</span>
                        <input class="box" type="text" />
                        <button class="btn" title="Submit Search">Search</button></fieldset>
                    </form>
                </div>--%>
                <!-- end div#search-box -->
            </div>
            <!-- end header -->
            <div id="page-wrap">
                <div id="right-sidebar">
                    <div class="innerdiv">
	                    <h2 class="head">Comments</h2>
	                    <div class="innercontent clear">
		                    <h4>Recent Comments</h4>
		                    <ul id="comments" class="tooltip-enabled">
		                        <li><a href="#" title="this comment is from John Doe">
		                            <img alt="user" src="./new_images/icons/sender.png" />
		                            <span class="sender">John Doe</span>
		                            <span class="description">Hey i liked your theme!</span>
		                            <span class="date">20 minutes ago...</span> </a>
		                            <span class="floatright"><a title="Aprrove this comment" href="#">Approve</a> | 
		                            <a title="Delete this comment"  href="#">Delete</a>
		                            </span>
		                            <br class="clear" />
		                        </li>
		                        <li><a href="#" title="again this guy!">
		                            <img alt="user" src="./new_images/icons/sender.png" />
		                            <span class="sender">John Doe</span>
		                            <span class="description">Hi, i need some help.</span>
		                            <span class="date">24 minutes ago...</span> </a>
		                            <span class="floatright"><a title="Aprrove this comment"  href="#">Approve</a> | 
		                            <a title="Delete this comment"  href="#">Delete</a>
		                            </span>
		                            <br class="clear" />
		                        </li>
		                        <li><a href="#" title="is he a spammer?">
		                            <img alt="user" src="./new_images/icons/sender.png" />
		                            <span class="sender">John Doe</span>
		                            <span class="description">Thank you for your help.</span>
		                            <span class="date">50 minutes ago...</span> </a>
		                            <span class="floatright"><a title="Aprrove this comment"  href="#">Approve</a> | 
		                            <a title="Delete this comment"  href="#">Delete</a>
		                            </span>
		                            <br class="clear" />
		                        </li>
		                        <li class="last"><a class="all-messages" title="See all comments awaiting confirmation." href="#">See all comments!
		                            <span class="unreaded">8 awaiting confirm.</span> </a>
		                        </li>
		                    </ul>
	                    </div>
                    </div>
                    <div class="clear"></div>                    
                </div>
	                <!-- end right-sidebar -->
	                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
	                    <Triggers>
	                        <asp:PostBackTrigger ControlID="btnSearch" />
	                    </Triggers>
	                    <ContentTemplate>
	                        <div id="main-content">
                                <br /><br /><br /><br /> 
	                            <div class="clear_body">
                                    <div class="innerdiv">
                                        <div class="innercontent">
                                            <!-- tab "panes" -->				                
                                                <div>
                                                    <table class="full">
                                                        <thead>
                                                            <tr align="center">
                                                                <th colspan="5" style="text-align:center;"><h4>FORM 10</h4></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td>
                                                                <asp:Label ID="lblcategory" runat="server" Text="Category" Font-Bold="true" ></asp:Label>
                                                                </td>
                                                                <td>
                                                                <asp:DropDownList ID="ddlCategory" runat="server" AutoPostBack="true" Width="140" 
                                                                        Height="25" onselectedindexchanged="ddlCategory_SelectedIndexChanged" ></asp:DropDownList>
                                                                </td>
                                                                <td>
                                                                <asp:Label ID="lbldpt" runat="server" Text="Department" Font-Bold="true" ></asp:Label>
                                                                </td>
                                                                <td colspan="2">
                                                                <asp:DropDownList ID="ddldpt" runat="server" AutoPostBack="true" Width="140" 
                                                                        Height="25" onselectedindexchanged="ddldpt_SelectedIndexChanged" ></asp:DropDownList>
                                                                </td>
                                                            </tr>
                                                            <tr >
                                                                <td>
                                                                <asp:Label ID="lblfinance" runat="server" Text="Financial Year" Font-Bold="true" ></asp:Label>
                                                                </td>
                                                                <td>
                                                                <asp:DropDownList ID="ddlFinance" runat="server" AutoPostBack="true" Width="140" Height="28" ></asp:DropDownList>
                                                                </td>
                                                                <td>
                                                                <asp:Label ID="lblMonths" runat="server" Text="Month" Font-Bold="true" ></asp:Label>
                                                                </td>
                                                                <td>
                                                                <asp:DropDownList ID="ddlMonths" runat="server" AutoPostBack="true" Width="140" Height="28" ></asp:DropDownList>
                                                                </td>
                                                                <td colspan="1" >
                                                                <asp:Button ID="btnSearch" runat="server" Text="Search" Width="75" Height="28" 
                                                                        onclick="btnSearch_Click" CssClass="button green" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="5">
                                                                <asp:GridView ID="gvform10" runat="server" AutoGenerateColumns="false" >
                                                                <Columns>
                                                                <asp:TemplateField>
                                                                <HeaderTemplate>Sl.No</HeaderTemplate>
                                                                <ItemTemplate><%# Container.DataItemIndex + 1   %></ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField>
                                                                <HeaderTemplate>Account No</HeaderTemplate>
                                                                <ItemTemplate>
                                                                <asp:Label ID="lblacc" runat="server" Text='<%# Eval("PFAcc") %>' ></asp:Label>
                                                                </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField>
                                                                <HeaderTemplate>Name of the Member (in Block Letters)</HeaderTemplate>
                                                                <ItemTemplate>
                                                                <asp:Label ID="lblmember" runat="server" Text='<%# Eval("EmpName") %>' ></asp:Label>
                                                                </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField>
                                                                <HeaderTemplate>Father's Name or Husband's Name (in case of married Women)</HeaderTemplate>
                                                                <ItemTemplate>
                                                                <asp:Label ID="lblFatherName" runat="server" Text='<%# Eval("FatherName") %>' ></asp:Label>
                                                                </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField>
                                                                <HeaderTemplate>Date of Leaving Service</HeaderTemplate>
                                                                <ItemTemplate>
                                                                <asp:Label ID="lblDateofLeaving" runat="server" Text='<%# Eval("ResignDate") %>' ></asp:Label>
                                                                </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField>
                                                                <HeaderTemplate>reason for Leaving Service</HeaderTemplate>
                                                                <ItemTemplate>
                                                                <asp:Label ID="lblreason" runat="server" Text='<%# Eval("Reason") %>' ></asp:Label>
                                                                </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField>
                                                                <HeaderTemplate>Remarks</HeaderTemplate>
                                                                <ItemTemplate>
                                                                <asp:Label ID="Remarks" runat="server" ></asp:Label>
                                                                </ItemTemplate>
                                                                </asp:TemplateField>
                                                                </Columns>
                                                                </asp:GridView>
                                                                </td>                                                              
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
	                    </ContentTemplate>
	                </asp:UpdatePanel>          
            </div>
            <!-- end page-wrap -->
            <div id="footer">
                <ul>
                    <li>Copyright &copy; 2012. All rights reserved.</li>
                    <li>Powered by <a href="http://www.altius.co.in" target="_blank">Altius Infosystems</a></li>
                </ul>
            </div>
            <!-- end footer -->
        </form>
    </body>
</html>

