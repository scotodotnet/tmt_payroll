﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="RptProbationperiod.aspx.cs" Inherits="RptProbationperiod_1" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
    <head runat="server">
        <title>Payroll Management Systems</title>
        <link href="new_css/main.css" rel="stylesheet" type="text/css" />
        <link href="new_css/colors/dark_blue.css" rel="stylesheet" type="text/css" />
        <link href="new_css/superfish.css" rel="stylesheet" type="text/css" />

        <script src="new_js/jquery-1.4.2.min.js" type="text/javascript"></script>
        <script src="new_js/jquery.hoverintent.minified.js" type="text/javascript"></script>
        <script src="new_js/superfish.js" type="text/javascript"></script>
        <script src="new_js/jquery.tools.min.js" type="text/javascript"></script>
        <%--<script src="new_js/jquery.simplemodal.js" type="text/javascript"></script>--%>
        <script src="new_js/jquery.notifybar.js" type="text/javascript"></script>
        <script src="new_js/jquery.tipsy.js" type="text/javascript"></script>
        <script src="new_js/enhance.js" type="text/javascript"></script>
        <script src="new_js/excanvas.js" type="text/javascript"></script>
        <script src="new_js/visualize.jquery.js" type="text/javascript"></script>
        <script src="new_js/jquery.collapsible.js" type="text/javascript"></script>
        <script src="new_js/jquery.autosuggest.packed.js" type="text/javascript"></script>
        <script src="new_js/platinum-admin.js" type="text/javascript"></script>
        <script type ="text/jscript" >
        function GoBack() {
            window.history.forward();
        }
   </script>
        <style>
            .sf-navbar li ul 
            {
	            width: 1100px; /*IE6 soils itself without this*/
	            margin: 0px auto;
	            margin-left:-20em;	            
            }
        </style>
    </head>
    <body onload="GoBack();">
        <form id="form1" runat="server">
            <cc1:ToolkitScriptManager runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true"
                ID="ScriptManager1" EnablePartialRendering="true">
            </cc1:ToolkitScriptManager>
            <div id="header">
                <div id="header-top">
                    <div id="logo">
	                    <h1>&nbsp;</h1>
	                    <span id="slogan">&nbsp;</span> 
	                </div>
                    <!-- end logo -->
                    <div id="login-info">
                        <img id="profile" alt="profile" src="./new_images/icons/user.png" />
                        <p id="top"><span id="name"><asp:Label ID="lblusername" runat="server"></asp:Label></span><br />
                        <span id="links">
                        <%--<a id="message-link" href="#" title="click to see the messages!">
                        <span id="message-count">1</span> new message. </a>--%></span></p>
                        <div>
                            <asp:Label ID="lblComany" runat="server" Text="" Font-Bold="true"></asp:Label>
                        </div>
	                    <div id="messages-box">
		                    <h4>Messages</h4>
	                        <h5><a id="new-message" href="#">New Message</a></h5>
	                        <hr />
	                        <ul id="messages">
		                        <li class="new"><a href="#">
		                            <img alt="user" src="./new_images/icons/sender.png" />
		                            <span class="sender">John Doe</span>
		                            <span class="description">Hi this is just a description...</span>
		                            <span class="date">20 minutes ago...</span> </a>
		                        </li>
		                        <li><a href="#">
		                            <img alt="user" src="./new_images/icons/sender.png" />
		                            <span class="sender">John Doe</span>
		                            <span class="description">Hi this is just a description...</span>
		                            <span class="date">24 minutes ago...</span> </a>
		                        </li>
		                        <li><a href="#">
		                            <img alt="user" src="./new_images/icons/sender.png" />
		                            <span class="sender">John Doe</span>
		                            <span class="description">Hi this is just a description...</span>
		                            <span class="date">50 minutes ago...</span> </a>
		                        </li>
		                        <li id="last"><a id="all-messages" href="#">See all messages!
		                            <span id="unreaded">8 unreaded.</span> </a>
		                        </li>
		                    </ul>
		                    <br class="clear" />
	                    </div><!-- end messages-box -->
	                    <a id="power" href="Default.aspx" title="Logout">logout</a>
                    </div>
                    <!-- end login -->
                    <div id="nav">
	                    <ul class="sf-menu sf-navbar">
	                        <li><a id="dashboard" href="Dashboard.aspx">Dashboard</a>
	                            <ul>
		                            <li>&nbsp;
		                            </li>
	                            </ul>
	                        </li>
	                        <li><a id="Employee" href="EmployeeRegistration.aspx">Employee</a>
	                            <ul>
		                            <li><a href="EmployeeRegistration.aspx">Employee Registration</a> </li>					                    
		                            <li><a href="OfficalProfileDetails.aspx">Official Profile</a></li>
		                            <li><a href="SalaryMaster.aspx">Salary Master</a></li>
		                            <li><a href="LeaveAllocationEmployee.aspx">Leave Allocation</a></li>
		                            <li><a href="ApplyForLeave.aspx">Apply Leave</a></li>						                    
		                            <li><a href="SalaryRegistration.aspx">Salary Details</a></li>
		                            <li><a href="SalaryAdvance.aspx">Salary Advance</a></li>
		                             <li><a href="Settlement.aspx">Settlement</a></li>
		                            <li><a href="EmployeeDeactiveMode.aspx">Employee Re-Activate</a></li>					                    					                    
	                            </ul>
	                        </li>
	                       <li class="current"><a id="A2" href="RptDepartmentwiseEmployee.aspx">Reports</a>
	                            <ul>
		                            <li><a href="RptDepartmentwiseEmployee.aspx">Employee Details</a></li>
		                            <li><a href="RptAttendanceReport.aspx">Attendance Report</a></li>
		                            <li  class="current"><a href="RptProbationperiod.aspx">Probationary Period</a></li>
		                            <li><a href="ContractBreak.aspx">Contract Details</a></li>
		                            <li><a href="RptResign.aspx">Resignation Reports</a></li>
		                            <li><a href="PaySlipGen.aspx">Payslip</a></li>
		                            <%--<li><a href="RptSalary.aspx">Salary Details</a></li>--%>
	                            </ul>
	                        </li>
	                        <li><a id="OT" href="OverTime.aspx">OT</a>
	                            <ul>
	                                <li><a href="OverTime.aspx">OT</a></li>
	                            </ul>
	                        </li>
	                        <li><a id="Master" href="MstEmployeeType.aspx">Masters</a>
	                            <ul>
		                            <li><a href="MstEmployeeType.aspx">Employee Type</a></li>  
		                            <li><a href="MstLeave.aspx">Leave</a></li>
		                            <li><a href="MstProbationPeriod.aspx">Probationary Period</a></li>
		                            <li><a href="UserRegistration.aspx">User</a></li>
		                            <li><a href="MSTPFESI.aspx">PF</a></li>
		                            <li><a href="MstBank.aspx">Bank</a></li>
		                            <li><a href="MstDepartment.aspx">Department</a></li>
		                            <li><a href="MstInsuranceCompany.aspx">Insurance</a></li>
		                            <li><a href="MstQualification.aspx">Qualification</a></li>
	                            </ul>
	                        </li>
	                        <li><a id="A1" href="BonusForAll.aspx">Bonus</a>
	                            <ul>
		                            <li><a href="BonusForAll.aspx">Bonus</a></li>
		                            <li><a href="StandardBonus.aspx">Group Bonus</a></li>					                    
	                            </ul>
	                        </li>
	                        <li><a id="ESI_PF" href="PFForm5.aspx">ESI & PF</a>
	                            <ul>
		                            <li><a href="PFForm5.aspx">PF Form 5</a></li>
		                            <li><a href="PFDownload.aspx">PF Download</a></li>
		                            <li><a href="FOrm10.aspx">Form 10</a></li>
		                            <li><a href="PFForm3Anew.aspx">PF Form 3A</a></li>
		                            <li><a href="PFform6A.aspx">PF Form 6A</a></li>
		                            <li><a href="ESIForm7.aspx">ESI Form 7</a></li>
	                            </ul>
	                        </li>
	                        <li><a id="Upload" href="AttenanceUpload.aspx">Upload</a>
	                            <ul>
		                            <li><a href="AttenanceUpload.aspx">Attenance Upload</a></li>
		                            <li><a href="UploadEmployee.aspx">Employee Upload</a></li>	
	                            </ul>
	                        </li>
                        </ul>
                    </div>
                    <!-- end div#nav -->
                </div>
                <!-- end div#header-top -->
                <div class="breadCrumb">
                    <ul>
                        <li class="first"><a href="#">Home</a> </li>
                        <li><a href="#">Users</a> </li>
                        <li><a href="#">Roles</a> </li>
                        <li class="last">Probationary Period</li>
                    </ul>
                </div>
                <!-- end div#breadCrumb -->
                <%--<div id="search-box">
                    <form id="searchform" action="" method="get">
                        <fieldset class="search"><span>Search</span>
                        <input class="box" type="text" />
                        <button class="btn" title="Submit Search">Search</button></fieldset>
                    </form>
                </div>--%>
                <!-- end div#search-box -->
            </div>
            <!-- end header -->
            <div id="page-wrap">
                <div id="right-sidebar">
                    <div class="innerdiv">
	                    <h2 class="head">Comments</h2>
	                    <div class="innercontent clear">
		                    <h4>Recent Comments</h4>
		                    <ul id="comments" class="tooltip-enabled">
		                        <li><a href="#" title="this comment is from John Doe">
		                            <img alt="user" src="./new_images/icons/sender.png" />
		                            <span class="sender">John Doe</span>
		                            <span class="description">Hey i liked your theme!</span>
		                            <span class="date">20 minutes ago...</span> </a>
		                            <span class="floatright"><a title="Aprrove this comment" href="#">Approve</a> | 
		                            <a title="Delete this comment"  href="#">Delete</a>
		                            </span>
		                            <br class="clear" />
		                        </li>
		                        <li><a href="#" title="again this guy!">
		                            <img alt="user" src="./new_images/icons/sender.png" />
		                            <span class="sender">John Doe</span>
		                            <span class="description">Hi, i need some help.</span>
		                            <span class="date">24 minutes ago...</span> </a>
		                            <span class="floatright"><a title="Aprrove this comment"  href="#">Approve</a> | 
		                            <a title="Delete this comment"  href="#">Delete</a>
		                            </span>
		                            <br class="clear" />
		                        </li>
		                        <li><a href="#" title="is he a spammer?">
		                            <img alt="user" src="./new_images/icons/sender.png" />
		                            <span class="sender">John Doe</span>
		                            <span class="description">Thank you for your help.</span>
		                            <span class="date">50 minutes ago...</span> </a>
		                            <span class="floatright"><a title="Aprrove this comment"  href="#">Approve</a> | 
		                            <a title="Delete this comment"  href="#">Delete</a>
		                            </span>
		                            <br class="clear" />
		                        </li>
		                        <li class="last"><a class="all-messages" title="See all comments awaiting confirmation." href="#">See all comments!
		                            <span class="unreaded">8 awaiting confirm.</span> </a>
		                        </li>
		                    </ul>
	                    </div>
                    </div>
                    <div class="clear"></div>                    
                </div>
	            <!-- end right-sidebar -->
	            <asp:UpdatePanel ID="UpdatePanel1" runat="server">	                
	                <Triggers>	                	                
	                    <asp:PostBackTrigger ControlID="btnexport" />
	                </Triggers>
                    <ContentTemplate>
                        <div id="main-content">
                            <br /><br /><br /><br /><br />
                            <div class="clear_body">
                                <div class="innerdiv">
                                    <div class="innercontent">
                                        <!-- tab "panes" -->
                                        <div>
                                            <table class="full">
                                                <thead>
				                                    <tr>
					                                    <th colspan="4" style="text-align:center;"><h5>Probation Period Report</h5></th>
				                                    </tr>
				                                </thead>
				                                <tbody class="tooltip-enabled">
				                                    <tr>
                                                        <td><asp:Label ID="lblcategory" runat="server" Text="Category" Font-Bold="true"></asp:Label></td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlCategory" runat="server" Width="215" Height="25" 
                                                                AutoPostBack="true" onselectedindexchanged="ddlCategory_SelectedIndexChanged">
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td><asp:Label ID="lblDepartment" runat="server" Text="Department" Font-Bold="true" ></asp:Label></td>
                                                        <td>
                                                            <asp:DropDownList ID="ddldepartment" runat="server" AutoPostBack="true" Width="215" 
                                                                Height="25" onselectedindexchanged="Department_SelectedIndexchanged">
                                                            </asp:DropDownList>
                                                        </td>                                                        
                                                    </tr>
                                                    <tr>
                                                        <td><asp:Label ID="lblempNo" runat="server" Text="Employee No" Font-Bold="true" ></asp:Label></td>
                                                        <td>
                                                            <asp:TextBox ID="txtempNo" runat="server" ontextchanged="txtempNo_TextChanged" ></asp:TextBox>
                                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR1" TargetControlID="txtempNo"
                                                                runat="server" FilterMode="ValidChars" FilterType="UppercaseLetters,LowercaseLetters,Numbers,Custom" ValidChars="">
                                                            </cc1:FilteredTextBoxExtender>
                                                        </td>
                                                        <td><asp:Label ID="lblexist" runat="server" Text="Existing No" ></asp:Label></td>
                                                        <td>
                                                            <asp:TextBox ID="txtexist" runat="server" ontextchanged="txtexist_TextChanged" ></asp:TextBox>
                                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR2" TargetControlID="txtexist"
                                                                runat="server" FilterMode="ValidChars" FilterType="UppercaseLetters,LowercaseLetters,Numbers,Custom" ValidChars="">
                                                            </cc1:FilteredTextBoxExtender>
                                                            <asp:Button ID="btSearch" runat="server" Text="Search" onclick="btSearch_Click" 
                                                                Width="78" Height="27" CssClass="button green"/>
                                                        </td>                                                        
                                                    </tr>
                                                    <tr>
                                                        <td><asp:Label ID="lblformat"  runat="server" Text="Format" Font-Bold="true"></asp:Label></td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlrptReport" runat="server" Width="215" Height="25" >
                                                                <asp:ListItem Text="--Select---" Value="0"></asp:ListItem>
                                                                <asp:ListItem Text="Excel" Value="1"></asp:ListItem>
                                                                <asp:ListItem Text="PDF" Value ="2"></asp:ListItem>
                                                                <asp:ListItem Text="Word" Value="3"></asp:ListItem>
                                                            </asp:DropDownList>
                                                            <asp:Button ID="btnexport" runat="server" Text="Export" onclick="btnexport_Click" 
                                                                Height="28" Width="78" CssClass="button green"/>
                                                        </td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                    </tr>
				                                </tbody>
				                            </table>
				                            <br />
				                            <table class="full">
				                                <tbody>
				                                    <tr>
                                                        <td colspan="4">
                                                            <asp:GridView ID="gridprobation" runat="server"  AutoGenerateColumns="false" Width="100%">
                                                                <Columns>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Sl No</HeaderTemplate>
                                                                        <ItemTemplate><%# Container.DataItemIndex + 1 %></ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate> Emp Number</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblempno" runat="server" Text='<%# Eval("EmpNo") %>' ></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Emp Name</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblEmpName" runat="server" Text='<%# Eval("EmpName") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Date of Joining</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lbldepartmentnm" runat="server" Text='<%# Eval("DOJ") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Designation</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lbldesignation" runat="server" Text='<%# Eval("Designation") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Expiry date</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblstaff" runat="server" Text='<%# Eval("ExpiryDate") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                            </asp:GridView>
                                                        </td>
                                                    </tr>
				                                </tbody>
				                            </table>
				                        </div>
				                        <!-- end tab "panes" -->
				                    </div>
				                    <!-- end innercontent -->
				                </div>
				                <!-- end innerdiv -->
				            </div>
				            <!-- end clear_body -->
				        </div>
				        <!-- end main-content -->
				    </ContentTemplate>
				</asp:UpdatePanel>
            </div>
            <!-- end page-wrap -->
            <div id="footer">
                <ul>
                    <li>Copyright &copy; 2012. All rights reserved.</li>
                    <li>Powered by <a href="http://www.altius.co.in" target="_blank">Altius Infosystems</a></li>
                </ul>
            </div>
            <!-- end footer -->
        </form>
    </body>
</html>
