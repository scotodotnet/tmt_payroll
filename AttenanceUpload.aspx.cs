﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Configuration;
using System.Data.SqlClient;
using System.Data.OleDb;
using Payroll;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class AttenanceUpload_1 : System.Web.UI.Page
{
    string SessionAdmin;
    string DepartmentCode;
    string Name_Upload;
    string Name_Upload1;
    BALDataAccess objdata = new BALDataAccess();
    AttenanceClass objatt = new AttenanceClass();
    string SessionCcode;
    string SessionLcode;
    string WagesType;
    string StafforLabor = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        if (SessionAdmin == "2")
        {
            Response.Redirect("EmployeeRegistration.aspx");

        }
        lblComany.Text = SessionCcode + " - " + SessionLcode;
        SessionAdmin = Session["Isadmin"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        if (!IsPostBack)
        {
            Months_load();
            int currentYear = Utility.GetFinancialYear;
            for (int i = 0; i < 10; i++)
            {
                ddlfinance.Items.Add(new ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                //  ddlShowYear.Items.Add(new ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                currentYear = currentYear - 1;
            }
        }
        lblusername.Text = Session["Usernmdisplay"].ToString();
    }
    public void Months_load()
    {
        DataTable dt = new DataTable();
        dt = objdata.months_load();
        ddlMonths.DataSource = dt;
        ddlMonths.DataTextField = "Months";
        ddlMonths.DataValueField = "Months";
        ddlMonths.DataBind();
    }
    protected void btnUpload_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        try
        {

            if (ddlMonths.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Month.');", true);
                //System.Windows.Forms.MessageBox.Show("Select the Month", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            else if ((txtdays.Text.Trim() == "") || (txtdays.Text.Trim() == null))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Days.');", true);
                //System.Windows.Forms.MessageBox.Show("Enter the Working days in a Month", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            else if (Convert.ToInt32(txtdays.Text) == 0)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the days Properly.');", true);
                //System.Windows.Forms.MessageBox.Show("Enter the Days Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            else if (txtFrom.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the from Date Properly.');", true);
                //System.Windows.Forms.MessageBox.Show("Enter the Days Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            else if (txtTo.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the To Date Properly.');", true);
                //System.Windows.Forms.MessageBox.Show("Enter the Days Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            else if (txtNfh.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the To NFh Days Properly.');", true);
                //System.Windows.Forms.MessageBox.Show("Enter the Days Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            if (rbsalary.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Wges Type.');", true);
                ErrFlag = true;
            }
            //else if (Convert.ToInt32(txtdays.Text) > 31)
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the days properly.');", true);
            //    //System.Windows.Forms.MessageBox.Show("Enter the Days Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            //    ErrFlag = true;
            //}
            if (FileUpload.HasFile)
            {
                FileUpload.SaveAs(Server.MapPath("Upload/" + FileUpload.FileName));

            }
            if (!ErrFlag)
            {
                decimal total = (Convert.ToDecimal(txtdays.Text.Trim()));
                decimal days = 0;
                if ((ddlMonths.SelectedValue == "January") || (ddlMonths.SelectedValue == "March") || (ddlMonths.SelectedValue == "May") || (ddlMonths.SelectedValue == "July") || (ddlMonths.SelectedValue == "August") || (ddlMonths.SelectedValue == "October") || (ddlMonths.SelectedValue == "December"))
                {
                    days = 31;
                }
                else if ((ddlMonths.SelectedValue == "April") || (ddlMonths.SelectedValue == "June") || (ddlMonths.SelectedValue == "September") || (ddlMonths.SelectedValue == "November"))
                {
                    days = 30;
                }
               
                else if (ddlMonths.SelectedValue == "February")
                {
                    int yrs = (Convert.ToInt32(ddlfinance.SelectedValue) + 1);
                    if ((yrs % 4) == 0)
                    {
                        days = 29;
                    }
                    else
                    {
                        days = 28;
                    }
                }
                if (total > days)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the days properly.');", true);
                    ErrFlag = true;
                }
                if (!ErrFlag)
                {
                    string connectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + Server.MapPath("Upload/" + FileUpload.FileName) + ";" + "Extended Properties=Excel 8.0;";
                    OleDbConnection sSourceConnection = new OleDbConnection(connectionString);
                    DataTable dts = new DataTable();
                    using (sSourceConnection)
                    {
                        sSourceConnection.Open();

                        OleDbCommand command = new OleDbCommand("Select * FROM [Sheet1$];", sSourceConnection);
                        sSourceConnection.Close();

                        using (OleDbCommand cmd = sSourceConnection.CreateCommand())
                        {
                            command.CommandText = "Select * FROM [Sheet1$];";
                            sSourceConnection.Open();
                        }
                        using (OleDbDataReader dr = command.ExecuteReader())
                        {

                            if (dr.HasRows)
                            {

                            }

                        }
                        OleDbDataAdapter objDataAdapter = new OleDbDataAdapter();
                        objDataAdapter.SelectCommand = command;
                        DataSet ds = new DataSet();

                        objDataAdapter.Fill(ds);
                        DataTable dt = new DataTable();
                        dt = ds.Tables[0];
                        for (int i = 0; i < dt.Columns.Count; i++)
                        {
                            dt.Columns[i].ColumnName = dt.Columns[i].ColumnName.Replace(" ", string.Empty).ToString();

                        }
                        string constr = ConfigurationManager.AppSettings["ConnectionString"];
                        for (int j = 0; j < dt.Rows.Count; j++)
                        {
                            SqlConnection cn = new SqlConnection(constr);
                            //string Department = dt.Rows[j][0].ToString();
                            //string MachineID = dt.Rows[j][1].ToString();
                            //string EmpNo = dt.Rows[j][2].ToString();
                            string ExistingCode = dt.Rows[j][0].ToString();
                            string FirstName = dt.Rows[j][1].ToString();
                            string WorkingDays = dt.Rows[j][2].ToString();
                            string CL = dt.Rows[j][3].ToString();
                            string Home = dt.Rows[j][4].ToString();
                            //string AbsentDays = dt.Rows[j][5].ToString();
                            //string weekoff = dt.Rows[j][6].ToString();
                            string Half = dt.Rows[j][5].ToString();
                            string Full = dt.Rows[j][6].ToString();
                            string ThreeSided = dt.Rows[j][7].ToString();
                            string Rest = dt.Rows[j][8].ToString();
                            //if (Department == "")
                            //{
                            //    j = j + 2;
                            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Department. The Row Number is " + j + "');", true);
                            //    //System.Windows.Forms.MessageBox.Show("Enter the Department. The Row number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                            //    ErrFlag = true;
                            //}
                            //else if (MachineID == "")
                            //{
                            //    j = j + 2;
                            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Machine ID. The Row Number is " + j + "');", true);
                            //    //System.Windows.Forms.MessageBox.Show("Enter the Machine ID. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                            //    ErrFlag = true;
                            //}
                            //else if (EmpNo == "")
                            //{
                            //    j = j + 2;
                            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Employee Number. The Row Number is " + j + "');", true);
                            //    //System.Windows.Forms.MessageBox.Show("Enter the Employee Number. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                            //    ErrFlag = true;
                            //}
                            if (ExistingCode == "")
                            {
                                j = j + 2;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Existing Code. The Row Number is " + j + "');", true);
                                //System.Windows.Forms.MessageBox.Show("Enter the Existing Code. The Roe Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                ErrFlag = true;
                            }
                            else if (FirstName == "")
                            {
                                j = j + 2;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Name. The Row Number is " + j + "');", true);
                                //System.Windows.Forms.MessageBox.Show("Enter the Name. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                ErrFlag = true;
                            }
                            else if (WorkingDays == "")
                            {
                                j = j + 2;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Working Days. The Row Number is " + j + "');", true);
                                //System.Windows.Forms.MessageBox.Show("Enter the Name. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                ErrFlag = true;
                            }
                            else if (Home == "")
                            {
                                j = j + 2;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Working Days. The Row Number is " + j + "');", true);
                                //System.Windows.Forms.MessageBox.Show("Enter the Name. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                ErrFlag = true;
                            }
                            else if (CL == "")
                            {
                                j = j + 2;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the CL Days. The Row Number is " + j + "');", true);
                                //System.Windows.Forms.MessageBox.Show("Enter the Name. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                ErrFlag = true;
                            }
                            else if (Half == "")
                            {
                                j = j + 2;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the O/N Days. The Row Number is " + j + "');", true);
                                //System.Windows.Forms.MessageBox.Show("Enter the Name. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                ErrFlag = true;
                            }
                            else if (Full == "")
                            {
                                j = j + 2;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the F/N Days. The Row Number is " + j + "');", true);
                                //System.Windows.Forms.MessageBox.Show("Enter the Name. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                ErrFlag = true;
                            }
                            else if (ThreeSided == "")
                            {
                                j = j + 2;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Three Sided Days. The Row Number is " + j + "');", true);
                                //System.Windows.Forms.MessageBox.Show("Enter the Name. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                ErrFlag = true;
                            }
                            else if (Half =="")
                            {
                                j = j + 2;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the O/N Days Properly. The Row Number is " + j + "');", true);
                                //System.Windows.Forms.MessageBox.Show("Enter the Name. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                ErrFlag = true;
                            }
                            //else if ((Convert.ToDecimal(Full)) > Convert.ToDecimal(WorkingDays))
                            //{
                            //    j = j + 2;
                            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the O/N Days Properly. The Row Number is " + j + "');", true);
                            //    //System.Windows.Forms.MessageBox.Show("Enter the Name. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                            //    ErrFlag = true;
                            //}
                            //else if ((Convert.ToDecimal(ThreeSided)) > Convert.ToDecimal(WorkingDays))
                            //{
                            //    j = j + 2;
                            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the ThreeSided Days Properly. The Row Number is " + j + "');", true);
                            //    //System.Windows.Forms.MessageBox.Show("Enter the Name. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                            //    ErrFlag = true;
                            //}
                            
                            else if ((Convert.ToDecimal(WorkingDays) + Convert.ToDecimal(txtNfh.Text)) > 31)
                            {
                                j = j + 2;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Days properly. The Row Number is " + j + "');", true);
                                //System.Windows.Forms.MessageBox.Show("Enter the Days Properly. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                ErrFlag = true;
                            }
                            //else if (AbsentDays == "")
                            //{
                            //    j = j + 2;
                            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Absent Days properly. The Row Number is " + j + "');", true);
                            //    //System.Windows.Forms.MessageBox.Show("Enter the Days Properly. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                            //    ErrFlag = true;
                            //}

                            //else if (weekoff == "")
                            //{
                            //    j = j + 2;
                            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Weekoff Days Properly');", true);
                            //    ErrFlag = true;
                            //}
                            //cn.Open();
                            //string qry_dpt = "Select DepartmentNm from MstDepartment where DepartmentNm = '" + Department + "'";
                            //SqlCommand cmd_dpt = new SqlCommand(qry_dpt, cn);
                            //SqlDataReader sdr_dpt = cmd_dpt.ExecuteReader();
                            //if (sdr_dpt.HasRows)
                            //{
                            //}
                            //else
                            //{
                            //    j = j + 2;
                            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Department name not found in the department Details. The Row Number is " + j + "');", true);
                            //    //System.Windows.Forms.MessageBox.Show("Department Name not Found in Department Details. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                            //    ErrFlag = true;
                            //}
                            //cn.Close();
                            //cn.Open();
                            //string qry_dpt1 = "Select ED.Department from EmployeeDetails ED inner Join MstDepartment MSt on Mst.DepartmentCd=ED.Department where DepartmentNm = '" + Department + "' and ED.EmpNo='" + EmpNo + "'";
                            //SqlCommand cmd_dpt1 = new SqlCommand(qry_dpt1, cn);
                            //SqlDataReader sdr_dpt1 = cmd_dpt1.ExecuteReader();
                            //if (sdr_dpt1.HasRows)
                            //{
                            //}
                            //else
                            //{
                            //    j = j + 2;
                            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Department name not found in the department Details. The Row Number is " + j + "');", true);
                            //    //System.Windows.Forms.MessageBox.Show("Department Name not Found in Department Details. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                            //    ErrFlag = true;
                            //}
                            //cn.Close();
                            //cn.Open();
                            //string Qry_SalaryType = "Select EmpNo from Officialprofile where WagesType ='" + rbsalary.SelectedValue + "'";
                            //SqlCommand cmd_wages = new SqlCommand(Qry_SalaryType, cn);
                            //SqlDataReader sdr_wages = cmd_wages.ExecuteReader();
                            //if (sdr_wages.HasRows)
                            //{
                            //}
                            //else
                            //{
                            //    j = j + 2;
                            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('This " + EmpNo + " is Wages Type is incorrect.');", true);
                            //    //System.Windows.Forms.MessageBox.Show("Department Name not Found in Department Details. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                            //    ErrFlag = true;
                            //}
                            //cn.Close();
                            //cn.Open();
                            //string qry_empNo = "Select EmpNo from EmployeeDetails where EmpNo= '" + EmpNo + "' and MachineNo = '" + MachineID + "' and ExisistingCode = '" + ExistingCode + "' and EmpName = '" + FirstName + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                            //SqlCommand cmd_Emp = new SqlCommand(qry_empNo, cn);
                            //SqlDataReader sdr_Emp = cmd_Emp.ExecuteReader();
                            //if (sdr_Emp.HasRows)
                            //{
                            //}
                            //else
                            //{
                            //    j = j + 2;
                            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Employee Details Properly. The Row Number is " + j + "');", true);
                            //    //System.Windows.Forms.MessageBox.Show("Enter the Employee Details Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                            //    ErrFlag = true;
                            //}
                            //cn.Close();
                            total = (Convert.ToDecimal(WorkingDays) + Convert.ToDecimal(CL));
                            days = 0;
                            if ((ddlMonths.SelectedValue == "January") || (ddlMonths.SelectedValue == "March") || (ddlMonths.SelectedValue == "May") || (ddlMonths.SelectedValue == "July") || (ddlMonths.SelectedValue == "August") || (ddlMonths.SelectedValue == "October") || (ddlMonths.SelectedValue == "December"))
                            {
                                days = 31;
                            }
                            else if ((ddlMonths.SelectedValue == "April") || (ddlMonths.SelectedValue == "June") || (ddlMonths.SelectedValue == "September") || (ddlMonths.SelectedValue == "November"))
                            {
                                days = 30;
                            }
                            else if (ddlMonths.SelectedValue == "February")
                            {
                                int yrs = (Convert.ToInt32(ddlfinance.SelectedValue) + 1);
                                if ((yrs % 4) == 0)
                                {
                                    days = 29;
                                }
                                else
                                {
                                    days = 28;
                                }
                            }
                            if (total > days)
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the days properly.');", true);
                                ErrFlag = true;
                            }
                            //if (total > Convert.ToDecimal(txtdays.Text))
                            //{
                            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the days properly.');", true);
                            //    ErrFlag = true;
                            //}
                            decimal Att_sum = (Convert.ToDecimal(WorkingDays) + Convert.ToDecimal(CL));
                            if (Convert.ToDecimal(days) < Att_sum)
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Attenance Details Properly ...');", true);
                                ErrFlag = true;
                            }
                        }
                        if (!ErrFlag)
                        {
                            for (int i = 0; i < dt.Rows.Count; i++)
                            {
                                SqlConnection cn = new SqlConnection(constr);
                                cn.Open();
                                string qry_dpt1 = "Select EmpNo from EmployeeDetails where ActivateMode='Y' and BiometricID = '" + dt.Rows[i][0].ToString() + "' and Lcode='" + SessionLcode + "' and Ccode='" + SessionCcode + "'";
                                SqlCommand cmd_dpt1 = new SqlCommand(qry_dpt1, cn);
                                DepartmentCode = Convert.ToString(cmd_dpt1.ExecuteScalar()); //Employee No is DepartmentCode

                                if (DepartmentCode == "Per0420153171")
                                {
                                    string Val = "0";
                                }
                                string qry_emp = "Select AT.EmpNo from AttenanceDetails AT inner JOin SalaryDetails SD on SD.EmpNo=AT.EmpNo where AT.EmpNo='" + DepartmentCode + "' and " +
                                                 "AT.Months='" + ddlMonths.Text + "' and AT.FinancialYear='" + ddlfinance.SelectedValue + "' and AT.Ccode='" + SessionCcode + "' and AT.Lcode='" + SessionLcode + "' and SD.Month='" + ddlMonths.Text + "'" +
                                                 " and SD.FinancialYear='" + ddlfinance.SelectedValue + "' and SD.Process_Mode='1' and SD.Ccode='" + SessionCcode + "' and SD.Lcode='" + SessionLcode + "' and convert(datetime,AT.TODate,105) >= convert(datetime,'" + txtFrom.Text + "',105)";
                                //string qry_emp = "Select EmpNo from AttenanceDetails where EmpNo = '" + dt.Rows[i][2].ToString() + "' and Months = '" + ddlMonths.Text + "' and FinancialYear = '" + ddlfinance.SelectedValue + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                                SqlCommand cmd_verify = new SqlCommand(qry_emp, cn);
                                Name_Upload = Convert.ToString(cmd_verify.ExecuteScalar());
                                string qry_emp1 = "Select EmpNo from AttenanceDetails where EmpNo = '" + DepartmentCode + "' and Months = '" + ddlMonths.Text + "' and FinancialYear = '" + ddlfinance.SelectedValue + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and convert(datetime,TODate,105) >= convert(datetime,'" + txtFrom.Text + "',105)";
                                SqlCommand cmd_verify1 = new SqlCommand(qry_emp1, cn);
                                Name_Upload1 = Convert.ToString(cmd_verify1.ExecuteScalar());
                                string qry_Type = "Select Wagestype from Officialprofile where EmpNo = '" + DepartmentCode + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                                SqlCommand cmd_verify2 = new SqlCommand(qry_Type, cn);
                                WagesType = Convert.ToString(cmd_verify2.ExecuteScalar());

                                //Get Staff or Labor
                                if (DepartmentCode == "Per1120163784")
                                {
                                    int val = 0;
                                }

                                string qry_SL = "select StafforLabor from EmployeeDetails where BiometricID = '" + dt.Rows[i][0].ToString() + "' and Lcode='" + SessionLcode + "' and Ccode='" + SessionCcode + "'";
                                SqlCommand cmd_SL = new SqlCommand(qry_SL, cn);
                                StafforLabor = Convert.ToString(cmd_SL.ExecuteScalar());

                                string Employee_Type_Check = "";
                                string qry_emptype = "Select EmployeeType from EmployeeDetails Where EmpNo='" + DepartmentCode + "' And Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                                SqlCommand cmd_emptype_verify = new SqlCommand(qry_emptype, cn);
                                Employee_Type_Check = Convert.ToString(cmd_emptype_verify.ExecuteScalar());

                                if (DepartmentCode == "")
                                {

                                }
                                else
                                {
                                    //Parthi Start 
                                    string QryType = "select EmployeeType from EmployeeDetails where EmpNo='" + DepartmentCode + "'";
                                    string EmpTypeCheck = "";
                                    SqlCommand cmd_EmpType = new SqlCommand(QryType, cn);
                                    EmpTypeCheck = Convert.ToString(cmd_EmpType.ExecuteScalar());
                                    //Parthi End
                                    objatt.department = "0";
                                    objatt.EmpNo = DepartmentCode;
                                    objatt.Exist = "";
                                    objatt.EmpName = dt.Rows[i][1].ToString();

                                    //Check Temp Category are Not
                                    if (EmpTypeCheck.Trim() == "3")
                                    {
                                        objatt.days = (Convert.ToDecimal(dt.Rows[i][2].ToString()) + Convert.ToDecimal(dt.Rows[i][5].ToString())).ToString();
                                    }
                                    else
                                    {
                                        objatt.days = (Convert.ToDecimal(dt.Rows[i][2].ToString())).ToString();
                                    }
                                    
                                    

                                    objatt.Months = ddlMonths.Text;

                                    objatt.finance = ddlfinance.SelectedValue;
                                    objatt.Ccode = SessionCcode;
                                    objatt.Lcode = SessionLcode;
                                    objatt.nfh = "0";
                                    objatt.HomeDays = dt.Rows[i][4].ToString();
                                    if (Convert.ToDecimal(txtdays.Text.Trim()) >= (Convert.ToDecimal(dt.Rows[i][2].ToString()) + Convert.ToDecimal(dt.Rows[i][3].ToString())))
                                    {
                                        objatt.Absent = "0";
                                    }
                                    else
                                    {
                                        objatt.Absent = (Convert.ToDecimal(txtdays.Text.Trim()) - (Convert.ToDecimal(dt.Rows[i][2].ToString()) + Convert.ToDecimal(dt.Rows[i][3].ToString()))).ToString();
                                    }
                                    objatt.weekoff = "0";
                                    objatt.Rest = "0";
                                    if (WagesType == "1")
                                    {
                                        objatt.half = dt.Rows[i][5].ToString();  // WeekOff
                                        objatt.Full = txtNfh.Text;
                                        objatt.ThreeSided = (Convert.ToDecimal(dt.Rows[i][4].ToString()) + Convert.ToDecimal(dt.Rows[i][7].ToString())).ToString();
                                        objatt.TotalDays = (Convert.ToInt32(txtdays.Text)).ToString();
                                        objatt.workingdays = txtdays.Text;
                                        objatt.FixedDays = dt.Rows[i][8].ToString();
                                        objatt.NfhCount = "0";
                                        objatt.NfhPresent = "0";
                                        objatt.FineAmt = "0";
                                        objatt.WHPresent = "0";
                                        objatt.BonusDays = "0";
                                    }
                                    else if (WagesType == "3")
                                    {
                                        objatt.half = "0";
                                        objatt.Full = "0";
                                        objatt.ThreeSided = "0";
                                        objatt.Rest = "0";
                                        objatt.FixedDays = "0";
                                        objatt.NfhCount = "0";
                                        objatt.NfhPresent = "0";
                                        objatt.FineAmt = "0";
                                        objatt.WHPresent = "0";
                                        objatt.BonusDays = "0";

                                    }
                                    else
                                    {
                                        objatt.half = dt.Rows[i][5].ToString(); // WeekOff 
                                        objatt.Full = txtNfh.Text; //FH Days
                                        objatt.ThreeSided = (Convert.ToDecimal(dt.Rows[i][4].ToString()) + Convert.ToDecimal(dt.Rows[i][7].ToString())).ToString(); // WeekOff Present
                                        objatt.TotalDays = (Convert.ToInt32(txtdays.Text)).ToString();
                                        objatt.workingdays = txtdays.Text;
                                        objatt.FixedDays = dt.Rows[i][8].ToString();  //Fixed Days

                                        objatt.NfhCount = dt.Rows[i][4].ToString();
                                        objatt.NfhPresent = dt.Rows[i][5].ToString();
                                        objatt.FineAmt = dt.Rows[i][6].ToString();
                                        objatt.WHPresent = dt.Rows[i][7].ToString();
                                        objatt.BonusDays = dt.Rows[i][5].ToString();

                                    }
                                   
                                    if (dt.Rows[i][3].ToString().Trim() == "")
                                    {
                                        objatt.cl = "0";
                                    }

                                    else
                                    {
                                        objatt.cl = dt.Rows[i][3].ToString();
                                    }
                                    //objatt.workingdays = txtdays.Text;
                                    cn.Close();



                                    if (Name_Upload == DepartmentCode)
                                    {
                                        //string Del = "Delete from AttenanceDetails where EmpNo = '" + dt.Rows[i][2].ToString() + "' and Months = '" + ddlMonths.Text + "' and FinancialYear = '" + ddlfinance.SelectedValue + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                                        //cn.Open();
                                        //SqlCommand cmd_del = new SqlCommand(Del, cn);
                                        ////cn.Open();
                                        //cmd_del.ExecuteNonQuery();
                                        //cn.Close();
                                    }
                                    else if (Name_Upload1 == DepartmentCode)
                                    {
                                        string Del = "Delete from AttenanceDetails where EmpNo = '" + DepartmentCode + "' and Months = '" + ddlMonths.Text + "' and FinancialYear = '" + ddlfinance.SelectedValue + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and convert(Datetime, FromDate,105)=convert(Datetime,'" + txtFrom.Text.Trim() + "',105)";
                                        cn.Open();
                                        SqlCommand cmd_del = new SqlCommand(Del, cn);
                                        cmd_del.ExecuteNonQuery();
                                        cn.Close();
                                        DateTime MyDate = DateTime.ParseExact(txtFrom.Text, "dd-MM-yyyy", null);
                                        DateTime MyDate1 = DateTime.ParseExact(txtTo.Text, "dd-MM-yyyy", null);
                                        objdata.Attenance_insert(objatt, MyDate, MyDate1, rbsalary.SelectedValue);
                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Uploaded Successfully...');", true);
                                    }
                                    else
                                    {
                                        DateTime MyDate = DateTime.ParseExact(txtFrom.Text, "dd-MM-yyyy", null);
                                        DateTime MyDate1 = DateTime.ParseExact(txtTo.Text, "dd-MM-yyyy", null);
                                        objdata.Attenance_insert(objatt, MyDate, MyDate1, rbsalary.SelectedValue);
                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Uploaded Successfully...');", true);
                                    }
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Uploaded Successfully...');", true);
                                    //sSourceConnection.Close();
                                }
                            }
                            
                            //System.Windows.Forms.MessageBox.Show("Uploaded SuccessFully", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);

                        }
                        if (ErrFlag == true)
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Your File Not Upload...');", true);
                            //System.Windows.Forms.MessageBox.Show("Your File Not Uploaded", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Please Upload Correct File Format...');", true);
            //System.Windows.Forms.MessageBox.Show("Please Upload Correct File Format", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
        }
    }
    protected void btnEmp_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptEmpDownload.aspx");
    }
    protected void btnatt_Click(object sender, EventArgs e)
    {
        Response.Redirect("AttenanceDownload.aspx");
    }
    protected void btnLeave_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptLeaveSample.aspx");
    }
    protected void btnOT_Click(object sender, EventArgs e)
    {
        Response.Redirect("OTDownload.aspx");
    }
    protected void btncontract_Click(object sender, EventArgs e)
    {
        Response.Redirect("ContractRPT.aspx");
    }
    protected void rbsalary_SelectedIndexChanged(object sender, EventArgs e)
    {
        //if (rbsalary.SelectedValue == "1")
        //{
        //    PanelMonthly.Visible = true;
        //    panelWeekly.Visible = true;
        //    panelBimonth.Visible = false;

        //}
        //else if (rbsalary.SelectedValue == "2")
        //{
        //    PanelMonthly.Visible = true;
        //    panelWeekly.Visible = false;
        //    panelBimonth.Visible = false;
        //}
        //else if (rbsalary.SelectedValue == "3")
        //{
        //    PanelMonthly.Visible = true;
        //    panelWeekly.Visible = false;
        //    panelBimonth.Visible = true;
        //}
    }
    protected void txtFrom_TextChanged(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        txtTo.Text = null;
        int val_Month = Convert.ToDateTime(txtFrom.Text).Month;
        string Mont = "";
        switch (val_Month)
        {
            case 1:
                Mont = "January";
                break;

            case 2:
                Mont = "February";
                break;
            case 3:
                Mont = "March";
                break;
            case 4:
                Mont = "April";
                break;
            case 5:
                Mont = "May";
                break;
            case 6:
                Mont = "June";
                break;
            case 7:
                Mont = "July";
                break;
            case 8:
                Mont = "August";
                break;
            case 9:
                Mont = "September";
                break;
            case 10:
                Mont = "October";
                break;
            case 11:
                Mont = "November";
                break;
            case 12:
                Mont = "December";
                break;
            default:
                break;
        }
        if (Mont != ddlMonths.SelectedValue)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Date Properly...');", true);
            ErrFlag = true;
            txtFrom.Text = null;
        }
    }
    protected void txtTo_TextChanged(object sender, EventArgs e)
    {
        try
        {
            bool ErrFlag = false;
            if (txtFrom.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the From Date...');", true);
                ErrFlag = true;
            }
            
            if (!ErrFlag)
            {
                DateTime dfrom = Convert.ToDateTime(txtFrom.Text);
                DateTime dtto = Convert.ToDateTime(txtTo.Text);
                if (dfrom >= dtto)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Date Properly...');", true);
                    ErrFlag = true;
                    txtTo.Text = null;
                }
                if (!ErrFlag)
                {
                    decimal NoDays = (dtto - dfrom).Days;
                    NoDays = NoDays + 1;
                    txtdays.Text = NoDays.ToString();
                }
            }
        }
        catch (Exception ex)
        {
        }
    }
    protected void btnSal_Click(object sender, EventArgs e)
    {
        Response.Redirect("FrmDeductionDownload.aspx");
    }
}
