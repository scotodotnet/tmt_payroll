<%@ Page Language="C#" AutoEventWireup="true" CodeFile="BonusForAll.aspx.cs" Inherits="Bonusforall_1" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
    <head id="Head1" runat="server">
        <title>Payroll Management Systems</title>
        <link href="new_css/main.css" rel="stylesheet" type="text/css" />
        <link href="new_css/colors/dark_blue.css" rel="stylesheet" type="text/css" />
        <link href="new_css/superfish.css" rel="stylesheet" type="text/css" />

        <script src="new_js/jquery-1.4.2.min.js" type="text/javascript"></script>
        <script src="new_js/jquery.hoverintent.minified.js" type="text/javascript"></script>
        <script src="new_js/superfish.js" type="text/javascript"></script>
        <script src="new_js/jquery.tools.min.js" type="text/javascript"></script>
        <%--<script src="new_js/jquery.simplemodal.js" type="text/javascript"></script>--%>
        <script src="new_js/jquery.notifybar.js" type="text/javascript"></script>
        <script src="new_js/jquery.tipsy.js" type="text/javascript"></script>
        <script src="new_js/enhance.js" type="text/javascript"></script>
        <script src="new_js/excanvas.js" type="text/javascript"></script>
        <script src="new_js/visualize.jquery.js" type="text/javascript"></script>
        <script src="new_js/jquery.collapsible.js" type="text/javascript"></script>
        <script src="new_js/jquery.autosuggest.packed.js" type="text/javascript"></script>
        <script src="new_js/platinum-admin.js" type="text/javascript"></script>
        <style>
            .sf-navbar li ul 
            {
	            width: 1100px; /*IE6 soils itself without this*/
	            margin: 0px auto;
	            margin-left:-20em;	            
            }
        </style>
    </head>
    <body>
        <form id="form1" runat="server">
            <cc1:ToolkitScriptManager runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true"
                ID="ScriptManager1" EnablePartialRendering="true">
            </cc1:ToolkitScriptManager>
            <div id="header">
                <div id="header-top">
                    <div id="logo">
	                    <h1>&nbsp;</h1>
	                    <span id="slogan">&nbsp;</span> 
	                </div>
                    <!-- end logo -->
                    <div id="login-info">
                        <img id="profile" alt="profile" src="./new_images/icons/user.png" />
                        <p id="top"><span id="name"><asp:Label ID="lblusername" runat="server"></asp:Label></span><br />
                        <span id="links">
                        <%--<a id="message-link" href="#" title="click to see the messages!">
                        <span id="message-count">1</span> new message. </a>--%></span></p>
                        <div>
                            <asp:Label ID="lblComany" runat="server" Text="" Font-Bold="true"></asp:Label>
                        </div>
	                    <div id="messages-box">
		                    <h4>Messages</h4>
	                        <h5><a id="new-message" href="#">New Message</a></h5>
	                        <hr />
	                        <ul id="messages">
		                        <li class="new"><a href="#">
		                            <img alt="user" src="./new_images/icons/sender.png" />
		                            <span class="sender">John Doe</span>
		                            <span class="description">Hi this is just a description...</span>
		                            <span class="date">20 minutes ago...</span> </a>
		                        </li>
		                        <li><a href="#">
		                            <img alt="user" src="./new_images/icons/sender.png" />
		                            <span class="sender">John Doe</span>
		                            <span class="description">Hi this is just a description...</span>
		                            <span class="date">24 minutes ago...</span> </a>
		                        </li>
		                        <li><a href="#">
		                            <img alt="user" src="./new_images/icons/sender.png" />
		                            <span class="sender">John Doe</span>
		                            <span class="description">Hi this is just a description...</span>
		                            <span class="date">50 minutes ago...</span> </a>
		                        </li>
		                        <li id="last"><a id="all-messages" href="#">See all messages!
		                            <span id="unreaded">8 unreaded.</span> </a>
		                        </li>
		                    </ul>
		                    <br class="clear" />
	                    </div><!-- end messages-box -->
	                    <a id="power" href="Default.aspx" title="Logout">logout</a>
                    </div>
                    <!-- end login -->
                    <div id="nav">
	                    <ul class="sf-menu sf-navbar">
	                        <li><a id="dashboard" href="Dashboard.aspx">Dashboard</a>
	                            <ul>
		                            <li>&nbsp;
		                            </li>
	                            </ul>
	                        </li>
	                        <li><a id="Employee" href="EmployeeRegistration.aspx">Employee</a>
	                            <ul>
		                            <li><a href="EmployeeRegistration.aspx">Employee</a> </li>					                    
		                            <li><a href="OfficalProfileDetails.aspx">Profile</a></li>
		                            <li><a href="SalaryMaster.aspx">Salary Master</a></li>
		                            <%--<li><a href="LeaveAllocationEmployee.aspx">Leave Allocation</a></li>--%>
		                            <%--<li><a href="ApplyForLeave.aspx">Apply Leave</a></li>--%>					                    
		                            <li><a href="SalaryRegistration.aspx">Salary Details</a></li>
		                            <li><a href="SalaryAdvance.aspx">Advance</a></li>
		                            <li><a href="Settlement.aspx">Settlement</a></li>
		                            <li><a href="EmployeeDeactiveMode.aspx">Employee Re-Activate</a></li>
		                            				                    
	                            </ul>
	                        </li>
	                        <li><a id="Reports" href="RptDepartmentwiseEmployee.aspx">Reports</a>
	                            <ul>
		                            <li><a href="RptDepartmentwiseEmployee.aspx">Employee Details</a></li>
		                            <%--<li><a href="RptAttendanceReport.aspx">Attendance Report</a></li>--%>
		                            <%--<li><a href="RptProbationperiod.aspx">Probationary Period</a></li>
		                            <li><a href="ContractBreak.aspx">Contract Details</a></li>--%>
		                            <li><a href="RptResign.aspx">Resignation Reports</a></li>
		                            <li><a href="PaySlipGen.aspx">Payslip</a></li>
		                            <%--<li><a href="RptSalary.aspx">Salary Details</a></li>--%>
		                            <%--<li><a href="SalarySummary.aspx">Salary Summary</a></li>
	                                <li><a href="Incentive.aspx">Incentive Details</a></li>--%>
	                                <li><a href="RptDepartmentSalaryAbstract.aspx">Salary Abstract</a></li>
	                            </ul>
	                        </li>
	                        <li><a id="OT" href="OverTime.aspx">OT</a>
	                            <ul>
	                                <li><a href="OverTime.aspx">OT</a></li>
	                                <li><a href="RptOT.aspx">OT Report</a></li>
	                            </ul>
	                        </li>
	                        <li><a id="Master" href="MstEmployeeType.aspx">Masters</a>
	                            <ul>
		                            <li><a href="MstEmployeeType.aspx">Employee Type</a></li>
		                            <li><a href="UserRegistration.aspx">User</a></li>
		                            <li><a href="MSTPFESI.aspx">PF</a></li>
		                            <li><a href="MstBank.aspx">Bank</a></li>
		                            <li><a href="MstDepartment.aspx">Department</a></li>
		                            <li><a href="MstDAfix.aspx">DA Arrears FIX</a></li>
	                            </ul>
	                        </li>
	                        <li class="current"><a id="Bonus" href="BonusForAll.aspx">Bonus</a>
	                            <ul>
		                            <li class="current"><a href="BonusForAll.aspx">Bonus</a></li>
		                            <li><a href="StandardBonus.aspx">Group Bonus</a></li>	
		                            <li><a href="RptBonus.aspx">Bonus Report</a></li>				                    
	                            </ul>
	                        </li>
	                        <li><a id="ESI_PF" href="PFDownload.aspx">ESI & PF</a>
	                            <ul>
		                            <%--<li><a href="PFForm5.aspx">PF Form 5</a></li>--%>
		                            <li><a href="PFDownload.aspx">PF Download</a></li>
		                            <li><a href="ESIDownload.aspx">ESI Download</a></li>
		                            <li><a href="PFForm3Anew.aspx">PF Form 3A</a></li>
		                            <li><a href="PFform6A.aspx">PF Form 6A</a></li>
		                            <%--<li><a href="FOrm10.aspx">Form 10</a></li>
		                            <li><a href="PFForm3Anew.aspx">PF Form 3A</a></li>
		                            <li><a href="PFform6A.aspx">PF Form 6A</a></li>
		                            <li><a href="ESIForm7.aspx">ESI Form 7</a></li>--%>
	                            </ul>
	                        </li>
	                        <li><a id="Upload" href="UploadEmployee.aspx">Upload</a>
	                            <ul>
	                            <li><a href="UploadEmployee.aspx">Employee Upload</a></li>
		                            <li><a href="AttenanceUpload.aspx">Attenance Upload</a></li>
		                            	<li><a href="UploadOT.aspx">OT Upload</a></li>
		                            	<%--<li><a href="LoadEmpmusters.aspx">Employee Master</a></li>--%>
	                            </ul>
	                        </li>
                        </ul>
                    </div>
                    <!-- end div#nav -->
                </div>
                <!-- end div#header-top -->
                <div class="breadCrumb">
                    <ul>
                        <li class="first"><a href="#">Home</a> </li>
                        <li><a href="#">Users</a> </li>
                        <li><a href="#">Roles</a> </li>
                        <li class="last">Bonus </li>
                    </ul>
                </div>
                <!-- end div#breadCrumb -->
                <%--<div id="search-box">
                    <form id="searchform" action="" method="get">
                        <fieldset class="search"><span>Search</span>
                        <input class="box" type="text" />
                        <button class="btn" title="Submit Search">Search</button></fieldset>
                    </form>
                </div>--%>
                <!-- end div#search-box -->
            </div>
            <!-- end header -->
            <div id="page-wrap">
                <div id="right-sidebar">
                    <div class="innerdiv">
	                    <h2 class="head">Comments</h2>
	                    <div class="innerdiv">
	                    <h2 class="head">DownLoad Format</h2>
	                    <div class="innercontent clear">
		                    <h4>DownLoad Format</h4>
		                    <ul id="comments" class="tooltip-enabled">
		                    <asp:Button ID="btncontract" runat="server" Text="Contract Report" Width="200" 
                                    CssClass="button green" onclick="btncontract_Click" />
		                        <asp:Button ID="btnEmp" runat="server" Text="Employee Download" Width="200" 
                                    CssClass="button green" onclick="btnEmp_Click" />
		                        <asp:Button ID="btnatt" runat="server" Text="Attenance Download" Width="200" 
                                    CssClass="button green" onclick="btnatt_Click"/>
		                        <asp:Button ID="btnLeave" runat="server" Text="Leave Download" Width="200" 
                                    CssClass="button green" OnClick="btnLeave_Click" />
                                    <asp:Button ID="btnOT" runat="server" Text="OT Download" Width="200" 
                                    CssClass="button green" onclick="btnOT_Click" />
                                    <asp:Button ID="btnSal" runat="server" Text="Salary Download" Width="200" 
                                    CssClass="button green" onclick="btnSal_Click" />
		                        
		                    </ul>
		                    <%--<img src="new_images/icons/right_img.jpg" alt="" width="230"/>--%>
	                    </div>
                    </div>
                    </div>
                    <div class="clear"></div>                    
                </div>
	            <!-- end right-sidebar -->  
	             <asp:UpdatePanel ID="UpdatePanel1" runat="server">
	                    <ContentTemplate>
	                        <div id="main-content">
                                <br /><br /><br /><br /> 
	                            <div class="clear_body">
                                    <div class="innerdiv">
                                        <div class="innercontent">
                                            <!-- tab "panes" -->				                
                                                <div>
                                                    <table class="full">
                                                        <thead>
                                                            <tr align="center">
                                                                <th colspan="4" style="text-align:center;"><h4>BONUS</h4></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                             <tr>
                                                                <td><asp:Label ID="lblcategory" runat="server" Text="Category" Font-Bold="true"></asp:Label></td>
                                                                <td><asp:DropDownList ID="ddlcategory" runat="server" Width="100" Height="25" 
                                                                        onselectedindexchanged="ddlcategory_SelectedIndexChanged" AutoPostBack="true" ></asp:DropDownList></td>
                                                                
                                                                <td ><asp:Label ID="lbldept" runat="server" Text="Department" Font-Bold="true"></asp:Label></td>
                                                                <td><asp:DropDownList ID="ddldepat" runat="server" Width="180" Height="25" 
                                                                        onselectedindexchanged="ddldepat_SelectedIndexChanged" AutoPostBack="true" ></asp:DropDownList>
                                                                <asp:Button ID="btnclick" runat="server" Text="Click" Width="78" Height="28" 
                                                                        onclick="btnclick_Click" CssClass="button green" /></td>

                                                            </tr>
                                                           <%-- <asp:Panel ID="panelEmployee" runat="server" Visible="false" >--%>
                                                                <tr>
                                                                     <td><asp:Label ID="lblexisitingno" runat="server" Text="Existing Number" Font-Bold="true"></asp:Label></td>
                                                                     <td><asp:TextBox ID="txtexistingNo" runat="server" ></asp:TextBox>
                                                                      <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender_ExisistingCode" runat="server"
                                                                                                                            TargetControlID="txtexistingNo" FilterMode="ValidChars" FilterType="UppercaseLetters,LowercaseLetters,Numbers,Custom"                                                           ValidChars="0123456789+- ">
                                                                                                                        </cc1:FilteredTextBoxExtender></td>
                                                                     <td colspan="2">
                                                                     <asp:Button ID="btnexitingSearch" runat="server" Text="Search" Width="78"  
                                                                             Height="28" onclick="btnexitingSearch_Click" CssClass="button green"  /></td>
                                                                </tr>
                                                                <tr>
                                                                   
                                                                    <td><asp:Label ID="lblempcode" runat="server" Text="Emp No" Font-Bold="true"></asp:Label></td>
                                                                    <td><asp:DropDownList ID="ddlempcode" runat="server" Width="180" Height="25" onselectedindexchanged="ddlempcode_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList></td>
                                                                    <td><asp:Label ID="lblempname" runat="server" Text="Emp Name" Font-Bold="true"></asp:Label></td>
                                                                    <td><asp:DropDownList ID="ddlempname" runat="server" Width="180" Height="25" 
                                                                            onselectedindexchanged="ddlempname_SelectedIndexChanged" AutoPostBack="true" ></asp:DropDownList></td>
                                                                </tr>
                                                                <tr>
                                                                <td><asp:Label ID="LabelFinance" runat="server" Text="Financial Period" Font-Bold="true" ></asp:Label></td>
                                                                <td ><asp:DropDownList ID="ddFinance" runat="server" Width="180" Height="25" 
                                                                        AutoPostBack="true" onselectedindexchanged="ddFinance_SelectedIndexChanged" ></asp:DropDownList></td>
                                                                <td><asp:Label ID="lbldoj" runat="server" Text="Date of Joinig : " Font-Bold="true" ></asp:Label>
                                                                <asp:Label ID="doj" runat="server" Font-Bold="true" ></asp:Label></td>
                                                                <td><asp:Label ID="lblyop" runat="server" Text="Experience : " Font-Bold="true" ></asp:Label>
                                                                <asp:Label ID="yop" runat="server" Font-Bold="true" ></asp:Label></td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="4"><asp:Label ID="lblEmpProfile" runat="server" Text="Profile Type : " Font-Bold="true" ></asp:Label>
                                                                <asp:Label ID="EmpProfile" runat="server" Font-Bold="true" ></asp:Label></td>
                                                                <%--<td><asp:Label ID="lblEmpType" runat="server" Text="Employee Category" Font-Bold="true" ></asp:Label>
                                                                <asp:Label ID="EmpType" runat="server" Font-Bold="true" ></asp:Label>
                                                                </td>--%>
                                                            </tr>
                                                            <%--<tr>
                                                                <td>
                                                                    <asp:Label ID="lblSalarytype" runat="server" Text="Bonus Option" Font-Bold="true"></asp:Label>
                                                                </td>
                                                                <td colspan="5">
                                                                    <asp:RadioButtonList ID="rbbonus" runat="server" RepeatColumns="2" 
                                                                        onselectedindexchanged="rbbonus_SelectedIndexChanged" AutoPostBack="true" >
                                                                        <asp:ListItem Selected="False" Value="1" Text="Through basic Details" ></asp:ListItem>
                                                                        <asp:ListItem Selected="False" Value="2" Text="Net Amount of Year" ></asp:ListItem>
                                                                    </asp:RadioButtonList>
                                                                    
                                                                </td>--%>
                                                                <%--<td>
                                                                    <asp:Label ID="Label5" runat="server" Text="Month or Day Option" Font-Bold="true" ></asp:Label>
                                                                </td>
                                                                <td colspan="3">
                                                                    <asp:RadioButtonList ID="rbMonth" runat="server" RepeatColumns="2" >
                                                                        <asp:ListItem Selected="True" Value="1" Text="Days" ></asp:ListItem>
                                                                        <asp:ListItem Selected="False" Value="2" Text="Months" ></asp:ListItem>
                                                                    </asp:RadioButtonList>
                                                                    
                                                                </td>--%>
                                                            <%--</tr>--%>
                                                            <asp:Panel ID="lblpermenant" runat="server" Visible="false">
                                                            <tr>
                                                                
                                                                    <td colspan="4">
                                                                    <asp:CheckBox ID="chkbasic" runat="server" Text="Basic" AutoPostBack="true" 
                                                                            Font-Bold="true" Width="140" Font-Size="10" 
                                                                            oncheckedchanged="chkbasic_CheckedChanged" />
                                                                    <asp:CheckBox ID="chkHRA" runat="server" Text="HRA" AutoPostBack="true" 
                                                                            Font-Bold="true" Width="140" Font-Size="10" 
                                                                            oncheckedchanged="chkHRA_CheckedChanged" />
                                                                    <asp:CheckBox ID="chkFDA" runat="server" Text="FDA" AutoPostBack="true" 
                                                                            Font-Bold="true" Width="140" Font-Size="10" 
                                                                            oncheckedchanged="chkFDA_CheckedChanged" />
                                                                    <asp:CheckBox ID="chkVDA" runat="server" Text="VDA" AutoPostBack="true" 
                                                                            Font-Bold="true" Width="140" Font-Size="10" 
                                                                            oncheckedchanged="chkVDA_CheckedChanged" />
                                                                    <asp:CheckBox ID="chkAllowance1" runat="server" Text="Allowance 1" 
                                                                            AutoPostBack="true" Font-Bold="true"  Width="140" Font-Size="10" 
                                                                            oncheckedchanged="chkAllowance1_CheckedChanged"/>
                                                                    <asp:CheckBox ID="chkAllowance2" runat="server" Text="Allowance 2" 
                                                                            AutoPostBack="true" Font-Bold="true" Width="140" Font-Size="10" 
                                                                            oncheckedchanged="chkAllowance2_CheckedChanged"/>
                                                                    </td>
                                                            </tr>
                                                            
                                                            <tr >
                                                                    <td  colspan="4">
                                                                        <asp:CheckBox ID="chkDeduction1" runat="server" Text="Deduction1" 
                                                                            AutoPostBack="true" Font-Bold="true" Width="140" Font-Size="10" 
                                                                            oncheckedchanged="chkDeduction1_CheckedChanged" />
                                                                        <asp:CheckBox ID="chkdeduction2" runat="server" Text="Deduction2" 
                                                                            AutoPostBack="true" Font-Bold="true" Width="140" Font-Size="10" 
                                                                            oncheckedchanged="chkdeduction2_CheckedChanged" />
                                                                    </td>        
                                                            </tr>
                                                            
                                                            <tr id="PanelBasicLabel" runat="server" visible="false">
                                                                    <td ><asp:Label ID="lblbasic" runat="server" Text="Basic" Font-Bold="true"></asp:Label></td>
                                                                    <td colspan="3" ><asp:TextBox ID="txtbasic" runat="server" Text="0" Enabled="false" ></asp:TextBox>
                                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR14" runat="server"  FilterMode="ValidChars" FilterType="Numbers,Custom" TargetControlID="txtbasic" ValidChars=".">
                                                                                                                        </cc1:FilteredTextBoxExtender></td>
                                                                </tr>
                                                                <tr id="PanelHRALabel" runat="server" visible="false">
                                                                    <td  ><asp:Label ID="lblhra" runat="server" Text="HRA" Font-Bold="true"></asp:Label></td>
                                                                    <td colspan="3" ><asp:TextBox ID="txthra" runat="server" Text="0" Enabled="false"></asp:TextBox>
                                                                     <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR13" runat="server"  FilterMode="ValidChars" FilterType="Numbers,Custom" TargetControlID="txthra" ValidChars=".">
                                                                                                                        </cc1:FilteredTextBoxExtender></td>
                                                                </tr>
                                                                <tr id="PanelFDA" runat="server" visible="false">
                                                                    <td ><asp:Label ID="Label1" runat="server" Text="FDA" Font-Bold="true"></asp:Label></td>
                                                                    <td colspan="3" ><asp:TextBox ID="txtFDA" runat="server" Text="0" Enabled="false" ></asp:TextBox>
                                                                     <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR12" runat="server" FilterMode="ValidChars" FilterType="Numbers,Custom" TargetControlID="txtFDA" ValidChars=".">
                                                                                                                        </cc1:FilteredTextBoxExtender></td>
                                                                </tr>
                                                                <tr id="PanelVDA" runat="server" visible="false">
                                                                    <td ><asp:Label ID="Label2" runat="server" Text="VDA" Font-Bold="true"></asp:Label></td>
                                                                    <td colspan="3" ><asp:TextBox ID="txtVDA" runat="server" Text="0" Enabled="false"></asp:TextBox>
                                                                     <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR11" runat="server" FilterMode="ValidChars" FilterType="Numbers,Custom" TargetControlID="txtVDA" ValidChars=".">
                                                                                                                        </cc1:FilteredTextBoxExtender></td>
                                                                </tr>
                                                                <tr id="PanelAllow1" runat="server" visible="false">
                                                                    <td ><asp:Label ID="Label3" runat="server" Text="Allowance 1" Font-Bold="true"></asp:Label></td>
                                                                    <td colspan="3" ><asp:TextBox ID="txtallowance1" runat="server" Text="0" Enabled="false" ></asp:TextBox>
                                                                     <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR1" runat="server" FilterMode="ValidChars" FilterType="Numbers,Custom" TargetControlID="txtallowance1" ValidChars=".">
                                                                                                                        </cc1:FilteredTextBoxExtender></td>
                                                                </tr>
                                                                <tr id="PanelAllow2" runat="server" visible="false">
                                                                    <td ><asp:Label ID="Label4" runat="server" Text="Allowance 2" Font-Bold="true"></asp:Label></td>
                                                                    <td colspan="3" ><asp:TextBox ID="txtallowance2" runat="server" Text="0" Enabled="false"></asp:TextBox>
                                                                     <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR2" runat="server" FilterMode="ValidChars" FilterType="Numbers,Custom" TargetControlID="txtallowance2" ValidChars=".">
                                                                                                                        </cc1:FilteredTextBoxExtender></td>
                                                                </tr>
                                                                <tr id="panelDeduction1" runat="server" visible="false">
                                                                    <td><asp:Label ID="labelStaffded1" runat="server" Text="Deduction 1" Font-Bold="true" ></asp:Label></td>
                                                                    <td colspan="3"><asp:TextBox ID="txtded1" runat="server" Enabled="false" Text="0"></asp:TextBox></td>
                                                                </tr>
                                                                <tr id="panelDeduction2" runat="server" visible="false" >
                                                                    <td><asp:Label ID="labelstaffded2" runat="server" Text="Deduction 2" Font-Bold="true" ></asp:Label></td>
                                                                    <td colspan="3"><asp:TextBox ID="txtded2" runat="server" Enabled="false" Text="0" ></asp:TextBox></td>
                                                                </tr>
                                                                <tr>
                                                                            <td><asp:Button ID="btncalculation" runat="server" Text="Calculation" Width="100" 
                                                                                    Height="28" Font-Bold="true" CssClass="button green" 
                                                                                    onclick="btncalculation_Click" /> </td>
                                                                            <td colspan="5"><asp:TextBox ID="txtsum" runat="server" Enabled="false" Font-Bold="true" ></asp:TextBox></td>
                                                                        </tr>
                                                                        </asp:Panel>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:Label ID="lblsalary" runat="server" Text="Net Amount of year" Font-Bold="true" ></asp:Label>
                                                                            </td>
                                                                            <td  colspan="5">
                                                                                <asp:TextBox ID="txtSalAmt" runat="server" Text="0" Enabled="false" ></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <%--<tr>
                                                                            <td>
                                                                                <asp:Label ID="lblMonths" runat="server" Text="No of Months" Font-Bold="true" ></asp:Label>
                                                                            </td>
                                                                            <td  colspan="5">
                                                                                <asp:TextBox ID="txtMonths" runat="server" Text="12" ></asp:TextBox>
                                                                            </td>
                                                                        </tr>--%>
                                                                        <tr>
                                                                            <td><asp:Label ID="lblpercentage" runat="server" Text="Percentage" Font-Bold="true" ></asp:Label> </td>
                                                                            <td><asp:TextBox ID="txtpercentage" runat="server" Text="0" 
                                                                                    ontextchanged="txtpercentage_TextChanged" ></asp:TextBox>
                                                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR8" runat="server" FilterMode="ValidChars" FilterType="Numbers,Custom" TargetControlID="txtpercentage" ValidChars=".">
                                                                                                                                </cc1:FilteredTextBoxExtender></td>
                                                                            <td colspan="4"><asp:Button ID="btnpercentagecal" runat="server" Text="Percentage" 
                                                                                    Width="100" Height="28" Font-Bold="true" CssClass="button green" 
                                                                                    onclick="btnpercentagecal_Click" /></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td><asp:Label ID="lblpercentagevalue" runat="server" Text="Percentage Amount" Font-Bold="true" ></asp:Label> </td>
                                                                            <td colspan="5">
                                                                            <asp:TextBox ID="txtpercentageCal" runat="server" Text="0" Enabled="false" ></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:Label ID="lbldaily" runat="server" Text="No of Days" Font-Bold="true"></asp:Label>
                                                                            </td>
                                                                            <td colspan="5">
                                                                                <asp:TextBox ID="txtAttenanceDays" runat="server" Enabled="true" 
                                                                                    ontextchanged="txtAttenanceDays_TextChanged" ></asp:TextBox>
                                                                                <%--<asp:Button ID="btnSummary" runat="server" Text="Bonus" Font-Bold="true" 
                                                                                    Width="90" Height="28" CssClass="button green" onclick="btnSummary_Click" />--%>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                            <asp:Label ID="lbltotalAmount" runat="server" Text="Bonus Amount" Font-Bold="true" ></asp:Label>
                                                                            </td>
                                                                            <td colspan="5">
                                                                            <asp:TextBox ID="txttotalamt" runat="server" Text="0" Enabled="false" ></asp:TextBox>
                                                                            </td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td align="center" colspan="4">
                                                                        <asp:Button ID="btnSave" runat="server" onclick="btnSave_Click" Text="Save" Font-Bold="true" Width="90" Height="28" CssClass="button green" />
                                                                        <asp:Button ID="Button1" runat="server" Text="Reset" Font-Bold="true" CssClass="button green"
                                                                            Width="90" Height="28" onclick="Button1_Click" />
                                                                        <asp:Button ID="btnCancel" runat="server" onclick="btnCancel_Click"    Text="Cancel" Font-Bold="true" Width="90" Height="28" CssClass="button green" />
                                                                    </td>
                                                                </tr>
                                                            <%--</asp:Panel>--%>
                                                        </tbody>
                                                        </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>              
            </div>
            <!-- end page-wrap -->
            <div id="footer">
                <ul>
                    <li>Copyright &copy; 2012. All rights reserved.</li>
                    <li>Powered by <a href="http://www.altius.co.in" target="_blank">Altius Infosystems</a></li>
                </ul>
            </div>
            <!-- end footer -->
        </form>
    </body>
</html>
