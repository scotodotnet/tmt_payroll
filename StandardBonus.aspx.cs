﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Payroll;
using Payroll.Configuration;
using Payroll.Data;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class GroupBonus : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    string staffLabour;
    string groupEmp;
    string chkboxValue;
    string DeductionVal;
    string SessionAdmin;
    //string Stafflabour;
    string SessionCcode;
    string SessionLcode;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        string ss = Session["UserId"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        lblComany.Text = SessionCcode + " - " + SessionLcode;
        if (!IsPostBack)
        {
            //Department();
            DropDwonCategory();
            int currentYear = Utility.GetFinancialYear;
            for (int i = 0; i < 10; i++)
            {
                ddFinance.Items.Add(new ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                //  ddlShowYear.Items.Add(new ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                currentYear = currentYear - 1;
            }
        }
        lblusername.Text = Session["Usernmdisplay"].ToString();
    }
    public void clear()
    {
        chkbasic.Checked = false;
        chkHRA.Checked = false;
        chkFDA.Checked = false;
        chkVDA.Checked = false;
        chkAllowance1.Checked = false;
        chkAllowance2.Checked = false;
        chkDeduction1.Checked = false;
        chkdeduction2.Checked = false;
        txtpercentage.Text = "";
    }
    public void DropDwonCategory()
    {
        DataTable dtcate = new DataTable();
        dtcate = objdata.DropDownCategory();
        ddlcategory.DataSource = dtcate;
        ddlcategory.DataTextField = "CategoryName";
        ddlcategory.DataValueField = "CategoryCd";
        ddlcategory.DataBind();
    }
    protected void ddlcategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        clear();
        DataTable dempty = new DataTable();
        //GVDays.DataSource = dempty;
        //GVDays.DataBind();
        //GVMonthLabour.DataSource = dempty;
        //GVMonthLabour.DataBind();
        GridviewStaff.DataSource = dempty;
        GridviewStaff.DataBind();
        ddldepartment.DataSource = dempty;
        ddldepartment.DataBind();

        if (ddlcategory.SelectedValue == "1")
        {
            staffLabour = "S";
        }
        else if (ddlcategory.SelectedValue == "2")
        {
            staffLabour = "L";
        }
        else
        {
            staffLabour = "0";
        }
        DataTable dtDip = new DataTable();
        dtDip = objdata.DropDownDepartment_category(staffLabour);
        if (dtDip.Rows.Count > 1)
        {
            ddldepartment.DataSource = dtDip;
            ddldepartment.DataTextField = "DepartmentNm";
            ddldepartment.DataValueField = "DepartmentCd";
            ddldepartment.DataBind();
        }
        else
        {
            DataTable dt = new DataTable();
            ddldepartment.DataSource = dt;
            ddldepartment.DataBind();
        }
    }
    protected void ddldepartment_SelectedIndexChanged(object sender, EventArgs e)
    {
        clear();
        DataTable dempty = new DataTable();
        //GVDays.DataSource = dempty;
        //GVDays.DataBind();
        //GVMonthLabour.DataSource = dempty;
        //GVMonthLabour.DataBind();
        GridviewStaff.DataSource = dempty;
        GridviewStaff.DataBind();
    }
    protected void btnclick_Click(object sender, EventArgs e)
    {
        try
        {
            bool ErrFlag = false;
            if (ddlcategory.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Category....!');", true);

                //System.Windows.Forms.MessageBox.Show("Select the Category", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            else if (ddldepartment.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Department....!');", true);

                //System.Windows.Forms.MessageBox.Show("Select the Category", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            else if (ddldepartment.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Department....!');", true);

                //System.Windows.Forms.MessageBox.Show("Select the Category", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            if (!ErrFlag)
            {

                if (ddlcategory.SelectedValue == "1")
                {
                    staffLabour = "S";
                }
                else if (ddlcategory.SelectedValue == "2")
                {
                    staffLabour = "L";
                }
                DataTable dtempLoad = new DataTable();
                dtempLoad = objdata.StandardBonusEmpLoad(ddldepartment.SelectedValue, staffLabour, ddFinance.SelectedValue, SessionCcode, SessionLcode);
                GridviewStaff.DataSource = dtempLoad;
                GridviewStaff.DataBind();
                int yr = Convert.ToInt32(ddFinance.SelectedValue);
                foreach (GridViewRow gvrow in GridviewStaff.Rows)
                {
                    Label lblempNoDet = (Label)gvrow.FindControl("lblempno");
                    DataTable dt_1 = new DataTable();
                    Label Netamt = (Label)gvrow.FindControl("lblNetAmount");
                    dt_1 = objdata.Salary_total_bonus(lblempNoDet.Text, yr, SessionCcode, SessionLcode);
                    //txtSalAmt.Text = dt_1.Rows[0]["NetPay"].ToString();
                    Netamt.Text = dt_1.Rows[0]["NetPay"].ToString();
                }
            }
        }
        catch (Exception ex)
        {
        }
    }
    protected void ddFinance_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void txtpercentage_TextChanged(object sender, EventArgs e)
    {

    }
    protected void btnpercentagecal_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        string basic;
        string HRA;
        string FDA;
        string VDA;
        string All1;
        string All2;
        string ded1;
        string ded2;
        if (txtpercentage.Text.Trim() == "")
        {
            ErrFlag = true;
        }
        if (!ErrFlag)
        {
            if (GridviewStaff.Rows.Count > 0)
            {
                foreach (GridViewRow gvrow in GridviewStaff.Rows)
                {
                    CheckBox check_box = (CheckBox)gvrow.FindControl("chkClear");
                    //if (check_box != null)
                    {
                        //if (check_box.Checked)
                        {
                            Label lblempNoDet = (Label)gvrow.FindControl("lblempno");
                            //Label lblbasicAmt = (Label)gvrow.FindControl("lblbasic");
                            //Label lblhraAmt = (Label)gvrow.FindControl("lblHRA");
                            //Label lblAllow1 = (Label)gvrow.FindControl("lblAllowancw1");
                            //Label lblAllow2 = (Label)gvrow.FindControl("lblAllowancw2");
                            //Label lblVDA = (Label)gvrow.FindControl("VDA");
                            //Label lblFDA = (Label)gvrow.FindControl("FDA");
                            //Label lblded1 = (Label)gvrow.FindControl("lblded1");
                            //Label lblded2 = (Label)gvrow.FindControl("lblded2");
                            Label lblzNet = (Label)gvrow.FindControl("lblNetAmount");
                            //basic = lblbasicAmt.Text;
                            //HRA = lblhraAmt.Text;
                            //FDA = lblFDA.Text;
                            //VDA = lblVDA.Text;
                            //All1 = lblAllow1.Text;
                            //All2 = lblAllow2.Text;
                            //ded1 = lblded1.Text;
                            //ded2 = lblded2.Text;
                            if (chkbasic.Checked == true)
                            {
                            }
                            else
                            {
                                basic = "0";
                            }
                            if (chkHRA.Checked == true)
                            {
                            }
                            else
                            {
                                HRA = "0";
                            }
                            if (chkFDA.Checked == true)
                            {
                            }
                            else
                            {
                                FDA = "0";
                            }
                            if (chkVDA.Checked == true)
                            {
                            }
                            else
                            {
                                VDA = "0";
                            }
                            if (chkAllowance1.Checked == true)
                            {
                            }
                            else
                            {
                                All1 = "0";
                            }
                            if (chkAllowance2.Checked == true)
                            {
                            }
                            else
                            {
                                All2 = "0";
                            }
                            if (chkDeduction1.Checked == true)
                            {
                            }
                            else
                            {
                                ded1 = "0";
                            }
                            if (chkdeduction2.Checked == true)
                            {
                            }
                            else
                            {
                                ded2 = "0";
                            }
                            
                            Label lblBonusPer = (Label)gvrow.FindControl("lblBonuspercentage");
                            Label Attenance = (Label)gvrow.FindControl("lblDays");
                            Label NetAmt = (Label)gvrow.FindControl("lblNetAmount");
                            lblBonusPer.Text = txtpercentage.Text;
                            ////Label lblBonusMonth = (Label)gvrow.FindControl("lblMonths");
                            //////lblBonusMonth.Text = txtMonths.Text;
                            ////decimal Addition = (Convert.ToDecimal(basic) + Convert.ToDecimal(HRA) + Convert.ToDecimal(All1) + Convert.ToDecimal(All2) + Convert.ToDecimal(FDA) + Convert.ToDecimal(VDA));
                            ////decimal Subtract = (Convert.ToDecimal(ded1) + Convert.ToDecimal(ded2));
                            ////decimal sumstaff = (Addition - Subtract);
                            ////decimal bonusamt = ((sumstaff / 100) * Convert.ToDecimal(txtpercentage.Text));
                            Label lblbonusAmt = (Label)gvrow.FindControl("lblBonusAmount");
                            ////lblbonusAmt.Text = (bonusamt).ToString();
                            Label lbltotal = (Label)gvrow.FindControl("lblBonus");
                            lblbonusAmt.Text = ((Convert.ToDecimal(NetAmt.Text) / 100) * Convert.ToDecimal(lblBonusPer.Text)).ToString();
                            lblbonusAmt.Text = (Math.Round(Convert.ToDecimal(lblbonusAmt.Text), 2, MidpointRounding.ToEven)).ToString();
                            lbltotal.Text = (Math.Round(Convert.ToDecimal(lblbonusAmt.Text), 2, MidpointRounding.ToEven)).ToString();
                            ////Label lblConAmt = (Label)gvrow.FindControl("lblConAmt");
                            ////decimal totalBonus = (bonusamt * Convert.ToDecimal(Attenance.Text));
                            ////lblBonusPer.Text = txtpercentage.Text;
                            ////lbltotal.Text = (totalBonus).ToString();
                            ////lbltotal.Text = (Math.Round(Convert.ToDecimal(lbltotal.Text), 0, MidpointRounding.AwayFromZero)).ToString();
                            ////chkbasic.Enabled = false;
                            ////chkHRA.Enabled = false;
                            ////chkAllowance1.Enabled = false;
                            ////chkAllowance2.Enabled = false;
                            ////chkFDA.Enabled = false;
                            ////chkDeduction1.Enabled = false;
                            ////chkdeduction2.Enabled = false;
                            ////chkVDA.Enabled = false;

                        }
                    }
                }
            }
        }
    }
    protected void btnstaffSave_Click(object sender, EventArgs e)
    {
        try
        {
            bool ErrFlag = false;
            bool Issaved = false;
            bool chkFlag = false;
            if (ddlcategory.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Category....!');", true);

                //System.Windows.Forms.MessageBox.Show("Select the Category", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            else if (ddlcategory.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Category....!');", true);

                //System.Windows.Forms.MessageBox.Show("Select the Category", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            else if (ddldepartment.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Department....!');", true);

                //System.Windows.Forms.MessageBox.Show("Select the Category", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            else if (ddldepartment.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Department....!');", true);

                //System.Windows.Forms.MessageBox.Show("Select the Category", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            else if (GridviewStaff.Rows.Count <= 0)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('No Employee Found....!');", true);

                //System.Windows.Forms.MessageBox.Show("Select the Category", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            //else if (chkbasic.Enabled == true)
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Calculate the Details....!');", true);

            //    //System.Windows.Forms.MessageBox.Show("Select the Category", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            //    ErrFlag = true;
            //}
            if (!ErrFlag)
            {
                string basic_1 = "";
                string HRA_1 = "";
                string FDA_1 = "";
                string VDA_1 = "";
                string Allowance1_1 = "";
                string Allownance2_1 = "";
                string Deduction1_1 = "";
                string deduction2_1 = "";
                string basic = "";
                string HRA = "";
                string FDA = "";
                string VDA = "";
                string All1 = "";
                string All2 = "";
                string ded1 = "";
                string ded2 = "";
                if (ddlcategory.SelectedValue == "1")
                {
                    staffLabour = "S";
                }
                else if(ddlcategory.SelectedValue == "2")
                {
                    staffLabour = "L";
                }
                foreach (GridViewRow gvrowSave in GridviewStaff.Rows)
                {
                    CheckBox check_box = (CheckBox)gvrowSave.FindControl("chkClear");
                    if (check_box != null)
                    {
                        if (check_box.Checked)
                        {
                            Label EmpNo = (Label)gvrowSave.FindControl("lblempno");
                            Label NetAmt = (Label)gvrowSave.FindControl("lblNetAmount");
                            //Label lblbasicAmt = (Label)gvrowSave.FindControl("lblbasic");
                            //Label lblhraAmt = (Label)gvrowSave.FindControl("lblHRA");
                            //Label lblAllow1 = (Label)gvrowSave.FindControl("lblAllowancw1");
                            //Label lblAllow2 = (Label)gvrowSave.FindControl("lblAllowancw2");
                            //Label lblVDA = (Label)gvrowSave.FindControl("VDA");
                            //Label lblFDA = (Label)gvrowSave.FindControl("FDA");
                            //Label lblded1 = (Label)gvrowSave.FindControl("lblded1");
                            //Label lblded2 = (Label)gvrowSave.FindControl("lblded2");
                            Label lblBonusPer = (Label)gvrowSave.FindControl("lblBonuspercentage");
                            Label lblBonusAmt = (Label)gvrowSave.FindControl("lblBonusAmount");
                            Label lblDays = (Label)gvrowSave.FindControl("lblDays");
                            Label lblBonus = (Label)gvrowSave.FindControl("lblBonus");
                            Label lblconAmt = (Label)gvrowSave.FindControl("lblConAmt");
                           
                            string empdetail = objdata.BonusFinancialYear(EmpNo.Text, ddFinance.SelectedValue, SessionCcode, SessionLcode);
                            if (empdetail == EmpNo.Text)
                            {
                            }
                            else
                            {
                                //if (chkbasic.Checked == true)
                                //{
                                //    basic = lblbasicAmt.Text;
                                //    basic_1 = "1";
                                //}
                                //else
                                //{
                                //    basic_1 = "0";
                                //    basic = "0";
                                //}
                                //if (chkHRA.Checked == true)
                                //{
                                //    HRA_1 = "1";
                                //    HRA = lblhraAmt.Text;
                                //}
                                //else
                                //{
                                //    HRA_1 = "0";
                                //    HRA = "0";
                                //}
                                //if (chkFDA.Checked == true)
                                //{
                                //    FDA_1 = "1";
                                //    FDA = lblFDA.Text;
                                //}
                                //else
                                //{
                                //    FDA_1 = "0";
                                //    FDA = "0";
                                //}
                                //if (chkVDA.Checked == true)
                                //{
                                //    VDA_1 = "1";
                                //    VDA = lblVDA.Text;
                                //}
                                //else
                                //{
                                //    VDA_1 = "0";
                                //    VDA = "0";
                                //}
                                //if (chkAllowance1.Checked == true)
                                //{
                                //    Allowance1_1 = "1";
                                //    All1 = lblAllow1.Text;
                                //}
                                //else
                                //{
                                //    Allowance1_1 = "0";
                                //    All1 = "0";
                                //}
                                //if (chkAllowance2.Checked == true)
                                //{
                                //    Allownance2_1 = "1";
                                //    All2 = lblAllow2.Text;
                                //}
                                //else
                                //{
                                //    Allownance2_1 = "0";
                                //    All2 = "0";
                                //}
                                //if (chkDeduction1.Checked == true)
                                //{
                                //    Deduction1_1 = "1";
                                //    ded1 = lblded1.Text;
                                //}
                                //else
                                //{
                                //    Deduction1_1 = "0";
                                //    ded1 = "0";
                                //}
                                //if (chkdeduction2.Checked == true)
                                //{
                                //    deduction2_1 = "1";
                                //    ded2 = lblded2.Text;
                                //}
                                //else
                                //{
                                //    deduction2_1 = "0";
                                //    ded2 = "0";
                                //}
                                chkboxValue = basic_1 + "," + HRA_1 + "," + FDA_1 + "," + VDA_1 + "," + Allowance1_1 + "," + Allownance2_1;
                                DeductionVal = Deduction1_1 + "," + deduction2_1;
                                //decimal Sumval = Convert.ToDecimal(basic) + Convert.ToDecimal(HRA) + Convert.ToDecimal(All1) + Convert.ToDecimal(All2) + Convert.ToDecimal(VDA) + Convert.ToDecimal(FDA);
                                //decimal SubVal = Convert.ToDecimal(ded1) + Convert.ToDecimal(ded2);
                                //decimal addval = Sumval - SubVal;
                                BonusforAllclass objbonus = new BonusforAllclass();
                                objbonus.EmpCode = EmpNo.Text;
                                objbonus.Department = ddldepartment.SelectedValue;
                                objbonus.Category = staffLabour;
                                objbonus.BonusCalculation = chkboxValue;
                                objbonus.DeductionChkVal = DeductionVal;
                                objbonus.Basic = "0";
                                objbonus.HRA = "0";
                                objbonus.Allowance1 = "0";
                                objbonus.Allowance2 = "0";
                                objbonus.deduction1 = "0";
                                objbonus.deduction2 = "0";
                                objbonus.CalculationAmt = "0";
                                objbonus.Percentage = lblBonusPer.Text;
                                objbonus.PercentageAmt = lblBonusAmt.Text;
                                objbonus.Noofmonths = "0";
                                objbonus.Attenance = lblDays.Text;
                                objbonus.BonusAmt = lblBonus.Text;
                                objbonus.Ccode = SessionCcode;
                                objbonus.Lcode = SessionLcode;
                                objbonus.FDA = "0";
                                objbonus.Finance = ddFinance.SelectedValue;
                                objbonus.VDA = "0";
                                objbonus.CalculationAmt = NetAmt.Text;
                                objdata.BonusforallInsert(objbonus);
                                
                                Issaved = true;
                            }
                        }
                        if (Issaved == true)
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Saved Successfully....!');", true);
                            DataTable dtempty = new DataTable();                           
                            GridviewStaff.DataSource = dtempty;
                            GridviewStaff.DataBind();
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Empoyee....!');", true);
                        }
                        
                    }
                }
            }
        }
        catch (Exception ex)
        {

        }
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("StandardBonus.aspx");
    }
    protected void btnEmp_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptEmpDownload.aspx");
    }
    protected void btnatt_Click(object sender, EventArgs e)
    {
        Response.Redirect("AttenanceDownload.aspx");
    }
    protected void btnLeave_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptLeaveSample.aspx");
    }
    protected void btnOT_Click(object sender, EventArgs e)
    {
        Response.Redirect("OTDownload.aspx");
    }
    protected void btncontract_Click(object sender, EventArgs e)
    {
        Response.Redirect("ContractRPT.aspx");
    }
    protected void btnSal_Click(object sender, EventArgs e)
    {
        Response.Redirect("FrmDeductionDownload.aspx");
    }
}
