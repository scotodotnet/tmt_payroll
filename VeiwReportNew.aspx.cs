﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using Altius.BusinessAccessLayer.BALDataAccess;
public partial class VeiwReportNew : System.Web.UI.Page
{
    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    ReportDocument rd = new ReportDocument();
    BALDataAccess objdata = new BALDataAccess();
    SqlConnection con;
    string str_month;
    string str_yr;
    string str_cate;
    string str_dept;
    string SessionCcode;
    string SessionLcode;
    string SessionAdmin;
    static string CmpName;
    static string Cmpaddress;
    string query;
    string fromdate;
    string EmployeeType;
    string ToDate;
    string salaryType;
    string Year_Str;
    string MonthAndYear;

    protected void Page_Load(object sender, EventArgs e)
    {
        SqlConnection con = new SqlConnection(constr);
        SessionAdmin = Session["Isadmin"].ToString();
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        string ss = Session["UserId"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();

        str_cate = Request.QueryString["Cate"].ToString();
        str_dept = Request.QueryString["Depat"].ToString();
        fromdate = Request.QueryString["fromdate"].ToString();
        ToDate = Request.QueryString["ToDate"].ToString();
        EmployeeType = Request.QueryString["EmpType"].ToString();

        DataTable dt = new DataTable();
        dt = objdata.Company_retrive(SessionCcode, SessionLcode);
        if (dt.Rows.Count > 0)
        {
            CmpName = dt.Rows[0]["Cname"].ToString();
            Cmpaddress = (dt.Rows[0]["Address1"].ToString() + ", " + dt.Rows[0]["Address2"].ToString() + ", " + dt.Rows[0]["Location"].ToString() + "-" + dt.Rows[0]["Pincode"].ToString());
        }

        query = "select ExisistingCode,EmpName,DepartmentNm,ISNULL([April],0) as April,ISNULL([May],0) as May,ISNULL([June],0) as June,ISNULL([July],0) as July, " +
              "ISNULL([August],0) as August,ISNULL([September],0) as September,ISNULL([October],0) as October,ISNULL([November],0) as November, " +
              "ISNULL([December],0) as December,ISNULL([January],0) as January,ISNULL([February],0) as February,ISNULL([March],0) as March from " +
              "( " +
              "select ED.ExisistingCode,ED.EmpName,MD.DepartmentNm,Days,Months from AttenanceDetails AD inner join EmployeeDetails ED on AD.EmpNo=ED.EmpNo " +
              "inner join MstDepartment MD on MD.DepartmentCd=ED.Department " +
              "where FromDate>=convert(datetime,'" + fromdate + "',103) and " +
              "ToDate<=convert(datetime,'" + ToDate + "',103) and ED.StafforLabor='" + str_cate + "' and ED.EmployeeType='" + EmployeeType + "' " +
              "group by Months,ED.ExisistingCode,Days,ED.EmpName,MD.DepartmentNm " +
              ") AS ppv " +
              "pivot(max(Days) for Months in ([April],[May],[June],[July],[August],[September],[October],[November],[December],[January],[February],[March])) ppt order by cast(ExisistingCode as decimal(18,2)) asc ";

        SqlCommand cmd = new SqlCommand(query, con);
        SqlDataAdapter sda = new SqlDataAdapter(cmd);
        DataSet ds1 = new DataSet();
        con.Open();
        sda.Fill(ds1);
        DataTable dt_1 = new DataTable();
        sda.Fill(dt_1);
        con.Close();

        if (ds1.Tables[0].Rows.Count != 0)
        {
            rd.Load(Server.MapPath("Payslip/Summary.rpt"));
            rd.SetDataSource(ds1.Tables[0]);

            rd.DataDefinition.FormulaFields["CompanyName"].Text = "'" + CmpName + "'";
            rd.DataDefinition.FormulaFields["Unit"].Text = "'" + SessionLcode + "'";
            //rd.DataDefinition.FormulaFields["Month"].Text = "'" + MonthAndYear + "'";
            rd.DataDefinition.FormulaFields["CompanyAddress1"].Text = "'" + Cmpaddress + "'";

            rd.DataDefinition.FormulaFields["ReportHead"].Text = "'" + "STAFF - DETAILS" + "'";

            CrystalReportViewer1.ReportSource = rd;
            CrystalReportViewer1.RefreshReport();
            CrystalReportViewer1.DataBind();
        }

    }
}
