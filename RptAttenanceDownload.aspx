﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="RptAttenanceDownload.aspx.cs" Inherits="RptAttenanceDownload" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table>
            <tr id="Panelload" runat="server" visible="false">
                <td>
                    <asp:GridView ID="gvEmp" runat="server" AutoGenerateColumns="false" >
                    <Columns>
                        <asp:TemplateField>
                            <HeaderTemplate>DeptName</HeaderTemplate>
                            <ItemTemplate>
                            <asp:Label ID="DeptName" runat="server" Text='<%# Eval("DepartmentNm") %>' ></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>MachineID</HeaderTemplate>
                            <ItemTemplate>
                            <asp:Label ID="MachineID" runat="server" Text='<%# Eval("MachineNo") %>' ></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>EmpNo</HeaderTemplate>
                            <ItemTemplate>
                            <asp:Label ID="EmpNo" runat="server" Text='<%# Eval("EmpNo") %>' ></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>ExistingCode</HeaderTemplate>
                            <ItemTemplate>
                            <asp:Label ID="ExistingCode" runat="server" Text='<%# Eval("ExisistingCode") %>' ></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>FirstName</HeaderTemplate>
                            <ItemTemplate>
                            <asp:Label ID="FirstName" runat="server" Text='<%# Eval("EmpName") %>' ></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>Days</HeaderTemplate>
                            <ItemTemplate>
                            <asp:Label ID="Days" runat="server" Text="" ></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>CL</HeaderTemplate>
                            <ItemTemplate>
                            <asp:Label ID="CL" runat="server" Text="" ></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>NFH</HeaderTemplate>
                            <ItemTemplate>
                            <asp:Label ID="lblNFh" runat="server" Text="" ></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>Absent Days</HeaderTemplate>
                            <ItemTemplate>
                            <asp:Label ID="lblAbsent" runat="server" Text="" ></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>Weekoff Days</HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblweekoff" runat="server" Text="" ></asp:Label>
                             </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
