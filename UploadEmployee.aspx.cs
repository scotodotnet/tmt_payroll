﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Configuration;
using System.Data.SqlClient;
using System.Data.OleDb;
using Payroll;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class UploadEmployee_1 : System.Web.UI.Page
{
    EmployeeClass objEmp = new EmployeeClass();
    System.Globalization.CultureInfo culterInfo = new System.Globalization.CultureInfo("en-GB");
    BALDataAccess objdata = new BALDataAccess();
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string dd;
    protected void Page_Load(object sender, EventArgs e)
    {
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        if (SessionAdmin == "2")
        {
            Response.Redirect("EmployeeRegistration.aspx");
        }
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionAdmin = Session["Isadmin"].ToString();
        lblComany.Text = SessionCcode + " - " + SessionLcode;
        lblusername.Text = Session["Usernmdisplay"].ToString();
    }
    protected void btnupload_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        string EmpCode = "";
        string DepartmentCode = "";
        string DistrictCode = "";
        string TalukCode = "";
        string QualificationCode = "";
        string EmployeeTypeCode = "";
        string StaffCode = "";
        string GenderCode = "";
        string NonPfGradeCode = "";
        string RolCode = "1";
        string ExistingCode = "";
        string name = "";
        string nm = "";
        string EmpNo = "";
        string Edu_code = "";
        string martialStatus = "";
        string Initial = "";
        string ContractCode = "";


        try
        {

            if (fileUpload.HasFile)
            {
                fileUpload.SaveAs(Server.MapPath("Upload/" + fileUpload.FileName));

                string connectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + Server.MapPath("Upload/" + fileUpload.FileName) + ";" + "Extended Properties=Excel 8.0;";
                OleDbConnection sSourceConnection = new OleDbConnection(connectionString);
                DataTable dts = new DataTable();
                using (sSourceConnection)
                {
                    sSourceConnection.Open();
                    OleDbCommand command = new OleDbCommand("Select ExisistingCode,EmpName,FatherName,Gender,DOB,PSAdd1,PSAdd2,PSDistrict,PSTaluk,PSState,Phone,Mobile,PassportNo,DrivingLicenceNo,Qualification,University,Department,Designation,EmployeeType,NonPFGrade,StafforLabor,Educated,MartialStatus,Initial,ContractType,BiometricID FROM [Sheet1$];", sSourceConnection);
                    sSourceConnection.Close();

                    using (OleDbCommand cmd = new OleDbCommand())
                    {
                        command.CommandText = "Select ExisistingCode,EmpName,FatherName,Gender,DOB,PSAdd1,PSAdd2,PSDistrict,PSTaluk,PSState,Phone,Mobile,PassportNo,DrivingLicenceNo,Qualification,University,Department,Designation,EmployeeType,NonPFGrade,StafforLabor,Educated,MartialStatus,Initial,ContractType,BiometricID FROM [Sheet1$]";
                        sSourceConnection.Open();
                    }
                    using (OleDbDataReader dr = command.ExecuteReader())
                    {

                        if (dr.HasRows)
                        {

                        }

                    }
                    OleDbDataAdapter objDataAdapter = new OleDbDataAdapter();
                    objDataAdapter.SelectCommand = command;
                    DataSet ds = new DataSet();

                    objDataAdapter.Fill(ds);
                    DataTable dt = new DataTable();
                    dt = ds.Tables[0];
                    for (int i = 0; i < dt.Columns.Count; i++)
                    {
                        dt.Columns[i].ColumnName = dt.Columns[i].ColumnName.Replace(" ", string.Empty).ToString();

                    }
                    string constr = ConfigurationManager.AppSettings["ConnectionString"];
                    for (int j = 0; j < dt.Rows.Count; j++)
                    {
                        SqlConnection cn = new SqlConnection(constr);
                        //string EmpNo = dt.Rows[j]["EmpNo"].ToString();
                        string Existing = dt.Rows[j]["ExisistingCode"].ToString();
                        string EmpName = dt.Rows[j]["EmpName"].ToString();
                        string FatherName = dt.Rows[j]["FatherName"].ToString();
                        string Gender = dt.Rows[j]["Gender"].ToString();
                        string DOB = dt.Rows[j]["DOB"].ToString();
                        string Add1 = dt.Rows[j]["PSAdd1"].ToString();
                        string Add2 = dt.Rows[j]["PSAdd2"].ToString();
                        string taluk = dt.Rows[j]["PSTaluk"].ToString();
                        string District = dt.Rows[j]["PSDistrict"].ToString();
                        string State = dt.Rows[j]["PSState"].ToString();
                        string Phone = dt.Rows[j]["Phone"].ToString();
                        string Mobile = dt.Rows[j]["Mobile"].ToString();
                        string Passport = dt.Rows[j]["PassportNo"].ToString();
                        string DL = dt.Rows[j]["DrivingLicenceNo"].ToString();
                        string Qualification = dt.Rows[j]["Qualification"].ToString();
                        string university = dt.Rows[j]["University"].ToString();
                        string Department = dt.Rows[j]["Department"].ToString();
                        string Designation = dt.Rows[j]["Designation"].ToString();
                        string Employeetype = dt.Rows[j]["EmployeeType"].ToString();
                        string NonPFGrade = dt.Rows[j]["NonPFGrade"].ToString();
                        string StaffLabour = dt.Rows[j]["StafforLabor"].ToString();
                        string Educated = dt.Rows[j]["Educated"].ToString();
                        string MartialStatus = dt.Rows[j]["MartialStatus"].ToString();
                        string initial = dt.Rows[j]["Initial"].ToString();
                        string ContractType = dt.Rows[j]["ContractType"].ToString();
                        string Bio = dt.Rows[j]["BiometricID"].ToString();
                        string EMP = "";
                        if (EmpName == "")
                        {
                            j = j + 2;
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Employee Name. The Row Number is " + j + "');", true);
                            //System.Windows.Forms.MessageBox.Show("Enter the Employee Name. the Row number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                            ErrFlag = true;
                        }
                        else if (Existing == "")
                        {
                            j = j + 2;
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Existing code. The Row Number is " + j + "');", true);
                            //System.Windows.Forms.MessageBox.Show("Enter the Existing Code. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                            ErrFlag = true;
                        }
                        else if (FatherName == "")
                        {
                            j = j + 2;
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Father's Name. The Row Number is " + j + "');", true);
                            //System.Windows.Forms.MessageBox.Show("Enter the Father's Name. the Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                            ErrFlag = true;
                        }
                        else if (Gender == "")
                        {
                            j = j + 2;
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Gender. The Row Number is " + j + "');", true);
                            //System.Windows.Forms.MessageBox.Show("Enter the Gender. The Roe Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                            ErrFlag = true;
                        }
                        else if (DOB == "")
                        {
                            j = j + 2;
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter date of Birth. The Row Number is " + j + "');", true);
                            //System.Windows.Forms.MessageBox.Show("Enter the Date of Birth. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                            ErrFlag = true;
                        }
                        else if (Add1 == "")
                        {
                            j = j + 2;
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Address . The Row Number is " + j + "');", true);
                            //System.Windows.Forms.MessageBox.Show("Enter Address. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                            ErrFlag = true;
                        }
                        //else if (taluk == "")
                        //{
                        //    j = j + 2;
                        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Taluk. The Row Number is " + j + "');", true);
                        //    //System.Windows.Forms.MessageBox.Show("Enter the Taluk. the Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                        //    ErrFlag = true;
                        //}
                        //else if (District == "")
                        //{
                        //    j = j + 2;
                        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter District. The Row Number is " + j + "');", true);
                        //    //System.Windows.Forms.MessageBox.Show("Enter District. the Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                        //    ErrFlag = true;
                        //}
                        else if (State == "")
                        {
                            j = j + 2;
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter State. The Row Number is " + j + "');", true);
                            //System.Windows.Forms.MessageBox.Show("Enter State The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                            ErrFlag = true;
                        }
                        //else if (Phone == "")
                        //{
                        //    j = j + 2;
                        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Phone Number. The Row Number is " + j + "');", true);
                        //    //System.Windows.Forms.MessageBox.Show("Enter the Phone Number. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                        //    ErrFlag = true;
                        //}
                        //else if (Mobile == "")
                        //{
                        //    j = j + 2;
                        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Mobile Number. The Row Number is " + j + "');", true);
                        //    //System.Windows.Forms.MessageBox.Show("Enter Mobile Number. the Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                        //    ErrFlag = true;
                        //}
                        //else if ((Passport == "") && (DL == ""))
                        //{
                        //    j = j + 2;
                        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Passport or Driving Licence. The Row Number is " + j + "');", true);
                        //    //System.Windows.Forms.MessageBox.Show("Enter the Passport or Driving Licence Number. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                        //    ErrFlag = true;
                        //}
                        else if (Department == "")
                        {
                            j = j + 2;
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Department. The Row Number is " + j + "');", true);
                            //System.Windows.Forms.MessageBox.Show("Enter the Department. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                            ErrFlag = true;
                        }
                        else if (Designation == "")
                        {
                            j = j + 2;
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Designation. The Row Number is " + j + "');", true);
                            //System.Windows.Forms.MessageBox.Show("Enter Designation. the Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                            ErrFlag = true;
                        }
                        else if (Employeetype == "")
                        {
                            j = j + 2;
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Employee type. The Row Number is " + j + "');", true);
                            //System.Windows.Forms.MessageBox.Show("Enter Employee Type. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                            ErrFlag = true;
                        }
                        else if (NonPFGrade == "")
                        {
                            j = j + 2;
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Non PF Grade. The Row Number is " + j + "');", true);
                            //System.Windows.Forms.MessageBox.Show("Enter On Role or Off Role. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                            ErrFlag = true;
                        }
                        else if (StaffLabour == "")
                        {
                            j = j + 2;
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Staff. The Row Number is " + j + "');", true);
                            //System.Windows.Forms.MessageBox.Show("Enter Staff or labour. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                            ErrFlag = true;
                        }

                        else if (ExistingCode == "0")
                        {
                            j = j + 2;
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Existing Code properly " + j + "');", true);
                            //System.Windows.Forms.MessageBox.Show("Enter the Existing Code. the Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                            ErrFlag = true;
                        }
                        else if (Educated == "")
                        {
                            j = j + 2;
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Education " + j + "');", true);
                            //System.Windows.Forms.MessageBox.Show("Enter Education. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                            ErrFlag = true;
                        }
                        else if (MartialStatus.Trim() == "")
                        {
                            j = j + 2;
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Martial Status M or U " + j + "');", true);
                            //System.Windows.Forms.MessageBox.Show("Enter Education. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                            ErrFlag = true;
                        }
                        else if (initial.Trim() == "")
                        {
                            j = j + 2;
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Initial F or S " + j + "');", true);
                            //System.Windows.Forms.MessageBox.Show("Enter Education. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                            ErrFlag = true;
                        }
                        else if (Employeetype.Trim().ToLower() == "contract")
                        {
                            if (ContractType.Trim() == "")
                            {
                                j = j + 2;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Initial F or S " + j + "');", true);
                                //System.Windows.Forms.MessageBox.Show("Enter Education. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                ErrFlag = true;
                            }
                        }
                        else if (Bio.Trim() == "")
                        {
                            j = j + 2;
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Biometric ID " + j + "');", true);
                            //System.Windows.Forms.MessageBox.Show("Enter Education. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                            ErrFlag = true;
                        }
                        cn.Open();
                        string qry = "Select DepartmentNm from MstDepartment where DepartmentNm = '" + Department + "'";
                        SqlCommand cmd = new SqlCommand(qry, cn);
                        SqlDataReader sdr = cmd.ExecuteReader();
                        if (sdr.HasRows)
                        {


                        }
                        else
                        {
                            j = j + 2;
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('This Department name Not Availabe in Department Master.The Row Number is " + j + " ');", true);
                            //System.Windows.Forms.MessageBox.Show("This Department Name Not Available", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                            ErrFlag = true;

                        }
                        cn.Close();
                        
                        //cn.Open();
                        //string qry2 = "Select TalukNm from MstTaluk where TalukNm='" + taluk + "' and DistrictCd in (Select DistrictCd from MstDistrict " +
                        //              "where DistrictNm = '" + District + "')";
                        //SqlCommand cmd2 = new SqlCommand(qry2, cn);
                        //SqlDataReader sdr2 = cmd2.ExecuteReader();
                        //if (sdr2.HasRows)
                        //{
                        //}
                        //else
                        //{
                        //    j = j + 2;
                        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('This Taluk Name Not Availabe in Taluk Master.The Row Number is " + j + " ');", true);
                        //    //System.Windows.Forms.MessageBox.Show("This Taluk Name Not Available in Taluk Master. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                        //    ErrFlag = true;
                        //}
                        //cn.Close();
                        
                        if (Qualification.Trim() == "")
                        {
                        }
                        else
                        {
                            cn.Open();
                            string qry3 = "Select QualificationNm from MstQualification where QualificationNm = '" + Qualification + "'";
                            SqlCommand cmd3 = new SqlCommand(qry3, cn);
                            SqlDataReader sdr3 = cmd3.ExecuteReader();
                            if (sdr3.HasRows)
                            {
                            }
                            else
                            {
                                j = j + 2;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('This Qualification Not Availabe in Qualification Master.The Row Number is " + j + " ');", true);
                                //System.Windows.Forms.MessageBox.Show("Qualification Not Available in Qualification Master. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                ErrFlag = true;
                            }
                            cn.Close();
                        }
                        cn.Open();
                        string qry4 = "select EmpType from MstEmployeeType where EmpType = '" + Employeetype + "'";
                        SqlCommand cmd4 = new SqlCommand(qry4, cn);
                        SqlDataReader sdr4 = cmd4.ExecuteReader();
                        if (sdr4.HasRows)
                        {
                        }
                        else
                        {
                            j = j + 2;
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('This Employee type Not Availabe in Employee type Master.The Row Number is " + j + " ');", true);
                            //System.Windows.Forms.MessageBox.Show("Employee Type Not Available Employee Type Master. the Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                            ErrFlag = true;
                        }
                        cn.Close();
                        if (Employeetype == "Contract")
                        {
                            cn.Open();
                            string qry1 = "Select ContractName from ContractMaster where ContractName = '" + ContractType + "'";
                            SqlCommand cmd1 = new SqlCommand(qry1, cn);
                            SqlDataReader sdr1 = cmd1.ExecuteReader();
                            if (sdr1.HasRows)
                            {
                            }
                            else
                            {
                                j = j + 2;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('This Contract Name Not Availabe in Contract Master.The Row Number is " + j + " ');", true);
                                //System.Windows.Forms.MessageBox.Show("This District name Not Available in District Master. the Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                ErrFlag = true;
                            }
                            cn.Close();
                        }
                        //cn.Open();
                        //string qry_exist = "Select ExisistingCode from EmployeeDetails where ExisistingCode = '" + ExistingCode + "'";
                        //SqlCommand cmd_exist = new SqlCommand(qry_exist, cn);
                        //SqlDataReader sdr_exist = cmd_exist.ExecuteReader();
                        //if(sdr_exist.HasRows)
                        //{
                        //    j = j + 2;
                        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('This Exist No already Exist.The Row Number is " + j + " ');", true);
                        //    ErrFlag = true;
                        //}
                        //else
                        //{
                        //}
                        //cn.Close();
                    }
                    if (!ErrFlag)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            SqlConnection cn = new SqlConnection(constr);
                            //string qry_dt = "select DistrictCd from MstDistrict where DistrictNm = '" + dt.Rows[i]["PSDistrict"].ToString() + "'";
                            //SqlCommand cmd_dt = new SqlCommand(qry_dt, cn);
                            cn.Open();
                            //DistrictCode = Convert.ToString(cmd_dt.ExecuteScalar());
                            //string qry_tk = "Select TalukCd from MstTaluk where TalukNm = '" + dt.Rows[i]["PSTaluk"] + "'";
                            //SqlCommand cmd_tk = new SqlCommand(qry_tk, cn);
                            //TalukCode = Convert.ToString(cmd_tk.ExecuteScalar());
                            if (dt.Rows[i]["Qualification"].ToString().Trim() == "")
                            {
                                QualificationCode = "0";
                            }
                            else
                            {
                                string qry_ql = "Select QualificationCd from MstQualification where QualificationNm = '" + dt.Rows[i]["Qualification"].ToString() + "'";
                                SqlCommand cmd_ql = new SqlCommand(qry_ql, cn);
                                QualificationCode = Convert.ToString(cmd_ql.ExecuteScalar());
                            }
                            if (dt.Rows[i]["EmployeeType"].ToString().Trim().ToLower() == "contract")
                            {
                                string qry_q2 = "Select ContractCode from ContractMaster where ContractName = '" + dt.Rows[i]["ContractType"].ToString() + "'";
                                SqlCommand cmd_q2 = new SqlCommand(qry_q2, cn);
                                ContractCode = Convert.ToString(cmd_q2.ExecuteScalar());
                            }
                            else
                            {
                                ContractCode = "0";
                            }
                            string qry_dpt = "Select DepartmentCd from MstDepartment where DepartmentNm = '" + dt.Rows[i]["Department"].ToString() + "'";
                            SqlCommand cmd_dpt = new SqlCommand(qry_dpt, cn);
                            DepartmentCode = Convert.ToString(cmd_dpt.ExecuteScalar());
                            string qry_EmpType = "Select EmpTypeCd from MstEmployeeType where EmpType='" + dt.Rows[i]["EmployeeType"].ToString() + "'";
                            SqlCommand cmd_empType = new SqlCommand(qry_EmpType, cn);
                            EmployeeTypeCode = Convert.ToString(cmd_empType.ExecuteScalar());
                            string qry_exist = "Select ExisistingCode from EmployeeDetails where ExisistingCode = '" + dt.Rows[i]["ExisistingCode"].ToString() + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                            SqlCommand cmd_exist = new SqlCommand(qry_exist, cn);
                            ExistingCode = Convert.ToString(cmd_exist.ExecuteScalar());
                            if (dt.Rows[i]["StafforLabor"].ToString().ToLower().Trim() == "staff")
                            {
                                StaffCode = "S";
                                EmpCode = "Emp";
                            }
                            else if (dt.Rows[i]["StafforLabor"].ToString().ToLower().Trim() == "labour")
                            {
                                StaffCode = "L";
                                name = dt.Rows[i]["EmployeeType"].ToString();
                                nm = name.Substring(0, 3);
                                EmpCode = nm;

                            }
                            else
                            {
                                ErrFlag = true;
                            }
                            if (dt.Rows[i]["Gender"].ToString().ToLower().Trim() == "male")
                            {
                                GenderCode = "1";
                            }
                            else if (dt.Rows[i]["Gender"].ToString().ToLower().Trim() == "female")
                            {
                                GenderCode = "2";
                            }
                            else
                            {
                                ErrFlag = true;
                            }
                            if (dt.Rows[i]["NonPFGrade"].ToString().ToLower().Trim() == "yes")
                            {
                                NonPfGradeCode = "1";
                            }
                            else if (dt.Rows[i]["NonPFGrade"].ToString().ToLower().Trim() == "no")
                            {
                                NonPfGradeCode = "2";
                            }
                            else
                            {
                                ErrFlag = true;
                            }
                            if (dt.Rows[i]["Educated"].ToString().ToLower().Trim() == "yes")
                            {
                                Edu_code = "1";
                            }
                            else if (dt.Rows[i]["Educated"].ToString().ToLower().Trim() == "no")
                            {
                                Edu_code = "2";
                            }
                            else
                            {
                                ErrFlag = true;
                            }
                            if (dt.Rows[i]["MartialStatus"].ToString() == "U")
                            {
                                objEmp.Martial = "U";
                            }
                            else if (dt.Rows[i]["MartialStatus"].ToString() == "M")
                            {
                                objEmp.Martial = "M";
                            }
                            if (dt.Rows[i]["Initial"].ToString() == "F")
                            {
                                objEmp.Initial = "F";
                            }
                            else if (dt.Rows[i]["Initial"].ToString() == "M")
                            {
                                objEmp.Initial = "S";
                            }
                            if (!ErrFlag)
                            {
                                string MonthYear = System.DateTime.Now.ToString("MMyyyy");
                                string RegNo = objdata.MaxEmpNo();
                                int Reg = Convert.ToInt32(RegNo);
                                EmpNo = EmpCode + MonthYear + RegNo;
                                DateTime MyDateTime = new DateTime();
                                //MyDateTime = new DateTime();
                                objEmp.EmpCode = EmpNo;
                                objEmp.MachineCode = EmpNo;
                                objEmp.ExisitingCode = dt.Rows[i]["ExisistingCode"].ToString();
                                objEmp.EmpName = dt.Rows[i]["EmpName"].ToString();
                                objEmp.EmpFname = dt.Rows[i]["FatherName"].ToString();
                                objEmp.Gender = GenderCode;
                                objEmp.DOB = dt.Rows[i]["DOB"].ToString();
                                dd = Convert.ToDateTime(objEmp.DOB).ToShortDateString();
                                //MyDateTime = DateTime.ParseExact(dd, "dd/MM/yyyy", null);
                                objEmp.PSAdd1 = dt.Rows[i]["PSAdd1"].ToString();
                                objEmp.PSAdd2 = dt.Rows[i]["PSAdd2"].ToString();
                                objEmp.PSDistrict = dt.Rows[i]["PSDistrict"].ToString();
                                objEmp.PSTaluk = dt.Rows[i]["PSTaluk"].ToString();
                                objEmp.UnEducated = Edu_code;
                                objEmp.PSState = dt.Rows[i]["PSState"].ToString();
                                objEmp.ContractType = ContractCode;
                                objEmp.Bio = dt.Rows[i]["BiometricID"].ToString();
                                //objEmp.UnEducated = dt.Rows[i]["ExisistingCode"].ToString();
                                //objEmp.SchoolCollege = dt.Rows[i]["ExisistingCode"].ToString();


                                objEmp.Phone = dt.Rows[i]["Phone"].ToString();
                                objEmp.Mobile = dt.Rows[i]["Mobile"].ToString();

                                objEmp.PassportNo = dt.Rows[i]["PassportNo"].ToString();
                                objEmp.DrivingLicNo = dt.Rows[i]["DrivingLicenceNo"].ToString();
                                objEmp.Qualification = QualificationCode;
                                objEmp.University = dt.Rows[i]["University"].ToString();

                                objEmp.Department = DepartmentCode;
                                objEmp.Desingation = dt.Rows[i]["Designation"].ToString();

                                objEmp.EmployeeType = EmployeeTypeCode;
                                objEmp.Ccode = SessionCcode;
                                objEmp.Lcode = SessionLcode;
                                if (ExistingCode == dt.Rows[i]["ExisistingCode"].ToString())
                                {

                                }
                                else
                                {
                                    objdata.InsertEmployeeDetails(objEmp, Reg, dd, StaffCode, NonPfGradeCode, RolCode);
                                }
                                cn.Close();
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Uploaded Successfully...');", true);
                                //System.Windows.Forms.MessageBox.Show("Uploaded Successfully", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);


                            }
                        }
                    }
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Your File Not Upload...');", true);
                    //System.Windows.Forms.MessageBox.Show("Your File not Uploaded.", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);

                }
            }
        }
        catch
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Please Upload Correct File Format...');", true);
            //System.Windows.Forms.MessageBox.Show("Please Upload Correct File Format", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
        }
    }
    protected void btnEmp_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptEmpDownload.aspx");
    }
    protected void btnatt_Click(object sender, EventArgs e)
    {
        Response.Redirect("AttenanceDownload.aspx");
    }
    protected void btnLeave_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptLeaveSample.aspx");
    }
    protected void btnOT_Click(object sender, EventArgs e)
    {
        Response.Redirect("OTDownload.aspx");
    }
    protected void btncontract_Click(object sender, EventArgs e)
    {
        Response.Redirect("ContractRPT.aspx");
    }
    protected void btnSal_Click(object sender, EventArgs e)
    {
        Response.Redirect("FrmDeductionDownload.aspx");
    }
}
