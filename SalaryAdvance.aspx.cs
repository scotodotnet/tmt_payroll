﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using Payroll;
using Payroll.Configuration;
using Payroll.Data;

using Altius.BusinessAccessLayer.BALDataAccess;

public partial class SalaryAdvance_1 : System.Web.UI.Page
{
    protected System.Resources.ResourceManager rm;
    protected string PayrollRegisterContentMeta, pageTitle;
    BALDataAccess objdata = new BALDataAccess();
    AdvanceAmount objsal = new AdvanceAmount();
    bool searchFlag = false;
    string Datetime_ser;
    DateTime mydate;
    //string Mont;
    //int Monthid;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    protected void Page_Load(object sender, EventArgs e)
    {
        SessionAdmin = Session["Isadmin"].ToString();
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        string ss = Session["UserId"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        lblComany.Text = SessionCcode + " - " + SessionLcode;
        if (!IsPostBack)
        {
            category();
            SettingServerDate();
            //DropDownDepart();

        }
        lblusername.Text = Session["Usernmdisplay"].ToString();
    }
    public void category()
    {
        DataTable dtcate = new DataTable();
        dtcate = objdata.DropDownCategory();
        ddlcategory.DataSource = dtcate;
        ddlcategory.DataTextField = "CategoryName";
        ddlcategory.DataValueField = "CategoryCd";
        ddlcategory.DataBind();
    }
    public void SettingServerDate()
    {
        Datetime_ser = objdata.ServerDate();
        txtDate.Text = Datetime_ser;
    }
    public void DropDownDepart()
    {
        DataTable dt = new DataTable();
        dt = objdata.Depart_Labour();
        ddlDepartment.DataSource = dt;
        ddlDepartment.DataTextField = "DepartmentNm";
        ddlDepartment.DataValueField = "DepartmentCd";
        ddlDepartment.DataBind();
    }
    private void clear()
    {
        //txtempNo.Text = "";
        //txtexist.Text = "";
        txtmonthly.Text = "";
        txtadvance.Text = "";

        lbldoj1.Text = "";
        lbldepartment1.Text = "";
        lblDesignation1.Text = "";
        lblprofile1.Text = "";
        lblwages1.Text = "";
        lblBasic11.Text = "";
        txtmonths.Text = "";
        txtBalance.Text = "0";

    }
    protected void btnClear_Click(object sender, EventArgs e)
    {
        clear();
        DataTable dtempty = new DataTable();
        ddlempno.DataSource = dtempty;
        ddlempno.DataBind();
        ddlEmpname.DataSource = dtempty;
        ddlEmpname.DataBind();
        txtexist.Text = "";
        ddlDepartment.SelectedValue = "0";
    }
    protected void txtempNo_TextChanged(object sender, EventArgs e)
    {
        if (searchFlag == false)
        {
            clear();
            txtexist.Text = "";
        }
    }
    protected void txtexist_TextChanged(object sender, EventArgs e)
    {
        if (searchFlag == false)
        {
            clear();
            //txtempNo.Text = "";
        }
    }
    protected void btnsearch_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        DataTable dt = new DataTable();
        txtmonthly.Text = "";
        txtadvance.Text = "";
        //lblname1.Text = "";
        lbldoj1.Text = "";
        lbldepartment1.Text = "";
        lblDesignation1.Text = "";
        lblprofile1.Text = "";
        lblwages1.Text = "";
        lblBasic11.Text = "";
        txtBalance.Text = "0";
        if (((txtexist.Text == "")))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Enter the Existing Number...!');", true);
            //System.Windows.Forms.MessageBox.Show("Enter the Existing Number...!", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            ErrFlag = true;
        }
        else if (ddlDepartment.SelectedValue == "0")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select the Department...!');", true);
            //System.Windows.Forms.MessageBox.Show("Select the Department...!", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            ErrFlag = true;
        }
        if (!ErrFlag)
        {
            if (txtexist.Text != "")
            {
                if (SessionAdmin == "1")
                {
                    dt = objdata.AdvanceSalary_exist(txtexist.Text, ddlDepartment.SelectedValue, SessionCcode, SessionLcode);
                }
                else
                {
                    dt = objdata.AdvanceSalary_exist_user(txtexist.Text, ddlDepartment.SelectedValue, SessionCcode, SessionLcode);
                }
            }
            if (dt.Rows.Count > 0)
            {
                searchFlag = true;
                txtexist.Text = dt.Rows[0]["ExisistingCode"].ToString();
                ddlempno.SelectedValue = dt.Rows[0]["EmpNo"].ToString();
                ddlEmpname.SelectedValue = dt.Rows[0]["EmpNo"].ToString();
                lbldoj1.Text = dt.Rows[0]["Dateofjoining"].ToString();
                lbldepartment1.Text = dt.Rows[0]["DepartmentNm"].ToString();
                lblDesignation1.Text = dt.Rows[0]["Designation"].ToString();
                lblBasic11.Text = dt.Rows[0]["Base"].ToString();
                lblprofile1.Text = dt.Rows[0]["ProfileType"].ToString();
                lblwages1.Text = dt.Rows[0]["Wagestype"].ToString();
                string EmpNo1 = objdata.Advance_insert_verify(ddlempno.Text, SessionCcode, SessionLcode);
                if (EmpNo1 == ddlempno.Text)
                {
                    DataTable dtret = new DataTable();
                    dtret = objdata.Advance_restore(ddlempno.Text, SessionCcode, SessionLcode);
                    txtadvance.Text = dtret.Rows[0]["Amount"].ToString();
                    txtBalance.Text = dtret.Rows[0]["BalanceAmount"].ToString();
                    txtmonthly.Text = dtret.Rows[0]["MonthlyDeduction"].ToString();
                }

            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Employee Number Not Found....!');", true);
                //System.Windows.Forms.MessageBox.Show("Employee Number Not Found....!", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                clear();

            }
        }
        searchFlag = false;
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            bool ErrFlag = false;
            bool issaved = false;
            bool IsUpdated = true;
            if (ddlEmpname.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Search the Employee....!');", true);
                //System.Windows.Forms.MessageBox.Show("Search the Employee....!", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            else if (ddlempno.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Enter the Employee....!');", true);
                //System.Windows.Forms.MessageBox.Show("Enter the Employee....!", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            else if (txtadvance.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Enter the Advance Amount....!');", true);
                //System.Windows.Forms.MessageBox.Show("Enter the Advance Amount....!", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            else if (Convert.ToDecimal(txtadvance.Text) == 0)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Enter the Advance Amount Properly....!');", true);
                //System.Windows.Forms.MessageBox.Show("Enter the Advance Amount Properly....!", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            else if (txtmonthly.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Enter the Monthly Deduction....!');", true);
                //System.Windows.Forms.MessageBox.Show("Enter the Monthly Deduction....!", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            else if (Convert.ToDecimal(txtmonthly.Text) == 0)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Enter the Monthly Deduction Properly....!');", true);
                //System.Windows.Forms.MessageBox.Show("Enter the Monthly Deduction Properly....!", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            if (!ErrFlag)
            {
                string EmpId = objdata.Advance_employee_verify(txtexist.Text, ddlEmpname.Text);
                string EmpNo1 = objdata.Advance_insert_verify(ddlempno.Text, SessionCcode, SessionLcode);

                if (Convert.ToDecimal(txtmonthly.Text) > Convert.ToDecimal(txtadvance.Text))
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Enter the Monthly Deduction Properly....!');", true);
                    //System.Windows.Forms.MessageBox.Show("Enter the Monthly Deduction Properly....!", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    ErrFlag = true;
                }
                else if (EmpId == ddlempno.Text)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Enter the Employee Number Properly....!');", true);
                    //System.Windows.Forms.MessageBox.Show("Enter the Employee Number Properly....!", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    ErrFlag = true;
                }

                //else if (EmpNo1 == ddlempno.Text)
                //{
                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Already Loan Availed....!');", true);
                //    //System.Windows.Forms.MessageBox.Show("Already Loan Availed....!", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                //    ErrFlag = true;
                //}
                if (!ErrFlag)
                {
                    EmpNo1 = objdata.Advance_insert_verify(ddlempno.Text, SessionCcode, SessionLcode);

                    if (EmpNo1 == ddlempno.Text)
                    {
                        decimal balance = (Convert.ToDecimal(txtBalance.Text) / Convert.ToInt32(txtmonths.Text));
                        if (balance == Convert.ToDecimal(txtmonthly.Text))
                        {

                            objdata.AdvanceAmount_Update_Loan(ddlempno.SelectedValue, txtmonths.Text.Trim(), txtBalance.Text.Trim(), SessionCcode, SessionLcode, txtmonthly.Text.Trim());
                            IsUpdated = true;
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Click the Calculate Button....!');", true);
                        }

                    }
                    else
                    {
                        mydate = DateTime.ParseExact(txtDate.Text, "dd-MM-yyyy", null);
                        objsal.EmpNo = ddlempno.SelectedValue;
                        objsal.Exist = txtexist.Text;
                        objsal.Months = txtmonths.Text;
                        objsal.Amount = txtadvance.Text;
                        objsal.Ccode = SessionCcode;
                        objsal.Lcode = SessionLcode;
                        objsal.MontlyDeduction = txtmonthly.Text;
                        objdata.Advance_insert(objsal, mydate);

                        issaved = true;
                    }

                }
                if (issaved == true)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Saved Successfully....!');", true);
                    //System.Windows.Forms.MessageBox.Show("Saved Successfully....!", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                }
                if (IsUpdated == true)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Updated Successfully....!');", true);
                    //System.Windows.Forms.MessageBox.Show("Saved Successfully....!", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                }

            }
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Contact Admin....!');", true);
            //System.Windows.Forms.MessageBox.Show("Contact Admin....!", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
        }
    }
    protected void txtmonths_TextChanged(object sender, EventArgs e)
    {
        txtmonthly.Text = "";
    }
    protected void btncal_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        txtmonthly.Text = "";
        if (txtadvance.Text == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Enter the Amount....!');", true);
            //System.Windows.Forms.MessageBox.Show("Enter the Amount....!", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            ErrFlag = true;
        }
        else if (txtmonths.Text == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Enter the No of Months....!');", true);
            //System.Windows.Forms.MessageBox.Show("Enter the No of Months....!", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            ErrFlag = true;
        }
        if (!ErrFlag)
        {
            if (Convert.ToDecimal(txtadvance.Text) == 0)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Enter the Amount Properly....!');", true);
                //System.Windows.Forms.MessageBox.Show("Enter the Amount Properly....!", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            else if (Convert.ToDecimal(txtmonths.Text) > Convert.ToDecimal(txtadvance.Text))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Enter the Amount Properly....!');", true);
                //System.Windows.Forms.MessageBox.Show("Enter the Amount Properly....!", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            else if (Convert.ToDecimal(txtmonths.Text) == 0)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Enter the No of Months Properly....!');", true);
                //System.Windows.Forms.MessageBox.Show("Enter the No of Months Properly....!", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            if (!ErrFlag)
            {
                string EmpNo1 = objdata.Advance_insert_verify(ddlempno.Text, SessionCcode, SessionLcode);
                if (EmpNo1 == ddlempno.SelectedValue)
                {
                    decimal repay = (Convert.ToDecimal(txtBalance.Text) / Convert.ToDecimal(txtmonths.Text));
                    repay = Math.Round(repay, 2, MidpointRounding.ToEven);
                    txtmonthly.Text = repay.ToString();
                }
                else
                {
                    decimal repay = (Convert.ToDecimal(txtadvance.Text) / Convert.ToDecimal(txtmonths.Text));
                    repay = Math.Round(repay, 2, MidpointRounding.ToEven);
                    txtmonthly.Text = repay.ToString();
                }


            }
            else
            {
                txtmonthly.Text = "";
            }
        }
    }
    protected void txtadvance_TextChanged(object sender, EventArgs e)
    {
        txtmonthly.Text = "";
    }
    protected void btnHistory_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptAdvance.aspx");
    }
    protected void ddlDepartment_SelectedIndexChanged(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        DataTable dtempty = new DataTable();
        ddlempno.DataSource = dtempty;
        ddlempno.DataBind();
        ddlEmpname.DataSource = dtempty;
        ddlEmpname.DataBind();
        txtexist.Text = "";
        string stafforLabour = "";
        if (ddlDepartment.SelectedValue == "0")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select the Department Properly....!');", true);
            //System.Windows.Forms.MessageBox.Show("Select the Department Properly....!", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            ErrFlag = true;
        }
        if (!ErrFlag)
        {
            DataTable dtempcode = new DataTable();
            if (ddlcategory.SelectedValue == "1")
            {
                stafforLabour = "S";
            }
            else if (ddlcategory.SelectedValue == "2")
            {
                stafforLabour = "L";
            }
            
            if (SessionAdmin == "1")
            {
                dtempcode = objdata.LoadEmployeeForBonus(stafforLabour, ddlDepartment.SelectedValue, SessionCcode, SessionLcode);
            }
            else
            {
                dtempcode = objdata.EmpLoadFORBonus_USER(stafforLabour, ddlDepartment.SelectedValue, SessionCcode, SessionLcode);
            }
            ddlempno.DataSource = dtempcode;
            DataRow dr = dtempcode.NewRow();
            dr["EmpNo"] = ddlDepartment.SelectedItem.Text;
            dr["EmpName"] = ddlDepartment.SelectedItem.Text;
            dtempcode.Rows.InsertAt(dr, 0);

            ddlempno.DataTextField = "EmpNo";
            ddlempno.DataValueField = "EmpNo";
            ddlempno.DataBind();
            ddlEmpname.DataSource = dtempcode;
            ddlEmpname.DataTextField = "EmpName";
            ddlEmpname.DataValueField = "EmpNo";
            ddlEmpname.DataBind();
        }
        clear();
    }
    protected void ddlempno_SelectedIndexChanged(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        clear();
        if (ddlempno.SelectedValue == "0")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Employee Code....!');", true);
            //System.Windows.Forms.MessageBox.Show("Select the Employee Code....!", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            ErrFlag = true;
        }
        if (!ErrFlag)
        {
            DataTable dt = new DataTable();
            dt = objdata.AdvanceSalary_EmpNo(ddlempno.SelectedValue, SessionCcode, SessionLcode);
            if (dt.Rows.Count > 0)
            {
                ddlEmpname.DataSource = dt;
                ddlEmpname.DataTextField = "EmpName";
                ddlEmpname.DataValueField = "EmpNo";
                ddlEmpname.DataBind();
                txtexist.Text = dt.Rows[0]["ExisistingCode"].ToString();
                //ddlempno.Text = dt.Rows[0]["EmpNo"].ToString();
                //ddlEmpname.Text = dt.Rows[0]["EmpName"].ToString();
                lbldoj1.Text = dt.Rows[0]["Dateofjoining"].ToString();
                lbldepartment1.Text = dt.Rows[0]["DepartmentNm"].ToString();
                lblDesignation1.Text = dt.Rows[0]["Designation"].ToString();
                lblBasic11.Text = dt.Rows[0]["Base"].ToString();
                lblprofile1.Text = dt.Rows[0]["ProfileType"].ToString();
                lblwages1.Text = dt.Rows[0]["Wagestype"].ToString();
                txtBalance.Text = "0";
                string EmpNo1 = objdata.Advance_insert_verify(ddlempno.Text, SessionCcode, SessionLcode);
                if (EmpNo1 == ddlempno.Text)
                {
                    DataTable dtret = new DataTable();
                    dtret = objdata.Advance_restore(ddlempno.Text, SessionCcode, SessionLcode);
                    txtadvance.Text = dtret.Rows[0]["Amount"].ToString();
                    txtBalance.Text = dtret.Rows[0]["BalanceAmount"].ToString();
                    txtmonthly.Text = dtret.Rows[0]["MonthlyDeduction"].ToString();
                    txtmonths.Text = dtret.Rows[0]["DueMonth"].ToString();

                }
            }
        }
    }
    protected void ddlEmpname_SelectedIndexChanged(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        clear();
        if (ddlEmpname.SelectedValue == "0")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Employee Name....!');", true);
            //System.Windows.Forms.MessageBox.Show("Select the Employee Name....!", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            ErrFlag = true;
        }
        if (!ErrFlag)
        {
            DataTable dt = new DataTable();
            dt = objdata.AdvanceSalary_EmpNo(ddlEmpname.SelectedValue, SessionCcode, SessionLcode);
            if (dt.Rows.Count > 0)
            {
                ddlempno.DataSource = dt;
                ddlempno.DataTextField = "EmpNo";
                ddlempno.DataValueField = "EmpNo";
                ddlempno.DataBind();
                txtexist.Text = dt.Rows[0]["ExisistingCode"].ToString();
                //ddlempno.Text = dt.Rows[0]["EmpNo"].ToString();
                //ddlEmpname.Text = dt.Rows[0]["EmpName"].ToString();
                lbldoj1.Text = dt.Rows[0]["Dateofjoining"].ToString();
                lbldepartment1.Text = dt.Rows[0]["DepartmentNm"].ToString();
                lblDesignation1.Text = dt.Rows[0]["Designation"].ToString();
                lblBasic11.Text = dt.Rows[0]["Base"].ToString();
                lblprofile1.Text = dt.Rows[0]["ProfileType"].ToString();
                lblwages1.Text = dt.Rows[0]["Wagestype"].ToString();
                txtBalance.Text = "0";
                string EmpNo1 = objdata.Advance_insert_verify(ddlempno.Text, SessionCcode, SessionLcode);
                if (EmpNo1 == ddlempno.Text)
                {
                    DataTable dtret = new DataTable();
                    dtret = objdata.Advance_restore(ddlempno.Text, SessionCcode, SessionLcode);
                    txtadvance.Text = dtret.Rows[0]["Amount"].ToString();
                    txtBalance.Text = dtret.Rows[0]["BalanceAmount"].ToString();
                    txtmonthly.Text = dtret.Rows[0]["MonthlyDeduction"].ToString();
                }
            }
        }
    }
    protected void btnrepay_Click(object sender, EventArgs e)
    {
        Response.Redirect("AdvanceAmtrepay.aspx");
    }
    protected void btnEmp_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptEmpDownload.aspx");
    }
    protected void btnatt_Click(object sender, EventArgs e)
    {
        Response.Redirect("AttenanceDownload.aspx");
    }
    protected void btnLeave_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptLeaveSample.aspx");
    }
    protected void btnOT_Click(object sender, EventArgs e)
    {
        Response.Redirect("OTDownload.aspx");
    }
    protected void ddlcategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        string staffLabour_temp;
        DataTable dtempty = new DataTable();
        ddlDepartment.DataSource = dtempty;
        ddlDepartment.DataBind();
        if (ddlcategory.SelectedValue == "1")
        {
            staffLabour_temp = "S";
        }
        else if (ddlcategory.SelectedValue == "2")
        {
            staffLabour_temp = "L";
        }
        else
        {
            staffLabour_temp = "0";
        }
        DataTable dtDip = new DataTable();
        dtDip = objdata.DropDownDepartment_category(staffLabour_temp);
        if (dtDip.Rows.Count > 1)
        {
            ddlDepartment.DataSource = dtDip;
            ddlDepartment.DataTextField = "DepartmentNm";
            ddlDepartment.DataValueField = "DepartmentCd";
            ddlDepartment.DataBind();
        }
        else
        {
            DataTable dt = new DataTable();
            ddlDepartment.DataSource = dt;
            ddlDepartment.DataBind();
        }
    }
    protected void btncontract_Click(object sender, EventArgs e)
    {
        Response.Redirect("ContractRPT.aspx");
    }
    protected void btnSal_Click(object sender, EventArgs e)
    {
        Response.Redirect("FrmDeductionDownload.aspx");
    }
}
