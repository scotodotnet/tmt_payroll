﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class loanMaster : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    Bankloanclass objMas = new Bankloanclass();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            LoanDisplay();
        }
    }
    public void LoanDisplay()
    {
        DataTable dt = new DataTable();
        dt = objdata.LoanDisplay();
        GridLoan.DataSource = dt;
        GridLoan.DataBind();
    }
    protected void GridLoan_RowEditing(object sender, GridViewEditEventArgs e)
    {
        GridLoan.EditIndex = e.NewEditIndex;
        LoanDisplay();
    }
    protected void GridLoan_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        bool ErrFlag = false;
        try
        {
            TextBox txtLoannm1 = (TextBox)GridLoan.Rows[e.RowIndex].FindControl("txtloannm");
            if (txtLoannm1.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Loan Name');", true);
                ErrFlag = true;
            }
            if (!ErrFlag)
            {
                Label LblLoancd1 = (Label)GridLoan.Rows[e.RowIndex].FindControl("lblLoancd");

                objMas.LoanID = LblLoancd1.Text;
                objMas.LoanName = txtLoannm1.Text;
                objdata.UpdateLoan(objMas);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Loan Name Update Successfully..');", true);
                GridLoan.EditIndex = -1;
                LoanDisplay();
            }
        }
        catch (Exception ex)
        {

        }
    }
    protected void GridLoan_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void GridLoan_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        GridLoan.EditIndex = -1;
        LoanDisplay();
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            bool ErrFlag = false;
            if ((txtLoanCode.Text == "") || (txtLoanCode.Text == null))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Enter the Loan ID....!');", true);
                ErrFlag = true;
            }
            else if ((txtLoanname.Text == "") || (txtLoanname.Text == null))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Enter the Loan Name....!');", true);
                ErrFlag = true;
            }
            if (!ErrFlag)
            {
                objMas.LoanID = txtLoanCode.Text.Trim();
                objMas.LoanName = txtLoanname.Text.Trim();
                try
                {
                    objdata.LoanMaster_Insert(objMas);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Loan Saved Successfully..!');", true);
                    LoanDisplay();
                    txtLoanCode.Text = "";
                    txtLoanname.Text = "";
                }
                catch (Exception ex)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Contact Admin....!');", true);
                    ErrFlag = true;
                }
            }
        }
        catch (Exception ex)
        {
        }

    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("MstLoan.aspx");
    }
}
