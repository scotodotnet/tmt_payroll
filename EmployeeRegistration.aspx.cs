﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Specialized;
using Payroll;
using Payroll.Configuration;
using Payroll.Data;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.IO;
using System.Drawing;
using iTextSharp.text.pdf;
using iTextSharp.text.html;
using iTextSharp.text.html.simpleparser;

public partial class EmployeeRegistration_1 : System.Web.UI.Page
{
    protected System.Resources.ResourceManager rm;
    protected string PayrollRegisterContentMeta, pageTitle;
    System.Globalization.CultureInfo culterInfo = new System.Globalization.CultureInfo("en-GB");
    BALDataAccess objdata = new BALDataAccess();
    string stafflabor;
    string NonPFGrade;
    DataTable dttaluk = new DataTable();
    static string UserID = "", SesRoleCode = "", sessionAdmin = "";
    string stafflabour_temp;
    bool textSearch = false;
    string SessionCcode;
    string SessionLcode;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        UserID = Session["UserId"].ToString();
        SesRoleCode = Session["RoleCode"].ToString();
        sessionAdmin = Session["Isadmin"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        lblComany.Text = SessionCcode + " - " + SessionLcode;
        if (sessionAdmin == "1")
        {
            lblnonpfgrade.Visible = true;
            chkboxnonpfgrade.Visible = true;
        }
        else
        {
            lblnonpfgrade.Visible = false;
            chkboxnonpfgrade.Visible = false;
        }


        // PanelEditTextboxPanel.Visible = false;


        if (Request.QueryString != null)
        {
            //  PanelEditTextboxPanel.Visible = true;
        }


        if (!IsPostBack)
        {
            Panelstafflabor.Visible = false;
            District();
            //EmployeeType();
            Qualification();
            Department();
            LabourType();
            DropdownProbationPeriod();
            DropDwonCategory();
            Dropdowncontract();
            string LeaveconutMsg = objdata.LeavecountMessage();
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert(' " + LeaveconutMsg + " - Staff Apply For Leave ');", true);
            //panelLeave.Visible = true;
            //lblLeave.Text = LeaveconutMsg + " Staffs - " + "Apply For Leave";

        }
        lblusername.Text = Session["Usernmdisplay"].ToString();

    }
    public void Dropdowncontract()
    {
        DataTable dt_empty = new DataTable();
        ddlContract.DataSource = dt_empty;
        ddlContract.DataBind();
        DataTable dt = new DataTable();
        dt = objdata.DropDown_contract();
        ddlContract.DataSource = dt;
        ddlContract.DataTextField = "ContractName";
        ddlContract.DataValueField = "ContractCode";
        ddlContract.DataBind();
    }
    //Load Category
    public void DropDwonCategory()
    {
        DataTable dtcate = new DataTable();
        dtcate = objdata.DropDownCategory();
        ddlcategory.DataSource = dtcate;
        ddlcategory.DataTextField = "CategoryName";
        ddlcategory.DataValueField = "CategoryCd";
        ddlcategory.DataBind();
    }
    public void Employee_EmpNo_load()
    {
        //DataTable dtcate = new DataTable();
        //if (ddlcategory.SelectedValue == "1")
        //{
        //    stafflabor = "S";
        //}
        //else if (ddlcategory.SelectedValue == "2")
        //{
        //    stafflabor = "L";
        //}
        //    dtcate = objdata.Employee_EmpNo_load(SessionCcode,SessionLcode,sessionAdmin,stafflabor);
        //    ddlEmpNo.DataSource = dtcate;
        //    DataRow dr = dtcate.NewRow();
        //    dr["EmpNo"] = ddlcategory.SelectedItem.Text;
        //    dr["ExisistingCode"] = ddlcategory.SelectedItem.Text;
        //    dtcate.Rows.InsertAt(dr, 0);
        //    ddlEmpNo.DataTextField = "EmpNo";
        //    ddlEmpNo.DataValueField = "EmpNo";
       
        //    ddlEmpNo.DataBind();
        //    ddlExist.DataSource = dtcate;
        //    ddlExist.DataTextField = "ExisistingCode";
        //    ddlExist.DataValueField = "ExisistingCode";
        //    ddlExist.DataBind();
        

        
    }
    
    public void District()
    {


        DataTable dt1 = new DataTable();
        dt1 = objdata.DropDownDistrict();
        ddlPSDistrict.DataSource = dt1;
        ddlPSDistrict.DataTextField = "DistrictNm";
        ddlPSDistrict.DataValueField = "DistrictCd";
        ddlPSDistrict.DataBind();
    }
    public void DropdownProbationPeriod()
    {
        DataTable dtProbation = new DataTable();
        dtProbation = objdata.DropDownProbationPriod();
        ddlprobationpd.DataSource = dtProbation;
        ddlprobationpd.DataTextField = "ProbationMonth";
        ddlprobationpd.DataValueField = "ProbationCd";
        ddlprobationpd.DataBind();
    }
    public void EmployeeType()
    {
        DataTable dtemp = new DataTable();
        dtemp = objdata.EmployeeDropDown_no(ddlcategory.SelectedValue);
        ddemployeetype.DataSource = dtemp;
        ddemployeetype.DataTextField = "EmpType";
        ddemployeetype.DataValueField = "EmpTypeCd";
        ddemployeetype.DataBind();
    }
    public void LabourType()
    {
        DataTable dtemp = new DataTable();
        dtemp = objdata.LabourDropDown();
        ddllabourtype.DataSource = dtemp;
        ddllabourtype.DataTextField = "EmpType";
        ddllabourtype.DataValueField = "EmpTypeCd";
        ddllabourtype.DataBind();
    }
    public void Qualification()
    {
        DataTable dtQuali = new DataTable();
        dtQuali = objdata.Qualification();
        ddlqualification.DataSource = dtQuali;
        ddlqualification.DataTextField = "QualificationNm";
        ddlqualification.DataValueField = "QualificationCd";
        ddlqualification.DataBind();

    }
    public void Department()
    {
        DataTable dtDip = new DataTable();
        dtDip = objdata.DDdEPARTMENT_LOAD();
        ddldepartment.DataSource = dtDip;
        ddldepartment.DataTextField = "DepartmentNm";
        ddldepartment.DataValueField = "DepartmentCd";
        ddldepartment.DataBind();
    }

    protected void ddlPSDistrict_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable dttaluk = new DataTable();
        dttaluk = objdata.DropDownPSTaluk(ddlPSDistrict.SelectedValue);
        ddlPSTaluk.DataSource = dttaluk;
        ddlPSTaluk.DataTextField = "TalukNm";
        ddlPSTaluk.DataValueField = "TalukCd";
        ddlPSTaluk.DataBind();

    }
    protected void ddlcategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        Clear();
        Panelstafflabor.Visible = false;
        panelcontract.Visible = false;
        //DataTable dttaluk = new DataTable();
        ////dttaluk = objdata.DropDownPSTaluk(ddlPSDistrict.SelectedValue);
        ////ddlPSTaluk.DataSource = dttaluk;
        ////ddlPSTaluk.DataTextField = "TalukNm";
        ////ddlPSTaluk.DataValueField = "TalukCd";
        ////ddlPSTaluk.DataBind();

    }

    protected void rbtneducateduneducated_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (rbtneducateduneducated.SelectedValue == "1")
        {
            Panelstafflabor.Visible = true;
            Paneluneducated.Visible = false;
            PanelEducated.Visible = true;


        }
        else if (rbtneducateduneducated.SelectedValue == "2")
        {
            Panelstafflabor.Visible = true;
            Paneluneducated.Visible = false;
            PanelEducated.Visible = false;
            PanelSelectEducated.Visible = true;


        }
    }

    protected void btnclick_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        panelcontract.Visible = false;
        if (ddlcategory.SelectedValue == "0")
        {
            //panelError.Visible = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select Any One Staff or Labour');", true);
            //lblError.Text = "Select the Category";
            ErrFlag = true;
        }

        if (!ErrFlag)
        {
            EmployeeType();
            Employee_EmpNo_load();
            panelError.Visible = false;
            if (ddlcategory.SelectedValue == "1")
            {
                Panelstafflabor.Visible = true;
                Paneluneducated.Visible = false;
                PanelSelectEducated.Visible = false;
                PanelEducated.Visible = true;


            }
            else if (ddlcategory.SelectedValue == "2")
            {
                Panelstafflabor.Visible = true;
                Paneluneducated.Visible = false;
                PanelSelectEducated.Visible = true;
                PanelEducated.Visible = false;
                PanelSelectEducated.Visible = true;

            }
        }
    }

    protected void btncancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("EmployeeRegistration.aspx");
    }

    protected void btnsave_Click(object sender, EventArgs e)
    {
        try
        {
            string EmpNo = "";
            string EmNm = "";
            DateTime MyDateTime;
            MyDateTime = new DateTime();
            lblError.Text = "";
            EmployeeClass objEmp = new EmployeeClass();

            bool isRegistered = false;
            bool ErrFlag = false;
            bool Updateerr = false;
            bool updateregister = false;
            bool PhoneFlag = false;
            if (txtempname.Text == "" && txtempfname.Text == "" && txtPSAdd1.Text == "" && txtPSAdd2.Text == "")
            {
                //panelError.Visible = true;
                //lblError.Text = "Enter your Name\\n Enter your Father Name\\n Enter your Address";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Your Name\\nEnter Your Father Name\\nEnter Your PSAddress1\\nEnterYourPSAddress2\\nEnter Your PMAddress1\\nEnter Your PMAddress2');", true);
                //System.Windows.Forms.MessageBox.Show("Enter Your Name\\nEnter Your Father Name\\nEnter Your PSAddress1\\nEnterYourPSAddress2\\nEnter Your PMAddress1\\nEnter Your PMAddress2", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
               
            }
            else if (txtdob.Text == "")
            {
                //panelError.Visible = true;
                //lblError.Text = "Select Your Date of Birth\\nSelect Your PMDistrict\\nSelect Your PSDistrict\\nSelect Your PMTaluk\\nSelect Your PSTaluk";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select Your Date of Birth\\nSelect Your PMDistrict\\nSelect Your PSDistrict\\nSelect Your PMTaluk\\nSelect Your PSTaluk');", true);
                //System.Windows.Forms.MessageBox.Show("Select Your Date of Birth\\nSelect Your PMDistrict\\nSelect Your PSDistrict\\nSelect Your PMTaluk\\nSelect Your PSTaluk", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            //else if (txtuniversity.Text == "" && txtdesignation.Text == "")
            //{
            //    //panelError.Visible = true;
            //    //lblError.Text = "Enter Your Mobile No\\nEnter Your BloodGroup\\nEnter Your University Name\\nEnter Your Company Name\\nEnter Your Department\\nEnter Your Designation";
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Your Mobile No\\nEnter Your BloodGroup\\nEnter Your University Name\\nEnter Your Company Name\\nEnter Your Department\\nEnter Your Designation');", true);
            //    //System.Windows.Forms.MessageBox.Show("Enter Your Mobile No\\nEnter Your BloodGroup\\nEnter Your University Name\\nEnter Your Company Name\\nEnter Your Department\\nEnter Your Designation", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            //    ErrFlag = true;
            //}
            else if (ddldepartment.Text == "0")
            {
                //panelError.Visible = true;
                //lblError.Text = "Select the Department";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Department...!');", true);
                //System.Windows.Forms.MessageBox.Show("Select The Department", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            else if ((txtexistiongno.Text == "") || (txtexistiongno.Text == "0"))
            {
                //panelError.Visible = true;
                //lblError.Text = "Enter the Existing Code";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Existing No...!');", true);
                //System.Windows.Forms.MessageBox.Show("Enter the Existing No", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            else if (txtphone.Text != "")
            {
                if (txtPhone1.Text == "")
                {
                    //panelError.Visible = true;
                    //lblError.Text = "Enter the Phone No Properly";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Phone No properly...!');", true);
                    //System.Windows.Forms.MessageBox.Show("Enter the Phone No Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    ErrFlag = true;
                }
            }

            else if (txtphone.Text == "" && txtPhone1.Text == "")
            {
                //if (txtmobile.Text == "" && txtMobileCode.Text == "")
                //{
                //    //panelError.Visible = true;
                //    //lblError.Text = "Enter the Mobile No or Phone No Properly";
                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Mobile No or phone No properly...!');", true);
                //    //System.Windows.Forms.MessageBox.Show("Enter The Mobile No or Phone number Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                //    ErrFlag = true;
                //}
                if (txtMobileCode.Text != "")
                {
                    if (txtmobile.Text == "")
                    {
                        //panelError.Visible = true;
                        //lblError.Text = "Enter the Mobile No Properly";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Mobile No properly...!');", true);
                        //System.Windows.Forms.MessageBox.Show("Enter the Mobile No Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                        ErrFlag = true;
                    }
                }
            }

            if (!ErrFlag)
            {
                panelError.Visible = false;
                if (PanelEmployeeSearch.Visible == false)
                {
                    bool ErrFlag1 = false;
                    if (ddlcategory.SelectedValue == "1")
                    {
                        EmNm = "Emp";
                    }
                    else if (ddlcategory.SelectedValue == "2")
                    {

                        string Name = ddemployeetype.SelectedItem.Text;
                        string Nm = Name.Substring(0, 3);
                        EmNm = Nm;
                    }
                    string ExistingNo = objdata.ExistingNO_Verify(txtexistiongno.Text,SessionCcode,SessionLcode);
                    if (ExistingNo == txtexistiongno.Text)
                    {
                        //panelError.Visible = true;
                        //lblError.Text = "Existing no already Exist...!";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Existing No already Exist...!');", true);
                        //System.Windows.Forms.MessageBox.Show("Existing No Already Exist", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                        ErrFlag1 = true;
                    }
                    if (!ErrFlag1)
                    {
                        string MonthYear = System.DateTime.Now.ToString("MMyyyy");
                        // string dd = System.DateTime.Now.ToString("ddMMyyy");
                        string RegNo = objdata.MaxEmpNo();
                        int Reg = Convert.ToInt32(RegNo);
                        EmpNo = EmNm + MonthYear + RegNo;
                        objEmp.EmpCode = EmpNo;
                        objEmp.MachineCode = EmpNo;
                        objEmp.ExisitingCode = txtexistiongno.Text;
                        objEmp.EmpName = txtempname.Text;
                        objEmp.EmpFname = txtempfname.Text;
                        objEmp.Gender = rbtngender.SelectedValue;
                        objEmp.DOB = txtdob.Text;
                       // MyDateTime = DateTime.ParseExact(objEmp.DOB, "dd-MM-yyyy", culterInfo.DateTimeFormat);
                        objEmp.PSAdd1 = txtPSAdd1.Text;
                        objEmp.PSAdd2 = txtPSAdd2.Text;

                        objEmp.PSTaluk = txtTaluk.Text;
                        objEmp.PSDistrict = txtDistrict.Text;
                        objEmp.PSState = txtpsstate.Text;
                        if (ddlcategory.SelectedValue == "1")
                        {
                            objEmp.UnEducated = "1";
                        }
                        else if (ddlcategory.SelectedValue == "2")
                        {
                            objEmp.UnEducated = rbtneducateduneducated.SelectedValue;
                        }
                        objEmp.SchoolCollege = txtscholl.Text;


                        objEmp.Phone = (txtphone.Text + "-" + txtPhone1.Text);
                        objEmp.Mobile = (txtMobileCode.Text + "-" + txtmobile.Text);

                        objEmp.PassportNo = txtppno.Text;
                        objEmp.DrivingLicNo = txtdrivingno.Text;
                        objEmp.Qualification = ddlqualification.SelectedValue;
                        objEmp.University = txtuniversity.Text;

                        objEmp.Department = ddldepartment.SelectedValue;
                        objEmp.Desingation = txtdesignation.Text;

                        objEmp.EmployeeType = ddemployeetype.SelectedValue;
                        objEmp.Ccode = SessionCcode;
                        objEmp.Lcode = SessionLcode;
                        objEmp.Bio = txtbio.Text.Trim();
                        if (ddemployeetype.SelectedValue == "3")
                        {
                            objEmp.ContractType = ddlContract.SelectedValue;
                        }
                        else
                        {
                            objEmp.ContractType = "0";
                        }

                        string stafflabor = "", RoleCode = "";
                        if (ddlcategory.SelectedValue == "1")
                        {
                            stafflabor = "S";
                        }
                        else if (ddlcategory.SelectedValue == "2")
                        {
                            stafflabor = "L";
                        }
                        if (UserID == "Admin")
                        {
                            RoleCode = "1";

                        }
                        else
                        {
                            RoleCode = "2";
                        }
                        if (chkboxnonpfgrade.Checked == true)
                        {
                            NonPFGrade = "1";
                        }
                        else if (chkboxnonpfgrade.Checked == false)
                        {
                            NonPFGrade = "2";
                        }
                        objEmp.MaritalStatus = Rbmartial.SelectedValue;
                        objEmp.Initial = Rbfather.SelectedValue;
                        if (chkInsOuts.Checked == true) { objEmp.Ins_Outs = "INSIDER"; }
                        else { objEmp.Ins_Outs = "OUTSIDER"; }

                        try
                        {
                            panelError.Visible = false;
                            objdata.InsertEmployeeDetails(objEmp, Reg, txtdob.Text, stafflabor, NonPFGrade, RoleCode);
                            isRegistered = true;

                        }
                        catch (Exception ex)
                        {
                            //panelError.Visible = true;
                            //lblError.Text = "Server Error - Contact Admin";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Server Error Contact Admin....!');", true);
                            //System.Windows.Forms.MessageBox.Show("Contact Admin", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                            ErrFlag = true;
                        }
                    }

                }
                else if (PanelEmployeeSearch.Visible == true)
                {
                    string EmpVerify = objdata.EmployeeVerify(txtempNo.Text);

                    if (txtempNo.Text == "")
                    {
                        //panelError.Visible = true;
                        //lblError.Text = "Enter the Employee No Properly";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Employee Id properly....');", true);
                        //System.Windows.Forms.MessageBox.Show("Enter the Employee No Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                        Updateerr = true;
                    }
                    else if (txtempNo.Text != EmpVerify)
                    {
                        //panelError.Visible = true;
                        //lblError.Text = "Enter the Employee No Properly";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Employee Id properly....');", true);
                        //System.Windows.Forms.MessageBox.Show("Enter the Employee No Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                        Updateerr = true;
                    }
                    if (!Updateerr)
                    {
                        objEmp.EmpCode = txtempNo.Text;
                        objEmp.MachineCode = txtempNo.Text;
                        objEmp.ExisitingCode = txtexistiongno.Text;
                        objEmp.EmpName = txtempname.Text;
                        objEmp.EmpFname = txtempfname.Text;
                        objEmp.Gender = rbtngender.SelectedValue;
                        objEmp.DOB = txtdob.Text;
                        //MyDateTime = DateTime.ParseExact(objEmp.DOB, "dd-MM-yyyy", null);
                        objEmp.PSAdd1 = txtPSAdd1.Text;
                        objEmp.PSAdd2 = txtPSAdd2.Text;
                        objEmp.PSTaluk = txtTaluk.Text;
                        objEmp.PSDistrict = txtDistrict.Text;
                        objEmp.PSState = txtpsstate.Text;
                        if (ddlcategory.SelectedValue == "1")
                        {
                            objEmp.UnEducated = "1";
                        }
                        else if (ddlcategory.SelectedValue == "2")
                        {
                            objEmp.UnEducated = rbtneducateduneducated.SelectedValue;
                        }
                        
                        objEmp.SchoolCollege = txtscholl.Text;
                        objEmp.Phone = (txtphone.Text + "-" + txtPhone1.Text);
                        objEmp.Mobile = (txtMobileCode.Text + "-" + txtmobile.Text);
                        objEmp.PassportNo = txtppno.Text;
                        objEmp.DrivingLicNo = txtdrivingno.Text;
                        objEmp.Qualification = ddlqualification.SelectedValue;
                        objEmp.University = txtuniversity.Text;
                        objEmp.Department = ddldepartment.SelectedValue;
                        objEmp.Desingation = txtdesignation.Text;
                        //objEmp.UnEduDept = txtunedudept.Text;
                        //objEmp.UnEduDesig = txtunedudesg.Text;
                        //     objEmp.Shift = rbtnshift.SelectedItem.Text;
                        objEmp.EmployeeType = ddemployeetype.SelectedValue;
                        objEmp.Ccode = SessionCcode;
                        objEmp.Lcode = SessionLcode;
                        objEmp.Bio = txtbio.Text.Trim();
                        if (ddlcategory.SelectedValue == "1")
                        {
                            stafflabor = "S";
                        }
                        else if (ddlcategory.SelectedValue == "2")
                        {
                            stafflabor = "L";
                        }
                        // = rbtnlaburstaff.SelectedItem.Text;
                        //  string str = "1";
                        if (chkboxnonpfgrade.Checked == true)
                        {
                            NonPFGrade = "1";
                        }
                        else if (chkboxnonpfgrade.Checked == false)
                        {
                            NonPFGrade = "2";
                        }
                        if (UserID == "Admin")
                        {

                        }
                        objEmp.MaritalStatus = Rbmartial.SelectedValue;
                        objEmp.Initial = Rbfather.SelectedValue;
                        
                        if (chkInsOuts.Checked == true) { objEmp.Ins_Outs = "INSIDER"; }
                        else { objEmp.Ins_Outs = "OUTSIDER"; }

                        try
                        {
                            panelError.Visible = false;
                            objdata.UpdateEmployeeRegistration(objEmp, txtempNo.Text, stafflabor, txtdob.Text, NonPFGrade);
                            updateregister = true;

                        }
                        catch (Exception ex)
                        {
                            //panelError.Visible = true;
                            //lblError.Text = "Server Error - Contact Admin";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Server Error Contact Admin....!');", true);
                            //System.Windows.Forms.MessageBox.Show("Contact Admin", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                            ErrFlag = true;
                        }

                    }
                }
            }


            if (isRegistered)
            {
                if (SesRoleCode == "1")
                {
                    //panelsuccess.Visible = true;
                    //lbloutput.Text = "Saved Successfully";
                    //btncancel.Enabled = false;
                    //btnsearch.Enabled = false;
                    //btnsave.Enabled = false;
                    //btnedit.Enabled = false;
                    //btnreset.Enabled = false;
                    //panelError.Visible = false;
                    //btnok.Focus();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Your Registration No : " + EmpNo + "');", true);
                    //System.Windows.Forms.MessageBox.Show("Your Registration No : " + EmpNo, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    Clear();
                    //txtempname.Focus();
                }
                else if (SesRoleCode == "2")
                {
                    //lblError.Text = "Employee Registration Successfully";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Employee Registration Successfully...');", true);
                    //System.Windows.Forms.MessageBox.Show("Employee Registered Successfully", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    Clear();
                    ////panelsuccess.Visible = true;
                    ////lbloutput.Text = "Saved Successfully";
                    //btncancel.Enabled = false;
                    //btnsearch.Enabled = false;
                    //btnsave.Enabled = false;
                    //btnedit.Enabled = false;
                    //btnreset.Enabled = false;
                    //panelError.Visible = false;
                    //btnok.Focus();

                }

            }
            if (updateregister)
            {
                //lblError.Text = "Modification done Successfully";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Modification done Successfully...');", true);
                //System.Windows.Forms.MessageBox.Show("Modification Done Successfully", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                Clear();
                //panelsuccess.Visible = true;
                //lbloutput.Text = "Updated Successfully";
                //btnok.Visible = true;
                //btncancel.Enabled = false;
                //btnsearch.Enabled = false;
                //btnsave.Enabled = false;
                //btnedit.Enabled = false;
                //btnreset.Enabled = false;
                //panelError.Visible = false;
                //btnok.Focus();

            }
        }

        catch (Exception ex)
        {
            //panelError.Visible = true;
            //lblError.Text = "Contact Admin";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Please Contact Admin....!');", true);
            //System.Windows.Forms.MessageBox.Show("Contact Admin", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
        }

    }

    protected void btnedit_Click(object sender, EventArgs e)
    {
        PanelEmployeeSearch.Visible = true;
        txtexistiongno.Enabled = false;
        ddemployeetype.Enabled = false;
        ddlContract.Enabled = false;
        Employee_EmpNo_load();
    }

    protected void txtempNo_TextChanged(object sender, EventArgs e)
    {
        if (textSearch == false)
        {
            Clear();
            TxtExistSearch.Text = "";
        }
    }

    public void close()
    {
    }
    public void Clear()
    {
        txtbio.Text = "";
        panelError.Visible = false;
        //btnok.Visible = false;
        btncancel.Enabled = true;
        lbloutput.Text = "";
        //btnsearch.Enabled = true;
        btnsave.Enabled = true;
        btnedit.Enabled = true;
        btnreset.Enabled = true;
        panelsuccess.Visible = false;
        panelError.Visible = false;
        txtexistiongno.Text = "";
        txtempname.Text = "";
        txtempfname.Text = "";
        rbtngender.SelectedValue = "1";
        txtdob.Text = "";
        txtPSAdd1.Text = "";
        txtPSAdd2.Text = "";
        ddlPSDistrict.SelectedValue = "0";
        DataTable dt = new DataTable();
        ddlPSTaluk.DataSource = dt;
        ddlPSTaluk.DataBind();
        txtPhone1.Text = "";
        txtMobileCode.Text = "+91";
        txteducation.Text = "";
        txtscholl.Text = "";
        txtphone.Text = "";
        txtmobile.Text = "";
        txtDistrict.Text = "";
        txtTaluk.Text = "";
        txtppno.Text = "";
        txtdrivingno.Text = "";
        ddlqualification.SelectedValue = "0";
        txtuniversity.Text = "";

        ddldepartment.SelectedValue = "0";
        txtdesignation.Text = "";
        //    rbtnshift.SelectedValue = "0";
        ddemployeetype.SelectedValue = "0";

        // rbtngrade.SelectedValue = "1";
        //txtunedudept.Text = "";
        //txtunedudesg.Text = "";
        txtpsstate.Text = "TamilNadu";
        chkboxnonpfgrade.Checked = false;
        lblError.Text = "";
        panelcontract.Visible = false;
        Rbmartial.SelectedIndex = -1;
        Rbfather.SelectedIndex = -1;
        rbtncontratctype.SelectedIndex = -1;
        rbtneducateduneducated.SelectedIndex = -1;
        rbtngender.SelectedIndex = -1;

    }
    #region Web Form Designer generated code
    override protected void OnInit(EventArgs e)
    {

        rm = (System.Resources.ResourceManager)Application["rm"];
        Utility.SetThreadCulture();
        if (((SSSCultureInfo)System.Threading.Thread.CurrentThread.CurrentCulture).Encoding != null)
            Response.ContentEncoding = System.Text.Encoding.GetEncoding(((SSSCultureInfo)System.Threading.Thread.CurrentThread.CurrentCulture).Encoding);
        PayrollRegisterContentMeta = "text/html; charset=" + ((SSSCultureInfo)System.Threading.Thread.CurrentThread.CurrentCulture).Encoding;
        if (Application[Request.PhysicalPath] == null)
            Application.Add(Request.PhysicalPath, Response.ContentEncoding.WebName);
        InitializeComponent();
        this.Load += new System.EventHandler(this.Page_Load);
        // this.PreRender += new System.EventHandler(this.Page_PreRender);
        base.OnInit(e);
        if (!DBUtility.AuthorizeUser(new string[] { "1", "2", "6" }))

            Response.Redirect(Settings.AccessDeniedPage);
    }


    private void InitializeComponent()
    {
    }


    #endregion
    public void messagebox(string msg)
    {
        Label lbl = new Label();
        lbl.Text = "<script language='javascript'>" + Environment.NewLine + "window.alert(" + msg + ")</script>";
        System.Web.UI.Page page = new Page();
        page.Controls.Add(lbl);

    }
    protected void btnsearch_Click(object sender, EventArgs e)
    {
        bool errFlag = false;
        //panelError.Visible = true;
        if ((txtempNo.Text == "") && (TxtExistSearch.Text == ""))
        {
            
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Enter the Employee No or Existing No...!');", true);
            
            errFlag = true;
        }
        else if (ddlcategory.SelectedValue == "0")
        {
            
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Please Contact Admin....!');", true);
            //System.Windows.Forms.MessageBox.Show("Contact Admin", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            errFlag = true;
        }
        if (!errFlag)
        {
            panelError.Visible = false;
            DataTable dted = new DataTable();
            if (ddlcategory.SelectedValue == "1")
            {
                stafflabour_temp = "S";
            }
            else if (ddlcategory.SelectedValue == "2")
            {
                stafflabour_temp = "L";
            }
            if (txtempNo.Text != "")
            {
                dted = objdata.FillEmployeeRegistration(txtempNo.Text, stafflabour_temp, SessionCcode, SessionLcode);
            }
            else if (TxtExistSearch.Text != "")
            {
                dted = objdata.FillEmployeeRegistration_Existno(TxtExistSearch.Text, stafflabour_temp, SessionCcode, SessionLcode);
            }
            if (dted.Rows.Count > 0)
            {
                if (sessionAdmin == "1")
                {
                    textSearch = true;
                    PanelEducated.Visible = true;
                    txtempNo.Text = dted.Rows[0]["EmpNo"].ToString();
                    TxtExistSearch.Text = dted.Rows[0]["ExisistingCode"].ToString();
                    txtempname.Text = dted.Rows[0]["EmpName"].ToString();
                    txtempfname.Text = dted.Rows[0]["FatherName"].ToString();
                    rbtngender.Text = dted.Rows[0]["Gender"].ToString();
                    txtdob.Text = dted.Rows[0]["DOB"].ToString();

                    txtPSAdd1.Text = dted.Rows[0]["PSAdd1"].ToString();
                    txtPSAdd2.Text = dted.Rows[0]["PSAdd2"].ToString();
                    //ddlPSDistrict.SelectedValue = dted.Rows[0]["PSDistrict"].ToString();
                    txtDistrict.Text = dted.Rows[0]["PSDistrict"].ToString();
                    txtTaluk.Text = dted.Rows[0]["PSTaluk"].ToString();
                    txtpsstate.Text = dted.Rows[0]["PSState"].ToString();
                    string[] phone_split = dted.Rows[0]["Phone"].ToString().Split('-');
                    txtphone.Text = phone_split[0].ToString();
                    txtPhone1.Text = phone_split[1].ToString();
                    //txtphone.Text = dted.Rows[0]["Phone"].ToString();
                    string[] Mobile_split = dted.Rows[0]["Mobile"].ToString().Split('-');
                    txtMobileCode.Text = Mobile_split[0].ToString();
                    txtmobile.Text = Mobile_split[1].ToString();
                    //txtmobile.Text = dted.Rows[0]["Mobile"].ToString();
                    txtppno.Text = dted.Rows[0]["PassportNo"].ToString();
                    txtdrivingno.Text = dted.Rows[0]["DrivingLicenceNo"].ToString();
                    ddlqualification.SelectedValue = dted.Rows[0]["Qualification"].ToString();
                    txtscholl.Text = dted.Rows[0]["SchoolCollege"].ToString();
                    txtuniversity.Text = dted.Rows[0]["University"].ToString();
                    ddldepartment.SelectedValue = dted.Rows[0]["Department"].ToString();
                    txtdesignation.Text = dted.Rows[0]["Designation"].ToString();
                    //txtunedudept.Text = dted.Rows[0]["UnEduDept"].ToString();
                    //txtunedudesg.Text = dted.Rows[0]["UnEduDesig"].ToString();
                    ddemployeetype.SelectedValue = dted.Rows[0]["EmployeeType"].ToString();
                    txtexistiongno.Text = dted.Rows[0]["ExisistingCode"].ToString();
                    TxtExistSearch.Text = dted.Rows[0]["ExisistingCode"].ToString();
                    txtbio.Text = dted.Rows[0]["BiometricID"].ToString();
                    //string TalukCd = dted.Rows[0]["PSTaluk"].ToString();
                    //string Districtcd = dted.Rows[0]["PSDistrict"].ToString();
                    //int intTaluckCd = Convert.ToInt32(TalukCd);
                    //int intDistrict = Convert.ToInt32(Districtcd);
                    //ddlPSTaluk.SelectedValue = TalukCd;
                    //DataTable dtTlDis = new DataTable();
                    //dtTlDis = objdata.DropDownPSTaluk_ForEdit(intDistrict, intTaluckCd);
                    //ddlPSTaluk.DataSource = dtTlDis;
                    //ddlPSTaluk.DataTextField = "TalukNm";
                    //ddlPSTaluk.DataValueField = "TalukCd";

                    if (dted.Rows[0]["InsOuts"].ToString() == "INSIDER".ToString()) { chkInsOuts.Checked = true; }
                    else { chkInsOuts.Checked = false; }

                    if (dted.Rows[0]["MartialStatus"].ToString() == "U")
                    {
                        Rbmartial.SelectedValue = "U";
                    }
                    
                    else if (dted.Rows[0]["MartialStatus"].ToString() == "M")
                    {
                        Rbmartial.SelectedValue = "M";
                    }
                    if (dted.Rows[0]["EmployeeType"].ToString() == "3")
                    {
                        panelcontract.Visible = true;
                        ddlContract.SelectedValue = dted.Rows[0]["ContractType"].ToString();
                    }
                    else
                    {
                    }
                    if (dted.Rows[0]["Initial"].ToString() == "S")
                    {
                        Rbfather.SelectedValue = "S";
                    }
                    else if (dted.Rows[0]["Initial"].ToString() == "F")
                    {
                        Rbfather.SelectedValue = "F";
                    }
                    if (dted.Rows[0]["NonPFGrade"].ToString() == "1")
                    {
                        chkboxnonpfgrade.Checked = true;
                    }
                    else
                    {
                        chkboxnonpfgrade.Checked = false;
                    }
                    ddlPSTaluk.DataBind();

                    if (dted.Rows[0]["StafforLabor"].ToString() == "S")
                    {
                        ddlcategory.SelectedValue = "1";
                        PanelEducated.Visible = true;
                        Paneluneducated.Visible = false;
                        PanelSelectEducated.Visible = false;

                    }
                    else
                    {
                        ddlcategory.SelectedValue = "2";
                        PanelEducated.Visible = false;
                        Paneluneducated.Visible = false;
                        PanelSelectEducated.Visible = true;
                    }
                    if (dted.Rows[0]["Uneducated"].ToString() == "1")
                    {
                        rbtneducateduneducated.SelectedValue = "1";
                        PanelEducated.Visible = true;
                    }
                    else
                    {
                        rbtneducateduneducated.SelectedValue = "2";
                        PanelEducated.Visible = false;
                    }
                }
                else if (sessionAdmin == "2")
                {
                    if (dted.Rows[0]["NonPFGrade"].ToString() == "1")
                    {
                        //panelError.Visible = true;
                        //lblError.Text = "Contact Admin";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Contact Admin....!');", true);
                        //System.Windows.Forms.MessageBox.Show("Contact Admin", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    }
                    else if (dted.Rows[0]["NonPFGrade"].ToString() == "2")
                    {
                        panelError.Visible = false;
                        textSearch = true;
                        txtempNo.Text = dted.Rows[0]["EmpNo"].ToString();
                        TxtExistSearch.Text = dted.Rows[0]["ExisistingCode"].ToString();
                        txtempname.Text = dted.Rows[0]["EmpName"].ToString();
                        txtempfname.Text = dted.Rows[0]["FatherName"].ToString();
                        rbtngender.Text = dted.Rows[0]["Gender"].ToString();
                        txtdob.Text = dted.Rows[0]["DOB"].ToString();

                        txtPSAdd1.Text = dted.Rows[0]["PSAdd1"].ToString();
                        txtPSAdd2.Text = dted.Rows[0]["PSAdd2"].ToString();
                        txtDistrict.Text = dted.Rows[0]["PSDistrict"].ToString();
                        txtTaluk.Text = dted.Rows[0]["PSTaluk"].ToString();
                        txtpsstate.Text = dted.Rows[0]["PSState"].ToString();
                        string[] phone_split = dted.Rows[0]["Phone"].ToString().Split('-');
                        txtphone.Text = phone_split[0].ToString();
                        txtPhone1.Text = phone_split[1].ToString();
                        //txtphone.Text = dted.Rows[0]["Phone"].ToString();
                        string[] Mobile_split = dted.Rows[0]["Mobile"].ToString().Split('-');
                        txtMobileCode.Text = Mobile_split[0].ToString();
                        txtmobile.Text = Mobile_split[1].ToString();
                        //txtmobile.Text = dted.Rows[0]["Mobile"].ToString();
                        txtppno.Text = dted.Rows[0]["PassportNo"].ToString();
                        txtdrivingno.Text = dted.Rows[0]["DrivingLicenceNo"].ToString();
                        ddlqualification.SelectedValue = dted.Rows[0]["Qualification"].ToString();
                        txtscholl.Text = dted.Rows[0]["SchoolCollege"].ToString();
                        txtuniversity.Text = dted.Rows[0]["University"].ToString();
                        ddldepartment.SelectedValue = dted.Rows[0]["Department"].ToString();
                        txtdesignation.Text = dted.Rows[0]["Designation"].ToString();
                        //txtunedudept.Text = dted.Rows[0]["UnEduDept"].ToString();
                        //txtunedudesg.Text = dted.Rows[0]["UnEduDesig"].ToString();
                        ddemployeetype.SelectedValue = dted.Rows[0]["EmployeeType"].ToString();
                        txtexistiongno.Text = dted.Rows[0]["ExisistingCode"].ToString();
                        TxtExistSearch.Text = dted.Rows[0]["ExisistingCode"].ToString();
                        txtbio.Text = dted.Rows[0]["BiometricID"].ToString();
                        //string TalukCd = dted.Rows[0]["PSTaluk"].ToString();
                        //string Districtcd = dted.Rows[0]["PSDistrict"].ToString();
                        //int intTaluckCd = Convert.ToInt32(TalukCd);
                        //int intDistrict = Convert.ToInt32(Districtcd);
                        //ddlPSTaluk.SelectedValue = TalukCd;
                        //DataTable dtTlDis = new DataTable();
                        //dtTlDis = objdata.DropDownPSTaluk_ForEdit(intDistrict, intTaluckCd);
                        //ddlPSTaluk.DataSource = dtTlDis;
                        //ddlPSTaluk.DataTextField = "TalukNm";
                        //ddlPSTaluk.DataValueField = "TalukCd";
                        if (dted.Rows[0]["InsOuts"].ToString() == "INSIDER") { chkInsOuts.Checked = true; }
                        else { chkInsOuts.Checked = false; }

                        if (dted.Rows[0]["NonPFGrade"].ToString() == "1")
                        {
                            chkboxnonpfgrade.Checked = true;
                        }
                        else
                        {
                            chkboxnonpfgrade.Checked = false;
                        }
                        if (dted.Rows[0]["MartialStatus"].ToString() == "U")
                        {
                            Rbmartial.SelectedValue = "U";
                        }
                        else
                        {
                            Rbmartial.SelectedValue = "M";
                        }
                        if (dted.Rows[0][""].ToString() == "M")
                        {
                            Rbfather.SelectedValue = "M";
                        }
                        else
                        {
                            Rbfather.SelectedValue = "F";
                        }
                        if (dted.Rows[0]["Uneducated"].ToString() == "1")
                        {
                            rbtneducateduneducated.SelectedValue = "1";
                            PanelEducated.Visible = true;
                        }
                        else
                        {
                            rbtneducateduneducated.SelectedValue = "2";
                            PanelEducated.Visible = false;
                        }
                        ddlPSTaluk.DataBind();

                        if (dted.Rows[0]["StafforLabor"].ToString() == "S")
                        {
                            ddlcategory.SelectedValue = "1";
                            PanelEducated.Visible = true;
                            Paneluneducated.Visible = false;
                            PanelSelectEducated.Visible = false;

                        }
                        else
                        {
                            ddlcategory.SelectedValue = "2";
                            PanelEducated.Visible = false;
                            Paneluneducated.Visible = false;
                            PanelSelectEducated.Visible = true;
                        }
                    }
                }
            }
            else
            {
                //panelError.Visible = true;
                //lblError.Text = "Employee Not Found";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Employee Number Not Found....!');", true);
                //System.Windows.Forms.MessageBox.Show("Employee Not Found", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            }

        }
        textSearch = false;
    }

    protected void txtphone_TextChanged(object sender, EventArgs e)
    {
        if (txtphone.Text.Length == 5)
        {
            txtPhone1.Focus();
        }
    }

    protected void txtMobileCode_TextChanged(object sender, EventArgs e)
    {
        if (txtMobileCode.Text.Length == 3)
        {
            txtmobile.Focus();
        }
    }

    protected void btnreset_Click(object sender, EventArgs e)
    {
        Clear();
    }

    protected void chkboxnonpfgrade_CheckedChanged(object sender, EventArgs e)
    {
        if (chkboxnonpfgrade.Checked == true)
        {
            NonPFGrade = "1";
        }
        else
        {
            NonPFGrade = "2";
        }
    }

    protected void TxtExistSearch_TextChanged(object sender, EventArgs e)
    {
        if (textSearch == false)
        {
            Clear();
            //ddlEmpNo.SelectedValue = "";
        }
    }

    //-----------------------------------------------------------------------------------------------------------
    protected void rbtngender_SelectedIndexChanged(object sender, EventArgs e)
    {

    }





    protected void btnok_Click(object sender, EventArgs e)
    {
        Clear();

    }
    protected void ddlEmpNo_SelectedIndexChanged(object sender, EventArgs e)
    {
        //bool errFlag = false;
        ////panelError.Visible = true;
        //if ((ddlEmpNo.SelectedValue == "") && (ddlExist.SelectedValue == ""))
        //{

        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Enter the Employee No or Existing No...!');", true);

        //    errFlag = true;
        //}
        //else if (ddlcategory.SelectedValue == "0")
        //{

        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Please Contact Admin....!');", true);
        //    //System.Windows.Forms.MessageBox.Show("Contact Admin", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
        //    errFlag = true;
        //}
        //if (!errFlag)
        //{
        //    panelError.Visible = false;
        //    DataTable dted = new DataTable();
        //    if (ddlcategory.SelectedValue == "1")
        //    {
        //        stafflabour_temp = "S";
        //    }
        //    else if (ddlcategory.SelectedValue == "2")
        //    {
        //        stafflabour_temp = "L";
        //    }
        //    //if (ddlEmpNo.SelectedValue != "")
        //    //{
        //    //    dted = objdata.FillEmployeeRegistration(ddlEmpNo.SelectedValue, stafflabour_temp, SessionCcode, SessionLcode);
        //    //}
        //    //else if (ddlExist.SelectedValue != "")
        //    //{
        //    //    dted = objdata.FillEmployeeRegistration_Existno(ddlExist.SelectedValue, stafflabour_temp, SessionCcode, SessionLcode);
        //    //}
        //    dted = objdata.FillEmployeeRegistration(ddlEmpNo.SelectedValue, stafflabour_temp, SessionCcode, SessionLcode);
        //    if (dted.Rows.Count > 0)
        //    {
        //        if (sessionAdmin == "1")
        //        {
        //            textSearch = true;
        //            ddlEmpNo.SelectedValue = dted.Rows[0]["EmpNo"].ToString();
        //            ddlExist.SelectedValue = dted.Rows[0]["ExisistingCode"].ToString();
        //            txtempname.Text = dted.Rows[0]["EmpName"].ToString();
        //            txtempfname.Text = dted.Rows[0]["FatherName"].ToString();
        //            rbtngender.Text = dted.Rows[0]["Gender"].ToString();
        //            txtdob.Text = dted.Rows[0]["DOB"].ToString();

        //            txtPSAdd1.Text = dted.Rows[0]["PSAdd1"].ToString();
        //            txtPSAdd2.Text = dted.Rows[0]["PSAdd2"].ToString();
        //            ddlPSDistrict.SelectedValue = dted.Rows[0]["PSDistrict"].ToString();
        //            txtpsstate.Text = dted.Rows[0]["PSState"].ToString();
        //            string[] phone_split = dted.Rows[0]["Phone"].ToString().Split('-');
        //            txtphone.Text = phone_split[0].ToString();
        //            txtPhone1.Text = phone_split[1].ToString();
        //            //txtphone.Text = dted.Rows[0]["Phone"].ToString();
        //            string[] Mobile_split = dted.Rows[0]["Mobile"].ToString().Split('-');
        //            txtMobileCode.Text = Mobile_split[0].ToString();
        //            txtmobile.Text = Mobile_split[1].ToString();
        //            //txtmobile.Text = dted.Rows[0]["Mobile"].ToString();
        //            txtppno.Text = dted.Rows[0]["PassportNo"].ToString();
        //            txtdrivingno.Text = dted.Rows[0]["DrivingLicenceNo"].ToString();
        //            ddlqualification.SelectedValue = dted.Rows[0]["Qualification"].ToString();
        //            txtscholl.Text = dted.Rows[0]["SchoolCollege"].ToString();
        //            txtuniversity.Text = dted.Rows[0]["University"].ToString();
        //            ddldepartment.SelectedValue = dted.Rows[0]["Department"].ToString();
        //            txtdesignation.Text = dted.Rows[0]["Designation"].ToString();
        //            //txtunedudept.Text = dted.Rows[0]["UnEduDept"].ToString();
        //            //txtunedudesg.Text = dted.Rows[0]["UnEduDesig"].ToString();
        //            ddemployeetype.SelectedValue = dted.Rows[0]["EmployeeType"].ToString();
        //            txtexistiongno.Text = dted.Rows[0]["ExisistingCode"].ToString();
        //            ddlExist.SelectedValue = dted.Rows[0]["ExisistingCode"].ToString();
        //            string TalukCd = dted.Rows[0]["PSTaluk"].ToString();
        //            string Districtcd = dted.Rows[0]["PSDistrict"].ToString();
        //            int intTaluckCd = Convert.ToInt32(TalukCd);
        //            int intDistrict = Convert.ToInt32(Districtcd);
        //            //ddlPSTaluk.SelectedValue = TalukCd;
        //            DataTable dtTlDis = new DataTable();
        //            dtTlDis = objdata.DropDownPSTaluk_ForEdit(intDistrict, intTaluckCd);
        //            ddlPSTaluk.DataSource = dtTlDis;
        //            ddlPSTaluk.DataTextField = "TalukNm";
        //            ddlPSTaluk.DataValueField = "TalukCd";
        //            if (dted.Rows[0]["NonPFGrade"].ToString() == "1")
        //            {
        //                chkboxnonpfgrade.Checked = true;
        //            }
        //            else
        //            {
        //                chkboxnonpfgrade.Checked = false;
        //            }
        //            ddlPSTaluk.DataBind();

        //            if (dted.Rows[0]["StafforLabor"].ToString() == "S")
        //            {
        //                ddlcategory.SelectedValue = "1";
        //                PanelEducated.Visible = true;
        //                Paneluneducated.Visible = false;
        //                PanelSelectEducated.Visible = false;

        //            }
        //            else
        //            {
        //                ddlcategory.SelectedValue = "2";
        //                PanelEducated.Visible = false;
        //                Paneluneducated.Visible = false;
        //                PanelSelectEducated.Visible = true;
        //            }
        //            if (dted.Rows[0]["Uneducated"].ToString() == "1")
        //            {
        //                rbtneducateduneducated.SelectedValue = "1";
        //                PanelEducated.Visible = true;
        //            }
        //            else
        //            {
        //                rbtneducateduneducated.SelectedValue = "2";
        //                PanelEducated.Visible = false;
        //            }
        //        }
        //        else if (sessionAdmin == "2")
        //        {
        //            if (dted.Rows[0]["NonPFGrade"].ToString() == "1")
        //            {
        //                //panelError.Visible = true;
        //                //lblError.Text = "Contact Admin";
        //                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Contact Admin....!');", true);
        //                //System.Windows.Forms.MessageBox.Show("Contact Admin", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
        //            }
        //            else if (dted.Rows[0]["NonPFGrade"].ToString() == "2")
        //            {
        //                panelError.Visible = false;
        //                textSearch = true;
        //                ddlEmpNo.SelectedValue = dted.Rows[0]["EmpNo"].ToString();
        //                ddlExist.SelectedValue = dted.Rows[0]["ExisistingCode"].ToString();
        //                txtempname.Text = dted.Rows[0]["EmpName"].ToString();
        //                txtempfname.Text = dted.Rows[0]["FatherName"].ToString();
        //                rbtngender.Text = dted.Rows[0]["Gender"].ToString();
        //                txtdob.Text = dted.Rows[0]["DOB"].ToString();

        //                txtPSAdd1.Text = dted.Rows[0]["PSAdd1"].ToString();
        //                txtPSAdd2.Text = dted.Rows[0]["PSAdd2"].ToString();
        //                ddlPSDistrict.SelectedValue = dted.Rows[0]["PSDistrict"].ToString();
        //                txtpsstate.Text = dted.Rows[0]["PSState"].ToString();
        //                string[] phone_split = dted.Rows[0]["Phone"].ToString().Split('-');
        //                txtphone.Text = phone_split[0].ToString();
        //                txtPhone1.Text = phone_split[1].ToString();
        //                //txtphone.Text = dted.Rows[0]["Phone"].ToString();
        //                string[] Mobile_split = dted.Rows[0]["Mobile"].ToString().Split('-');
        //                txtMobileCode.Text = Mobile_split[0].ToString();
        //                txtmobile.Text = Mobile_split[1].ToString();
        //                //txtmobile.Text = dted.Rows[0]["Mobile"].ToString();
        //                txtppno.Text = dted.Rows[0]["PassportNo"].ToString();
        //                txtdrivingno.Text = dted.Rows[0]["DrivingLicenceNo"].ToString();
        //                ddlqualification.SelectedValue = dted.Rows[0]["Qualification"].ToString();
        //                txtscholl.Text = dted.Rows[0]["SchoolCollege"].ToString();
        //                txtuniversity.Text = dted.Rows[0]["University"].ToString();
        //                ddldepartment.SelectedValue = dted.Rows[0]["Department"].ToString();
        //                txtdesignation.Text = dted.Rows[0]["Designation"].ToString();
        //                //txtunedudept.Text = dted.Rows[0]["UnEduDept"].ToString();
        //                //txtunedudesg.Text = dted.Rows[0]["UnEduDesig"].ToString();
        //                ddemployeetype.SelectedValue = dted.Rows[0]["EmployeeType"].ToString();
        //                txtexistiongno.Text = dted.Rows[0]["ExisistingCode"].ToString();
        //                ddlExist.SelectedValue = dted.Rows[0]["ExisistingCode"].ToString();
        //                string TalukCd = dted.Rows[0]["PSTaluk"].ToString();
        //                string Districtcd = dted.Rows[0]["PSDistrict"].ToString();
        //                int intTaluckCd = Convert.ToInt32(TalukCd);
        //                int intDistrict = Convert.ToInt32(Districtcd);
        //                //ddlPSTaluk.SelectedValue = TalukCd;
        //                DataTable dtTlDis = new DataTable();
        //                dtTlDis = objdata.DropDownPSTaluk_ForEdit(intDistrict, intTaluckCd);
        //                ddlPSTaluk.DataSource = dtTlDis;
        //                ddlPSTaluk.DataTextField = "TalukNm";
        //                ddlPSTaluk.DataValueField = "TalukCd";
        //                if (dted.Rows[0]["NonPFGrade"].ToString() == "1")
        //                {
        //                    chkboxnonpfgrade.Checked = true;
        //                }
        //                else
        //                {
        //                    chkboxnonpfgrade.Checked = false;
        //                }
        //                if (dted.Rows[0]["Uneducated"].ToString() == "1")
        //                {
        //                    rbtneducateduneducated.SelectedValue = "1";
        //                    PanelEducated.Visible = true;
        //                }
        //                else
        //                {
        //                    rbtneducateduneducated.SelectedValue = "2";
        //                    PanelEducated.Visible = false;
        //                }
        //                ddlPSTaluk.DataBind();

        //                if (dted.Rows[0]["StafforLabor"].ToString() == "S")
        //                {
        //                    ddlcategory.SelectedValue = "1";
        //                    PanelEducated.Visible = true;
        //                    Paneluneducated.Visible = false;
        //                    PanelSelectEducated.Visible = false;

        //                }
        //                else
        //                {
        //                    ddlcategory.SelectedValue = "2";
        //                    PanelEducated.Visible = false;
        //                    Paneluneducated.Visible = false;
        //                    PanelSelectEducated.Visible = true;
        //                }
        //            }
        //        }
        //    }
        //    else
        //    {
        //        //panelError.Visible = true;
        //        //lblError.Text = "Employee Not Found";
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Employee Number Not Found....!');", true);
        //        //System.Windows.Forms.MessageBox.Show("Employee Not Found", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
        //    }

        //}
        //textSearch = false;
    }
    protected void ddlExist_SelectedIndexChanged(object sender, EventArgs e)
    {
        //bool errFlag = false;
        ////panelError.Visible = true;
        //if ((ddlEmpNo.SelectedValue == "") && (ddlExist.SelectedValue == ""))
        //{

        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Enter the Employee No or Existing No...!');", true);

        //    errFlag = true;
        //}
        //else if (ddlcategory.SelectedValue == "0")
        //{

        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Please Contact Admin....!');", true);
        //    //System.Windows.Forms.MessageBox.Show("Contact Admin", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
        //    errFlag = true;
        //}
        //if (!errFlag)
        //{
        //    panelError.Visible = false;
        //    DataTable dted = new DataTable();
        //    if (ddlcategory.SelectedValue == "1")
        //    {
        //        stafflabour_temp = "S";
        //    }
        //    else if (ddlcategory.SelectedValue == "2")
        //    {
        //        stafflabour_temp = "L";
        //    }
        //    //if (ddlEmpNo.SelectedValue != "")
        //    //{
        //    //    dted = objdata.FillEmployeeRegistration(ddlEmpNo.SelectedValue, stafflabour_temp, SessionCcode, SessionLcode);
        //    //}
        //    //else if (ddlExist.SelectedValue != "")
        //    //{
        //    //    dted = objdata.FillEmployeeRegistration_Existno(ddlExist.SelectedValue, stafflabour_temp, SessionCcode, SessionLcode);
        //    //}
        //    dted = objdata.FillEmployeeRegistration_Existno(ddlExist.SelectedValue, stafflabour_temp, SessionCcode, SessionLcode);
        //    if (dted.Rows.Count > 0)
        //    {
        //        if (sessionAdmin == "1")
        //        {
        //            textSearch = true;
        //            ddlEmpNo.SelectedValue = dted.Rows[0]["EmpNo"].ToString();
        //            ddlExist.SelectedValue = dted.Rows[0]["ExisistingCode"].ToString();
        //            txtempname.Text = dted.Rows[0]["EmpName"].ToString();
        //            txtempfname.Text = dted.Rows[0]["FatherName"].ToString();
        //            rbtngender.Text = dted.Rows[0]["Gender"].ToString();
        //            txtdob.Text = dted.Rows[0]["DOB"].ToString();

        //            txtPSAdd1.Text = dted.Rows[0]["PSAdd1"].ToString();
        //            txtPSAdd2.Text = dted.Rows[0]["PSAdd2"].ToString();
        //            ddlPSDistrict.SelectedValue = dted.Rows[0]["PSDistrict"].ToString();
        //            txtpsstate.Text = dted.Rows[0]["PSState"].ToString();
        //            string[] phone_split = dted.Rows[0]["Phone"].ToString().Split('-');
        //            txtphone.Text = phone_split[0].ToString();
        //            txtPhone1.Text = phone_split[1].ToString();
        //            //txtphone.Text = dted.Rows[0]["Phone"].ToString();
        //            string[] Mobile_split = dted.Rows[0]["Mobile"].ToString().Split('-');
        //            txtMobileCode.Text = Mobile_split[0].ToString();
        //            txtmobile.Text = Mobile_split[1].ToString();
        //            //txtmobile.Text = dted.Rows[0]["Mobile"].ToString();
        //            txtppno.Text = dted.Rows[0]["PassportNo"].ToString();
        //            txtdrivingno.Text = dted.Rows[0]["DrivingLicenceNo"].ToString();
        //            ddlqualification.SelectedValue = dted.Rows[0]["Qualification"].ToString();
        //            txtscholl.Text = dted.Rows[0]["SchoolCollege"].ToString();
        //            txtuniversity.Text = dted.Rows[0]["University"].ToString();
        //            ddldepartment.SelectedValue = dted.Rows[0]["Department"].ToString();
        //            txtdesignation.Text = dted.Rows[0]["Designation"].ToString();
        //            //txtunedudept.Text = dted.Rows[0]["UnEduDept"].ToString();
        //            //txtunedudesg.Text = dted.Rows[0]["UnEduDesig"].ToString();
        //            ddemployeetype.SelectedValue = dted.Rows[0]["EmployeeType"].ToString();
        //            txtexistiongno.Text = dted.Rows[0]["ExisistingCode"].ToString();
        //            ddlExist.SelectedValue = dted.Rows[0]["ExisistingCode"].ToString();
        //            string TalukCd = dted.Rows[0]["PSTaluk"].ToString();
        //            string Districtcd = dted.Rows[0]["PSDistrict"].ToString();
        //            int intTaluckCd = Convert.ToInt32(TalukCd);
        //            int intDistrict = Convert.ToInt32(Districtcd);
        //            //ddlPSTaluk.SelectedValue = TalukCd;
        //            DataTable dtTlDis = new DataTable();
        //            dtTlDis = objdata.DropDownPSTaluk_ForEdit(intDistrict, intTaluckCd);
        //            ddlPSTaluk.DataSource = dtTlDis;
        //            ddlPSTaluk.DataTextField = "TalukNm";
        //            ddlPSTaluk.DataValueField = "TalukCd";
        //            if (dted.Rows[0]["NonPFGrade"].ToString() == "1")
        //            {
        //                chkboxnonpfgrade.Checked = true;
        //            }
        //            else
        //            {
        //                chkboxnonpfgrade.Checked = false;
        //            }
        //            ddlPSTaluk.DataBind();

        //            if (dted.Rows[0]["StafforLabor"].ToString() == "S")
        //            {
        //                ddlcategory.SelectedValue = "1";
        //                PanelEducated.Visible = true;
        //                Paneluneducated.Visible = false;
        //                PanelSelectEducated.Visible = false;

        //            }
        //            else
        //            {
        //                ddlcategory.SelectedValue = "2";
        //                PanelEducated.Visible = false;
        //                Paneluneducated.Visible = false;
        //                PanelSelectEducated.Visible = true;
        //            }
        //            if (dted.Rows[0]["Uneducated"].ToString() == "1")
        //            {
        //                rbtneducateduneducated.SelectedValue = "1";
        //                PanelEducated.Visible = true;
        //            }
        //            else
        //            {
        //                rbtneducateduneducated.SelectedValue = "2";
        //                PanelEducated.Visible = false;
        //            }
        //        }
        //        else if (sessionAdmin == "2")
        //        {
        //            if (dted.Rows[0]["NonPFGrade"].ToString() == "1")
        //            {
        //                //panelError.Visible = true;
        //                //lblError.Text = "Contact Admin";
        //                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Contact Admin....!');", true);
        //                //System.Windows.Forms.MessageBox.Show("Contact Admin", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
        //            }
        //            else if (dted.Rows[0]["NonPFGrade"].ToString() == "2")
        //            {
        //                panelError.Visible = false;
        //                textSearch = true;
        //                ddlEmpNo.SelectedValue = dted.Rows[0]["EmpNo"].ToString();
        //                ddlExist.SelectedValue = dted.Rows[0]["ExisistingCode"].ToString();
        //                txtempname.Text = dted.Rows[0]["EmpName"].ToString();
        //                txtempfname.Text = dted.Rows[0]["FatherName"].ToString();
        //                rbtngender.Text = dted.Rows[0]["Gender"].ToString();
        //                txtdob.Text = dted.Rows[0]["DOB"].ToString();

        //                txtPSAdd1.Text = dted.Rows[0]["PSAdd1"].ToString();
        //                txtPSAdd2.Text = dted.Rows[0]["PSAdd2"].ToString();
        //                ddlPSDistrict.SelectedValue = dted.Rows[0]["PSDistrict"].ToString();
        //                txtpsstate.Text = dted.Rows[0]["PSState"].ToString();
        //                string[] phone_split = dted.Rows[0]["Phone"].ToString().Split('-');
        //                txtphone.Text = phone_split[0].ToString();
        //                txtPhone1.Text = phone_split[1].ToString();
        //                //txtphone.Text = dted.Rows[0]["Phone"].ToString();
        //                string[] Mobile_split = dted.Rows[0]["Mobile"].ToString().Split('-');
        //                txtMobileCode.Text = Mobile_split[0].ToString();
        //                txtmobile.Text = Mobile_split[1].ToString();
        //                //txtmobile.Text = dted.Rows[0]["Mobile"].ToString();
        //                txtppno.Text = dted.Rows[0]["PassportNo"].ToString();
        //                txtdrivingno.Text = dted.Rows[0]["DrivingLicenceNo"].ToString();
        //                ddlqualification.SelectedValue = dted.Rows[0]["Qualification"].ToString();
        //                txtscholl.Text = dted.Rows[0]["SchoolCollege"].ToString();
        //                txtuniversity.Text = dted.Rows[0]["University"].ToString();
        //                ddldepartment.SelectedValue = dted.Rows[0]["Department"].ToString();
        //                txtdesignation.Text = dted.Rows[0]["Designation"].ToString();
        //                //txtunedudept.Text = dted.Rows[0]["UnEduDept"].ToString();
        //                //txtunedudesg.Text = dted.Rows[0]["UnEduDesig"].ToString();
        //                ddemployeetype.SelectedValue = dted.Rows[0]["EmployeeType"].ToString();
        //                txtexistiongno.Text = dted.Rows[0]["ExisistingCode"].ToString();
        //                ddlExist.SelectedValue = dted.Rows[0]["ExisistingCode"].ToString();
        //                string TalukCd = dted.Rows[0]["PSTaluk"].ToString();
        //                string Districtcd = dted.Rows[0]["PSDistrict"].ToString();
        //                int intTaluckCd = Convert.ToInt32(TalukCd);
        //                int intDistrict = Convert.ToInt32(Districtcd);
        //                //ddlPSTaluk.SelectedValue = TalukCd;
        //                DataTable dtTlDis = new DataTable();
        //                dtTlDis = objdata.DropDownPSTaluk_ForEdit(intDistrict, intTaluckCd);
        //                ddlPSTaluk.DataSource = dtTlDis;
        //                ddlPSTaluk.DataTextField = "TalukNm";
        //                ddlPSTaluk.DataValueField = "TalukCd";
        //                if (dted.Rows[0]["NonPFGrade"].ToString() == "1")
        //                {
        //                    chkboxnonpfgrade.Checked = true;
        //                }
        //                else
        //                {
        //                    chkboxnonpfgrade.Checked = false;
        //                }
        //                if (dted.Rows[0]["Uneducated"].ToString() == "1")
        //                {
        //                    rbtneducateduneducated.SelectedValue = "1";
        //                    PanelEducated.Visible = true;
        //                }
        //                else
        //                {
        //                    rbtneducateduneducated.SelectedValue = "2";
        //                    PanelEducated.Visible = false;
        //                }
        //                ddlPSTaluk.DataBind();

        //                if (dted.Rows[0]["StafforLabor"].ToString() == "S")
        //                {
        //                    ddlcategory.SelectedValue = "1";
        //                    PanelEducated.Visible = true;
        //                    Paneluneducated.Visible = false;
        //                    PanelSelectEducated.Visible = false;

        //                }
        //                else
        //                {
        //                    ddlcategory.SelectedValue = "2";
        //                    PanelEducated.Visible = false;
        //                    Paneluneducated.Visible = false;
        //                    PanelSelectEducated.Visible = true;
        //                }
        //            }
        //        }
        //    }
        //    else
        //    {
        //        //panelError.Visible = true;
        //        //lblError.Text = "Employee Not Found";
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Employee Number Not Found....!');", true);
        //        //System.Windows.Forms.MessageBox.Show("Employee Not Found", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
        //    }

        //}
        //textSearch = false;
    }
    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }
    protected void btnDownLoad_Click(object sender, EventArgs e)
    {
        try
        {
            DataTable dt = new DataTable();
            dt = objdata.Employee_Download(SessionCcode, SessionLcode, sessionAdmin);
            gvdownload.DataSource = dt;
            gvdownload.DataBind();
            if (dt.Rows.Count > 0)
            {
                string attachment = "attachment;filename=Employee.xls";
                Response.ClearContent();
                Response.AddHeader("content-disposition", attachment);
                Response.ContentType = "application/ms-excel";

                StringWriter stw = new StringWriter();
                HtmlTextWriter htextw = new HtmlTextWriter(stw);
                gvdownload.RenderControl(htextw);
                //gvDownload.RenderControl(htextw);
                //Response.Write("Contract Details");
                Response.Write(stw.ToString());
                Response.End();
                Response.Clear();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Downloaded Successfully');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('No Data Found....!');", true);
            }
        }
        catch (Exception ex)
        {
        }
    }
    protected void btnEmp_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptEmpDownload.aspx");
    }
    protected void btnatt_Click(object sender, EventArgs e)
    {
        Response.Redirect("AttenanceDownload.aspx");
    }
    protected void btnLeave_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptLeaveSample.aspx");
    }
    protected void btnOT_Click(object sender, EventArgs e)
    {
        Response.Redirect("OTDownload.aspx");
    }
    protected void btncontract_Click(object sender, EventArgs e)
    {
        Response.Redirect("ContractRpt.aspx");
    }
    protected void ddlContract_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void ddemployeetype_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddemployeetype.SelectedValue == "3")
        {
            panelcontract.Visible = true;
        }
        else
        {
            panelcontract.Visible = false;
        }
    }
    protected void gvdownload_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    protected void btnSal_Click(object sender, EventArgs e)
    {
        Response.Redirect("FrmDeductionDownload.aspx");
    }
}
