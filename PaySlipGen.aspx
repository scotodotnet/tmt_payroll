﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PaySlipGen.aspx.cs" Inherits="PaySlip_gen1" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">


<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Payroll Management Systems</title>
    <link href="new_css/main.css" rel="stylesheet" type="text/css" />
    <link href="new_css/colors/dark_blue.css" rel="stylesheet" type="text/css" />
    <link href="new_css/superfish.css" rel="stylesheet" type="text/css" />

    <script src="new_js/jquery-1.4.2.min.js" type="text/javascript"></script>

    <script src="new_js/jquery.hoverintent.minified.js" type="text/javascript"></script>

    <script src="new_js/superfish.js" type="text/javascript"></script>

    <script src="new_js/jquery.tools.min.js" type="text/javascript"></script>

    <%--<script src="new_js/jquery.simplemodal.js" type="text/javascript"></script>--%>

    <script src="new_js/jquery.notifybar.js" type="text/javascript"></script>

    <script src="new_js/jquery.tipsy.js" type="text/javascript"></script>

    <script src="new_js/enhance.js" type="text/javascript"></script>

    <script src="new_js/excanvas.js" type="text/javascript"></script>

    <script src="new_js/visualize.jquery.js" type="text/javascript"></script>

    <script src="new_js/jquery.collapsible.js" type="text/javascript"></script>

    <script src="new_js/jquery.autosuggest.packed.js" type="text/javascript"></script>

    <script src="new_js/platinum-admin.js" type="text/javascript"></script>

    <style>
        .sf-navbar li ul
        {
            width: 1100px; /*IE6 soils itself without this*/
            margin: 0px auto;
            margin-left: -20em;
        }
        .text
        {
            height: 22px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <cc1:ToolkitScriptManager runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true"
        ID="ScriptManager1" EnablePartialRendering="true">
    </cc1:ToolkitScriptManager>
    <div id="header">
        <div id="header-top">
            <div id="logo">
                <h1>
                    &nbsp;</h1>
                <span id="slogan">&nbsp;</span>
            </div>
            <!-- end logo -->
            <div id="login-info">
                <img id="profile" alt="profile" src="./new_images/icons/user.png" />
                <p id="top">
                    <span id="name">
                        <asp:Label ID="lblusername" runat="server"></asp:Label></span><br />
                    <span id="links">
                        <%--<a id="message-link" href="#" title="click to see the messages!">
                        <span id="message-count">1</span> new message. </a>--%></span></p>
                <div>
                    <asp:Label ID="lblComany" runat="server" Text="" Font-Bold="true"></asp:Label>
                </div>
                <div id="messages-box">
                    <h4>
                        Messages</h4>
                    <h5>
                        <a id="new-message" href="#">New Message</a></h5>
                    <hr />
                    <ul id="messages">
                        <li class="new"><a href="#">
                            <img alt="user" src="./new_images/icons/sender.png" />
                            <span class="sender">John Doe</span> <span class="description">Hi this is just a description...</span>
                            <span class="date">20 minutes ago...</span> </a></li>
                        <li><a href="#">
                            <img alt="user" src="./new_images/icons/sender.png" />
                            <span class="sender">John Doe</span> <span class="description">Hi this is just a description...</span>
                            <span class="date">24 minutes ago...</span> </a></li>
                        <li><a href="#">
                            <img alt="user" src="./new_images/icons/sender.png" />
                            <span class="sender">John Doe</span> <span class="description">Hi this is just a description...</span>
                            <span class="date">50 minutes ago...</span> </a></li>
                        <li id="last"><a id="all-messages" href="#">See all messages! <span id="unreaded">8
                            unreaded.</span> </a></li>
                    </ul>
                    <br class="clear" />
                </div>
                <!-- end messages-box -->
                <a id="power" href="default.aspx" title="Logout">logout</a>
            </div>
            <!-- end login -->
            <div id="nav">
                <ul class="sf-menu sf-navbar">
                    <li><a id="dashboard" href="Dashboard.aspx">Dashboard</a>
                        <ul>
                            <li>&nbsp; </li>
                        </ul>
                    </li>
                    <li><a id="Employee" href="EmployeeRegistration.aspx">Employee</a>
                        <ul>
                            <li><a href="EmployeeRegistration.aspx">Employee</a> </li>
                            <li><a href="OfficalProfileDetails.aspx">Profile</a></li>
                            <li><a href="SalaryMaster.aspx">Salary Master</a></li>
                            <%--<li><a href="LeaveAllocationEmployee.aspx">Leave Allocation</a></li>--%>
                            <%--<li><a href="ApplyForLeave.aspx">Apply Leave</a></li>--%>
                            <li><a href="SalaryRegistration.aspx">Salary Details</a></li>
                            <li><a href="SalaryAdvance.aspx">Advance</a></li>
                            <li><a href="Settlement.aspx">Settlement</a></li>
                            <li><a href="EmployeeDeactiveMode.aspx">Employee Re-Activate</a></li>
                        </ul>
                    </li>
                    <li class="current"><a id="A1" href="RptDepartmentwiseEmployee.aspx">Reports</a>
                        <ul>
                            <li><a href="RptDepartmentwiseEmployee.aspx">Employee Details</a></li>
                            <%--<li><a href="RptAttendanceReport.aspx">Attendance Report</a></li>--%>
                            <%--<li><a href="RptProbationperiod.aspx">Probationary Period</a></li>
		                            <li><a href="ContractBreak.aspx">Contract Details</a></li>--%>
                            <li><a href="RptResign.aspx">Resignation Reports</a></li>
                            <li class="current"><a href="PaySlipGen.aspx">Payslip</a></li>
                            <%--<li><a href="RptSalary.aspx">Salary Details</a></li>--%>
                            <%--<li><a href="SalarySummary.aspx">Salary Summary</a></li>--%>
                            <%--<li><a href="Incentive.aspx">Incentive Details</a></li>--%>
                            <li><a href="RptDepartmentSalaryAbstract.aspx">Salary Abstract</a></li>
                            <li><a href="AttedanceSummary.aspx">Summary</a></li>
                        </ul>
                    </li>
                    <li><a id="OT" href="OverTime.aspx">OT</a>
                        <ul>
                            <li><a href="OverTime.aspx">OT</a></li>
                        </ul>
                    </li>
                    <li><a id="Master" href="MstEmployeeType.aspx">Masters</a>
                        <ul>
                            <li><a href="MstEmployeeType.aspx">Employee Type</a></li>
                            <li><a href="UserRegistration.aspx">User</a></li>
                            <li><a href="MSTPFESI.aspx">PF</a></li>
                            <li><a href="MstBank.aspx">Bank</a></li>
                            <li><a href="MstDepartment.aspx">Department</a></li>
                            <li><a href="MstDAfix.aspx">DA Arrears FIX</a></li>
                        </ul>
                    </li>
                    <li><a id="Bonus" href="BonusForAll.aspx">Bonus</a>
                        <ul>
                            <li><a href="BonusForAll.aspx">Bonus</a></li>
                            <li><a href="StandardBonus.aspx">Group Bonus</a></li>
                            <li><a href="RptBonus.aspx">Bonus Report</a></li>
                        </ul>
                    </li>
                    <li><a id="ESI_PF" href="PFDownload.aspx">ESI & PF</a>
                        <ul>
                            <%--<li><a href="PFForm5.aspx">PF Form 5</a></li>--%>
                            <li><a href="PFDownload.aspx">PF Download</a></li>
                            <li><a href="ESIDownload.aspx">ESI Download</a></li>
                            <li><a href="PFForm3Anew.aspx">PF Form 3A</a></li>
		                            <li><a href="PFform6A.aspx">PF Form 6A</a></li>
                            <%--<li><a href="FOrm10.aspx">Form 10</a></li>
		                            <li><a href="PFForm3Anew.aspx">PF Form 3A</a></li>
		                            <li><a href="PFform6A.aspx">PF Form 6A</a></li>
		                            <li><a href="ESIForm7.aspx">ESI Form 7</a></li>--%>
                        </ul>
                    </li>
                    <li><a id="Upload" href="UploadEmployee.aspx">Upload</a>
                        <ul>
                            <li><a href="UploadEmployee.aspx">Employee Upload</a></li>
                            <li><a href="AttenanceUpload.aspx">Attenance Upload</a></li>
                            <li><a href="UploadOT.aspx">OT Upload</a></li>
                            <%--<li><a href="LoadEmpmusters.aspx">Employee Master</a></li>--%>
                        </ul>
                    </li>
                </ul>
            </div>
            <!-- end div#nav -->
        </div>
        <!-- end div#header-top -->
        <div class="breadCrumb">
            <ul>
                <li class="first"><a href="#">Home</a> </li>
                <li><a href="#">Users</a> </li>
                <li><a href="#">Roles</a> </li>
                <li class="last">PaySlip </li>
            </ul>
        </div>
        <!-- end div#breadCrumb -->
        <%--<div id="search-box">
                    <form id="searchform" action="" method="get">
                        <fieldset class="search"><span>Search</span>
                        <input class="box" type="text" />
                        <button class="btn" title="Submit Search">Search</button></fieldset>
                    </form>
                </div>--%>
        <!-- end div#search-box -->
    </div>
    <!-- end header -->
    <div id="page-wrap">
        <div id="right-sidebar">
            <div class="innerdiv">
                <h2 class="head">
                    DownLoad Format</h2>
                <div class="innercontent clear">
                    <h4>
                        DownLoad Format</h4>
                    <ul id="comments" class="tooltip-enabled">
                    <asp:Button ID="btncontract" runat="server" Text="Contract Report" Width="200" 
                                    CssClass="button green" onclick="btncontract_Click" />
                        <asp:Button ID="btnEmp" runat="server" Text="Employee Download" Width="200" CssClass="button green"
                            OnClick="btnEmp_Click" />
                        <asp:Button ID="btnatt" runat="server" Text="Attenance Download" Width="200" CssClass="button green"
                            OnClick="btnatt_Click" />
                        <asp:Button ID="btnLeave" runat="server" Text="Leave Download" Width="200" CssClass="button green"
                            OnClick="btnLeave_Click" />
                        <asp:Button ID="btnOT" runat="server" Text="OT Download" Width="200" CssClass="button green"
                            OnClick="btnOT_Click" />
                    </ul>
                    <%--<img src="new_images/icons/right_img.jpg" alt="" width="230"/>--%>
                </div>
            </div>
            <div class="clear">
            </div>
        </div>
        <!-- end right-sidebar -->
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <Triggers>
                <asp:PostBackTrigger ControlID="btnExport" />
                <asp:PostBackTrigger ControlID="btnOther" />
            </Triggers>
            <ContentTemplate>
                <div id="main-content">
                    <br />
                    <br />
                    <br />
                    <br />
                    <div class="clear_body">
                        <div class="innerdiv">
                            <div class="innercontent">
                                <!-- tab "panes" -->
                                <div>
                                    <table class="full">
                                        <thead>
                                            <tr align="center">
                                                <th colspan="5" style="text-align: center;">
                                                    <h4>PAY SLIP REPORT</h4>
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr align="center">
                                                <td colspan="5">
                                                    <asp:Label ID="lblWagesType" runat="server" Text="Wages Type" Font-Bold="true" ></asp:Label>
                                                    <asp:RadioButtonList ID="rbsalary" runat="server" AutoPostBack="true" 
                                                        RepeatColumns="3" onselectedindexchanged="rbsalary_SelectedIndexChanged">
                                                        <asp:ListItem Text ="Weekly Wages" Value="1"></asp:ListItem>
                                                        <asp:ListItem Text="Monthly Wages" Value="2" ></asp:ListItem>                                                                                                                                       
                                                        <asp:ListItem Text="Bi-Monthly Wages" Value="3"></asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </td>
                                            </tr>
                                            <asp:Panel ID="pnlMonth" runat="server" Visible="false" >
                                                <tr>
                                                    <td><asp:Label ID="lblfrom" runat="server" Text="From Date" Font-Bold="true"></asp:Label></td>
                                                    <td>
                                                        <asp:TextBox ID="txtfrom" runat="server" CssClass="text" AutoPostBack="True" 
                                                            TabIndex="3" ontextchanged="txtfrom_TextChanged" ></asp:TextBox>
                                                        <cc1:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtfrom" Format ="dd-MM-yyyy" CssClass ="orange" Enabled="true"></cc1:CalendarExtender>
                                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR38" runat="server" FilterMode="ValidChars" FilterType="Numbers,Custom" TargetControlID="txtfrom" ValidChars="0123456789/- "></cc1:FilteredTextBoxExtender>
                                                    </td>
                                                    <td><asp:Label ID="lblToDate" runat="server" Text="To Date" Font-Bold="true"></asp:Label></td>
                                                    <td colspan="2">
                                                        <asp:TextBox ID="txtTo" runat="server" CssClass="text" AutoPostBack="True" 
                                                        TabIndex="3" ontextchanged="txtTo_TextChanged" ></asp:TextBox>
                                                        <cc1:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtTo" Format ="dd-MM-yyyy" CssClass ="orange" Enabled="true"></cc1:CalendarExtender>
                                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR39" runat="server" FilterMode="ValidChars" FilterType="Numbers,Custom" TargetControlID="txtTo" ValidChars="0123456789/- "></cc1:FilteredTextBoxExtender>
                                                    </td>
                                                </tr>
                                            </asp:Panel>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lblcategory" runat="server" Text="Category" Font-Bold="true"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlcategory" runat="server" AutoPostBack="true" Width="180"
                                                        Height="25" OnSelectedIndexChanged="ddlcategory_SelectedIndexChanged">
                                                    </asp:DropDownList>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblDeparmtent" runat="server" Text="Employee Type" Font-Bold="true"></asp:Label>
                                                </td>
                                                <td colspan="2">
                                                    <asp:DropDownList ID="ddldept" runat="server" AutoPostBack="true" Width="180" Height="25"
                                                        OnSelectedIndexChanged="ddldept_SelectedIndexChanged">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lblMonth" runat="server" Text="Month" Font-Bold="true"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlMonths" runat="server" AutoPostBack="true" Width="180" Height="25">
                                                    </asp:DropDownList>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblfinance" runat="server" Text="Financial Year" Font-Bold="true"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlFinance" runat="server" AutoPostBack="true" Width="180"
                                                        Height="25">
                                                    </asp:DropDownList>
                                                </td>
                                                 <td colspan="1">
                                                    <asp:Button ID="btnSearch" runat="server" Text="Print" OnClick="btnSearch_Click"
                                                        CssClass="button green" />
                                                    <asp:Button ID="btnExport" runat="server" Text="Export" CssClass="button green" OnClick="btnExport_Click" />
                                                     <asp:Button ID="btnOther" runat="server" Text="RM Days" CssClass="button green" 
                                                         onclick="btnOther_Click"  />
                                                    
                                                    
                                                    <asp:Button ID="btnConfirm" runat="server" Text="Confirmation" Visible="false" 
                                                        CssClass="button green" Width="120" onclick="btnConfirm_Click" />
                                                </td>
                                                
                                            </tr>
                                            
                                            <!-- Common OLD GRID -->
                                            <tr id="gv" runat="server" visible="false">
                                                <td colspan="4">
                                                    <asp:GridView ID="gvSalary" runat="server" AutoGenerateColumns="false" 
                                                        onselectedindexchanged="gvSalary_SelectedIndexChanged">
                                                        <Columns>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>
                                                                    SI. No</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <%# Container.DataItemIndex + 1   %></ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                        <HeaderTemplate>Emp No</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblEmpNo" runat="server" Text='<%# Eval("EmpNo") %>'></asp:Label>
                                                                        </ItemTemplate>    
                                                                    </asp:TemplateField>
                                                                    
                                                             <asp:TemplateField>
                                                                        <HeaderTemplate>Acc No</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblAccNo" runat="server" Text='<%# Eval("AccNo") %>'></asp:Label>
                                                                        </ItemTemplate>    
                                                                    </asp:TemplateField>       
                                                                    
                                                                    
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>
                                                                    Ticket No</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblExist" runat="server" Text='<%# Eval("ExisistingCode") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>
                                                                    Name</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblname" runat="server" Text='<%# Eval("EmpName") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                         
                                                            <asp:TemplateField>
                                                                        <HeaderTemplate>Department</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lbldepartment" runat="server" Text='<%# Eval("DepartmentNm") %>' ></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                        <HeaderTemplate>Totalworkingdays</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lbltotalworkingdays" runat="server" Text='<%# Eval("Totalworkingdays") %>' ></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>
                                                                    Working Days</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblWorkingDays" runat="server" Text='<%# Eval("WorkedDays") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField Visible="false">
                                                                <HeaderTemplate>Remaining Days</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblRemainingDays" runat="server" Text='<%# Eval("Remaining_Days") %>' Visible="false"></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                      
                                                            <%--<asp:TemplateField>
                                                                <HeaderTemplate>Total Days</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblTotal" runat="server" Text='<%# Eval("tot") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>--%>
                                                          
                                                       
                                                            <%--<asp:TemplateField>
                                                                <HeaderTemplate>
                                                                    OT Hours</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblOThours" runat="server" Text=""></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>--%>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>
                                                                    Basic Salary</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblBasic" runat="server" Text='<%# Eval("Base") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>
                                                                    Base</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblBase" runat="server" Text='<%# Eval("BasicandDA") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                     
                                                       <asp:TemplateField>
                                                                <HeaderTemplate>
                                                                    TotalEarnings</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbltot" runat="server" Text='<%# Eval("GrossEarnings") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        
                                                                                          
                                                           
                                                      
                                                        
                                                      
                                                         
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>
                                                                    DAY INCENTIVE</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblDay" runat="server" Text='<%# Eval("DayIncentive") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                                  <asp:TemplateField>
                                                                <HeaderTemplate>
                                                                    OT AMT</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblOT" runat="server" Text='<%# Eval("OverTime") %>'></asp:Label>
                                                                </ItemTemplate>
                                                         </asp:TemplateField>
                                                         
                                                           
                                                       
                                                      
                                                     
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>
                                                                    Total Deduction</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbltotded" runat="server" Text='<%# Eval("TotalDeductions") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                         <%--    <asp:TemplateField>
                                                                <HeaderTemplate>
                                                                    OT</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblOThours" runat="server" Text=''></asp:Label>
                                                                </ItemTemplate>
                                                         </asp:TemplateField>--%>
                                                   
                                                           
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Net Amount</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblnet" runat="server" Text='<%# Eval("NetPay") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Signature</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="LBLSIGNATURE" runat="server" Text=""></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                            <!-- Common OLD GRID End -->
                                            
                                            <!-- Casual Worker Grid -->
                                            <tr id="CasualWorker" runat="server" visible="false">
                                                <td colspan="4">
                                                    <asp:GridView ID="CasualWorkerGridView" runat="server" AutoGenerateColumns="false">
                                                        <Columns>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>SI. No</HeaderTemplate>
                                                                <ItemTemplate><%# Container.DataItemIndex + 1   %></ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Emp No</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblEmpNo" runat="server" Text='<%# Eval("EmpNo") %>'></asp:Label>
                                                                </ItemTemplate>    
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                           
                                                                        <HeaderTemplate>Acc No</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblAccNo" runat="server" Text='<%# Eval("AccNo") %>'></asp:Label>
                                                                        </ItemTemplate>    
                                                                    </asp:TemplateField>       
                                                            
                                                            
                                                             <asp:TemplateField>
                                                                <HeaderTemplate>Ticket No</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblExist" runat="server" Text='<%# Eval("ExisistingCode") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                           
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Name</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblname" runat="server" Text='<%# Eval("EmpName") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                  
                                                            
                                                                 <asp:TemplateField>
                                                                <HeaderTemplate> NO.OF DAYS</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblWorkDays" runat="server" Text='<%# Eval("WorkedDays") %>' ></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                                                                                 
                                                        
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>ONEDAY SALARY</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblBasicSal" runat="server" Text='<%# Eval("Base") %>' ></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                      <asp:TemplateField>
                                                                <HeaderTemplate>TOTAL WAGES</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblBasicSal" runat="server" Text='<%# Eval("TotalBasic") %>' ></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                             <asp:TemplateField>
                                                                <HeaderTemplate>Fine1</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblDeduction3" runat="server" Text='<%# Eval("Deduction3") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Fine2</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblDeduction4" runat="server" Text='<%# Eval("Deduction4") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                             <asp:TemplateField>
                                                                <HeaderTemplate>Fine3</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblDeduction3" runat="server" Text='<%# Eval("Deduction5") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                                 <asp:TemplateField>
                                                                <HeaderTemplate>Late Fine</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblDeduction3" runat="server" Text='<%# Eval("FineAmt") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField>
                                                                <HeaderTemplate>
                                                                    Total Deduction</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbltotded" runat="server" Text='<%# Eval("TotalDeductions") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                
                                                    
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Net Amount</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblnet" runat="server" Text='<%# Eval("NetPay") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                            <asp:TemplateField>
                                                              <HeaderTemplate>
                                                                Round
                                                              </HeaderTemplate>
                                                              <ItemTemplate>
                                                                  <asp:Label ID="lblRound" runat="server" Text='<%# Eval("Val") %>'></asp:Label>
                                                              </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Signature</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="LBLSIGNATURE" runat="server" Text=""></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                            <!-- Casual Worker Grid End -->
                                            
                                            <!--Staff Worker NEW-->
                                              <tr id="Tr1" runat="server" visible="false">
                                                <td colspan="4">
                                                    <asp:GridView ID="StaffWorkerNew" runat="server" AutoGenerateColumns="false">
                                                        <Columns>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>SI. No</HeaderTemplate>
                                                                <ItemTemplate><%# Container.DataItemIndex + 1   %></ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Emp No</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblEmpNo" runat="server" Text='<%# Eval("EmpNo") %>'></asp:Label>
                                                                </ItemTemplate>    
                                                            </asp:TemplateField>
                                                            
                                                             <asp:TemplateField>
                                                                        <HeaderTemplate>UAN No</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblUAN" runat="server" Text='<%# Eval("UAN") %>'></asp:Label>
                                                                        </ItemTemplate>    
                                                                    </asp:TemplateField>  
                                                            
                                                             <asp:TemplateField>
                                                                        <HeaderTemplate>Acc No</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblAccNo" runat="server" Text='<%# Eval("AccNo") %>'></asp:Label>
                                                                        </ItemTemplate>    
                                                                    </asp:TemplateField>       
                                                            
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Ticket No</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblExist" runat="server" Text='<%# Eval("ExisistingCode") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                           
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Name</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblname" runat="server" Text='<%# Eval("EmpName") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                          <%--   <asp:TemplateField>
                                                                <HeaderTemplate>Department</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblDepartment" runat="server" Text='<%# Eval("DepartmentNm") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>--%>
                                                            
                                                                 <asp:TemplateField>
                                                                <HeaderTemplate> NO.OF DAYS</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblWorkDays" runat="server" Text='<%# Eval("WorkedDays") %>' ></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                                                                                 
                                                        
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>BASIC SALARY</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblBasicSal" runat="server" Text='<%# Eval("Base") %>' ></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                            
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>BASIC</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblBasicAndDA" runat="server" Text='<%# Eval("BasicAndDA") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                             <asp:TemplateField>
                                                                <HeaderTemplate>HRA</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblHRA" runat="server" Text='<%# Eval("HRA") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                             <asp:TemplateField>
                                                                <HeaderTemplate>TA</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblFDA" runat="server" Text='<%# Eval("FDA") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                             <%--<asp:TemplateField>
                                                                <HeaderTemplate>CA</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblCA" runat="server" Text='<%# Eval("Conveyance") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>WA</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblWA" runat="server" Text='<%# Eval("Washing") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>--%>
                                                            
                                                        
                                                           
                                                          
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Total</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblBasicTot" runat="server" Text='<%# Eval("TotalBasic") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                            <asp:TemplateField>
                                                             <HeaderTemplate>P.F</HeaderTemplate>
                                                               <ItemTemplate>
                                                                 <asp:Label ID="lblOT_Days" runat="server" Text='<%# Eval("PfSalary") %>'></asp:Label>
                                                               </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                             <asp:TemplateField>
                                                                <HeaderTemplate>ESI</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblESI" runat="server" Text='<%# Eval("ESI") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                             <asp:TemplateField>
                                                                <HeaderTemplate>Fine1</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblDeduction3" runat="server" Text='<%# Eval("Deduction3") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                             <asp:TemplateField>
                                                                <HeaderTemplate>Fine2</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblDeduction4" runat="server" Text='<%# Eval("Deduction4") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                             <asp:TemplateField>
                                                                <HeaderTemplate>Fine3</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblDeduction3" runat="server" Text='<%# Eval("Deduction5") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                                  <asp:TemplateField>
                                                                <HeaderTemplate>Late Fine</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblDeduction3" runat="server" Text='<%# Eval("FineAmt") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                         <%--   <asp:TemplateField>
                                                              <HeaderTemplate>
                                                              OT_Days_AMT
                                                              </HeaderTemplate>
                                                              <ItemTemplate>
                                                                    <asp:Label ID="lblOT_Days_AMT" runat="server" Text='<%# Eval("withpay") %>'></asp:Label>
                                                              </ItemTemplate>
                                                            </asp:TemplateField>--%>
                                                       
                                                           
                                                            
                                                       
                                                       
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>
                                                                    Total Deduction</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbltotded" runat="server" Text='<%# Eval("TotalDeductions") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                        <%--    <asp:TemplateField>
                                                                <HeaderTemplate>
                                                                    OT Hours</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblOThours" runat="server" Text=""></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>
                                                                    OT</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblOT" runat="server" Text='<%# Eval("OverTime") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>--%>
                                                    
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Net Amount</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblnet" runat="server" Text='<%# Eval("NetPay") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Round</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblRound" runat="server" Text='<%# Eval("Val") %>'  ></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                        
                                                            
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Signature</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="LBLSIGNATURE" runat="server" Text=""></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                            <!--Staff Worker NEW End-->
                                            
                                            <!-- Scheme Worker Grid -->
                                            <tr id="SchemeWorker" runat="server" visible="false">
                                                <td colspan="4">
                                                    <asp:GridView ID="SchemeWorkerGridView" runat="server" AutoGenerateColumns="false">
                                                        <Columns>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>SI. No</HeaderTemplate>
                                                                <ItemTemplate><%# Container.DataItemIndex + 1   %></ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Emp No</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblEmpNo" runat="server" Text='<%# Eval("EmpNo") %>'></asp:Label>
                                                                </ItemTemplate>    
                                                            </asp:TemplateField>
                                                            
                                                             <asp:TemplateField>
                                                                        <HeaderTemplate>UAN No</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblUAN" runat="server" Text='<%# Eval("UAN") %>'></asp:Label>
                                                                        </ItemTemplate>    
                                                                    </asp:TemplateField>  
                                                            
                                                             <asp:TemplateField>
                                                                        <HeaderTemplate>Acc No</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblAccNo" runat="server" Text='<%# Eval("AccNo") %>'></asp:Label>
                                                                        </ItemTemplate>    
                                                                    </asp:TemplateField>       
                                                            
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Ticket No</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblExist" runat="server" Text='<%# Eval("ExisistingCode") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                           
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Name</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblname" runat="server" Text='<%# Eval("EmpName") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                          <%--   <asp:TemplateField>
                                                                <HeaderTemplate>Department</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblDepartment" runat="server" Text='<%# Eval("DepartmentNm") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>--%>
                                                            
                                                                 <asp:TemplateField>
                                                                <HeaderTemplate> NO.OF DAYS</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblWorkDays" runat="server" Text='<%# Eval("WorkedDays") %>' ></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                                                                                 
                                                        
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>BASIC SALARY</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblBasicSal" runat="server" Text='<%# Eval("Base") %>' ></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                            
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>BASIC</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblBasicAndDA" runat="server" Text='<%# Eval("BasicAndDA") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                             <asp:TemplateField>
                                                                <HeaderTemplate>HRA</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblHRA" runat="server" Text='<%# Eval("HRA") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                            <%-- <asp:TemplateField>
                                                                <HeaderTemplate>TA</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblFDA" runat="server" Text='<%# Eval("FDA") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>--%>
                                                             <asp:TemplateField>
                                                                <HeaderTemplate>CA</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblCA" runat="server" Text='<%# Eval("Conveyance") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>WA</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblWA" runat="server" Text='<%# Eval("Washing") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                        
                                                           
                                                          
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Total</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblBasicTot" runat="server" Text='<%# Eval("TotalBasic") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                            <asp:TemplateField>
                                                             <HeaderTemplate>P.F</HeaderTemplate>
                                                               <ItemTemplate>
                                                                 <asp:Label ID="lblOT_Days" runat="server" Text='<%# Eval("PfSalary") %>'></asp:Label>
                                                               </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                             <asp:TemplateField>
                                                                <HeaderTemplate>ESI</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblESI" runat="server" Text='<%# Eval("ESI") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                             <asp:TemplateField>
                                                                <HeaderTemplate>Fine1</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblDeduction3" runat="server" Text='<%# Eval("Deduction3") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                             <asp:TemplateField>
                                                                <HeaderTemplate>Fine2</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblDeduction4" runat="server" Text='<%# Eval("Deduction4") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                             <asp:TemplateField>
                                                                <HeaderTemplate>Fine3</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblDeduction3" runat="server" Text='<%# Eval("Deduction5") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                                  <asp:TemplateField>
                                                                <HeaderTemplate>Late Fine</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblDeduction3" runat="server" Text='<%# Eval("FineAmt") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                         <%--   <asp:TemplateField>
                                                              <HeaderTemplate>
                                                              OT_Days_AMT
                                                              </HeaderTemplate>
                                                              <ItemTemplate>
                                                                    <asp:Label ID="lblOT_Days_AMT" runat="server" Text='<%# Eval("withpay") %>'></asp:Label>
                                                              </ItemTemplate>
                                                            </asp:TemplateField>--%>
                                                       
                                                           
                                                            
                                                       
                                                       
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>
                                                                    Total Deduction</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbltotded" runat="server" Text='<%# Eval("TotalDeductions") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                        <%--    <asp:TemplateField>
                                                                <HeaderTemplate>
                                                                    OT Hours</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblOThours" runat="server" Text=""></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>
                                                                    OT</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblOT" runat="server" Text='<%# Eval("OverTime") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>--%>
                                                    
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Net Amount</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblnet" runat="server" Text='<%# Eval("NetPay") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Round</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblRound" runat="server" Text='<%# Eval("Val") %>'  ></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                        
                                                            
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Signature</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="LBLSIGNATURE" runat="server" Text=""></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                            <!-- Scheme Worker Grid End -->
                                            
                                            
                                            
                                            <!-- Non Scheme Worker Grid -->
                                            <tr id="NonSchemeWorker" runat="server" visible="false">
                                                <td colspan="4">
                                                    <asp:GridView ID="NonSchemeWorkerGridView" runat="server" AutoGenerateColumns="false">
                                                        <Columns>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>SI. No</HeaderTemplate>
                                                                <ItemTemplate><%# Container.DataItemIndex + 1   %></ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Emp No</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblEmpNo" runat="server" Text='<%# Eval("EmpNo") %>'></asp:Label>
                                                                </ItemTemplate>    
                                                            </asp:TemplateField>
                                                            
                                                             <asp:TemplateField>
                                                                        <HeaderTemplate>Acc No</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblAccNo" runat="server" Text='<%# Eval("AccNo") %>'></asp:Label>
                                                                        </ItemTemplate>    
                                                                    </asp:TemplateField>       
                                                            
                                                            
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Ticket No</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblExist" runat="server" Text='<%# Eval("ExisistingCode") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                              <asp:TemplateField>
                                                                <HeaderTemplate>Name</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblname" runat="server" Text='<%# Eval("EmpName") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Department</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblDepartment" runat="server" Text='<%# Eval("DepartmentNm") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                              <asp:TemplateField>
                                                            
                                                                <HeaderTemplate>Per Day</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblBasicSal" runat="server" Text='<%# Eval("Base") %>' ></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                          
                                                          
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Working Days</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblWorkDays" runat="server" Text='<%# Eval("WorkedDays") %>' ></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                          
                                                     <%--
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Washing Allowance</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblall1" runat="server" Text='<%# Eval("allowances1") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>--%>
                                                            
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>GrossAmount</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblBasicTot" runat="server" Text='<%# Eval("GrossEarnings") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                           
                                                        
                                                        
                                                     
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>
                                                                    Mess</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblded1" runat="server" Text='<%# Eval("Hosteldeduction") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                   
                                                       
                                                        <%--       <asp:TemplateField>
                                                                <HeaderTemplate>OT Hours</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblOThours" runat="server" Text=''></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                           <asp:TemplateField>
                                                                <HeaderTemplate>OT Amount</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblOverTime" runat="server" Text='<%# Eval("OverTime") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>--%>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>
                                                                    Others</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbltotded" runat="server" Text='<%# Eval("Total") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                     
                                                                 <asp:TemplateField>
                                                                <HeaderTemplate>Net Amount</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblnet" runat="server" Text='<%# Eval("NetPay") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Signature</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="LBLSIGNATURE" runat="server" Text=""></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                            <!-- Non Scheme Worker Grid End -->
                                            
                                            <!-- STAFF Worker PF Grid -->
                                            <tr id="StaffWorker" runat="server" visible="false">
                                                <td colspan="4">
                                                    <asp:GridView ID="StaffWorkerPFGridView" runat="server" AutoGenerateColumns="false">
                                                        <Columns>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>SI. No</HeaderTemplate>
                                                                <ItemTemplate><%# Container.DataItemIndex + 1   %></ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Emp No</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblEmpNo" runat="server" Text='<%# Eval("EmpNo") %>'></asp:Label>
                                                                </ItemTemplate>    
                                                            </asp:TemplateField>
                                                            
                                                             <asp:TemplateField>
                                                                        <HeaderTemplate>Acc No</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblAccNo" runat="server" Text='<%# Eval("AccNo") %>'></asp:Label>
                                                                        </ItemTemplate>    
                                                                    </asp:TemplateField>       
                                                            
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Ticket No</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblExist" runat="server" Text='<%# Eval("ExisistingCode") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                              <asp:TemplateField>
                                                                <HeaderTemplate>Name</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblname" runat="server" Text='<%# Eval("EmpName") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Department</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblDepartment" runat="server" Text='<%# Eval("DepartmentNm") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                             <asp:TemplateField>
                                                                <HeaderTemplate>Fixed Salary</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblBasicSal" runat="server" Text='<%# Eval("Base") %>' ></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                              <asp:TemplateField>
                                                                <HeaderTemplate>FixedDays</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblFix" runat="server" text='<%# Eval("FixedDays") %>' ></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                          
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Working Days</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblWorkDays" runat="server" Text='<%# Eval("WorkedDays") %>' ></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                          
                                                           
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Basic & DA</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblFixBasicDA" runat="server" Text='<%# Eval("Fbasic") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>HRA</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblhra" runat="server" Text='<%# Eval("HRA") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        
                                                            <%--<asp:TemplateField>
                                                                <HeaderTemplate>VDA</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblBasicTot" runat="server" Text='<%# Eval("VDA") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>FDA</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblBasicTot" runat="server" Text='<%# Eval("FDA") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>--%>
                                                        
                                                        <asp:TemplateField>
                                                                <HeaderTemplate>GrossEarnings</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblgross" runat="server" text='<%# Eval("GrossEarnings") %>' ></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Ceiling</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblded1" runat="server" Text='<%# Eval("PFS") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                           
                                                       <%--  <asp:TemplateField>
                                                                <HeaderTemplate>OT Hours</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblOThours" runat="server" Text=''></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                           <asp:TemplateField>
                                                                <HeaderTemplate>OT Amount</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblOverTime" runat="server" Text='<%# Eval("OverTime") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>--%>
                                                            
                                                                <asp:TemplateField>
                                                                <HeaderTemplate>P.F</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblPF" runat="server" Text='<%# Eval("PfSalary") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        
                                                         
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Others</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbltotal" runat="server" Text='<%# Eval("Total") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                               <asp:TemplateField>
                                                                <HeaderTemplate>Total</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblTDeduction" runat="server" Text='<%# Eval("TotalDeductions") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                         
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Net Amount</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblnet" runat="server" Text='<%# Eval("NetPay") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Signature</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="LBLSIGNATURE" runat="server" Text=""></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                            
                                            
                                            <!-- STAFF Worker Grid End -->
                                            
                                            <!-- STAFF Worker Non PF Grid>-->
                                            <tr id="StaffWorkerNonPfGridView" runat="server" visible="false">
                                              <td colspan="4">
                                                  <asp:GridView ID="StaffWorkerNonPF" runat="server" AutoGenerateColumns="false">
                                                  <Columns>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>SI. No</HeaderTemplate>
                                                                <ItemTemplate><%# Container.DataItemIndex + 1   %></ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Emp No</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblEmpNo" runat="server" Text='<%# Eval("EmpNo") %>'></asp:Label>
                                                                </ItemTemplate>    
                                                            </asp:TemplateField>
                                                            
                                                             <asp:TemplateField>
                                                                        <HeaderTemplate>Acc No</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblAccNo" runat="server" Text='<%# Eval("AccNo") %>'></asp:Label>
                                                                        </ItemTemplate>    
                                                                    </asp:TemplateField>       
                                                            
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Ticket No</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblExist" runat="server" Text='<%# Eval("ExisistingCode") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                              <asp:TemplateField>
                                                                <HeaderTemplate>Name</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblname" runat="server" Text='<%# Eval("EmpName") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Department</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblDepartment" runat="server" Text='<%# Eval("DepartmentNm") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                             <asp:TemplateField>
                                                                <HeaderTemplate>Fixed Salary</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblBasicSal" runat="server" Text='<%# Eval("Base") %>' ></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                              <asp:TemplateField>
                                                                <HeaderTemplate>FixedDays</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblFix" runat="server" text='<%# Eval("FixedDays") %>' ></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                          
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Working Days</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblWorkDays" runat="server" Text='<%# Eval("WorkedDays") %>' ></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                          
                                                           
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Basic & DA</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblFixBasicDA" runat="server" Text='<%# Eval("Fbasic") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>HRA</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblhra" runat="server" Text='<%# Eval("HRA") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        
                                                            <%--<asp:TemplateField>
                                                                <HeaderTemplate>VDA</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblBasicTot" runat="server" Text='<%# Eval("VDA") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>FDA</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblBasicTot" runat="server" Text='<%# Eval("FDA") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>--%>
                                                        
                                                        <asp:TemplateField>
                                                                <HeaderTemplate>GrossEarnings</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblgross" runat="server" text='<%# Eval("GrossEarnings") %>' ></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Ceiling</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblded1" runat="server" Text='<%# Eval("PFS") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                           
                                                       <%--  <asp:TemplateField>
                                                                <HeaderTemplate>OT Hours</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblOThours" runat="server" Text=''></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                           <asp:TemplateField>
                                                                <HeaderTemplate>OT Amount</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblOverTime" runat="server" Text='<%# Eval("OverTime") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>--%>
                                                            
                                                                <asp:TemplateField>
                                                                <HeaderTemplate>Advance</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblPF" runat="server" Text='<%# Eval("Advance") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        
                                                         
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Others</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbltotal" runat="server" Text='<%# Eval("Total") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                               <asp:TemplateField>
                                                                <HeaderTemplate>Total</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblTDeduction" runat="server" Text='<%# Eval("TotalDeductions") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                         
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Net Amount</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblnet" runat="server" Text='<%# Eval("NetPay") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Signature</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="LBLSIGNATURE" runat="server" Text=""></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                  </asp:GridView>
                                              </td>
                                            </tr>
                                            
                                            
                                            <!-- Staff Worker All Emp -->
                                            <tr id="StaffWorkerAll" runat="server" visible="false">
                                               <td colspan="4">
                                                   <asp:GridView ID="StaffWorkerAllGridView" runat="server" AutoGenerateColumns="false">
                                                   <Columns>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>SI. No</HeaderTemplate>
                                                                <ItemTemplate><%# Container.DataItemIndex + 1   %></ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Emp No</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblEmpNo" runat="server" Text='<%# Eval("EmpNo") %>'></asp:Label>
                                                                </ItemTemplate>    
                                                            </asp:TemplateField>
                                                            
                                                             <asp:TemplateField>
                                                                        <HeaderTemplate>Acc No</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblAccNo" runat="server" Text='<%# Eval("AccNo") %>'></asp:Label>
                                                                        </ItemTemplate>    
                                                                    </asp:TemplateField>      
                                                            
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Ticket No</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblExist" runat="server" Text='<%# Eval("ExisistingCode") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                              <asp:TemplateField>
                                                                <HeaderTemplate>Name</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblname" runat="server" Text='<%# Eval("EmpName") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Department</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblDepartment" runat="server" Text='<%# Eval("DepartmentNm") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                             <asp:TemplateField>
                                                                <HeaderTemplate>Fixed Salary</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblBasicSal" runat="server" Text='<%# Eval("Base") %>' ></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                              <asp:TemplateField>
                                                                <HeaderTemplate>FixedDays</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblFix" runat="server" text='<%# Eval("FixedDays") %>' ></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                          
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Working Days</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblWorkDays" runat="server" Text='<%# Eval("WorkedDays") %>' ></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                          
                                                           
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Basic & DA</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblFixBasicDA" runat="server" Text='<%# Eval("Fbasic") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>HRA</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblhra" runat="server" Text='<%# Eval("HRA") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        
                                                            <%--<asp:TemplateField>
                                                                <HeaderTemplate>VDA</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblBasicTot" runat="server" Text='<%# Eval("VDA") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>FDA</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblBasicTot" runat="server" Text='<%# Eval("FDA") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>--%>
                                                        
                                                        <asp:TemplateField>
                                                                <HeaderTemplate>GrossEarnings</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblgross" runat="server" text='<%# Eval("GrossEarnings") %>' ></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Ceiling</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblded1" runat="server" Text='<%# Eval("PFS") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                           
                                                       <%--  <asp:TemplateField>
                                                                <HeaderTemplate>OT Hours</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblOThours" runat="server" Text=''></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                           <asp:TemplateField>
                                                                <HeaderTemplate>OT Amount</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblOverTime" runat="server" Text='<%# Eval("OverTime") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>--%>
                                                            
                                                             <asp:TemplateField>
                                                                <HeaderTemplate>P.F</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblPF" runat="server" Text='<%# Eval("FDA") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                                <asp:TemplateField>
                                                                <HeaderTemplate>Advance</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblAdvance" runat="server" Text='<%# Eval("Advance") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        
                                                         
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Others</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbltotal" runat="server" Text='<%# Eval("Total") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                               <asp:TemplateField>
                                                                <HeaderTemplate>Total</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblTDeduction" runat="server" Text='<%# Eval("TotalDeductions") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                         
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Net Amount</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblnet" runat="server" Text='<%# Eval("NetPay") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Signature</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="LBLSIGNATURE" runat="server" Text=""></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                   </asp:GridView>
                                               </td>
                                            </tr>
                                            
                                            
                                            <!-- STAFF Salary Through Gride -->
                                            
                                            <tr id="StaffSalaryThrough" runat="server" visible="false">
                                              <td>
                                                  <asp:GridView ID="StaffSalaryThroughGridView" runat="server" AutoGenerateColumns="false">
                                                  <Columns>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>SI. No</HeaderTemplate>
                                                                <ItemTemplate><%# Container.DataItemIndex + 1   %></ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Emp No</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblEmpNo" runat="server" Text='<%# Eval("EmpNo") %>'></asp:Label>
                                                                </ItemTemplate>    
                                                            </asp:TemplateField>
                                                            
                                                             <asp:TemplateField>
                                                                        <HeaderTemplate>Acc No</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblAccNo" runat="server" Text='<%# Eval("AccNo") %>'></asp:Label>
                                                                        </ItemTemplate>    
                                                                    </asp:TemplateField>    
                                                            
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Ticket No</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblExist" runat="server" Text='<%# Eval("ExisistingCode") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                              <asp:TemplateField>
                                                                <HeaderTemplate>Name</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblname" runat="server" Text='<%# Eval("EmpName") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Department</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblDepartment" runat="server" Text='<%# Eval("DepartmentNm") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                             <asp:TemplateField>
                                                                <HeaderTemplate>Fixed Salary</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblBasicSal" runat="server" Text='<%# Eval("Base") %>' ></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                              <asp:TemplateField>
                                                                <HeaderTemplate>FixedDays</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblFix" runat="server" text='<%# Eval("FixedDays") %>' ></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                          
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Working Days</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblWorkDays" runat="server" Text='<%# Eval("WorkedDays") %>' ></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                          
                                                           
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Basic & DA</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblFixBasicDA" runat="server" Text='<%# Eval("Fbasic") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>HRA</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblhra" runat="server" Text='<%# Eval("HRA") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        
                                                            <%--<asp:TemplateField>
                                                                <HeaderTemplate>VDA</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblBasicTot" runat="server" Text='<%# Eval("VDA") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>FDA</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblBasicTot" runat="server" Text='<%# Eval("FDA") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>--%>
                                                        
                                                        <asp:TemplateField>
                                                                <HeaderTemplate>GrossEarnings</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblgross" runat="server" text='<%# Eval("GrossEarnings") %>' ></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Ceiling</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblded1" runat="server" Text='<%# Eval("PFS") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                           
                                                       <%--  <asp:TemplateField>
                                                                <HeaderTemplate>OT Hours</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblOThours" runat="server" Text=''></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                           <asp:TemplateField>
                                                                <HeaderTemplate>OT Amount</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblOverTime" runat="server" Text='<%# Eval("OverTime") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>--%>
                                                            
                                                                <asp:TemplateField>
                                                                <HeaderTemplate>P.F</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblPF" runat="server" Text='<%# Eval("PfSalary") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        
                                                         
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Others</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbltotal" runat="server" Text='<%# Eval("Total") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                               <asp:TemplateField>
                                                                <HeaderTemplate>Total</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblTDeduction" runat="server" Text='<%# Eval("TotalDeductions") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                         
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Net Amount</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblnet" runat="server" Text='<%# Eval("NetPay") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Signature</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="LBLSIGNATURE" runat="server" Text=""></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                  </asp:GridView>
                                              </td>
                                            </tr>
                                            
                                            <!-- Hindi-boys  Grid -->
                                            <tr id="PermanentWorker" runat="server" visible="false">
                                                <td colspan="4">
                                                    <asp:GridView ID="PermanentWorkerGridView" runat="server" AutoGenerateColumns="false">
                                                        <Columns>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>SI. No</HeaderTemplate>
                                                                <ItemTemplate><%# Container.DataItemIndex + 1   %></ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Emp No</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblEmpNo" runat="server" Text='<%# Eval("EmpNo") %>'></asp:Label>
                                                                </ItemTemplate>    
                                                            </asp:TemplateField>
                                                            
                                                             <asp:TemplateField>
                                                                        <HeaderTemplate>Acc No</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblAccNo" runat="server" Text='<%# Eval("AccNo") %>'></asp:Label>
                                                                        </ItemTemplate>    
                                                                    </asp:TemplateField>    
                                                            
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Name</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblname" runat="server" Text='<%# Eval("EmpName") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Department</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblDepartment" runat="server" Text='<%# Eval("DepartmentNm") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Ticket No</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblExist" runat="server" Text='<%# Eval("ExisistingCode") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Basic Salary</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblBasicSal" runat="server" Text='<%# Eval("Base") %>' ></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Work Days</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblWorkDays" runat="server" Text='<%# Eval("WorkedDays") %>' ></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>NFH Days</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblNFH" runat="server" Text='<%# Eval("NFh") %>' ></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Total Work Days</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblTotalWorkDay" runat="server" text='<%# Eval("TotalWorkDay") %>' ></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Amount</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblamt" runat="server" Text='<%# Eval("BasicandDA") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                          <asp:TemplateField>
                                                                <HeaderTemplate>OT Hours</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblOThours" runat="server" Text=''></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                           <asp:TemplateField>
                                                                <HeaderTemplate>OT Amount</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblOverTime" runat="server" Text='<%# Eval("OverTime") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Gross Amount</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblTotalSal" runat="server" Text='<%# Eval("TotalSalary") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Mess</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblAdvance" runat="server" Text='<%# Eval("Advance") %>'></asp:Label> 
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Total Deduction</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbltotded" runat="server" Text='<%# Eval("TotalDeductions") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Net Amount</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblnet" runat="server" Text='<%# Eval("NetPay") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Signature</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="LBLSIGNATURE" runat="server" Text=""></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                            
                                            <tr id="OtherWorker" runat="server" visible="false">
                                             <td colspan="4">
                                                 <asp:GridView ID="GvOtherWorker" runat="server" AutoGenerateColumns="false">
                                                   <Columns>
                                                    <asp:TemplateField>
                                                                <HeaderTemplate>Emp No</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblEmpNo" runat="server" Text='<%# Eval("EmpNo") %>'></asp:Label>
                                                                </ItemTemplate>    
                                                            </asp:TemplateField>
                                                             
                                                              
                                                               <asp:TemplateField>
                                                                <HeaderTemplate>Ticket No</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblExist" runat="server" Text='<%# Eval("ExisistingCode") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                           
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Name</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblname" runat="server" Text='<%# Eval("EmpName") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                  
                                                            
                                                                 <asp:TemplateField>
                                                                <HeaderTemplate> NO.OF DAYS</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblWorkDays" runat="server" Text='<%# Eval("FFDA") %>' ></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Basic</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblFFDA" runat="server" Text='<%# Eval("Fbasic") %>' ></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                                                         
                                                                                         
                                                                                                                 
                                                        
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>SALARY</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblBasicSal" runat="server" Text='<%# Eval("FHRA") %>' ></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                             <asp:TemplateField>
                                                             
                                                                <HeaderTemplate>Round</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblRound" runat="server" Text='<%# Eval("Val") %>'  ></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                    
                                                
                                                    
                                                          <%--  <asp:TemplateField>
                                                                <HeaderTemplate>Net Amount</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblnet" runat="server" Text='<%# Eval("NetPay") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>--%>
                                                            
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Signature</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="LBLSIGNATURE" runat="server" Text=""></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                   </Columns>
                                                 </asp:GridView>
                                             </td>
                                            </tr>
                                            <!-- Permanent Worker Grid End -->
                                    </table>
                                    <br />
                                    <br />
                                    <br />
                                    <br />
                                    <br />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <!-- end page-wrap -->
    <div id="footer">
        <ul>
            <li>Copyright &copy; 2012. All rights reserved.       </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <!-- end page-wrap -->
    <div id="footer">
        <ul>
            <li>Copyright &copy; 2012. All rights reserved.</li>
            <li>Powered by <a href="http://www.altius.co.in" target="_blank">Altius Infosystems</a></li>
        </ul>
    </div>
    <!-- end footer -->
    </form>
</body>
</html>
