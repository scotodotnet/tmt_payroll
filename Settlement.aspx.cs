﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Payroll;
using System.Globalization;
using Payroll.Configuration;
using Payroll.Data;
using System.Data.SqlClient;

using Altius.BusinessAccessLayer.BALDataAccess;

public partial class Settlement_1 : System.Web.UI.Page
{
    protected System.Resources.ResourceManager rm;
    protected string PayrollRegisterContentMeta, pageTitle;
    BALDataAccess objdata = new BALDataAccess();
    SalaryClass objSal = new SalaryClass();
    resignClass rej = new resignClass();
    DateTime JoiningDate;
    DateTime today;
    string stafflabour;
    string SessionAdmin = "";
    string SessionCcode;
    string SessionLcode;
    //string username = "Admin";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (SessionAdmin == "2")
        {
            Response.Redirect("AEmployeeRegistration.aspx");
        }
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        string ss = Session["UserId"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        lblComany.Text = SessionCcode + " - " + SessionLcode;
        if (!IsPostBack)
        {
            DropDwonCategory();
        }
        lblusername.Text = Session["Usernmdisplay"].ToString();
    }
    public void DropDwonCategory()
    {
        DataTable dtcate = new DataTable();
        dtcate = objdata.DropDownCategory();
        ddlcategory.DataSource = dtcate;
        ddlcategory.DataTextField = "CategoryName";
        ddlcategory.DataValueField = "CategoryCd";
        ddlcategory.DataBind();
    }
    public void clr()
    {
        txtESI.Text = "0";
        txtPF.Text = "0";
        txtresign.Text = "";
        lblDesignation1.Text = "";
        lbldoj1.Text = "";
        lblExperience1.Text = "";
        txtpfaccount.Text = "";
        txtESICacc.Text = "";
        txtESI.Enabled = false;
        txtReason.Text = "";
        txtPF.Enabled = false;

    }
    protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable dtempty = new DataTable();
        ddldepartment.DataSource = dtempty;
        ddldepartment.DataBind();
        ddlEmpNo.DataSource = dtempty;
        ddlEmpNo.DataBind();
        ddlEmpName.DataSource = dtempty;
        ddlEmpName.DataBind();
        clr();
        txtExist.Text = "";
        DataTable dtDip = new DataTable();
        if (ddlcategory.SelectedValue == "1")
        {
            stafflabour = "S";
        }
        else if (ddlcategory.SelectedValue == "2")
        {
            stafflabour = "L";
        }
        else
        {
            stafflabour = "0";
        }
        dtDip = objdata.DropDownDepartment_category(stafflabour);
        if (dtDip.Rows.Count > 1)
        {
            ddldepartment.DataSource = dtDip;
            ddldepartment.DataTextField = "DepartmentNm";
            ddldepartment.DataValueField = "DepartmentCd";
            ddldepartment.DataBind();
        }
        else
        {
            DataTable dt1 = new DataTable();
            ddldepartment.DataSource = dt1;
            ddldepartment.DataBind();
        }
    }
    protected void ddldepartment_SelectedIndexChanged(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        if (ddlcategory.SelectedValue == "0")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Category');", true);
            //System.Windows.Forms.MessageBox.Show("Select the Category", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            ErrFlag = true;
        }
        if (!ErrFlag)
        {
            DataTable dtempty = new DataTable();
            ddlEmpNo.DataSource = dtempty;
            ddlEmpNo.DataBind();
            ddlEmpName.DataSource = dtempty;
            ddlEmpName.DataBind();
            clr();
            txtExist.Text = "";
        }
    }
    protected void btnclick_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        if (ddlcategory.SelectedValue == "0")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Category Staff or Labour!');", true);
            //System.Windows.Forms.MessageBox.Show("Select the Category", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);

            ErrFlag = true;

        }
        else if (ddldepartment.SelectedValue == "0")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Department ');", true);
            //System.Windows.Forms.MessageBox.Show("Select the Department", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            ErrFlag = true;
        }
        if (!ErrFlag)
        {
            if (ddlcategory.SelectedValue == "1")
            {
                stafflabour = "S";
            }
            else if (ddlcategory.SelectedValue == "2")
            {
                stafflabour = "L";
            }
            DataTable dtempcode = new DataTable();
            dtempcode = objdata.SalaryEmpIDLoad(stafflabour, ddldepartment.SelectedValue, SessionCcode, SessionLcode);
            if (dtempcode.Rows.Count > 0)
            {
                ddlEmpNo.DataSource = dtempcode;
                DataRow dr = dtempcode.NewRow();
                dr["EmpNo"] = ddldepartment.SelectedItem.Text;
                dr["EmpName"] = ddldepartment.SelectedItem.Text;
                dtempcode.Rows.InsertAt(dr, 0);

                ddlEmpNo.DataTextField = "EmpNo";
                ddlEmpNo.DataValueField = "EmpNo";
                ddlEmpNo.DataBind();
                ddlEmpName.DataSource = dtempcode;
                ddlEmpName.DataTextField = "EmpName";
                ddlEmpName.DataValueField = "EmpNo";
                ddlEmpName.DataBind();
            }
        }
    }
    protected void ddlEmpNo_SelectedIndexChanged(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        DataTable dt = new DataTable();
        clr();
        txtExist.Text = "";
        if (ddlcategory.SelectedValue == "0")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Category Staff or Labour!');", true);
            //System.Windows.Forms.MessageBox.Show("Select the Category", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            ErrFlag = true;

        }
        else if (ddldepartment.SelectedValue == "0")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Department ');", true);
            //System.Windows.Forms.MessageBox.Show("Select the Department", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            ErrFlag = true;
        }
        if (!ErrFlag)
        {
            dt = objdata.Settle_EmpNo_load(ddlEmpNo.SelectedValue, SessionCcode, SessionLcode);
            if (dt.Rows.Count > 0)
            {
                lblDesignation1.Text = dt.Rows[0]["Designation"].ToString();
                ddlEmpName.SelectedValue = dt.Rows[0]["EmpNo"].ToString();
                txtExist.Text = dt.Rows[0]["ExisistingCode"].ToString();
                lbldoj1.Text = dt.Rows[0]["doj"].ToString();
                txtpfaccount.Text = dt.Rows[0]["PFnumber"].ToString();
                txtESICacc.Text = dt.Rows[0]["ESICnumber"].ToString();
                if (dt.Rows[0]["ElgibleESI"].ToString() == "1")
                {
                    rbesi.SelectedValue = "1";
                }
                else
                {
                    rbesi.SelectedValue = "2";
                }
                if (rbesi.SelectedValue == "1")
                {
                    txtESI.Enabled = true;
                }
                else
                {
                    txtESI.Enabled = false;
                    txtESI.Text = "0";
                }
                string dtserver = objdata.ServerDate();
                JoiningDate = DateTime.ParseExact(lbldoj1.Text, "dd-MM-yyyy", null);
                today = DateTime.ParseExact(dtserver, "dd-MM-yyyy", null);
                TimeSpan exp = new TimeSpan();
                exp = (today - JoiningDate);
                DateTime yrCal = DateTime.MinValue + exp;
                int Years = yrCal.Year - 1;
                int Months = yrCal.Month - 1;
                string Experience = (Years + "." + Months);
                lblExperience1.Text = Experience;
                DataTable dtadv = new DataTable();
                dtadv = objdata.Salary_advance_retrive(ddlEmpNo.SelectedValue, SessionCcode, SessionLcode);
                if (dtadv.Rows.Count > 0)
                {
                    txtAdvance.Text = dtadv.Rows[0]["BalanceAmount"].ToString();
                }
                else
                {
                    txtAdvance.Text = "0";
                }
                DataTable dtpf = new DataTable();
                dtpf = objdata.PF_ESI_Amt(ddlEmpNo.SelectedValue, SessionCcode, SessionLcode);
                if (dtpf.Rows[0]["PF"].ToString().Trim() != "")
                {
                    txtPF.Text = dtpf.Rows[0]["PF"].ToString();
                }
                else
                {
                    txtPF.Text = "0";
                }
                if (dtpf.Rows[0]["ESI"].ToString().Trim() != "")
                {
                    txtESI.Text = dtpf.Rows[0]["ESI"].ToString();
                }
                else
                {
                    txtESI.Text = "0";
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('No Employee Found');", true);
                //System.Windows.Forms.MessageBox.Show("No employee Found", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            }
        }
    }
    protected void ddlEmpName_SelectedIndexChanged(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        DataTable dt = new DataTable();
        clr();
        txtExist.Text = "";
        if (ddlcategory.SelectedValue == "0")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Category Staff or Labour!');", true);
            //System.Windows.Forms.MessageBox.Show("Select the Category", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            ErrFlag = true;

        }
        else if (ddldepartment.SelectedValue == "0")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Department ');", true);
            //System.Windows.Forms.MessageBox.Show("Select the Department", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            ErrFlag = true;
        }
        if (!ErrFlag)
        {
            dt = objdata.Settle_EmpNo_load(ddlEmpName.SelectedValue, SessionCcode, SessionLcode);
            if (dt.Rows.Count > 0)
            {
                lblDesignation1.Text = dt.Rows[0]["Designation"].ToString();
                ddlEmpNo.SelectedValue = dt.Rows[0]["EmpNo"].ToString();
                txtExist.Text = dt.Rows[0]["ExisistingCode"].ToString();
                lbldoj1.Text = dt.Rows[0]["doj"].ToString();
                txtpfaccount.Text = dt.Rows[0]["PFnumber"].ToString();
                txtESICacc.Text = dt.Rows[0]["ESICnumber"].ToString();
                if (dt.Rows[0]["ElgibleESI"].ToString() == "1")
                {
                    rbesi.SelectedValue = "1";
                }
                else
                {
                    rbesi.SelectedValue = "2";
                }
                if (rbesi.SelectedValue == "1")
                {
                    txtESI.Enabled = true;
                }
                else
                {
                    txtESI.Enabled = false;
                    txtESI.Text = "0";
                }
                string dtserver = objdata.ServerDate();
                JoiningDate = DateTime.ParseExact(lbldoj1.Text, "dd-MM-yyyy", null);
                today = DateTime.ParseExact(dtserver, "dd-MM-yyyy", null);
                TimeSpan exp = new TimeSpan();
                exp = (today - JoiningDate);
                DateTime yrCal = DateTime.MinValue + exp;
                int Years = yrCal.Year - 1;
                int Months = yrCal.Month - 1;
                string Experience = (Years + "." + Months);
                lblExperience1.Text = Experience;
                DataTable dtadv = new DataTable();
                dtadv = objdata.Salary_advance_retrive(ddlEmpNo.SelectedValue, SessionCcode, SessionLcode);
                if (dtadv.Rows.Count > 0)
                {
                    txtAdvance.Text = dtadv.Rows[0]["BalanceAmount"].ToString();
                }
                else
                {
                    txtAdvance.Text = "0";
                }
                DataTable dtpf = new DataTable();
                dtpf = objdata.PF_ESI_Amt(ddlEmpNo.SelectedValue, SessionCcode, SessionLcode);
                if (dtpf.Rows[0]["PF"].ToString().Trim() != "")
                {
                    txtPF.Text = dtpf.Rows[0]["PF"].ToString();
                }
                else
                {
                    txtPF.Text = "0";
                }
                if (dtpf.Rows[0]["ESI"].ToString().Trim() != "")
                {
                    txtESI.Text = dtpf.Rows[0]["ESI"].ToString();
                }
                else
                {
                    txtESI.Text = "0";
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('No Employee Found');", true);
                //System.Windows.Forms.MessageBox.Show("No Employees Found", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            }
        }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        DataTable dt = new DataTable();
        clr();
        if (ddlcategory.SelectedValue == "0")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Category Staff or Labour!');", true);
            //System.Windows.Forms.MessageBox.Show("Select the Category", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            ErrFlag = true;

        }
        else if (ddldepartment.SelectedValue == "0")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Department ');", true);
            //System.Windows.Forms.MessageBox.Show("Select the Department", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            ErrFlag = true;
        }
        if (!ErrFlag)
        {
            if (ddlcategory.SelectedValue == "1")
            {
                stafflabour = "S";
            }
            else if (ddlcategory.SelectedValue == "2")
            {
                stafflabour = "L";
            }

            dt = objdata.Settle_Exist_load(txtExist.Text, stafflabour, ddldepartment.SelectedValue, SessionCcode, SessionLcode);
            if (dt.Rows.Count > 0)
            {
                lblDesignation1.Text = dt.Rows[0]["Designation"].ToString();
                ddlEmpNo.SelectedValue = dt.Rows[0]["EmpNo"].ToString();
                ddlEmpName.SelectedValue = dt.Rows[0]["EmpNo"].ToString();
                txtExist.Text = dt.Rows[0]["ExisistingCode"].ToString();
                lbldoj1.Text = dt.Rows[0]["doj"].ToString();
                txtpfaccount.Text = dt.Rows[0]["PFnumber"].ToString();
                if (txtpfaccount.Text.Trim() == null)
                {
                    txtPF.Enabled = false;
                    txtPF.Text = "0";
                }
                else
                {
                    txtPF.Enabled = true;
                    txtPF.Text = "0";
                }
                txtESICacc.Text = dt.Rows[0]["ESICnumber"].ToString();
                if (dt.Rows[0]["ElgibleESI"].ToString() == "1")
                {
                    rbesi.SelectedValue = "1";
                }
                else
                {
                    rbesi.SelectedValue = "2";
                }
                if (rbesi.SelectedValue == "1")
                {
                    txtESI.Enabled = true;
                }
                else
                {
                    txtESI.Enabled = false;
                    txtESI.Text = "0";
                }
                string dtserver = objdata.ServerDate();
                JoiningDate = DateTime.ParseExact(lbldoj1.Text, "dd-MM-yyyy", null);
                today = DateTime.ParseExact(dtserver, "dd-MM-yyyy", null);
                TimeSpan exp = new TimeSpan();
                exp = (today - JoiningDate);
                DateTime yrCal = DateTime.MinValue + exp;
                int Years = yrCal.Year - 1;
                int Months = yrCal.Month - 1;
                string Experience = (Years + "." + Months);
                lblExperience1.Text = Experience;
                DataTable dtadv = new DataTable();
                dtadv = objdata.Salary_advance_retrive(ddlEmpNo.SelectedValue, SessionCcode, SessionLcode);
                if (dtadv.Rows.Count > 0)
                {
                    txtAdvance.Text = dtadv.Rows[0]["BalanceAmount"].ToString();
                }
                else
                {
                    txtAdvance.Text = "0";
                }
                DataTable dtpf = new DataTable();
                dtpf = objdata.PF_ESI_Amt(ddlEmpNo.SelectedValue, SessionCcode, SessionLcode);
                if (dtpf.Rows[0]["PF"].ToString().Trim() != "")
                {
                    txtPF.Text = dtpf.Rows[0]["PF"].ToString();
                }
                else
                {
                    txtPF.Text = "0";
                }
                if (dtpf.Rows[0]["ESI"].ToString().Trim() != "")
                {
                    txtESI.Text = dtpf.Rows[0]["ESI"].ToString();
                }
                else
                {
                    txtESI.Text = "0";
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('No Employee Found');", true);
                //System.Windows.Forms.MessageBox.Show("No Employee Found", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            }
        }
    }
    protected void txtExist_TextChanged(object sender, EventArgs e)
    {
        clr();
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        bool issave = false;
        try
        {
            if (ddlcategory.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Category Staff or Labour!');", true);
                //System.Windows.Forms.MessageBox.Show("Select the Category", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;

            }
            else if (ddldepartment.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Department ');", true);
                //System.Windows.Forms.MessageBox.Show("Select the Department", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            else if (txtExist.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Existing Code');", true);
                //System.Windows.Forms.MessageBox.Show("Enter the Existing Code", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            else if (txtPF.Text.Trim() == null)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the PF Amount');", true);
                //System.Windows.Forms.MessageBox.Show("Enter the PF Amount", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            else if (txtESI.Text.Trim() == null)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the ESI Amount');", true);
                //System.Windows.Forms.MessageBox.Show("Enter the ESI Amount", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            else if (lblDesignation1.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Proper Employee');", true);
                //System.Windows.Forms.MessageBox.Show("Select the Employee Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            else if (ddlEmpNo.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Employee properly');", true);
                //System.Windows.Forms.MessageBox.Show("Select the Employee Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            else if (txtresign.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the resignation Date');", true);
                //System.Windows.Forms.MessageBox.Show("Enter the Resignation Date", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            else if (txtReason.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Reason');", true);
                //System.Windows.Forms.MessageBox.Show("Enter the Reason", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            if (!ErrFlag)
            {
                if (ddlcategory.SelectedValue == "1")
                {
                    stafflabour = "S";
                }
                else if (ddlcategory.SelectedValue == "2")
                {
                    stafflabour = "L";
                }
                rej.Category = stafflabour;
                rej.department = ddldepartment.SelectedValue;
                rej.EmpNo = ddlEmpNo.SelectedValue;
                rej.EmpName = ddlEmpName.Text;
                rej.Exist = txtExist.Text;
                rej.designation = lblDesignation1.Text;
                rej.Experience = lblExperience1.Text;
                rej.EligibleESI = rbesi.SelectedValue;
                rej.pfacc = txtpfaccount.Text;
                rej.esiacc = txtESICacc.Text;
                rej.pfamt = txtPF.Text;
                rej.ESiamt = txtESI.Text;
                rej.Reason = txtReason.Text;
                rej.Ccode = SessionCcode;
                rej.Lcode = SessionLcode;
                rej.Username = Session["UserId"].ToString();
                rej.Advance = txtAdvance.Text;
                DateTime doj;
                DateTime dor;
                doj = DateTime.ParseExact(lbldoj1.Text, "dd-MM-yyyy", null);
                dor = DateTime.ParseExact(txtresign.Text, "dd-MM-yyyy", null);
                objdata.settle_insert(rej, doj, dor);
                issave = true;
            }
            if (issave == true)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Saved Successfully');", true);
                //System.Windows.Forms.MessageBox.Show("Saved Successfully", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                clr();
                txtExist.Text = "";
                DataTable dtempty = new DataTable();
                ddldepartment.DataSource = dtempty;
                ddldepartment.DataBind();
                ddlEmpNo.DataSource = dtempty;
                ddlEmpNo.DataBind();
                ddlEmpName.DataSource = dtempty;
                ddlEmpName.DataBind();
            }
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Contact Admin');", true);
            //System.Windows.Forms.MessageBox.Show("Contact Admin", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
        }
    }
    protected void btncancel_Click(object sender, EventArgs e)
    {
        clr();
        txtExist.Text = "";
        DataTable dtempty = new DataTable();
        ddldepartment.DataSource = dtempty;
        ddldepartment.DataBind();
        ddlEmpNo.DataSource = dtempty;
        ddlEmpNo.DataBind();
        ddlEmpName.DataSource = dtempty;
        ddlEmpName.DataBind();
    }
    protected void btnEmp_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptEmpDownload.aspx");
    }
    protected void btnatt_Click(object sender, EventArgs e)
    {
        Response.Redirect("AttenanceDownload.aspx");
    }
    protected void btnLeave_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptLeaveSample.aspx");
    }
    protected void btnOT_Click(object sender, EventArgs e)
    {
        Response.Redirect("OTDownload.aspx");
    }
    protected void btncontract_Click(object sender, EventArgs e)
    {
        Response.Redirect("ContractRPT.aspx");
    }
    protected void btnSal_Click(object sender, EventArgs e)
    {
        Response.Redirect("FrmDeductionDownload.aspx");
    }
}
