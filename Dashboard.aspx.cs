﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Drawing;
using Payroll;
using Payroll.Configuration;
using Payroll.Data;
using System.IO;

using Altius.BusinessAccessLayer.BALDataAccess;

using System.Web.UI.DataVisualization.Charting;
using System.Data;
using System.Data.SqlClient;

public partial class Dashboard : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    bool dd = false;
    string SessionAdmin;
    bool SearchErr = false;
    string Stafflabour;
    string SessionCcode;
    string SessionLcode;
    string Datetime_ser;
    static int month;
    static string yr;
    DateTime mydate;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        string ss = Session["UserId"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();
        if (!IsPostBack)
        {
            SettingServerDate();
            //DropDwonCategory();
            Chart_Call();
        }
        lblComany.Text = SessionCcode + " - " + SessionLcode;
        lblusername.Text = Session["Usernmdisplay"].ToString();
        //Chart_Call();
    }
    public void SettingServerDate()
    {
        Datetime_ser = objdata.ServerDate();
        //txtDate.Text = Datetime_ser;
    }
    public void Chart_Call()
    {

        DataTable dt = new DataTable();
        DataSet ds = new DataSet();
        SqlDataAdapter da= new SqlDataAdapter();
        mydate = Convert.ToDateTime(Datetime_ser).AddMonths(-1);
        month = Convert.ToInt32(mydate.Month.ToString());
        yr = mydate.Year.ToString();
        #region Month
        string Mont = "";
        switch (month)
        {
            case 1:
                Mont = "January";
                break;

            case 2:
                Mont = "February";
                break;
            case 3:
                Mont = "March";
                break;
            case 4:
                Mont = "April";
                break;
            case 5:
                Mont = "May";
                break;
            case 6:
                Mont = "June";
                break;
            case 7:
                Mont = "July";
                break;
            case 8:
                Mont = "August";
                break;
            case 9:
                Mont = "September";
                break;
            case 10:
                Mont = "October";
                break;
            case 11:
                Mont = "November";
                break;
            case 12:
                Mont = "December";
                break;
            default:
                break;
        }
        #endregion
        string constr = ConfigurationManager.AppSettings["ConnectionString"];
        SqlConnection con = new SqlConnection(constr);
        string qry = "Select count(ED.EmpNo) as EmpNo,MD.DepartmentNm from EmployeeDetails ED inner Join MstDepartment MD on " +
                     " MD.DepartmentCd=ED.Department where ED.ActivateMode='Y' and ED.Ccode='" + SessionCcode + "' and ED.Lcode='" + SessionLcode + "' group by ED.Department, MD.DepartmentNm";
        SqlCommand cmd = new SqlCommand(qry,con);
        da.SelectCommand = cmd;
        con.Open();
        da.Fill(ds);
        con.Close();
        dt = ds.Tables[0];
        Chart1.DataSource = dt;
        Chart1.Series["Series1"].XValueMember = "DepartmentNm";
        Chart1.Series["Series1"].YValueMembers = "EmpNo";
        this.Chart1.Series[0]["PieLabelStyle"] = "Outside";
        this.Chart1.Series[0].BorderWidth = 1;
        this.Chart1.Series[0].BorderColor = System.Drawing.Color.FromArgb(26, 59, 105);
        this.Chart1.Legends.Add("Legend1");
        this.Chart1.Legends[0].Enabled = true;
        this.Chart1.Legends[0].Docking = Docking.Bottom;
        this.Chart1.Legends[0].Alignment = System.Drawing.StringAlignment.Center;
        this.Chart1.Series[0].LegendText = "#PERCENT{P0}";
        this.Chart1.DataManipulator.Sort(PointSortOrder.Descending, Chart2.Series[0]);
        Chart1.DataBind();

        Chart2.DataSource = dt;
        Chart2.Series["Series1"].XValueMember = "DepartmentNm";
        Chart2.Series["Series1"].YValueMembers = "EmpNo";
        
        Chart2.Series[0].IsValueShownAsLabel = true;
        //this.Chart2.Series[0]["PieLabelStyle"] = "Outside";
        //this.Chart2.Series[0].BorderWidth = 1;
        //this.Chart2.Series[0].BorderColor = System.Drawing.Color.FromArgb(26, 59, 105);
        //this.Chart2.Legends.Add("Legend1");
        //this.Chart2.Legends[0].Enabled = true;
        //this.Chart2.Legends[0].Docking = Docking.Bottom;
        //this.Chart2.Legends[0].Alignment = System.Drawing.StringAlignment.Center;
        //this.Chart2.Series[0].LegendText = "#Value";
        //this.Chart2.DataManipulator.Sort(PointSortOrder.Descending, Chart2.Series[0]);
        Chart2.DataBind();

        //Line Chart
        Chart3.DataSource = dt;
        Chart3.Series["Series1"].XValueMember = "DepartmentNm";
        Chart3.Series["Series1"].YValueMembers = "EmpNo";

        Chart3.Series[0].IsValueShownAsLabel = true;
        //this.Chart3.Series[0]["PieLabelStyle"] = "Outside";
        //this.Chart3.Series[0].BorderWidth = 1;
        //this.Chart3.Series[0].BorderColor = System.Drawing.Color.FromArgb(26, 59, 105);
        //this.Chart3.Legends.Add("Legend1");
        //this.Chart3.Legends[0].Enabled = true;
        //this.Chart3.Legends[0].Docking = Docking.Bottom;
        //this.Chart3.Legends[0].Alignment = System.Drawing.StringAlignment.Center;
        //this.Chart3.Series[0].LegendText = "#Value";
        //this.Chart3.DataManipulator.Sort(PointSortOrder.Descending, Chart2.Series[0]);
        Chart3.DataBind();
    }
    protected void btnEmp_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptEmpDownload.aspx");
    }
    protected void btnatt_Click(object sender, EventArgs e)
    {
        Response.Redirect("AttenanceDownload.aspx");
    }
    protected void btnLeave_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptLeaveSample.aspx");
    }
    protected void btnOT_Click(object sender, EventArgs e)
    {
        Response.Redirect("OTDownload.aspx");
    }
    protected void btncontract_Click(object sender, EventArgs e)
    {
        Response.Redirect("ContractRPT.aspx");
    }
    protected void btnSal_Click(object sender, EventArgs e)
    {
        Response.Redirect("FrmDeductionDownload.aspx");
    }
}
