﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.CrystalReports;
using CrystalDecisions.Web.Design;
using CrystalDecisions.Shared;

public partial class PayReports : System.Web.UI.Page
{
    string constr = ConfigurationManager.ConnectionStrings["PayrollConnectionString"].ConnectionString;
 //   CrystalDecisions.CrystalReports.Engine.ReportDocument rd;
    ReportDocument RdDocu;

    string server = "CBE-SERVER";
    string database = "Payroll";
    string user = "sa";
    string password = "Altius2005";
    string SessionAdmin;
    protected void Page_Load(object sender, EventArgs e)
    {
        SessionAdmin = Session["Isadmin"].ToString();
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        string ss = Session["UserId"].ToString();
    }
    protected void btnReport_Click(object sender, EventArgs e)
    {
        SqlConnection con = new SqlConnection(constr);
        string qry = "Select case Ed.Gender when 1 then 'Male' else  'Female'  end as Gender,Ed.EmpNo,Ed.EmpName,Ed.FatherName,MT.TalukNm, "+
                     " Convert(varchar(10),ED.Dob,103) as Dob,Ed.Psadd1,Ed.psadd2,MD.DistrictNm,Ed.PSState,Ed.Phone,Ed.Mobile, "+
                     " case Ed.MaritalStatus when 1 then 'UnMarried' else 'Married' end as MaritalStatus "+
                     " ,Ed.Nationality,Ed.University,Ed.CompanyName,Ed.Department,Ed.Designation,Ed.EmployeeType,Ed.Grade,MQ.QualificationNm,ME.EmpType from EmployeeDetails ED "+
                     " Join MstDistrict MD on Ed.PsDistrict=MD.DistrictCd "+
                     " join MstQualification MQ on MQ.QualificationCd=Ed.Qualification "+
                     " join MstEmployeeType ME on ME.EmpTypeCd=Ed.EmployeeType "+
                     " join MstTaluk MT on MT.TalukCd=Ed.PsTaluk and MD.Districtcd=MT.DistrictCd "+
                     " Order by Ed.EmpNO " +
                     "  ";
        SqlDataAdapter sda = new SqlDataAdapter(qry, con);
       // sda.SelectCommand = new SqlCommand(qry, con);
        PayDataSet payds=new PayDataSet();

        sda.Fill(payds, "PayDataTable");


        RdDocu = new ReportDocument();
        RdDocu.Load(Server.MapPath("PayCrystalReport.rpt"));
        RdDocu.SetDataSource(payds.Tables["PayDataTable"]);

        SetConnectionInfo(server, database, user, password);
        CrystalReportViewer1.ReportSource = RdDocu;
        CrystalReportViewer1.DataBind();

        //string str = Server.MapPath("PayCrystalReport.rpt");
        //rd = new CrystalDecisions.CrystalReports.Engine.ReportDocument();
        //rd.Load(str);
        //rd.SetDataSource(payds);

        //CrystalReportViewer1.ReportSource = rd;
        //CrystalReportViewer1.DisplayGroupTree = false;
        //CrystalReportViewer1.DisplayToolbar = true;
        //CrystalReportViewer1.DataBind();

        //CrystalReportViewer1.HasPrintButton = true;
        //CrystalReportViewer1.HasToggleGroupTreeButton = false;
        //CrystalReportViewer1.HasViewList = false;

    }

    public void SetConnectionInfo(string server, string database, string user, string password)
    {
        TableLogOnInfo logOnInfo = new TableLogOnInfo();

        logOnInfo = RdDocu.Database.Tables[0].LogOnInfo;
        ConnectionInfo connectionInfo = new ConnectionInfo();
        connectionInfo = logOnInfo.ConnectionInfo;
        connectionInfo.DatabaseName = database;
        connectionInfo.ServerName = server;
        connectionInfo.UserID = user;
        connectionInfo.Password = password;
        RdDocu.Database.Tables[0].ApplyLogOnInfo(logOnInfo);
    }
}
