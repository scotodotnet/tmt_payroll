﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Data.SqlClient;
using System.Configuration;

public partial class MstDepartment_1 : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    MastersClass objDep = new MastersClass();
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    static string lblprobation_Common;
    protected void Page_Load(object sender, EventArgs e)
    {
        
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        string ss = Session["UserId"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();
        if (!IsPostBack)
        {

            DepartmentGriddisplay();
        }
        //string ss = Session["UserId"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        lblusername.Text = Session["Usernmdisplay"].ToString();
        lblComany.Text = SessionCcode + " - " + SessionLcode;
    }
    public void DepartmentGriddisplay()
    {
        DataTable dtgv = new DataTable();
        dtgv = objdata.MstDepartmentGridDisplay();
        gvDepartment.DataSource = dtgv;
        gvDepartment.DataBind();
    }
    protected void btncancel_Click(object sender, EventArgs e)
    {
        txtdepartmentcd.Text = "";
        txtdepartmentnm.Text = "";
        DepartmentGriddisplay();
    }
    protected void btnsave_Click(object sender, EventArgs e)
    {
        try
        {
            //objDep.CompanyCd = ddlcompany.SelectedValue;
            bool ErrFlag = false;
            if (txtdepartmentcd.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Department Code');", true);
                //System.Windows.Forms.MessageBox.Show("Enter the Department Code", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            else if (txtdepartmentnm.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Department Name');", true);
                //System.Windows.Forms.MessageBox.Show("Enter the Department Name", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            if (!ErrFlag)
            {
                string dptcode = objdata.Departmentcode_check(txtdepartmentcd.Text);
                string dptname = objdata.DepartmentName_check(txtdepartmentnm.Text);
                if (dptcode.Trim() == txtdepartmentcd.Text.Trim())
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Department Code already Exist');", true);
                    //System.Windows.Forms.MessageBox.Show("Department Code Already Exist", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    ErrFlag = true;
                }
                if (dptname.Trim().ToLower() == txtdepartmentnm.Text.Trim().ToLower())
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Department Name already Exist');", true);
                    //System.Windows.Forms.MessageBox.Show("Department name Already Exist", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    ErrFlag = true;
                }
                if (!ErrFlag)
                {

                    objDep.DepartmentCd = txtdepartmentcd.Text;
                    objDep.DepartmentNm = txtdepartmentnm.Text;
                    objdata.MstDepartment(objDep);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Department Save Successfully');", true);
                    //System.Windows.Forms.MessageBox.Show("Department Saved Successfully", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    txtdepartmentcd.Text = "";
                    txtdepartmentnm.Text = "";
                    DepartmentGriddisplay();
                }

            }
        }
        catch (Exception ex)
        {

        }
    }
    protected void RowEdit_Canceling(object sender, GridViewCancelEditEventArgs e)
    {
        gvDepartment.EditIndex = -1;
        DepartmentGriddisplay();
    }
    protected void RowEdit_Canceling(object sender, GridViewEditEventArgs e)
    {
        gvDepartment.EditIndex = e.NewEditIndex;
        DepartmentGriddisplay();
    }
    protected void RowEdit_Updating(object sender, GridViewUpdateEventArgs e)
    {
        try
        {
            //objDep.CompanyCd = ddlcompany.SelectedValue;
            bool ErrFlag = false;
            TextBox txtdepartmentcd1 = (TextBox)gvDepartment.Rows[e.RowIndex].FindControl("txtdepartmentnm");
            if (txtdepartmentcd1.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Department Name');", true);
                ErrFlag = true;
            }

            if (!ErrFlag)
            {
                Label lbldepartmentcd1 = (Label)gvDepartment.Rows[e.RowIndex].FindControl("lbldepartmentcd");
                string dptname = objdata.DepartmentName_check(txtdepartmentcd1.Text);
                if (dptname.Trim().ToLower() == txtdepartmentcd1.Text.Trim().ToLower())
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Department Name already Exist');", true);
                    //System.Windows.Forms.MessageBox.Show("Department name Already Exist", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    ErrFlag = true;
                }
                if (!ErrFlag)
                {
                    objDep.DepartmentCd = lbldepartmentcd1.Text;
                    objDep.DepartmentNm = txtdepartmentcd1.Text;

                    objdata.UpdateMstDepartment(objDep);
                    gvDepartment.EditIndex = -1;
                    DepartmentGriddisplay();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Department Name Update Successfully');", true);
                    //System.Windows.Forms.MessageBox.Show("Department Name Updated Successfully", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                }
            }

        }
        catch (Exception ex)
        {

        }
    }
    protected void gvDepartment_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        bool ErrFlag = false;
        string constr = ConfigurationManager.AppSettings["ConnectionString"];
        Label lbldepartment = (Label)gvDepartment.Rows[e.RowIndex].FindControl("lbldepartmentcd");
        lblprobation_Common = lbldepartment.Text;

        string dptcode = objdata.Departmentcode_delete(lbldepartment.Text);
        if (dptcode.Trim() == lbldepartment.Text.Trim())
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Department cannot be deleted');", true);
            //System.Windows.Forms.MessageBox.Show("Department Cannot be Deleted", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            ErrFlag = true;
        }
        if (!ErrFlag)
        {
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "script", "showConfirm();", true);
            //SqlConnection con = new SqlConnection(constr);

            //System.Windows.Forms.DialogResult dres = System.Windows.Forms.MessageBox.Show("Are you Sure want to Delete ?", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.YesNo, System.Windows.Forms.MessageBoxIcon.Warning);
            //if (dres.ToString() == "Yes")
            //{
            //    string qry1 = "Delete from MstDepartment where DepartmentCd='" + lbldepartment.Text + "'";
            //    SqlCommand cmd1 = new SqlCommand(qry1, con);
            //    con.Open();
            //    cmd1.ExecuteNonQuery();
            //    con.Close();
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Deleted the Record....! ');", true);
            //    //System.Windows.Forms.MessageBox.Show("Deleted The Record", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            //    DepartmentGriddisplay();
            //}
        }
    }

    protected void cmdOverriteGrade_Click(object sender, EventArgs e)
    {
        try
        {
            string constr = ConfigurationManager.AppSettings["ConnectionString"];
            //Label lblbank = (Label)gvBank.Rows[e.RowIndex].FindControl("lblBankcd");
            SqlConnection con = new SqlConnection(constr);
            //System.Windows.Forms.DialogResult dres = System.Windows.Forms.MessageBox.Show("Are you Sure want to Delete ?", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.YesNo, System.Windows.Forms.MessageBoxIcon.Warning);
            //if (dres.ToString() == "Yes")

            string qry1 = "Delete from MstDepartment where DepartmentCd='" + lblprobation_Common + "'";
            SqlCommand cmd1 = new SqlCommand(qry1, con);
            con.Open();
            cmd1.ExecuteNonQuery();
            con.Close();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Deleted the Record....! ');", true);
            //System.Windows.Forms.MessageBox.Show("Deleted The Record", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            DepartmentGriddisplay();

        }
        catch (Exception ex)
        {
        }
    }
    protected void gvDepartment_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void btnEmp_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptEmpDownload.aspx");
    }
    protected void btnatt_Click(object sender, EventArgs e)
    {
        Response.Redirect("AttenanceDownload.aspx");
    }
    protected void btnLeave_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptLeaveSample.aspx");
    }
    protected void btnOT_Click(object sender, EventArgs e)
    {
        Response.Redirect("OTDownload.aspx");
    }
    protected void btncontract_Click(object sender, EventArgs e)
    {
        Response.Redirect("ContractRPT.aspx");
    }
    protected void btnSal_Click(object sender, EventArgs e)
    {
        Response.Redirect("FrmDeductionDownload.aspx");
    }
}
