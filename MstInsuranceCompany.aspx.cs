﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class MstInsuranceCompany_1 : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    MastersClass objInsu = new MastersClass();
    string SessionAdmin;
    static string lblprobation_Common;
    string SessionCcode;
    string SessionLcode;
    protected void Page_Load(object sender, EventArgs e)
    {
        
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        string ss = Session["UserId"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();
        if (!IsPostBack)
        {
            InsuranceDisplay();
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        lblComany.Text = SessionCcode + " - " + SessionLcode;
        lblusername.Text = Session["Usernmdisplay"].ToString();
    }
    public void InsuranceDisplay()
    {
        DataTable dt = new DataTable();
        dt = objdata.MstInsuranceDisplay();
        gvInsurance.DataSource = dt;
        gvInsurance.DataBind();


    }   
    protected void btnsave_Click(object sender, EventArgs e)
    {
        try
        {
            bool ErrFlag = false;
            if (txtinscompcd.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Insurance Company Code');", true);
                //System.Windows.Forms.MessageBox.Show("Enter the Insurance Company Code", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            else if (txtinsurancecompnm.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Insurance Company Name');", true);
                //System.Windows.Forms.MessageBox.Show("Enter Insurance Company Name", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            string constr = ConfigurationManager.AppSettings["ConnectionString"];
            SqlConnection con = new SqlConnection(constr);
            SqlCommand cmd = new SqlCommand("Select InsuranceCmpNm from MstInsurance Where InsuranceCmpNm='" + txtinsurancecompnm.Text + "'", con);
            con.Open();
            SqlDataReader sdr = cmd.ExecuteReader();
            if (sdr.HasRows)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('The Company Name All Ready Registered');", true);
                //System.Windows.Forms.MessageBox.Show("The Company name Already Registered", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            else
            {

            }
            con.Close();

            if (!ErrFlag)
            {
                string insid = objdata.Insuranceid_check(txtinscompcd.Text);
                string insname = objdata.Insurancename_check(txtinsurancecompnm.Text);
                if (insid == txtinscompcd.Text)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('The Company Code All Ready Registered');", true);
                    //System.Windows.Forms.MessageBox.Show("The Company Code Already registered", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    ErrFlag = true;
                }
                if (insname.ToLower() == txtinsurancecompnm.Text.ToLower())
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('The Company Name All Ready Registered');", true);
                    //System.Windows.Forms.MessageBox.Show("Company Name Already Exist", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    ErrFlag = true;
                }
                if (!ErrFlag)
                {
                    objInsu.InsuranceCmpCd = txtinscompcd.Text;
                    objInsu.InsuranceCmpNm = txtinsurancecompnm.Text;
                    objdata.MstInsurance(objInsu);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Insurance Company Save Successfully...');", true);
                    //System.Windows.Forms.MessageBox.Show("Insurance Company Save Successfully", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    txtinsurancecompnm.Text = "";
                    txtinscompcd.Text = "";
                    InsuranceDisplay();
                }
            }
        }
        catch (Exception ex)
        {

        }
    }
    protected void RowEdit_Canceling(object sender, GridViewCancelEditEventArgs e)
    {
        gvInsurance.EditIndex = -1;
        InsuranceDisplay();
    }
    protected void RowEdit_Editing(object sender, GridViewEditEventArgs e)
    {
        gvInsurance.EditIndex = e.NewEditIndex;
        InsuranceDisplay();
    }
    protected void RowEdit_Updating(object sender, GridViewUpdateEventArgs e)
    {
        try
        {
            bool ErrFlag = false;
            TextBox lblisname1 = (TextBox)gvInsurance.Rows[e.RowIndex].FindControl("txtinsurancempnm");

            if (lblisname1.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Insurance Company Name');", true);
                //System.Windows.Forms.MessageBox.Show("Enter Insurance Company Name", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            if (!ErrFlag)
            {
                Label lblins1 = (Label)gvInsurance.Rows[e.RowIndex].FindControl("lblinscompcd");
                string insname = objdata.Insurancename_check(lblisname1.Text);
                if (insname.ToLower() == lblisname1.Text.ToLower())
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('The Company Name All Ready Registered');", true);
                    //System.Windows.Forms.MessageBox.Show("The Company Name Already registered", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    ErrFlag = true;
                }
                if (!ErrFlag)
                {
                    objInsu.InsuranceCmpCd = lblins1.Text;
                    objInsu.InsuranceCmpNm = lblisname1.Text;
                    objdata.UpdateMstInsurance(objInsu);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Insurance Company Update Successfully...');", true);
                    //System.Windows.Forms.MessageBox.Show("Insurance Company Updated Successfully", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    gvInsurance.EditIndex = -1;
                    InsuranceDisplay();
                }
            }
        }
        catch (Exception ex)
        { }
    }
    protected void gvInsurance_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        bool ErrFlag = false;
        string constr = ConfigurationManager.AppSettings["ConnectionString"];
        SqlConnection con = new SqlConnection(constr);
        Label lblid = (Label)gvInsurance.Rows[e.RowIndex].FindControl("lblinscompcd");
        string insid = objdata.Insurance_delete(lblid.Text);
        lblprobation_Common = lblid.Text;
        if (insid == lblid.Text)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('It cannot be Deleted');", true);
            //System.Windows.Forms.MessageBox.Show("It cannot be deleted", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            ErrFlag = true;
        }
        if (!ErrFlag)
        {
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "script", "showConfirm();", true);
            //System.Windows.Forms.DialogResult dres = System.Windows.Forms.MessageBox.Show("Are you Sure want to Delete ?", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.YesNo, System.Windows.Forms.MessageBoxIcon.Warning);
            //if (dres.ToString() == "Yes")
            //{
            //    string qry = "Delete from MstInsurance where Insurancecmpcd = '" + lblid.Text + "'";
            //    SqlCommand cmd = new SqlCommand(qry, con);
            //    con.Open();
            //    cmd.ExecuteNonQuery();
            //    con.Close();
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Deleted Successfully');", true);
            //    //System.Windows.Forms.MessageBox.Show("Deleted Successfully", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            //    InsuranceDisplay();
            //}
        }
    }

    protected void cmdOverriteGrade_Click(object sender, EventArgs e)
    {
        try
        {
            string constr = ConfigurationManager.AppSettings["ConnectionString"];
            SqlConnection con = new SqlConnection(constr);
            //Label lblbank = (Label)gvBank.Rows[e.RowIndex].FindControl("lblBankcd");
            string qry = "Delete from MstInsurance where Insurancecmpcd = '" + lblprobation_Common + "'";
            SqlCommand cmd = new SqlCommand(qry, con);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Deleted Successfully');", true);
            //System.Windows.Forms.MessageBox.Show("Deleted Successfully", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            InsuranceDisplay();

        }
        catch (Exception ex)
        {
        }
    }

    protected void btncancel_Click(object sender, EventArgs e)
    {
        txtinsurancecompnm.Text = "";
        txtinscompcd.Text = "";
        InsuranceDisplay();
    }
}
