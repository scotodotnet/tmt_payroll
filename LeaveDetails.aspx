﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="LeaveDetails.aspx.cs" Inherits="LeaveDetails" %>

<%@ Register  Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<!-- Mirrored from www.ait.sk/uniadmin/ by HTTrack Website Copier/3.x [XR&CO'2010], Tue, 20 Jul 2010 00:37:25 GMT -->
<!--   Added by HTTrack -->
<meta http-equiv="content-type" content="text/html;charset=UTF-8">
<!-- /Added by HTTrack -->
<head id="hea" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <meta name="robots" content="ALL,FOLLOW" />
    <meta name="Author" content="AIT" />
    <meta http-equiv="imagetoolbar" content="no" />
    <title>Payroll Management Systems</title>
    <link rel="stylesheet" href="css/reset.css" type="text/css" />
    <link rel="stylesheet" href="css/screen.css" type="text/css" />
    <link rel="stylesheet" href="css/fancybox.css" type="text/css" />
    <link rel="stylesheet" href="css/jquery.wysiwyg.css" type="text/css" />
    <link rel="stylesheet" href="css/jquery.ui.css" type="text/css" />
    <link rel="stylesheet" href="css/visualize.css" type="text/css" />
    <link rel="stylesheet" href="css/visualize-light.css" type="text/css" />
    <link rel="Stylesheet" href="css/form.css" type="text/css" />
    <link rel="Stylesheet" href="css/orange.css" type="text/css" />
    <!--[if IE 7]>
	<link rel="stylesheet" type="text/css" href="css/ie7.css" />
<![endif]-->

    <script type="text/javascript" src="js/jquery.js"></script>

    <script type="text/javascript" src="js/jquery.visualize.js"></script>

    <script type="text/javascript" src="js/jquery.wysiwyg.js"></script>

    <script type="text/javascript" src="js/tiny_mce/jquery.tinymce.js"></script>

    <script type="text/javascript" src="js/jquery.fancybox.js"></script>

    <script type="text/javascript" src="js/jquery.idtabs.js"></script>

    <script type="text/javascript" src="js/jquery.datatables.js"></script>

    <script type="text/javascript" src="js/jquery.jeditable.js"></script>

    <script type="text/javascript" src="js/jquery.ui.js"></script>

    <script type="text/javascript" src="js/excanvas.js"></script>

    <script type="text/javascript" src="js/cufon.js"></script>

    <script type="text/javascript" src="js/Geometr231_Hv_BT_400.font.js"></script>

    <script type="text/javascript" src="js/script.js"></script>

</head>
<body>
    <div class="clear"> <!-- Start Clear-->
        <div class="sidebar">
            <!-- *** sidebar layout *** -->
            <div class="logo clear">
               <img src="images/altius_ForDarkBackGround.png" alt="" class="picture" width="140" height="60" />
        
            </div>
            
        
            
           <%--<div class="page clear">--%>
            <div class="Elaya">
                <ul >
                    <li><a href="EmployeeRegistration.aspx" >Home</a></li>
                    <li><a href="EmployeeRegistration.aspx"><span class="text">Employee Registration</span></a></li>
                    <li><a href="OfficalProfileDetails.aspx" ><span class="text">Official Profile </span></a></li>
                    <li><a href="LeaveDetails.aspx">Leave Details</a></li>
                    <li><a href="ApplyForLeave.aspx">Apply For Leave</a></li>
                    <li><a href="LeaveApproval.aspx">Leave Approval</a></li>
                    <li><a href="LeaveApprovalForStaff.aspx">Leave Approval For Staff</a></li>
                    <li><a href="SalaryRegistration.aspx">Salary Details</a></li>
                    <li><a href="MstEmployeeType.aspx">Masters</a></li>
                    <li><a href="#">Reports</a></li>
                     <li><a href="Default.aspx">Logout</a></li>
                    
                    
                </ul>
            </div>
            <%--</div>--%>
        </div>
        <div class="main"> <!-- Start Main -->
            <!-- *** mainpage layout *** -->
            <div class="main-wrap">  <!-- Start Main Warp -->
                <div class="header clear">
                    <ul class="links clear">
                        <li></li>
                        <li><h1><span class="title">Payroll Management System</span></h1></li>
                     
                    </ul>
                    
              
                   </div>
                    <!-- *** User Name layout *** -->
                    <div class="header1 clear">
                    <ul class="links1 clear">
                        
                        <li><h4><span class="title"> <asp:Label ID="lblusername" runat="server" ></asp:Label></span></h4></li>
                     
                    </ul>
                    
              
                   </div>
                  
                <div class="page clear"> <!-- Start page Clear -->
                 
                    <div class="main-icons clear">
                    <ul class="links clear">
			        <li></li>
			        <li><a href="#"><span class="text"></span></a></li>
			        <li><a href="#"><span class="text"> </span></a></li>
			        <li><a href="#"> <span class="text"></span></a></li>
			        <li><a href="#"> <span class="text"></span></a></li>
			        <li><a href="#"><span class="text"> </span></a></li>
        		
			        </ul>
                   
                       <%-- <ul class="clear">
                            <li><a href="EmployeeRegistration.aspx"> <img src="images/ico_folder_64.png" class="icon" alt="" /><span class="text">Employee Registration</span></a></li>
                             <li><a href="#"> <span class="text"></span></a></li>
                            <li><a href="OfficalProfileDetails.aspx"><img src="images/op2.jpg" class="icon" alt="" /><span class="text">Official Profile</span></a></li>
                            <li> <span class="text"></span></li>
                            <li><a href="LeaveDetails.aspx"><img src="images/date/calendar_week .png" class="icon" alt="" /><span class="text">Leave  Details</span></a></li>
                            <li><a href="#"> <span class="text"></span></a></li>
                            <li><a href="SalaryRegistration.aspx"><img src="images/rs5.jpg" class="icon" alt="" /><span class="text">Salary Details</span></a></li>
                            <li><a href="#"> <span class="text"></span></a></li>
                         
                                
                               
                        </ul>--%>
                    </div>
                    <!-- MODAL WINDOW -->
                    <div id="modal" class="modal-window">
                        <!-- <div class="modal-head clear"><a onclick="$.fancybox.close();" href="javascript:;" class="close-modal">Close</a></div> -->
                        <div class="notification note-info">
                            <a href="#" class="close" title="Close notification"><span>close</span></a> <span
                                class="icon"></span>
                        </div>
                    </div>
                    <!-- CONTENT BOXES -->
                    <div class="content-box">
                        <div class="box-header clear">
                        
                            <%--	<ul class="tabs clear">
						<li><a href="#data-table"></a></li>
						<li><a href="#table"></a></li>
						<li><a href="#forms"></a></li>
					</ul>--%>
                        </div>
                        <div class="box-body clear">
                            <!-- TABLE -->
                                                        
                            
<form id="form1" runat="server" class="form">
<cc1:ToolkitScriptManager runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true" ID="ScriptManager1" EnablePartialRendering="true"></cc1:ToolkitScriptManager>
<div id="homepage" class="main_content">
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
   
    <table align="center" cellpadding="0" cellspacing="0" border="0" >
    <tr>
    <td><asp:Label ID="lbldatemonth" runat="server" Text="Date"></asp:Label></td>
    <td><asp:TextBox ID="txtdatemonth" runat="server" 
            ontextchanged="txtdatemonth_TextChanged" ></asp:TextBox>
     <cc1:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtdatemonth" Format ="dd/MM/yyyy"  CssClass ="orange" Enabled="true"></cc1:CalendarExtender>
    </td>
    <td><asp:Button ID="btnclick" runat="server" Text="Click" 
            onclick="btnclick_Click" /></td>
    </tr>
    <tr>
    <td colspan="3"><asp:GridView ID="gvLeaveDetails" runat="server" 
            AutoGenerateColumns="false" onrowcancelingedit="RowEdit_Canceling" 
            onrowediting="RowEdit_Editing" onrowupdating="RowEdit_Updating">
    <Columns>
    <asp:TemplateField>
    <HeaderTemplate>Sl No</HeaderTemplate>
    <ItemTemplate><%# Container.DataItemIndex +1  %></ItemTemplate>
    </asp:TemplateField>
    <asp:TemplateField>
    <HeaderTemplate>EmpNo</HeaderTemplate>
    <ItemTemplate>
    <asp:Label ID="lblempno" runat="server" Text='<%# Eval("EmpNo") %>'></asp:Label>
    </ItemTemplate>
    </asp:TemplateField>
      <asp:TemplateField>
    <HeaderTemplate>Emp Name</HeaderTemplate>
    <ItemTemplate>
    <asp:Label ID="lblEmpname" runat="server" Text='<%# Eval("EmpName") %>'></asp:Label>
    </ItemTemplate>
    </asp:TemplateField>
      <asp:TemplateField>
    <HeaderTemplate>Department</HeaderTemplate>
    <ItemTemplate>
    <asp:Label ID="lbldepartment" runat="server" Text='<%# Eval("DepartmentNm") %>'></asp:Label>
    </ItemTemplate>
    </asp:TemplateField>
      <asp:TemplateField>
    <HeaderTemplate>Designation</HeaderTemplate>
    <ItemTemplate>
    <asp:Label ID="lbldesignation" runat="server" Text='<%# Eval("Designation") %>'></asp:Label>
    </ItemTemplate>
    </asp:TemplateField>
      <asp:TemplateField>
    <HeaderTemplate>Casual Leave</HeaderTemplate>
    <ItemTemplate>
    <asp:TextBox ID="txtcasualLeave" runat="server" Width="50" Text="0"  ></asp:TextBox>
    </ItemTemplate>
    </asp:TemplateField>
      <asp:TemplateField>
    <HeaderTemplate>Sick Leave</HeaderTemplate>
    <ItemTemplate> 
    <asp:TextBox ID="txtsickleave" runat="server" Width="50" Text="0" ></asp:TextBox>
    </ItemTemplate>
    </asp:TemplateField>
    <asp:TemplateField>
    <HeaderTemplate>Earned Leave</HeaderTemplate>
    <ItemTemplate>
    <asp:TextBox ID="txtearnedleave" runat="server" Width="50" Text="0" ></asp:TextBox>
    </ItemTemplate>
    </asp:TemplateField>
    <asp:TemplateField>
    <HeaderTemplate>Maternal Leave</HeaderTemplate>
    <ItemTemplate>
    <asp:TextBox ID="txtmaternalleave" runat="server" Width="50" Text="0" ></asp:TextBox>
    </ItemTemplate>
    </asp:TemplateField>
     <asp:CommandField ButtonType="Button" CancelText="Cancel" UpdateText="Update" EditText="Edit" ShowEditButton="true" />
    </Columns>
    </asp:GridView></td>
    </tr>
 <%--   <tr >
    <td><asp:Label ID="lblempno" runat="server" Text="Emp Code" Font-Bold="true" ></asp:Label></td>
    <td><asp:TextBox ID="txtempno" runat="server" CssClass="text"></asp:TextBox></td>
    <td><asp:Label ID="lblempname" runat="server" Text="Emp Name" Font-Bold="true" ></asp:Label></td>
    <td><asp:Label ID="lblempname1" runat="server" Font-Bold="true"></asp:Label></td>
    <td><asp:Button ID="btnsearch" runat="server" Text="Search" 
            onclick="btnsearch_Click" /> </td>
    </tr>
      <tr >
    <td colspan="5"><asp:Image ID="Image1" runat="server" ImageUrl="~/Images/citybg.gif" Width="700px" Height="5px" /></td>
    </tr>
    <tr >
    
     <td><asp:Label ID="lbldate" runat="server" Text="Enter Date" Font-Bold="true"></asp:Label></td>
    <td >
        <asp:TextBox ID="txtdate" runat="server" 
            ontextchanged="txtdate_TextChanged" AutoPostBack="True" ></asp:TextBox>
        <cc1:CalendarExtender ID="Caleder_txtdob" runat="server" TargetControlID="txtdate" Format ="dd/MM/yyyy"  CssClass ="orange" Enabled="true"></cc1:CalendarExtender>

    </td>
    <td><asp:Label ID="lbltotalwork" runat="server" Text="Total Working Days" Font-Bold="true"></asp:Label></td>
    <td colspan="2"><asp:TextBox ID="txttotalworkingdy" runat="server" 
            AutoPostBack="True" Enabled="False" ></asp:TextBox></td>
   
    </tr >
    <tr >
    <td><asp:Label ID="lblleavetype" runat="server" Text="Leave Type1" Font-Bold="true"></asp:Label></td>
    <td><asp:DropDownList ID="ddlleavetype" runat="server"  Width="150" Height="30" ></asp:DropDownList></td>
    <td><asp:Label ID="lblnoofleave" runat="server" Text="Number of Leave1" Font-Bold="true"></asp:Label></td>
    <td colspan="2"><asp:TextBox ID="txtnoofleave" runat="server" ></asp:TextBox></td>
    </tr >
    <tr >
    <td><asp:Label ID="Label1" runat="server" Text="Leave Type2" Font-Bold="true"></asp:Label></td>
    <td><asp:DropDownList ID="ddlleavetype1" runat="server"  Width="150" Height="30" ></asp:DropDownList></td>
    <td><asp:Label ID="Label2" runat="server" Text="Number of Leave2" Font-Bold="true"></asp:Label></td>
    <td colspan="2"><asp:TextBox ID="txtnoofleave2" runat="server" ></asp:TextBox></td>
    </tr>
    <tr >
    <td><asp:Label ID="Label3" runat="server" Text="Leave Type3" Font-Bold="true"></asp:Label></td>
    <td><asp:DropDownList ID="ddlleavetype2" runat="server" CssClass="Drp" Width="150" Height="30"></asp:DropDownList></td>
    <td><asp:Label ID="Label4" runat="server" Text="Number of Leave3" Font-Bold="true"></asp:Label></td>
    <td colspan="2"><asp:TextBox ID="txtnofleave3" runat="server" ></asp:TextBox></td>
    </tr>
    <tr >
    <td colspan="5"><asp:Button ID="btnsave" runat="server" Text="Save" onclick="btnsave_Click" /></td>
    </tr>--%>
    </table>
         </ContentTemplate>
        </asp:UpdatePanel>
</div>
</form>
</div>
                    </div>
                </div> <!-- end of page Clear -->
                <!-- end of page -->
                <div class="footer clear">
                    <span class="copy"><strong>© 2012 Copyright by <a href="http://www.altius.co.in" />Altius Infosystems.
                    </strong></span> Powered by <a href="http://www.altius.co.in/">Altius.</a>
                </div>
            </div> <!-- end of MainWarp -->
        </div>  <!-- end of Main -->
    </div> <!-- end of Clear -->

    <script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-12958851-7']);
        _gaq.push(['_trackPageview']);

        (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();

    </script>

</body>
<!-- Mirrored from www.ait.sk/uniadmin/ by HTTrack Website Copier/3.x [XR&CO'2010], Tue, 20 Jul 2010 00:38:01 GMT -->
<!-- Added by HTTrack -->
<meta http-equiv="content-type" content="text/html;charset=UTF-8">
<!-- /Added by HTTrack -->
</html>
