﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using System.Data;

using Payroll;
using Altius.BusinessAccessLayer.BALDataAccess;
public partial class ApplyForLeave_1 : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    System.Globalization.CultureInfo culterInfo = new System.Globalization.CultureInfo("en-GB");
    int mon = 0;
    string Mont = "";
    string SessionAdmin;
    string stafflabour_temp;
    string tempdateForm;
    string tempDateto;
    string transId;
    string SessionCcode;
    string SessionLcode;
    protected void Page_Load(object sender, EventArgs e)
    {

        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        if (Request.QueryString["edit"] != null)
        {
            PanelEditPFGrade.Visible = true;
            ddlcategory.Enabled = false;
            ddldepartment.Enabled = false;
            ddlempno.Enabled = false;
            ddlempname.Enabled = false;
            txtexistingNo.Enabled = false;
            btnsearch.Enabled = false;
            btnexitingSearch.Enabled = false;
            btnback.Visible = true;
            btndel.Visible = true;


        }
        string ss = Session["UserId"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        lblComany.Text = SessionCcode + " - " + SessionLcode;
        if (!IsPostBack)
        {
            DropDownLeaveDisplay();
            //Department();
            DropDwonCategory();
            Loadgrid();
            int currentYear = Utility.GetFinancialYear;
            for (int i = 0; i < 10; i++)
            {
                ddlFinaYear.Items.Add(new ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                //  ddlShowYear.Items.Add(new ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                currentYear = currentYear - 1;
            }

        }
        lblusername.Text = Session["Usernmdisplay"].ToString();
    }
    public void Loadgrid()
    {
        DataTable dt = objdata.Loadgrid_leaveApply(SessionCcode, SessionLcode);
        GvPFGradeData.DataSource = dt;
        GvPFGradeData.DataBind();
    }
    public void DropDownLeaveDisplay()
    {
        DataTable dt = new DataTable();
        dt = objdata.DropDwonLeaveType();
        ddlleavetype.DataSource = dt;
        ddlleavetype.DataTextField = "LeaveType";
        ddlleavetype.DataValueField = "LeaveCd";
        ddlleavetype.DataBind();
    }
    public void DropDwonCategory()
    {
        DataTable dtcate = new DataTable();
        dtcate = objdata.DropDownCategory();
        ddlcategory.DataSource = dtcate;
        ddlcategory.DataTextField = "CategoryName";
        ddlcategory.DataValueField = "CategoryCd";
        ddlcategory.DataBind();
    }
    public void Department()
    {
        DataTable dtDip = new DataTable();
        dtDip = objdata.DropDownDepartment();
        ddldepartment.DataSource = dtDip;
        ddldepartment.DataTextField = "DepartmentNm";
        ddldepartment.DataValueField = "DepartmentCd";
        ddldepartment.DataBind();
    }
    public void clear()
    {

        txtfromdate.Text = "";
        txttodate.Text = "";

        lbldesigdis.Text = "";
        lbljoindis.Text = "";
        lblleaveallocated.Text = "";
        lblOutputleaves.Text = "";
        lblleaveexceeded.Text = "";
        ddlleavetype.SelectedValue = "0";
        txtapplyleave.Text = "";
        txtapprovedbye.Text = "";
        txtapproveddate.Text = "";
        txtexistingNo.Text = "";
        lbltrans.Text = "";
        txtfromdate.Enabled = true;
        panelError.Visible = false;
        panelsuccess.Visible = false;
        panelconfirm.Visible = false;
        lblError.Text = "";
        lbloutput.Text = "";
        lblError.Text = "";
        lbloutput.Text = "";
        btnclick.Enabled = true;
        btnexitingSearch.Enabled = true;
        btnsearch.Enabled = true;
        btnLeave.Enabled = true;
        btnapply.Enabled = true;
        btnEdit.Enabled = true;
        btnupload.Enabled = true;
        btnback.Enabled = true;
        btnclear.Enabled = true;
        btndel.Enabled = true;
        Button1.Enabled = true;
        GvPFGradeData.Enabled = true;
    }
    public void Month()
    {

        switch (mon)
        {
            case 1:
                Mont = "January";
                break;

            case 2:
                Mont = "February";
                break;
            case 3:
                Mont = "March";
                break;
            case 4:
                Mont = "April";
                break;
            case 5:
                Mont = "May";
                break;
            case 6:
                Mont = "June";
                break;
            case 7:
                Mont = "July";
                break;
            case 8:
                Mont = "August";
                break;
            case 9:
                Mont = "September";
                break;
            case 10:
                Mont = "October";
                break;
            case 11:
                Mont = "November";
                break;
            case 12:
                Mont = "December";
                break;
            default:
                break;
        }

    }
    protected void ddlcategory_SelectedIndexChanged(object sender, EventArgs e)
    {

        string staffLabour_temp;
        DataTable dtempty = new DataTable();
        ddldepartment.DataSource = dtempty;
        ddldepartment.DataBind();
        if (ddlcategory.SelectedValue == "1")
        {
            staffLabour_temp = "S";
        }
        else if (ddlcategory.SelectedValue == "2")
        {
            staffLabour_temp = "L";
        }
        else
        {
            staffLabour_temp = "0";
        }
        DataTable dtDip = new DataTable();
        dtDip = objdata.DropDownDepartment_category(staffLabour_temp);
        if (dtDip.Rows.Count > 1)
        {
            ddldepartment.DataSource = dtDip;
            ddldepartment.DataTextField = "DepartmentNm";
            ddldepartment.DataValueField = "DepartmentCd";
            ddldepartment.DataBind();
        }
        txtexistingNo.Text = "";
        ddlempno.DataSource = dtempty;
        ddlempno.DataBind();
        ddlempname.DataSource = dtempty;
        ddlempname.DataBind();
        clear();
    }
    protected void ddldepartment_SelectedIndexChanged(object sender, EventArgs e)
    {
        //bool ErrFlag=false;
        //string Staff="";
        //if (ddlcategory.SelectedValue == "")
        //{
        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Employee Number');", true);
        //    ErrFlag = true;
        //}
        //if(!ErrFlag)
        //{
        //    if (ddlcategory.SelectedValue == "1")
        //    {
        //        Staff="S";
        //    }
        //    else if (ddlcategory.SelectedValue == "2")
        //    {
        //        Staff="L";
        //    }
        //    DataTable dtemp = new DataTable();
        //    int dd = Convert.ToInt32(ddldepartment.SelectedValue);
        //    dtemp = objdata.EmployeeNoandName(Staff, dd);
        //    DataRow dr = dtemp.NewRow();
        //    dr["EmpNo"] = ddldepartment.SelectedItem.Text;
        //    dr["EmpName"] = ddldepartment.SelectedItem.Text;
        //    dtemp.Rows.InsertAt(dr, 0);

        //    ddlempno.DataSource = dtemp;
        //    ddlempno.DataTextField = "EmpNo";
        //    ddlempno.DataValueField = "EmpNo";
        //    ddlempno.DataBind();

        //    ddlempname.DataSource = dtemp;
        //    ddlempname.DataTextField = "EmpName";
        //    ddlempname.DataValueField = "EmpNo";
        //    ddlempname.DataBind();
        //}
        DataTable dtempty = new DataTable();
        ddlempno.DataSource = dtempty;
        ddlempno.DataBind();
        ddlempname.DataSource = dtempty;
        ddlempname.DataBind();
        clear();
    }
    protected void btnclick_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        //panelError.Visible = true;
        string Staff = "";
        if (ddlcategory.SelectedValue == "0")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Category');", true);
            //System.Windows.Forms.MessageBox.Show("Select the Category", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            //lblError.Text = "Select the Category";
            ErrFlag = true;
        }
        else if (ddldepartment.SelectedValue == "0")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Department');", true);
            //System.Windows.Forms.MessageBox.Show("Select the Department", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            //lblError.Text = "Select the Department";
            ErrFlag = true;
        }
        if (!ErrFlag)
        {
            lblError.Text = "";
            if (ddlcategory.SelectedValue == "1")
            {
                Staff = "S";
            }
            else if (ddlcategory.SelectedValue == "2")
            {
                Staff = "L";
            }
            DataTable dtemp = new DataTable();
            string dd = ddldepartment.SelectedValue;
            if (SessionAdmin == "1")
            {
                dtemp = objdata.EmployeeNoandName(Staff, dd, SessionCcode, SessionLcode);
            }
            else
            {
                dtemp = objdata.EmployeeNoandName_user(Staff, dd, SessionCcode, SessionLcode);
            }
            ddlempno.DataSource = dtemp;
            DataRow dr = dtemp.NewRow();
            dr["EmpNo"] = ddldepartment.SelectedItem.Text;
            dr["EmpName"] = ddldepartment.SelectedItem.Text;
            dtemp.Rows.InsertAt(dr, 0);

            ddlempno.DataTextField = "EmpNo";
            ddlempno.DataValueField = "EmpNo";
            ddlempno.DataBind();

            ddlempname.DataSource = dtemp;
            ddlempname.DataTextField = "EmpName";
            ddlempname.DataValueField = "EmpNo";
            ddlempname.DataBind();
        }
    }
    protected void btnexitingSearch_Click(object sender, EventArgs e)
    {
        //panelError.Visible = true;
        bool ErrFlag = false;
        if (ddlcategory.SelectedValue == "0")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select Category on Staff or Labour');", true);
            //System.Windows.Forms.MessageBox.Show("Select the Category", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            //lblError.Text = "Select the Category";
            ErrFlag = true;
        }
        else if (ddldepartment.SelectedValue == "0")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select Department');", true);
            //System.Windows.Forms.MessageBox.Show("Select the Department", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            //lblError.Text = "Select the Department";
            ErrFlag = true;
        }
        else if (txtexistingNo.Text.Trim() == "")
        {
            lblError.Text = "Enter the Existing Code";
            ErrFlag = true;
        }
        if (!ErrFlag)
        {
            lblError.Text = "";
            if (ddlcategory.SelectedValue == "1")
            {
                string Cate = "S";
                DataTable dtempcode = new DataTable();
                if (SessionAdmin == "1")
                {
                    dtempcode = objdata.SalaryMasterLabour_ForSearchingExisiting(ddldepartment.SelectedValue, txtexistingNo.Text, Cate, SessionCcode, SessionLcode, SessionAdmin);
                }
                else
                {
                    dtempcode = objdata.SalaryMasterLabour_ForSearchingExisiting_user(ddldepartment.SelectedValue, txtexistingNo.Text, Cate, SessionCcode, SessionLcode);
                }
                if (dtempcode.Rows.Count > 0)
                {
                    //ddlempno.DataSource = dtempcode;
                    //ddlempno.DataTextField = "EmpNo";
                    //ddlempno.DataValueField = "EmpNo";
                    //ddlempno.DataBind();
                    ddlempno.SelectedValue = dtempcode.Rows[0]["EmpNo"].ToString();
                    ddlempname.DataSource = dtempcode;
                    ddlempname.DataTextField = "EmpName";
                    ddlempname.DataValueField = "EmpNo";
                    ddlempname.DataBind();
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Employee not found');", true);
                    //System.Windows.Forms.MessageBox.Show("Employee Not Found", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);

                    ErrFlag = true;
                    clear();
                    //panelError.Visible = true;
                    //lblError.Text = "Employee Not Found";
                    stafflabour_temp = "S";
                    DataTable dtemp = new DataTable();
                    if (SessionAdmin == "1")
                    {
                        dtemp = objdata.EmployeeNoandName(stafflabour_temp, ddldepartment.SelectedValue, SessionCcode, SessionLcode);
                    }
                    else
                    {
                        dtemp = objdata.EmployeeNoandName_user(stafflabour_temp, ddldepartment.SelectedValue, SessionCcode, SessionLcode);
                    }
                    ddlempno.DataSource = dtemp;
                    DataRow dr = dtemp.NewRow();
                    dr["EmpNo"] = ddldepartment.SelectedItem.Text;
                    dr["EmpName"] = ddldepartment.SelectedItem.Text;
                    dtemp.Rows.InsertAt(dr, 0);

                    ddlempno.DataTextField = "EmpNo";
                    ddlempno.DataValueField = "EmpNo";
                    ddlempno.DataBind();

                    ddlempname.DataSource = dtemp;
                    ddlempname.DataTextField = "EmpName";
                    ddlempname.DataValueField = "EmpNo";
                    ddlempname.DataBind();

                }
                #region Staff Search
                if (!ErrFlag)
                {
                    if (ddlcategory.SelectedValue == "1")
                    {
                        DataTable dt = new DataTable();
                        dt = objdata.ApplyForLeaveinEmployee(ddlempno.SelectedValue, ddlFinaYear.SelectedValue, SessionCcode, SessionLcode);
                        if (dt.Rows.Count > 0)
                        {

                            lbldesigdis.Text = dt.Rows[0]["Designation"].ToString();
                            lbljoindis.Text = dt.Rows[0]["Dateofjoining"].ToString();
                            lblleaveallocated.Text = dt.Rows[0]["AllocatedLeave"].ToString();
                            txtexistingNo.Text = dt.Rows[0]["ExisistingCode"].ToString();
                            lblleaveexceeded.Text = "0";

                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Allocat the Leave');", true);
                            //System.Windows.Forms.MessageBox.Show("Allocate the Leave", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                            //lblError.Text = "Allocat Leave";
                            lbldesigdis.Text = "";
                            lbljoindis.Text = "";
                            lblleaveallocated.Text = "";
                        }
                        DataTable dt1 = new DataTable();
                        dt1 = objdata.AlreadyTakenLeave(ddlempno.SelectedItem.Text, ddlFinaYear.SelectedValue, SessionCcode, SessionLcode);
                        if (dt1.Rows.Count > 0)
                        {
                            lblOutputleaves.Text = dt1.Rows[0]["Leave"].ToString();
                            lblleaveexceeded.Text = dt1.Rows[0]["ExcessLeave"].ToString();
                        }
                        // lblleaveexceeded.Text = "";

                    }
                    else if (ddlcategory.SelectedValue == "2")
                    {
                        bool RegularSearch = false;
                        string RegulatLabour = objdata.RegularLabore(ddlempno.SelectedValue, SessionCcode, SessionLcode);
                        string Regul = RegulatLabour.Replace(" ", "");
                        if (Regul == "Regular")
                        {
                            DataTable dt = new DataTable();
                            dt = objdata.ApplyForLeaveinEmployee(ddlempno.SelectedValue, ddlFinaYear.SelectedValue, SessionCcode, SessionLcode);
                            if (dt.Rows.Count > 0)
                            {

                                lbldesigdis.Text = dt.Rows[0]["Designation"].ToString();
                                lbljoindis.Text = dt.Rows[0]["Dateofjoining"].ToString();
                                lblleaveallocated.Text = dt.Rows[0]["AllocatedLeave"].ToString();
                                txtexistingNo.Text = dt.Rows[0]["ExisistingCode"].ToString();
                                lblleaveexceeded.Text = "0";

                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Allocat the Leave');", true);
                                //System.Windows.Forms.MessageBox.Show("Allocate the Leave", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                //lblError.Text = "Allocate Leave";
                                lbldesigdis.Text = "";
                                lbljoindis.Text = "";
                                lblleaveallocated.Text = "";
                            }
                            DataTable dt1 = new DataTable();
                            dt1 = objdata.AlreadyTakenLeave(ddlempno.SelectedItem.Text, ddlFinaYear.SelectedValue, SessionCcode, SessionLcode);
                            if (dt1.Rows.Count > 0)
                            {
                                lblOutputleaves.Text = dt1.Rows[0]["Leave"].ToString();
                                lblleaveexceeded.Text = dt1.Rows[0]["ExcessLeave"].ToString();
                            }
                            RegularSearch = true;
                            // lblleaveexceeded.Text = "";
                        }

                        if (!RegularSearch)
                        {

                            DataTable dt = new DataTable();
                            dt = objdata.ApplyForLeaveinLabour_1(ddlempno.SelectedValue, ddlFinaYear.SelectedValue, SessionCcode, SessionLcode);
                            if (dt.Rows.Count > 0)
                            {

                                lbldesigdis.Text = dt.Rows[0]["Designation"].ToString();
                                lbljoindis.Text = dt.Rows[0]["Dateofjoining"].ToString();
                                lblleaveallocated.Text = "0";
                                lblleaveexceeded.Text = "0";

                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Official Profile Not Registered Your Employee Number');", true);
                                //System.Windows.Forms.MessageBox.Show("Official Profile Not registered your Employee Number", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                //lblError.Text = "Employee details not Found in Official Profile";
                                lbldesigdis.Text = "";
                                lbljoindis.Text = "";
                                lblleaveallocated.Text = "";
                            }
                            DataTable dt1 = new DataTable();
                            dt1 = objdata.AlreadyTakenLeaveForLabour(ddlempno.SelectedItem.Text, ddlFinaYear.SelectedValue, SessionCcode, SessionLcode);
                            if (dt1.Rows.Count > 0)
                            {
                                lblError.Text = "";
                                //  lblOutputleaves.Text = dt1.Rows[0]["Leave"].ToString();
                                lblleaveexceeded.Text = dt1.Rows[0]["ExcessLeave"].ToString();
                            }
                        }
                    }
                }
                #endregion
            }
            else if (ddlcategory.SelectedValue == "2")
            {
                string Cate = "L";
                DataTable dtempcode = new DataTable();
                if (SessionAdmin == "1")
                {
                    dtempcode = objdata.SalaryMasterLabour_ForSearchingExisiting(ddldepartment.SelectedValue, txtexistingNo.Text, Cate, SessionCcode, SessionLcode, SessionAdmin);
                }
                else
                {
                    dtempcode = objdata.SalaryMasterLabour_ForSearchingExisiting_user(ddldepartment.SelectedValue, txtexistingNo.Text, Cate, SessionCcode, SessionLcode);
                }
                if (dtempcode.Rows.Count > 0)
                {
                    //ddlempno.DataSource = dtempcode;
                    //ddlempno.DataTextField = "EmpNo";
                    //ddlempno.DataValueField = "EmpNo";
                    //ddlempno.DataBind();
                    ddlempno.SelectedValue = dtempcode.Rows[0]["EmpNo"].ToString();
                    ddlempname.DataSource = dtempcode;
                    ddlempname.DataTextField = "EmpName";
                    ddlempname.DataValueField = "EmpNo";
                    ddlempname.DataBind();
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Employee not Found');", true);
                    //System.Windows.Forms.MessageBox.Show("Employee Not Found", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    //lblError.Text = "Employee Not Found";
                    ErrFlag = true;
                    clear();
                    stafflabour_temp = "L";
                    DataTable dtemp = new DataTable();
                    if (SessionAdmin == "1")
                    {
                        dtemp = objdata.EmployeeNoandName(stafflabour_temp, ddldepartment.SelectedValue, SessionCcode, SessionLcode);
                    }
                    else
                    {
                        dtemp = objdata.EmployeeNoandName_user(stafflabour_temp, ddldepartment.SelectedValue, SessionCcode, SessionLcode);
                    }
                    ddlempno.DataSource = dtemp;
                    DataRow dr = dtemp.NewRow();
                    dr["EmpNo"] = ddldepartment.SelectedItem.Text;
                    dr["EmpName"] = ddldepartment.SelectedItem.Text;
                    dtemp.Rows.InsertAt(dr, 0);

                    ddlempno.DataTextField = "EmpNo";
                    ddlempno.DataValueField = "EmpNo";
                    ddlempno.DataBind();

                    ddlempname.DataSource = dtemp;
                    ddlempname.DataTextField = "EmpName";
                    ddlempname.DataValueField = "EmpNo";
                    ddlempname.DataBind();
                }
                #region Labour search
                if (!ErrFlag)
                {
                    if (ddlcategory.SelectedValue == "1")
                    {
                        DataTable dt = new DataTable();
                        dt = objdata.ApplyForLeaveinEmployee(ddlempno.SelectedValue, ddlFinaYear.SelectedValue, SessionCcode, SessionLcode);
                        if (dt.Rows.Count > 0)
                        {

                            lbldesigdis.Text = dt.Rows[0]["Designation"].ToString();
                            lbljoindis.Text = dt.Rows[0]["Dateofjoining"].ToString();
                            lblleaveallocated.Text = dt.Rows[0]["AllocatedLeave"].ToString();
                            txtexistingNo.Text = dt.Rows[0]["ExisistingCode"].ToString();
                            lblleaveexceeded.Text = "0";

                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Allocat the Leave');", true);
                            //System.Windows.Forms.MessageBox.Show("Allocate the Leave", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                            //lblError.Text = "Allocate the Leave";
                            lbldesigdis.Text = "";
                            lbljoindis.Text = "";
                            lblleaveallocated.Text = "";
                        }
                        DataTable dt1 = new DataTable();
                        dt1 = objdata.AlreadyTakenLeave(ddlempno.SelectedItem.Text, ddlFinaYear.SelectedValue, SessionCcode, SessionLcode);
                        if (dt1.Rows.Count > 0)
                        {
                            lblOutputleaves.Text = dt1.Rows[0]["Leave"].ToString();
                            lblleaveexceeded.Text = dt1.Rows[0]["ExcessLeave"].ToString();
                        }
                        // lblleaveexceeded.Text = "";

                    }
                    else if (ddlcategory.SelectedValue == "2")
                    {
                        bool RegularSearch = false;
                        string RegulatLabour = objdata.RegularLabore(ddlempno.SelectedValue, SessionCcode, SessionLcode);
                        string Regul = RegulatLabour.Replace(" ", "");
                        if (Regul == "Regular")
                        {
                            DataTable dt = new DataTable();
                            dt = objdata.ApplyForLeaveinEmployee(ddlempno.SelectedValue, ddlFinaYear.SelectedValue, SessionCcode, SessionLcode);
                            if (dt.Rows.Count > 0)
                            {

                                lbldesigdis.Text = dt.Rows[0]["Designation"].ToString();
                                lbljoindis.Text = dt.Rows[0]["Dateofjoining"].ToString();
                                lblleaveallocated.Text = dt.Rows[0]["AllocatedLeave"].ToString();
                                txtexistingNo.Text = dt.Rows[0]["ExisistingCode"].ToString();
                                lblleaveexceeded.Text = "0";

                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Allocat the Leave');", true);
                                //System.Windows.Forms.MessageBox.Show("Allocate the Leave", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                //lblError.Text = "Allocate the Leave";
                                lbldesigdis.Text = "";
                                lbljoindis.Text = "";
                                lblleaveallocated.Text = "";
                            }
                            DataTable dt1 = new DataTable();
                            dt1 = objdata.AlreadyTakenLeave(ddlempno.SelectedItem.Text, ddlFinaYear.SelectedValue, SessionCcode, SessionLcode);
                            if (dt1.Rows.Count > 0)
                            {
                                lblOutputleaves.Text = dt1.Rows[0]["Leave"].ToString();
                                lblleaveexceeded.Text = dt1.Rows[0]["ExcessLeave"].ToString();
                            }
                            RegularSearch = true;
                            // lblleaveexceeded.Text = "";
                        }

                        if (!RegularSearch)
                        {

                            DataTable dt = new DataTable();
                            dt = objdata.ApplyForLeaveinLabour_1(ddlempno.SelectedValue, ddlFinaYear.SelectedValue, SessionCcode, SessionLcode);
                            if (dt.Rows.Count > 0)
                            {

                                lbldesigdis.Text = dt.Rows[0]["Designation"].ToString();
                                lbljoindis.Text = dt.Rows[0]["Dateofjoining"].ToString();
                                lblleaveallocated.Text = "0";
                                lblleaveexceeded.Text = "0";

                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Official Profile Not Registered Your Employee Number');", true);
                                //System.Windows.Forms.MessageBox.Show("Official Profile Not Registered Your Employee Number", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                //lblError.Text = "Employee Details not Found in Official Profile";
                                lbldesigdis.Text = "";
                                lbljoindis.Text = "";
                                lblleaveallocated.Text = "";
                            }
                            DataTable dt1 = new DataTable();
                            dt1 = objdata.AlreadyTakenLeaveForLabour(ddlempno.SelectedItem.Text, ddlFinaYear.SelectedValue, SessionCcode, SessionLcode);
                            if (dt1.Rows.Count > 0)
                            {
                                lblError.Text = "";
                                //  lblOutputleaves.Text = dt1.Rows[0]["Leave"].ToString();
                                lblleaveexceeded.Text = dt1.Rows[0]["ExcessLeave"].ToString();
                            }
                        }
                    }
                }
                #endregion

            }
        }
    }
    protected void ddlempno_SelectedIndexChanged(object sender, EventArgs e)
    {
        lblError.Text = "";
        DataTable dtemp = new DataTable();
        dtemp = objdata.DropDownEmployeeName(ddlempno.SelectedValue, SessionCcode, SessionLcode);
        ddlempname.DataSource = dtemp;
        ddlempname.DataTextField = "EmpName";
        ddlempname.DataValueField = "EmpNo";
        ddlempname.DataBind();
        clear();
    }
    protected void ddlempname_SelectedIndexChanged(object sender, EventArgs e)
    {
        lblError.Text = "";
        DataTable dtemp = new DataTable();
        dtemp = objdata.DropDownEmployeeNumber(ddlempname.SelectedItem.Text, SessionCcode, SessionLcode);
        ddlempno.DataSource = dtemp;
        ddlempno.DataTextField = "EmpNo";
        ddlempno.DataValueField = "EmpNo";
        ddlempno.DataBind();
        clear();
    }
    protected void btnsearch_Click1(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        //panelError.Visible = true;
        if (ddlcategory.SelectedValue == "0")
        {
            lblError.Text = "Select the Category";
            ErrFlag = true;
        }
        else if (ddldepartment.SelectedValue == "")
        {
            lblError.Text = "Select the Department";
            ErrFlag = true;
        }
        else if (ddldepartment.SelectedValue == "0")
        {
            lblError.Text = "Select the Department";
            ErrFlag = true;
        }
        if (!ErrFlag)
        {
            if (ddlempno.SelectedItem.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Employee Number');", true);
                //System.Windows.Forms.MessageBox.Show("Enter the Employee Number", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                //lblError.Text = "Enter the Employee No.";
                ErrFlag = true;
            }
            if (!ErrFlag)
            {
                if (ddlcategory.SelectedValue == "1")
                {
                    lblError.Text = "";
                    DataTable dt = new DataTable();
                    dt = objdata.ApplyForLeaveinEmployee(ddlempno.SelectedValue, ddlFinaYear.SelectedValue, SessionCcode, SessionLcode);
                    if (dt.Rows.Count > 0)
                    {

                        lbldesigdis.Text = dt.Rows[0]["Designation"].ToString();
                        lbljoindis.Text = dt.Rows[0]["Dateofjoining"].ToString();
                        lblleaveallocated.Text = "0";
                        txtexistingNo.Text = dt.Rows[0]["ExisistingCode"].ToString();
                        lblleaveexceeded.Text = "0";

                    }
                    else
                    {
                        //lblError.Text = "Allocate the Leave";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Allocat the Leave');", true);
                        //System.Windows.Forms.MessageBox.Show("Allocate the Leave", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                        lbldesigdis.Text = "";
                        lbljoindis.Text = "";
                        lblleaveallocated.Text = "";
                    }
                    DataTable dt1 = new DataTable();
                    dt1 = objdata.AlreadyTakenLeave(ddlempno.SelectedItem.Text, ddlFinaYear.SelectedValue, SessionCcode, SessionLcode);
                    if (dt1.Rows.Count > 0)
                    {
                        lblOutputleaves.Text = dt1.Rows[0]["Leave"].ToString();
                        lblleaveexceeded.Text = dt1.Rows[0]["ExcessLeave"].ToString();
                    }
                    // lblleaveexceeded.Text = "";

                }
                else if (ddlcategory.SelectedValue == "2")
                {
                    bool RegularSearch = false;
                    string RegulatLabour = objdata.RegularLabore(ddlempno.SelectedValue, SessionCcode, SessionLcode);
                    string Regul = RegulatLabour.Replace(" ", "");
                    if (Regul == "Regular")
                    {
                        lblError.Text = "";
                        DataTable dt = new DataTable();
                        dt = objdata.ApplyForLeaveinEmployee(ddlempno.SelectedValue, ddlFinaYear.SelectedValue, SessionCcode, SessionLcode);
                        if (dt.Rows.Count > 0)
                        {

                            lbldesigdis.Text = dt.Rows[0]["Designation"].ToString();
                            lbljoindis.Text = dt.Rows[0]["Dateofjoining"].ToString();
                            lblleaveallocated.Text = "0";
                            txtexistingNo.Text = dt.Rows[0]["ExisistingCode"].ToString();
                            lblleaveexceeded.Text = "0";

                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Allocat the Leave');", true);
                            //System.Windows.Forms.MessageBox.Show("Allocate the Leave", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                            //lblError.Text = "Allocate the leave";
                            lbldesigdis.Text = "";
                            lbljoindis.Text = "";
                            lblleaveallocated.Text = "";
                        }
                        DataTable dt1 = new DataTable();
                        dt1 = objdata.AlreadyTakenLeave(ddlempno.SelectedItem.Text, ddlFinaYear.SelectedValue, SessionCcode, SessionLcode);
                        if (dt1.Rows.Count > 0)
                        {
                            lblOutputleaves.Text = dt1.Rows[0]["Leave"].ToString();
                            lblleaveexceeded.Text = dt1.Rows[0]["ExcessLeave"].ToString();
                        }
                        RegularSearch = true;
                        // lblleaveexceeded.Text = "";
                    }

                    if (!RegularSearch)
                    {
                        lblError.Text = "";
                        DataTable dt = new DataTable();
                        dt = objdata.ApplyForLeaveinLabour_1(ddlempno.SelectedValue, ddlFinaYear.SelectedValue, SessionCcode, SessionLcode);
                        if (dt.Rows.Count > 0)
                        {

                            lbldesigdis.Text = dt.Rows[0]["Designation"].ToString();
                            lbljoindis.Text = dt.Rows[0]["Dateofjoining"].ToString();
                            txtexistingNo.Text = dt.Rows[0]["ExisistingCode"].ToString();
                            lblleaveallocated.Text = "0";
                            lblleaveexceeded.Text = "0";

                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Official Profile Not Registered Your Employee Number');", true);
                            //System.Windows.Forms.MessageBox.Show("Official Profile Not Registered Your Employee Number", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                            //lblError.Text = "Employee Details not Found in Official Profile";
                            lbldesigdis.Text = "";
                            lbljoindis.Text = "";
                            lblleaveallocated.Text = "";
                        }
                        DataTable dt1 = new DataTable();
                        dt1 = objdata.AlreadyTakenLeaveForLabour(ddlempno.SelectedItem.Text, ddlFinaYear.SelectedValue, SessionCcode, SessionLcode);
                        if (dt1.Rows.Count > 0)
                        {
                            lblError.Text = "";
                            //  lblOutputleaves.Text = dt1.Rows[0]["Leave"].ToString();
                            lblleaveexceeded.Text = dt1.Rows[0]["ExcessLeave"].ToString();
                            lblOutputleaves.Text = dt1.Rows[0]["ExcessLeave"].ToString();
                        }
                    }
                }
            }
        }
    }
    protected void txtapplyleave_TextChanged(object sender, EventArgs e)
    {

    }
    protected void txtfromdate_TextChanged(object sender, EventArgs e)
    {

    }
    protected void txttodate_TextChanged(object sender, EventArgs e)
    {

    }
    protected void btnLeave_Click(object sender, EventArgs e)
    {
        try
        {
            //panelError.Visible = true;
            bool ErrFlag = false;
            bool ErrDate = false;
            bool CheckFlag = false;
            string year, TotalWorkingdays = "";
            string ServerDate = objdata.ServerDate();
            double Daysss = 0;
            txtapplyleave.Text = "";
            DateTime FromDate, Todate;
            TimeSpan DateDiff;
            //if (txtapplyleave.Text == "")
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Apply for Leave');", true);
            //    ErrFlag = true;
            //}
            if (lblleaveallocated.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Click Search Button');", true);
                //System.Windows.Forms.MessageBox.Show("Click the Search Button", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                //lblError.Text = "Click the Search Button";
                ErrFlag = true;
            }
            else if (txtfromdate.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select From Date');", true);
                //System.Windows.Forms.MessageBox.Show("Select the From Date", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                //lblError.Text = "Select the From Date";
                ErrFlag = true;
            }
            else if (txttodate.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select To Date');", true);
                //System.Windows.Forms.MessageBox.Show("Select The To Date", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                //lblError.Text = "Select the To Date";
                ErrFlag = true;
            }
            if (ddlleavetype.SelectedItem.Text == "Maternity Leave")
            {
                string Female = objdata.Female(ddlempno.SelectedValue, SessionCcode, SessionLcode);
                int Fmale = Convert.ToInt32(Female);
                if (Fmale == 2)
                {
                    lblError.Text = "";
                    string Maternity = objdata.AvailableMaternityLeave(ddlempno.SelectedValue, ddlFinaYear.SelectedValue, SessionCcode, SessionLcode);
                    if (Maternity == "")
                    {
                        Maternity = "0";
                    }
                    int Mater = Convert.ToInt32(Maternity);
                    int dropmater = Convert.ToInt32(ddlleavetype.SelectedValue);
                    if (Mater == dropmater)
                    {
                        //lblError.Text = "The Maternity Leave yoy already Applied";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('The Maternity Leave You All Ready Apply');", true);
                        //System.Windows.Forms.MessageBox.Show("Maternity Leave you Already Applied", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                        ErrFlag = true;
                    }


                }
                else if (Fmale == 1)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Maternity Leave only Applicable For Female');", true);
                    //System.Windows.Forms.MessageBox.Show("Maternity Leave only Spplicable for female", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    //lblError.Text = "Maternity Leave only applicable for Female Employees";
                    ErrFlag = true;
                }
            }



            if (!ErrFlag)
            {
                lblError.Text = "";
                FromDate = DateTime.ParseExact(txtfromdate.Text, "dd-MM-yyyy", culterInfo.DateTimeFormat);
                Todate = DateTime.ParseExact(txttodate.Text, "dd-MM-yyyy", culterInfo.DateTimeFormat);
                //FromDate = DateTime.ParseExact(txtfromdate.Text, "dd/MM/yyyy", culterInfo.DateTimeFormat);
                ////FromDate = DateTime.ParseExact(txtfromdate.Text, "dd-MM-yyyy", null);
                //Todate = DateTime.ParseExact(txttodate.Text, "dd/MM/yyyy", culterInfo.DateTimeFormat);
                ////Todate = DateTime.ParseExact(txttodate.Text, "dd-MM-yyyy", null);
                DataTable dtchek = new DataTable();
                DataTable dtleave = new DataTable();
                if (Request.QueryString["edit"] != null)
                {
                    dtleave = objdata.AlreadyLeaveDateLoad_edit(ddlempno.SelectedValue, ddlFinaYear.SelectedValue, lbltrans.Text, SessionCcode, SessionLcode);
                }
                else
                {
                    dtleave = objdata.AlreadyLeaveDateLoad(ddlempno.SelectedValue, ddlFinaYear.SelectedValue, SessionCcode, SessionLcode);
                }
                if (dtleave.Rows.Count > 0)
                {
                    for (int i = 0; i < dtleave.Rows.Count; i++)
                    {
                        tempdateForm = dtleave.Rows[i]["FromDate"].ToString();
                        tempDateto = dtleave.Rows[i]["ToDate"].ToString();
                        for (DateTime j = Convert.ToDateTime(tempdateForm); j <= Convert.ToDateTime(tempDateto); )
                        {
                            if (j == Convert.ToDateTime(txtfromdate.Text) || (j == Convert.ToDateTime(txttodate.Text)))
                            {
                                ErrFlag = true;
                            }
                            else
                            {

                            }
                            j = j.AddDays(1);
                        }

                    }
                }
                if (!ErrFlag)
                {
                    if (Request.QueryString["edit"] != null)
                    {
                        dtchek = objdata.AllReadyApplyForLeaveDate_edit(ddlempno.SelectedValue, FromDate, Todate, ddlFinaYear.SelectedValue, lbltrans.Text, SessionCcode, SessionLcode);
                    }
                    else
                    {
                        dtchek = objdata.AllReadyApplyForLeaveDate(ddlempno.SelectedValue, FromDate, Todate, ddlFinaYear.SelectedValue, SessionCcode, SessionLcode);
                    }
                    if (dtchek.Rows.Count > 0)
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('You All Ready Apply to Leave  From " + dtchek.Rows[0]["FromDate"].ToString() + " To " + dtchek.Rows[0]["ToDate"].ToString() + " ');", true);
                        //System.Windows.Forms.MessageBox.Show("You Already Applied Leave From" + dtchek.Rows[0]["FromDate"].ToString() + " To " + dtchek.Rows[0]["ToDate"].ToString() , "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                        //lblError.Text = "You already applied Leave from " + dtchek.Rows[0]["FromDate"].ToString() + " To " + dtchek.Rows[0]["ToDate"].ToString();
                        CheckFlag = true;
                    }
                    else if (txtfromdate.Enabled == true)
                    {
                        if (Convert.ToDateTime(txtfromdate.Text) < Convert.ToDateTime(ServerDate))
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select From Date Properly');", true);
                            //System.Windows.Forms.MessageBox.Show("Select the From Date propeerly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                            //lblError.Text = "Select From Date Properly";
                            CheckFlag = true;
                            txtfromdate.Text = "";
                            txttodate.Text = "";
                        }
                        else if (Convert.ToDateTime(txttodate.Text) < Convert.ToDateTime(ServerDate))
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select To Date Properly');", true);
                            //System.Windows.Forms.MessageBox.Show("Select the To Date Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                            //lblError.Text = "Select To Date Properly";
                            CheckFlag = true;
                            txtfromdate.Text = "";
                            txttodate.Text = "";
                        }
                    }

                    if (!CheckFlag)
                    {
                        DateDiff = new TimeSpan(Todate.Ticks - FromDate.Ticks);
                        Daysss = DateDiff.Days;
                        int Days = Convert.ToInt32(Daysss);
                        mon = FromDate.Month;
                        Month();
                        int yr = FromDate.Year;
                        Days = Days + 1;
                        txtapplyleave.Text = Days.ToString();
                        string month = Convert.ToString(mon);
                        year = Convert.ToString(yr);
                        int dd = DateTime.DaysInMonth(yr, mon);
                        TotalWorkingdays = Convert.ToString(dd);
                        int Apply = Convert.ToInt32(txtapplyleave.Text);
                        //if (Apply > dd)
                        //{
                        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Valid Leave');", true);
                        //    ErrDate = true;
                        //}

                        if (FromDate > Todate)
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select  Date Properly');", true);
                            //System.Windows.Forms.MessageBox.Show("Select the Date properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                            //lblError.Text = "Select Date Properly";
                            ErrDate = true;
                        }
                        //if (Days == 1)
                        //{
                        //    if (FromDate == Todate)
                        //    {
                        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select  Date Properly');", true);
                        //        ErrDate = true;
                        //    }
                        //    else
                        //    {

                        //    }
                        //}
                        if (Days == (Daysss + 1))
                        {
                            //if (FromDate == Todate)
                            //{
                            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select  Date Properly');", true);
                            //    ErrDate = true;
                            //}
                            //else
                            //{

                            //}
                        }
                        else if (Days != Daysss)
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select  Date Properly');", true);
                            //System.Windows.Forms.MessageBox.Show("Select the Date properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                            //lblError.Text = "Select Date Properly";
                            ErrDate = true;
                        }
                        if (!ErrDate)
                        {
                            if (ddlcategory.SelectedValue == "1")
                            {
                                int ExcessLeaveCalculation = 0;
                                int Leavesavailed = 0;
                                if ((lblleaveallocated.Text.Trim() == "") || (lblleaveallocated.Text.Trim() == null))
                                {
                                    lblleaveallocated.Text = "0";
                                }
                                else if ((lblOutputleaves.Text.Trim() == null) || (lblOutputleaves.Text.Trim() == ""))
                                {
                                    lblOutputleaves.Text = "0";
                                }

                                if (ddlleavetype.SelectedItem.Text == "Maternity Leave")
                                {


                                }
                                else
                                {
                                    ExcessLeaveCalculation = (Convert.ToInt32(lblleaveallocated.Text.Trim()) - Convert.ToInt32(lblOutputleaves.Text.Trim()));
                                    Leavesavailed = ExcessLeaveCalculation - Convert.ToInt32(txtapplyleave.Text.Trim());
                                    if (Leavesavailed < 0)
                                    {
                                        lblleaveexceeded.Text = (Leavesavailed * (-1)).ToString();
                                    }
                                    else
                                    {
                                        lblleaveexceeded.Text = "0";
                                    }
                                }
                            }
                        }
                        else if (ddlcategory.SelectedValue == "2")
                        {

                        }
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Date properly');", true);
                    //System.Windows.Forms.MessageBox.Show("Select the Date Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    //lblError.Text = "Select the Date Properly";
                }
            }
        }
        catch (Exception ex)
        {

        }
    }
    protected void btnapply_Click(object sender, EventArgs e)
    {
        try
        {
            //panelError.Visible = true;
            bool ErrFlag = false;
            bool ErrDate = false;
            bool CheckFlag = false;
            string year, Leavetype, Applyleave, TotalWorkingdays = "";
            string EmpNo = "";
            double Daysss = 0;

            DateTime FromDate, Todate, ApprovedDate;
            TimeSpan DateDiff;
            if (ddlempno.SelectedItem.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select Employee Number');", true);
                //System.Windows.Forms.MessageBox.Show("Select the Employee Number", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                //lblError.Text = "Select the Employee No.";
                ErrFlag = true;
            }
            else if (ddlleavetype.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select Leave Type');", true);
                //System.Windows.Forms.MessageBox.Show("Select the Leave Type", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                //lblError.Text = "Select the Leave Type";
                ErrFlag = true;
            }
            else if (txtapplyleave.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter How many Days in Leave');", true);
                //System.Windows.Forms.MessageBox.Show("Enter the How many Days Leave", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                //lblError.Text = "Enter how many Days in Leave";
                ErrFlag = true;
            }
            else if (txtfromdate.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select From Date');", true);
                //System.Windows.Forms.MessageBox.Show("Select the From Date", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                //lblError.Text = "Select the From Date";
                ErrFlag = true;
            }
            else if (txttodate.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select To Date');", true);
                //System.Windows.Forms.MessageBox.Show("Select the To Date", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                //lblError.Text = "Select To Date";
                ErrFlag = true;
            }
            else if (txtapprovedbye.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Approved By');", true);
                //System.Windows.Forms.MessageBox.Show("Enter the Approved By", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                //lblError.Text = "Enter Approved By";
                ErrFlag = true;
            }
            else if (txtapproveddate.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select Approved Date');", true);
                //System.Windows.Forms.MessageBox.Show("Select the Approved Date", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                //lblError.Text = "Enter the Approved Date";
                ErrFlag = true;
            }
            if (!ErrFlag)
            {
                lblError.Text = "";
                FromDate = DateTime.ParseExact(txtfromdate.Text, "dd-MM-yyyy", null);
                Todate = DateTime.ParseExact(txttodate.Text, "dd-MM-yyyy", null);
                DataTable dtchek = new DataTable();
                if (Request.QueryString["edit"] != null)
                {
                    dtchek = objdata.AllReadyApplyForLeaveDate_edit(ddlempno.SelectedValue, FromDate, Todate, ddlFinaYear.SelectedValue, lbltrans.Text, SessionCcode, SessionLcode);
                }
                else
                {
                    dtchek = objdata.AllReadyApplyForLeaveDate(ddlempno.SelectedValue, FromDate, Todate, ddlFinaYear.SelectedValue, SessionCcode, SessionLcode);
                }
                if (dtchek.Rows.Count > 0)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('You All Ready Apply to Leave From " + dtchek.Rows[0]["FromDate"].ToString() + " To " + dtchek.Rows[0]["ToDate"].ToString() + " ');", true);
                    //System.Windows.Forms.MessageBox.Show("You Already Applied Leave for this Days", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    //lblError.Text = "You already Applied Leave for this Days";
                    CheckFlag = true;
                }
                if (!CheckFlag)
                {
                    lblError.Text = "";
                    ApprovedDate = DateTime.ParseExact(txtapproveddate.Text, "dd-MM-yyyy", culterInfo.DateTimeFormat);
                    DateDiff = new TimeSpan(Todate.Ticks - FromDate.Ticks);
                    Daysss = DateDiff.Days;
                    Daysss = Daysss + 1;
                    int Days = Convert.ToInt32(txtapplyleave.Text);
                    mon = FromDate.Month;
                    Month();
                    int yr = FromDate.Year;

                    string month = Convert.ToString(mon);
                    year = Convert.ToString(yr);
                    int dd = DateTime.DaysInMonth(yr, mon);
                    TotalWorkingdays = Convert.ToString(dd);

                    if (FromDate > Todate)
                    {
                        //lblError.Text = "Select Date Properly";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select  Date Properly');", true);
                        //System.Windows.Forms.MessageBox.Show("Select Date Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                        ErrDate = true;
                    }
                    if (Days == 1)
                    {
                        //if (FromDate == Todate)
                        //{
                        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select  Date Properly');", true);
                        //    ErrDate = true;
                        //}
                        //else
                        //{

                        //}
                    }
                    else if (Days == Daysss)
                    {
                        //if (FromDate == Todate)
                        //{
                        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select  Date Properly');", true);
                        //    ErrDate = true;
                        //}
                        //else
                        //{

                        //}
                    }
                    else if (Days != Daysss)
                    {
                        //ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select  Date Properly');", true);
                        ErrDate = true;
                    }
                    if (!ErrDate)
                    {
                        string Maternity = "";
                        if (ddlleavetype.SelectedItem.Text == "Maternity Leave")
                        {
                            Maternity = "Y";
                        }
                        EmpNo = ddlempno.SelectedItem.Text;
                        Leavetype = ddlleavetype.SelectedValue;
                        Applyleave = txtapplyleave.Text;
                        string Leavecode = objdata.LeaveCode();
                        if (ddlcategory.SelectedValue == "1")
                        {
                            if (Request.QueryString["edit"] != null)
                            {
                                objdata.leaveupdate_verify(lbltrans.Text, SessionCcode, SessionLcode);
                                objdata.ApplyForLeave(EmpNo, Leavetype, Applyleave, TotalWorkingdays, Leavecode, FromDate, Todate, Mont, year, lblleaveallocated.Text, lblOutputleaves.Text, lblleaveexceeded.Text, txtapprovedbye.Text, ApprovedDate, ddlFinaYear.SelectedValue, Maternity, SessionCcode, SessionLcode);
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Staff Leave Updated Successfully...');", true);
                                //System.Windows.Forms.MessageBox.Show("Staff Leave Applied Successfully", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);

                                //panelError.Visible = false;
                                ////panelsuccess.Visible = true;
                                //lblError.Text = "";
                                ////lbloutput.Text = "Staff Leave Updated Successfully";
                                //btnclick.Enabled = false;
                                //btnexitingSearch.Enabled = false;
                                //btnsearch.Enabled = false;
                                //btnLeave.Enabled = false;
                                //btnapply.Enabled = false;
                                //btnEdit.Enabled = false;
                                //btnupload.Enabled = false;
                                //btnback.Enabled = false;
                                //btnclear.Enabled = false;
                                //btndel.Enabled = false;
                                //Button1.Enabled = false;
                                ////btnok.Focus();
                                Loadgrid();
                            }
                            else
                            {
                                objdata.ApplyForLeave(EmpNo, Leavetype, Applyleave, TotalWorkingdays, Leavecode, FromDate, Todate, Mont, year, lblleaveallocated.Text, lblOutputleaves.Text, lblleaveexceeded.Text, txtapprovedbye.Text, ApprovedDate, ddlFinaYear.SelectedValue, Maternity, SessionCcode, SessionLcode);
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Staff Leave Apply Successfully...');", true);
                                //System.Windows.Forms.MessageBox.Show("Staff Leave Applied Successfully", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                //lblError.Text = "Staff Leave Applied Successfully";
                                //panelError.Visible = false;
                                ////panelsuccess.Visible = true;
                                //lblError.Text = "";
                                ////lbloutput.Text = "Staff Leave Applied Successfully";
                                //btnclick.Enabled = false;
                                //btnexitingSearch.Enabled = false;
                                //btnsearch.Enabled = false;
                                //btnLeave.Enabled = false;
                                //btnapply.Enabled = false;
                                //btnEdit.Enabled = false;
                                //btnupload.Enabled = false;
                                //btnback.Enabled = false;
                                //btnclear.Enabled = false;
                                //btndel.Enabled = false;
                                //Button1.Enabled = false;
                                //btnok.Focus();
                            }


                        }
                        else if (ddlcategory.SelectedValue == "2")
                        {
                            bool RegulInse = false;
                            string RegulatLabour = objdata.RegularLabore(EmpNo, SessionCcode, SessionLcode);
                            string Regul = RegulatLabour.Replace(" ", "");
                            if (Regul == "Regular")
                            {
                                if (Request.QueryString["edit"] != null)
                                {
                                    objdata.leaveupdate_verify(lbltrans.Text, SessionCcode, SessionLcode);
                                    objdata.ApplyForLeave(EmpNo, Leavetype, Applyleave, TotalWorkingdays, Leavecode, FromDate, Todate, Mont, year, lblleaveallocated.Text, lblOutputleaves.Text, lblleaveexceeded.Text, txtapprovedbye.Text, ApprovedDate, ddlFinaYear.SelectedValue, Maternity, SessionCcode, SessionLcode);
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Regular Labour Leave Updated Successfully...');", true);
                                    //System.Windows.Forms.MessageBox.Show("Regular Labour Leave Updated Successfully", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                    //lblError.Text = "Labour Leave updated Successfully";
                                    //panelError.Visible = false;
                                    ////panelsuccess.Visible = true;
                                    //lblError.Text = "";
                                    ////lbloutput.Text = "Labour Leave Updated Successfully";
                                    //btnclick.Enabled = false;
                                    //btnexitingSearch.Enabled = false;
                                    //btnsearch.Enabled = false;
                                    //btnLeave.Enabled = false;
                                    //btnapply.Enabled = false;
                                    //btnEdit.Enabled = false;
                                    //btnupload.Enabled = false;
                                    //btnback.Enabled = false;
                                    //btnclear.Enabled = false;
                                    //btndel.Enabled = false;
                                    //Button1.Enabled = false;
                                    ////btnok.Focus();
                                    Loadgrid();
                                }
                                else
                                {
                                    objdata.ApplyForLeave(EmpNo, Leavetype, Applyleave, TotalWorkingdays, Leavecode, FromDate, Todate, Mont, year, lblleaveallocated.Text, lblOutputleaves.Text, lblleaveexceeded.Text, txtapprovedbye.Text, ApprovedDate, ddlFinaYear.SelectedValue, Maternity, SessionCcode, SessionLcode);
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Regular Labour Leave Apply Successfully...');", true);
                                    //System.Windows.Forms.MessageBox.Show("Regular Labour Leave Applied Successfully", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                    //lblError.Text = "Labour Leave Applied Successfully";
                                    //panelError.Visible = false;
                                    ////panelsuccess.Visible = true;
                                    //lblError.Text = "";
                                    ////lbloutput.Text = "Labour Leave Applied Successfully";
                                    //btnclick.Enabled = false;
                                    //btnexitingSearch.Enabled = false;
                                    //btnsearch.Enabled = false;
                                    //btnLeave.Enabled = false;
                                    //btnapply.Enabled = false;
                                    //btnEdit.Enabled = false;
                                    //btnupload.Enabled = false;
                                    //btnback.Enabled = false;
                                    //btnclear.Enabled = false;
                                    //btndel.Enabled = false;
                                    //Button1.Enabled = false;
                                    //btnok.Focus();

                                }

                                RegulInse = true;

                            }
                            if (!RegulInse)
                            {
                                if (Request.QueryString["edit"] != null)
                                {
                                    objdata.leaveupdate_verify(lbltrans.Text, SessionCcode, SessionLcode);
                                    objdata.ApplyForLeaveinLabour(EmpNo, Leavetype, Applyleave, TotalWorkingdays, Leavecode, FromDate, Todate, Mont, year, txtapprovedbye.Text, ApprovedDate, ddlFinaYear.SelectedValue, SessionCcode, SessionLcode);
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Labour Leave Updated Successfully...');", true);
                                    //System.Windows.Forms.MessageBox.Show("Labour Leave Applied Successfully", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                    //lblError.Text = "Labour Leave Updated Successfully";
                                    //panelError.Visible = false;
                                    ////panelsuccess.Visible = true;
                                    //lblError.Text = "";
                                    ////lbloutput.Text = "Labour Leave Updated Successfully";
                                    //btnclick.Enabled = false;
                                    //btnexitingSearch.Enabled = false;
                                    //btnsearch.Enabled = false;
                                    //btnLeave.Enabled = false;
                                    //btnapply.Enabled = false;
                                    //btnEdit.Enabled = false;
                                    //btnupload.Enabled = false;
                                    //btnback.Enabled = false;
                                    //btnclear.Enabled = false;
                                    //btndel.Enabled = false;
                                    //Button1.Enabled = false;
                                    ////btnok.Focus();
                                    Loadgrid();
                                }
                                else
                                {
                                    objdata.ApplyForLeaveinLabour(EmpNo, Leavetype, Applyleave, TotalWorkingdays, Leavecode, FromDate, Todate, Mont, year, txtapprovedbye.Text, ApprovedDate, ddlFinaYear.SelectedValue, SessionCcode, SessionLcode);
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Labour Leave Apply Successfully...');", true);
                                    //System.Windows.Forms.MessageBox.Show("Labour Leave Applied Successfully", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                    //lblError.Text = "Labour leave Applied Successfully";
                                    panelError.Visible = false;
                                    //panelsuccess.Visible = true;
                                    //lblError.Text = "";
                                    ////lbloutput.Text = "Labour Leave Applied Successfully";
                                    //btnclick.Enabled = false;
                                    //btnexitingSearch.Enabled = false;
                                    //btnsearch.Enabled = false;
                                    //btnLeave.Enabled = false;
                                    //btnapply.Enabled = false;
                                    //btnEdit.Enabled = false;
                                    //btnupload.Enabled = false;
                                    //btnback.Enabled = false;
                                    //btnclear.Enabled = false;
                                    //btndel.Enabled = false;
                                    //Button1.Enabled = false;
                                    //btnok.Focus();
                                }

                            }
                        }

                    }
                }
            }
        }
        catch (Exception ex)
        {
        }
    }
    protected void btnEdit_Click(object sender, EventArgs e)
    {
        Response.Redirect("ApplyForLeave.aspx?edit=Edit");
    }
    protected void btnclear_Click(object sender, EventArgs e)
    {
        Response.Redirect("ApplyForLeave.aspx");
    }
    protected void btnupload_Click(object sender, EventArgs e)
    {
        Response.Redirect("UploadLeave.aspx");
    }
    protected void btnback_Click(object sender, EventArgs e)
    {
        Response.Redirect("ApplyForLeave.aspx");
    }
    protected void btndel_Click(object sender, EventArgs e)
    {
        bool valueCheck = false;
        bool isdeleted = false;
        //panelError.Visible = true;
        string server_date = objdata.ServerDate();
        string tempserDate = Convert.ToDateTime(server_date).ToShortDateString();
        string tempfromdate = "";
        DateTime serDate;
        DateTime fromDateval;
        serDate = new DateTime();
        fromDateval = new DateTime();
        foreach (GridViewRow gv in GvPFGradeData.Rows)
        {
            RadioButton rb = (RadioButton)gv.FindControl("rbtnstatus");
            if (rb.Checked == true)
            {
                valueCheck = true;
            }
        }
        if (valueCheck == true)
        {
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "script", "showConfirm();", true);
        }
        if (valueCheck == false)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Employee');", true);

        }
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        try
        {
            //panelError.Visible = true;
            bool valueCheck = false;
            clear();
            transId = "";
            string server_date = objdata.ServerDate();
            string tempserDate = Convert.ToDateTime(server_date).ToShortDateString();
            string tempfromdate = "";
            DateTime serDate;
            DateTime fromDateval;
            serDate = new DateTime();
            fromDateval = new DateTime();
            foreach (GridViewRow gv in GvPFGradeData.Rows)
            {
                RadioButton rb = (RadioButton)gv.FindControl("rbtnstatus");
                if (rb.Checked == true)
                {
                    valueCheck = true;
                }
            }
            foreach (GridViewRow gv in GvPFGradeData.Rows)
            {

                RadioButton rb = (RadioButton)gv.FindControl("rbtnstatus");
                if (rb.Checked == true)
                {
                    Label lblemp = (Label)GvPFGradeData.Rows[gv.RowIndex].FindControl("lblempno");
                    Label lblempname = (Label)GvPFGradeData.Rows[gv.RowIndex].FindControl("lblempname");
                    Label lblexist = (Label)GvPFGradeData.Rows[gv.RowIndex].FindControl("lblExistingCode");
                    Label lbldept = (Label)GvPFGradeData.Rows[gv.RowIndex].FindControl("lbldept");
                    Label lbldesignation = (Label)GvPFGradeData.Rows[gv.RowIndex].FindControl("lbldesignation");
                    Label lblapplyLeave = (Label)GvPFGradeData.Rows[gv.RowIndex].FindControl("lblapplyLeave");
                    Label lblfromdate = (Label)GvPFGradeData.Rows[gv.RowIndex].FindControl("lblfromdate");
                    Label lbltodate = (Label)GvPFGradeData.Rows[gv.RowIndex].FindControl("lbltodate");
                    Label lblleavetype = (Label)GvPFGradeData.Rows[gv.RowIndex].FindControl("lblleavetype");
                    Label lbltransId = (Label)GvPFGradeData.Rows[gv.RowIndex].FindControl("lbltransId");
                    Label lbldeptcode = (Label)GvPFGradeData.Rows[gv.RowIndex].FindControl("lbldeptcode");
                    DataTable dt = new DataTable();
                    dt = objdata.retrive_leaveApply(lblemp.Text);
                    if (dt.Rows[0]["StafforLabor"].ToString() == "S")
                    {
                        ddlcategory.SelectedValue = "1";
                    }
                    else if (dt.Rows[0]["StafforLabor"].ToString() == "L")
                    {
                        ddlcategory.SelectedValue = "2";
                    }
                    DataTable dtdpt = new DataTable();
                    dtdpt = objdata.DropDownDepartment_category(dt.Rows[0]["StafforLabor"].ToString());
                    ddldepartment.DataSource = dtdpt;
                    ddldepartment.DataSource = dtdpt;
                    ddldepartment.DataTextField = "DepartmentNm";
                    ddldepartment.DataValueField = "DepartmentCd";

                    ddldepartment.DataBind();
                    ddldepartment.SelectedValue = lbldeptcode.Text;
                    DataTable dtemp = new DataTable();
                    dtemp = objdata.EmployeeNoandName(dt.Rows[0]["StafforLabor"].ToString(), lbldeptcode.Text, SessionCcode, SessionLcode);
                    ddlempno.DataSource = dtemp;
                    ddlempno.DataTextField = "EmpNo";
                    ddlempno.DataValueField = "EmpNo";
                    ddlempno.DataBind();

                    ddlempname.DataSource = dtemp;
                    ddlempname.DataTextField = "EmpName";
                    ddlempname.DataValueField = "EmpNo";
                    ddlempname.DataBind();
                    ddlempno.SelectedValue = lblemp.Text;
                    ddlempname.SelectedValue = lblemp.Text;
                    txtexistingNo.Text = lblexist.Text;
                    lbldesigdis.Text = lbldesignation.Text;
                    lbljoindis.Text = dt.Rows[0]["doj"].ToString();
                    if ((dt.Rows[0]["AllocatedLeaves"].ToString() == "") || (dt.Rows[0]["AllocatedLeaves"].ToString() == null))
                    {
                        lblleaveallocated.Text = "0";
                    }
                    else
                    {
                        lblleaveallocated.Text = dt.Rows[0]["AllocatedLeaves"].ToString();
                    }
                    if ((dt.Rows[0]["AlreadyTakenLeave"].ToString() == "") || (dt.Rows[0]["AlreadyTakenLeave"].ToString() == null))
                    {
                        lblOutputleaves.Text = "0";
                    }
                    else
                    {
                        lblOutputleaves.Text = dt.Rows[0]["AlreadyTakenLeave"].ToString();
                    }
                    if ((dt.Rows[0]["ExcessLeave"].ToString() == "") || (dt.Rows[0]["ExcessLeave"].ToString() == null))
                    {
                        lblleaveexceeded.Text = "0";
                    }
                    else
                    {
                        lblleaveexceeded.Text = dt.Rows[0]["ExcessLeave"].ToString();
                    }
                    //lblleaveexceeded.Text = dt.Rows[0]["ExcessLeave"].ToString();
                    txtapprovedbye.Text = dt.Rows[0]["Approvededby"].ToString();
                    txtapproveddate.Text = dt.Rows[0]["Approveddate"].ToString();
                    ddlFinaYear.SelectedValue = dt.Rows[0]["FinancialYear"].ToString();
                    ddlleavetype.SelectedValue = dt.Rows[0]["LeaveType"].ToString();
                    txtfromdate.Text = lblfromdate.Text;
                    txttodate.Text = lbltodate.Text;
                    txtapplyleave.Text = lblapplyLeave.Text;
                    lbltrans.Text = lbltransId.Text;
                    serDate = DateTime.ParseExact(tempserDate, "dd-MM-yyyy", culterInfo.DateTimeFormat);
                    tempfromdate = Convert.ToDateTime(lblfromdate.Text).ToShortDateString();
                    fromDateval = DateTime.ParseExact(tempfromdate, "dd-MM-yyyy", culterInfo.DateTimeFormat);

                    //serDate = DateTime.ParseExact(tempserDate, "dd-MM-yyyy", culterInfo.DateTimeFormat);
                    ////serDate = Convert.ToDateTime(tempserDate);
                    //tempfromdate = Convert.ToDateTime(lblfromdate.Text).ToShortDateString();
                    //fromDateval = DateTime.ParseExact(tempfromdate, "dd-MM-yyyy", culterInfo.DateTimeFormat);
                    ////fromDateval = Convert.ToDateTime(tempfromdate);
                    if (fromDateval > serDate)
                    {
                        txtfromdate.Enabled = true;
                    }
                    else
                    {
                        txtfromdate.Enabled = false;
                    }
                }
                if (valueCheck == false)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Employee');", true);
                    //System.Windows.Forms.MessageBox.Show("Select the Employee", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    //lblError.Text = "Select the Employee";
                }
            }
        }
        catch (Exception ex)
        {

        }
    }
    protected void btnok_Click(object sender, EventArgs e)
    {
        panelError.Visible = false;
        panelsuccess.Visible = false;
        lblError.Text = "";
        lbloutput.Text = "";
        btnclick.Enabled = true;
        btnexitingSearch.Enabled = true;
        btnsearch.Enabled = true;
        btnLeave.Enabled = true;
        btnapply.Enabled = true;
        btnEdit.Enabled = true;
        btnupload.Enabled = true;
        btnback.Enabled = true;
        btnclear.Enabled = true;
        btndel.Enabled = true;
        Button1.Enabled = true;
        panelconfirm.Visible = false;
    }
    protected void btnYes_Click(object sender, EventArgs e)
    {
        bool valueCheck = false;
        bool isdeleted = false;
        //panelError.Visible = true;
        string server_date = objdata.ServerDate();
        string tempserDate = Convert.ToDateTime(server_date).ToShortDateString();
        string tempfromdate = "";
        DateTime serDate;
        DateTime fromDateval;
        serDate = new DateTime();
        fromDateval = new DateTime();
        panelconfirm.Visible = false;
        panelError.Visible = false;
        panelsuccess.Visible = false;
        lblError.Text = "";
        lbloutput.Text = "";
        btnclick.Enabled = true;
        btnexitingSearch.Enabled = true;
        btnsearch.Enabled = true;
        btnLeave.Enabled = true;
        btnapply.Enabled = true;
        btnEdit.Enabled = true;
        btnupload.Enabled = true;
        btnback.Enabled = true;
        btnclear.Enabled = true;
        btndel.Enabled = true;
        Button1.Enabled = true;
        GvPFGradeData.Enabled = true;
        foreach (GridViewRow gv in GvPFGradeData.Rows)
        {
            RadioButton rb = (RadioButton)gv.FindControl("rbtnstatus");
            if (rb.Checked == true)
            {
                Label lbltransId = (Label)GvPFGradeData.Rows[gv.RowIndex].FindControl("lbltransId");
                Label lblfromdate = (Label)GvPFGradeData.Rows[gv.RowIndex].FindControl("lblfromdate");
                serDate = DateTime.Parse(tempserDate);
                tempfromdate = Convert.ToDateTime(lblfromdate.Text).ToShortDateString();
                fromDateval = DateTime.Parse(tempfromdate);
                if (fromDateval > serDate)
                {
                    objdata.leaveCancel(lbltransId.Text);
                    isdeleted = true;
                    Loadgrid();
                    clear();
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('It cannot be Deleted');", true);
                    //System.Windows.Forms.MessageBox.Show("It cannot be Deleted", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    clear();
                    //lblError.Text = "It cannot be Deleted";

                }
            }
        }
        if (valueCheck == false)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Employee');", true);
            //panelError.Visible = true;
            //lblError.Text = "Select the Employee";
        }
        if (isdeleted == true)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Cancelled Successfully');", true);
            //panelError.Visible = true;
            //lblError.Text = "Cancelled Successfully";
        }
    }
    protected void cmdOverriteGrade_Click(object sender, EventArgs e)
    {
        try
        {
            string server_date = objdata.ServerDate();
            string tempserDate = Convert.ToDateTime(server_date).ToShortDateString();
            string tempfromdate = "";
            DateTime serDate;
            DateTime fromDateval;
            serDate = new DateTime();
            fromDateval = new DateTime();
            foreach (GridViewRow gv in GvPFGradeData.Rows)
            {
                RadioButton rb = (RadioButton)gv.FindControl("rbtnstatus");
                if (rb.Checked == true)
                {
                    Label lbltransId = (Label)GvPFGradeData.Rows[gv.RowIndex].FindControl("lbltransId");
                    Label lblfromdate = (Label)GvPFGradeData.Rows[gv.RowIndex].FindControl("lblfromdate");
                    serDate = DateTime.Parse(tempserDate);
                    tempfromdate = Convert.ToDateTime(lblfromdate.Text).ToShortDateString();
                    fromDateval = DateTime.Parse(tempfromdate);
                    if (fromDateval > serDate)
                    {
                        objdata.leaveCancel(lbltransId.Text);
                        //isdeleted = true;
                        Loadgrid();
                        clear();
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('deleted Successfully');", true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('It cannot be Deleted');", true);

                        clear();


                    }
                }
            }
        }
        catch (Exception ex)
        {
        }
    }

    protected void btnNo_Click(object sender, EventArgs e)
    {
        panelconfirm.Visible = false;
        panelError.Visible = false;
        panelsuccess.Visible = false;
        lblError.Text = "";
        lbloutput.Text = "";
        btnclick.Enabled = true;
        btnexitingSearch.Enabled = true;
        btnsearch.Enabled = true;
        btnLeave.Enabled = true;
        btnapply.Enabled = true;
        btnEdit.Enabled = true;
        btnupload.Enabled = true;
        btnback.Enabled = true;
        btnclear.Enabled = true;
        btndel.Enabled = true;
        Button1.Enabled = true;
        GvPFGradeData.Enabled = true;
    }
    protected void btnEmp_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptEmpDownload.aspx");
    }
    protected void btnatt_Click(object sender, EventArgs e)
    {
        Response.Redirect("AttenanceDownload.aspx");
    }
    protected void btnLeave1_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptLeaveSample.aspx");
    }
    protected void btnOT_Click(object sender, EventArgs e)
    {
        Response.Redirect("OTDownload.aspx");
    }
    protected void btnSal_Click(object sender, EventArgs e)
    {
        Response.Redirect("FrmDeductionDownload.aspx");
    }
}
