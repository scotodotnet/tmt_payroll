﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class RptForm5 : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    string SessionAdmin;
    protected void Page_Load(object sender, EventArgs e)
    {
        SessionAdmin = Session["Isadmin"].ToString();
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        string ss = Session["UserId"].ToString();
    }
    protected void btnsearch_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;

        DataTable dt = new DataTable();
        if (txtpfnumber.Text == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Enter PF Number ...!');", true);
            ErrFlag = true;
        }
        if (!ErrFlag)
        {
            dt = objdata.SearchPFNumberForForm5(txtpfnumber.Text);
            txtempname.Text = dt.Rows[0]["EmpName"].ToString();
            txtfathername.Text = dt.Rows[0]["FatherName"].ToString();
            txtdob.Text = dt.Rows[0]["Dob"].ToString();
            txtgender.Text = dt.Rows[0]["Gender"].ToString();
            txtdoj.Text = dt.Rows[0]["DOJ"].ToString();
        }


    }
    protected void btnprint_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        if (txtpfnumber.Text == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Enter PF Number ...!');", true);
            ErrFlag = true;
        }
        if (!ErrFlag)
        {
            Response.Redirect("Reports.aspx?PFNumber=" + txtpfnumber.Text + "&EmpName=" + txtempname.Text + "&FatherName=" + txtempname.Text + "  "+
                " &Dob=" + txtdob.Text + "&Gender=" + txtgender.Text + "&Doj=" + txtdoj.Text + "&FundDate=" +txtdojthefund.Text + "      &rpttype=payslip");
        }

    }
}
