﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Payroll;
using System.Globalization;
using Payroll.Configuration;
using Payroll.Data;
using System.Data.SqlClient;

using Altius.BusinessAccessLayer.BALDataAccess;


public partial class SPMSalaryRegistration : System.Web.UI.Page
{
    string constr = ConfigurationManager.AppSettings["ConnectionString"];
    SqlConnection cn;
    protected System.Resources.ResourceManager rm;
    protected string PayrollRegisterContentMeta, pageTitle;
    BALDataAccess objdata = new BALDataAccess();
    SalaryClass objSal = new SalaryClass();
    string stafflabour;
    static string Adv_id;
    static string Adv_BalanceAmt;
    static string Increment_mont;
    static string Dec_mont;
    static string Adv_due;
    bool isUK = false;
    static string ExisitingNo = "", LeaveTakenMonth = "", FirstMonth = "", SecondMonth = "", ThirdMonth = "";
    static double BalValu = 0, DoMonthValue = 0;
    string LeaveTakenForDisplay = "";
    static decimal NetPay1 = 0;
    static int NetPay = 0;
    string SessionAdmin;
    static int d = 0;
    string TempDate;
    int Tyr;
    int Tmonth;
    DateTime TransDate;
    string SessionCcode;
    string SessionLcode;
    string staffLabour;
    static string EmpType;
    static string PF_salary;
    static decimal AdvAmt;
    static string ID;
    string sum_val = "0";
    static string FixedBasic;
    static string FixedFDA;
    static string FixedHRA;
    decimal val;
    static string pf = "0.00";
    string halfNight;
    string FullNight;
    string ThreeSided;
    string WagesType;
    string Name_Upload;
    protected void Page_Load(object sender, EventArgs e)
    {
        cn = new SqlConnection(constr);
        //this.MyCustomButton.AlertMessage = "Hey, stop clicking me!"; 
        //  Convert.ToInt32(txtbasic.Text);
        
      
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionAdmin = Session["Isadmin"].ToString();
        if (SessionAdmin == "2")
        {
            Response.Redirect("EmployeeRegistration.aspx");
        }
        string ss = Session["UserId"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        lblComany.Text = SessionCcode + " - " + SessionLcode;
        if (Request.QueryString != null)
        {

        }

        if (!IsPostBack)
        {
            Months_load();
            //Department();
            clr();
            DropDwonCategory();

            int currentYear = Utility.GetFinancialYear;
            for (int i = 0; i < 10; i++)
            {
                ddlFinaYear.Items.Add(new ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                //  ddlShowYear.Items.Add(new ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                currentYear = currentYear - 1;
            }
            //MstBankDropDown();
        }
        lblusername.Text = Session["Usernmdisplay"].ToString();
    }
    public void Months_load()
    {
        DataTable dt = new DataTable();
        dt = objdata.months_load();
        ddlMonths.DataSource = dt;
        ddlMonths.DataTextField = "Months";
        ddlMonths.DataValueField = "Months";
        ddlMonths.DataBind();
    }
    public void clr()
    {
        txtrest.Text = "";
        txtfrom.Text = "";
        txtTo.Text = "";
        txtexistingNo.Text = "";
        txtsalaryday.Text = "";
        txtempdays.Text = "0";
        txtNFh.Text = "0";
        txtwork.Text = "0";
        //txtapplyleave.Text = "0";
        txtlossofpay.Text = "0";
        txtbasic.Text = "0";
        txtpf.Text = "0";
        txtFDA.Text = "0";
        txtESI.Text = "0";
        txtVDA.Text = "0";
        txtAdvance.Text = "0";
        txtHRA.Text = "0";
        txtStamp.Text = "0";
        txtAllowance.Text = "0";
        txtunion.Text = "0";
        txtOT.Text = "0";
        txtdeduction.Text = "0";
        EarningsAmt.Text = "0";
        DeductionAmt.Text = "0";
        lblNetPayAmt.Text = "0";
        lbltext.Text = "";
        txtcl.Text = "";
        txtAllowance2.Text = "0";
        txtdeduction2.Text = "0";
        txtded3.Text = "0";
        txtded4.Text = "0";
        txall3.Text = "0";
        txtsll4.Text = "0";
        txtSBasic.Text = "0";
        txtSPF.Text = "0";
        txtSesi.Text = "0";
        txtSAdvance.Text = "0";
        txtSstamp.Text = "0";
        txtSAll1.Text = "0";
        txtSAll2.Text = "0";
        txtSAll3.Text = "0";
        txtSAll4.Text = "0";
        txtSded1.Text = "0";
        txtSded2.Text = "0";
        txtSded3.Text = "0";
        txtSded4.Text = "0";
        txtWBasic.Text = "0";
        txtWpf.Text = "0";
        txtWesi.Text = "0";
        txtWot.Text = "0";
        txtWadvance.Text = "0";
        txtWAll1.Text = "0";
        txtWall2.Text = "0";
        txtWall3.Text = "0";
        txtWall4.Text = "0";
        txtWded1.Text = "0";
        txtWded2.Text = "0";
        txtWDed3.Text = "0";
        txtWded4.Text = "0";
        txtSLOP.Text = "0";
        txthome.Text = "";
        txtSAll5.Text = "0";
        txtSded5.Text = "0";
        txtHalf.Text = "0.00";
        txtFull.Text = "0.00";
        txtDayIncentive.Text = "0.00";
        txtThreeSided.Text = "0.00";
        txtSpinning.Text = "0.00";
        btnEdit.Enabled = true;
        btnWadvance.Enabled = true;
        btnSadvance.Enabled = true;
    }
    public void DropDwonCategory()
    {
        DataTable dtcate = new DataTable();
        dtcate = objdata.DropDownCategory();
        ddlcategory.DataSource = dtcate;
        ddlcategory.DataTextField = "CategoryName";
        ddlcategory.DataValueField = "CategoryCd";
        ddlcategory.DataBind();
    }
    public string NumerictoNumber(int Number, bool isUK)
    {
        if (Number == 0)
            return "Zero";

        string and = isUK ? "and " : "";
        if (Number == -2147483648)
            return "Minus Two Billion One Hundred " + and +
        "Forty Seven Million Four Hundred " + and + "Eighty Three Thousand " +
        "Six Hundred " + and + "Forty Eight";

        int[] num = new int[4];
        int first = 0;
        int u, h, t = 0;
        System.Text.StringBuilder sb = new System.Text.StringBuilder();
        if (Number < 0)
        {
            sb.Append("Minus");
            Number = -Number;
        }

        string[] words0 = { "", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine" };
        string[] words1 = { "Ten ", "Eleven ", "Twelve ", "Thirteen ", "Fourteen ", "Fifteen ", "Sixteen ", "Seventeen ", "Eighteen ", "Nineteen " };
        string[] words2 = { "Twenty ", "Thirty ", "Forty ", "Fifty ", "Sixty ", "Seventy ", "Eighty ", "Ninety " };
        string[] words3 = { "Thousand ", "Lak", "Crore", "Million ", "Billion " };
        num[0] = Number % 1000;
        num[1] = Number / 1000;
        num[2] = Number / 1000000;
        num[1] = num[1] - 1000 * num[2];  // thousands
        num[2] = num[2] - 100 * num[3];//laks
        num[3] = Number / 10000000;     // billions
        num[2] = num[2] - 1000 * num[3];  // millions

        for (int i = 3; i > 0; i--)
        {
            if (num[i] != 0)
            {
                first = i;
                break;
            }
        }
        for (int i = first; i >= 0; i--)
        {
            if (num[i] == 0) continue;
            u = num[i] % 10;
            t = num[i] / 10;
            h = num[i] / 100;
            t = t - 10 * h;

            if (h > 0)
                sb.Append(words0[h] + " Hundred ");
            if (u > 0 || t > 0)
            {
                if (h > 0 || i < first)
                    sb.Append(and);

                if (t == 0)
                    sb.Append(words0[u]);
                else if (t == 1)
                    sb.Append(words1[u]);
                else
                    sb.Append(words2[t - 2] + words0[u]);

            }

            if (i != 0)
                sb.Append(words3[i - 1]);

        }

        return sb.ToString().TrimEnd();



    }
    public void EmployeeIDLoad()
    {
        if (ddlcategory.SelectedValue == "1")
        {
            stafflabour = "S";
        }
        else
        {
            stafflabour = "L";
        }
        DataTable dtempcode = new DataTable();
        dtempcode = objdata.EmployeeNameandNumber_SalaryDetails(stafflabour, ddldepartment.SelectedValue, SessionCcode, SessionLcode, rbsalary.SelectedValue);
        if (ddlcategory.SelectedValue == "2")
        {
            //ddlempcodeLabour.DataSource = dtempcode;
            DataRow dr = dtempcode.NewRow();
            dr["EmpNo"] = ddldepartment.SelectedItem.Text;
            dr["EmpName"] = ddldepartment.SelectedItem.Text;
            dr["ExisistingCode"] = ddldepartment.SelectedItem.Text;
            dtempcode.Rows.InsertAt(dr, 0);

            //ddlempcodeLabour.DataTextField = "EmpNo";
            //ddlempcodeLabour.DataValueField = "EmpNo";
            //ddlempcodeLabour.DataBind();
            //ddlempNameLabour.DataSource = dtempcode;
            //ddlempNameLabour.DataTextField = "EmpName";
            //ddlempNameLabour.DataValueField = "EmpNo";
            //ddlempNameLabour.DataBind();


            ddlempCodeStaff.DataSource = dtempcode;
            ddlempCodeStaff.DataTextField = "EmpNo";
            ddlempCodeStaff.DataValueField = "EmpNo";
            ddlempCodeStaff.DataBind();
            ddlempNameStaff.DataSource = dtempcode;
            ddlempNameStaff.DataTextField = "EmpName";
            ddlempNameStaff.DataValueField = "EmpNo";
            ddlempNameStaff.DataBind();
            ddToken.DataSource = dtempcode;
            ddToken.DataTextField = "ExisistingCode";
            ddToken.DataValueField = "ExisistingCode";
            ddToken.DataBind();
        }
        else if (ddlcategory.SelectedValue == "1")
        {

            ddlempCodeStaff.DataSource = dtempcode;
            DataRow dr = dtempcode.NewRow();
            dr["EmpNo"] = ddldepartment.SelectedItem.Text;
            dr["EmpName"] = ddldepartment.SelectedItem.Text;
            dr["ExisistingCode"] = ddldepartment.SelectedItem.Text;
            dtempcode.Rows.InsertAt(dr, 0);
            ddlempCodeStaff.DataTextField = "EmpNo";
            ddlempCodeStaff.DataValueField = "EmpNo";
            ddlempCodeStaff.DataBind();

            ddlempNameStaff.DataSource = dtempcode;
            ddlempNameStaff.DataTextField = "EmpName";
            ddlempNameStaff.DataValueField = "EmpNo";
            ddlempNameStaff.DataBind();
            ddToken.DataSource = dtempcode;
            ddToken.DataTextField = "ExisistingCode";
            ddToken.DataValueField = "ExisistingCode";
            ddToken.DataBind();
        }

    }
    protected void ddlcategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            bool ErrFlag = false;
            PanelStaff.Visible = false;
            PanelWorker.Visible = false;
            PanelPermenant.Visible = false;
            DataTable dempty = new DataTable();
            DataTable dt = new DataTable();
            ddlempCodeStaff.DataSource = dempty;
            ddlempCodeStaff.DataBind();
            ddlempNameStaff.DataSource = dempty;
            ddlempNameStaff.DataBind();
            ddToken.DataSource = dempty;
            ddToken.DataBind();
            clr();
            if (rbsalary.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Wages Type');", true);
                ErrFlag = true;
            }
            if (!ErrFlag)
            {
                if (ddlcategory.SelectedValue == "1")
                {
                    staffLabour = "S";
                }
                else if (ddlcategory.SelectedValue == "2")
                {
                    staffLabour = "L";
                }
                else
                {
                    staffLabour = "0";
                }
                DataTable dtDip = new DataTable();
                dtDip = objdata.DropDownDepartment_category(staffLabour);
                if (dtDip.Rows.Count > 1)
                {
                    ddldepartment.DataSource = dtDip;
                    ddldepartment.DataTextField = "DepartmentNm";
                    ddldepartment.DataValueField = "DepartmentCd";
                    ddldepartment.DataBind();
                }
                else
                {
                    DataTable dt1 = new DataTable();
                    ddldepartment.DataSource = dt1;
                    ddldepartment.DataBind();
                }
            }
        }
        catch (Exception ex)
        {
        }
    }
    protected void ddldepartment_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            PanelStaff.Visible = false;
            PanelWorker.Visible = false;
            PanelPermenant.Visible = false;
            DataTable dempty = new DataTable();
            DataTable dt = new DataTable();
            ddlempCodeStaff.DataSource = dempty;
            ddlempCodeStaff.DataBind();
            ddlempNameStaff.DataSource = dempty;
            ddlempNameStaff.DataBind();
            ddToken.DataSource = dempty;
            ddToken.DataBind();
            clr();
        }
        catch (Exception ex)
        {
        }
    }

    protected void btnclick_Click(object sender, EventArgs e)
    {
        try
        {
            PanelStaff.Visible = false;
            PanelWorker.Visible = false;
            PanelPermenant.Visible = false;
            bool ErrFlag = false;
            clr();
            if (ddlcategory.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Category Staff or Labour!');", true);
                //System.Windows.Forms.MessageBox.Show("Select the Category Staff or Labour", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;

            }
            else if (ddldepartment.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Department ');", true);
                //System.Windows.Forms.MessageBox.Show("Select the Department", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }

            if (!ErrFlag)
            {


                if (ddlcategory.SelectedValue == "1")
                {
                    //PanelStaffSalary.Visible = true;
                    //PanelLaborSalary.Visible = false;
                    //PanelLabourSalaryNew.Visible = false;
                    EmployeeIDLoad();
                    //clr();
                    //lblsalary.Text = "Staff Salary Details";
                }
                else if (ddlcategory.SelectedValue == "2")
                {
                    //PanelStaffSalary.Visible = false;
                    //PanelLaborSalary.Visible = false;
                    //PanelLabourSalaryNew.Visible = true;
                    EmployeeIDLoad();
                    //clr();
                    //lblsalary.Text = "Labour Salary Details";
                }
            }
        }
        catch (Exception ex)
        {
        }
    }
    protected void btnexitingSearch_Click(object sender, EventArgs e)
    {
        try
        {
            bool ErrFlag = false;
            string temp_exist = "";
            temp_exist = txtexistingNo.Text;
            string Cate = "";
            clr();
            txtexistingNo.Text = temp_exist;
            if (ddlcategory.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Category Staff or Labour');", true);

                ErrFlag = true;
            }
            else if (rbsalary.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Salary wages');", true);

                ErrFlag = true;
            }
            else if (ddldepartment.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select Department');", true);

                ErrFlag = true;
            }
            else if (txtexistingNo.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Existing Number');", true);

                ErrFlag = true;
            }
            if (!ErrFlag)
            {
                if (ddlcategory.SelectedValue == "1")
                {
                     Cate = "S";
                }
                else if (ddlcategory.SelectedValue == "2")
                {
                    Cate = "L";
                }
                
                DataTable dtempcode = new DataTable();
                dtempcode = objdata.SalaryMasterLabour_ForSearchingExisiting_New(ddldepartment.SelectedValue, txtexistingNo.Text, Cate, SessionCcode, SessionLcode, SessionAdmin, rbsalary.SelectedValue);
                if (dtempcode.Rows.Count > 0)
                {
                    EmpType = dtempcode.Rows[0]["EmployeeType"].ToString();

                    if (EmpType == "1")
                    {
                        PanelStaff.Visible = true;
                        PanelWorker.Visible = false;
                        PanelPermenant.Visible = false;

                    }
                    //else if (EmpType == "2")
                    //{
                    //    PanelStaff.Visible = false;
                    //    PanelWorker.Visible = false;
                    //    PanelPermenant.Visible = true;
                    //}
                    //else if (EmpType == "3")
                    //{
                    //    PanelStaff.Visible = false;
                    //    PanelWorker.Visible = true;
                    //    PanelPermenant.Visible = false;
                    //}
                    //else if (EmpType == "4")
                    //{
                    //    PanelStaff.Visible = false;
                    //    PanelWorker.Visible = false;
                    //    PanelPermenant.Visible = true;
                    //}
                    else
                    {
                        PanelStaff.Visible = true;
                        PanelWorker.Visible = false;
                        PanelPermenant.Visible = false;
                    }
                    if (dtempcode.Rows.Count > 0)
                    {
                        //ddlempCodeStaff.DataTextField = "EmpNo";
                        //ddlempCodeStaff.DataValueField = "EmpNo";
                        //ddlempCodeStaff.DataBind();
                        ddlempCodeStaff.SelectedItem.Text = dtempcode.Rows[0]["EmpNo"].ToString();
                        //ddlempCodeStaff.DataBind();
                        ddlempNameStaff.DataSource = dtempcode;
                        ddlempNameStaff.DataTextField = "EmpName";
                        ddlempNameStaff.DataValueField = "EmpNo";
                        ddlempNameStaff.DataBind();
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Employee not Found');", true);

                        ErrFlag = true;
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Employee not Found');", true);

                    ErrFlag = true;
                }
            }
        }
        catch (Exception ex)
        {
        }
    }
    protected void txtexistingNo_TextChanged(object sender, EventArgs e)
    {

    }
    protected void ddlempCode1_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            clr();
            bool ErrFlag = false;
            if (ddlempCodeStaff.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Employee Code....!');", true);
                //System.Windows.Forms.MessageBox.Show("Select the Employee Number", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            if (!ErrFlag)
            {
                DataTable dtempName = new DataTable();
                dtempName = objdata.BonusEmployeeName(ddlempCodeStaff.SelectedValue, SessionCcode, SessionLcode);
                EmpType = dtempName.Rows[0]["EmployeeType"].ToString();

                if (EmpType == "1")
                {
                    PanelStaff.Visible = true;
                    PanelWorker.Visible = false;
                    PanelPermenant.Visible = false;

                }
                //else if (EmpType == "2")
                //{
                //    PanelStaff.Visible = false;
                //    PanelWorker.Visible = false;
                //    PanelPermenant.Visible = true;
                //}
                //else if (EmpType == "3")
                //{
                //    PanelStaff.Visible = false;
                //    PanelWorker.Visible = true;
                //    PanelPermenant.Visible = false;
                //}
                //else if (EmpType == "4")
                //{
                //    PanelStaff.Visible = false;
                //    PanelWorker.Visible = false;
                //    PanelPermenant.Visible = true;
                //}
                else
                {
                    PanelStaff.Visible = true;
                    PanelWorker.Visible = false;
                    PanelPermenant.Visible = false;
                }
                //DataRow dr = dtempName.NewRow();
                //dr["EmpNo"] = "0";
                //dr["EmpName"] = ddldepartment.SelectedItem.Text;

                ddlempNameStaff.DataSource = dtempName;
                ddlempNameStaff.DataTextField = "EmpName";
                ddlempNameStaff.DataValueField = "EmpNo";
                ddlempNameStaff.DataBind();
                ddToken.DataSource = dtempName;
                ddToken.DataTextField = "ExisistingCode";
                ddToken.DataValueField = "ExisistingCode";
                ddToken.DataBind();
                //txtexistingNo.Text = dtempName.Rows[0]["ExisistingCode"].ToString();
            }
        }
        catch (Exception ex)
        {
        }
    }
    protected void ddlEmpName1_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            bool ErrFlag = false;
            clr();
            if (ddlempCodeStaff.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Employee Code....!');", true);

                ErrFlag = true;
            }
            if (!ErrFlag)
            {
                DataTable dtempName = new DataTable();
                dtempName = objdata.BonusEmployeeName(ddlempNameStaff.SelectedValue, SessionCcode, SessionLcode);
                EmpType = dtempName.Rows[0]["EmployeeType"].ToString();

                if (EmpType == "1")
                {
                    PanelStaff.Visible = true;
                    PanelWorker.Visible = false;
                    PanelPermenant.Visible = false;

                }
                //else if (EmpType == "2")
                //{
                //    PanelStaff.Visible = false;
                //    PanelWorker.Visible = false;
                //    PanelPermenant.Visible = true;
                //}
                //else if (EmpType == "3")
                //{
                //    PanelStaff.Visible = false;
                //    PanelWorker.Visible = true;
                //    PanelPermenant.Visible = false;
                //}
                //else if (EmpType == "4")
                //{
                //    PanelStaff.Visible = false;
                //    PanelWorker.Visible = false;
                //    PanelPermenant.Visible = true;
                //}
                else
                {
                    PanelStaff.Visible = true;
                    PanelWorker.Visible = false;
                    PanelPermenant.Visible = false;
                }
                ddlempCodeStaff.DataSource = dtempName;
                ddlempCodeStaff.DataTextField = "EmpNo";
                ddlempCodeStaff.DataValueField = "EmpNo";
                ddlempCodeStaff.DataBind();
                ddToken.DataSource = dtempName;
                ddToken.DataTextField = "ExisistingCode";
                ddToken.DataValueField = "ExisistingCode";
                ddToken.DataBind();
                //txtexistingNo.Text = dtempName.Rows[0]["ExisistingCode"].ToString();
            }
        }
        catch (Exception ex)
        {
        }
    }
    protected void txtsalaryday_TextChanged(object sender, EventArgs e)
    {
        //try
        //{
        //    ID = "";
        //    bool ErrFlag = false;
        //    string temp_exist = "";
        //    string temp_salary = "";
        //    bool SecondErr = false;
        //    temp_exist = txtexistingNo.Text;
        //    temp_salary = txtsalaryday.Text;
        //    clr();
        //    txtexistingNo.Text = temp_exist;
        //    txtsalaryday.Text = temp_salary;

        //    string Months = "", MonthValue = "";
        //    if (ddlcategory.SelectedValue == "")
        //    {
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select Category...!');", true);

        //        ErrFlag = true;
        //    }
        //    else if (ddlempCodeStaff.SelectedValue == "0")
        //    {
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Enter Registration Number ...!');", true);

        //        ErrFlag = true;

        //    }
        //    if (!ErrFlag)
        //    {
        //        TempDate = Convert.ToDateTime(txtsalaryday.Text).AddMonths(-1).ToShortDateString();
        //        string Month = TempDate;
        //        DateTime m;
        //        m = Convert.ToDateTime(TempDate);
        //        d = m.Month;
        //        int mon = m.Month;
        //        int yr = m.Year;
        //        #region Month
        //        string Mont = "";
        //        switch (d)
        //        {
        //            case 1:
        //                Mont = "January";
        //                break;

        //            case 2:
        //                Mont = "February";
        //                break;
        //            case 3:
        //                Mont = "March";
        //                break;
        //            case 4:
        //                Mont = "April";
        //                break;
        //            case 5:
        //                Mont = "May";
        //                break;
        //            case 6:
        //                Mont = "June";
        //                break;
        //            case 7:
        //                Mont = "July";
        //                break;
        //            case 8:
        //                Mont = "August";
        //                break;
        //            case 9:
        //                Mont = "September";
        //                break;
        //            case 10:
        //                Mont = "October";
        //                break;
        //            case 11:
        //                Mont = "November";
        //                break;
        //            case 12:
        //                Mont = "December";
        //                break;
        //            default:
        //                break;
        //        }
        //        #endregion
        //        DataTable dtAttenance = new DataTable();
        //        //dtAttenance = objdata.Salary_attenanceDetails(ddlempCodeStaff.SelectedValue, Mont, ddlFinaYear.SelectedValue, ddldepartment.SelectedValue, SessionCcode, SessionLcode);
        //        if (dtAttenance.Rows.Count > 0)
        //        {

        //            txtempdays.Text = dtAttenance.Rows[0]["Days"].ToString();
        //            txtwork.Text = dtAttenance.Rows[0]["TotalDays"].ToString();
        //            txtNFh.Text = dtAttenance.Rows[0]["NFh"].ToString();
        //            txtcl.Text = dtAttenance.Rows[0]["CL"].ToString();
        //            txtlossofpay.Text = dtAttenance.Rows[0]["AbsentDays"].ToString();
        //            txtweekoff.Text = dtAttenance.Rows[0]["weekoff"].ToString();
        //            //txtlossofpay.Text = (Convert.ToDecimal(txtwork.Text) - (Convert.ToDecimal(txtempdays.Text) + (Convert.ToDecimal(txtNFh.Text) + (Convert.ToDecimal(txtcl.Text))))).ToString();
        //            DataTable dt_ot = new DataTable();
        //            dt_ot = objdata.Attenance_ot(ddlempCodeStaff.SelectedValue, Mont, ddlFinaYear.SelectedValue, ddldepartment.SelectedValue, SessionCcode, SessionLcode);
        //            if (dt_ot.Rows.Count > 0)
        //            {
        //                txtOT.Text = dt_ot.Rows[0]["netAmount"].ToString();
        //            }
        //            else
        //            {
        //                txtOT.Text = "0";
        //            }
        //            string Salarymonth = "";
        //            //string Salarymonth = objdata.SalaryMonthFromLeave(ddlempCodeStaff.SelectedValue, ddlFinaYear.SelectedValue, SessionCcode, SessionLcode,Mont);

        //            string MonthEEE = Salarymonth.Replace(" ", "");

        //            int result = string.Compare(Salarymonth, Mont, true);
        //            //int i = DateTime.ParseExact(Salarymonth, "MMMM", CultureInfo.CurrentCulture).Month;
        //            string PayDay = objdata.SalaryPayDay();
        //            // txtattendancewithpay.Text = PayDay;
        //            if (Mont == MonthEEE)
        //            {
        //                //ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('This " + Mont + " Month Salary All Ready Processed...!');", true);
        //                ////System.Windows.Forms.MessageBox.Show("This " + Month + " month Salary Already Processed" , "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
        //                DataTable dt_val = new DataTable();
        //                DataTable dt1 = new DataTable();
        //                dt1 = objdata.NewSlary_Load(SessionCcode, SessionLcode, ddlempCodeStaff.SelectedValue);

        //                DataTable dteligible = new DataTable();
        //                DataTable dtpf = new DataTable();
        //                dtpf = objdata.Load_pf_details(SessionCcode, SessionLcode);
        //                dteligible = objdata.EligibleESI_PF(SessionCcode, SessionLcode, ddlempCodeStaff.SelectedValue);
        //                dt_val = objdata.Salary_retrive(ddlempCodeStaff.SelectedValue, ddlFinaYear.SelectedValue, Mont, SessionCcode, SessionLcode);
        //                if (dt_val.Rows.Count > 0)
        //                {
        //                    //btnEdit.Enabled = false;
        //                    //btnWadvance.Enabled = false;
        //                    //btnSadvance.Enabled = false;
        //                    if (EmpType == "1")
        //                    {
        //                        //txtSBasic.Text = dt_val.Rows[o][""].ToString();
        //                        txtSAll1.Text = dt_val.Rows[0]["allowances1"].ToString();
        //                        txtSAll2.Text = dt_val.Rows[0]["allowances2"].ToString();
        //                        txtSAll3.Text = dt_val.Rows[0]["allowances3"].ToString();
        //                        txtSAll4.Text = dt_val.Rows[0]["allowances4"].ToString();
        //                        txtSded1.Text = dt_val.Rows[0]["Deduction1"].ToString();
        //                        txtSded2.Text = dt_val.Rows[0]["Deduction2"].ToString();
        //                        txtSded3.Text = dt_val.Rows[0]["Deduction3"].ToString();
        //                        txtSded4.Text = dt_val.Rows[0]["Deduction4"].ToString();
        //                        txtSLOP.Text = dt_val.Rows[0]["Losspay"].ToString();
        //                        txtSstamp.Text = dt_val.Rows[0]["Stamp"].ToString();
        //                        txtSAdvance.Text = dt_val.Rows[0]["Advance"].ToString();
        //                        ID = dt_val.Rows[0]["SalaryId"].ToString();
        //                        AdvAmt = Convert.ToDecimal(txtSAdvance.Text);
        //                        txtSBasic.Text = (Convert.ToDecimal(dt1.Rows[0]["Base"].ToString())).ToString();
        //                        FixedBasic = dt1.Rows[0]["Base"].ToString();
        //                        FixedFDA = "0";
        //                        FixedHRA = "0";
        //                        //txtSAll1.Text = (Convert.ToDecimal(dt1.Rows[0]["Alllowance1amt"].ToString()) / 26).ToString();
        //                        txtSAll1.Text = ((Convert.ToDecimal(txtempdays.Text)) * Convert.ToDecimal(txtSAll1.Text)).ToString();
        //                        txtSAll1.Text = (Math.Round(Convert.ToDecimal(txtSAll1.Text), 0, MidpointRounding.AwayFromZero)).ToString();
        //                        //txtSAll2.Text = (Convert.ToDecimal(dt1.Rows[0]["Allowance2amt"].ToString()) / 26).ToString();
        //                        //txtSAll2.Text = ((Convert.ToDecimal(txtempdays.Text) + Convert.ToInt32(txtcl.Text)) * Convert.ToDecimal(txtSAll2.Text)).ToString();
        //                        //txtSAll2.Text = (Math.Round(Convert.ToDecimal(txtSAll2.Text), 0, MidpointRounding.AwayFromZero)).ToString();
        //                        //txtSded1.Text = (Convert.ToDecimal(dt1.Rows[0]["Deduction1"].ToString()) / 26).ToString();
        //                        //txtSded1.Text = ((Convert.ToDecimal(txtempdays.Text) + Convert.ToInt32(txtcl.Text)) * Convert.ToDecimal(txtSded1.Text)).ToString();
        //                        //txtSded1.Text = (Math.Round(Convert.ToDecimal(txtSded1.Text), 0, MidpointRounding.AwayFromZero)).ToString();
        //                        //txtSded2.Text = (Convert.ToDecimal(dt1.Rows[0]["Deduction2"].ToString()) / 26).ToString();
        //                        //txtSded2.Text = ((Convert.ToDecimal(txtempdays.Text) + Convert.ToInt32(txtcl.Text)) * Convert.ToDecimal(txtSded2.Text)).ToString();
        //                        //txtSded2.Text = (Math.Round(Convert.ToDecimal(txtSded2.Text), 0, MidpointRounding.AwayFromZero)).ToString();
        //                        //txtSLOP.Text = ((Convert.ToDecimal(txtSBasic.Text) / 26) * Convert.ToDecimal(txtlossofpay.Text)).ToString();
        //                        //txtSLOP.Text = (Math.Round(Convert.ToDecimal(txtSLOP.Text), 0, MidpointRounding.AwayFromZero)).ToString();
        //                        //txtSstamp.Text = dtpf.Rows[0]["StampCg"].ToString();
        //                        if (dteligible.Rows[0]["EligiblePF"].ToString() == "1")
        //                        {
        //                            if (dteligible.Rows[0]["PF_Type"].ToString() == "1")
        //                            {
        //                                PF_salary = "1";
        //                                if (Convert.ToDecimal(txtSBasic.Text) >= Convert.ToDecimal(dtpf.Rows[0]["StaffSalary"].ToString()))
        //                                {
        //                                    txtSPF.Text = ((Convert.ToDecimal(dtpf.Rows[0]["StaffSalary"].ToString()) * Convert.ToDecimal(dtpf.Rows[0]["PF_per"].ToString())) / 100).ToString();
        //                                    txtSPF.Text = (Math.Round(Convert.ToDecimal(txtSPF.Text), 0, MidpointRounding.AwayFromZero)).ToString();
        //                                }
        //                                else
        //                                {
        //                                    decimal PF_val = (Convert.ToDecimal(txtSBasic.Text));
        //                                    txtSPF.Text = ((PF_val * Convert.ToDecimal(dtpf.Rows[0]["PF_per"].ToString())) / 100).ToString();
        //                                    txtSPF.Text = (Math.Round(Convert.ToDecimal(txtSPF.Text), 0, MidpointRounding.AwayFromZero)).ToString();
        //                                }
        //                            }
        //                            else
        //                            {
        //                                PF_salary = "0";
        //                                decimal PF_val = (Convert.ToDecimal(txtSBasic.Text));
        //                                txtSPF.Text = ((PF_val * Convert.ToDecimal(dtpf.Rows[0]["PF_per"].ToString())) / 100).ToString();
        //                                txtSPF.Text = (Math.Round(Convert.ToDecimal(txtSPF.Text), 0, MidpointRounding.AwayFromZero)).ToString();
        //                            }

        //                        }
        //                        else
        //                        {
        //                            txtSPF.Text = "0";
        //                        }
        //                        if (dteligible.Rows[0]["ElgibleESI"].ToString() == "1")
        //                        {
        //                            decimal ESI_val = (Convert.ToDecimal(txtSBasic.Text));
        //                            txtSesi.Text = ((ESI_val * Convert.ToDecimal(dtpf.Rows[0]["ESI_per"].ToString())) / 100).ToString();
        //                            txtSesi.Text = (Math.Round(Convert.ToDecimal(txtSesi.Text), 2, MidpointRounding.ToEven)).ToString();
        //                            string[] split_val = txtSesi.Text.Split('.');
        //                            string Ist = split_val[0].ToString();
        //                            string IInd = split_val[1].ToString();

        //                            if (Convert.ToInt32(IInd) > 0)
        //                            {
        //                                txtSesi.Text = (Convert.ToInt32(Ist) + 1).ToString();
        //                            }
        //                            else
        //                            {
        //                                txtSesi.Text = Ist;
        //                            }

        //                        }
        //                        else
        //                        {
        //                            txtSesi.Text = "0";
        //                        }
        //                        //txtStamp.Text = dtpf.Rows[0]["StampCg"].ToString();
                                

        //                    }
        //                    //else if (EmpType == "2")
        //                    //{

        //                    //    txtAdvance.Text = dt_val.Rows[0]["Advance"].ToString();
        //                    //    txtOT.Text = dt_val.Rows[0]["OverTime"].ToString();
        //                    //    txtStamp.Text = dt_val.Rows[0]["Stamp"].ToString();
        //                    //    txtunion.Text = dt_val.Rows[0]["Unioncg"].ToString();
        //                    //    txtAllowance.Text = dt_val.Rows[0]["allowances1"].ToString();
        //                    //    txtAllowance2.Text = dt_val.Rows[0]["allowances2"].ToString();
        //                    //    txall3.Text = dt_val.Rows[0]["allowances3"].ToString();
        //                    //    txtsll4.Text = dt_val.Rows[0]["allowances4"].ToString();
        //                    //    txtdeduction.Text = dt_val.Rows[0]["Deduction1"].ToString();
        //                    //    txtdeduction2.Text = dt_val.Rows[0]["Deduction2"].ToString();
        //                    //    txtded3.Text = dt_val.Rows[0]["Deduction3"].ToString();
        //                    //    txtded4.Text = dt_val.Rows[0]["Deduction4"].ToString();
        //                    //    AdvAmt = Convert.ToDecimal(txtAdvance.Text);
        //                    //    ID = dt_val.Rows[0]["SalaryId"].ToString();
        //                    //    FixedBasic = dt1.Rows[0]["Base"].ToString();
        //                    //    FixedFDA = dt1.Rows[0]["FDA"].ToString();
        //                    //    FixedHRA = dt1.Rows[0]["HRA"].ToString();
        //                    //    txtbasic.Text = ((Convert.ToDecimal(dt1.Rows[0]["Base"].ToString())) / 26).ToString();
        //                    //    txtbasic.Text = ((Convert.ToDecimal(txtempdays.Text) + Convert.ToInt32(txtcl.Text) + Convert.ToInt32(txtNFh.Text)) * (Convert.ToDecimal(txtbasic.Text))).ToString();
        //                    //    txtbasic.Text = (Math.Round(Convert.ToDecimal(txtbasic.Text), 2, MidpointRounding.ToEven)).ToString();
        //                    //    txtFDA.Text = ((Convert.ToDecimal(dt1.Rows[0]["FDA"].ToString())) / 26).ToString();
        //                    //    txtFDA.Text = ((Convert.ToDecimal(txtempdays.Text) + Convert.ToInt32(txtcl.Text) + Convert.ToInt32(txtNFh.Text)) * (Convert.ToDecimal(txtFDA.Text))).ToString();
        //                    //    txtFDA.Text = (Math.Round(Convert.ToDecimal(txtFDA.Text), 2, MidpointRounding.ToEven)).ToString();
        //                    //    decimal vdaval = (Convert.ToDecimal(dtpf.Rows[0]["VDA1"].ToString()) - Convert.ToDecimal(dtpf.Rows[0]["VDA2"].ToString()));
        //                    //    txtVDA.Text = ((vdaval * Convert.ToDecimal(dt1.Rows[0]["VDA"].ToString())) / 26).ToString();
        //                    //    txtVDA.Text = ((Convert.ToDecimal(txtempdays.Text) + Convert.ToInt32(txtcl.Text) + Convert.ToInt32(txtNFh.Text)) * (Convert.ToDecimal(txtVDA.Text))).ToString();
        //                    //    txtVDA.Text = (Math.Round(Convert.ToDecimal(txtVDA.Text), 2, MidpointRounding.ToEven)).ToString();
        //                    //    txtHRA.Text = (Convert.ToDecimal(dt1.Rows[0]["HRA"].ToString()) / 26).ToString();
        //                    //    txtHRA.Text = ((Convert.ToDecimal(txtempdays.Text) + Convert.ToInt32(txtcl.Text)) * (Convert.ToDecimal(txtHRA.Text))).ToString();
        //                    //    txtHRA.Text = (Math.Round(Convert.ToDecimal(txtHRA.Text), 0, MidpointRounding.ToEven)).ToString();
        //                    //    txtStamp.Text = dtpf.Rows[0]["StampCg"].ToString();
        //                    //    if (dteligible.Rows[0]["EligiblePF"].ToString() == "1")
        //                    //    {
        //                    //        if (dteligible.Rows[0]["PF_Type"].ToString() == "1")
        //                    //        {
        //                    //            PF_salary = "1";
        //                    //            decimal PF_val_dum = (Convert.ToDecimal(txtbasic.Text) + Convert.ToDecimal(txtFDA.Text) + Convert.ToDecimal(txtVDA.Text));
        //                    //            if (PF_val_dum >= Convert.ToDecimal(dtpf.Rows[0]["StaffSalary"].ToString()))
        //                    //            {
        //                    //                txtpf.Text = ((Convert.ToDecimal(dtpf.Rows[0]["StaffSalary"].ToString()) * Convert.ToDecimal(dtpf.Rows[0]["PF_per"].ToString())) / 100).ToString();
        //                    //                txtpf.Text = (Math.Round(Convert.ToDecimal(txtpf.Text), 0, MidpointRounding.AwayFromZero)).ToString();
        //                    //            }
        //                    //            else
        //                    //            {
        //                    //                decimal PF_val = (Convert.ToDecimal(txtbasic.Text) + Convert.ToDecimal(txtFDA.Text) + Convert.ToDecimal(txtVDA.Text));
        //                    //                txtpf.Text = ((PF_val * Convert.ToDecimal(dtpf.Rows[0]["PF_per"].ToString())) / 100).ToString();
        //                    //                txtpf.Text = (Math.Round(Convert.ToDecimal(txtpf.Text), 0, MidpointRounding.AwayFromZero)).ToString();
        //                    //            }
        //                    //        }
        //                    //        else
        //                    //        {
        //                    //            PF_salary = "0";
        //                    //            decimal PF_val = (Convert.ToDecimal(txtbasic.Text) + Convert.ToDecimal(txtFDA.Text) + Convert.ToDecimal(txtVDA.Text));
        //                    //            txtpf.Text = ((PF_val * Convert.ToDecimal(dtpf.Rows[0]["PF_per"].ToString())) / 100).ToString();
        //                    //            txtpf.Text = (Math.Round(Convert.ToDecimal(txtpf.Text), 0, MidpointRounding.AwayFromZero)).ToString();
        //                    //        }

        //                    //    }
        //                    //    else
        //                    //    {
        //                    //        txtpf.Text = "0";
        //                    //    }
        //                    //    //txtAllowance.Text = (Convert.ToDecimal(dt1.Rows[0]["Alllowance1amt"].ToString()) / 26).ToString();
        //                    //    txtAllowance.Text = ((Convert.ToDecimal(txtempdays.Text)) * Convert.ToDecimal(txtAllowance.Text)).ToString();
        //                    //    txtAllowance.Text = (Math.Round(Convert.ToDecimal(txtAllowance.Text), 0, MidpointRounding.AwayFromZero)).ToString();
        //                    //    //txtAllowance2.Text = (Convert.ToDecimal(dt1.Rows[0]["Allowance2amt"].ToString()) / 26).ToString();
        //                    //    //txtAllowance2.Text = ((Convert.ToDecimal(txtempdays.Text) + Convert.ToInt32(txtcl.Text)) * Convert.ToDecimal(txtAllowance2.Text)).ToString();
        //                    //    //txtAllowance2.Text = (Math.Round(Convert.ToDecimal(txtAllowance2.Text), 0, MidpointRounding.AwayFromZero)).ToString();
        //                    //    //txtdeduction.Text = (Convert.ToDecimal(dt1.Rows[0]["Deduction1"].ToString()) / 26).ToString();
        //                    //    //txtdeduction.Text = ((Convert.ToDecimal(txtempdays.Text) + Convert.ToInt32(txtcl.Text)) * Convert.ToDecimal(txtdeduction.Text)).ToString();
        //                    //    //txtdeduction.Text = (Math.Round(Convert.ToDecimal(txtdeduction.Text), 0, MidpointRounding.AwayFromZero)).ToString();
        //                    //    //txtdeduction2.Text = (Convert.ToDecimal(dt1.Rows[0]["Deduction2"].ToString()) / 26).ToString();
        //                    //    //txtdeduction2.Text = ((Convert.ToDecimal(txtempdays.Text) + Convert.ToInt32(txtcl.Text)) * Convert.ToDecimal(txtdeduction2.Text)).ToString();
        //                    //    //txtdeduction2.Text = (Math.Round(Convert.ToDecimal(txtdeduction2.Text), 0, MidpointRounding.AwayFromZero)).ToString();
        //                    //    //txtStamp.Text = dtpf.Rows[0]["StampCg"].ToString();
        //                    //    if (dteligible.Rows[0]["ElgibleESI"].ToString() == "1")
        //                    //    {
        //                    //        decimal ESI_val = (Convert.ToDecimal(txtbasic.Text) + Convert.ToDecimal(txtFDA.Text) + Convert.ToDecimal(txtVDA.Text) + Convert.ToDecimal(txtHRA.Text));
        //                    //        txtESI.Text = ((ESI_val * Convert.ToDecimal(dtpf.Rows[0]["ESI_per"].ToString())) / 100).ToString();
        //                    //        txtESI.Text = (Math.Round(Convert.ToDecimal(txtESI.Text), 2, MidpointRounding.ToEven)).ToString();
        //                    //        string[] split_val = txtESI.Text.Split('.');
        //                    //        string Ist = split_val[0].ToString();
        //                    //        string IInd = split_val[1].ToString();

        //                    //        if (Convert.ToInt32(IInd) > 0)
        //                    //        {
        //                    //            txtESI.Text = (Convert.ToInt32(Ist) + 1).ToString();
        //                    //        }
        //                    //        else
        //                    //        {
        //                    //            txtESI.Text = Ist;
        //                    //        }

        //                    //    }
        //                    //    else
        //                    //    {
        //                    //        txtESI.Text = "0";
        //                    //    }
        //                    //}
        //                    //else if (EmpType == "3")
        //                    //{

        //                    //    txtWadvance.Text = dt_val.Rows[0]["Advance"].ToString();
        //                    //    txtWot.Text = dt_val.Rows[0]["OverTime"].ToString();
        //                    //    txtWstamp.Text = dt_val.Rows[0]["Stamp"].ToString();
                                
        //                    //    txtWAll1.Text = dt_val.Rows[0]["allowances1"].ToString();
        //                    //    txtWall2.Text = dt_val.Rows[0]["allowances2"].ToString();
        //                    //    txall3.Text = dt_val.Rows[0]["allowances3"].ToString();
        //                    //    txtsll4.Text = dt_val.Rows[0]["allowances4"].ToString();
        //                    //    txtdeduction.Text = dt_val.Rows[0]["Deduction1"].ToString();
        //                    //    txtdeduction2.Text = dt_val.Rows[0]["Deduction2"].ToString();
        //                    //    txtded3.Text = dt_val.Rows[0]["Deduction3"].ToString();
        //                    //    txtded4.Text = dt_val.Rows[0]["Deduction4"].ToString();
        //                    //    AdvAmt = Convert.ToDecimal(txtWadvance.Text);
        //                    //    ID = dt_val.Rows[0]["SalaryId"].ToString();
        //                    //    FixedBasic = (26 * (Convert.ToDecimal(dt1.Rows[0]["Base"].ToString()))).ToString();
        //                    //    FixedFDA = "0";
        //                    //    FixedHRA = "0";
        //                    //    //txtWBasic.Text = ((Convert.ToDecimal(dt1.Rows[0]["Base"].ToString())) * 26).ToString();
        //                    //    txtWBasic.Text = (((Convert.ToDecimal(txtempdays.Text)) + Convert.ToDecimal(txtNFh.Text)) * (Convert.ToDecimal(dt1.Rows[0]["Base"].ToString()))).ToString();
        //                    //    txtWBasic.Text = (Math.Round(Convert.ToDecimal(txtWBasic.Text), 2, MidpointRounding.ToEven)).ToString();
        //                    //    txtWAll1.Text = ((Convert.ToDecimal(dt1.Rows[0]["Alllowance1amt"].ToString())) * (Convert.ToDecimal(txtempdays.Text))).ToString();
        //                    //    txtWAll1.Text = (Math.Round(Convert.ToDecimal(txtWAll1.Text), 2, MidpointRounding.ToEven)).ToString();
        //                    //    //txtWall2.Text = ((Convert.ToDecimal(dt1.Rows[0]["Allowance2amt"].ToString())) * (Convert.ToDecimal(txtempdays.Text))).ToString();
        //                    //    //txtWall2.Text = (Math.Round(Convert.ToDecimal(txtWall2.Text), 2, MidpointRounding.ToEven)).ToString();
        //                    //    //txtWded1.Text = (Convert.ToDecimal(dt1.Rows[0]["Deduction1"].ToString()) * (Convert.ToDecimal(txtempdays.Text))).ToString();
        //                    //    //txtWded1.Text = (Math.Round(Convert.ToDecimal(txtWded1.Text), 2, MidpointRounding.ToEven)).ToString();
        //                    //    //txtWded2.Text = (Convert.ToDecimal(dt1.Rows[0]["Deduction2"].ToString()) * (Convert.ToDecimal(txtempdays.Text))).ToString();
        //                    //    //txtWded2.Text = (Math.Round(Convert.ToDecimal(txtWded2.Text), 2, MidpointRounding.ToEven)).ToString();
        //                    //    //txtWstamp.Text = dtpf.Rows[0]["StampCg"].ToString();
        //                    //    if (dteligible.Rows[0]["EligiblePF"].ToString() == "1")
        //                    //    {
        //                    //        if (dteligible.Rows[0]["PF_Type"].ToString() == "1")
        //                    //        {
        //                    //            PF_salary = "1";
        //                    //            if (Convert.ToDecimal(txtWBasic.Text) >= Convert.ToDecimal(dtpf.Rows[0]["StaffSalary"].ToString()))
        //                    //            {
        //                    //                txtWpf.Text = ((Convert.ToDecimal(dtpf.Rows[0]["StaffSalary"].ToString()) * Convert.ToDecimal(dtpf.Rows[0]["PF_per"].ToString())) / 100).ToString();
        //                    //                txtWpf.Text = (Math.Round(Convert.ToDecimal(txtWpf.Text), 0, MidpointRounding.AwayFromZero)).ToString();
        //                    //            }
        //                    //            else
        //                    //            {
        //                    //                decimal PF_val = (Convert.ToDecimal(txtWBasic.Text));
        //                    //                txtWpf.Text = ((PF_val * Convert.ToDecimal(dtpf.Rows[0]["PF_per"].ToString())) / 100).ToString();
        //                    //                txtWpf.Text = (Math.Round(Convert.ToDecimal(txtWpf.Text), 0, MidpointRounding.AwayFromZero)).ToString();
        //                    //            }
        //                    //        }
        //                    //        else
        //                    //        {
        //                    //            PF_salary = "0";
        //                    //            decimal PF_val = (Convert.ToDecimal(txtWBasic.Text));
        //                    //            txtWpf.Text = ((PF_val * Convert.ToDecimal(dtpf.Rows[0]["PF_per"].ToString())) / 100).ToString();
        //                    //            txtWpf.Text = (Math.Round(Convert.ToDecimal(txtWpf.Text), 0, MidpointRounding.AwayFromZero)).ToString();
        //                    //        }

        //                    //    }
        //                    //    else
        //                    //    {
        //                    //        txtWpf.Text = "0";
        //                    //    }
        //                    //    if (dteligible.Rows[0]["ElgibleESI"].ToString() == "1")
        //                    //    {
        //                    //        decimal ESI_val = (Convert.ToDecimal(txtWBasic.Text));
        //                    //        txtWesi.Text = ((ESI_val * Convert.ToDecimal(dtpf.Rows[0]["ESI_per"].ToString())) / 100).ToString();
        //                    //        txtWesi.Text = (Math.Round(Convert.ToDecimal(txtWesi.Text), 2, MidpointRounding.ToEven)).ToString();
        //                    //        string[] split_val = txtWesi.Text.Split('.');
        //                    //        string Ist = split_val[0].ToString();
        //                    //        string IInd = split_val[1].ToString();

        //                    //        if (Convert.ToInt32(IInd) > 0)
        //                    //        {
        //                    //            txtWesi.Text = (Convert.ToInt32(Ist) + 1).ToString();
        //                    //        }
        //                    //        else
        //                    //        {
        //                    //            txtWesi.Text = Ist;
        //                    //        }

        //                    //    }
        //                    //    else
        //                    //    {
        //                    //        txtWesi.Text = "0";
        //                    //    }
        //                    //}
        //                    //else if (EmpType == "4")
        //                    //{
        //                    //    txtAdvance.Text = dt_val.Rows[0]["Advance"].ToString();
        //                    //    txtOT.Text = dt_val.Rows[0]["OverTime"].ToString();
        //                    //    txtStamp.Text = dt_val.Rows[0]["Stamp"].ToString();
        //                    //    txtunion.Text = dt_val.Rows[0]["Unioncg"].ToString();
        //                    //    txtAllowance.Text = dt_val.Rows[0]["allowances1"].ToString();
        //                    //    txtAllowance2.Text = dt_val.Rows[0]["allowances2"].ToString();
        //                    //    txall3.Text = dt_val.Rows[0]["allowances3"].ToString();
        //                    //    txtsll4.Text = dt_val.Rows[0]["allowances4"].ToString();
        //                    //    txtdeduction.Text = dt_val.Rows[0]["Deduction1"].ToString();
        //                    //    txtdeduction2.Text = dt_val.Rows[0]["Deduction2"].ToString();
        //                    //    txtded3.Text = dt_val.Rows[0]["Deduction3"].ToString();
        //                    //    txtded4.Text = dt_val.Rows[0]["Deduction4"].ToString();
        //                    //    AdvAmt = Convert.ToDecimal(txtAdvance.Text);
        //                    //    ID = dt_val.Rows[0]["SalaryId"].ToString();
        //                    //    FixedBasic = (26 * (Convert.ToDecimal(dt1.Rows[0]["Base"].ToString()))).ToString();
        //                    //    FixedFDA = "0";
        //                    //    FixedHRA = "0";
        //                    //    txtbasic.Text = ((Convert.ToDecimal(txtempdays.Text) + Convert.ToInt32(txtcl.Text) + Convert.ToInt32(txtNFh.Text)) * (Convert.ToDecimal(dt1.Rows[0]["Base"].ToString()))).ToString();
        //                    //    txtbasic.Text = (Math.Round(Convert.ToDecimal(txtbasic.Text), 2, MidpointRounding.ToEven)).ToString();
        //                    //    txtFDA.Text = "0";
        //                    //    txtVDA.Text = "0";
        //                    //    txtHRA.Text = "0";
        //                    //    txtStamp.Text = dtpf.Rows[0]["StampCg"].ToString();
        //                    //    if (dteligible.Rows[0]["EligiblePF"].ToString() == "1")
        //                    //    {
        //                    //        if (dteligible.Rows[0]["PF_Type"].ToString() == "1")
        //                    //        {

        //                    //            PF_salary = "1";
        //                    //            decimal PF_val_dum1 = (Convert.ToDecimal(txtbasic.Text) + Convert.ToDecimal(txtFDA.Text) + Convert.ToDecimal(txtVDA.Text));
        //                    //            if (PF_val_dum1 >= Convert.ToDecimal(dtpf.Rows[0]["StaffSalary"].ToString()))
        //                    //            {
        //                    //                txtpf.Text = ((Convert.ToDecimal(dtpf.Rows[0]["StaffSalary"].ToString()) * Convert.ToDecimal(dtpf.Rows[0]["PF_per"].ToString())) / 100).ToString();
        //                    //                txtpf.Text = (Math.Round(Convert.ToDecimal(txtpf.Text), 0, MidpointRounding.AwayFromZero)).ToString();
        //                    //            }
        //                    //            else
        //                    //            {
        //                    //                decimal PF_val = (Convert.ToDecimal(txtbasic.Text) + Convert.ToDecimal(txtFDA.Text) + Convert.ToDecimal(txtVDA.Text));
        //                    //                txtpf.Text = ((PF_val * Convert.ToDecimal(dtpf.Rows[0]["PF_per"].ToString())) / 100).ToString();
        //                    //                txtpf.Text = (Math.Round(Convert.ToDecimal(txtpf.Text), 0, MidpointRounding.AwayFromZero)).ToString();
        //                    //            }
                                        
        //                    //        }
        //                        //    else
        //                        //    {
        //                        //        PF_salary = "0";
        //                        //        decimal PF_val = (Convert.ToDecimal(txtbasic.Text) + Convert.ToDecimal(txtFDA.Text) + Convert.ToDecimal(txtVDA.Text));
        //                        //        txtpf.Text = ((PF_val * Convert.ToDecimal(dtpf.Rows[0]["PF_per"].ToString())) / 100).ToString();
        //                        //        txtpf.Text = (Math.Round(Convert.ToDecimal(txtpf.Text), 0, MidpointRounding.AwayFromZero)).ToString();
        //                        //    }

        //                        //}
        //                        //else
        //                        //{
        //                        //    txtpf.Text = "0";
        //                        //}
        //                        ////txtAllowance.Text = (Convert.ToDecimal(dt1.Rows[0]["Alllowance1amt"].ToString()) / 26).ToString();
        //                        //txtAllowance.Text = ((Convert.ToDecimal(txtempdays.Text)) * Convert.ToDecimal(txtAllowance.Text)).ToString();
        //                        //txtAllowance.Text = (Math.Round(Convert.ToDecimal(txtAllowance.Text), 0, MidpointRounding.AwayFromZero)).ToString();
        //                        //txtAllowance2.Text = (Convert.ToDecimal(dt1.Rows[0]["Allowance2amt"].ToString()) / 26).ToString();
        //                        //txtAllowance2.Text = ((Convert.ToDecimal(txtempdays.Text) + Convert.ToInt32(txtcl.Text)) * Convert.ToDecimal(txtAllowance2.Text)).ToString();
        //                        //txtAllowance2.Text = (Math.Round(Convert.ToDecimal(txtAllowance2.Text), 0, MidpointRounding.AwayFromZero)).ToString();
        //                        //txtdeduction.Text = (Convert.ToDecimal(dt1.Rows[0]["Deduction1"].ToString()) / 26).ToString();
        //                        //txtdeduction.Text = ((Convert.ToDecimal(txtempdays.Text) + Convert.ToInt32(txtcl.Text)) * Convert.ToDecimal(txtdeduction.Text)).ToString();
        //                        //txtdeduction.Text = (Math.Round(Convert.ToDecimal(txtdeduction.Text), 0, MidpointRounding.AwayFromZero)).ToString();
        //                        //txtdeduction2.Text = (Convert.ToDecimal(dt1.Rows[0]["Deduction2"].ToString()) / 26).ToString();
        //                        //txtdeduction2.Text = ((Convert.ToDecimal(txtempdays.Text) + Convert.ToInt32(txtcl.Text)) * Convert.ToDecimal(txtdeduction2.Text)).ToString();
        //                        //txtdeduction2.Text = (Math.Round(Convert.ToDecimal(txtdeduction2.Text), 0, MidpointRounding.AwayFromZero)).ToString();
        //                        //txtStamp.Text = dtpf.Rows[0]["StampCg"].ToString();
        //                    //    if (dteligible.Rows[0]["ElgibleESI"].ToString() == "1")
        //                    //    {
        //                    //        decimal ESI_val = (Convert.ToDecimal(txtbasic.Text) + Convert.ToDecimal(txtFDA.Text) + Convert.ToDecimal(txtVDA.Text) + Convert.ToDecimal(txtHRA.Text));
        //                    //        txtESI.Text = ((ESI_val * Convert.ToDecimal(dtpf.Rows[0]["ESI_per"].ToString())) / 100).ToString();
        //                    //        txtESI.Text = (Math.Round(Convert.ToDecimal(txtESI.Text), 2, MidpointRounding.ToEven)).ToString();
        //                    //        string[] split_val = txtESI.Text.Split('.');
        //                    //        string Ist = split_val[0].ToString();
        //                    //        string IInd = split_val[1].ToString();

        //                    //        if (Convert.ToInt32(IInd) > 0)
        //                    //        {
        //                    //            txtESI.Text = (Convert.ToInt32(Ist) + 1).ToString();
        //                    //        }
        //                    //        else
        //                    //        {
        //                    //            txtESI.Text = Ist;
        //                    //        }

        //                    //    }
        //                    //    else
        //                    //    {
        //                    //        txtESI.Text = "0";
        //                    //    }
        //                    //}
        //                    else
        //                    {

        //                        txtAdvance.Text = dt_val.Rows[0]["Advance"].ToString();
        //                        txtOT.Text = dt_val.Rows[0]["OverTime"].ToString();
        //                        txtStamp.Text = dt_val.Rows[0]["Stamp"].ToString();
        //                        txtunion.Text = dt_val.Rows[0]["Unioncg"].ToString();
        //                        txtAllowance.Text = dt_val.Rows[0]["allowances1"].ToString();
        //                        txtAllowance2.Text = dt_val.Rows[0]["allowances2"].ToString();
        //                        txall3.Text = dt_val.Rows[0]["allowances3"].ToString();
        //                        txtsll4.Text = dt_val.Rows[0]["allowances4"].ToString();
        //                        txtdeduction.Text = dt_val.Rows[0]["Deduction1"].ToString();
        //                        txtdeduction2.Text = dt_val.Rows[0]["Deduction2"].ToString();
        //                        txtded3.Text = dt_val.Rows[0]["Deduction3"].ToString();
        //                        txtded4.Text = dt_val.Rows[0]["Deduction4"].ToString();
        //                        AdvAmt = Convert.ToDecimal(txtAdvance.Text);
        //                        ID = dt_val.Rows[0]["SalaryId"].ToString();
        //                        FixedBasic = dt1.Rows[0]["Base"].ToString();
        //                        FixedFDA = dt1.Rows[0]["FDA"].ToString();
        //                        FixedHRA = dt1.Rows[0]["HRA"].ToString();
        //                        txtbasic.Text = ((Convert.ToDecimal(dt1.Rows[0]["Base"].ToString())) / 26).ToString();
        //                        txtbasic.Text = ((Convert.ToDecimal(txtempdays.Text) + Convert.ToInt32(txtcl.Text) + Convert.ToInt32(txtNFh.Text)) * (Convert.ToDecimal(txtbasic.Text))).ToString();
        //                        txtbasic.Text = (Math.Round(Convert.ToDecimal(txtbasic.Text), 2, MidpointRounding.ToEven)).ToString();
        //                        txtFDA.Text = ((Convert.ToDecimal(dt1.Rows[0]["FDA"].ToString())) / 26).ToString();
        //                        txtFDA.Text = ((Convert.ToDecimal(txtempdays.Text) + Convert.ToInt32(txtcl.Text) + Convert.ToInt32(txtNFh.Text)) * (Convert.ToDecimal(txtFDA.Text))).ToString();
        //                        txtFDA.Text = (Math.Round(Convert.ToDecimal(txtFDA.Text), 2, MidpointRounding.ToEven)).ToString();
        //                        decimal vdaval = (Convert.ToDecimal(dtpf.Rows[0]["VDA1"].ToString()) - Convert.ToDecimal(dtpf.Rows[0]["VDA2"].ToString()));
        //                        txtVDA.Text = ((vdaval * Convert.ToDecimal(dt1.Rows[0]["VDA"].ToString())) / 26).ToString();
        //                        txtVDA.Text = ((Convert.ToDecimal(txtempdays.Text) + Convert.ToInt32(txtcl.Text) + Convert.ToInt32(txtNFh.Text)) * (Convert.ToDecimal(txtVDA.Text))).ToString();
        //                        txtVDA.Text = (Math.Round(Convert.ToDecimal(txtVDA.Text), 2, MidpointRounding.ToEven)).ToString();
        //                        txtHRA.Text = (Convert.ToDecimal(dt1.Rows[0]["HRA"].ToString()) / 26).ToString();
        //                        txtHRA.Text = ((Convert.ToDecimal(txtempdays.Text) + Convert.ToInt32(txtcl.Text)) * (Convert.ToDecimal(txtHRA.Text))).ToString();
        //                        txtHRA.Text = (Math.Round(Convert.ToDecimal(txtHRA.Text), 0, MidpointRounding.ToEven)).ToString();
        //                        txtStamp.Text = dtpf.Rows[0]["StampCg"].ToString();
        //                        if (dteligible.Rows[0]["EligiblePF"].ToString() == "1")
        //                        {
        //                            if (dteligible.Rows[0]["PF_Type"].ToString() == "1")
        //                            {
        //                                PF_salary = "1";
        //                                decimal PF_val_dum = (Convert.ToDecimal(txtbasic.Text) + Convert.ToDecimal(txtFDA.Text) + Convert.ToDecimal(txtVDA.Text));
        //                                if (PF_val_dum >= Convert.ToDecimal(dtpf.Rows[0]["StaffSalary"].ToString()))
        //                                {
        //                                    txtpf.Text = ((Convert.ToDecimal(dtpf.Rows[0]["StaffSalary"].ToString()) * Convert.ToDecimal(dtpf.Rows[0]["PF_per"].ToString())) / 100).ToString();
        //                                    txtpf.Text = (Math.Round(Convert.ToDecimal(txtpf.Text), 0, MidpointRounding.AwayFromZero)).ToString();
        //                                }
        //                                else
        //                                {
        //                                    decimal PF_val = (Convert.ToDecimal(txtbasic.Text) + Convert.ToDecimal(txtFDA.Text) + Convert.ToDecimal(txtVDA.Text));
        //                                    txtpf.Text = ((PF_val * Convert.ToDecimal(dtpf.Rows[0]["PF_per"].ToString())) / 100).ToString();
        //                                    txtpf.Text = (Math.Round(Convert.ToDecimal(txtpf.Text), 0, MidpointRounding.AwayFromZero)).ToString();
        //                                }
        //                            }
        //                            else
        //                            {
        //                                PF_salary = "0";
        //                                decimal PF_val = (Convert.ToDecimal(txtbasic.Text) + Convert.ToDecimal(txtFDA.Text) + Convert.ToDecimal(txtVDA.Text));
        //                                txtpf.Text = ((PF_val * Convert.ToDecimal(dtpf.Rows[0]["PF_per"].ToString())) / 100).ToString();
        //                                txtpf.Text = (Math.Round(Convert.ToDecimal(txtpf.Text), 0, MidpointRounding.AwayFromZero)).ToString();
        //                            }

        //                        }
        //                        else
        //                        {
        //                            txtpf.Text = "0";
        //                        }
        //                        //txtAllowance.Text = (Convert.ToDecimal(dt1.Rows[0]["Alllowance1amt"].ToString()) / 26).ToString();
        //                        txtAllowance.Text = ((Convert.ToDecimal(txtempdays.Text)) * Convert.ToDecimal(txtAllowance.Text)).ToString();
        //                        txtAllowance.Text = (Math.Round(Convert.ToDecimal(txtAllowance.Text), 0, MidpointRounding.AwayFromZero)).ToString();
        //                        //txtAllowance2.Text = (Convert.ToDecimal(dt1.Rows[0]["Allowance2amt"].ToString()) / 26).ToString();
        //                        //txtAllowance2.Text = ((Convert.ToDecimal(txtempdays.Text) + Convert.ToInt32(txtcl.Text)) * Convert.ToDecimal(txtAllowance2.Text)).ToString();
        //                        //txtAllowance2.Text = (Math.Round(Convert.ToDecimal(txtAllowance2.Text), 0, MidpointRounding.AwayFromZero)).ToString();
        //                        //txtdeduction.Text = (Convert.ToDecimal(dt1.Rows[0]["Deduction1"].ToString()) / 26).ToString();
        //                        //txtdeduction.Text = ((Convert.ToDecimal(txtempdays.Text) + Convert.ToInt32(txtcl.Text)) * Convert.ToDecimal(txtdeduction.Text)).ToString();
        //                        //txtdeduction.Text = (Math.Round(Convert.ToDecimal(txtdeduction.Text), 0, MidpointRounding.AwayFromZero)).ToString();
        //                        //txtdeduction2.Text = (Convert.ToDecimal(dt1.Rows[0]["Deduction2"].ToString()) / 26).ToString();
        //                        //txtdeduction2.Text = ((Convert.ToDecimal(txtempdays.Text) + Convert.ToInt32(txtcl.Text)) * Convert.ToDecimal(txtdeduction2.Text)).ToString();
        //                        //txtdeduction2.Text = (Math.Round(Convert.ToDecimal(txtdeduction2.Text), 0, MidpointRounding.AwayFromZero)).ToString();
        //                        //txtStamp.Text = dtpf.Rows[0]["StampCg"].ToString();
        //                        if (dteligible.Rows[0]["ElgibleESI"].ToString() == "1")
        //                        {
        //                            decimal ESI_val = (Convert.ToDecimal(txtbasic.Text) + Convert.ToDecimal(txtFDA.Text) + Convert.ToDecimal(txtVDA.Text) + Convert.ToDecimal(txtHRA.Text));
        //                            txtESI.Text = ((ESI_val * Convert.ToDecimal(dtpf.Rows[0]["ESI_per"].ToString())) / 100).ToString();
        //                            txtESI.Text = (Math.Round(Convert.ToDecimal(txtESI.Text), 2, MidpointRounding.ToEven)).ToString();
        //                            string[] split_val = txtESI.Text.Split('.');
        //                            string Ist = split_val[0].ToString();
        //                            string IInd = split_val[1].ToString();

        //                            if (Convert.ToInt32(IInd) > 0)
        //                            {
        //                                txtESI.Text = (Convert.ToInt32(Ist) + 1).ToString();
        //                            }
        //                            else
        //                            {
        //                                txtESI.Text = Ist;
        //                            }

        //                        }
        //                        else
        //                        {
        //                            txtESI.Text = "0";
        //                        }
        //                    }
        //                    DataTable dtadv = new DataTable();
        //                    dtadv = objdata.Salary_advance_retrive(ddlempCodeStaff.SelectedValue, SessionCcode, SessionLcode);
        //                    if (dtadv.Rows.Count > 0)
        //                    {
        //                        Adv_id = dtadv.Rows[0]["ID"].ToString();
        //                        Adv_BalanceAmt = dtadv.Rows[0]["BalanceAmount"].ToString();
        //                        Adv_due = dtadv.Rows[0]["MonthlyDeduction"].ToString();
        //                        Increment_mont = dtadv.Rows[0]["IncreaseMonth"].ToString();

        //                        Dec_mont = dtadv.Rows[0]["ReductionMonth"].ToString();
        //                        //if (Dec_mont == "1")
        //                        //{
        //                        //    txtAdvance.Text = Adv_BalanceAmt;
        //                        //}
        //                        //else if (Convert.ToDecimal(Adv_BalanceAmt) < Convert.ToDecimal(Adv_due))
        //                        //{
        //                        //    txtAdvance.Text = Adv_BalanceAmt;
        //                        //}
        //                        //else
        //                        //{
        //                        //    txtAdvance.Text = dtadv.Rows[0]["MonthlyDeduction"].ToString();
        //                        //}
        //                    }

        //                }
        //                else
        //                {
        //                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('This " + Mont + " Month Salary All Ready Processed...!');", true);
        //                    //System.Windows.Forms.MessageBox.Show("This " + Month + " month Salary Already Processed" , "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                           
        //                }

        //                SecondErr = true;
        //            }
        //            if (!SecondErr)
        //            {
        //                AdvAmt = 0;
        //                ID = "";
        //                DataTable dt1 = new DataTable();
        //                dt1 = objdata.NewSlary_Load(SessionCcode, SessionLcode, ddlempCodeStaff.SelectedValue);
                        
        //                DataTable dteligible = new DataTable();
        //                DataTable dtpf = new DataTable();
        //                dtpf = objdata.Load_pf_details(SessionCcode, SessionLcode);
        //                dteligible = objdata.EligibleESI_PF(SessionCcode, SessionLcode, ddlempCodeStaff.SelectedValue);
        //                txtStamp.Text = dtpf.Rows[0]["StampCg"].ToString();
        //                if (dtpf.Rows[0]["Union_val"].ToString() == "1")
        //                {
        //                    txtunion.Text = dt1.Rows[0]["Unioncharges"].ToString();
        //                }
        //                else
        //                {
        //                    txtunion.Text = "0";
        //                }
                        
        //                if (dt1.Rows.Count > 0)
        //                {
        //                    if (EmpType == "1")
        //                    {
        //                        txtSBasic.Text = (Convert.ToDecimal(dt1.Rows[0]["Base"].ToString())).ToString();
        //                        //txtSAll1.Text = (Convert.ToDecimal(dt1.Rows[0]["Alllowance1amt"].ToString()) / 26).ToString();
        //                        txtSAll1.Text = ((Convert.ToDecimal(txtempdays.Text)) * Convert.ToDecimal(txtSAll1.Text)).ToString();
        //                        txtSAll1.Text = (Math.Round(Convert.ToDecimal(txtSAll1.Text), 0, MidpointRounding.AwayFromZero)).ToString();
        //                        txtSAll2.Text = (Convert.ToDecimal(dt1.Rows[0]["Allowance2amt"].ToString()) / 26).ToString();
        //                        txtSAll2.Text = ((Convert.ToDecimal(txtempdays.Text) + Convert.ToInt32(txtcl.Text)) * Convert.ToDecimal(txtSAll2.Text)).ToString();
        //                        txtSAll2.Text = (Math.Round(Convert.ToDecimal(txtSAll2.Text), 0, MidpointRounding.AwayFromZero)).ToString();
        //                        txtSded1.Text = (Convert.ToDecimal(dt1.Rows[0]["Deduction1"].ToString()) / 26).ToString();
        //                        txtSded1.Text = ((Convert.ToDecimal(txtempdays.Text) + Convert.ToInt32(txtcl.Text)) * Convert.ToDecimal(txtSded1.Text)).ToString();
        //                        txtSded1.Text = (Math.Round(Convert.ToDecimal(txtSded1.Text), 0, MidpointRounding.AwayFromZero)).ToString();
        //                        txtSded2.Text = (Convert.ToDecimal(dt1.Rows[0]["Deduction2"].ToString()) / 26).ToString();
        //                        txtSded2.Text = ((Convert.ToDecimal(txtempdays.Text) + Convert.ToInt32(txtcl.Text)) * Convert.ToDecimal(txtSded2.Text)).ToString();
        //                        txtSded2.Text = (Math.Round(Convert.ToDecimal(txtSded2.Text), 0, MidpointRounding.AwayFromZero)).ToString();
        //                        txtSLOP.Text = ((Convert.ToDecimal(txtSBasic.Text) / 26) * Convert.ToDecimal(txtlossofpay.Text)).ToString();
        //                        txtSLOP.Text = (Math.Round(Convert.ToDecimal(txtSLOP.Text), 0, MidpointRounding.AwayFromZero)).ToString();
        //                        txtSstamp.Text = dtpf.Rows[0]["StampCg"].ToString();
        //                        if (dteligible.Rows[0]["EligiblePF"].ToString() == "1")
        //                        {
        //                            if (dteligible.Rows[0]["PF_Type"].ToString() == "1")
        //                            {
        //                                PF_salary = "1";
        //                                if (Convert.ToDecimal(txtSBasic.Text) >= Convert.ToDecimal(dtpf.Rows[0]["StaffSalary"].ToString()))
        //                                {
        //                                    txtSPF.Text = ((Convert.ToDecimal(dtpf.Rows[0]["StaffSalary"].ToString()) * Convert.ToDecimal(dtpf.Rows[0]["PF_per"].ToString())) / 100).ToString();
        //                                    txtSPF.Text = (Math.Round(Convert.ToDecimal(txtSPF.Text), 0, MidpointRounding.AwayFromZero)).ToString();
        //                                }
        //                                else
        //                                {
        //                                    decimal PF_val = (Convert.ToDecimal(txtSBasic.Text));
        //                                    txtSPF.Text = ((PF_val * Convert.ToDecimal(dtpf.Rows[0]["PF_per"].ToString())) / 100).ToString();
        //                                    txtSPF.Text = (Math.Round(Convert.ToDecimal(txtSPF.Text), 0, MidpointRounding.AwayFromZero)).ToString();
        //                                }
        //                            }
        //                            else
        //                            {
        //                                PF_salary = "0";
        //                                decimal PF_val = (Convert.ToDecimal(txtSBasic.Text));
        //                                txtSPF.Text = ((PF_val * Convert.ToDecimal(dtpf.Rows[0]["PF_per"].ToString())) / 100).ToString();
        //                                txtSPF.Text = (Math.Round(Convert.ToDecimal(txtSPF.Text), 0, MidpointRounding.AwayFromZero)).ToString();
        //                            }

        //                        }
        //                        else
        //                        {
        //                            txtSPF.Text = "0";
        //                        }
        //                        if (dteligible.Rows[0]["ElgibleESI"].ToString() == "1")
        //                        {
        //                            decimal ESI_val = (Convert.ToDecimal(txtSBasic.Text));
        //                            txtSesi.Text = ((ESI_val * Convert.ToDecimal(dtpf.Rows[0]["ESI_per"].ToString())) / 100).ToString();
        //                            txtSesi.Text = (Math.Round(Convert.ToDecimal(txtSesi.Text), 2, MidpointRounding.ToEven)).ToString();
        //                            string[] split_val = txtSesi.Text.Split('.');
        //                            string Ist = split_val[0].ToString();
        //                            string IInd = split_val[1].ToString();

        //                            if (Convert.ToInt32(IInd) > 0)
        //                            {
        //                                txtSesi.Text = (Convert.ToInt32(Ist) + 1).ToString();
        //                            }
        //                            else
        //                            {
        //                                txtSesi.Text = Ist;
        //                            }

        //                        }
        //                        else
        //                        {
        //                            txtSesi.Text = "0";
        //                        }
        //                        txtStamp.Text = dtpf.Rows[0]["StampCg"].ToString();
        //                    }
        //                    //else if (EmpType == "2")
        //                    //{
        //                    //    txtbasic.Text = ((Convert.ToDecimal(dt1.Rows[0]["Base"].ToString())) / 26).ToString();
        //                    //    txtbasic.Text = ((Convert.ToDecimal(txtempdays.Text) + Convert.ToInt32(txtcl.Text) + Convert.ToInt32(txtNFh.Text)) * (Convert.ToDecimal(txtbasic.Text))).ToString();
        //                    //    txtbasic.Text = (Math.Round(Convert.ToDecimal(txtbasic.Text), 2, MidpointRounding.ToEven)).ToString();
        //                    //    txtFDA.Text = ((Convert.ToDecimal(dt1.Rows[0]["FDA"].ToString())) / 26).ToString();
        //                    //    txtFDA.Text = ((Convert.ToDecimal(txtempdays.Text) + Convert.ToDecimal(txtcl.Text) + Convert.ToDecimal(txtNFh.Text)) * (Convert.ToDecimal(txtFDA.Text))).ToString();
        //                    //    txtFDA.Text = (Math.Round(Convert.ToDecimal(txtFDA.Text), 2, MidpointRounding.ToEven)).ToString();
        //                    //    decimal vdaval = (Convert.ToDecimal(dtpf.Rows[0]["VDA1"].ToString()) - Convert.ToDecimal(dtpf.Rows[0]["VDA2"].ToString()));
        //                    //    txtVDA.Text = ((vdaval * Convert.ToDecimal(dt1.Rows[0]["VDA"].ToString())) / 26).ToString();
        //                    //    txtVDA.Text = ((Convert.ToDecimal(txtempdays.Text) + Convert.ToInt32(txtcl.Text) + Convert.ToInt32(txtNFh.Text)) * (Convert.ToDecimal(txtVDA.Text))).ToString();
        //                    //    txtVDA.Text = (Math.Round(Convert.ToDecimal(txtVDA.Text), 2, MidpointRounding.ToEven)).ToString();
        //                    //    txtHRA.Text = (Convert.ToDecimal(dt1.Rows[0]["HRA"].ToString()) / 26).ToString();
        //                    //    txtHRA.Text = ((Convert.ToDecimal(txtempdays.Text) + Convert.ToInt32(txtcl.Text)) * (Convert.ToDecimal(txtHRA.Text))).ToString();
        //                    //    txtHRA.Text = (Math.Round(Convert.ToDecimal(txtHRA.Text), 0, MidpointRounding.ToEven)).ToString();
        //                    //    txtStamp.Text = dtpf.Rows[0]["StampCg"].ToString();
        //                    //    if (dteligible.Rows[0]["EligiblePF"].ToString() == "1")
        //                    //    {
        //                    //        if (dteligible.Rows[0]["PF_Type"].ToString() == "1")
        //                    //        {
        //                    //            PF_salary = "1";
        //                    //            decimal PF_val_dum = (Convert.ToDecimal(txtbasic.Text) + Convert.ToDecimal(txtFDA.Text) + Convert.ToDecimal(txtVDA.Text));
        //                    //            if (PF_val_dum >= Convert.ToDecimal(dtpf.Rows[0]["StaffSalary"].ToString()))
        //                    //            {
        //                    //                txtpf.Text = ((Convert.ToDecimal(dtpf.Rows[0]["StaffSalary"].ToString()) * Convert.ToDecimal(dtpf.Rows[0]["PF_per"].ToString())) / 100).ToString();
        //                    //                txtpf.Text = (Math.Round(Convert.ToDecimal(txtpf.Text), 0, MidpointRounding.AwayFromZero)).ToString();
        //                    //            }
        //                    //            else
        //                    //            {
        //                    //                decimal PF_val = (Convert.ToDecimal(txtbasic.Text) + Convert.ToDecimal(txtFDA.Text) + Convert.ToDecimal(txtVDA.Text));
        //                    //                txtpf.Text = ((PF_val * Convert.ToDecimal(dtpf.Rows[0]["PF_per"].ToString())) / 100).ToString();
        //                    //                txtpf.Text = (Math.Round(Convert.ToDecimal(txtpf.Text), 0, MidpointRounding.AwayFromZero)).ToString();
        //                    //            }
        //                    //        }
        //                    //        else
        //                    //        {
        //                    //            PF_salary = "0";
        //                    //            decimal PF_val = (Convert.ToDecimal(txtbasic.Text) + Convert.ToDecimal(txtFDA.Text) + Convert.ToDecimal(txtVDA.Text));
        //                    //            txtpf.Text = ((PF_val * Convert.ToDecimal(dtpf.Rows[0]["PF_per"].ToString())) / 100).ToString();
        //                    //            txtpf.Text = (Math.Round(Convert.ToDecimal(txtpf.Text), 0, MidpointRounding.AwayFromZero)).ToString();
        //                    //        }
                                    
        //                    //    }
        //                    //    else
        //                    //    {
        //                    //        txtpf.Text = "0";
        //                    //    }
        //                    //    //txtAllowance.Text = (Convert.ToDecimal(dt1.Rows[0]["Alllowance1amt"].ToString()) / 26).ToString();
        //                    //    txtAllowance.Text = ((Convert.ToDecimal(txtempdays.Text)) * Convert.ToDecimal(txtAllowance.Text)).ToString();
        //                    //    txtAllowance.Text = (Math.Round(Convert.ToDecimal(txtAllowance.Text), 0, MidpointRounding.AwayFromZero)).ToString();
        //                    //    txtAllowance2.Text = (Convert.ToDecimal(dt1.Rows[0]["Allowance2amt"].ToString()) / 26).ToString();
        //                    //    txtAllowance2.Text = ((Convert.ToDecimal(txtempdays.Text) + Convert.ToInt32(txtcl.Text)) * Convert.ToDecimal(txtAllowance2.Text)).ToString();
        //                    //    txtAllowance2.Text = (Math.Round(Convert.ToDecimal(txtAllowance2.Text), 0, MidpointRounding.AwayFromZero)).ToString();
        //                    //    txtdeduction.Text = (Convert.ToDecimal(dt1.Rows[0]["Deduction1"].ToString()) / 26).ToString();
        //                    //    txtdeduction.Text = ((Convert.ToDecimal(txtempdays.Text) + Convert.ToInt32(txtcl.Text)) * Convert.ToDecimal(txtdeduction.Text)).ToString();
        //                    //    txtdeduction.Text = (Math.Round(Convert.ToDecimal(txtdeduction.Text), 0, MidpointRounding.AwayFromZero)).ToString();
        //                    //    txtdeduction2.Text = (Convert.ToDecimal(dt1.Rows[0]["Deduction2"].ToString()) / 26).ToString();
        //                    //    txtdeduction2.Text = ((Convert.ToDecimal(txtempdays.Text) + Convert.ToInt32(txtcl.Text)) * Convert.ToDecimal(txtdeduction2.Text)).ToString();
        //                    //    txtdeduction2.Text = (Math.Round(Convert.ToDecimal(txtdeduction2.Text), 0, MidpointRounding.AwayFromZero)).ToString();
        //                    //    txtStamp.Text = dtpf.Rows[0]["StampCg"].ToString();
        //                    //    if (dteligible.Rows[0]["ElgibleESI"].ToString() == "1")
        //                    //    {
        //                    //        decimal ESI_val = (Convert.ToDecimal(txtbasic.Text) + Convert.ToDecimal(txtFDA.Text) + Convert.ToDecimal(txtVDA.Text) + Convert.ToDecimal(txtHRA.Text));
        //                    //        txtESI.Text = ((ESI_val * Convert.ToDecimal(dtpf.Rows[0]["ESI_per"].ToString())) / 100).ToString();
        //                    //        txtESI.Text = (Math.Round(Convert.ToDecimal(txtESI.Text), 2, MidpointRounding.ToEven)).ToString();
        //                    //        string[] split_val = txtESI.Text.Split('.');
        //                    //        string Ist = split_val[0].ToString();
        //                    //        string IInd = split_val[1].ToString();

        //                    //        if (Convert.ToInt32(IInd) > 0)
        //                    //        {
        //                    //            txtESI.Text = (Convert.ToInt32(Ist) + 1).ToString();
        //                    //        }
        //                    //        else
        //                    //        {
        //                    //            txtESI.Text = Ist;
        //                    //        }

        //                    //    }
        //                    //    else
        //                    //    {
        //                    //        txtESI.Text = "0";
        //                    //    }
        //                    //}
        //                    //else if (EmpType == "3")
        //                    //{
        //                    //    //txtWBasic.Text = ((Convert.ToDecimal(dt1.Rows[0]["Base"].ToString())) * 26).ToString();
        //                    //    txtWBasic.Text = (((Convert.ToDecimal(txtempdays.Text)) + Convert.ToDecimal(txtNFh.Text)) * (Convert.ToDecimal(dt1.Rows[0]["Base"].ToString()))).ToString();
        //                    //    txtWBasic.Text = (Math.Round(Convert.ToDecimal(txtWBasic.Text), 2, MidpointRounding.ToEven)).ToString();
        //                    //    txtWAll1.Text = ((Convert.ToDecimal(dt1.Rows[0]["Alllowance1amt"].ToString())) * (Convert.ToDecimal(txtempdays.Text))).ToString();
        //                    //    txtWAll1.Text = (Math.Round(Convert.ToDecimal(txtWAll1.Text), 2, MidpointRounding.ToEven)).ToString();
        //                    //    txtWall2.Text = ((Convert.ToDecimal(dt1.Rows[0]["Allowance2amt"].ToString())) * (Convert.ToDecimal(txtempdays.Text))).ToString();
        //                    //    txtWall2.Text = (Math.Round(Convert.ToDecimal(txtWall2.Text), 2, MidpointRounding.ToEven)).ToString();
        //                    //    txtWded1.Text = (Convert.ToDecimal(dt1.Rows[0]["Deduction1"].ToString()) * (Convert.ToDecimal(txtempdays.Text))).ToString();
        //                    //    txtWded1.Text = (Math.Round(Convert.ToDecimal(txtWded1.Text), 2, MidpointRounding.ToEven)).ToString();
        //                    //    txtWded2.Text = (Convert.ToDecimal(dt1.Rows[0]["Deduction2"].ToString()) * (Convert.ToDecimal(txtempdays.Text))).ToString();
        //                    //    txtWded2.Text = (Math.Round(Convert.ToDecimal(txtWded2.Text), 2, MidpointRounding.ToEven)).ToString();
        //                    //    txtWstamp.Text = dtpf.Rows[0]["StampCg"].ToString();
        //                    //    if (dteligible.Rows[0]["EligiblePF"].ToString() == "1")
        //                    //    {
        //                    //        if (dteligible.Rows[0]["PF_Type"].ToString() == "1")
        //                    //        {
        //                    //            PF_salary = "1";
        //                    //            if (Convert.ToDecimal(txtWBasic.Text) >= Convert.ToDecimal(dtpf.Rows[0]["StaffSalary"].ToString()))
        //                    //            {
        //                    //                txtWpf.Text = ((Convert.ToDecimal(dtpf.Rows[0]["StaffSalary"].ToString()) * Convert.ToDecimal(dtpf.Rows[0]["PF_per"].ToString())) / 100).ToString();
        //                    //                txtWpf.Text = (Math.Round(Convert.ToDecimal(txtWpf.Text), 0, MidpointRounding.AwayFromZero)).ToString();
        //                    //            }
        //                    //            else
        //                    //            {
        //                    //                decimal PF_val = (Convert.ToDecimal(txtWBasic.Text));
        //                    //                txtWpf.Text = ((PF_val * Convert.ToDecimal(dtpf.Rows[0]["PF_per"].ToString())) / 100).ToString();
        //                    //                txtWpf.Text = (Math.Round(Convert.ToDecimal(txtWpf.Text), 0, MidpointRounding.AwayFromZero)).ToString();
        //                    //            }
        //                    //        }
        //                    //        else
        //                    //        {
        //                    //            PF_salary = "0";
        //                    //            decimal PF_val = (Convert.ToDecimal(txtWBasic.Text));
        //                    //            txtWpf.Text = ((PF_val * Convert.ToDecimal(dtpf.Rows[0]["PF_per"].ToString())) / 100).ToString();
        //                    //            txtWpf.Text = (Math.Round(Convert.ToDecimal(txtWpf.Text), 0, MidpointRounding.AwayFromZero)).ToString();
        //                    //        }

        //                    //    }
        //                    //    else
        //                    //    {
        //                    //        txtWpf.Text = "0";
        //                    //    }
        //                    //    if (dteligible.Rows[0]["ElgibleESI"].ToString() == "1")
        //                    //    {
        //                    //        decimal ESI_val = (Convert.ToDecimal(txtWBasic.Text));
        //                    //        txtWesi.Text = ((ESI_val * Convert.ToDecimal(dtpf.Rows[0]["ESI_per"].ToString())) / 100).ToString();
        //                    //        txtWesi.Text = (Math.Round(Convert.ToDecimal(txtWesi.Text), 2, MidpointRounding.ToEven)).ToString();
        //                    //        string[] split_val = txtWesi.Text.Split('.');
        //                    //        string Ist = split_val[0].ToString();
        //                    //        string IInd = split_val[1].ToString();

        //                    //        if (Convert.ToInt32(IInd) > 0)
        //                    //        {
        //                    //            txtWesi.Text = (Convert.ToInt32(Ist) + 1).ToString();
        //                    //        }
        //                    //        else
        //                    //        {
        //                    //            txtWesi.Text = Ist;
        //                    //        }

        //                    //    }
        //                    //    else
        //                    //    {
        //                    //        txtWesi.Text = "0";
        //                    //    }
        //                    //}
        //                    //else if (EmpType == "4")
        //                    //{
        //                    //    //txtbasic.Text = ((Convert.ToDecimal(dt1.Rows[0]["Base"].ToString())) * 26).ToString();
        //                    //    txtbasic.Text = ((Convert.ToDecimal(txtempdays.Text) + Convert.ToInt32(txtcl.Text) + Convert.ToInt32(txtNFh.Text)) * (Convert.ToDecimal(dt1.Rows[0]["Base"].ToString()))).ToString();
        //                    //    txtbasic.Text = (Math.Round(Convert.ToDecimal(txtbasic.Text), 2, MidpointRounding.ToEven)).ToString();
        //                    //    txtFDA.Text = "0";
        //                    //    txtVDA.Text = "0";
        //                    //    txtHRA.Text = "0";
        //                    //    if (dteligible.Rows[0]["EligiblePF"].ToString() == "1")
        //                    //    {
        //                    //        if (dteligible.Rows[0]["PF_Type"].ToString() == "1")
        //                    //        {
        //                    //            PF_salary = "1";
        //                    //            decimal PF_val_dum1 = (Convert.ToDecimal(txtbasic.Text) + Convert.ToDecimal(txtFDA.Text) + Convert.ToDecimal(txtVDA.Text));
        //                    //            if (PF_val_dum1 >= Convert.ToDecimal(dtpf.Rows[0]["StaffSalary"].ToString()))
        //                    //            {
        //                    //                txtpf.Text = ((Convert.ToDecimal(dtpf.Rows[0]["StaffSalary"].ToString()) * Convert.ToDecimal(dtpf.Rows[0]["PF_per"].ToString())) / 100).ToString();
        //                    //                txtpf.Text = (Math.Round(Convert.ToDecimal(txtpf.Text), 0, MidpointRounding.AwayFromZero)).ToString();
        //                    //            }
        //                    //            else
        //                    //            {
        //                    //                decimal PF_val = (Convert.ToDecimal(txtbasic.Text) + Convert.ToDecimal(txtFDA.Text) + Convert.ToDecimal(txtVDA.Text));
        //                    //                txtpf.Text = ((PF_val * Convert.ToDecimal(dtpf.Rows[0]["PF_per"].ToString())) / 100).ToString();
        //                    //                txtpf.Text = (Math.Round(Convert.ToDecimal(txtpf.Text), 0, MidpointRounding.AwayFromZero)).ToString();
        //                    //            }
        //                    //        }
        //                    //        else
        //                    //        {
        //                    //            PF_salary = "0";
        //                    //            decimal PF_val = (Convert.ToDecimal(txtbasic.Text) + Convert.ToDecimal(txtFDA.Text) + Convert.ToDecimal(txtVDA.Text));
        //                    //            txtpf.Text = ((PF_val * Convert.ToDecimal(dtpf.Rows[0]["PF_per"].ToString())) / 100).ToString();
        //                    //            txtpf.Text = (Math.Round(Convert.ToDecimal(txtpf.Text), 0, MidpointRounding.AwayFromZero)).ToString();
        //                    //        }

        //                    //    }
        //                    //    else
        //                    //    {
        //                    //        txtpf.Text = "0";
        //                    //    }
        //                    //    //txtAllowance.Text = (Convert.ToDecimal(dt1.Rows[0]["Alllowance1amt"].ToString()) / 26).ToString();
        //                    //    txtAllowance.Text = ((Convert.ToDecimal(txtempdays.Text)) * Convert.ToDecimal(txtAllowance.Text)).ToString();
        //                    //    txtAllowance.Text = (Math.Round(Convert.ToDecimal(txtAllowance.Text), 0, MidpointRounding.AwayFromZero)).ToString();
        //                    //    txtAllowance2.Text = (Convert.ToDecimal(dt1.Rows[0]["Allowance2amt"].ToString()) / 26).ToString();
        //                    //    txtAllowance2.Text = ((Convert.ToDecimal(txtempdays.Text) + Convert.ToInt32(txtcl.Text)) * Convert.ToDecimal(txtAllowance2.Text)).ToString();
        //                    //    txtAllowance2.Text = (Math.Round(Convert.ToDecimal(txtAllowance2.Text), 0, MidpointRounding.AwayFromZero)).ToString();
        //                    //    txtdeduction.Text = (Convert.ToDecimal(dt1.Rows[0]["Deduction1"].ToString()) / 26).ToString();
        //                    //    txtdeduction.Text = ((Convert.ToDecimal(txtempdays.Text) + Convert.ToInt32(txtcl.Text)) * Convert.ToDecimal(txtdeduction.Text)).ToString();
        //                    //    txtdeduction.Text = (Math.Round(Convert.ToDecimal(txtdeduction.Text), 0, MidpointRounding.AwayFromZero)).ToString();
        //                    //    txtdeduction2.Text = (Convert.ToDecimal(dt1.Rows[0]["Deduction2"].ToString()) / 26).ToString();
        //                    //    txtdeduction2.Text = ((Convert.ToDecimal(txtempdays.Text) + Convert.ToInt32(txtcl.Text)) * Convert.ToDecimal(txtdeduction2.Text)).ToString();
        //                    //    txtdeduction2.Text = (Math.Round(Convert.ToDecimal(txtdeduction2.Text), 0, MidpointRounding.AwayFromZero)).ToString();
        //                    //    if (dteligible.Rows[0]["ElgibleESI"].ToString() == "1")
        //                    //    {
        //                    //        decimal ESI_val = (Convert.ToDecimal(txtbasic.Text) + Convert.ToDecimal(txtFDA.Text) + Convert.ToDecimal(txtVDA.Text) + Convert.ToDecimal(txtHRA.Text));
        //                    //        txtESI.Text = ((ESI_val * Convert.ToDecimal(dtpf.Rows[0]["ESI_per"].ToString())) / 100).ToString();
        //                    //        txtESI.Text = (Math.Round(Convert.ToDecimal(txtESI.Text), 2, MidpointRounding.ToEven)).ToString();
        //                    //        string[] split_val = txtESI.Text.Split('.');
        //                    //        string Ist = split_val[0].ToString();
        //                    //        string IInd = split_val[1].ToString();

        //                    //        if (Convert.ToInt32(IInd) > 0)
        //                    //        {
        //                    //            txtESI.Text = (Convert.ToInt32(Ist) + 1).ToString();
        //                    //        }
        //                    //        else
        //                    //        {
        //                    //            txtESI.Text = Ist;
        //                    //        }

        //                    //    }
        //                    //    else
        //                    //    {
        //                    //        txtESI.Text = "0";
        //                    //    }
        //                    //}
        //                    else
        //                    {
        //                        txtbasic.Text = ((Convert.ToDecimal(dt1.Rows[0]["Base"].ToString())) / 26).ToString();
        //                        txtbasic.Text = ((Convert.ToDecimal(txtempdays.Text) + Convert.ToInt32(txtcl.Text) + Convert.ToInt32(txtNFh.Text)) * (Convert.ToDecimal(txtbasic.Text))).ToString();
        //                        txtbasic.Text = (Math.Round(Convert.ToDecimal(txtbasic.Text), 2, MidpointRounding.ToEven)).ToString();
        //                        txtFDA.Text = ((Convert.ToDecimal(dt1.Rows[0]["FDA"].ToString())) / 26).ToString();
        //                        txtFDA.Text = ((Convert.ToDecimal(txtempdays.Text) + Convert.ToDecimal(txtcl.Text) + Convert.ToDecimal(txtNFh.Text)) * (Convert.ToDecimal(txtFDA.Text))).ToString();
        //                        txtFDA.Text = (Math.Round(Convert.ToDecimal(txtFDA.Text), 2, MidpointRounding.ToEven)).ToString();
        //                        decimal vdaval = (Convert.ToDecimal(dtpf.Rows[0]["VDA1"].ToString()) - Convert.ToDecimal(dtpf.Rows[0]["VDA2"].ToString()));
        //                        txtVDA.Text = ((vdaval * Convert.ToDecimal(dt1.Rows[0]["VDA"].ToString())) / 26).ToString();
        //                        txtVDA.Text = ((Convert.ToDecimal(txtempdays.Text) + Convert.ToInt32(txtcl.Text) + Convert.ToInt32(txtNFh.Text)) * (Convert.ToDecimal(txtVDA.Text))).ToString();
        //                        txtVDA.Text = (Math.Round(Convert.ToDecimal(txtVDA.Text), 2, MidpointRounding.ToEven)).ToString();
        //                        txtHRA.Text = (Convert.ToDecimal(dt1.Rows[0]["HRA"].ToString()) / 26).ToString();
        //                        txtHRA.Text = ((Convert.ToDecimal(txtempdays.Text) + Convert.ToInt32(txtcl.Text)) * (Convert.ToDecimal(txtHRA.Text))).ToString();
        //                        txtHRA.Text = (Math.Round(Convert.ToDecimal(txtHRA.Text), 0, MidpointRounding.ToEven)).ToString();
        //                        txtStamp.Text = dtpf.Rows[0]["StampCg"].ToString();
        //                        if (dteligible.Rows[0]["EligiblePF"].ToString() == "1")
        //                        {
        //                            if (dteligible.Rows[0]["PF_Type"].ToString() == "1")
        //                            {
        //                                PF_salary = "1";
        //                                decimal PF_val_dum = (Convert.ToDecimal(txtbasic.Text) + Convert.ToDecimal(txtFDA.Text) + Convert.ToDecimal(txtVDA.Text));
        //                                if (PF_val_dum >= Convert.ToDecimal(dtpf.Rows[0]["StaffSalary"].ToString()))
        //                                {
        //                                    txtpf.Text = ((Convert.ToDecimal(dtpf.Rows[0]["StaffSalary"].ToString()) * Convert.ToDecimal(dtpf.Rows[0]["PF_per"].ToString())) / 100).ToString();
        //                                    txtpf.Text = (Math.Round(Convert.ToDecimal(txtpf.Text), 0, MidpointRounding.AwayFromZero)).ToString();
        //                                }
        //                                else
        //                                {
        //                                    decimal PF_val = (Convert.ToDecimal(txtbasic.Text) + Convert.ToDecimal(txtFDA.Text) + Convert.ToDecimal(txtVDA.Text));
        //                                    txtpf.Text = ((PF_val * Convert.ToDecimal(dtpf.Rows[0]["PF_per"].ToString())) / 100).ToString();
        //                                    txtpf.Text = (Math.Round(Convert.ToDecimal(txtpf.Text), 0, MidpointRounding.AwayFromZero)).ToString();
        //                                }
        //                            }
        //                            else
        //                            {
        //                                PF_salary = "0";
        //                                decimal PF_val = (Convert.ToDecimal(txtbasic.Text) + Convert.ToDecimal(txtFDA.Text) + Convert.ToDecimal(txtVDA.Text));
        //                                txtpf.Text = ((PF_val * Convert.ToDecimal(dtpf.Rows[0]["PF_per"].ToString())) / 100).ToString();
        //                                txtpf.Text = (Math.Round(Convert.ToDecimal(txtpf.Text), 0, MidpointRounding.AwayFromZero)).ToString();
        //                            }

        //                        }
        //                        else
        //                        {
        //                            txtpf.Text = "0";
        //                        }
        //                        //txtAllowance.Text = (Convert.ToDecimal(dt1.Rows[0]["Alllowance1amt"].ToString()) / 26).ToString();
        //                        txtAllowance.Text = ((Convert.ToDecimal(txtempdays.Text)) * Convert.ToDecimal(txtAllowance.Text)).ToString();
        //                        txtAllowance.Text = (Math.Round(Convert.ToDecimal(txtAllowance.Text), 0, MidpointRounding.AwayFromZero)).ToString();
        //                        txtAllowance2.Text = (Convert.ToDecimal(dt1.Rows[0]["Allowance2amt"].ToString()) / 26).ToString();
        //                        txtAllowance2.Text = ((Convert.ToDecimal(txtempdays.Text) + Convert.ToInt32(txtcl.Text)) * Convert.ToDecimal(txtAllowance2.Text)).ToString();
        //                        txtAllowance2.Text = (Math.Round(Convert.ToDecimal(txtAllowance2.Text), 0, MidpointRounding.AwayFromZero)).ToString();
        //                        txtdeduction.Text = (Convert.ToDecimal(dt1.Rows[0]["Deduction1"].ToString()) / 26).ToString();
        //                        txtdeduction.Text = ((Convert.ToDecimal(txtempdays.Text) + Convert.ToInt32(txtcl.Text)) * Convert.ToDecimal(txtdeduction.Text)).ToString();
        //                        txtdeduction.Text = (Math.Round(Convert.ToDecimal(txtdeduction.Text), 0, MidpointRounding.AwayFromZero)).ToString();
        //                        txtdeduction2.Text = (Convert.ToDecimal(dt1.Rows[0]["Deduction2"].ToString()) / 26).ToString();
        //                        txtdeduction2.Text = ((Convert.ToDecimal(txtempdays.Text) + Convert.ToInt32(txtcl.Text)) * Convert.ToDecimal(txtdeduction2.Text)).ToString();
        //                        txtdeduction2.Text = (Math.Round(Convert.ToDecimal(txtdeduction2.Text), 0, MidpointRounding.AwayFromZero)).ToString();
        //                        txtStamp.Text = dtpf.Rows[0]["StampCg"].ToString();
        //                        if (dteligible.Rows[0]["ElgibleESI"].ToString() == "1")
        //                        {
        //                            decimal ESI_val = (Convert.ToDecimal(txtbasic.Text) + Convert.ToDecimal(txtFDA.Text) + Convert.ToDecimal(txtVDA.Text) + Convert.ToDecimal(txtHRA.Text));
        //                            txtESI.Text = ((ESI_val * Convert.ToDecimal(dtpf.Rows[0]["ESI_per"].ToString())) / 100).ToString();
        //                            txtESI.Text = (Math.Round(Convert.ToDecimal(txtESI.Text), 2, MidpointRounding.ToEven)).ToString();
        //                            string[] split_val = txtESI.Text.Split('.');
        //                            string Ist = split_val[0].ToString();
        //                            string IInd = split_val[1].ToString();

        //                            if (Convert.ToInt32(IInd) > 0)
        //                            {
        //                                txtESI.Text = (Convert.ToInt32(Ist) + 1).ToString();
        //                            }
        //                            else
        //                            {
        //                                txtESI.Text = Ist;
        //                            }

        //                        }
        //                        else
        //                        {
        //                            txtESI.Text = "0";
        //                        }
        //                    }
        //                    DataTable dtadv = new DataTable();
        //                    dtadv = objdata.Salary_advance_retrive(ddlempCodeStaff.SelectedValue, SessionCcode, SessionLcode);
        //                    if (dtadv.Rows.Count > 0)
        //                    {
        //                        Adv_id = dtadv.Rows[0]["ID"].ToString();
        //                        Adv_BalanceAmt = dtadv.Rows[0]["BalanceAmount"].ToString();
        //                        Adv_due = dtadv.Rows[0]["MonthlyDeduction"].ToString();
        //                        Increment_mont = dtadv.Rows[0]["IncreaseMonth"].ToString();

        //                        Dec_mont = dtadv.Rows[0]["ReductionMonth"].ToString();
        //                        if (Dec_mont == "1")
        //                        {
        //                            txtAdvance.Text = Adv_BalanceAmt;
        //                            txtSAdvance.Text = Adv_BalanceAmt;
        //                            txtWadvance.Text = Adv_BalanceAmt;
        //                        }
        //                        else if (Convert.ToDecimal(Adv_BalanceAmt) < Convert.ToDecimal(Adv_due))
        //                        {
        //                            txtAdvance.Text = Adv_BalanceAmt;
        //                            txtSAdvance.Text = Adv_BalanceAmt;
        //                            txtWadvance.Text = Adv_BalanceAmt;
        //                        }
        //                        else
        //                        {
        //                            txtAdvance.Text = dtadv.Rows[0]["MonthlyDeduction"].ToString();
        //                            txtSAdvance.Text = dtadv.Rows[0]["MonthlyDeduction"].ToString();
        //                            txtWadvance.Text = dtadv.Rows[0]["MonthlyDeduction"].ToString();
        //                        }
        //                    }
                            
        //                    string dt_ot1 = "";
        //                    dt_ot1 = objdata.OT_Salary(ddlempCodeStaff.SelectedValue, SessionCcode, SessionLcode, Mont);
        //                    if (dt_ot1.Trim() != "")
        //                    {
        //                        txtOT.Text = dt_ot1.ToString();
        //                    }
        //                    else
        //                    {
        //                        txtOT.Text = "0";
        //                    }
        //                }

        //            }
        //        }
        //        else
        //        {
        //            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Upload the Employee Details ...!');", true);
        //            txtsalaryday.Text = "";
        //        }
        //    }
        //}
        //catch (Exception ex)
        //{
        //}
    }
    protected void rbtnsalarythroug_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void btnsave_Click(object sender, EventArgs e)
     {
        try
        {
            bool ErrFlag = false;
            string pfsalary_val = "";
            bool isRegistered = false;
            string constr = ConfigurationManager.AppSettings["ConnectionString"];
            SqlConnection con = new SqlConnection(constr);
            if (txtsalaryday.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select  Salary Date');", true);
                //System.Windows.Forms.MessageBox.Show("Select the Salary Date", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            //else if (btnEdit.Text == "Ok")
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Calculate the salary Properly ');", true);
            //    //System.Windows.Forms.MessageBox.Show("Calculate the Salary Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            //    ErrFlag = true;
            //}
            //if (txtbasic.Text.Trim() == "")
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Basic Salary Amount');", true);
            //    ErrFlag = true;
            //}
            //else if (txtFDA.Text.Trim() == "")
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the FDA Amount');", true);
            //    ErrFlag = true;
            //}
            //else if (txtVDA.Text.Trim() == "")
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the VDA Amount');", true);
            //    ErrFlag = true;
            //}
            //else if (txtHRA.Text.Trim() == "")
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the HRA Amount');", true);
            //    ErrFlag = true;
            //}
            //else if (txtOT.Text.Trim() == "")
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the OT Amount');", true);
            //    ErrFlag = true;
            //}
            //else if (txtAllowance.Text.Trim() == "")
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Allowance Amount');", true);
            //    ErrFlag = true;
            //}
            //else if (txtpf.Text.Trim() == "")
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the PF Amount');", true);
            //    ErrFlag = true;
            //}
            //else if (txtESI.Text.Trim() == "")
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the ESI Amount');", true);
            //    ErrFlag = true;
            //}
            //else if (txtAdvance.Text.Trim() == "")
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Advance Amount');", true);
            //    ErrFlag = true;
            //}
            //else if (txtStamp.Text.Trim() == "")
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Stamp Amount');", true);
            //    ErrFlag = true;
            //}
            //else if (txtunion.Text.Trim() == "")
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Union Amount');", true);
            //    ErrFlag = true;
            //}
            //else if (txtdeduction.Text.Trim() == "")
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Deduction Amount');", true);
            //    ErrFlag = true;
            //}
            else if (EarningsAmt.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Click the Calculate Button');", true);
                ErrFlag = true;
            }
            else if (DeductionAmt.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Click the Calculate Button');", true);
                ErrFlag = true;
            }
            else if (lblNetPayAmt.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Click the Calculate Button');", true);
                ErrFlag = true;
            }
            else if (lbltext.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Click the Calculate Button');", true);
                ErrFlag = true;
            }
            else if (txthome.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Homw Town Days Properly');", true);
                ErrFlag = true;
            }
            if (!ErrFlag)
            {
                //if (EmpType == "1")
                //{
                    if (txtSBasic.Text.Trim() == "")
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Basic Salary Amount');", true);
                        ErrFlag = true;
                    }
                    else if (txtSPF.Text.Trim() == "")
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the PF Amount');", true);
                        ErrFlag = true;
                    }
                    else if (txtSesi.Text.Trim() == "")
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the ESI Amount');", true);
                        ErrFlag = true;
                    }
                    else if (txtSAll1.Text.Trim() == "")
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Allowance 1 Amount');", true);
                        ErrFlag = true;
                    }
                    else if (txtSAll2.Text.Trim() == "")
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Allowance 2 Amount');", true);
                        ErrFlag = true;
                    }
                    else if (txtSAll3.Text.Trim() == "")
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Allowance 3 Amount');", true);
                        ErrFlag = true;
                    }
                    else if (txtSAll4.Text.Trim() == "")
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Allowance 4 Amount');", true);
                        ErrFlag = true;
                    }
                    else if (txtSAll5.Text.Trim() == "")
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Allowance 5 Amount');", true);
                        ErrFlag = true;
                    }
                    else if (txtSAdvance.Text.Trim() == "")
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Advance Amount');", true);
                        ErrFlag = true;
                    }
                    else if (txtSstamp.Text.Trim() == "")
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Stamp Amount');", true);
                        ErrFlag = true;
                    }
                    else if (txtSded1.Text.Trim() == "")
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Deduction 1 Amount');", true);
                        ErrFlag = true;
                    }
                    else if (txtSded2.Text.Trim() == "")
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Deduction 2 Amount');", true);
                        ErrFlag = true;
                    }
                    else if (txtSded3.Text.Trim() == "")
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Deduction 3 Amount');", true);
                        ErrFlag = true;
                    }
                    else if (txtSded4.Text.Trim() == "")
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Deduction 4 Amount');", true);
                        ErrFlag = true;
                    }
                    else if (txtSded5.Text.Trim() == "")
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Deduction 5 Amount');", true);
                        ErrFlag = true;
                    }
                    else if (txtSLOP.Text.Trim() == "")
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the LOP Amount');", true);
                        ErrFlag = true;
                    }
                //}
                //else if (EmpType == "2")
                //{
                //    if (txtbasic.Text.Trim() == "")
                //    {
                //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Basic Salary Amount');", true);
                //        ErrFlag = true;
                //    }
                //    else if (txtFDA.Text.Trim() == "")
                //    {
                //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the FDA Amount');", true);
                //        ErrFlag = true;
                //    }
                //    else if (txtVDA.Text.Trim() == "")
                //    {
                //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the VDA Amount');", true);
                //        ErrFlag = true;
                //    }
                //    else if (txtHRA.Text.Trim() == "")
                //    {
                //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the HRA Amount');", true);
                //        ErrFlag = true;
                //    }
                //    else if (txtOT.Text.Trim() == "")
                //    {
                //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the OT Amount');", true);
                //        ErrFlag = true;
                //    }
                //    else if (txtAllowance.Text.Trim() == "")
                //    {
                //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Allowance Amount');", true);
                //        ErrFlag = true;
                //    }
                //    else if (txtpf.Text.Trim() == "")
                //    {
                //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the PF Amount');", true);
                //        ErrFlag = true;
                //    }
                //    else if (txtESI.Text.Trim() == "")
                //    {
                //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the ESI Amount');", true);
                //        ErrFlag = true;
                //    }
                //    else if (txtAdvance.Text.Trim() == "")
                //    {
                //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Advance Amount');", true);
                //        ErrFlag = true;
                //    }
                //    else if (txtStamp.Text.Trim() == "")
                //    {
                //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Stamp Amount');", true);
                //        ErrFlag = true;
                //    }
                //    else if (txtunion.Text.Trim() == "")
                //    {
                //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Union Amount');", true);
                //        ErrFlag = true;
                //    }
                //    else if (txtdeduction.Text.Trim() == "")
                //    {
                //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Deduction Amount');", true);
                //        ErrFlag = true;
                //    }
                //}
                //else if (EmpType == "3")
                //{
                //    if (txtWBasic.Text.Trim() == "")
                //    {
                //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Basic Salary Amount');", true);
                //        ErrFlag = true;
                //    }
                //    else if (txtWot.Text.Trim() == "")
                //    {
                //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the OT Amount');", true);
                //        ErrFlag = true;
                //    }
                //    else if (txtWAll1.Text.Trim() == "")
                //    {
                //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Allowance 1 Amount');", true);
                //        ErrFlag = true;
                //    }
                //    else if (txtWall2.Text.Trim() == "")
                //    {
                //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Allowance 2 Amount');", true);
                //        ErrFlag = true;
                //    }
                //    else if (txtWall3.Text.Trim() == "")
                //    {
                //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Allowance 3 Amount');", true);
                //        ErrFlag = true;
                //    }
                //    else if (txtWall4.Text.Trim() == "")
                //    {
                //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Allowance 4 Amount');", true);
                //        ErrFlag = true;
                //    }
                //    else if (txtWpf.Text.Trim() == "")
                //    {
                //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the PF Amount');", true);
                //        ErrFlag = true;
                //    }
                //    else if (txtWesi.Text.Trim() == "")
                //    {
                //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the ESI Amount');", true);
                //        ErrFlag = true;
                //    }
                //    else if (txtWadvance.Text.Trim() == "")
                //    {
                //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Advance Amount');", true);
                //        ErrFlag = true;
                //    }
                //    else if (txtWded1.Text.Trim() == "")
                //    {
                //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Deduction 1 Amount');", true);
                //        ErrFlag = true;
                //    }
                //    else if (txtWded2.Text.Trim() == "")
                //    {
                //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Deduction 2 Amount');", true);
                //        ErrFlag = true;
                //    }
                //    else if (txtWDed3.Text.Trim() == "")
                //    {
                //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Deduction 3 Amount');", true);
                //        ErrFlag = true;
                //    }
                //    else if (txtWded4.Text.Trim() == "")
                //    {
                //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Deduction 4 Amount');", true);
                //        ErrFlag = true;
                //    }
                //}
                //else if (EmpType == "4")
                //{
                //    if (txtbasic.Text.Trim() == "")
                //    {
                //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Basic Salary Amount');", true);
                //        ErrFlag = true;
                //    }
                //    else if (txtFDA.Text.Trim() == "")
                //    {
                //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the FDA Amount');", true);
                //        ErrFlag = true;
                //    }
                //    else if (txtVDA.Text.Trim() == "")
                //    {
                //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the VDA Amount');", true);
                //        ErrFlag = true;
                //    }
                //    else if (txtHRA.Text.Trim() == "")
                //    {
                //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the HRA Amount');", true);
                //        ErrFlag = true;
                //    }
                //    else if (txtOT.Text.Trim() == "")
                //    {
                //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the OT Amount');", true);
                //        ErrFlag = true;
                //    }
                //    else if (txtAllowance.Text.Trim() == "")
                //    {
                //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Allowance 1 Amount');", true);
                //        ErrFlag = true;
                //    }
                //    else if (txtpf.Text.Trim() == "")
                //    {
                //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the PF Amount');", true);
                //        ErrFlag = true;
                //    }
                //    else if (txtESI.Text.Trim() == "")
                //    {
                //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the ESI Amount');", true);
                //        ErrFlag = true;
                //    }
                //    else if (txtAdvance.Text.Trim() == "")
                //    {
                //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Advance Amount');", true);
                //        ErrFlag = true;
                //    }
                //    else if (txtStamp.Text.Trim() == "")
                //    {
                //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Stamp Amount');", true);
                //        ErrFlag = true;
                //    }
                //    else if (txtunion.Text.Trim() == "")
                //    {
                //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Union Amount');", true);
                //        ErrFlag = true;
                //    }
                //    else if (txtdeduction.Text.Trim() == "")
                //    {
                //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Deduction 1 Amount');", true);
                //        ErrFlag = true;
                //    }
                //    else if (txtdeduction2.Text.Trim() == "")
                //    {
                //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Deduction 2 Amount');", true);
                //        ErrFlag = true;
                //    }
                //    else if (txtded3.Text.Trim() == "")
                //    {
                //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Deduction 3 Amount');", true);
                //        ErrFlag = true;
                //    }
                //    else if (txtded4.Text.Trim() == "")
                //    {
                //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Deduction 4 Amount');", true);
                //        ErrFlag = true;
                //    }
                //    else if (txall3.Text.Trim() == "")
                //    {
                //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Allowance 3 Amount');", true);
                //        ErrFlag = true;
                //    }
                //    else if (txtAllowance2.Text.Trim() == "")
                //    {
                //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Allowance 2 Amount');", true);
                //        ErrFlag = true;
                //    }
                //    else if (txtsll4.Text.Trim() == "")
                //    {
                //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Allowance 4 Amount');", true);
                //        ErrFlag = true;
                //    }
                //}
                //else
                //{
                //    if (txtbasic.Text.Trim() == "")
                //    {
                //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Basic Salary Amount');", true);
                //        ErrFlag = true;
                //    }
                //    else if (txtFDA.Text.Trim() == "")
                //    {
                //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the FDA Amount');", true);
                //        ErrFlag = true;
                //    }
                //    else if (txtVDA.Text.Trim() == "")
                //    {
                //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the VDA Amount');", true);
                //        ErrFlag = true;
                //    }
                //    else if (txtHRA.Text.Trim() == "")
                //    {
                //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the HRA Amount');", true);
                //        ErrFlag = true;
                //    }
                //    else if (txtOT.Text.Trim() == "")
                //    {
                //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the OT Amount');", true);
                //        ErrFlag = true;
                //    }
                //    else if (txtAllowance.Text.Trim() == "")
                //    {
                //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Allowance Amount');", true);
                //        ErrFlag = true;
                //    }
                //    else if (txtpf.Text.Trim() == "")
                //    {
                //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the PF Amount');", true);
                //        ErrFlag = true;
                //    }
                //    else if (txtESI.Text.Trim() == "")
                //    {
                //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the ESI Amount');", true);
                //        ErrFlag = true;
                //    }
                //    else if (txtAdvance.Text.Trim() == "")
                //    {
                //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Advance Amount');", true);
                //        ErrFlag = true;
                //    }
                //    else if (txtStamp.Text.Trim() == "")
                //    {
                //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Stamp Amount');", true);
                //        ErrFlag = true;
                //    }
                //    else if (txtunion.Text.Trim() == "")
                //    {
                //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Union Amount');", true);
                //        ErrFlag = true;
                //    }
                //    else if (txtdeduction.Text.Trim() == "")
                //    {
                //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Deduction Amount');", true);
                //        ErrFlag = true;
                //    }
                //}
                if (!ErrFlag)
                {
                    DateTime dfrom = Convert.ToDateTime(txtfrom.Text);
                    DateTime dtto = Convert.ToDateTime(txtTo.Text);
                    DateTime MyDate1 = DateTime.ParseExact(txtfrom.Text, "dd-MM-yyyy", null);
                    DateTime MyDate2 = DateTime.ParseExact(txtTo.Text, "dd-MM-yyyy", null);
                    #region MonthName
                    DateTime Md = new DateTime();

                    TempDate = Convert.ToDateTime(txtfrom.Text).AddMonths(0).ToShortDateString();
                    string Month = TempDate;
                    Md = Convert.ToDateTime(TempDate);
                    int d = Md.Month;
                    string Mont = "";
                    switch (d)
                    {
                        case 1:
                            Mont = "January";
                            break;

                        case 2:
                            Mont = "February";
                            break;
                        case 3:
                            Mont = "March";
                            break;
                        case 4:
                            Mont = "April";
                            break;
                        case 5:
                            Mont = "May";
                            break;
                        case 6:
                            Mont = "June";
                            break;
                        case 7:
                            Mont = "July";
                            break;
                        case 8:
                            Mont = "August";
                            break;
                        case 9:
                            Mont = "September";
                            break;
                        case 10:
                            Mont = "October";
                            break;
                        case 11:
                            Mont = "November";
                            break;
                        case 12:
                            Mont = "December";
                            break;
                        default:
                            break;
                    }
                    #endregion
                    TempDate = Convert.ToDateTime(txtfrom.Text).AddMonths(0).ToShortDateString();
                    Month = TempDate;
                    DateTime m;
                    m = Convert.ToDateTime(TempDate);
                    d = m.Month;
                    int mon = m.Month;
                    int yr = m.Year;
                    ExisitingNo = ddToken.SelectedValue;



                    string SMonth = Convert.ToString(mon);
                    string Year = Convert.ToString(yr);
                    int dd = DateTime.DaysInMonth(yr, mon);
                    string MyMonth = Mont;
                    //Year = System.DateTime.Now.ToString("yyyy");
                    string SalaryMonth = objdata.SalaryMonth(ddlempCodeStaff.SelectedValue, ddlFinaYear.SelectedValue, SessionCcode, SessionLcode, Mont, MyDate1, MyDate2);

                    if (MyMonth != SalaryMonth)
                    {
                        string pfEligible = objdata.PF_Eligible_Salary(ddlempCodeStaff.SelectedValue, SessionCcode, SessionLcode);
                        DateTime MyDate = new DateTime();
                        objSal.TotalWorkingDays = txtwork.Text;
                        objSal.NFh = txtNFh.Text;
                        objSal.Lcode = SessionLcode;
                        objSal.Ccode = SessionCcode;
                        objSal.ApplyLeaveDays = txtlossofpay.Text;
                        objSal.GrossEarnings = EarningsAmt.Text;
                        objSal.TotalDeduction = DeductionAmt.Text;
                        objSal.NetPay = lblNetPayAmt.Text;
                        objSal.Words = lbltext.Text;
                        objSal.AvailableDays = txtempdays.Text;
                        objSal.SalaryDate = txtsalaryday.Text;
                        objSal.work = txtempdays.Text;
                        objSal.weekoff = txtweekoff.Text;
                        objSal.CL = txtcl.Text;
                        objSal.FixedBase = FixedBasic;
                        objSal.FixedFDA = FixedFDA;
                        objSal.FixedFRA = FixedHRA;
                        objSal.HomeTown = txthome.Text.Trim();
                        MyDate = DateTime.ParseExact(objSal.SalaryDate, "dd-MM-yyyy", null);
                        //if (EmpType == "1")
                        //{
                        objSal.BasicAndDA = txtSBasic.Text;
                        objSal.HRA = "0";
                        objSal.FDA = "0";
                        objSal.VDA = "0";
                        objSal.OT = "0";
                        objSal.Allowances1 = txtSAll1.Text;
                        objSal.Allowances2 = txtSAll2.Text;
                        objSal.Allowances3 = txtSAll3.Text;
                        objSal.Allowances4 = txtSAll4.Text;
                        objSal.Allowances5 = txtSAll5.Text;
                        objSal.Deduction1 = txtSded1.Text;
                        objSal.Deduction2 = txtSded2.Text;
                        objSal.Deduction3 = txtSded3.Text;
                        objSal.Deduction4 = txtSded4.Text;
                        objSal.Deduction5 = txtSded5.Text;
                        objSal.ProvidentFund = txtSPF.Text;
                        objSal.HalfNightAmt = txtHalf.Text;
                        objSal.FullNightAmt = txtFull.Text;
                        objSal.ThreesidedAmt = txtThreeSided.Text;
                        objSal.SpinningAmt = txtSpinning.Text;
                        objSal.DayIncentive = txtDayIncentive.Text;
                        objSal.ESI = txtSesi.Text;
                        objSal.Stamp = txtSstamp.Text;
                        objSal.Advance = txtSAdvance.Text;
                        objSal.LossofPay = txtSLOP.Text;
                        objSal.OT = txtSOT.Text;
                        if (pfEligible == "1")
                        {
                            if (ddlcategory.SelectedValue == "1")
                            {
                                pfsalary_val = (Convert.ToDecimal(txtSBasic.Text) - Convert.ToDecimal(txtSLOP.Text)).ToString();
                            }
                            else
                            {
                                pfsalary_val = txtSBasic.Text;
                            }
                            objSal.Union = "0";

                            if (PF_salary == "1")
                            {
                                DataTable dtpf = new DataTable();

                                dtpf = objdata.Load_pf_details(SessionCcode, SessionLcode);
                                //sum_val = (convert).ToString();
                                if (Convert.ToDecimal(pfsalary_val) >= Convert.ToDecimal(dtpf.Rows[0]["StaffSalary"].ToString()))
                                {
                                    objSal.PfSalary = pf;
                                }
                                else
                                {
                                    objSal.PfSalary = pf;
                                }
                            }
                            else
                            {

                                objSal.PfSalary = pf;
                            }
                            if (Convert.ToDecimal(objSal.PfSalary) > 6500)
                            {
                                objSal.Emp_PF = "6500";
                            }
                            else
                            {
                                objSal.Emp_PF = pf;
                            }
                        }
                        else
                        {
                            objSal.PfSalary = "0.00";
                            objSal.Emp_PF = "0.00";
                        }

                        //}
                        //else if (EmpType == "2")
                        //{
                        //    objSal.BasicAndDA = txtbasic.Text;
                        //    objSal.HRA = txtHRA.Text;
                        //    objSal.FDA = txtFDA.Text;
                        //    objSal.VDA = txtVDA.Text;
                        //    objSal.OT = txtOT.Text;
                        //    objSal.Allowances1 = txtAllowance.Text;
                        //    objSal.Allowances2 = txtAllowance2.Text;
                        //    objSal.Allowances3 = txall3.Text;
                        //    objSal.Allowances4 = txtsll4.Text;
                        //    objSal.ProvidentFund = txtpf.Text;
                        //    objSal.ESI = txtESI.Text;
                        //    objSal.Advance = txtAdvance.Text;
                        //    objSal.Stamp = txtStamp.Text;
                        //    objSal.Union = txtunion.Text;
                        //    objSal.Deduction1 = txtdeduction.Text;
                        //    objSal.Deduction2 = txtdeduction2.Text;
                        //    objSal.Deduction3 = txtded3.Text;
                        //    objSal.Deduction4 = txtded4.Text;
                        //    objSal.LossofPay = "0.00";
                        //    if (PF_salary == "1")
                        //    {
                        //        DataTable dtpf = new DataTable();

                        //        dtpf = objdata.Load_pf_details(SessionCcode, SessionLcode);
                        //        sum_val = (Convert.ToDecimal(objSal.BasicAndDA) + Convert.ToDecimal(objSal.FDA) + Convert.ToDecimal(objSal.VDA)).ToString();
                        //        if (Convert.ToDecimal(sum_val) >= Convert.ToDecimal(dtpf.Rows[0]["StaffSalary"].ToString()))
                        //        {
                        //            objSal.PfSalary = dtpf.Rows[0]["StaffSalary"].ToString();
                        //        }
                        //        else
                        //        {
                        //            objSal.PfSalary = (Convert.ToDecimal(txtbasic.Text) + Convert.ToDecimal(txtFDA.Text) + Convert.ToDecimal(txtVDA.Text)).ToString();
                        //        }

                        //    }
                        //    else
                        //    {
                        //        objSal.PfSalary = (Convert.ToDecimal(txtbasic.Text) + Convert.ToDecimal(txtFDA.Text) + Convert.ToDecimal(txtVDA.Text)).ToString();
                        //    }
                        //    if (Convert.ToDecimal(objSal.PfSalary) > 6500)
                        //    {
                        //        objSal.Emp_PF = "6500";
                        //    }
                        //    else
                        //    {
                        //        objSal.Emp_PF = objSal.PfSalary;
                        //    }
                        //}
                        //else if (EmpType == "4")
                        //{
                        //    objSal.BasicAndDA = txtbasic.Text;
                        //    objSal.HRA = txtHRA.Text;
                        //    objSal.FDA = txtFDA.Text;
                        //    objSal.VDA = txtVDA.Text;
                        //    objSal.OT = txtOT.Text;
                        //    objSal.Allowances1 = txtAllowance.Text;
                        //    objSal.Allowances2 = txtAllowance2.Text;
                        //    objSal.Allowances3 = txall3.Text;
                        //    objSal.Allowances4 = txtsll4.Text;
                        //    objSal.ProvidentFund = txtpf.Text;
                        //    objSal.ESI = txtESI.Text;
                        //    objSal.Advance = txtAdvance.Text;
                        //    objSal.Stamp = txtStamp.Text;
                        //    objSal.Union = txtunion.Text;
                        //    objSal.Deduction1 = txtdeduction.Text;
                        //    objSal.Deduction2 = txtdeduction2.Text;
                        //    objSal.Deduction3 = txtded3.Text;
                        //    objSal.Deduction4 = txtded4.Text;
                        //    objSal.LossofPay = "0.00";
                        //    if (PF_salary == "1")
                        //    {
                        //        DataTable dtpf = new DataTable();

                        //        dtpf = objdata.Load_pf_details(SessionCcode, SessionLcode);
                        //        sum_val = (Convert.ToDecimal(objSal.BasicAndDA) + Convert.ToDecimal(objSal.FDA) + Convert.ToDecimal(objSal.VDA)).ToString();
                        //        if (Convert.ToDecimal(sum_val) >= Convert.ToDecimal(dtpf.Rows[0]["StaffSalary"].ToString()))
                        //        {
                        //            objSal.PfSalary = dtpf.Rows[0]["StaffSalary"].ToString();
                        //        }
                        //        else
                        //        {
                        //            objSal.PfSalary = (Convert.ToDecimal(txtbasic.Text) + Convert.ToDecimal(txtFDA.Text) + Convert.ToDecimal(txtVDA.Text)).ToString();
                        //        }
                        //    }
                        //    else
                        //    {
                        //        objSal.PfSalary = (Convert.ToDecimal(txtbasic.Text) + Convert.ToDecimal(txtFDA.Text) + Convert.ToDecimal(txtVDA.Text)).ToString();
                        //    }
                        //    if (Convert.ToDecimal(objSal.PfSalary) > 6500)
                        //    {
                        //        objSal.Emp_PF = "6500";
                        //    }
                        //    else
                        //    {
                        //        objSal.Emp_PF = objSal.PfSalary;
                        //    }
                        //}
                        //else if (EmpType == "3")
                        //{
                        //    objSal.BasicAndDA = txtWBasic.Text;
                        //    objSal.HRA = "0";
                        //    objSal.FDA = "0";
                        //    objSal.VDA = "0";
                        //    objSal.OT = txtWot.Text;
                        //    objSal.Allowances1 = txtWAll1.Text;
                        //    objSal.Allowances2 = txtWall2.Text;
                        //    objSal.Allowances3 = txtWall3.Text;
                        //    objSal.Allowances4 = txtWall4.Text;
                        //    objSal.ProvidentFund = txtWpf.Text;
                        //    objSal.ESI = txtWesi.Text;
                        //    objSal.Advance = txtWadvance.Text;
                        //    objSal.Stamp = txtWstamp.Text;
                        //    objSal.Union = "0";
                        //    objSal.Deduction1 = txtWded1.Text;
                        //    objSal.Deduction2 = txtWded2.Text;
                        //    objSal.Deduction3 = txtWDed3.Text;
                        //    objSal.Deduction4 = txtWded4.Text;
                        //    objSal.LossofPay = "0.00";
                        //    if (PF_salary == "1")
                        //    {
                        //        DataTable dtpf = new DataTable();

                        //        dtpf = objdata.Load_pf_details(SessionCcode, SessionLcode);
                        //        sum_val = (Convert.ToDecimal(objSal.BasicAndDA) + Convert.ToDecimal(objSal.FDA) + Convert.ToDecimal(objSal.VDA)).ToString();
                        //        if (Convert.ToDecimal(sum_val) >= Convert.ToDecimal(dtpf.Rows[0]["StaffSalary"].ToString()))
                        //        {
                        //            objSal.PfSalary = dtpf.Rows[0]["StaffSalary"].ToString();
                        //        }
                        //        else
                        //        {
                        //            objSal.PfSalary = txtWBasic.Text;
                        //        }
                        //    }
                        //    else
                        //    {
                        //        objSal.PfSalary = txtWBasic.Text;
                        //    }
                        //    if (Convert.ToDecimal(objSal.PfSalary) > 6500)
                        //    {
                        //        objSal.Emp_PF = "6500";
                        //    }
                        //    else
                        //    {
                        //        objSal.Emp_PF = objSal.PfSalary;
                        //    }
                        //}
                        //else
                        //{
                        //    objSal.BasicAndDA = txtbasic.Text;
                        //    objSal.HRA = txtHRA.Text;
                        //    objSal.FDA = txtFDA.Text;
                        //    objSal.VDA = txtVDA.Text;
                        //    objSal.OT = txtOT.Text;
                        //    objSal.Allowances1 = txtAllowance.Text;
                        //    objSal.Allowances2 = txtAllowance2.Text;
                        //    objSal.Allowances3 = txall3.Text;
                        //    objSal.Allowances4 = txtsll4.Text;
                        //    objSal.ProvidentFund = txtpf.Text;
                        //    objSal.ESI = txtESI.Text;
                        //    objSal.Advance = txtAdvance.Text;
                        //    objSal.Stamp = txtStamp.Text;
                        //    objSal.Union = txtunion.Text;
                        //    objSal.Deduction1 = txtdeduction.Text;
                        //    objSal.Deduction2 = txtdeduction2.Text;
                        //    objSal.Deduction3 = txtded3.Text;
                        //    objSal.Deduction4 = txtded4.Text;
                        //    objSal.LossofPay = "0.00";
                        //    if (PF_salary == "1")
                        //    {
                        //        DataTable dtpf = new DataTable();

                        //        dtpf = objdata.Load_pf_details(SessionCcode, SessionLcode);
                        //        sum_val = (Convert.ToDecimal(objSal.BasicAndDA) + Convert.ToDecimal(objSal.FDA) + Convert.ToDecimal(objSal.VDA)).ToString();
                        //        if (Convert.ToDecimal(sum_val) >= Convert.ToDecimal(dtpf.Rows[0]["StaffSalary"].ToString()))
                        //        {
                        //            objSal.PfSalary = dtpf.Rows[0]["StaffSalary"].ToString();
                        //        }
                        //        else
                        //        {
                        //            objSal.PfSalary = (Convert.ToDecimal(txtbasic.Text) + Convert.ToDecimal(txtFDA.Text) + Convert.ToDecimal(txtVDA.Text)).ToString();
                        //        }

                        //    }
                        //    else
                        //    {
                        //        objSal.PfSalary = (Convert.ToDecimal(txtbasic.Text) + Convert.ToDecimal(txtFDA.Text) + Convert.ToDecimal(txtVDA.Text)).ToString();
                        //    }
                        //    if (Convert.ToDecimal(objSal.PfSalary) > 6500)
                        //    {
                        //        objSal.Emp_PF = "6500";
                        //    }
                        //    else
                        //    {
                        //        objSal.Emp_PF = objSal.PfSalary;
                        //    }
                        //}
                        MyDate = DateTime.ParseExact(objSal.SalaryDate, "dd-MM-yyyy", null);

                        TempDate = Convert.ToDateTime(txtsalaryday.Text).AddMonths(0).ToShortDateString();
                        TransDate = DateTime.ParseExact(TempDate, "dd/MM/yyyy", null);
                        DataTable dtadv = new DataTable();
                        //dtadv = objdata.Salary_advance_retrive(ddlempCodeStaff.SelectedValue, SessionCcode, SessionLcode);
                        //if (dtadv.Rows.Count > 0)
                        //{
                        //    Adv_id = dtadv.Rows[0]["ID"].ToString();
                        //}
                        //else
                        //{
                        //    Adv_id = "";
                        //}
                        isRegistered = true;
                        if (isRegistered)
                        {
                            //if (Adv_id != "")
                            //{
                            //  if (Convert.ToInt32(Dec_mont) == 1)
                            //{
                            //  if ((Convert.ToDecimal(Adv_BalanceAmt) == Convert.ToDecimal(objSal.Advance)) || (Convert.ToDecimal(objSal.Advance) == 0))
                            //{
                            //    if (Convert.ToDecimal(objSal.Advance) == 0)
                            //    {
                            //        objdata.Advance_repayInsert(ddlempCodeStaff.SelectedValue, MyDate, MyMonth, objSal.Advance, Adv_id, SessionCcode, SessionLcode);
                            //        string bal = (Convert.ToDecimal(Adv_BalanceAmt) - Convert.ToDecimal(objSal.Advance)).ToString();
                            //        string decrementMonths = Convert.ToInt32(Dec_mont).ToString();
                            //        string IncrementMonths = (Convert.ToInt32(Increment_mont) + 1).ToString();
                            //        string Completed = "N";

                            //        objdata.Advance_update(ddlempCodeStaff.SelectedValue, bal, IncrementMonths, decrementMonths, Adv_id, Completed, MyDate, SessionCcode, SessionLcode);
                            //    }
                            //    else
                            //    {
                            //        objdata.Advance_repayInsert(ddlempCodeStaff.SelectedValue, MyDate, MyMonth, objSal.Advance, Adv_id, SessionCcode, SessionLcode);
                            //        string bal = (Convert.ToDecimal(Adv_BalanceAmt) - Convert.ToDecimal(objSal.Advance)).ToString();
                            //        string decrementMonths = (Convert.ToInt32(Dec_mont) - 1).ToString();
                            //        string IncrementMonths = (Convert.ToInt32(Increment_mont) + 1).ToString();
                            //        string Completed = "Y";

                            //        objdata.Advance_update(ddlempCodeStaff.SelectedValue, bal, IncrementMonths, decrementMonths, Adv_id, Completed, MyDate, SessionCcode, SessionLcode);
                            //    }

                            objdata.SalaryDetails(objSal, ddlempCodeStaff.SelectedValue, ExisitingNo, MyDate, MyMonth, Year, ddlFinaYear.SelectedValue, TransDate, MyDate1, MyDate2);
                            string Balanceday = objdata.Contract_GetEmpDet(ddlempCodeStaff.SelectedValue, SessionCcode, SessionLcode);
                            if (Balanceday.Trim() != "")
                            {
                                if (Convert.ToDecimal(Balanceday) > 0)
                                {
                                    DataTable dt_det = new DataTable();
                                    dt_det = objdata.Contract_getPlanDetails(ddlempCodeStaff.SelectedValue, SessionCcode, SessionLcode);
                                    if (dt_det.Rows.Count > 0)
                                    {
                                        if (dt_det.Rows[0]["ContractType"].ToString() == "1")
                                        {
                                            val = (Convert.ToDecimal(txtempdays.Text) + Convert.ToDecimal(txtcl.Text));
                                            Balanceday = (Convert.ToDecimal(Balanceday) - val).ToString();
                                            //objdata.Contract_Reducing(Balanceday, ddlempCodeStaff.SelectedValue, val.ToString(), SessionCcode, SessionLcode);
                                        }
                                        else if (dt_det.Rows[0]["ContractType"].ToString() == "2")
                                        {
                                            val = (Convert.ToDecimal(txtempdays.Text) + Convert.ToDecimal(txtcl.Text));
                                            if (val >= Convert.ToDecimal(dt_det.Rows[0]["FixedDays"].ToString()))
                                            {
                                                string Mon = (Convert.ToInt32(dt_det.Rows[0]["DecMonth"].ToString()) - 1).ToString();
                                                Balanceday = (Convert.ToDecimal(Balanceday) - val).ToString();
                                                //objdata.Contract_Reducing_Month(Balanceday, ddlempCodeStaff.SelectedValue, val.ToString(), SessionCcode, SessionLcode, Mon);
                                            }
                                            else
                                            {
                                                string Mon = (Convert.ToInt32(dt_det.Rows[0]["DecMonth"].ToString()) - 1).ToString();
                                                //Balanceday = (Convert.ToDecimal(Balanceday) - val).ToString();
                                                //objdata.Contract_Reducing(Balanceday, ddlempCodeStaff.SelectedValue, val, SessionCcode, SessionLcode, Mon);
                                                //objdata.Not_Contract_Reducing_Month(ddlempCodeStaff.SelectedValue, val.ToString(), SessionCcode, SessionLcode);
                                            }
                                        }
                                        else if (dt_det.Rows[0]["ContractType"].ToString() == "3")
                                        {
                                            val = (Convert.ToDecimal(txtempdays.Text) + Convert.ToDecimal(txtNFh.Text));
                                            Balanceday = (Convert.ToDecimal(Balanceday) - val).ToString();
                                            //objdata.Contract_Reducing(Balanceday, ddlempCodeStaff.SelectedValue, val.ToString(), SessionCcode, SessionLcode);
                                        }
                                    }
                                }
                            }
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert(' " + MyMonth + " Month Salary has been processed');", true);
                            //System.Windows.Forms.MessageBox.Show(MyMonth + " Month Salary has been processed", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                        }
                    }
                    else
                    {
                        string qry_emp = "Select AT.EmpNo from AttenanceDetails AT inner JOin SalaryDetails SD on SD.EmpNo=AT.EmpNo where AT.EmpNo='" + ddlempCodeStaff.SelectedValue + "' and " +
                                                 "AT.Months='" + MyMonth + "' and AT.FinancialYear='" + ddlFinaYear.SelectedValue + "' and AT.Ccode='" + SessionCcode + "' and AT.Lcode='" + SessionLcode + "' and SD.Month='" + MyMonth + "'" +
                                                 " and SD.FinancialYear='" + ddlFinaYear.SelectedValue + "' and SD.Process_Mode='1' and SD.Ccode='" + SessionCcode + "' and SD.Lcode='" + SessionLcode + "' and convert(datetime,AT.TODate,105) = convert(datetime,'" + txtTo.Text + "',105) and convert(datetime,AT.FromDate,105) = convert(datetime,'" + txtfrom.Text + "',105)";
                        //string qry_emp = "Select EmpNo from AttenanceDetails where EmpNo = '" + dt.Rows[i][2].ToString() + "' and Months = '" + ddlMonths.Text + "' and FinancialYear = '" + ddlfinance.SelectedValue + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                        cn.Open();
                        SqlCommand cmd_verify = new SqlCommand(qry_emp, cn);

                        Name_Upload = Convert.ToString(cmd_verify.ExecuteScalar());                        
                        cn.Close();
                        if (Name_Upload != "")
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Salary has been Processed already');", true);
                            return;
                        }
                        string pfEligible = objdata.PF_Eligible_Salary(ddlempCodeStaff.SelectedValue, SessionCcode, SessionLcode);
                        DateTime MyDate = new DateTime();
                        objSal.TotalWorkingDays = txtwork.Text;
                        objSal.NFh = txtNFh.Text;
                        objSal.Lcode = SessionLcode;
                        objSal.Ccode = SessionCcode;
                        objSal.ApplyLeaveDays = txtlossofpay.Text;
                        objSal.GrossEarnings = EarningsAmt.Text;
                        objSal.TotalDeduction = DeductionAmt.Text;
                        objSal.NetPay = lblNetPayAmt.Text;
                        objSal.Words = lbltext.Text;
                        objSal.AvailableDays = txtempdays.Text;
                        objSal.SalaryDate = txtsalaryday.Text;
                        objSal.work = txtempdays.Text;
                        objSal.weekoff = txtweekoff.Text;
                        objSal.CL = txtcl.Text;
                        objSal.FixedBase = FixedBasic;
                        objSal.FixedFDA = FixedFDA;
                        objSal.FixedFRA = FixedHRA;
                        objSal.HomeTown = txthome.Text.Trim();
                        MyDate = DateTime.ParseExact(objSal.SalaryDate, "dd-MM-yyyy", null);
                        //if (EmpType == "1")
                        //{
                        objSal.BasicAndDA = txtSBasic.Text;
                        objSal.HRA = "0";
                        objSal.FDA = "0";
                        objSal.VDA = "0";
                        objSal.OT = "0";
                        objSal.Allowances1 = txtSAll1.Text;
                        objSal.Allowances2 = txtSAll2.Text;
                        objSal.Allowances3 = txtSAll3.Text;
                        objSal.Allowances4 = txtSAll4.Text;
                        objSal.Allowances5 = txtSAll5.Text;
                        objSal.Deduction1 = txtSded1.Text;
                        objSal.Deduction2 = txtSded2.Text;
                        objSal.Deduction3 = txtSded3.Text;
                        objSal.Deduction4 = txtSded4.Text;
                        objSal.Deduction5 = txtSded5.Text;
                        objSal.ProvidentFund = txtSPF.Text;
                        objSal.ESI = txtSesi.Text;
                        objSal.Stamp = txtSstamp.Text;
                        objSal.Advance = txtSAdvance.Text;
                        objSal.LossofPay = txtSLOP.Text;
                        objSal.OT = txtSOT.Text;
                        objSal.HalfNightAmt = txtHalf.Text;
                        objSal.FullNightAmt = txtFull.Text;
                        objSal.ThreesidedAmt = txtThreeSided.Text;
                        objSal.SpinningAmt = txtSpinning.Text;
                        objSal.DayIncentive = txtDayIncentive.Text;
                        if (pfEligible == "1")
                        {
                            if (ddlcategory.SelectedValue == "1")
                            {
                                pfsalary_val = (Convert.ToDecimal(txtSBasic.Text) - Convert.ToDecimal(txtSLOP.Text)).ToString();
                            }
                            else
                            {
                                pfsalary_val = txtSBasic.Text;
                            }
                            objSal.Union = "0";

                            if (PF_salary == "1")
                            {
                                DataTable dtpf = new DataTable();

                                dtpf = objdata.Load_pf_details(SessionCcode, SessionLcode);
                                //sum_val = (convert).ToString();
                                if (Convert.ToDecimal(pfsalary_val) >= Convert.ToDecimal(dtpf.Rows[0]["StaffSalary"].ToString()))
                                {
                                    objSal.PfSalary = pf;
                                }
                                else
                                {
                                    objSal.PfSalary = pf;
                                }
                            }
                            else
                            {

                                objSal.PfSalary = pf;
                            }
                            if (Convert.ToDecimal(objSal.PfSalary) > 6500)
                            {
                                objSal.Emp_PF = "6500";
                            }
                            else
                            {
                                objSal.Emp_PF = pf;
                            }
                        }
                        else
                        {
                            objSal.PfSalary = "0.00";
                            objSal.Emp_PF = "0.00";
                        }
                        objSal.FDA = "0.00";
                        objSal.VDA = "0.00";
                        objSal.HRA = "0.00";
                        MyDate = DateTime.ParseExact(objSal.SalaryDate, "dd-MM-yyyy", null);

                        TempDate = Convert.ToDateTime(txtsalaryday.Text).AddMonths(0).ToShortDateString();
                        TransDate = DateTime.ParseExact(TempDate, "dd/MM/yyyy", null);
                        DataTable dtadv = new DataTable();
                        isRegistered = true;
                        if (isRegistered)
                            {
                            //objdata.SalaryDetails(objSal, ddlempCodeStaff.SelectedValue, ExisitingNo, MyDate, MyMonth, Year, ddlFinaYear.SelectedValue, TransDate, MyDate1, MyDate2);
                            //string Del = "Delete from SalaryDetails where EmpNo='" + ddlempCodeStaff.SelectedValue + "' and " +
                            //                     "Month='" + MyMonth + "' and FinancialYear='" + ddlFinaYear.SelectedValue + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'" +
                            //                     " and Process_Mode='0' and convert(datetime,FromDate,105) = convert(datetime,'" + txtfrom.Text + "',105) and convert(datetime, ToDate, 105) = convert(datetime, '" + txtTo.Text + "', 105)";
                            //cn.Open();
                            //SqlCommand cmd_verify1 = new SqlCommand(Del, cn);
                            //cmd_verify1.ExecuteNonQuery();
                            //cn.Close();
                            MyDate = DateTime.ParseExact(objSal.SalaryDate, "dd-MM-yyyy", null);
                            objdata.SalaryDetails_update(objSal, ddlempCodeStaff.SelectedValue, ExisitingNo, MyDate, MyMonth, Year, ddlFinaYear.SelectedValue, TransDate, MyDate1, MyDate2);
                            //objdata.SalaryDetails(objSal, ddlempCodeStaff.SelectedValue, ExisitingNo, MyDate, MyMonth, Year, ddlFinaYear.SelectedValue, TransDate, MyDate1, MyDate2);
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert(' " + MyMonth + " Month Salary has been processed');", true);
                        }
                    }
                                //else
                                //{
                                //    bool Err = false;
                                //    //if (Convert.ToDecimal(objSal.Advance) > Convert.ToDecimal(Adv_BalanceAmt))
                                //    //{
                                //    //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Enter the Advance amount Properly');", true);
                                //    //    //System.Windows.Forms.MessageBox.Show(MyMonth + "Enter the Advance Amount Proprly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                //    //    Err = true;
                                //    //}
                                //    //else if (Convert.ToDecimal(objSal.Advance) == Convert.ToDecimal(Adv_BalanceAmt))
                                //    //{
                                //    //    objdata.Advance_repayInsert(ddlempCodeStaff.SelectedValue, MyDate, MyMonth, objSal.Advance, Adv_id, SessionCcode, SessionLcode);
                                //    //    string bal = (Convert.ToDecimal(Adv_BalanceAmt) - Convert.ToDecimal(objSal.Advance)).ToString();
                                //    //    string decrementMonths = (Convert.ToInt32(Dec_mont) - 1).ToString();
                                //    //    string IncrementMonths = (Convert.ToInt32(Increment_mont) + 1).ToString();
                                //    //    string Completed = "Y";

                                //    //    objdata.Advance_update(ddlempCodeStaff.SelectedValue, bal, IncrementMonths, decrementMonths, Adv_id, Completed, MyDate, SessionCcode, SessionLcode);
                                //    //}
                                //    //else if (Convert.ToDecimal(txtAdvance.Text) == 0)
                                //    //{
                                //    //    objdata.Advance_repayInsert(ddlempCodeStaff.SelectedValue, MyDate, MyMonth, objSal.Advance, Adv_id, SessionCcode, SessionLcode);
                                //    //    string bal = (Convert.ToDecimal(Adv_BalanceAmt) - Convert.ToDecimal(objSal.Advance)).ToString();
                                //    //    string decrementMonths = Convert.ToInt32(Dec_mont).ToString();
                                //    //    string IncrementMonths = (Convert.ToInt32(Increment_mont) + 1).ToString();
                                //    //    string Completed = "N";

                                //    //    objdata.Advance_update(ddlempCodeStaff.SelectedValue, bal, IncrementMonths, decrementMonths, Adv_id, Completed, MyDate, SessionCcode, SessionLcode);
                                //    //}
                                //    //else
                                //    //{
                                //    //    objdata.Advance_repayInsert(ddlempCodeStaff.SelectedValue, MyDate, MyMonth, objSal.Advance, Adv_id, SessionCcode, SessionLcode);
                                //    //    string bal = (Convert.ToDecimal(Adv_BalanceAmt) - Convert.ToDecimal(objSal.Advance)).ToString();
                                //    //    string decrementMonths = (Convert.ToInt32(Dec_mont) - 1).ToString();
                                //    //    string IncrementMonths = (Convert.ToInt32(Increment_mont) + 1).ToString();
                                //    //    string Completed = "N";

                                //    //    objdata.Advance_update(ddlempCodeStaff.SelectedValue, bal, IncrementMonths, decrementMonths, Adv_id, Completed, MyDate, SessionCcode, SessionLcode);
                                //    //}
                                //    if (!Err)
                                //    {
                                //        objdata.SalaryDetails(objSal, ddlempCodeStaff.SelectedValue, ExisitingNo, MyDate, MyMonth, Year, ddlFinaYear.SelectedValue, TransDate);
                                //        string Balanceday = objdata.Contract_GetEmpDet(ddlempCodeStaff.SelectedValue, SessionCcode, SessionLcode);
                                //        if (Balanceday.Trim() != "")
                                //        {
                                //            if (Convert.ToDecimal(Balanceday) > 0)
                                //            {
                                //                DataTable dt_det = new DataTable();
                                //                dt_det = objdata.Contract_getPlanDetails(ddlempCodeStaff.SelectedValue, SessionCcode, SessionLcode);
                                //                if (dt_det.Rows.Count > 0)
                                //                {
                                //                    if (dt_det.Rows[0]["ContractType"].ToString() == "1")
                                //                    {
                                //                        val = (Convert.ToDecimal(txtempdays.Text) + Convert.ToDecimal(txtNFh.Text));
                                //                        Balanceday = (Convert.ToDecimal(Balanceday) - val).ToString();
                                //                        objdata.Contract_Reducing(Balanceday, ddlempCodeStaff.SelectedValue, val.ToString(), SessionCcode, SessionLcode);
                                //                    }
                                //                    else if (dt_det.Rows[0]["ContractType"].ToString() == "2")
                                //                    {
                                //                        val = (Convert.ToDecimal(txtempdays.Text) + Convert.ToDecimal(txtNFh.Text));
                                //                        if (val >= Convert.ToDecimal(dt_det.Rows[0]["FixedDays"].ToString()))
                                //                        {
                                //                            string Mon = (Convert.ToInt32(dt_det.Rows[0]["DecMonth"].ToString()) - 1).ToString();
                                //                            Balanceday = (Convert.ToDecimal(Balanceday) - val).ToString();
                                //                            objdata.Contract_Reducing_Month(Balanceday, ddlempCodeStaff.SelectedValue, val.ToString(), SessionCcode, SessionLcode, Mon);
                                //                        }
                                //                        else
                                //                        {
                                //                            string Mon = (Convert.ToInt32(dt_det.Rows[0]["DecMonth"].ToString()) - 1).ToString();
                                //                            //Balanceday = (Convert.ToDecimal(Balanceday) - val).ToString();
                                //                            //objdata.Contract_Reducing(Balanceday, ddlempCodeStaff.SelectedValue, val, SessionCcode, SessionLcode, Mon);
                                //                            objdata.Not_Contract_Reducing_Month(ddlempCodeStaff.SelectedValue, val.ToString(), SessionCcode, SessionLcode);
                                //                        }
                                //                    }
                                //                    else if (dt_det.Rows[0]["ContractType"].ToString() == "3")
                                //                    {
                                //                        val = (Convert.ToDecimal(txtempdays.Text) + Convert.ToDecimal(txtNFh.Text));
                                //                        Balanceday = (Convert.ToDecimal(Balanceday) - val).ToString();
                                //                        objdata.Contract_Reducing(Balanceday, ddlempCodeStaff.SelectedValue, val.ToString(), SessionCcode, SessionLcode);
                                //                    }
                                //                }
                                //            }
                                //        }
                                //        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert(' " + MyMonth + " Month Salary has been processed');", true);
                                //        //System.Windows.Forms.MessageBox.Show(MyMonth + " Month Salary has been processed", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                //    }
                                //}
                            }
                            //else
                            //{
                            //    objdata.SalaryDetails(objSal, ddlempCodeStaff.SelectedValue, ExisitingNo, MyDate, MyMonth, Year, ddlFinaYear.SelectedValue, TransDate);
                                
                            //    string Balanceday = objdata.Contract_GetEmpDet(ddlempCodeStaff.SelectedValue, SessionCcode, SessionLcode);
                            //    if (Balanceday.Trim() != "")
                            //    {
                            //        if (Convert.ToDecimal(Balanceday) > 0)
                            //        {
                            //            DataTable dt_det = new DataTable();
                            //            dt_det = objdata.Contract_getPlanDetails(ddlempCodeStaff.SelectedValue, SessionCcode, SessionLcode);
                            //            if (dt_det.Rows.Count > 0)
                            //            {
                            //                if (dt_det.Rows[0]["ContractType"].ToString() == "1")
                            //                {
                            //                    val = (Convert.ToDecimal(txtempdays.Text) + Convert.ToDecimal(txtNFh.Text));
                            //                    Balanceday = (Convert.ToDecimal(Balanceday) - val).ToString();
                            //                    objdata.Contract_Reducing(Balanceday, ddlempCodeStaff.SelectedValue, val.ToString(), SessionCcode, SessionLcode);
                            //                }
                            //                else if (dt_det.Rows[0]["ContractType"].ToString() == "2")
                            //                {
                            //                    val = (Convert.ToDecimal(txtempdays.Text) + Convert.ToDecimal(txtNFh.Text));
                            //                    if (val >= Convert.ToDecimal(dt_det.Rows[0]["FixedDays"].ToString()))
                            //                    {
                            //                        string Mon = (Convert.ToInt32(dt_det.Rows[0]["DecMonth"].ToString()) - 1).ToString();
                            //                        Balanceday = (Convert.ToDecimal(Balanceday) - val).ToString();
                            //                        objdata.Contract_Reducing_Month(Balanceday, ddlempCodeStaff.SelectedValue, val.ToString(), SessionCcode, SessionLcode, Mon);
                            //                    }
                            //                    else
                            //                    {
                            //                        string Mon = (Convert.ToInt32(dt_det.Rows[0]["DecMonth"].ToString()) - 1).ToString();
                            //                        //Balanceday = (Convert.ToDecimal(Balanceday) - val).ToString();
                            //                        //objdata.Contract_Reducing(Balanceday, ddlempCodeStaff.SelectedValue, val, SessionCcode, SessionLcode, Mon);
                            //                        objdata.Not_Contract_Reducing_Month(ddlempCodeStaff.SelectedValue, val.ToString(), SessionCcode, SessionLcode);
                            //                    }
                            //                }
                            //                else if (dt_det.Rows[0]["ContractType"].ToString() == "3")
                            //                {
                            //                    val = (Convert.ToDecimal(txtempdays.Text) + Convert.ToDecimal(txtNFh.Text));
                            //                    Balanceday = (Convert.ToDecimal(Balanceday) - val).ToString();
                            //                    objdata.Contract_Reducing(Balanceday, ddlempCodeStaff.SelectedValue, val.ToString(), SessionCcode, SessionLcode);
                            //                }
                            //            }
                            //        }
                            //    }
                            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert(' " + MyMonth + " Month Salary has been processed');", true);
                            //    //System.Windows.Forms.MessageBox.Show(MyMonth + " Month Salary has been processed", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                            //}


                        }
                        //lbltext.Text = NumerictoNumber(NetPay, isUK);
                    }
                    //else
                    //{
                    //    DateTime MyDate = new DateTime();
                    //    objSal.TotalWorkingDays = txtwork.Text;
                    //    objSal.NFh = txtNFh.Text;
                    //    objSal.Lcode = SessionLcode;
                    //    objSal.Ccode = SessionCcode;
                    //    objSal.ApplyLeaveDays = txtlossofpay.Text;
                    //    objSal.GrossEarnings = EarningsAmt.Text;
                    //    objSal.TotalDeduction = DeductionAmt.Text;
                    //    objSal.NetPay = lblNetPayAmt.Text;
                    //    objSal.Words = lbltext.Text;
                    //    objSal.AvailableDays = txtempdays.Text;
                    //    objSal.SalaryDate = txtsalaryday.Text;
                    //    objSal.work = txtempdays.Text;
                    //    objSal.weekoff = txtweekoff.Text;
                    //    objSal.CL = txtcl.Text;
                    //    objSal.FixedBase = FixedBasic;
                    //    objSal.FixedFDA = FixedFDA;
                    //    objSal.FixedFRA = FixedHRA;
                    //    MyDate = DateTime.ParseExact(objSal.SalaryDate, "dd-MM-yyyy", null);
                    //    if (EmpType == "1")
                    //    {
                    //        objSal.BasicAndDA = txtSBasic.Text;
                    //        objSal.HRA = "0";
                    //        objSal.FDA = "0";
                    //        objSal.VDA = "0";
                    //        objSal.OT = "0";
                    //        objSal.Allowances1 = txtSAll1.Text;
                    //        objSal.Allowances2 = txtSAll2.Text;
                    //        objSal.Allowances3 = txtSAll3.Text;
                    //        objSal.Allowances4 = txtSAll4.Text;
                    //        objSal.Deduction1 = txtSded1.Text;
                    //        objSal.Deduction2 = txtSded2.Text;
                    //        objSal.Deduction3 = txtSded3.Text;
                    //        objSal.Deduction4 = txtSded4.Text;
                    //        objSal.ProvidentFund = txtSPF.Text;
                    //        objSal.ESI = txtSesi.Text;
                    //        objSal.Stamp = txtSstamp.Text;
                    //        objSal.Advance = txtSAdvance.Text;
                    //        objSal.LossofPay = txtSLOP.Text;
                    //        objSal.Union = "0";
                    //        if (PF_salary == "1")
                    //        {
                    //            DataTable dtpf = new DataTable();

                    //            dtpf = objdata.Load_pf_details(SessionCcode, SessionLcode);
                    //            sum_val = (Convert.ToDecimal(objSal.BasicAndDA) + Convert.ToDecimal(objSal.FDA) + Convert.ToDecimal(objSal.VDA)).ToString();
                    //            if (Convert.ToDecimal(sum_val) >= Convert.ToDecimal(dtpf.Rows[0]["StaffSalary"].ToString()))
                    //            {
                    //                objSal.PfSalary = dtpf.Rows[0]["StaffSalary"].ToString();
                    //            }
                    //            else
                    //            {
                    //                objSal.PfSalary = txtSBasic.Text;
                    //            }
                    //        }
                    //        else
                    //        {
                    //            objSal.PfSalary = txtSBasic.Text;
                    //        }
                    //        if (Convert.ToDecimal(objSal.PfSalary) > 6500)
                    //        {
                    //            objSal.Emp_PF = "6500";
                    //        }
                    //        else
                    //        {
                    //            objSal.Emp_PF = objSal.PfSalary;
                    //        }
                    //    }
                    //    //else if (EmpType == "2")
                    //    //{
                    //    //    objSal.BasicAndDA = txtbasic.Text;
                    //    //    objSal.HRA = txtHRA.Text;
                    //    //    objSal.FDA = txtFDA.Text;
                    //    //    objSal.VDA = txtVDA.Text;
                    //    //    objSal.OT = txtOT.Text;
                    //    //    objSal.Allowances1 = txtAllowance.Text;
                    //    //    objSal.Allowances2 = txtAllowance2.Text;
                    //    //    objSal.Allowances3 = txall3.Text;
                    //    //    objSal.Allowances4 = txtsll4.Text;
                    //    //    objSal.ProvidentFund = txtpf.Text;
                    //    //    objSal.ESI = txtESI.Text;
                    //    //    objSal.Advance = txtAdvance.Text;
                    //    //    objSal.Stamp = txtStamp.Text;
                    //    //    objSal.Union = txtunion.Text;
                    //    //    objSal.Deduction1 = txtdeduction.Text;
                    //    //    objSal.Deduction2 = txtdeduction2.Text;
                    //    //    objSal.Deduction3 = txtded3.Text;
                    //    //    objSal.Deduction4 = txtded4.Text;
                    //    //    objSal.LossofPay = "0.00";
                    //    //    if (PF_salary == "1")
                    //    //    {
                    //    //        DataTable dtpf = new DataTable();

                    //    //        dtpf = objdata.Load_pf_details(SessionCcode, SessionLcode);
                    //    //        sum_val = (Convert.ToDecimal(objSal.BasicAndDA) + Convert.ToDecimal(objSal.FDA) + Convert.ToDecimal(objSal.VDA)).ToString();
                    //    //        if (Convert.ToDecimal(sum_val) >= Convert.ToDecimal(dtpf.Rows[0]["StaffSalary"].ToString()))
                    //    //        {
                    //    //            objSal.PfSalary = dtpf.Rows[0]["StaffSalary"].ToString();
                    //    //        }
                    //    //        else
                    //    //        {
                    //    //            objSal.PfSalary = (Convert.ToDecimal(txtbasic.Text) + Convert.ToDecimal(txtFDA.Text) + Convert.ToDecimal(txtVDA.Text)).ToString();
                    //    //        }
                    //    //    }
                    //    //    else
                    //    //    {
                    //    //        objSal.PfSalary = (Convert.ToDecimal(txtbasic.Text) + Convert.ToDecimal(txtFDA.Text) + Convert.ToDecimal(txtVDA.Text)).ToString();
                    //    //    }
                    //    //    if (Convert.ToDecimal(objSal.PfSalary) > 6500)
                    //    //    {
                    //    //        objSal.Emp_PF = "6500";
                    //    //    }
                    //    //    else
                    //    //    {
                    //    //        objSal.Emp_PF = objSal.PfSalary;
                    //    //    }
                    //    //}
                    //    //else if (EmpType == "4")
                    //    //{
                    //    //    objSal.BasicAndDA = txtbasic.Text;
                    //    //    objSal.HRA = txtHRA.Text;
                    //    //    objSal.FDA = txtFDA.Text;
                    //    //    objSal.VDA = txtVDA.Text;
                    //    //    objSal.OT = txtOT.Text;
                    //    //    objSal.Allowances1 = txtAllowance.Text;
                    //    //    objSal.Allowances2 = txtAllowance2.Text;
                    //    //    objSal.Allowances3 = txall3.Text;
                    //    //    objSal.Allowances4 = txtsll4.Text;
                    //    //    objSal.ProvidentFund = txtpf.Text;
                    //    //    objSal.ESI = txtESI.Text;
                    //    //    objSal.Advance = txtAdvance.Text;
                    //    //    objSal.Stamp = txtStamp.Text;
                    //    //    objSal.Union = txtunion.Text;
                    //    //    objSal.Deduction1 = txtdeduction.Text;
                    //    //    objSal.Deduction2 = txtdeduction2.Text;
                    //    //    objSal.Deduction3 = txtded3.Text;
                    //    //    objSal.Deduction4 = txtded4.Text;
                    //    //    objSal.LossofPay = "0.00";
                    //    //    if (PF_salary == "1")
                    //    //    {
                    //    //        DataTable dtpf = new DataTable();

                    //    //        dtpf = objdata.Load_pf_details(SessionCcode, SessionLcode);
                    //    //        sum_val = (Convert.ToDecimal(objSal.BasicAndDA) + Convert.ToDecimal(objSal.FDA) + Convert.ToDecimal(objSal.VDA)).ToString();
                    //    //        if (Convert.ToDecimal(sum_val) >= Convert.ToDecimal(dtpf.Rows[0]["StaffSalary"].ToString()))
                    //    //        {
                    //    //            objSal.PfSalary = dtpf.Rows[0]["StaffSalary"].ToString();
                    //    //        }
                    //    //        else
                    //    //        {
                    //    //            objSal.PfSalary = (Convert.ToDecimal(txtbasic.Text) + Convert.ToDecimal(txtFDA.Text) + Convert.ToDecimal(txtVDA.Text)).ToString();
                    //    //        }
                    //    //    }
                    //    //    else
                    //    //    {
                    //    //        objSal.PfSalary = (Convert.ToDecimal(txtbasic.Text) + Convert.ToDecimal(txtFDA.Text) + Convert.ToDecimal(txtVDA.Text)).ToString();
                    //    //    }
                    //    //    if (Convert.ToDecimal(objSal.PfSalary) > 6500)
                    //    //    {
                    //    //        objSal.Emp_PF = "6500";
                    //    //    }
                    //    //    else
                    //    //    {
                    //    //        objSal.Emp_PF = objSal.PfSalary;
                    //    //    }
                    //    //}
                    //    //else if (EmpType == "3")
                    //    //{
                    //    //    objSal.BasicAndDA = txtWBasic.Text;
                    //    //    objSal.HRA = "0";
                    //    //    objSal.FDA = "0";
                    //    //    objSal.VDA = "0";
                    //    //    objSal.OT = txtWot.Text;
                    //    //    objSal.Allowances1 = txtWAll1.Text;
                    //    //    objSal.Allowances2 = txtWall2.Text;
                    //    //    objSal.Allowances3 = txtWall3.Text;
                    //    //    objSal.Allowances4 = txtWall4.Text;
                    //    //    objSal.ProvidentFund = txtWpf.Text;
                    //    //    objSal.ESI = txtWesi.Text;
                    //    //    objSal.Advance = txtWadvance.Text;
                    //    //    objSal.Stamp = txtWstamp.Text;
                    //    //    objSal.Union = "0";
                    //    //    objSal.Deduction1 = txtWded1.Text;
                    //    //    objSal.Deduction2 = txtWded2.Text;
                    //    //    objSal.Deduction3 = txtWDed3.Text;
                    //    //    objSal.Deduction4 = txtWded4.Text;
                    //    //    objSal.LossofPay = "0.00";
                    //    //    if (PF_salary == "1")
                    //    //    {
                    //    //        DataTable dtpf = new DataTable();

                    //    //        dtpf = objdata.Load_pf_details(SessionCcode, SessionLcode);
                    //    //        sum_val = (Convert.ToDecimal(objSal.BasicAndDA) + Convert.ToDecimal(objSal.FDA) + Convert.ToDecimal(objSal.VDA)).ToString();
                    //    //        if (Convert.ToDecimal(sum_val) >= Convert.ToDecimal(dtpf.Rows[0]["StaffSalary"].ToString()))
                    //    //        {
                    //    //            objSal.PfSalary = dtpf.Rows[0]["StaffSalary"].ToString();
                    //    //        }
                    //    //        else
                    //    //        {
                    //    //            objSal.PfSalary = txtWBasic.Text;
                    //    //        }
                    //    //    }
                    //    //    else
                    //    //    {
                    //    //        objSal.PfSalary = txtWBasic.Text;
                    //    //    }
                    //    //    if (Convert.ToDecimal(objSal.PfSalary) > 6500)
                    //    //    {
                    //    //        objSal.Emp_PF = "6500";
                    //    //    }
                    //    //    else
                    //    //    {
                    //    //        objSal.Emp_PF = objSal.PfSalary;
                    //    //    }
                    //    //}
                    //    else
                    //    {
                    //        objSal.BasicAndDA = txtbasic.Text;
                    //        objSal.HRA = txtHRA.Text;
                    //        objSal.FDA = txtFDA.Text;
                    //        objSal.VDA = txtVDA.Text;
                    //        objSal.OT = txtOT.Text;
                    //        objSal.Allowances1 = txtAllowance.Text;
                    //        objSal.Allowances2 = txtAllowance2.Text;
                    //        objSal.Allowances3 = txall3.Text;
                    //        objSal.Allowances4 = txtsll4.Text;
                    //        objSal.ProvidentFund = txtpf.Text;
                    //        objSal.ESI = txtESI.Text;
                    //        objSal.Advance = txtAdvance.Text;
                    //        objSal.Stamp = txtStamp.Text;
                    //        objSal.Union = txtunion.Text;
                    //        objSal.Deduction1 = txtdeduction.Text;
                    //        objSal.Deduction2 = txtdeduction2.Text;
                    //        objSal.Deduction3 = txtded3.Text;
                    //        objSal.Deduction4 = txtded4.Text;
                    //        objSal.LossofPay = "0.00";
                    //        if (PF_salary == "1")
                    //        {
                    //            DataTable dtpf = new DataTable();

                    //            dtpf = objdata.Load_pf_details(SessionCcode, SessionLcode);
                    //            sum_val = (Convert.ToDecimal(objSal.BasicAndDA) + Convert.ToDecimal(objSal.FDA) + Convert.ToDecimal(objSal.VDA)).ToString();
                    //            if (Convert.ToDecimal(sum_val) >= Convert.ToDecimal(dtpf.Rows[0]["StaffSalary"].ToString()))
                    //            {
                    //                objSal.PfSalary = dtpf.Rows[0]["StaffSalary"].ToString();
                    //            }
                    //            else
                    //            {
                    //                objSal.PfSalary = (Convert.ToDecimal(txtbasic.Text) + Convert.ToDecimal(txtFDA.Text) + Convert.ToDecimal(txtVDA.Text)).ToString();
                    //            }
                    //        }
                    //        else
                    //        {
                    //            objSal.PfSalary = (Convert.ToDecimal(txtbasic.Text) + Convert.ToDecimal(txtFDA.Text) + Convert.ToDecimal(txtVDA.Text)).ToString();
                    //        }
                    //        if (Convert.ToDecimal(objSal.PfSalary) > 6500)
                    //        {
                    //            objSal.Emp_PF = "6500";
                    //        }
                    //        else
                    //        {
                    //            objSal.Emp_PF = objSal.PfSalary;
                    //        }
                    //    }
                    //    MyDate = DateTime.ParseExact(objSal.SalaryDate, "dd-MM-yyyy", null);

                    //    TempDate = Convert.ToDateTime(txtsalaryday.Text).AddMonths(-1).ToShortDateString();
                    //    TransDate = DateTime.ParseExact(TempDate, "dd/MM/yyyy", null);
                    //    DataTable dt_edit = new DataTable();
                    //    dt_edit = objdata.Advance_Edit(ddlempCodeStaff.SelectedValue, Mont, AdvAmt.ToString(), SessionCcode, SessionLcode);
                    //    if (dt_edit.Rows.Count > 0)
                    //    {
                    //        Dec_mont = (Convert.ToDecimal(dt_edit.Rows[0]["ReductionMonth"].ToString()) + 1).ToString();

                    //        if (dt_edit.Rows[0]["AdvanceId"].ToString() != "")
                    //        {
                    //            if (Dec_mont == "1")
                    //            {
                    //                Adv_BalanceAmt = ((Convert.ToDecimal(dt_edit.Rows[0]["BalanceAmount"].ToString()) + AdvAmt)).ToString();
                    //                //if ((Convert.ToDecimal(Adv_BalanceAmt) == Convert.ToDecimal(txtAdvance.Text)) || (Convert.ToDecimal(txtAdvance.Text) == 0))
                    //                if ((Convert.ToDecimal(Adv_BalanceAmt) == Convert.ToDecimal(objSal.Advance)) || (Convert.ToDecimal(objSal.Advance) == 0))
                    //                {
                    //                    if (Convert.ToDecimal(objSal.Advance) == 0)
                    //                    {
                    //                        string qry = "Update Advancerepayment set Amount='" + 0 + "' where EmpNo='" + ddlempCodeStaff.SelectedValue + "' and Months='" + Mont + "' and AdvanceID='" + dt_edit.Rows[0]["AdvanceId"].ToString() + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                    //                        SqlCommand cmd = new SqlCommand(qry, con);
                    //                        con.Open();
                    //                        cmd.ExecuteNonQuery();
                    //                        con.Close();

                    //                        objdata.Advance_repayInsert(ddlempCodeStaff.SelectedValue, MyDate, MyMonth, objSal.Advance, dt_edit.Rows[0]["AdvanceId"].ToString(), SessionCcode, SessionLcode);
                    //                        string bal = (Convert.ToDecimal(Adv_BalanceAmt) - Convert.ToDecimal(objSal.Advance)).ToString();
                    //                        string decrementMonths = Convert.ToInt32(Dec_mont).ToString();
                    //                        string IncrementMonths = (Convert.ToInt32(Increment_mont) + 1).ToString();
                    //                        string Inc = (Convert.ToInt32(dt_edit.Rows[0]["IncreaseMonth"].ToString()) - 1).ToString();
                    //                        string Completed = "N";
                    //                        string qry1 = "Update AdvancePayment set ReductionMonth='" + decrementMonths + "',IncreaseMonth='" + Inc + "', BalanceAmount='" + bal + "', Completed='" + Completed + "',Modifieddate=convert(datetime,'" + MyDate + "',105) where EmpNo='" + ddlempCodeStaff.SelectedValue + "' and ID='" + dt_edit.Rows[0]["ID"].ToString() + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                    //                        SqlCommand cmd1 = new SqlCommand(qry1, con);
                    //                        con.Open();
                    //                        cmd1.ExecuteNonQuery();
                    //                        con.Close();
                    //                        objdata.Advance_update(ddlempCodeStaff.SelectedValue, bal, IncrementMonths, decrementMonths, Adv_id, Completed, MyDate, SessionCcode, SessionLcode);
                    //                    }
                    //                    else
                    //                    {
                    //                        string qry2 = "Update Advancerepayment set Amount='" + 0 + "' where EmpNo='" + ddlempCodeStaff.SelectedValue + "' and Months='" + Mont + "' and AdvanceID='" + dt_edit.Rows[0]["AdvanceId"].ToString() + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                    //                        SqlCommand cmd2 = new SqlCommand(qry2, con);
                    //                        con.Open();
                    //                        cmd2.ExecuteNonQuery();
                    //                        con.Close();
                    //                        objdata.Advance_repayInsert(ddlempCodeStaff.SelectedValue, MyDate, MyMonth, objSal.Advance, Adv_id, SessionCcode, SessionLcode);
                    //                        string bal = (Convert.ToDecimal(Adv_BalanceAmt) - Convert.ToDecimal(objSal.Advance)).ToString();
                    //                        string decrementMonths = (Convert.ToInt32(Dec_mont) - 1).ToString();
                    //                        string IncrementMonths = (Convert.ToInt32(Increment_mont) + 1).ToString();
                    //                        string Inc = (Convert.ToInt32(dt_edit.Rows[0]["IncreaseMonth"].ToString()) - 1).ToString();
                    //                        string Completed = "Y";
                    //                        string qry3 = "Update AdvancePayment set ReductionMonth='" + Dec_mont.ToString() + "',IncreaseMonth='" + Inc + "',BalanceAmount='" + bal + "', Completed='" + Completed + "',Modifieddate=convert(datetime,'" + MyDate + "',105) where EmpNo='" + ddlempCodeStaff.SelectedValue + "' and ID='" + dt_edit.Rows[0]["ID"].ToString() + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                    //                        SqlCommand cmd3 = new SqlCommand(qry3, con);
                    //                        con.Open();
                    //                        cmd3.ExecuteNonQuery();
                    //                        con.Close();
                    //                        objdata.Advance_update(ddlempCodeStaff.SelectedValue, bal, IncrementMonths, decrementMonths, Adv_id, Completed, MyDate, SessionCcode, SessionLcode);
                    //                    }
                    //                    objdata.SalaryDetails_update(objSal, ddlempCodeStaff.SelectedValue, ExisitingNo, MyDate, MyMonth, Year, ddlFinaYear.SelectedValue, TransDate);
                    //                    string Balanceday = objdata.Contract_GetEmpDet(ddlempCodeStaff.SelectedValue, SessionCcode, SessionLcode);
                    //                    if (Balanceday.Trim() != "")
                    //                    {
                    //                        if (Convert.ToDecimal(Balanceday) > 0)
                    //                        {
                    //                            DataTable dt_det = new DataTable();
                    //                            dt_det = objdata.Contract_getPlanDetails(ddlempCodeStaff.SelectedValue, SessionCcode, SessionLcode);
                                                
                    //                            if (dt_det.Rows.Count > 0)
                    //                            {
                    //                                if (dt_det.Rows[0]["ContractType"].ToString() == "1")
                    //                                {
                    //                                    Balanceday = (Convert.ToDecimal(Balanceday) + Convert.ToDecimal(dt_det.Rows[0]["MonthDays"].ToString())).ToString();
                    //                                    val = (Convert.ToDecimal(txtempdays.Text) + Convert.ToDecimal(txtNFh.Text));
                    //                                    Balanceday = (Convert.ToDecimal(Balanceday) - val).ToString();
                    //                                    objdata.Contract_Reducing(Balanceday, ddlempCodeStaff.SelectedValue, val.ToString(), SessionCcode, SessionLcode);
                    //                                }
                    //                                else if (dt_det.Rows[0]["ContractType"].ToString() == "2")
                    //                                {
                    //                                    val = (Convert.ToDecimal(txtempdays.Text) + Convert.ToDecimal(txtNFh.Text));
                                                        
                    //                                    if (Convert.ToDecimal(dt_det.Rows[0]["MonthDays"].ToString()) >= Convert.ToDecimal(dt_det.Rows[0]["FixedDays"].ToString()))
                    //                                    {
                    //                                        Balanceday = (Convert.ToDecimal(Balanceday) + Convert.ToDecimal(dt_det.Rows[0]["MonthDays"].ToString())).ToString();
                    //                                        if (val >= Convert.ToDecimal(dt_det.Rows[0]["FixedDays"].ToString()))
                    //                                        {

                    //                                            string Mon = (Convert.ToInt32(dt_det.Rows[0]["DecMonth"].ToString()) - 1).ToString();
                    //                                            Balanceday = (Convert.ToDecimal(Balanceday) - val).ToString();
                    //                                            objdata.Not_Contract_Reducing_Month(ddlempCodeStaff.SelectedValue, val.ToString(), SessionCcode, SessionLcode);
                    //                                        }
                    //                                        else
                    //                                        {
                    //                                            string Mon = (Convert.ToInt32(dt_det.Rows[0]["DecMonth"].ToString()) + 1).ToString();
                    //                                            //Balanceday = (Convert.ToDecimal(Balanceday) - val).ToString();
                    //                                            //objdata.Contract_Reducing(Balanceday, ddlempCodeStaff.SelectedValue, val, SessionCcode, SessionLcode, Mon);
                    //                                            objdata.Contract_Reducing_Month(Balanceday, ddlempCodeStaff.SelectedValue, val.ToString(), SessionCcode, SessionLcode, Mon);
                    //                                        }
                    //                                    }
                    //                                    else
                    //                                    {
                    //                                        if (val >= Convert.ToDecimal(dt_det.Rows[0]["FixedDays"].ToString()))
                    //                                        {

                    //                                            string Mon = (Convert.ToInt32(dt_det.Rows[0]["DecMonth"].ToString()) - 1).ToString();
                    //                                            Balanceday = (Convert.ToDecimal(Balanceday) - val).ToString();
                    //                                            objdata.Contract_Reducing_Month(Balanceday, ddlempCodeStaff.SelectedValue, val.ToString(), SessionCcode, SessionLcode, Mon);
                    //                                        }
                    //                                        else
                    //                                        {
                    //                                            string Mon = (Convert.ToInt32(dt_det.Rows[0]["DecMonth"].ToString()) - 1).ToString();
                    //                                            //Balanceday = (Convert.ToDecimal(Balanceday) - val).ToString();
                    //                                            //objdata.Contract_Reducing(Balanceday, ddlempCodeStaff.SelectedValue, val, SessionCcode, SessionLcode, Mon);
                    //                                            objdata.Not_Contract_Reducing_Month(ddlempCodeStaff.SelectedValue, val.ToString(), SessionCcode, SessionLcode);
                    //                                        }
                    //                                    }
                    //                                }
                    //                                else if (dt_det.Rows[0]["ContractType"].ToString() == "3")
                    //                                {
                    //                                    Balanceday = (Convert.ToDecimal(Balanceday) + Convert.ToDecimal(dt_det.Rows[0]["MonthDays"].ToString())).ToString();
                    //                                    val = (Convert.ToDecimal(txtempdays.Text) + Convert.ToDecimal(txtNFh.Text));
                    //                                    Balanceday = (Convert.ToDecimal(Balanceday) - val).ToString();
                    //                                    objdata.Contract_Reducing(Balanceday, ddlempCodeStaff.SelectedValue, val.ToString(), SessionCcode, SessionLcode);
                    //                                }
                    //                            }
                    //                        }
                    //                    }
                    //                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert(' " + MyMonth + " Month Salary has been processed');", true);                                        
                    //                    //System.Windows.Forms.MessageBox.Show(MyMonth + " Month Salary has been processed", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    //                }
                    //            }
                    //            else
                    //            {
                    //                bool Err = false;
                    //                Adv_BalanceAmt = ((Convert.ToDecimal(dt_edit.Rows[0]["BalanceAmount"].ToString()) + AdvAmt)).ToString();
                    //                if (Convert.ToDecimal(objSal.Advance) > Convert.ToDecimal(Adv_BalanceAmt))
                    //                {
                    //                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Enter the Advance amount Properly');", true);
                    //                    //System.Windows.Forms.MessageBox.Show(MyMonth + "Enter the Advance Amount Proprly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    //                    Err = true;
                    //                }
                    //                else if (Convert.ToDecimal(objSal.Advance) == Convert.ToDecimal(Adv_BalanceAmt))
                    //                {
                    //                    string qry4 = "Update Advancerepayment set Amount='" + 0 + "' where EmpNo='" + ddlempCodeStaff.SelectedValue + "' and Months='" + Mont + "' and AdvanceID='" + dt_edit.Rows[0]["AdvanceId"].ToString() + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                    //                    SqlCommand cmd4 = new SqlCommand(qry4, con);
                    //                    con.Open();
                    //                    cmd4.ExecuteNonQuery();
                    //                    con.Close();
                    //                    objdata.Advance_repayInsert(ddlempCodeStaff.SelectedValue, MyDate, MyMonth, objSal.Advance, Adv_id, SessionCcode, SessionLcode);
                    //                    string bal = (Convert.ToDecimal(Adv_BalanceAmt) - Convert.ToDecimal(objSal.Advance)).ToString();
                    //                    string decrementMonths = (Convert.ToInt32(Dec_mont) - 1).ToString();
                    //                    string IncrementMonths = (Convert.ToInt32(Increment_mont) + 1).ToString();
                    //                    string Inc = (Convert.ToInt32(dt_edit.Rows[0]["IncreaseMonth"].ToString()) - 1).ToString();
                    //                    string Completed = "Y";
                    //                    string qry5 = "Update AdvancePayment set ReductionMonth='" + Dec_mont.ToString() + "',IncreaseMonth='" + Inc + "',BalanceAmount='" + bal + "', Completed='" + Completed + "',Modifieddate=convert(datetime,'" + MyDate + "',105) where EmpNo='" + ddlempCodeStaff.SelectedValue + "' and ID='" + dt_edit.Rows[0]["ID"].ToString() + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                    //                    SqlCommand cmd5 = new SqlCommand(qry5, con);
                    //                    con.Open();
                    //                    cmd5.ExecuteNonQuery();
                    //                    con.Close();

                    //                    objdata.Advance_update(ddlempCodeStaff.SelectedValue, bal, IncrementMonths, decrementMonths, Adv_id, Completed, MyDate, SessionCcode, SessionLcode);
                    //                }
                    //                else if (Convert.ToDecimal(txtAdvance.Text) == 0)
                    //                {
                    //                    string qry6 = "Update Advancerepayment set Amount='" + 0 + "' where EmpNo='" + ddlempCodeStaff.SelectedValue + "' and Months='" + Mont + "' and AdvanceID='" + dt_edit.Rows[0]["AdvanceId"].ToString() + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                    //                    SqlCommand cmd6 = new SqlCommand(qry6, con);
                    //                    con.Open();
                    //                    cmd6.ExecuteNonQuery();
                    //                    con.Close();
                    //                    objdata.Advance_repayInsert(ddlempCodeStaff.SelectedValue, MyDate, MyMonth, objSal.Advance, Adv_id, SessionCcode, SessionLcode);
                    //                    string bal = (Convert.ToDecimal(Adv_BalanceAmt) - Convert.ToDecimal(objSal.Advance)).ToString();
                    //                    string decrementMonths = Convert.ToInt32(Dec_mont).ToString();
                    //                    string IncrementMonths = (Convert.ToInt32(Increment_mont) + 1).ToString();
                    //                    string Inc = (Convert.ToInt32(dt_edit.Rows[0]["IncreaseMonth"].ToString()) - 1).ToString();
                    //                    string Completed = "N";
                    //                    string qry7 = "Update AdvancePayment set ReductionMonth='" + Dec_mont.ToString() + "',IncreaseMonth='" + Inc + "',BalanceAmount='" + bal + "', Completed='" + Completed + "',Modifieddate=convert(datetime,'" + MyDate + "',105) where EmpNo='" + ddlempCodeStaff.SelectedValue + "' and ID='" + dt_edit.Rows[0]["ID"].ToString() + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                    //                    SqlCommand cmd7 = new SqlCommand(qry7, con);
                    //                    con.Open();
                    //                    cmd7.ExecuteNonQuery();
                    //                    con.Close();
                    //                    Inc = (Convert.ToInt32(Inc) + 1).ToString();
                    //                    objdata.Advance_update(ddlempCodeStaff.SelectedValue, bal, IncrementMonths, decrementMonths, Adv_id, Completed, MyDate, SessionCcode, SessionLcode);
                    //                }
                    //                else
                    //                {
                    //                    string qry8 = "Update Advancerepayment set Amount='" + 0 + "' where EmpNo='" + ddlempCodeStaff.SelectedValue + "' and Months='" + Mont + "' and AdvanceID='" + dt_edit.Rows[0]["AdvanceId"].ToString() + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                    //                    SqlCommand cmd8 = new SqlCommand(qry8, con);
                    //                    con.Open();
                    //                    cmd8.ExecuteNonQuery();
                    //                    con.Close();
                    //                    objdata.Advance_repayInsert(ddlempCodeStaff.SelectedValue, MyDate, MyMonth, objSal.Advance, Adv_id, SessionCcode, SessionLcode);
                    //                    string bal = (Convert.ToDecimal(Adv_BalanceAmt) - Convert.ToDecimal(objSal.Advance)).ToString();
                    //                    string decrementMonths = (Convert.ToInt32(Dec_mont) - 1).ToString();
                    //                    string IncrementMonths = (Convert.ToInt32(Increment_mont) + 1).ToString();
                    //                    string Inc = (Convert.ToInt32(dt_edit.Rows[0]["IncreaseMonth"].ToString()) - 1).ToString();
                    //                    string Completed = "N";

                    //                    string qry9 = "Update AdvancePayment set ReductionMonth='" + Dec_mont.ToString() + "',IncreaseMonth='" + Inc + "',BalanceAmount='" + bal + "', Completed='" + Completed + "',Modifieddate=convert(datetime,'" + MyDate + "',105) where EmpNo='" + ddlempCodeStaff.SelectedValue + "' and ID='" + dt_edit.Rows[0]["ID"].ToString() + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                    //                    SqlCommand cmd9 = new SqlCommand(qry9, con);
                    //                    con.Open();
                    //                    cmd9.ExecuteNonQuery();
                    //                    con.Close();
                    //                    objdata.Advance_update(ddlempCodeStaff.SelectedValue, bal, IncrementMonths, decrementMonths, Adv_id, Completed, MyDate, SessionCcode, SessionLcode);
                    //                }
                    //                if (!Err)
                    //                {
                    //                    objdata.SalaryDetails_update(objSal, ddlempCodeStaff.SelectedValue, ExisitingNo, MyDate, MyMonth, Year, ddlFinaYear.SelectedValue, TransDate);
                    //                    string Balanceday = objdata.Contract_GetEmpDet(ddlempCodeStaff.SelectedValue, SessionCcode, SessionLcode);
                    //                    if (Balanceday.Trim() != "")
                    //                    {
                    //                        if (Convert.ToDecimal(Balanceday) > 0)
                    //                        {
                    //                            DataTable dt_det = new DataTable();
                    //                            dt_det = objdata.Contract_getPlanDetails(ddlempCodeStaff.SelectedValue, SessionCcode, SessionLcode);

                    //                            if (dt_det.Rows.Count > 0)
                    //                            {
                    //                                if (dt_det.Rows[0]["ContractType"].ToString() == "1")
                    //                                {
                    //                                    Balanceday = (Convert.ToDecimal(Balanceday) + Convert.ToDecimal(dt_det.Rows[0]["MonthDays"].ToString())).ToString();
                    //                                    val = (Convert.ToDecimal(txtempdays.Text) + Convert.ToDecimal(txtNFh.Text));
                    //                                    Balanceday = (Convert.ToDecimal(Balanceday) - val).ToString();
                    //                                    objdata.Contract_Reducing(Balanceday, ddlempCodeStaff.SelectedValue, val.ToString(), SessionCcode, SessionLcode);
                    //                                }
                    //                                else if (dt_det.Rows[0]["ContractType"].ToString() == "2")
                    //                                {
                    //                                    val = (Convert.ToDecimal(txtempdays.Text) + Convert.ToDecimal(txtNFh.Text));
                                                        
                    //                                    if (Convert.ToDecimal(dt_det.Rows[0]["MonthDays"].ToString()) >= Convert.ToDecimal(dt_det.Rows[0]["FixedDays"].ToString()))
                    //                                    {
                    //                                        Balanceday = (Convert.ToDecimal(Balanceday) + Convert.ToDecimal(dt_det.Rows[0]["MonthDays"].ToString())).ToString();
                    //                                        if (val >= Convert.ToDecimal(dt_det.Rows[0]["FixedDays"].ToString()))
                    //                                        {

                    //                                            string Mon = (Convert.ToInt32(dt_det.Rows[0]["DecMonth"].ToString()) - 1).ToString();
                    //                                            Balanceday = (Convert.ToDecimal(Balanceday) - val).ToString();
                    //                                            objdata.Not_Contract_Reducing_Month(ddlempCodeStaff.SelectedValue, val.ToString(), SessionCcode, SessionLcode);
                    //                                        }
                    //                                        else
                    //                                        {
                    //                                            string Mon = (Convert.ToInt32(dt_det.Rows[0]["DecMonth"].ToString()) + 1).ToString();
                    //                                            //Balanceday = (Convert.ToDecimal(Balanceday) - val).ToString();
                    //                                            //objdata.Contract_Reducing(Balanceday, ddlempCodeStaff.SelectedValue, val, SessionCcode, SessionLcode, Mon);
                    //                                            objdata.Contract_Reducing_Month(Balanceday, ddlempCodeStaff.SelectedValue, val.ToString(), SessionCcode, SessionLcode, Mon);
                    //                                        }
                    //                                    }
                    //                                    else
                    //                                    {
                    //                                        if (val >= Convert.ToDecimal(dt_det.Rows[0]["FixedDays"].ToString()))
                    //                                        {

                    //                                            string Mon = (Convert.ToInt32(dt_det.Rows[0]["DecMonth"].ToString()) - 1).ToString();
                    //                                            Balanceday = (Convert.ToDecimal(Balanceday) - val).ToString();
                    //                                            objdata.Contract_Reducing_Month(Balanceday, ddlempCodeStaff.SelectedValue, val.ToString(), SessionCcode, SessionLcode, Mon);
                    //                                        }
                    //                                        else
                    //                                        {
                    //                                            string Mon = (Convert.ToInt32(dt_det.Rows[0]["DecMonth"].ToString()) - 1).ToString();
                    //                                            //Balanceday = (Convert.ToDecimal(Balanceday) - val).ToString();
                    //                                            //objdata.Contract_Reducing(Balanceday, ddlempCodeStaff.SelectedValue, val, SessionCcode, SessionLcode, Mon);
                    //                                            objdata.Not_Contract_Reducing_Month(ddlempCodeStaff.SelectedValue, val.ToString(), SessionCcode, SessionLcode);
                    //                                        }
                    //                                    }
                    //                                }
                    //                                else if (dt_det.Rows[0]["ContractType"].ToString() == "3")
                    //                                {
                    //                                    Balanceday = (Convert.ToDecimal(Balanceday) + Convert.ToDecimal(dt_det.Rows[0]["MonthDays"].ToString())).ToString();
                    //                                    val = (Convert.ToDecimal(txtempdays.Text) + Convert.ToDecimal(txtNFh.Text));
                    //                                    Balanceday = (Convert.ToDecimal(Balanceday) - val).ToString();
                    //                                    objdata.Contract_Reducing(Balanceday, ddlempCodeStaff.SelectedValue, val.ToString(), SessionCcode, SessionLcode);
                    //                                }
                    //                            }
                    //                        }
                    //                    }
                    //                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert(' " + MyMonth + " Month Salary has been processed');", true);
                    //                    //System.Windows.Forms.MessageBox.Show(MyMonth + " Month Salary has been processed", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    //                }
                    //            }
                    //        }
                    //    }
                    //    else
                    //    {
                    //        objdata.SalaryDetails_update(objSal, ddlempCodeStaff.SelectedValue, ExisitingNo, MyDate, MyMonth, Year, ddlFinaYear.SelectedValue, TransDate);
                    //        string Balanceday = objdata.Contract_GetEmpDet(ddlempCodeStaff.SelectedValue, SessionCcode, SessionLcode);
                    //        if (Balanceday.Trim() != "")
                    //        {
                    //            if (Convert.ToDecimal(Balanceday) > 0)
                    //            {
                    //                DataTable dt_det = new DataTable();
                    //                dt_det = objdata.Contract_getPlanDetails(ddlempCodeStaff.SelectedValue, SessionCcode, SessionLcode);

                    //                if (dt_det.Rows.Count > 0)
                    //                {
                    //                    if (dt_det.Rows[0]["ContractType"].ToString() == "1")
                    //                    {
                    //                        Balanceday = (Convert.ToDecimal(Balanceday) + Convert.ToDecimal(dt_det.Rows[0]["MonthDays"].ToString())).ToString();
                    //                        val = (Convert.ToDecimal(txtempdays.Text) + Convert.ToDecimal(txtNFh.Text));
                    //                        Balanceday = (Convert.ToDecimal(Balanceday) - val).ToString();
                    //                        objdata.Contract_Reducing(Balanceday, ddlempCodeStaff.SelectedValue, val.ToString(), SessionCcode, SessionLcode);
                    //                    }
                    //                    else if (dt_det.Rows[0]["ContractType"].ToString() == "2")
                    //                    {
                    //                        val = (Convert.ToDecimal(txtempdays.Text) + Convert.ToDecimal(txtNFh.Text));
                                            
                    //                        if (Convert.ToDecimal(dt_det.Rows[0]["MonthDays"].ToString()) >= Convert.ToDecimal(dt_det.Rows[0]["FixedDays"].ToString()))
                    //                        {
                    //                            Balanceday = (Convert.ToDecimal(Balanceday) + Convert.ToDecimal(dt_det.Rows[0]["MonthDays"].ToString())).ToString();
                    //                            if (val >= Convert.ToDecimal(dt_det.Rows[0]["FixedDays"].ToString()))
                    //                            {

                    //                                string Mon = (Convert.ToInt32(dt_det.Rows[0]["DecMonth"].ToString()) - 1).ToString();
                    //                                Balanceday = (Convert.ToDecimal(Balanceday) - val).ToString();
                    //                                objdata.Not_Contract_Reducing_Month(ddlempCodeStaff.SelectedValue, val.ToString(), SessionCcode, SessionLcode);
                    //                            }
                    //                            else
                    //                            {
                    //                                string Mon = (Convert.ToInt32(dt_det.Rows[0]["DecMonth"].ToString()) + 1).ToString();
                    //                                //Balanceday = (Convert.ToDecimal(Balanceday) - val).ToString();
                    //                                //objdata.Contract_Reducing(Balanceday, ddlempCodeStaff.SelectedValue, val, SessionCcode, SessionLcode, Mon);
                    //                                objdata.Contract_Reducing_Month(Balanceday, ddlempCodeStaff.SelectedValue, val.ToString(), SessionCcode, SessionLcode, Mon);
                    //                            }
                    //                        }
                    //                        else
                    //                        {
                    //                            if (val >= Convert.ToDecimal(dt_det.Rows[0]["FixedDays"].ToString()))
                    //                            {

                    //                                string Mon = (Convert.ToInt32(dt_det.Rows[0]["DecMonth"].ToString()) - 1).ToString();
                    //                                Balanceday = (Convert.ToDecimal(Balanceday) - val).ToString();
                    //                                objdata.Contract_Reducing_Month(Balanceday, ddlempCodeStaff.SelectedValue, val.ToString(), SessionCcode, SessionLcode, Mon);
                    //                            }
                    //                            else
                    //                            {
                    //                                string Mon = (Convert.ToInt32(dt_det.Rows[0]["DecMonth"].ToString()) - 1).ToString();
                    //                                //Balanceday = (Convert.ToDecimal(Balanceday) - val).ToString();
                    //                                //objdata.Contract_Reducing(Balanceday, ddlempCodeStaff.SelectedValue, val, SessionCcode, SessionLcode, Mon);
                    //                                objdata.Not_Contract_Reducing_Month(ddlempCodeStaff.SelectedValue, val.ToString(), SessionCcode, SessionLcode);
                    //                            }
                    //                        }
                    //                    }
                    //                    else if (dt_det.Rows[0]["ContractType"].ToString() == "3")
                    //                    {
                    //                        Balanceday = (Convert.ToDecimal(Balanceday) + Convert.ToDecimal(dt_det.Rows[0]["MonthDays"].ToString())).ToString();
                    //                        val = (Convert.ToDecimal(txtempdays.Text) + Convert.ToDecimal(txtNFh.Text));
                    //                        Balanceday = (Convert.ToDecimal(Balanceday) - val).ToString();
                    //                        objdata.Contract_Reducing(Balanceday, ddlempCodeStaff.SelectedValue, val.ToString(), SessionCcode, SessionLcode);
                    //                    }
                    //                }
                    //            }
                    //        }
                    //        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert(' " + MyMonth + " Month Salary has been processed');", true);
                    //    }
                    //    //ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Your " + MyMonth + " Month Salary All Ready processed');", true);
                    //    //System.Windows.Forms.MessageBox.Show(MyMonth + " Month Salary All Ready processed", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    //}
                //}
            //}


        //}
        catch (Exception ex)
        {

        }
    }
    protected void btnreset_Click(object sender, EventArgs e)
    {
        clr();
        Response.Redirect("SalaryRegistration.aspx");
    }
    protected void btnpayslip_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;


        if (ddlempCodeStaff.SelectedValue == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Employee Number');", true);
            //System.Windows.Forms.MessageBox.Show("Enter the Employee Number", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            ErrFlag = true;
        }
        else if (txtsalaryday.Text == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Salary Date');", true);
            //System.Windows.Forms.MessageBox.Show("Enter the Salary Date", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            ErrFlag = true;
        }
        if (!ErrFlag)
        {
            string Print_Date = (Convert.ToDateTime(txtsalaryday.Text).AddMonths(-1)).ToShortDateString();
            int mm = Convert.ToInt32(Convert.ToDateTime(Print_Date).Month.ToString());

            #region Month
            string Mont = "";
            switch (mm)
            {
                case 1:
                    Mont = "January";
                    break;

                case 2:
                    Mont = "February";
                    break;
                case 3:
                    Mont = "March";
                    break;
                case 4:
                    Mont = "April";
                    break;
                case 5:
                    Mont = "May";
                    break;
                case 6:
                    Mont = "June";
                    break;
                case 7:
                    Mont = "July";
                    break;
                case 8:
                    Mont = "August";
                    break;
                case 9:
                    Mont = "September";
                    break;
                case 10:
                    Mont = "October";
                    break;
                case 11:
                    Mont = "November";
                    break;
                case 12:
                    Mont = "December";
                    break;
                default:
                    break;
            }
            #endregion
            if (ddlcategory.SelectedValue == "1")
            {
                stafflabour = "S";
            }
            else if (ddlcategory.SelectedValue == "2")
            {
                stafflabour = "L";
            }
            //System.Diagnostics.Process.Start("Reports.aspx?empNo=" + ddlempCodeStaff.SelectedValue + "&dateval=" + Mont + "&rpttype=payslip&stafftype=" + stafflabour + "&yr=" + Convert.ToDateTime(txtsalaryday.Text).Year.ToString());
            //Response.Write("<script type='text/javascript'>window.open('Reports.aspx?empNo=" + ddlempCodeStaff.SelectedValue + "&dateval=" + Mont + "&rpttype=payslip&stafftype=" + stafflabour + "&yr=" + Convert.ToDateTime(txtsalaryday.Text).Year.ToString() + "','_blank');</script>");
            //ClientScript.RegisterStartupScript(this.GetType(), "openwindow", "<script type='text/javascript'> window.open('Reports.aspx?empNo=" + ddlempCodeStaff.SelectedValue + "&dateval=" + Mont + "&rpttype=payslip&stafftype=" + stafflabour + "&yr=" + Convert.ToDateTime(txtsalaryday.Text).Year.ToString() + "'); </script>");
            //string navigate = "<script type= text/javascript>window.open('Reports.aspx?empNo=" + ddlempCodeStaff.SelectedValue + "&dateval=" + Mont + "&rpttype=payslip&stafftype=" + stafflabour + "&yr=" + Convert.ToDateTime(txtsalaryday.Text).Year.ToString() + "');</script>"; 

            //Response.Redirect("Reports.aspx?empNo=" + ddlempCodeStaff.SelectedValue + "&dateval=" + Mont + "&rpttype=payslip&stafftype=" + stafflabour + "&yr=" + Convert.ToDateTime(txtsalaryday.Text).Year.ToString());

            ResponseHelper.Redirect("Reports.aspx?empNo=" + ddlempCodeStaff.SelectedValue + "&dateval=" + Mont + "&rpttype=payslip&stafftype=" + stafflabour + "&yr=" + Convert.ToDateTime(Print_Date).Year.ToString(), "_blank", "");

        }
    }
    protected void btncancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("SalaryRegistration.aspx");
    }
    protected void btnEdit_Click(object sender, EventArgs e)
    {
        lbltext.Text = "";
        lblNetPayAmt.Text = "";
        if (ID == "")
        {
            if (btnEdit.Text == "Edit")
            {
                txtAdvance.Enabled = true;
                btnEdit.Text = "Ok";
            }
            else if (btnEdit.Text == "Ok")
            {
                if (Dec_mont == "1")
                {
                    if ((Convert.ToDecimal(Adv_BalanceAmt) == Convert.ToDecimal(txtAdvance.Text)) || (Convert.ToDecimal(txtAdvance.Text) == 0))
                    {
                        txtAdvance.Enabled = false;
                        btnEdit.Text = "Edit";
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('You must repay the Full Amount');", true);
                        //System.Windows.Forms.MessageBox.Show("You must Repay the Full Amount", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    }
                }
                else if (txtAdvance.Text == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Advance Amount');", true);
                    //System.Windows.Forms.MessageBox.Show("Enter the Advance Amount", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                }
                else if (txtAdvance.Text == ".")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Advance Amount');", true);
                    //System.Windows.Forms.MessageBox.Show("Enter the Advance Amount", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                }


                else
                {
                    if (Adv_id != "")
                    {
                        if (Convert.ToDecimal(txtAdvance.Text) > Convert.ToDecimal(Adv_BalanceAmt))
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Advance Amount');", true);
                            //System.Windows.Forms.MessageBox.Show("Enter the Advance Amount", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                        }
                        else
                        {
                            txtAdvance.Enabled = false;
                            btnEdit.Text = "Edit";
                        }
                    }
                    else
                    {
                        txtAdvance.Enabled = false;
                        btnEdit.Text = "Edit";
                    }
                }

            }
        }
        else
        {
            DateTime Md = new DateTime();
            TempDate = Convert.ToDateTime(txtsalaryday.Text).AddMonths(-1).ToShortDateString();
                    string Month = TempDate;
                    Md = Convert.ToDateTime(TempDate);
                    int d = Md.Month;
                    string Mont = "";
                    switch (d)
                    {
                        case 1:
                            Mont = "January";
                            break;

                        case 2:
                            Mont = "February";
                            break;
                        case 3:
                            Mont = "March";
                            break;
                        case 4:
                            Mont = "April";
                            break;
                        case 5:
                            Mont = "May";
                            break;
                        case 6:
                            Mont = "June";
                            break;
                        case 7:
                            Mont = "July";
                            break;
                        case 8:
                            Mont = "August";
                            break;
                        case 9:
                            Mont = "September";
                            break;
                        case 10:
                            Mont = "October";
                            break;
                        case 11:
                            Mont = "November";
                            break;
                        case 12:
                            Mont = "December";
                            break;
                        default:
                            break;
                    }
            if (btnEdit.Text == "Edit")
            {
                txtAdvance.Enabled = true;
                btnEdit.Text = "Ok";
            }
            else if (btnEdit.Text == "Ok")
            {
                DataTable dt_edit = new DataTable();
                dt_edit = objdata.Advance_Edit(ddlempCodeStaff.SelectedValue, Mont, AdvAmt.ToString(), SessionCcode, SessionLcode);
                if (dt_edit.Rows.Count > 0)
                {
                    Dec_mont = (Convert.ToDecimal(dt_edit.Rows[0]["ReductionMonth"].ToString()) + 1).ToString();

                    if (Dec_mont == "1")
                    {

                        Adv_BalanceAmt = ((Convert.ToDecimal(dt_edit.Rows[0]["BalanceAmount"].ToString()) + AdvAmt)).ToString();
                        if ((Convert.ToDecimal(Adv_BalanceAmt) == Convert.ToDecimal(txtAdvance.Text)) || (Convert.ToDecimal(txtAdvance.Text) == 0))
                        {
                            txtAdvance.Enabled = false;
                            btnEdit.Text = "Edit";
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('You must repay the Full Amount');", true);
                            //System.Windows.Forms.MessageBox.Show("You must Repay the Full Amount", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                        }
                    }
                    else if (txtAdvance.Text == "")
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Advance Amount');", true);
                        //System.Windows.Forms.MessageBox.Show("Enter the Advance Amount", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    }
                    else if (txtAdvance.Text == ".")
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Advance Amount');", true);
                        //System.Windows.Forms.MessageBox.Show("Enter the Advance Amount", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    }


                    else
                    {
                        if (Adv_id != "")
                        {
                            Adv_BalanceAmt = ((Convert.ToDecimal(dt_edit.Rows[0]["BalanceAmount"].ToString()) + AdvAmt)).ToString();
                            if (Convert.ToDecimal(txtAdvance.Text) > Convert.ToDecimal(Adv_BalanceAmt))
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Advance Amount');", true);
                                //System.Windows.Forms.MessageBox.Show("Enter the Advance Amount", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                            }
                            else
                            {
                                txtAdvance.Enabled = false;
                                btnEdit.Text = "Edit";
                            }
                        }
                        else
                        {
                            txtAdvance.Enabled = false;
                            btnEdit.Text = "Edit";
                        }
                    }
                }

            }
        }
    }
    protected void txtAdvance_TextChanged(object sender, EventArgs e)
    {
        lbltext.Text = "";
        lblNetPayAmt.Text = "";
    }
    protected void txtAllowance_TextChanged(object sender, EventArgs e)
    {
        lbltext.Text = "";
        lblNetPayAmt.Text = "";
    }
    protected void txtdeduction_TextChanged(object sender, EventArgs e)
    {
        lbltext.Text = "";
        lblNetPayAmt.Text = "";
    }
    protected void txtStamp_TextChanged(object sender, EventArgs e)
    {
        lbltext.Text = "";
        lblNetPayAmt.Text = "";
    }
    protected void btnCalculate_Click(object sender, EventArgs e)
    {
        try
        {
            bool ErrFlag = false;
            //if (EmpType == "2")
            //{
            //    if (txtbasic.Text.Trim() == "")
            //    {
            //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Basic Salary Amount');", true);
            //        ErrFlag = true;
            //    }
            //    else if (txtFDA.Text.Trim() == "")
            //    {
            //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the FDA Amount');", true);
            //        ErrFlag = true;
            //    }
            //    else if (txtVDA.Text.Trim() == "")
            //    {
            //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the VDA Amount');", true);
            //        ErrFlag = true;
            //    }
            //    else if (txtHRA.Text.Trim() == "")
            //    {
            //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the HRA Amount');", true);
            //        ErrFlag = true;
            //    }
            //    else if (txtOT.Text.Trim() == "")
            //    {
            //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the OT Amount');", true);
            //        ErrFlag = true;
            //    }
            //    else if (txtAllowance.Text.Trim() == "")
            //    {
            //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Allowance Amount');", true);
            //        ErrFlag = true;
            //    }
            //    else if (txtpf.Text.Trim() == "")
            //    {
            //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the PF Amount');", true);
            //        ErrFlag = true;
            //    }
            //    else if (txtESI.Text.Trim() == "")
            //    {
            //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the ESI Amount');", true);
            //        ErrFlag = true;
            //    }
            //    else if (txtAdvance.Text.Trim() == "")
            //    {
            //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Advance Amount');", true);
            //        ErrFlag = true;
            //    }
            //    else if (txtStamp.Text.Trim() == "")
            //    {
            //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Stamp Amount');", true);
            //        ErrFlag = true;
            //    }
            //    else if (txtunion.Text.Trim() == "")
            //    {
            //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Union Amount');", true);
            //        ErrFlag = true;
            //    }
            //    else if (txtdeduction.Text.Trim() == "")
            //    {
            //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Deduction Amount');", true);
            //        ErrFlag = true;
            //    }
            //    if (!ErrFlag)
            //    {
            //        EarningsAmt.Text = (Convert.ToDecimal(txtbasic.Text) + Convert.ToDecimal(txtFDA.Text) + Convert.ToDecimal(txtVDA.Text) + Convert.ToDecimal(txtHRA.Text) + Convert.ToDecimal(txtOT.Text) + Convert.ToDecimal(txtAllowance.Text) + Convert.ToDecimal(txtAllowance2.Text) + Convert.ToDecimal(txall3.Text) + Convert.ToDecimal(txtsll4.Text)).ToString();
            //        EarningsAmt.Text = (Math.Round(Convert.ToDecimal(EarningsAmt.Text), 2, MidpointRounding.ToEven)).ToString();
            //        DeductionAmt.Text = (Convert.ToDecimal(txtpf.Text) + Convert.ToDecimal(txtESI.Text) + Convert.ToDecimal(txtAdvance.Text) + Convert.ToDecimal(txtStamp.Text) + Convert.ToDecimal(txtunion.Text) + Convert.ToDecimal(txtdeduction.Text) + Convert.ToDecimal(txtdeduction2.Text) + Convert.ToDecimal(txtded3.Text) + Convert.ToDecimal(txtded4.Text)).ToString();
            //        DeductionAmt.Text = (Math.Round(Convert.ToDecimal(DeductionAmt.Text), 2, MidpointRounding.ToEven)).ToString();
            //        lblNetPayAmt.Text = (Convert.ToDecimal(EarningsAmt.Text) - Convert.ToDecimal(DeductionAmt.Text)).ToString();
            //        lblNetPayAmt.Text = (Math.Round(Convert.ToDecimal(lblNetPayAmt.Text), 0, MidpointRounding.AwayFromZero)).ToString();
            //        lbltext.Text = NumerictoNumber(Convert.ToInt32(lblNetPayAmt.Text), isUK).ToString() + " " + "Only";
            //    }
            //}
            //else if (EmpType == "4")
            //{
            //    if (txtbasic.Text.Trim() == "")
            //    {
            //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Basic Salary Amount');", true);
            //        ErrFlag = true;
            //    }
            //    else if (txtFDA.Text.Trim() == "")
            //    {
            //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the FDA Amount');", true);
            //        ErrFlag = true;
            //    }
            //    else if (txtVDA.Text.Trim() == "")
            //    {
            //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the VDA Amount');", true);
            //        ErrFlag = true;
            //    }
            //    else if (txtHRA.Text.Trim() == "")
            //    {
            //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the HRA Amount');", true);
            //        ErrFlag = true;
            //    }
            //    else if (txtOT.Text.Trim() == "")
            //    {
            //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the OT Amount');", true);
            //        ErrFlag = true;
            //    }
            //    else if (txtAllowance.Text.Trim() == "")
            //    {
            //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Allowance 1 Amount');", true);
            //        ErrFlag = true;
            //    }
            //    else if (txtpf.Text.Trim() == "")
            //    {
            //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the PF Amount');", true);
            //        ErrFlag = true;
            //    }
            //    else if (txtESI.Text.Trim() == "")
            //    {
            //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the ESI Amount');", true);
            //        ErrFlag = true;
            //    }
            //    else if (txtAdvance.Text.Trim() == "")
            //    {
            //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Advance Amount');", true);
            //        ErrFlag = true;
            //    }
            //    else if (txtStamp.Text.Trim() == "")
            //    {
            //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Stamp Amount');", true);
            //        ErrFlag = true;
            //    }
            //    else if (txtunion.Text.Trim() == "")
            //    {
            //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Union Amount');", true);
            //        ErrFlag = true;
            //    }
            //    else if (txtdeduction.Text.Trim() == "")
            //    {
            //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Deduction 1 Amount');", true);
            //        ErrFlag = true;
            //    }
            //    else if (txtdeduction2.Text.Trim() == "")
            //    {
            //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Deduction 2 Amount');", true);
            //        ErrFlag = true;
            //    }
            //    else if (txtded3.Text.Trim() == "")
            //    {
            //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Deduction 3 Amount');", true);
            //        ErrFlag = true;
            //    }
            //    else if (txtded4.Text.Trim() == "")
            //    {
            //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Deduction 4 Amount');", true);
            //        ErrFlag = true;
            //    }
            //    else if(txall3.Text.Trim() == "")
            //    {
            //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Allowance 3 Amount');", true);
            //        ErrFlag = true;
            //    }
            //    else if (txtAllowance2.Text.Trim() == "")
            //    {
            //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Allowance 2 Amount');", true);
            //        ErrFlag = true;
            //    }
            //    else if (txtsll4.Text.Trim() == "")
            //    {
            //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Allowance 4 Amount');", true);
            //        ErrFlag = true;
            //    }
            //    if (!ErrFlag)
            //    {
            //        EarningsAmt.Text = (Convert.ToDecimal(txtbasic.Text) + Convert.ToDecimal(txtFDA.Text) + Convert.ToDecimal(txtVDA.Text) + Convert.ToDecimal(txtHRA.Text) + Convert.ToDecimal(txtOT.Text) + Convert.ToDecimal(txtAllowance.Text) + Convert.ToDecimal(txtAllowance2.Text) + Convert.ToDecimal(txall3.Text) + Convert.ToDecimal(txtsll4.Text)).ToString();
            //        EarningsAmt.Text = (Math.Round(Convert.ToDecimal(EarningsAmt.Text), 2, MidpointRounding.ToEven)).ToString();
            //        DeductionAmt.Text = (Convert.ToDecimal(txtpf.Text) + Convert.ToDecimal(txtESI.Text) + Convert.ToDecimal(txtAdvance.Text) + Convert.ToDecimal(txtStamp.Text) + Convert.ToDecimal(txtunion.Text) + Convert.ToDecimal(txtdeduction.Text) + Convert.ToDecimal(txtdeduction2.Text) + Convert.ToDecimal(txtded3.Text) + Convert.ToDecimal(txtded4.Text)).ToString();
            //        DeductionAmt.Text = (Math.Round(Convert.ToDecimal(DeductionAmt.Text), 2, MidpointRounding.ToEven)).ToString();
            //        lblNetPayAmt.Text = (Convert.ToDecimal(EarningsAmt.Text) - Convert.ToDecimal(DeductionAmt.Text)).ToString();
            //        lblNetPayAmt.Text = (Math.Round(Convert.ToDecimal(lblNetPayAmt.Text), 0, MidpointRounding.AwayFromZero)).ToString();
            //        if (Convert.ToDecimal(lblNetPayAmt.Text) < 0)
            //        {
            //            lblNetPayAmt.Text = "0";
            //        }
            //        lbltext.Text = NumerictoNumber(Convert.ToInt32(lblNetPayAmt.Text), isUK).ToString() + " " + "Only";
            //    }
            //}
            //if (EmpType == "1")
            //{
                if (txtSBasic.Text.Trim() == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Basic Salary Amount');", true);
                    ErrFlag = true;
                }
                else if (txtSPF.Text.Trim() == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the PF Amount');", true);
                    ErrFlag = true;
                }
                else if (txtSesi.Text.Trim() == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the ESI Amount');", true);
                    ErrFlag = true;
                }
                else if (txtSAll1.Text.Trim() == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Allowance 1 Amount');", true);
                    ErrFlag = true;
                }
                else if (txtSAll2.Text.Trim() == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Allowance 2 Amount');", true);
                    ErrFlag = true;
                }
                else if (txtSAll3.Text.Trim() == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Allowance 3 Amount');", true);
                    ErrFlag = true;
                }
                else if (txtSAll4.Text.Trim() == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Allowance 4 Amount');", true);
                    ErrFlag = true;
                }
                else if (txtSAdvance.Text.Trim() == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Advance Amount');", true);
                    ErrFlag = true;
                }
                else if (txtSstamp.Text.Trim() == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Stamp Amount');", true);
                    ErrFlag = true;
                }
                else if (txtSded1.Text.Trim() == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Deduction 1 Amount');", true);
                    ErrFlag = true;
                }
                else if (txtSded2.Text.Trim() == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Deduction 2 Amount');", true);
                    ErrFlag = true;
                }
                else if (txtSded3.Text.Trim() == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Deduction 3 Amount');", true);
                    ErrFlag = true;
                }
                else if (txtSded4.Text.Trim() == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Deduction 4 Amount');", true);
                    ErrFlag = true;
                }
                else if (txtSLOP.Text.Trim() == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the LOP Amount');", true);
                    ErrFlag = true;
                }
                if (!ErrFlag)
                {
                    EarningsAmt.Text = (Convert.ToDecimal(txtSBasic.Text) + Convert.ToDecimal(txtSAll1.Text) + Convert.ToDecimal(txtSAll2.Text) + Convert.ToDecimal(txtSAll3.Text) + Convert.ToDecimal(txtSAll4.Text) + Convert.ToDecimal(txtSOT.Text) + Convert.ToDecimal(txtSAll5.Text) + Convert.ToDecimal(txtDayIncentive.Text) + Convert.ToDecimal(txtThreeSided.Text) + Convert.ToDecimal(txtHalf.Text) + Convert.ToDecimal(txtFull.Text) + Convert.ToDecimal(txtSpinning.Text)).ToString();
                    EarningsAmt.Text = (Math.Round(Convert.ToDecimal(EarningsAmt.Text), 2, MidpointRounding.ToEven)).ToString();
                    DeductionAmt.Text = (Convert.ToDecimal(txtSPF.Text) + Convert.ToDecimal(txtSesi.Text) + Convert.ToDecimal(txtSAdvance.Text) + Convert.ToDecimal(txtSstamp.Text) + Convert.ToDecimal(txtSLOP.Text) + Convert.ToDecimal(txtSded1.Text) + Convert.ToDecimal(txtSded2.Text) + Convert.ToDecimal(txtSded3.Text) + Convert.ToDecimal(txtSded4.Text) + Convert.ToDecimal(txtSded5.Text)).ToString();
                    DeductionAmt.Text = (Math.Round(Convert.ToDecimal(DeductionAmt.Text), 2, MidpointRounding.ToEven)).ToString();
                    lblNetPayAmt.Text = (Convert.ToDecimal(EarningsAmt.Text) - Convert.ToDecimal(DeductionAmt.Text)).ToString();
                    lblNetPayAmt.Text = (Math.Round(Convert.ToDecimal(lblNetPayAmt.Text), 0, MidpointRounding.AwayFromZero)).ToString();
                    if (Convert.ToDecimal(lblNetPayAmt.Text) < 0)
                    {
                        lblNetPayAmt.Text = "0";
                    }
                    lbltext.Text = NumerictoNumber(Convert.ToInt32(lblNetPayAmt.Text), isUK).ToString() + " " + "Only";
                }
            //}
            //else if (EmpType == "3")
            //{
            //    if (txtWBasic.Text.Trim() == "")
            //    {
            //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Basic Salary Amount');", true);
            //        ErrFlag = true;
            //    }
            //    else if (txtWot.Text.Trim() == "")
            //    {
            //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the OT Amount');", true);
            //        ErrFlag = true;
            //    }
            //    else if (txtWAll1.Text.Trim() == "")
            //    {
            //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Allowance 1 Amount');", true);
            //        ErrFlag = true;
            //    }
            //    else if (txtWall2.Text.Trim() == "")
            //    {
            //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Allowance 2 Amount');", true);
            //        ErrFlag = true;
            //    }
            //    else if (txtWall3.Text.Trim() == "")
            //    {
            //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Allowance 3 Amount');", true);
            //        ErrFlag = true;
            //    }
            //    else if (txtWall4.Text.Trim() == "")
            //    {
            //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Allowance 4 Amount');", true);
            //        ErrFlag = true;
            //    }
            //    else if (txtWpf.Text.Trim() == "")
            //    {
            //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the PF Amount');", true);
            //        ErrFlag = true;
            //    }
            //    else if (txtWesi.Text.Trim() == "")
            //    {
            //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the ESI Amount');", true);
            //        ErrFlag = true;
            //    }
            //    else if (txtWadvance.Text.Trim() == "")
            //    {
            //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Advance Amount');", true);
            //        ErrFlag = true;
            //    }
            //    else if (txtWded1.Text.Trim() == "")
            //    {
            //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Deduction 1 Amount');", true);
            //        ErrFlag = true;
            //    }
            //    else if (txtWded2.Text.Trim() == "")
            //    {
            //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Deduction 2 Amount');", true);
            //        ErrFlag = true;
            //    }
            //    else if (txtWDed3.Text.Trim() == "")
            //    {
            //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Deduction 3 Amount');", true);
            //        ErrFlag = true;
            //    }
            //    else if (txtWded4.Text.Trim() == "")
            //    {
            //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Deduction 4 Amount');", true);
            //        ErrFlag = true;
            //    }
            //    if (!ErrFlag)
            //    {
            //        EarningsAmt.Text = (Convert.ToDecimal(txtWBasic.Text) + Convert.ToDecimal(txtWAll1.Text) + Convert.ToDecimal(txtWall2.Text) + Convert.ToDecimal(txtWall3.Text) + Convert.ToDecimal(txtWall4.Text)).ToString();
            //        EarningsAmt.Text = (Math.Round(Convert.ToDecimal(EarningsAmt.Text), 2, MidpointRounding.ToEven)).ToString();
            //        DeductionAmt.Text = (Convert.ToDecimal(txtWpf.Text) + Convert.ToDecimal(txtWesi.Text) + Convert.ToDecimal(txtWadvance.Text) + Convert.ToDecimal(txtWstamp.Text) + Convert.ToDecimal(txtWded1.Text) + Convert.ToDecimal(txtWded2.Text) + Convert.ToDecimal(txtWDed3.Text) + Convert.ToDecimal(txtWded4.Text)).ToString();
            //        DeductionAmt.Text = (Math.Round(Convert.ToDecimal(DeductionAmt.Text), 2, MidpointRounding.ToEven)).ToString();
            //        lblNetPayAmt.Text = (Convert.ToDecimal(EarningsAmt.Text) - Convert.ToDecimal(DeductionAmt.Text)).ToString();
            //        lblNetPayAmt.Text = (Math.Round(Convert.ToDecimal(lblNetPayAmt.Text), 0, MidpointRounding.AwayFromZero)).ToString();
            //        if (Convert.ToDecimal(lblNetPayAmt.Text) < 0)
            //        {
            //            lblNetPayAmt.Text = "0";
            //        }
            //        lbltext.Text = NumerictoNumber(Convert.ToInt32(lblNetPayAmt.Text), isUK).ToString() + " " + "Only";
            //    }
            //}
            //else
            //{
            //    if (txtbasic.Text.Trim() == "")
            //    {
            //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Basic Salary Amount');", true);
            //        ErrFlag = true;
            //    }
            //    else if (txtFDA.Text.Trim() == "")
            //    {
            //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the FDA Amount');", true);
            //        ErrFlag = true;
            //    }
            //    else if (txtVDA.Text.Trim() == "")
            //    {
            //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the VDA Amount');", true);
            //        ErrFlag = true;
            //    }
            //    else if (txtHRA.Text.Trim() == "")
            //    {
            //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the HRA Amount');", true);
            //        ErrFlag = true;
            //    }
            //    else if (txtOT.Text.Trim() == "")
            //    {
            //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the OT Amount');", true);
            //        ErrFlag = true;
            //    }
            //    else if (txtAllowance.Text.Trim() == "")
            //    {
            //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Allowance Amount');", true);
            //        ErrFlag = true;
            //    }
            //    else if (txtpf.Text.Trim() == "")
            //    {
            //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the PF Amount');", true);
            //        ErrFlag = true;
            //    }
            //    else if (txtESI.Text.Trim() == "")
            //    {
            //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the ESI Amount');", true);
            //        ErrFlag = true;
            //    }
            //    else if (txtAdvance.Text.Trim() == "")
            //    {
            //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Advance Amount');", true);
            //        ErrFlag = true;
            //    }
            //    else if (txtStamp.Text.Trim() == "")
            //    {
            //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Stamp Amount');", true);
            //        ErrFlag = true;
            //    }
            //    else if (txtunion.Text.Trim() == "")
            //    {
            //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Union Amount');", true);
            //        ErrFlag = true;
            //    }
            //    else if (txtdeduction.Text.Trim() == "")
            //    {
            //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Deduction Amount');", true);
            //        ErrFlag = true;
            //    }
            //    if (!ErrFlag)
            //    {
            //        EarningsAmt.Text = (Convert.ToDecimal(txtbasic.Text) + Convert.ToDecimal(txtFDA.Text) + Convert.ToDecimal(txtVDA.Text) + Convert.ToDecimal(txtHRA.Text) + Convert.ToDecimal(txtOT.Text) + Convert.ToDecimal(txtAllowance.Text) + Convert.ToDecimal(txtAllowance2.Text) + Convert.ToDecimal(txall3.Text) + Convert.ToDecimal(txtsll4.Text)).ToString();
            //        EarningsAmt.Text = (Math.Round(Convert.ToDecimal(EarningsAmt.Text), 2, MidpointRounding.ToEven)).ToString();
            //        DeductionAmt.Text = (Convert.ToDecimal(txtpf.Text) + Convert.ToDecimal(txtESI.Text) + Convert.ToDecimal(txtAdvance.Text) + Convert.ToDecimal(txtStamp.Text) + Convert.ToDecimal(txtunion.Text) + Convert.ToDecimal(txtdeduction.Text) + Convert.ToDecimal(txtdeduction2.Text) + Convert.ToDecimal(txtded3.Text) + Convert.ToDecimal(txtded4.Text)).ToString();
            //        DeductionAmt.Text = (Math.Round(Convert.ToDecimal(DeductionAmt.Text), 2, MidpointRounding.ToEven)).ToString();
            //        lblNetPayAmt.Text = (Convert.ToDecimal(EarningsAmt.Text) - Convert.ToDecimal(DeductionAmt.Text)).ToString();
            //        lblNetPayAmt.Text = (Math.Round(Convert.ToDecimal(lblNetPayAmt.Text), 0, MidpointRounding.AwayFromZero)).ToString();
            //        lbltext.Text = NumerictoNumber(Convert.ToInt32(lblNetPayAmt.Text), isUK).ToString() + " " + "Only";
            //    }
            //}
        }
        catch (Exception ex)
        {
        }
    }
    protected void btnEmp_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptEmpDownload.aspx");
    }
    protected void btnatt_Click(object sender, EventArgs e)
    {
        Response.Redirect("AttenanceDownload.aspx");
    }
    protected void btnLeave_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptLeaveSample.aspx");
    }
    protected void btnOT_Click(object sender, EventArgs e)
    {
        Response.Redirect("OTDownload.aspx");
    }
    protected void txtunion_TextChanged(object sender, EventArgs e)
    {
        lbltext.Text = "";
        lblNetPayAmt.Text = "";
    }
    protected void txtAllowance2_TextChanged(object sender, EventArgs e)
    {
        lbltext.Text = "";
        lblNetPayAmt.Text = "";
    }
    protected void txtdeduction2_TextChanged(object sender, EventArgs e)
    {
        lbltext.Text = "";
        lblNetPayAmt.Text = "";
    }
    protected void txall3_TextChanged(object sender, EventArgs e)
    {
        lbltext.Text = "";
        lblNetPayAmt.Text = "";
    }
    protected void txtded3_TextChanged(object sender, EventArgs e)
    {
        lbltext.Text = "";
        lblNetPayAmt.Text = "";
    }
    protected void txtsll4_TextChanged(object sender, EventArgs e)
    {
        lbltext.Text = "";
        lblNetPayAmt.Text = "";
    }
    protected void txtded4_TextChanged(object sender, EventArgs e)
    {
        lbltext.Text = "";
        lblNetPayAmt.Text = "";
    }
    protected void txtSAll2_TextChanged(object sender, EventArgs e)
    {
        lbltext.Text = "";
        lblNetPayAmt.Text = "";
    }
    protected void txtSAll1_TextChanged(object sender, EventArgs e)
    {
        lbltext.Text = "";
        lblNetPayAmt.Text = "";
    }
    protected void txtSAll3_TextChanged(object sender, EventArgs e)
    {
        lbltext.Text = "";
        lblNetPayAmt.Text = "";
    }
    protected void txtSded1_TextChanged(object sender, EventArgs e)
    {
        lbltext.Text = "";
        lblNetPayAmt.Text = "";
    }
    protected void txtSAll4_TextChanged(object sender, EventArgs e)
    {
        lbltext.Text = "";
        lblNetPayAmt.Text = "";
    }
    protected void txtSded4_TextChanged(object sender, EventArgs e)
    {
        lbltext.Text = "";
        lblNetPayAmt.Text = "";
    }
    protected void txtSded3_TextChanged(object sender, EventArgs e)
    {
        lbltext.Text = "";
        lblNetPayAmt.Text = "";
    }
    protected void txtSded2_TextChanged(object sender, EventArgs e)
    {
        lbltext.Text = "";
        lblNetPayAmt.Text = "";
    }
    protected void txtWAll1_TextChanged(object sender, EventArgs e)
    {
        lbltext.Text = "";
        lblNetPayAmt.Text = "";
    }
    protected void txtWall2_TextChanged(object sender, EventArgs e)
    {
        lbltext.Text = "";
        lblNetPayAmt.Text = "";
    }
    protected void txtWUnion_TextChanged(object sender, EventArgs e)
    {
        lbltext.Text = "";
        lblNetPayAmt.Text = "";
    }
    protected void txtWall3_TextChanged(object sender, EventArgs e)
    {
        lbltext.Text = "";
        lblNetPayAmt.Text = "";
    }
    protected void txtWded1_TextChanged(object sender, EventArgs e)
    {
        lbltext.Text = "";
        lblNetPayAmt.Text = "";
    }
    protected void txtWall4_TextChanged(object sender, EventArgs e)
    {
        lbltext.Text = "";
        lblNetPayAmt.Text = "";
    }
    protected void txtWded2_TextChanged(object sender, EventArgs e)
    {
        lbltext.Text = "";
        lblNetPayAmt.Text = "";
    }
    protected void txtWDed3_TextChanged(object sender, EventArgs e)
    {
        lbltext.Text = "";
        lblNetPayAmt.Text = "";
    }
    protected void txtWded4_TextChanged(object sender, EventArgs e)
    {
        lbltext.Text = "";
        lblNetPayAmt.Text = "";
    }
    protected void txtSAdvance_TextChanged(object sender, EventArgs e)
    {
        lbltext.Text = "";
        lblNetPayAmt.Text = "";
    }
    protected void txtWadvance_TextChanged(object sender, EventArgs e)
    {
        lbltext.Text = "";
        lblNetPayAmt.Text = "";
    }
    protected void btnSadvance_Click(object sender, EventArgs e)
    {
        lbltext.Text = "";
        lblNetPayAmt.Text = "";
        if (ID == "")
        {
            if (btnSadvance.Text == "Edit")
            {
                txtSAdvance.Enabled = true;
                btnSadvance.Text = "Ok";
            }
            else if (btnSadvance.Text == "Ok")
            {
                if (Dec_mont == "1")
                {
                    if ((Convert.ToDecimal(Adv_BalanceAmt) == Convert.ToDecimal(txtSAdvance.Text)) || (Convert.ToDecimal(txtSAdvance.Text) == 0))
                    {
                        txtSAdvance.Enabled = false;
                        btnSadvance.Text = "Edit";
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('You must repay the Full Amount');", true);
                        //System.Windows.Forms.MessageBox.Show("You must Repay the Full Amount", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    }
                }
                else if (txtSAdvance.Text == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Advance Amount');", true);
                    //System.Windows.Forms.MessageBox.Show("Enter the Advance Amount", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                }
                else if (txtSAdvance.Text == ".")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Advance Amount');", true);
                    //System.Windows.Forms.MessageBox.Show("Enter the Advance Amount", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                }


                else
                {
                    if (Adv_id != "")
                    {
                        if (Convert.ToDecimal(txtSAdvance.Text) > Convert.ToDecimal(Adv_BalanceAmt))
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Advance Amount');", true);
                            //System.Windows.Forms.MessageBox.Show("Enter the Advance Amount", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                        }
                        else
                        {
                            txtSAdvance.Enabled = false;
                            btnSadvance.Text = "Edit";
                        }
                    }
                    else
                    {
                        txtSAdvance.Enabled = false;
                        btnSadvance.Text = "Edit";
                    }
                }

            }
        }
        else
        {
            DateTime Md = new DateTime();
            TempDate = Convert.ToDateTime(txtsalaryday.Text).AddMonths(-1).ToShortDateString();
            string Month = TempDate;
            Md = Convert.ToDateTime(TempDate);
            int d = Md.Month;
            string Mont = "";
            switch (d)
            {
                case 1:
                    Mont = "January";
                    break;

                case 2:
                    Mont = "February";
                    break;
                case 3:
                    Mont = "March";
                    break;
                case 4:
                    Mont = "April";
                    break;
                case 5:
                    Mont = "May";
                    break;
                case 6:
                    Mont = "June";
                    break;
                case 7:
                    Mont = "July";
                    break;
                case 8:
                    Mont = "August";
                    break;
                case 9:
                    Mont = "September";
                    break;
                case 10:
                    Mont = "October";
                    break;
                case 11:
                    Mont = "November";
                    break;
                case 12:
                    Mont = "December";
                    break;
                default:
                    break;
            }
            if (btnSadvance.Text == "Edit")
            {
                txtSAdvance.Enabled = true;
                btnSadvance.Text = "Ok";
            }
            else if (btnSadvance.Text == "Ok")
            {
                DataTable dt_edit = new DataTable();
                dt_edit = objdata.Advance_Edit(ddlempCodeStaff.SelectedValue, Mont, AdvAmt.ToString(), SessionCcode, SessionLcode);
                Dec_mont = (Convert.ToDecimal(dt_edit.Rows[0]["ReductionMonth"].ToString()) + 1).ToString();

                if (Dec_mont == "1")
                {

                    Adv_BalanceAmt = ((Convert.ToDecimal(dt_edit.Rows[0]["BalanceAmount"].ToString()) + AdvAmt)).ToString();
                    if ((Convert.ToDecimal(Adv_BalanceAmt) == Convert.ToDecimal(txtSAdvance.Text)) || (Convert.ToDecimal(txtSAdvance.Text) == 0))
                    {
                        txtSAdvance.Enabled = false;
                        btnSadvance.Text = "Edit";
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('You must repay the Full Amount');", true);
                        //System.Windows.Forms.MessageBox.Show("You must Repay the Full Amount", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    }
                }
                else if (txtSAdvance.Text == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Advance Amount');", true);
                    //System.Windows.Forms.MessageBox.Show("Enter the Advance Amount", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                }
                else if (txtSAdvance.Text == ".")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Advance Amount');", true);
                    //System.Windows.Forms.MessageBox.Show("Enter the Advance Amount", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                }


                else
                {
                    if (Adv_id != "")
                    {
                        Adv_BalanceAmt = ((Convert.ToDecimal(dt_edit.Rows[0]["BalanceAmount"].ToString()) + AdvAmt)).ToString();
                        if (Convert.ToDecimal(txtSAdvance.Text) > Convert.ToDecimal(Adv_BalanceAmt))
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Advance Amount');", true);
                            //System.Windows.Forms.MessageBox.Show("Enter the Advance Amount", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                        }
                        else
                        {
                            txtSAdvance.Enabled = false;
                            btnSadvance.Text = "Edit";
                        }
                    }
                    else
                    {
                        txtSAdvance.Enabled = false;
                        btnSadvance.Text = "Edit";
                    }
                }

            }
        }
    }
    protected void btnWadvance_Click(object sender, EventArgs e)
    {
        lbltext.Text = "";
        lblNetPayAmt.Text = "";
        if (ID == "")
        {
            if (btnWadvance.Text == "Edit")
            {
                txtWadvance.Enabled = true;
                btnWadvance.Text = "Ok";
            }
            else if (btnWadvance.Text == "Ok")
            {
                if (Dec_mont == "1")
                {
                    if ((Convert.ToDecimal(Adv_BalanceAmt) == Convert.ToDecimal(txtWadvance.Text)) || (Convert.ToDecimal(txtWadvance.Text) == 0))
                    {
                        txtWadvance.Enabled = false;
                        btnWadvance.Text = "Edit";
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('You must repay the Full Amount');", true);
                        //System.Windows.Forms.MessageBox.Show("You must Repay the Full Amount", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    }
                }
                else if (txtWadvance.Text == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Advance Amount');", true);
                    //System.Windows.Forms.MessageBox.Show("Enter the Advance Amount", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                }
                else if (txtWadvance.Text == ".")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Advance Amount');", true);
                    //System.Windows.Forms.MessageBox.Show("Enter the Advance Amount", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                }


                else
                {
                    if (Adv_id != "")
                    {
                        if (Convert.ToDecimal(txtWadvance.Text) > Convert.ToDecimal(Adv_BalanceAmt))
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Advance Amount');", true);
                            //System.Windows.Forms.MessageBox.Show("Enter the Advance Amount", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                        }
                        else
                        {
                            txtWadvance.Enabled = false;
                            btnWadvance.Text = "Edit";
                        }
                    }
                    else
                    {
                        txtWadvance.Enabled = false;
                        btnWadvance.Text = "Edit";
                    }
                }

            }
        }
        else
        {
            DateTime Md = new DateTime();
            TempDate = Convert.ToDateTime(txtsalaryday.Text).AddMonths(-1).ToShortDateString();
            string Month = TempDate;
            Md = Convert.ToDateTime(TempDate);
            int d = Md.Month;
            string Mont = "";
            switch (d)
            {
                case 1:
                    Mont = "January";
                    break;

                case 2:
                    Mont = "February";
                    break;
                case 3:
                    Mont = "March";
                    break;
                case 4:
                    Mont = "April";
                    break;
                case 5:
                    Mont = "May";
                    break;
                case 6:
                    Mont = "June";
                    break;
                case 7:
                    Mont = "July";
                    break;
                case 8:
                    Mont = "August";
                    break;
                case 9:
                    Mont = "September";
                    break;
                case 10:
                    Mont = "October";
                    break;
                case 11:
                    Mont = "November";
                    break;
                case 12:
                    Mont = "December";
                    break;
                default:
                    break;
            }
            if (btnWadvance.Text == "Edit")
            {
                txtWadvance.Enabled = true;
                btnWadvance.Text = "Ok";
            }
            else if (btnWadvance.Text == "Ok")
            {
                DataTable dt_edit = new DataTable();
                dt_edit = objdata.Advance_Edit(ddlempCodeStaff.SelectedValue, Mont, AdvAmt.ToString(), SessionCcode, SessionLcode);
                Dec_mont = (Convert.ToDecimal(dt_edit.Rows[0]["ReductionMonth"].ToString()) + 1).ToString();

                if (Dec_mont == "1")
                {

                    Adv_BalanceAmt = ((Convert.ToDecimal(dt_edit.Rows[0]["BalanceAmount"].ToString()) + AdvAmt)).ToString();
                    if ((Convert.ToDecimal(Adv_BalanceAmt) == Convert.ToDecimal(txtWadvance.Text)) || (Convert.ToDecimal(txtWadvance.Text) == 0))
                    {
                        txtWadvance.Enabled = false;
                        btnWadvance.Text = "Edit";
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('You must repay the Full Amount');", true);
                        //System.Windows.Forms.MessageBox.Show("You must Repay the Full Amount", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    }
                }
                else if (txtWadvance.Text == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Advance Amount');", true);
                    //System.Windows.Forms.MessageBox.Show("Enter the Advance Amount", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                }
                else if (txtWadvance.Text == ".")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Advance Amount');", true);
                    //System.Windows.Forms.MessageBox.Show("Enter the Advance Amount", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                }


                else
                {
                    if (Adv_id != "")
                    {
                        Adv_BalanceAmt = ((Convert.ToDecimal(dt_edit.Rows[0]["BalanceAmount"].ToString()) + AdvAmt)).ToString();
                        if (Convert.ToDecimal(txtWadvance.Text) > Convert.ToDecimal(Adv_BalanceAmt))
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Advance Amount');", true);
                            //System.Windows.Forms.MessageBox.Show("Enter the Advance Amount", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                        }
                        else
                        {
                            txtWadvance.Enabled = false;
                            btnWadvance.Text = "Edit";
                        }
                    }
                    else
                    {
                        txtWadvance.Enabled = false;
                        btnWadvance.Text = "Edit";
                    }
                }

            }
        }
    }
    protected void txtSLOP_TextChanged(object sender, EventArgs e)
    {
        lbltext.Text = "";
        lblNetPayAmt.Text = "";
    }
    protected void txtWstamp_TextChanged(object sender, EventArgs e)
    {
        lbltext.Text = "";
        lblNetPayAmt.Text = "";
    }
    protected void txtlossofpay_TextChanged(object sender, EventArgs e)
    {
        lbltext.Text = "";
        lblNetPayAmt.Text = "";
    }
    protected void txtempdays_TextChanged(object sender, EventArgs e)
    {
        lbltext.Text = "";
        lblNetPayAmt.Text = "";
    }
    protected void txtSstamp_TextChanged(object sender, EventArgs e)
    {
        lbltext.Text = "";
        lblNetPayAmt.Text = "";
    }
    protected void ddlFinaYear_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void btngroup_Click(object sender, EventArgs e)
    {
        Response.Redirect("SalaryCalculation.aspx");
    }
    protected void rbsalary_SelectedIndexChanged(object sender, EventArgs e)
    {
        clr();
        DataTable dt_empty = new DataTable();
        //ddldepartment.DataSource = dt_empty;
        //ddldepartment.DataBind();
        ddlempCodeStaff.DataSource = dt_empty;
        ddlempCodeStaff.DataBind();
        ddlempNameStaff.DataSource = dt_empty;
        ddlempNameStaff.DataBind();
        ddToken.DataSource = dt_empty;
        ddToken.DataBind();
        PanelStaff.Visible = false;
        PanelWorker.Visible = false;
        PanelPermenant.Visible = false;
        if (rbsalary.SelectedValue == "1")
        {
            PanelMonths.Visible = false;
        }
        else if (rbsalary.SelectedValue == "2")
        {
            PanelMonths.Visible = false;
        }
        else if (rbsalary.SelectedValue == "3")
        {
            PanelMonths.Visible = false;
        }
    }
    protected void txtTo_TextChanged(object sender, EventArgs e)
    {
        try
        {
            
            bool ErrFlag = false;
            ID = "";
            //bool ErrFlag = false;
            string temp_exist = "";
            string temp_salary = "";
            string temp_fromdate = "";
            string temp_todate = "";
            bool SecondErr = false;
            temp_exist = txtexistingNo.Text;
            temp_salary = txtsalaryday.Text;
            temp_fromdate = txtfrom.Text;
            temp_todate = txtTo.Text;
            clr();
            txtfrom.Text = temp_fromdate;
            txtTo.Text = temp_todate;
            txtexistingNo.Text = temp_exist;
            txtsalaryday.Text = temp_salary;

            string Months = "", MonthValue = "";
            if (txtfrom.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the From Date...');", true);
                txtfrom.Text = null;
                txtTo.Text = null;
                ErrFlag = true;
            }
            else if (txtTo.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the To Date...');", true);
                txtfrom.Text = null;
                txtTo.Text = null;
                ErrFlag = true;
            }
            
            else if (ddlcategory.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select Category...!');", true);

                ErrFlag = true;
            }
            else if (ddlempCodeStaff.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Enter Registration Number ...!');", true);

                ErrFlag = true;

            }
            if (!ErrFlag)
            {
                DateTime dfrom = Convert.ToDateTime(txtfrom.Text);
                DateTime dtto = Convert.ToDateTime(txtTo.Text);
                DateTime MyDate1 = DateTime.ParseExact(txtfrom.Text, "dd-MM-yyyy", null);
                DateTime MyDate2 = DateTime.ParseExact(txtTo.Text, "dd-MM-yyyy", null);
                if (dtto < dfrom)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the From Date...');", true);
                    txtfrom.Text = null;
                    txtTo.Text = null;
                    ErrFlag = true;
                }
                    if (!ErrFlag)
                    {
                        TempDate = Convert.ToDateTime(txtfrom.Text).AddMonths(0).ToShortDateString();
                        string Month = TempDate;
                        DateTime m;
                        m = Convert.ToDateTime(TempDate);
                        d = m.Month;
                        int mon = m.Month;
                        int yr = m.Year;
                        #region Month
                        string Mont = "";
                        switch (d)
                        {
                            case 1:
                                Mont = "January";
                                break;

                            case 2:
                                Mont = "February";
                                break;
                            case 3:
                                Mont = "March";
                                break;
                            case 4:
                                Mont = "April";
                                break;
                            case 5:
                                Mont = "May";
                                break;
                            case 6:
                                Mont = "June";
                                break;
                            case 7:
                                Mont = "July";
                                break;
                            case 8:
                                Mont = "August";
                                break;
                            case 9:
                                Mont = "September";
                                break;
                            case 10:
                                Mont = "October";
                                break;
                            case 11:
                                Mont = "November";
                                break;
                            case 12:
                                Mont = "December";
                                break;
                            default:
                                break;
                        }
                        #endregion
                        DataTable dtAttenance = new DataTable();

                        dtAttenance = objdata.Salary_attenanceDetails(ddlempCodeStaff.SelectedValue, Mont, ddlFinaYear.SelectedValue, ddldepartment.SelectedValue, SessionCcode, SessionLcode, MyDate1, MyDate2);
                        if (dtAttenance.Rows.Count <= 0)
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('No Data Found');", true);
                        }
                        else
                        {
                            if (dtAttenance.Rows.Count > 0)
                            {

                                txtempdays.Text = dtAttenance.Rows[0]["Days"].ToString();
                                txtwork.Text = dtAttenance.Rows[0]["TotalDays"].ToString();
                                txtNFh.Text = dtAttenance.Rows[0]["NFh"].ToString();
                                txtcl.Text = dtAttenance.Rows[0]["CL"].ToString();
                                txtlossofpay.Text = dtAttenance.Rows[0]["AbsentDays"].ToString();
                                txtweekoff.Text = dtAttenance.Rows[0]["weekoff"].ToString();
                                txthome.Text = dtAttenance.Rows[0]["home"].ToString();
                                halfNight = dtAttenance.Rows[0]["halfNight"].ToString();
                                FullNight = dtAttenance.Rows[0]["FullNight"].ToString();
                                ThreeSided = dtAttenance.Rows[0]["ThreeSided"].ToString();
                                txtrest.Text = dtAttenance.Rows[0]["Rest"].ToString();
                                //txtlossofpay.Text = (Convert.ToDecimal(txtwork.Text) - (Convert.ToDecimal(txtempdays.Text) + (Convert.ToDecimal(txtNFh.Text) + (Convert.ToDecimal(txtcl.Text))))).ToString();
                                DataTable dt_ot = new DataTable();
                                dt_ot = objdata.Attenance_ot(ddlempCodeStaff.SelectedValue, Mont, ddlFinaYear.SelectedValue, ddldepartment.SelectedValue, SessionCcode, SessionLcode, MyDate1, MyDate2);
                                if (dt_ot.Rows.Count > 0)
                                {
                                    txtSOT.Text = dt_ot.Rows[0]["netAmount"].ToString();
                                }
                                else
                                {
                                    txtSOT.Text = "0";
                                }
                            }
                            string Salarymonth = objdata.SalaryMonthFromLeave(ddlempCodeStaff.SelectedValue, ddlFinaYear.SelectedValue, SessionCcode, SessionLcode,Mont, MyDate1, MyDate2);

                            string MonthEEE = Salarymonth.Replace(" ", "");

                            int result = string.Compare(Salarymonth, Mont, true);
                            //int i = DateTime.ParseExact(Salarymonth, "MMMM", CultureInfo.CurrentCulture).Month;
                            string PayDay = objdata.SalaryPayDay();
                            // txtattendancewithpay.Text = PayDay;
                            if (Mont == MonthEEE)
                            {
                                DataTable dteligible = new DataTable();
                                DataTable dtpf = new DataTable();
                                DataTable dt1 = new DataTable();
                                dt1 = objdata.NewSlary_Load(SessionCcode, SessionLcode, ddlempCodeStaff.SelectedValue);
                                DataTable dt_val = new DataTable();
                                dt_val = objdata.Salary_retrive(ddlempCodeStaff.SelectedValue, ddlFinaYear.SelectedValue, Mont, SessionCcode, SessionLcode);
                                dtpf = objdata.Load_pf_details(SessionCcode, SessionLcode);
                                dteligible = objdata.EligibleESI_PF(SessionCcode, SessionLcode, ddlempCodeStaff.SelectedValue);
                                if (ddlcategory.SelectedValue == "1")
                                {
                                    
                                    //DataTable dt1 = new DataTable();
                                    //dt1 = objdata.NewSlary_Load(SessionCcode, SessionLcode, ddlempCodeStaff.SelectedValue);

                                    
                                    dtpf = objdata.Load_pf_details(SessionCcode, SessionLcode);
                                    dteligible = objdata.EligibleESI_PF(SessionCcode, SessionLcode, ddlempCodeStaff.SelectedValue);
                                    
                                    if (dt_val.Rows.Count > 0)
                                    {
                                        pf = dt1.Rows[0]["PFS"].ToString();
                                        txtSBasic.Text = (Convert.ToDecimal(dt1.Rows[0]["Base"].ToString())).ToString();
                                        if ((Convert.ToDecimal(txtempdays.Text.Trim()) + Convert.ToDecimal(txtcl.Text.Trim())) >= Convert.ToDecimal(txtwork.Text.Trim()))
                                        {
                                            txtSBasic.Text = (Convert.ToDecimal(txtSBasic.Text) / Convert.ToDecimal(txtwork.Text)).ToString();
                                            txtSBasic.Text = (Convert.ToDecimal(txtSBasic.Text) * (Convert.ToDecimal(txtwork.Text))).ToString();
                                        }
                                        else
                                        {
                                            txtSBasic.Text = (Convert.ToDecimal(txtSBasic.Text) / Convert.ToDecimal(txtwork.Text)).ToString();
                                            txtSBasic.Text = (Convert.ToDecimal(txtSBasic.Text) * (Convert.ToDecimal(txtcl.Text) + Convert.ToDecimal(txtempdays.Text))).ToString();
                                        }
                                        
                                        //txtSAll1.Text = (Convert.ToDecimal(dt1.Rows[0]["Alllowance1amt"].ToString()) * Convert.ToDecimal(txtempdays.Text)).ToString();
                                        txtSAll1.Text = ((Convert.ToDecimal(txtempdays.Text) + Convert.ToDecimal(txtcl.Text)) * Convert.ToDecimal(dt1.Rows[0]["Alllowance1amt"].ToString())).ToString();
                                        txtSAll1.Text = (Math.Round(Convert.ToDecimal(txtSAll1.Text), 0, MidpointRounding.AwayFromZero)).ToString();
                                        //txtSAll2.Text = (Convert.ToDecimal(dt1.Rows[0]["Allowance2amt"].ToString()) * Convert.ToDecimal(txtempdays.Text)).ToString();
                                        txtSAll2.Text = ((Convert.ToDecimal(txtempdays.Text) + Convert.ToDecimal(txtcl.Text)) * Convert.ToDecimal(dt1.Rows[0]["Allowance2amt"].ToString())).ToString();
                                        txtSAll2.Text = (Math.Round(Convert.ToDecimal(txtSAll2.Text), 0, MidpointRounding.AwayFromZero)).ToString();
                                        //txtSded1.Text = (Convert.ToDecimal(dt1.Rows[0]["Deduction1"].ToString()) * Convert.ToDecimal(txtempdays.Text)).ToString();
                                        txtSded1.Text = ((Convert.ToDecimal(txtempdays.Text) + (Convert.ToDecimal(txtrest.Text.Trim()))) * Convert.ToDecimal(dt1.Rows[0]["Deduction1"].ToString())).ToString();
                                        txtSded1.Text = (Math.Round(Convert.ToDecimal(txtSded1.Text), 0, MidpointRounding.AwayFromZero)).ToString();
                                        //txtSded2.Text = (Convert.ToDecimal(dt1.Rows[0]["Deduction2"].ToString()) * Convert.ToDecimal(txtempdays.Text)).ToString();
                                        txtSded2.Text = ((Convert.ToDecimal(txtempdays.Text)) * Convert.ToDecimal(dt1.Rows[0]["Deduction2"].ToString())).ToString();
                                        txtSded2.Text = (Math.Round(Convert.ToDecimal(txtSded2.Text), 0, MidpointRounding.AwayFromZero)).ToString();
                                        txtSLOP.Text = ((Convert.ToDecimal(txtSBasic.Text) / Convert.ToDecimal(txtwork.Text)) * Convert.ToDecimal(txtlossofpay.Text)).ToString();
                                        txtSLOP.Text = (Math.Round(Convert.ToDecimal(txtSLOP.Text), 0, MidpointRounding.AwayFromZero)).ToString();
                                        txtSstamp.Text = dtpf.Rows[0]["StampCg"].ToString();
                                        decimal basicsalary = (Convert.ToDecimal(txtSBasic.Text) - Convert.ToDecimal(txtSLOP.Text));
                                        //string TemSalDate = Convert.ToDateTime(dt_val.Rows[0]["TransDate"].ToString()).ToShortDateString();
                                        //txtsalaryday.Text = (DateTime.ParseExact(dt_val.Rows[0]["TransDate"].ToString(), "dd-MM-yyyy", null)).ToString();

                                        if (dteligible.Rows[0]["EligiblePF"].ToString() == "1")
                                        {
                                            decimal PF_val = (Convert.ToDecimal(dt1.Rows[0]["PFS"].ToString()));
                                            txtSPF.Text = ((PF_val * Convert.ToDecimal(dtpf.Rows[0]["PF_per"].ToString())) / 100).ToString();
                                            txtSPF.Text = (Math.Round(Convert.ToDecimal(txtSPF.Text), 0, MidpointRounding.AwayFromZero)).ToString();
                                        }
                                        else
                                        {
                                            txtSPF.Text = "0";
                                        }
                                        if (dteligible.Rows[0]["ElgibleESI"].ToString() == "1")
                                        {
                                            decimal ESI_val = (Convert.ToDecimal(basicsalary));
                                            txtSesi.Text = ((ESI_val * Convert.ToDecimal(dtpf.Rows[0]["ESI_per"].ToString())) / 100).ToString();
                                            txtSesi.Text = (Math.Round(Convert.ToDecimal(txtSesi.Text), 2, MidpointRounding.ToEven)).ToString();
                                            string[] split_val = txtSesi.Text.Split('.');
                                            string Ist = split_val[0].ToString();
                                            string IInd = split_val[1].ToString();

                                            if (Convert.ToInt32(IInd) > 0)
                                            {
                                                txtSesi.Text = (Convert.ToInt32(Ist) + 1).ToString();
                                            }
                                            else
                                            {
                                                txtSesi.Text = Ist;
                                            }

                                        }
                                        else
                                        {
                                            txtSesi.Text = "0";
                                        }
                                        txtSAll3.Text = dt_val.Rows[0]["allowances3"].ToString();
                                        txtSAll4.Text = dt_val.Rows[0]["allowances4"].ToString();
                                        txtSAll5.Text = dt_val.Rows[0]["allowances5"].ToString();
                                        txtSded3.Text = dt_val.Rows[0]["Deduction3"].ToString();
                                        txtSded4.Text = dt_val.Rows[0]["Deduction4"].ToString();
                                        txtSded5.Text = dt_val.Rows[0]["Deduction5"].ToString();
                                        txtSAdvance.Text = dt_val.Rows[0]["Advance"].ToString();
                                        txtHalf.Text = "0.00";
                                        txtFull.Text = "0.00";
                                        txtSpinning.Text = "0.00";
                                        txtThreeSided.Text = "0.00";
                                        txtDayIncentive.Text = "0.00";
                                    }
                                }
                                else
                                {
                                    DataTable dt_con = new DataTable();
                                    DataTable dt_val1 = new DataTable();
                                    dt_con = objdata.Contract_SalaryCal(ddlempCodeStaff.SelectedValue, SessionCcode, SessionLcode);
                                    if (dt_con.Rows.Count > 0)
                                    {
                                        dt_val1 = objdata.Contract_Eligible(dt_con.Rows[0]["ContractName"].ToString());
                                        if (Convert.ToInt32(dt_con.Rows[0]["BasicMonths"].ToString()) < Convert.ToInt32(dt_val1.Rows[0]["NotEMonths"].ToString()))
                                        {
                                            //basic = (Convert.ToDecimal(dt1.Rows[0]["Base"].ToString()) * (Convert.ToDecimal(EmployeeDays) + Convert.ToInt32(cl))).ToString();
                                            //basic = (Math.Round(Convert.ToDecimal(basic), 0, MidpointRounding.AwayFromZero)).ToString();
                                            txtSBasic.Text = (Convert.ToDecimal(dt1.Rows[0]["Base"].ToString()) * (Convert.ToDecimal(txtempdays.Text) + Convert.ToInt32(txtcl.Text))).ToString();
                                            txtSBasic.Text = (Math.Round(Convert.ToDecimal(txtSBasic.Text), 0, MidpointRounding.AwayFromZero)).ToString();
                                        }
                                        else
                                        {
                                            if (dt_con.Rows[0]["MonthCount"].ToString() == "0")
                                            {
                                                string ID_val = "1";
                                                string slbAmt = objdata.Slb_Salary(ID_val, dt_con.Rows[0]["ContractName"].ToString());
                                                if (slbAmt.Trim() == "")
                                                {
                                                    slbAmt = "0";
                                                }
                                                if ((Convert.ToDecimal(txtempdays.Text) + Convert.ToDecimal(txtcl.Text)) >= (Convert.ToDecimal(dt_con.Rows[0]["FixedDays"].ToString())))
                                                {
                                                    //basic = (Convert.ToDecimal(slbAmt) * (Convert.ToDecimal(EmployeeDays) + Convert.ToInt32(cl))).ToString();
                                                    //basic = (Math.Round(Convert.ToDecimal(basic), 0, MidpointRounding.AwayFromZero)).ToString();
                                                    txtSBasic.Text = (Convert.ToDecimal(slbAmt) * (Convert.ToDecimal(txtempdays.Text) + Convert.ToInt32(txtcl.Text))).ToString();
                                                    txtSBasic.Text = (Math.Round(Convert.ToDecimal(txtSBasic.Text), 0, MidpointRounding.AwayFromZero)).ToString();
                                                }
                                                else
                                                {
                                                    //basic = (Convert.ToDecimal(dt1.Rows[0]["Base"].ToString()) * (Convert.ToDecimal(EmployeeDays) + Convert.ToInt32(cl))).ToString();
                                                    //basic = (Math.Round(Convert.ToDecimal(basic), 0, MidpointRounding.AwayFromZero)).ToString();
                                                    txtSBasic.Text = (Convert.ToDecimal(dt1.Rows[0]["Base"].ToString()) * (Convert.ToDecimal(txtempdays.Text) + Convert.ToInt32(txtcl.Text))).ToString();
                                                    txtSBasic.Text = (Math.Round(Convert.ToDecimal(txtSBasic.Text), 0, MidpointRounding.AwayFromZero)).ToString();

                                                }
                                            }
                                            else
                                            {
                                                if ((Convert.ToDecimal(txtempdays.Text) + Convert.ToDecimal(txtcl.Text)) >= (Convert.ToDecimal(dt_con.Rows[0]["FixedDays"].ToString())))
                                                {
                                                    string ID_val = dt_con.Rows[0]["MonthCount"].ToString();
                                                    ID_val = (Convert.ToInt32(ID_val) + 1).ToString();
                                                    string slbAmt = objdata.Slb_Salary(ID_val, dt_con.Rows[0]["ContractName"].ToString());
                                                    if (slbAmt.Trim() == "")
                                                    {
                                                        slbAmt = "0";
                                                    }
                                                    //basic = (Convert.ToDecimal(slbAmt) * (Convert.ToDecimal(EmployeeDays) + Convert.ToInt32(cl))).ToString();
                                                    //basic = (Math.Round(Convert.ToDecimal(basic), 0, MidpointRounding.AwayFromZero)).ToString();
                                                    txtSBasic.Text = (Convert.ToDecimal(slbAmt) * (Convert.ToDecimal(txtempdays.Text) + Convert.ToInt32(txtcl.Text))).ToString();
                                                    txtSBasic.Text = (Math.Round(Convert.ToDecimal(txtSBasic.Text), 0, MidpointRounding.AwayFromZero)).ToString();
                                                }
                                                else
                                                {
                                                    string ID_val = dt_con.Rows[0]["MonthCount"].ToString();
                                                    //ID_val = (Convert.ToInt32(ID_val) + 1);
                                                    string slbAmt = objdata.Slb_Salary(ID_val, dt_con.Rows[0]["ContractName"].ToString());
                                                    if (slbAmt.Trim() == "")
                                                    {
                                                        slbAmt = "0";
                                                    }
                                                    txtSBasic.Text = (Convert.ToDecimal(slbAmt) * (Convert.ToDecimal(txtempdays.Text) + Convert.ToInt32(txtcl.Text))).ToString();
                                                    txtSBasic.Text = (Math.Round(Convert.ToDecimal(txtSBasic.Text), 0, MidpointRounding.AwayFromZero)).ToString();
                                                    //basic = (Convert.ToDecimal(slbAmt) * (Convert.ToDecimal(EmployeeDays) + Convert.ToInt32(cl))).ToString();
                                                    //basic = (Math.Round(Convert.ToDecimal(basic), 0, MidpointRounding.AwayFromZero)).ToString();
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        txtSBasic.Text = (Convert.ToDecimal(dt1.Rows[0]["Base"].ToString()) * (Convert.ToDecimal(txtempdays.Text) + Convert.ToInt32(txtcl.Text))).ToString();
                                        txtSBasic.Text = (Math.Round(Convert.ToDecimal(txtSBasic.Text), 0, MidpointRounding.AwayFromZero)).ToString();
                                    }                                  
                                    //txtSBasic.Text = (Convert.ToDecimal(dt1.Rows[0]["Base"].ToString()) * (Convert.ToDecimal(txtempdays.Text) + Convert.ToInt32(txtcl.Text))).ToString();
                                    //txtSBasic.Text = (Math.Round(Convert.ToDecimal(txtSBasic.Text), 0, MidpointRounding.AwayFromZero)).ToString();
                                    txtSAll1.Text = ((Convert.ToDecimal(txtempdays.Text) + Convert.ToDecimal(txtcl.Text)) * Convert.ToDecimal(dt1.Rows[0]["Alllowance1amt"].ToString())).ToString();
                                    txtSAll1.Text = (Math.Round(Convert.ToDecimal(txtSAll1.Text), 0, MidpointRounding.AwayFromZero)).ToString();
                                    //txtSAll2.Text = (Convert.ToDecimal(dt1.Rows[0]["Allowance2amt"].ToString()) * Convert.ToDecimal(txtempdays.Text)).ToString();
                                    txtSAll2.Text = ((Convert.ToDecimal(txtempdays.Text) + Convert.ToDecimal(txtcl.Text)) * Convert.ToDecimal(dt1.Rows[0]["Allowance2amt"].ToString())).ToString();
                                    txtSAll2.Text = (Math.Round(Convert.ToDecimal(txtSAll2.Text), 0, MidpointRounding.AwayFromZero)).ToString();
                                    //txtSded1.Text = (Convert.ToDecimal(dt1.Rows[0]["Deduction1"].ToString()) * Convert.ToDecimal(txtempdays.Text)).ToString();
                                    txtSded1.Text = ((Convert.ToDecimal(txtempdays.Text) + (Convert.ToDecimal(txtrest.Text.Trim()))) * Convert.ToDecimal(dt1.Rows[0]["Deduction1"].ToString())).ToString();
                                    txtSded1.Text = (Math.Round(Convert.ToDecimal(txtSded1.Text), 0, MidpointRounding.AwayFromZero)).ToString();
                                    //txtSded2.Text = (Convert.ToDecimal(dt1.Rows[0]["Deduction2"].ToString()) * Convert.ToDecimal(txtempdays.Text)).ToString();
                                    txtSded2.Text = ((Convert.ToDecimal(txtempdays.Text)) * Convert.ToDecimal(dt1.Rows[0]["Deduction2"].ToString())).ToString();
                                    txtSded2.Text = (Math.Round(Convert.ToDecimal(txtSded2.Text), 0, MidpointRounding.AwayFromZero)).ToString();
                                    //txtSLOP.Text = ((Convert.ToDecimal(txtSBasic.Text) / 30) * Convert.ToDecimal(txtlossofpay.Text)).ToString();
                                    txtSLOP.Text = "0.00";
                                    txtsalaryday.Text = Convert.ToDateTime(dt_val.Rows[0]["TransDate"].ToString()).ToShortDateString();
                                    if (dteligible.Rows[0]["EligiblePF"].ToString() == "1")
                                    {
                                        decimal PF_val = (Convert.ToDecimal(dt1.Rows[0]["PFS"].ToString()));
                                        txtSPF.Text = ((PF_val * Convert.ToDecimal(dtpf.Rows[0]["PF_per"].ToString())) / 100).ToString();
                                        txtSPF.Text = (Math.Round(Convert.ToDecimal(txtSPF.Text), 0, MidpointRounding.AwayFromZero)).ToString();
                                    }
                                    else
                                    {
                                        txtSPF.Text = "0";
                                    }
                                    string qry_Type = "Select Wagestype from Officialprofile where EmpNo = '" + ddlempCodeStaff.SelectedValue + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                                    cn.Open();
                                    SqlCommand cmd_verify2 = new SqlCommand(qry_Type, cn);
                                    WagesType = Convert.ToString(cmd_verify2.ExecuteScalar());
                                    cn.Close();
                                    if (WagesType == "1")
                                    {
                                        txtHalf.Text = (Convert.ToDecimal(halfNight) * Convert.ToDecimal(dtpf.Rows[0]["halfNight"].ToString())).ToString();
                                        txtFull.Text = (Convert.ToDecimal(FullNight) * Convert.ToDecimal(dtpf.Rows[0]["FullNight"].ToString())).ToString();
                                        txtThreeSided.Text = "0.00";
                                        if (Convert.ToDecimal(txtempdays.Text) >= 6)
                                        {
                                            txtDayIncentive.Text = (Convert.ToDecimal(txtempdays.Text) * Convert.ToDecimal(dtpf.Rows[0]["DayIncentive"].ToString())).ToString();
                                        }
                                        else
                                        {
                                            txtDayIncentive.Text = "0.00";
                                        }
                                        if (ddldepartment.SelectedItem.ToString().ToUpper() == "SPINNING")
                                        {
                                            txtSpinning.Text = (Convert.ToDecimal(txtempdays.Text) * Convert.ToDecimal(dtpf.Rows[0]["Spinning"].ToString())).ToString();
                                        }
                                        else
                                        {
                                            txtSpinning.Text = "0.00";
                                        }
                                    }
                                    else if (WagesType == "3")
                                    {
                                        txtThreeSided.Text = (Convert.ToDecimal(ThreeSided) * Convert.ToDecimal(dtpf.Rows[0]["ThreeSideAmt"].ToString())).ToString();
                                    }
                                    else
                                    {
                                        txtHalf.Text = "0.00";
                                        txtFull.Text = "0.00";
                                        txtSpinning.Text = "0.00";
                                        txtThreeSided.Text = "0.00";
                                        txtDayIncentive.Text = "0.00";
                                    }
                                    txtSAll3.Text = dt_val.Rows[0]["allowances3"].ToString();
                                    txtSAll4.Text = dt_val.Rows[0]["allowances4"].ToString();
                                    txtSAll5.Text = dt_val.Rows[0]["allowances5"].ToString();
                                    txtSded3.Text = dt_val.Rows[0]["Deduction3"].ToString();
                                    txtSded4.Text = dt_val.Rows[0]["Deduction4"].ToString();
                                    txtSded5.Text = dt_val.Rows[0]["Deduction5"].ToString();
                                    txtSAdvance.Text = dt_val.Rows[0]["Advance"].ToString();

                                }
                                
                            }
                            else
                            {
                                if (ddlcategory.SelectedValue == "1")
                                {
                                    AdvAmt = 0;
                                    ID = "";
                                    DataTable dt1 = new DataTable();
                                    dt1 = objdata.NewSlary_Load(SessionCcode, SessionLcode, ddlempCodeStaff.SelectedValue);

                                    DataTable dteligible = new DataTable();
                                    DataTable dtpf = new DataTable();
                                    dtpf = objdata.Load_pf_details(SessionCcode, SessionLcode);
                                    dteligible = objdata.EligibleESI_PF(SessionCcode, SessionLcode, ddlempCodeStaff.SelectedValue);
                                    txtStamp.Text ="0";
                                    txtunion.Text = "0";
                                    //if (dtpf.Rows[0]["Union_val"].ToString() == "1")
                                    //{
                                    //    txtunion.Text = dt1.Rows[0]["Unioncharges"].ToString();
                                    //}
                                    //else
                                    //{
                                    //    txtunion.Text = "0";
                                    //}

                                    if (dt1.Rows.Count > 0)
                                    {

                                        //string dt_ot1 = "";
                                        //dt_ot1 = objdata.OT_Salary(ddlempCodeStaff.SelectedValue, SessionCcode, SessionLcode, Mont);
                                        //if (dt_ot1.Trim() != "")
                                        //{
                                        //    txtOT.Text = dt_ot1.ToString();
                                        //}
                                        //else
                                        //{
                                        //    txtOT.Text = "0";
                                        //}
                                        pf = dt1.Rows[0]["PFS"].ToString();
                                        //txtSBasic.Text = (Convert.ToDecimal(dt1.Rows[0]["Base"].ToString())).ToString();
                                        txtSBasic.Text = (Convert.ToDecimal(dt1.Rows[0]["Base"].ToString())).ToString();
                                        if ((Convert.ToDecimal(txtempdays.Text.Trim()) + Convert.ToDecimal(txtcl.Text.Trim())) >= Convert.ToDecimal(txtwork.Text.Trim()))
                                        {
                                            txtSBasic.Text = (Convert.ToDecimal(txtSBasic.Text) / Convert.ToDecimal(txtwork.Text)).ToString();
                                            txtSBasic.Text = (Convert.ToDecimal(txtSBasic.Text) * (Convert.ToDecimal(txtwork.Text))).ToString();
                                        }
                                        else
                                        {
                                            txtSBasic.Text = (Convert.ToDecimal(txtSBasic.Text) / Convert.ToDecimal(txtwork.Text)).ToString();
                                            txtSBasic.Text = (Convert.ToDecimal(txtSBasic.Text) * (Convert.ToDecimal(txtcl.Text) + Convert.ToDecimal(txtempdays.Text))).ToString();
                                        }
                                        //txtSAll1.Text = (Convert.ToDecimal(dt1.Rows[0]["Alllowance1amt"].ToString()) * Convert.ToDecimal(txtempdays.Text)).ToString();
                                        txtSAll1.Text = ((Convert.ToDecimal(txtempdays.Text) + Convert.ToDecimal(txtcl.Text)) * Convert.ToDecimal(dt1.Rows[0]["Alllowance1amt"].ToString())).ToString();
                                        txtSAll1.Text = (Math.Round(Convert.ToDecimal(txtSAll1.Text), 0, MidpointRounding.AwayFromZero)).ToString();
                                        //txtSAll2.Text = (Convert.ToDecimal(dt1.Rows[0]["Allowance2amt"].ToString()) * Convert.ToDecimal(txtempdays.Text)).ToString();
                                        txtSAll2.Text = ((Convert.ToDecimal(txtempdays.Text) + Convert.ToDecimal(txtcl.Text)) * Convert.ToDecimal(dt1.Rows[0]["Allowance2amt"].ToString())).ToString();
                                        txtSAll2.Text = (Math.Round(Convert.ToDecimal(txtSAll2.Text), 0, MidpointRounding.AwayFromZero)).ToString();
                                        //txtSded1.Text = (Convert.ToDecimal(dt1.Rows[0]["Deduction1"].ToString()) * Convert.ToDecimal(txtempdays.Text)).ToString();
                                        txtSded1.Text = ((Convert.ToDecimal(txtempdays.Text) + (Convert.ToDecimal(txtrest.Text.Trim()))) * Convert.ToDecimal(dt1.Rows[0]["Deduction1"].ToString())).ToString();
                                        txtSded1.Text = (Math.Round(Convert.ToDecimal(txtSded1.Text), 0, MidpointRounding.AwayFromZero)).ToString();
                                        //txtSded2.Text = (Convert.ToDecimal(dt1.Rows[0]["Deduction2"].ToString()) * Convert.ToDecimal(txtempdays.Text)).ToString();
                                        txtSded2.Text = ((Convert.ToDecimal(txtempdays.Text)) * Convert.ToDecimal(dt1.Rows[0]["Deduction2"].ToString())).ToString();
                                        txtSded2.Text = (Math.Round(Convert.ToDecimal(txtSded2.Text), 0, MidpointRounding.AwayFromZero)).ToString();
                                        txtSLOP.Text = ((Convert.ToDecimal(txtSBasic.Text) / Convert.ToDecimal(txtwork.Text)) * Convert.ToDecimal(txtlossofpay.Text)).ToString();
                                        txtSLOP.Text = (Math.Round(Convert.ToDecimal(txtSLOP.Text), 0, MidpointRounding.AwayFromZero)).ToString();
                                        //txtSLOP.Text = (Math.Round(Convert.ToDecimal(txtSLOP.Text), 0, MidpointRounding.AwayFromZero)).ToString();
                                        txtSstamp.Text = dtpf.Rows[0]["StampCg"].ToString();
                                        decimal basicsalary = (Convert.ToDecimal(txtSBasic.Text) - Convert.ToDecimal(txtSLOP.Text));
                                        txtHalf.Text = "0.00";
                                        txtFull.Text = "0.00";
                                        txtSpinning.Text = "0.00";
                                        txtThreeSided.Text = "0.00";
                                        txtDayIncentive.Text = "0.00";
                                        if (dteligible.Rows[0]["EligiblePF"].ToString() == "1")
                                        {
                                            //if (dteligible.Rows[0]["PF_Type"].ToString() == "1")
                                            //{
                                                
                                            //    PF_salary = "1";
                                            //    if (Convert.ToDecimal(basicsalary) >= Convert.ToDecimal(dtpf.Rows[0]["StaffSalary"].ToString()))
                                            //    {
                                            //        txtSPF.Text = ((Convert.ToDecimal(dtpf.Rows[0]["StaffSalary"].ToString()) * Convert.ToDecimal(dtpf.Rows[0]["PF_per"].ToString())) / 100).ToString();
                                            //        txtSPF.Text = (Math.Round(Convert.ToDecimal(txtSPF.Text), 0, MidpointRounding.AwayFromZero)).ToString();
                                            //    }
                                            //    else
                                            //    {
                                            //        decimal PF_val = (Convert.ToDecimal(dt1.Rows[0]["PFS"].ToString()));
                                            //        txtSPF.Text = ((PF_val * Convert.ToDecimal(dtpf.Rows[0]["PF_per"].ToString())) / 100).ToString();
                                            //        txtSPF.Text = (Math.Round(Convert.ToDecimal(txtSPF.Text), 0, MidpointRounding.AwayFromZero)).ToString();
                                            //    }
                                            //}
                                            //else
                                            //{
                                            //    PF_salary = "0";
                                            //    decimal PF_val = (Convert.ToDecimal(dt1.Rows[0]["PFS"].ToString()));
                                            //    txtSPF.Text = ((PF_val * Convert.ToDecimal(dtpf.Rows[0]["PF_per"].ToString())) / 100).ToString();
                                            //    txtSPF.Text = (Math.Round(Convert.ToDecimal(txtSPF.Text), 0, MidpointRounding.AwayFromZero)).ToString();
                                            //}
                                            decimal PF_val = (Convert.ToDecimal(dt1.Rows[0]["PFS"].ToString()));
                                            txtSPF.Text = ((PF_val * Convert.ToDecimal(dtpf.Rows[0]["PF_per"].ToString())) / 100).ToString();
                                            txtSPF.Text = (Math.Round(Convert.ToDecimal(txtSPF.Text), 0, MidpointRounding.AwayFromZero)).ToString();
                                        }
                                        else
                                        {
                                            txtSPF.Text = "0";
                                        }
                                        if (dteligible.Rows[0]["ElgibleESI"].ToString() == "1")
                                        {
                                            decimal ESI_val = (Convert.ToDecimal(basicsalary));
                                            txtSesi.Text = ((ESI_val * Convert.ToDecimal(dtpf.Rows[0]["ESI_per"].ToString())) / 100).ToString();
                                            txtSesi.Text = (Math.Round(Convert.ToDecimal(txtSesi.Text), 2, MidpointRounding.ToEven)).ToString();
                                            string[] split_val = txtSesi.Text.Split('.');
                                            string Ist = split_val[0].ToString();
                                            string IInd = split_val[1].ToString();

                                            if (Convert.ToInt32(IInd) > 0)
                                            {
                                                txtSesi.Text = (Convert.ToInt32(Ist) + 1).ToString();
                                            }
                                            else
                                            {
                                                txtSesi.Text = Ist;
                                            }

                                        }
                                        else
                                        {
                                            txtSesi.Text = "0";
                                        }
                                        if (dteligible.Rows[0]["ElgibleESI"].ToString() == "1")
                                        {
                                            decimal ESI_val = (Convert.ToDecimal(txtSBasic.Text));
                                            txtSesi.Text = ((ESI_val * Convert.ToDecimal(dtpf.Rows[0]["ESI_per"].ToString())) / 100).ToString();
                                            txtSesi.Text = (Math.Round(Convert.ToDecimal(txtSesi.Text), 2, MidpointRounding.ToEven)).ToString();
                                            string[] split_val = txtSesi.Text.Split('.');
                                            string Ist = split_val[0].ToString();
                                            string IInd = split_val[1].ToString();

                                            if (Convert.ToInt32(IInd) > 0)
                                            {
                                                txtSesi.Text = (Convert.ToInt32(Ist) + 1).ToString();
                                            }
                                            else
                                            {
                                                txtSesi.Text = Ist;
                                            }

                                        }
                                        else
                                        {
                                            txtSesi.Text = "0";
                                        }
                                        txtStamp.Text = dtpf.Rows[0]["StampCg"].ToString();
                                        string dt_ot1 = "";
                                        dt_ot1 = objdata.OT_Salary(ddlempCodeStaff.SelectedValue, SessionCcode, SessionLcode, Mont, MyDate1, MyDate2);
                                        if (dt_ot1.Trim() != "")
                                        {
                                            txtSOT.Text = dt_ot1.ToString();
                                        }
                                        else
                                        {
                                            txtSOT.Text = "0";
                                        }
                                        
                                        //txtStamp.Text = dtpf.Rows[0]["StampCg"].ToString();
                                    }
                                }
                                else
                                {
                                    AdvAmt = 0;
                                    ID = "";
                                    DataTable dt1 = new DataTable();
                                    dt1 = objdata.NewSlary_Load(SessionCcode, SessionLcode, ddlempCodeStaff.SelectedValue);

                                    DataTable dteligible = new DataTable();
                                    DataTable dtpf = new DataTable();
                                    dtpf = objdata.Load_pf_details(SessionCcode, SessionLcode);
                                    dteligible = objdata.EligibleESI_PF(SessionCcode, SessionLcode, ddlempCodeStaff.SelectedValue);
                                    txtStamp.Text = dtpf.Rows[0]["StampCg"].ToString();
                                    if (dtpf.Rows[0]["Union_val"].ToString() == "1")
                                    {
                                        txtunion.Text = dt1.Rows[0]["Unioncharges"].ToString();
                                    }
                                    else
                                    {
                                        txtunion.Text = "0";
                                    }

                                    if (dt1.Rows.Count > 0)
                                    {


                                        DataTable dt_con = new DataTable();
                                        DataTable dt_val1 = new DataTable();
                                        dt_con = objdata.Contract_SalaryCal(ddlempCodeStaff.SelectedValue, SessionCcode, SessionLcode);
                                        if (dt_con.Rows.Count > 0)
                                        {
                                            dt_val1 = objdata.Contract_Eligible(dt_con.Rows[0]["ContractName"].ToString());
                                            if (Convert.ToInt32(dt_con.Rows[0]["BasicMonths"].ToString()) < Convert.ToInt32(dt_val1.Rows[0]["NotEMonths"].ToString()))
                                            {
                                                //basic = (Convert.ToDecimal(dt1.Rows[0]["Base"].ToString()) * (Convert.ToDecimal(EmployeeDays) + Convert.ToInt32(cl))).ToString();
                                                //basic = (Math.Round(Convert.ToDecimal(basic), 0, MidpointRounding.AwayFromZero)).ToString();
                                                txtSBasic.Text = (Convert.ToDecimal(dt1.Rows[0]["Base"].ToString()) * (Convert.ToDecimal(txtempdays.Text) + Convert.ToInt32(txtcl.Text))).ToString();
                                                txtSBasic.Text = (Math.Round(Convert.ToDecimal(txtSBasic.Text), 0, MidpointRounding.AwayFromZero)).ToString();
                                            }
                                            else
                                            {
                                                if (dt_con.Rows[0]["MonthCount"].ToString() == "0")
                                                {
                                                    string ID_val = "1";
                                                    string slbAmt = objdata.Slb_Salary(ID_val, dt_con.Rows[0]["ContractName"].ToString());
                                                    if (slbAmt.Trim() == "")
                                                    {
                                                        slbAmt = "0";
                                                    }
                                                    if ((Convert.ToDecimal(txtempdays.Text) + Convert.ToDecimal(txtcl.Text)) >= (Convert.ToDecimal(dt_con.Rows[0]["FixedDays"].ToString())))
                                                    {
                                                        //basic = (Convert.ToDecimal(slbAmt) * (Convert.ToDecimal(EmployeeDays) + Convert.ToInt32(cl))).ToString();
                                                        //basic = (Math.Round(Convert.ToDecimal(basic), 0, MidpointRounding.AwayFromZero)).ToString();
                                                        txtSBasic.Text = (Convert.ToDecimal(slbAmt) * (Convert.ToDecimal(txtempdays.Text) + Convert.ToInt32(txtcl.Text))).ToString();
                                                        txtSBasic.Text = (Math.Round(Convert.ToDecimal(txtSBasic.Text), 0, MidpointRounding.AwayFromZero)).ToString();
                                                    }
                                                    else
                                                    {
                                                        //basic = (Convert.ToDecimal(dt1.Rows[0]["Base"].ToString()) * (Convert.ToDecimal(EmployeeDays) + Convert.ToInt32(cl))).ToString();
                                                        //basic = (Math.Round(Convert.ToDecimal(basic), 0, MidpointRounding.AwayFromZero)).ToString();
                                                        txtSBasic.Text = (Convert.ToDecimal(dt1.Rows[0]["Base"].ToString()) * (Convert.ToDecimal(txtempdays.Text) + Convert.ToInt32(txtcl.Text))).ToString();
                                                        txtSBasic.Text = (Math.Round(Convert.ToDecimal(txtSBasic.Text), 0, MidpointRounding.AwayFromZero)).ToString();

                                                    }
                                                }
                                                else
                                                {
                                                    if ((Convert.ToDecimal(txtempdays.Text) + Convert.ToDecimal(txtcl.Text)) >= (Convert.ToDecimal(dt_con.Rows[0]["FixedDays"].ToString())))
                                                    {
                                                        string ID_val = dt_con.Rows[0]["MonthCount"].ToString();
                                                        ID_val = (Convert.ToInt32(ID_val) + 1).ToString();
                                                        string slbAmt = objdata.Slb_Salary(ID_val, dt_con.Rows[0]["ContractName"].ToString());
                                                        if (slbAmt.Trim() == "")
                                                        {
                                                            slbAmt = "0";
                                                        }
                                                        //basic = (Convert.ToDecimal(slbAmt) * (Convert.ToDecimal(EmployeeDays) + Convert.ToInt32(cl))).ToString();
                                                        //basic = (Math.Round(Convert.ToDecimal(basic), 0, MidpointRounding.AwayFromZero)).ToString();
                                                        txtSBasic.Text = (Convert.ToDecimal(slbAmt) * (Convert.ToDecimal(txtempdays.Text) + Convert.ToInt32(txtcl.Text))).ToString();
                                                        txtSBasic.Text = (Math.Round(Convert.ToDecimal(txtSBasic.Text), 0, MidpointRounding.AwayFromZero)).ToString();
                                                    }
                                                    else
                                                    {
                                                        string ID_val = dt_con.Rows[0]["MonthCount"].ToString();
                                                        //ID_val = (Convert.ToInt32(ID_val) + 1);
                                                        string slbAmt = objdata.Slb_Salary(ID_val, dt_con.Rows[0]["ContractName"].ToString());
                                                        if (slbAmt.Trim() == "")
                                                        {
                                                            slbAmt = "0";
                                                        }
                                                        txtSBasic.Text = (Convert.ToDecimal(slbAmt) * (Convert.ToDecimal(txtempdays.Text) + Convert.ToInt32(txtcl.Text))).ToString();
                                                        txtSBasic.Text = (Math.Round(Convert.ToDecimal(txtSBasic.Text), 0, MidpointRounding.AwayFromZero)).ToString();
                                                        //basic = (Convert.ToDecimal(slbAmt) * (Convert.ToDecimal(EmployeeDays) + Convert.ToInt32(cl))).ToString();
                                                        //basic = (Math.Round(Convert.ToDecimal(basic), 0, MidpointRounding.AwayFromZero)).ToString();
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            txtSBasic.Text = (Convert.ToDecimal(dt1.Rows[0]["Base"].ToString()) * (Convert.ToDecimal(txtempdays.Text) + Convert.ToInt32(txtcl.Text))).ToString();
                                            txtSBasic.Text = (Math.Round(Convert.ToDecimal(txtSBasic.Text), 0, MidpointRounding.AwayFromZero)).ToString();
                                        }                                 
                                        //txtSBasic.Text = (Convert.ToDecimal(dt1.Rows[0]["Base"].ToString()) * (Convert.ToDecimal(txtempdays.Text) + Convert.ToInt32(txtcl.Text))).ToString();
                                        //txtSBasic.Text = (Math.Round(Convert.ToDecimal(txtSBasic.Text), 0, MidpointRounding.AwayFromZero)).ToString();
                                        txtSAll1.Text = ((Convert.ToDecimal(txtempdays.Text) + Convert.ToDecimal(txtcl.Text)) * Convert.ToDecimal(dt1.Rows[0]["Alllowance1amt"].ToString())).ToString();
                                        txtSAll1.Text = (Math.Round(Convert.ToDecimal(txtSAll1.Text), 0, MidpointRounding.AwayFromZero)).ToString();
                                        //txtSAll2.Text = (Convert.ToDecimal(dt1.Rows[0]["Allowance2amt"].ToString()) * Convert.ToDecimal(txtempdays.Text)).ToString();
                                        txtSAll2.Text = ((Convert.ToDecimal(txtempdays.Text) + Convert.ToDecimal(txtcl.Text)) * Convert.ToDecimal(dt1.Rows[0]["Allowance2amt"].ToString())).ToString();
                                        txtSAll2.Text = (Math.Round(Convert.ToDecimal(txtSAll2.Text), 0, MidpointRounding.AwayFromZero)).ToString();
                                        //txtSded1.Text = (Convert.ToDecimal(dt1.Rows[0]["Deduction1"].ToString()) * Convert.ToDecimal(txtempdays.Text)).ToString();
                                        txtSded1.Text = ((Convert.ToDecimal(txtempdays.Text) + (Convert.ToDecimal(txtrest.Text.Trim()))) * Convert.ToDecimal(dt1.Rows[0]["Deduction1"].ToString())).ToString();
                                        txtSded1.Text = (Math.Round(Convert.ToDecimal(txtSded1.Text), 0, MidpointRounding.AwayFromZero)).ToString();
                                        //txtSded2.Text = (Convert.ToDecimal(dt1.Rows[0]["Deduction2"].ToString()) * Convert.ToDecimal(txtempdays.Text)).ToString();
                                        txtSded2.Text = ((Convert.ToDecimal(txtempdays.Text)) * Convert.ToDecimal(dt1.Rows[0]["Deduction2"].ToString())).ToString();
                                        txtSded2.Text = (Math.Round(Convert.ToDecimal(txtSded2.Text), 0, MidpointRounding.AwayFromZero)).ToString();
                                        //txtSLOP.Text = ((Convert.ToDecimal(txtSBasic.Text) / 30) * Convert.ToDecimal(txtlossofpay.Text)).ToString();
                                        txtSLOP.Text = "0.00";
                                        //txtSLOP.Text = (Math.Round(Convert.ToDecimal(txtSLOP.Text), 0, MidpointRounding.AwayFromZero)).ToString();
                                        if (dteligible.Rows[0]["EligiblePF"].ToString() == "1")
                                        {
                                            //if (dteligible.Rows[0]["PF_Type"].ToString() == "1")
                                            //{
                                            //    PF_salary = "1";
                                            //    if (Convert.ToDecimal(txtSBasic.Text) >= Convert.ToDecimal(dtpf.Rows[0]["StaffSalary"].ToString()))
                                            //    {
                                            //        txtSPF.Text = ((Convert.ToDecimal(dtpf.Rows[0]["StaffSalary"].ToString()) * Convert.ToDecimal(dtpf.Rows[0]["PF_per"].ToString())) / 100).ToString();
                                            //        txtSPF.Text = (Math.Round(Convert.ToDecimal(txtSPF.Text), 0, MidpointRounding.AwayFromZero)).ToString();
                                            //    }
                                            //    else
                                            //    {
                                            //        decimal PF_val = (Convert.ToDecimal(txtSBasic.Text));
                                            //        txtSPF.Text = ((PF_val * Convert.ToDecimal(dtpf.Rows[0]["PF_per"].ToString())) / 100).ToString();
                                            //        txtSPF.Text = (Math.Round(Convert.ToDecimal(txtSPF.Text), 0, MidpointRounding.AwayFromZero)).ToString();
                                            //    }
                                            //}
                                            //else
                                            //{
                                            //    PF_salary = "0";
                                            //    decimal PF_val = (Convert.ToDecimal(txtSBasic.Text));
                                            //    txtSPF.Text = ((PF_val * Convert.ToDecimal(dtpf.Rows[0]["PF_per"].ToString())) / 100).ToString();
                                            //    txtSPF.Text = (Math.Round(Convert.ToDecimal(txtSPF.Text), 0, MidpointRounding.AwayFromZero)).ToString();
                                            //}
                                            PF_salary = "0";
                                            decimal PF_val = (Convert.ToDecimal(dt1.Rows[0]["PFS"].ToString()));
                                            txtSPF.Text = ((PF_val * Convert.ToDecimal(dtpf.Rows[0]["PF_per"].ToString())) / 100).ToString();
                                            txtSPF.Text = (Math.Round(Convert.ToDecimal(txtSPF.Text), 0, MidpointRounding.AwayFromZero)).ToString();
                                        }
                                        else
                                        {
                                            txtSPF.Text = "0";
                                        }
                                        string qry_Type = "Select Wagestype from Officialprofile where EmpNo = '" + ddlempCodeStaff.SelectedValue + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                                        cn.Open();
                                        SqlCommand cmd_verify2 = new SqlCommand(qry_Type, cn);
                                        WagesType = Convert.ToString(cmd_verify2.ExecuteScalar());
                                        cn.Close();
                                        if (WagesType == "1")
                                        {
                                            txtHalf.Text = (Convert.ToDecimal(halfNight) * Convert.ToDecimal(dtpf.Rows[0]["halfNight"].ToString())).ToString();
                                            txtFull.Text = (Convert.ToDecimal(FullNight) * Convert.ToDecimal(dtpf.Rows[0]["FullNight"].ToString())).ToString();
                                            txtThreeSided.Text = "0.00";
                                            if (Convert.ToDecimal(txtempdays.Text) >= 6)
                                            {
                                                txtDayIncentive.Text = (Convert.ToDecimal(txtempdays.Text) * Convert.ToDecimal(dtpf.Rows[0]["DayIncentive"].ToString())).ToString();
                                            }
                                            else
                                            {
                                                txtDayIncentive.Text = "0.00";
                                            }
                                            if (ddldepartment.Text.ToUpper() == "SPINNING")
                                            {
                                                txtSpinning.Text = (Convert.ToDecimal(txtempdays.Text) * Convert.ToDecimal(dtpf.Rows[0]["Spinning"].ToString())).ToString();
                                            }
                                            else
                                            {
                                                txtSpinning.Text = "0.00";
                                            }
                                        }
                                        else if (WagesType == "3")
                                        {
                                            txtThreeSided.Text = (Convert.ToDecimal(ThreeSided) * Convert.ToDecimal(dtpf.Rows[0]["ThreeSideAmt"].ToString())).ToString();
                                        }
                                        else
                                        {
                                            txtHalf.Text = "0.00";
                                            txtFull.Text = "0.00";
                                            txtSpinning.Text = "0.00";
                                            txtThreeSided.Text = "0.00";
                                            txtDayIncentive.Text = "0.00";
                                        }
                                        if (dteligible.Rows[0]["ElgibleESI"].ToString() == "1")
                                        {
                                            decimal ESI_val = (Convert.ToDecimal(txtSBasic.Text));
                                            txtSesi.Text = ((ESI_val * Convert.ToDecimal(dtpf.Rows[0]["ESI_per"].ToString())) / 100).ToString();
                                            txtSesi.Text = (Math.Round(Convert.ToDecimal(txtSesi.Text), 2, MidpointRounding.ToEven)).ToString();
                                            string[] split_val = txtSesi.Text.Split('.');
                                            string Ist = split_val[0].ToString();
                                            string IInd = split_val[1].ToString();

                                            if (Convert.ToInt32(IInd) > 0)
                                            {
                                                txtSesi.Text = (Convert.ToInt32(Ist) + 1).ToString();
                                            }
                                            else
                                            {
                                                txtSesi.Text = Ist;
                                            }

                                        }
                                        else
                                        {
                                            txtSesi.Text = "0";
                                        }
                                        txtStamp.Text = dtpf.Rows[0]["StampCg"].ToString();
                                        string dt_ot1 = "";
                                        dt_ot1 = objdata.OT_Salary(ddlempCodeStaff.SelectedValue, SessionCcode, SessionLcode, Mont, MyDate1, MyDate2);
                                        if (dt_ot1.Trim() != "")
                                        {
                                            txtSOT.Text = dt_ot1.ToString();
                                        }
                                        else
                                        {
                                            txtSOT.Text = "0";
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
        }
        catch (Exception ex)
        {
        }

    }
    protected void btncontract_Click(object sender, EventArgs e)
    {
        Response.Redirect("ContractRPT.aspx");
    }
    protected void ddToken_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            bool ErrFlag = false;
            string temp_exist = "";
            temp_exist = txtexistingNo.Text;
            string Cate = "";
            clr();
            txtexistingNo.Text = temp_exist;
            if (ddlcategory.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Category Staff or Labour');", true);

                ErrFlag = true;
            }
            else if (rbsalary.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Salary wages');", true);

                ErrFlag = true;
            }
            else if (ddldepartment.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select Department');", true);

                ErrFlag = true;
            }
            //else if (txtexistingNo.Text == "")
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Existing Number');", true);

            //    ErrFlag = true;
            //}
            if (!ErrFlag)
            {
                if (ddlcategory.SelectedValue == "1")
                {
                    Cate = "S";
                }
                else if (ddlcategory.SelectedValue == "2")
                {
                    Cate = "L";
                }

                DataTable dtempcode = new DataTable();
                dtempcode = objdata.SalaryMasterLabour_ForSearchingExisiting_New(ddldepartment.SelectedValue, ddToken.SelectedValue, Cate, SessionCcode, SessionLcode, SessionAdmin, rbsalary.SelectedValue);
                if (dtempcode.Rows.Count > 0)
                {
                    EmpType = dtempcode.Rows[0]["EmployeeType"].ToString();

                    if (EmpType == "1")
                    {
                        PanelStaff.Visible = true;
                        PanelWorker.Visible = false;
                        PanelPermenant.Visible = false;

                    }
                    //else if (EmpType == "2")
                    //{
                    //    PanelStaff.Visible = false;
                    //    PanelWorker.Visible = false;
                    //    PanelPermenant.Visible = true;
                    //}
                    //else if (EmpType == "3")
                    //{
                    //    PanelStaff.Visible = false;
                    //    PanelWorker.Visible = true;
                    //    PanelPermenant.Visible = false;
                    //}
                    //else if (EmpType == "4")
                    //{
                    //    PanelStaff.Visible = false;
                    //    PanelWorker.Visible = false;
                    //    PanelPermenant.Visible = true;
                    //}
                    else
                    {
                        PanelStaff.Visible = true;
                        PanelWorker.Visible = false;
                        PanelPermenant.Visible = false;
                    }
                    if (dtempcode.Rows.Count > 0)
                    {
                        ddlempCodeStaff.DataSource = dtempcode;
                        ddlempCodeStaff.DataTextField = "EmpNo";
                        ddlempCodeStaff.DataValueField = "EmpNo";
                        ddlempCodeStaff.DataBind();
                        //ddlempCodeStaff.SelectedItem.Text = dtempcode.Rows[0]["EmpNo"].ToString();
                        //ddlempCodeStaff.DataBind();
                        ddlempNameStaff.DataSource = dtempcode;
                        ddlempNameStaff.DataTextField = "EmpName";
                        ddlempNameStaff.DataValueField = "EmpNo";
                        ddlempNameStaff.DataBind();
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Employee not Found');", true);

                        ErrFlag = true;
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Employee not Found');", true);

                    ErrFlag = true;
                }
            }
        }
        catch (Exception ex)
        {
        }
    }
    protected void ddlMonths_SelectedIndexChanged(object sender, EventArgs e)
    {
        //string Days = "";
        //string Year = "";
        //string Mon = "";
        //Year = (ddlFinaYear.SelectedValue);
        //Year = (Convert.ToInt32(Year) + 1).ToString();
        //if (ddlMonths.SelectedValue == "January")
        //{
        //    Mon = "01";
        //    Year = (Convert.ToInt32(Year) + 1).ToString();
        //}
        //else if (ddlMonths.SelectedValue == "February")
        //{
        //    Mon = "02";
        //    Year = (Convert.ToInt32(Year) + 1).ToString();
        //}
        //else if (ddlMonths.SelectedValue == "march")
        //{
        //    Mon = "03";
        //    Year = (Convert.ToInt32(Year) + 1).ToString();
        //}
        //else if (ddlMonths.SelectedValue == "April")
        //{
        //    Mon = "04";
        //    Year = (ddlFinaYear.SelectedValue);
        //}
        //else if (ddlMonths.SelectedValue == "May")
        //{
        //    Mon = "05";
        //    Year = (ddlFinaYear.SelectedValue);
        //}
        //else if (ddlMonths.SelectedValue == "June")
        //{
        //    Mon = "06";
        //    Year = (ddlFinaYear.SelectedValue);
        //}
        //else if (ddlMonths.SelectedValue == "July")
        //{
        //    Mon = "07";
        //    Year = (ddlFinaYear.SelectedValue);
        //}
        //else if (ddlMonths.SelectedValue == "August")
        //{
        //    Mon = "08";
        //    Year = (ddlFinaYear.SelectedValue);
        //}
        //else if (ddlMonths.SelectedValue == "September")
        //{
        //    Mon = "09";
        //    Year = (ddlFinaYear.SelectedValue);
        //}
        //else if (ddlMonths.SelectedValue == "October")
        //{
        //    Mon = "10";
        //    Year = (ddlFinaYear.SelectedValue);
        //}
        //else if (ddlMonths.SelectedValue == "November")
        //{
        //    Mon = "11";
        //    Year = (ddlFinaYear.SelectedValue);
        //}
        //else if (ddlMonths.SelectedValue == "December")
        //{
        //    Mon = "12";
        //    Year = (ddlFinaYear.SelectedValue);
        //}
        //if ((ddlMonths.SelectedValue == "January") || (ddlMonths.SelectedValue == "March") || (ddlMonths.SelectedValue == "May") || (ddlMonths.SelectedValue == "July") || (ddlMonths.SelectedValue == "August") || (ddlMonths.SelectedValue == "October") || (ddlMonths.SelectedValue == "December"))
        //{
        //    Days = "31";
        //}
        //else if ((ddlMonths.SelectedValue == "April") || (ddlMonths.SelectedValue == "June") || (ddlMonths.SelectedValue == "September") || (ddlMonths.SelectedValue == "November"))
        //{
        //    Days = "30";
        //}
        //else if (ddlMonths.SelectedValue == "February")
        //{
        //    Year = (Convert.ToInt32(Year) + 1).ToString();
        //    if ((Convert.ToInt32(Year) / 4) == 0)
        //    {
        //        Days = "29";
        //    }
        //    else
        //    {
        //        Days = "28";
        //    }
           
        //}
        //txtfrom.Text = ("01-" + Mon + "-" + Year).ToString();
        //txtTo.Text = (Days + "-" + Mon + "-" + Year).ToString();
    }
    protected void btnSal_Click(object sender, EventArgs e)
    {
        Response.Redirect("FrmDeductionDownload.aspx");
    }
}
