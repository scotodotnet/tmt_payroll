﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Payroll;
using Payroll.Configuration;
using Payroll.Data;

using Altius.BusinessAccessLayer.BALDataAccess;


public partial class OfficalProfileDetails_1 : System.Web.UI.Page
{
    protected System.Resources.ResourceManager rm;
    protected string PayrollRegisterContentMeta, pageTitle;
    BALDataAccess objdata = new BALDataAccess();
    OfficialprofileClass objOff = new OfficialprofileClass();
    EmployeeClass objEmp = new EmployeeClass();
    System.Globalization.CultureInfo culterInfo = new System.Globalization.CultureInfo("en-GB");
    // static string ExisitingNo = "";
    static string UserID = "", SesRoleCode = "", SessionAdmin = "";
    string SessionCcode;
    string SessionLcode;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        UserID = Session["UserId"].ToString();
        SesRoleCode = Session["RoleCode"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        lblComany.Text = SessionCcode + " - " + SessionLcode;
        if (!IsPostBack)
        {
            OfficialProfileDisplay();
            DropdownProbationPeriod();
            DropDownInsurance();
            //Department();
            DropDwonCategory();
            MstBankDropDown();
            DropDownContract();
            int currentYear = Utility.GetFinancialYear;
            for (int i = 0; i < 10; i++)
            {
                ddlFinancialPeriod.Items.Add(new ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                //  ddlShowYear.Items.Add(new ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                currentYear = currentYear - 1;
            }

        }

        lblusername.Text = Session["Usernmdisplay"].ToString();
    }
    public void DropDwonCategory()
    {
        DataTable dtcate = new DataTable();
        dtcate = objdata.DropDownCategory();
        ddlcategory.DataSource = dtcate;
        ddlcategory.DataTextField = "CategoryName";
        ddlcategory.DataValueField = "CategoryCd";
        ddlcategory.DataBind();
    }
//    public void Department()
//    {
//        DataTable dtDip = new DataTable();
//        dtDip = objdata.DropDownDepartment();
//        ddldepartment.DataSource = dtDip;
//        ddldepartment.DataTextField = "DepartmentNm";
//        ddldepartment.DataValueField = "DepartmentCd";
//        ddldepartment.DataBind();
//    }
    public void DropdownProbationPeriod()
    {
        DataTable dtProbation = new DataTable();
        dtProbation = objdata.DropDownProbationPriod();
        ddlprobation.DataSource = dtProbation;
        ddlprobation.DataTextField = "ProbationMonth";
        ddlprobation.DataValueField = "ProbationCd";
        ddlprobation.DataBind();
        
    }
    public void DropDownContract()
    {
        DataTable dt = new DataTable();
        dt = objdata.DropDown_contract();
        ddlcontract.DataSource = dt;
        ddlcontract.DataTextField = "ContractName";
        ddlcontract.DataValueField = "ContractCode";
        ddlcontract.DataBind();
    }
    public void DropDownInsurance()
    {
        DataTable dtIns = new DataTable();
        dtIns = objdata.DropDownInsurance();
        ddlInsurance.DataSource = dtIns;
        ddlInsurance.DataTextField = "InsuranceCmpNm";
        ddlInsurance.DataValueField = "Insurancecmpcd";
        ddlInsurance.DataBind();
    }
    public void MstBankDropDown()
    {

        DataTable dt1 = new DataTable();
        dt1 = objdata.MstBankDropDown();
        ddlbankname.DataSource = dt1;
        ddlbankname.DataTextField = "BankName";
        ddlbankname.DataValueField = "Bankcd";
        ddlbankname.DataBind();
    }

    public void OfficialProfileDisplay()
    {
        if (SessionAdmin == "1")
        {
            if (ddlcategory.SelectedValue == "1")
            {
                string Staff = "S";
                DataTable dtOPP = new DataTable();
                dtOPP = objdata.OfficialProfileGridView(Staff, ddldepartment.SelectedValue, SessionCcode, SessionLcode);
                GvOfficialProfile.DataSource = dtOPP;
                GvOfficialProfile.DataBind();
                PanelLabour.Visible = false;
            }
            else if (ddlcategory.SelectedValue == "2")
            {
                string Staff = "L";
                DataTable dtOPP = new DataTable();
                dtOPP = objdata.OfficialProfileGridView(Staff, ddldepartment.SelectedValue, SessionCcode, SessionLcode);
                GvOtherUsers.DataSource = dtOPP;
                GvOtherUsers.DataBind();
                PanelLabour.Visible = true;
            }
        }
        else if (SessionAdmin == "2")
        {
            if (ddlcategory.SelectedValue == "1")
            {
                string Staff = "S";
                DataTable dtOPP = new DataTable();
                dtOPP = objdata.OfficialProfileGridView(Staff, ddldepartment.SelectedValue, SessionCcode, SessionLcode);
                GvOfficialProfile.DataSource = dtOPP;
                GvOfficialProfile.DataBind();
                PanelLabour.Visible = false;
            }
            else if (ddlcategory.SelectedValue == "2")
            {
                string Staff = "L";
                DataTable dtOPP = new DataTable();
                dtOPP = objdata.OfficialProfileGridView(Staff, ddldepartment.SelectedValue, SessionCcode, SessionLcode);
                GvOtherUsers.DataSource = dtOPP;
                GvOtherUsers.DataBind();
                PanelLabour.Visible = true;
            }
        }
    }
    public void OfficialProfileDisplay_admin()
    {
        if (ddlcategory.SelectedValue == "1")
        {
            string Staff = "S";
            DataTable dtOPP = new DataTable();
            dtOPP = objdata.OfficialProfileGridViewForAdmin(Staff, ddldepartment.SelectedValue, SessionCcode, SessionLcode);
            GvOtherUsers.DataSource = dtOPP;
            GvOtherUsers.DataBind();
            PanelLabour.Visible = false;
        }
        else if (ddlcategory.SelectedValue == "2")
        {
            string Staff = "L";
            DataTable dtOPP = new DataTable();
            dtOPP = objdata.OfficialProfileGridViewForAdmin(Staff, ddldepartment.SelectedValue, SessionCcode, SessionLcode);
            GvOtherUsers.DataSource = dtOPP;
            GvOtherUsers.DataBind();
            PanelLabour.Visible = true;
        }
    }
    protected void PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GvOfficialProfile.PageIndex = e.NewPageIndex;
        OfficialProfileDisplay();
    }

    public void Clear()
    {
        panelError.Visible = false;
        txtdobjoin.Text = "";
        ddlInsurance.SelectedValue = "0";
        txtconfirmationdate.Text = "";
        txtpannumber.Text = "";
        txtesic.Text = "";
        txtpfnumber.Text = "";
        ddlprobation.SelectedValue = "0";
        txtinsuranceno.Text = "";
        txtempno.Text = "";
        txtauthoritynm.Text = "";
        lblempname1.Text = "";
        txtlabourtype.Text = "";
        txtbasicSalary.Text = "";
        // txtovertime.Text = "";
        // rbtnmodeofpay.SelectedValue="0";
        rbtnProfie.Enabled = true;
        ddlprobation.Enabled = true;
        ddlcontract.Enabled = false;
        txtbranch.Text = "";
        txtbankacno.Text = "";
        ddlbankname.SelectedValue = "0";
        //rbtnsalarythroug.SelectedValue = "";
        txtconfirmationdate.Enabled = false;
        rbPF.SelectedIndex = -1;
        rbtncontract.SelectedIndex = -1;
        rbtnesi.SelectedIndex = -1;
        rbtnotherwages.SelectedIndex = -1;
        rbtnovertime.SelectedIndex = -1;
        rbtnProfie.SelectedIndex = -1;
        rbtnsalarythroug.SelectedIndex = -1;
        txtUAN.Text = "";



    }

    #region Web Form Designer generated code
    override protected void OnInit(EventArgs e)
    {

        rm = (System.Resources.ResourceManager)Application["rm"];
        Utility.SetThreadCulture();
        if (((SSSCultureInfo)System.Threading.Thread.CurrentThread.CurrentCulture).Encoding != null)
            Response.ContentEncoding = System.Text.Encoding.GetEncoding(((SSSCultureInfo)System.Threading.Thread.CurrentThread.CurrentCulture).Encoding);
        PayrollRegisterContentMeta = "text/html; charset=" + ((SSSCultureInfo)System.Threading.Thread.CurrentThread.CurrentCulture).Encoding;
        if (Application[Request.PhysicalPath] == null)
            Application.Add(Request.PhysicalPath, Response.ContentEncoding.WebName);
        InitializeComponent();
        this.Load += new System.EventHandler(this.Page_Load);
        // this.PreRender += new System.EventHandler(this.Page_PreRender);
        base.OnInit(e);
        if (!DBUtility.AuthorizeUser(new string[] { "1", "2", "6" }))
            ////string UId=Session["UserId"].ToString();
            ////if(UId !=null
            //if (Session["UserId"] == null)
            Response.Redirect(Settings.AccessDeniedPage);
    }


    private void InitializeComponent()
    {

    }
    #endregion


    protected void ddlcategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        string Stafflabour;
        Clear();
        DataTable dempty = new DataTable();
        GvOfficialProfile.DataSource = dempty;
        GvOfficialProfile.DataBind();
        GvOtherUsers.DataSource = dempty;
        GvOtherUsers.DataBind();
        if (ddlcategory.SelectedValue == "1")
        {
            Stafflabour = "S";
        }
        else if (ddlcategory.SelectedValue == "2")
        {
            Stafflabour = "L";
        }
        else
        {
            Stafflabour = "0";
        }
        DataTable dtDip = new DataTable();
        dtDip = objdata.DropDownDepartment_category(Stafflabour);
        if (dtDip.Rows.Count > 1)
        {
            ddldepartment.DataSource = dtDip;
            ddldepartment.DataTextField = "DepartmentNm";
            ddldepartment.DataValueField = "DepartmentCd";
            ddldepartment.DataBind();
        }
        else
        {
            DataTable dt = new DataTable();
            ddldepartment.DataSource = dt;
            ddldepartment.DataBind();
        }
        panelError.Visible = false;
    }
    protected void ddldepartment_SelectedIndexChanged(object sender, EventArgs e)
    {
        Clear();
        DataTable dempty = new DataTable();
        GvOfficialProfile.DataSource = dempty;
        GvOfficialProfile.DataBind();
        GvOtherUsers.DataSource = dempty;
        GvOtherUsers.DataBind();
        panelError.Visible = false;
    }
    protected void btnclick_Click(object sender, EventArgs e)
    {
         bool ErrFlag = false;
         //panelError.Visible = true;
        if (ddlcategory.SelectedValue == "0")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select Category Staff or Labour');", true);
            //System.Windows.Forms.MessageBox.Show("Select the Category", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            //lblError.Text = "Select the Category";
            ErrFlag = true;
        }
        else if (ddldepartment.SelectedValue == "0")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select Department');", true);
            //System.Windows.Forms.MessageBox.Show("Select Department", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            //lblError.Text = "Select the Department";
            ErrFlag = true;
        }
        if (!ErrFlag)
        {
            if (ddlcategory.SelectedValue == "1")
            {
                panelError.Visible = false;
                if (SessionAdmin == "1")
                {
                    string Staff = "S";
                    DataTable dtOPP = new DataTable();
                    dtOPP = objdata.OfficialProfileGridViewForAdmin(Staff, ddldepartment.SelectedValue, SessionCcode, SessionLcode);
                    GvOfficialProfile.DataSource = dtOPP;
                    GvOfficialProfile.DataBind();
                    PanelLabour.Visible = false;
                    PanelLabour.Visible = false;
                    PanelOthers.Visible = false;
                    panelContract.Visible = false;
                    if (dtOPP.Rows.Count > 0)
                    {
                        panelgrid.Visible = true;
                        PanelOtherUserForGrid.Visible = false;
                    }

                }
                else if (SessionAdmin == "2")
                {
                    string Staff = "S";
                    DataTable dtOPP = new DataTable();
                    dtOPP = objdata.OfficialProfileGridView(Staff, ddldepartment.SelectedValue, SessionCcode, SessionLcode);
                    GvOtherUsers.DataSource = dtOPP;
                    GvOtherUsers.DataBind();
                    PanelLabour.Visible = false;
                    PanelLabour.Visible = false;
                    PanelOthers.Visible = false;
                    panelContract.Visible = false;
                    PanelReportAuthrity.Visible = false;
                    if (dtOPP.Rows.Count > 0)
                    {
                        PanelOtherUserForGrid.Visible = true;
                        panelgrid.Visible = false;
                    }

                }
            }
            else if (ddlcategory.SelectedValue == "2")
            {
                if (SessionAdmin == "1")
                {
                    string Staff = "L";
                    DataTable dtOPP = new DataTable();
                    dtOPP = objdata.OfficialProfileGridViewForAdmin(Staff, ddldepartment.SelectedValue, SessionCcode, SessionLcode);
                    GvOfficialProfile.DataSource = dtOPP;
                    GvOfficialProfile.DataBind();
                    PanelReportAuthrity.Visible = true;
                    PanelLabour.Visible = true;
                    PanelOthers.Visible = true;
                    panelContract.Visible = false;
                    if (dtOPP.Rows.Count > 0)
                    {
                        panelgrid.Visible = true;
                        PanelOtherUserForGrid.Visible = false;
                    }

                }
                else if (SessionAdmin == "2")
                {
                    string Staff = "L";
                    DataTable dtOPP = new DataTable();
                    dtOPP = objdata.OfficialProfileGridView(Staff, ddldepartment.SelectedValue,SessionCcode, SessionLcode);
                    GvOtherUsers.DataSource = dtOPP;
                    GvOtherUsers.DataBind();

                    PanelLabour.Visible = true;
                    PanelOthers.Visible = true;
                    panelContract.Visible = false;
                    if (dtOPP.Rows.Count > 0)
                    {
                        PanelOtherUserForGrid.Visible = true;
                        panelgrid.Visible = false;
                    }

                }

            }
        }
    }
    protected void btnadd_Click(object sender, EventArgs e)
    {
        bool valuechk = false;
        //panelError.Visible = true;
        if (SessionAdmin == "1")
        {
            if (ddlcategory.SelectedValue == "1")
            {
                PanelOthers.Visible = false;
                
            }
            else
            {
                PanelOthers.Visible = true;
            }
            foreach (GridViewRow row in GvOfficialProfile.Rows)
            {
                RadioButton rb = (RadioButton)row.FindControl("rbtnstatus");

                if (rb.Checked)
                {
                    valuechk = true;
                    Label lblempno1 = (Label)GvOfficialProfile.Rows[row.RowIndex].FindControl("lblempno");
                    Label lblempname2 = (Label)GvOfficialProfile.Rows[row.RowIndex].FindControl("lblEmpName");
                    Label lbllabourtype = (Label)GvOfficialProfile.Rows[row.RowIndex].FindControl("lblcontracttype");
                    // Label lblOtherUser=(Label)GvOfficialProfile.Rows[row                    
                    txtempno.Text = lblempno1.Text;
                    lblempname1.Text = lblempname2.Text;
                    txtlabourtype.Text = lbllabourtype.Text;
                    if (txtlabourtype.Text == "Contract")
                    {
                        ddlcontract.SelectedValue = objdata.Emp_contract(lblempno1.Text);
                    }
                    if (ddlcategory.SelectedValue == "1")
                    {
                        if (SesRoleCode == "1")
                        {
                            txtempno.Text = lblempno1.Text;
                            lblempname1.Text = lblempname2.Text;
                            txtlabourtype.Text = lbllabourtype.Text;
                            PanelAdminUser.Visible = true;
                            
                            //PanelOtherUser.Visible = false;
                            if (txtlabourtype.Text == "Contract")
                            {
                                
                                PanelAdminUser.Visible = true;
                                PanelContarctTypeLabel.Visible = false;
                                PanelContarctTypeRadio.Visible = false;
                                PanelOthers.Visible = false;
                                panelContract.Visible = true;
                            }
                            else
                            {
                                PanelAdminUser.Visible = true;
                                PanelContarctTypeLabel.Visible = false;
                                PanelContarctTypeRadio.Visible = false;
                                //PanelOthers.Visible = true;
                                panelContract.Visible = false;
                            }
                        }
                        else if (SesRoleCode == "2")
                        {

                        }
                    }
                    else if (ddlcategory.SelectedValue == "2")
                    {
                        if (txtlabourtype.Text == "Contract")
                        {
                            PanelAdminUser.Visible = true;
                            PanelContarctTypeLabel.Visible = false;
                            PanelContarctTypeRadio.Visible = false;
                            PanelOthers.Visible = true;
                            panelContract.Visible = true;
                        }
                        else
                        {
                            PanelAdminUser.Visible = true;
                            PanelContarctTypeLabel.Visible = false;
                            PanelContarctTypeRadio.Visible = false;
                            //PanelOthers.Visible = true;
                            panelContract.Visible = false;
                        }
                    }


                    //if (ddlcategory.SelectedValue == "2")
                    //{
                    //    PanelAdminUser.Visible = true;
                    //    PanelContarctTypeLabel.Visible = false;
                    //    PanelContarctTypeRadio.Visible = false;
                    //    PanelOthers.Visible = true;
                    //    panelContract.Visible = true;

                    //}
                }
                panelError.Visible = false;


            }
            if (GvOfficialProfile.Rows.Count == 0)
            {
                //panelError.Visible = true;
                //lblError.Text = "Select the Department";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select the Department');", true);
                //System.Windows.Forms.MessageBox.Show("Select the Department", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            }
            if (valuechk == false)
            {
                //panelError.Visible = true;
                //lblError.Text = "Select the Employee";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select the Employee');", true);
                //System.Windows.Forms.MessageBox.Show("Select the Employee", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            }
        }
        else if (SessionAdmin == "2")
        {
            panelError.Visible = false;
            foreach (GridViewRow row in GvOtherUsers.Rows)
            {
                RadioButton rb = (RadioButton)row.FindControl("rbtnstatus");
                if (rb.Checked)
                {
                    valuechk = true;
                    Label lblempno1 = (Label)GvOtherUsers.Rows[row.RowIndex].FindControl("lblempno");

                    Label lblempname2 = (Label)GvOtherUsers.Rows[row.RowIndex].FindControl("lblEmpName");
                    Label lbllabourtype = (Label)GvOtherUsers.Rows[row.RowIndex].FindControl("lblcontracttype");
                    txtempno.Text = lblempno1.Text;
                    lblempname1.Text = lblempname2.Text;
                    txtlabourtype.Text = lbllabourtype.Text;
                    if (ddlcategory.SelectedValue == "1")
                    {
                        PanelAdminUser.Visible = true;
                        PanelContarctTypeLabel.Visible = false;
                        PanelContarctTypeRadio.Visible = false;
                        PanelOthers.Visible = false;
                        panelContract.Visible = true;
                        //if (txtlabourtype.Text == "Contract")
                        //{
                        //    PanelAdminUser.Visible = true;
                        //    PanelContarctTypeLabel.Visible = false;
                        //    PanelContarctTypeRadio.Visible = false;
                        //    PanelOthers.Visible = true;
                        //    panelContract.Visible = true;
                        //}
                        //else if (txtlabourtype.Text == "Others")
                        //{
                        //    PanelAdminUser.Visible = true;
                        //    PanelContarctTypeLabel.Visible = false;
                        //    PanelContarctTypeRadio.Visible = false;
                        //    PanelOthers.Visible = true;
                        //    panelContract.Visible = false;
                        //}
                    }
                    else if (ddlcategory.SelectedValue == "2")
                    {
                        PanelAdminUser.Visible = true;
                        PanelContarctTypeLabel.Visible = false;
                        PanelContarctTypeRadio.Visible = false;
                        PanelOthers.Visible = true;
                        panelContract.Visible = true;

                    }
                }
            }
            if (valuechk == false)
            {
                //panelError.Visible = true;
                //lblError.Text = "Select the Emmployee";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select the Employee');", true);
                //System.Windows.Forms.MessageBox.Show("Select the Employee", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            }
            if (GvOtherUsers.Rows.Count == 0)
            {
                //panelError.Visible = true;
                //lblError.Text = "Select the Department";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select the Department');", true);
                //System.Windows.Forms.MessageBox.Show("Select the Department", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            }
        }

    }
    protected void txtdobjoin_TextChanged(object sender, EventArgs e)
    {
        //panelError.Visible = true;
        if (txtdobjoin.Text.Length == 10)
        {
            panelError.Visible = false;
            int year = Convert.ToDateTime(txtdobjoin.Text).Year;
            int month = Convert.ToDateTime(txtdobjoin.Text).Month;
            if (month <= 3)
            {
                ddlFinancialPeriod.SelectedItem.Text = Convert.ToString((year - 1) + "-" + year);
            }
            else if (month >= 4)
            {
                ddlFinancialPeriod.SelectedItem.Text = Convert.ToString(year + "-" + (year + 1));
            }
        }
        else
        {
            //panelError.Visible = true;
            //lblError.Text = "Enter the date of Joining Properly";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Enter the date of joining Properly');", true);
            //System.Windows.Forms.MessageBox.Show("Enter the Date of Joining Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
        }
        //DateTime dateofjoining, confirmationdate;
        //dateofjoining = DateTime.ParseExact(txtdobjoin.Text, "dd/MM/yyyy", null);
        //confirmationdate = DateTime.ParseExact(txtconfirmationdate.Text, "dd/MM/yyyy", null);
        //if (dateofjoining < confirmationdate)
        //{
        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Enter the date of joining Properly');", true);

        //}
    }
    protected void rbtnProfie_SelectedIndexChanged(object sender, EventArgs e)
    {
        //panelError.Visible = true;
        if (rbtnProfie.SelectedValue == "1")
        {
            txtconfirmationdate.Enabled = false;
            txtconfirmationdate.Text = "";
        }
        else if (rbtnProfie.SelectedValue == "2")
        {
            txtconfirmationdate.Enabled = true;
            txtconfirmationdate.Text = "";
            //panelError.Visible = true;
            //lblError.Text = "You have selected Confirmation period";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('You Have selected the Confirmation Period....!');", true);
            //System.Windows.Forms.MessageBox.Show("You have Selected the Confirmation Period", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
        }
    }
    protected void rbPF_SelectedIndexChanged(object sender, EventArgs e)
    {
                if (rbPF.SelectedValue == "1")
                {
                    txtpfnumber.Enabled = true;
                }
                else if (rbPF.SelectedValue == "2")
                {
                    txtpfnumber.Enabled = false;
                }
    }
    protected void txtconfirmationdate_TextChanged(object sender, EventArgs e)
    {

    }
    protected void rbtnesi_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (rbtnesi.SelectedValue == "1")
        {
            txtesic.Enabled = true;
        }
        else
        {
            txtesic.Enabled = false;
            txtesic.Text = "";
        }
    }
    protected void rbtnsalarythroug_SelectedIndexChanged(object sender, EventArgs e)
    {
         if (rbtnsalarythroug.SelectedValue == "1")
         {
             txtbankacno.Enabled = false;
             ddlbankname.Enabled = false;
             txtbranch.Enabled = false;
             txtbranch.Text = "";
             txtbankacno.Text = "";
             MstBankDropDown();

         }
         else if (rbtnsalarythroug.SelectedValue == "2")
         {
             txtbankacno.Enabled = true;
             ddlbankname.Enabled = true;
             txtbranch.Enabled = true;
         }
    }
    protected void btnsave_Click(object sender, EventArgs e)
    {
        try
        {

            //panelError.Visible = true;
            bool isRegistered = false;
            string pf_type = "";
            bool ErrFlag = false;
            DateTime Dofjoin, ExpiryDate, ExpiredDate1 = Convert.ToDateTime("01-01-1999"), ContractStartingDate, ContractStartingDate1 = Convert.ToDateTime("01-01-1999"), ContractEndingDate, ContractEndingDate1 = Convert.ToDateTime("01-01-1999");
            DateTime DofConfir = Convert.ToDateTime("01-01-1999");
            Dofjoin = new DateTime();
            //DateTime DofConfir;
            //DofConfir = new DateTime();
            string ExpDate = "";
            DateTime dateofjoining, confirmationdate;


            if (txtempno.Text == "")
            {
                //lblError.Text = "Select the Employee and click Add";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select Radio Button in Display Record and click the Add button');", true);
                //System.Windows.Forms.MessageBox.Show("Select the Employee", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            else if (txtdobjoin.Text == "")
            {
                //lblError.Text = "Enter the Date of Joining";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Date of joining');", true);
                //System.Windows.Forms.MessageBox.Show("Enter the Date of Joining", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }

            else if (rbtnProfie.SelectedValue == "")
            {
                //lblError.Text = "Select the Profile Type";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Profile type');", true);
                //System.Windows.Forms.MessageBox.Show("Select the Profile type", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            else if (rbtnesi.SelectedValue == "")
            {
                //lblError.Text = "Select the Option Eligible for ESI";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the option Elgible for ESI');", true);
                //System.Windows.Forms.MessageBox.Show("Select the ESI", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            else if (rbtnsalarythroug.SelectedValue == "")
            {
                //lblError.Text = "Select the Salary Through";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select Salary Through');", true);
                //System.Windows.Forms.MessageBox.Show("Select the Salary Through", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
         
            else if (rbPF.SelectedValue == "")
            {
                //lblError.Text = "Select the Eligible for PF";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select Eligible for PF');", true);
                //System.Windows.Forms.MessageBox.Show("Select the Eligible for PF", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            else if (rbtnesi.SelectedValue == "")
            {
                //lblError.Text = "Select the ESI";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the ESI');", true);
                //System.Windows.Forms.MessageBox.Show("Select the ESI", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            else if (txtdobjoin.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Date of Join');", true);
                ErrFlag = true;
            }
            else if (rbPF.SelectedValue == "1")
            {
                if (txtpfnumber.Text == "")
                {
                    //lblError.Text = "Enter the PF Number";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the PF number');", true);
                    //System.Windows.Forms.MessageBox.Show("Enter the PF Number", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    ErrFlag = true;

                }
            }

            else if (rbtnesi.SelectedValue == "1")
            {
                if (txtesic.Text == "")
                {
                    //lblError.Text = "Enter the ESI Value";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the ESI Value');", true);
                    //System.Windows.Forms.MessageBox.Show("Enter the ESI Value", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    ErrFlag = true;
                }
            }

            else if (rbtnsalarythroug.SelectedValue == "2")
            {
                if (ddlbankname.SelectedValue == "0")
                {
                    //lblError.Text = "Select the Bank Name";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select Bank Name');", true);
                    //System.Windows.Forms.MessageBox.Show("Select the Bank Name", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    ErrFlag = true;
                }
                else if (txtbankacno.Text == "")
                {
                    //lblError.Text = "Enter the Account No";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Bank Account Number');", true);
                    //System.Windows.Forms.MessageBox.Show("Enter the Bank Account Number", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    ErrFlag = true;
                }
                else if (txtbranch.Text == "")
                {
                    //lblError.Text = "Enter the Branch Name";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Branch Name');", true);
                    //System.Windows.Forms.MessageBox.Show("Enter the Branch Name", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    ErrFlag = true;
                }
            }

            if (rbtnProfie.SelectedValue == "2")
            {
                if (txtconfirmationdate.Text == "")
                {
                    //lblError.Text = "Enter the Confirmation Date";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the confirmation date');", true);
                    //System.Windows.Forms.MessageBox.Show("Enter the Confirmation Date", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    ErrFlag = true;
                }
            }
            if (ddlcategory.SelectedValue == "2")
            {
                //if (txtlabourtype.Text == "Contract")
                //{
                //    if (rbtncontract.SelectedValue == "")
                //    {
                //        //lblError.Text = "Select the Contract Type";
                //        ScriptManager.RegisterStartupScript(this, this.GetType(), "windows", "alert('Select Contract Type');", true);
                //        //System.Windows.Forms.MessageBox.Show("Select the Contract Type", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                //        ErrFlag = true;
                //    }
                //    else if (ddlcontract.SelectedValue == "0")
                //    {
                //        //lblError.Text = "Select the Contract period";
                //        ScriptManager.RegisterStartupScript(this, this.GetType(), "windows", "alert('Select Contract Period');", true);
                //        //System.Windows.Forms.MessageBox.Show("Select the Contract Period", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                //        ErrFlag = true;
                //    }
                //}
                //else if (txtlabourtype.Text == "Others")
                //{
                //    if (rbtnotherwages.SelectedValue == "")
                //    {
                //        //lblError.Text = "Select the Wages Type";
                //        ScriptManager.RegisterStartupScript(this, this.GetType(), "windows", "alert('Select Wages Type');", true);
                //        //System.Windows.Forms.MessageBox.Show("Select the Wages Type", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                //        ErrFlag = true;
                //    }
                //}

            }

            if (!ErrFlag)
            {
                if (rbtnProfie.SelectedValue == "2")
                {
                    dateofjoining = DateTime.ParseExact(txtdobjoin.Text, "dd-MM-yyyy", culterInfo.DateTimeFormat);
                    confirmationdate = DateTime.ParseExact(txtconfirmationdate.Text, "dd-MM-yyyy", culterInfo.DateTimeFormat);
                    if (dateofjoining > confirmationdate)
                    {
                        //lblError.Text = "Enter the Date of Joining";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Enter the date of joining Properly');", true);
                        //System.Windows.Forms.MessageBox.Show("Enter the Date of joining", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                        ErrFlag = true;
                    }
                }
            }

            if (!ErrFlag)
            {
                #region Probation Period For Staff
                if (ddlcategory.SelectedValue == "1")
                {
                    #region Probation Period Expired

                    objOff.Dateofjoining = txtdobjoin.Text;
                    Dofjoin = DateTime.ParseExact(objOff.Dateofjoining, "dd-MM-yyyy", null);
                    objOff.Probationperiod = ddlprobation.SelectedItem.Text;
                    if (ddlprobation.SelectedItem.Text == "3 Months")
                    {
                        ExpiryDate = Dofjoin.AddMonths(3);
                        ExpiredDate1 = ExpiryDate.Date;
                    }
                    else if (ddlprobation.SelectedItem.Text == "6 Months")
                    {

                        ExpiryDate = Dofjoin.AddMonths(6);
                        ExpiredDate1 = ExpiryDate.Date;

                    }
                    else if (ddlprobation.SelectedItem.Text == "1 Year")
                    {
                        ExpiryDate = Dofjoin.AddMonths(12);
                        ExpiredDate1 = ExpiryDate.Date;
                    }
                    else if (ddlprobation.SelectedItem.Text == "2 Year")
                    {
                        ExpiryDate = Dofjoin.AddMonths(24);
                        ExpiredDate1 = ExpiryDate.Date;
                    }
                    else if (ddlprobation.SelectedItem.Text == "3 Year")
                    {
                        ExpiryDate = Dofjoin.AddMonths(36);
                        ExpiredDate1 = ExpiryDate.Date;
                    }
                    else if (ddlprobation.SelectedItem.Text == "5 Year")
                    {
                        ExpiryDate = Dofjoin.AddMonths(60);
                        ExpiredDate1 = ExpiryDate.Date;
                    }
                    else
                    {
                        ExpiryDate = Dofjoin;
                        ExpiredDate1 = ExpiryDate.Date;
                    }
                    //if (ddlcontract.SelectedItem.Text == "3 Months")
                    //{
                    //    ContractStartingDate = ExpiredDate1.AddDays(1);
                    //    ContractStartingDate1 = ContractStartingDate.Date;
                    //    ContractEndingDate = ContractStartingDate1.AddMonths(3);
                    //    ContractEndingDate1 = ContractEndingDate.Date;
                    //}
                    //else if (ddlcontract.SelectedItem.Text == "6 Months")
                    //{
                    //    ContractStartingDate = ExpiredDate1.AddDays(1);
                    //    ContractStartingDate1 = ContractStartingDate.Date;
                    //    ContractEndingDate = ContractStartingDate1.AddMonths(6);
                    //    ContractEndingDate1 = ContractEndingDate.Date;
                    //}
                    //else if (ddlcontract.SelectedItem.Text == "1 Year")
                    //{
                    //    ContractStartingDate = ExpiredDate1.AddDays(1);
                    //    ContractStartingDate1 = ContractStartingDate.Date;
                    //    ContractEndingDate = ContractStartingDate1.AddMonths(12);
                    //    ContractEndingDate1 = ContractEndingDate.Date;
                    //}
                    //else if (ddlcontract.SelectedItem.Text == "2 Year")
                    //{
                    //    ContractStartingDate = ExpiredDate1.AddDays(1);
                    //    ContractStartingDate1 = ContractStartingDate.Date;
                    //    ContractEndingDate = ContractStartingDate1.AddMonths(24);
                    //    ContractEndingDate1 = ContractEndingDate.Date;
                    //}
                    //else if (ddlcontract.SelectedItem.Text == "3 Year")
                    //{
                    //    ContractStartingDate = ExpiredDate1.AddDays(1);
                    //    ContractStartingDate1 = ContractStartingDate.Date;
                    //    ContractEndingDate = ContractStartingDate1.AddMonths(36);
                    //    ContractEndingDate1 = ContractEndingDate.Date;
                    //}
                    //else if (ddlcontract.SelectedItem.Text == "5 Year")
                    //{
                    //    ContractStartingDate = ExpiredDate1.AddDays(1);
                    //    ContractStartingDate1 = ContractStartingDate.Date;
                    //    ContractEndingDate = ContractStartingDate1.AddMonths(60);
                    //    ContractEndingDate1 = ContractEndingDate.Date;
                    //}
                    //else
                    //{
                    //    ContractStartingDate = ExpiredDate1;
                    //    ContractStartingDate1 = ContractStartingDate.Date;
                    //    ContractEndingDate = ContractStartingDate1;
                    //    ContractEndingDate1 = ContractEndingDate.Date;
                    //}
                }


                    #endregion

                #endregion
                else if (ddlcategory.SelectedValue == "2")
                {
                    #region ContractLabour
                    if (txtlabourtype.Text == "Contract")
                    {
                        if (rbtncontract.SelectedValue == "1")
                        {
                            //objOff.Dateofjoining = txtdobjoin.Text;
                            //Dofjoin = DateTime.ParseExact(objOff.Dateofjoining, "dd-MM-yyyy", null);
                            //objOff.Probationperiod = ddlprobation.SelectedItem.Text;
                            //if (ddlprobation.SelectedItem.Text == "3 Months")
                            //{
                            //    ExpiryDate = Dofjoin.AddMonths(3);
                            //    ExpiredDate1 = ExpiryDate.Date;
                            //}
                            //else if (ddlprobation.SelectedItem.Text == "6 Months")
                            //{

                            //    ExpiryDate = Dofjoin.AddMonths(6);
                            //    ExpiredDate1 = ExpiryDate.Date;

                            //}
                            //else if (ddlprobation.SelectedItem.Text == "1 Year")
                            //{
                            //    ExpiryDate = Dofjoin.AddMonths(12);
                            //    ExpiredDate1 = ExpiryDate.Date;
                            //}
                            //else if (ddlprobation.SelectedItem.Text == "2 Year")
                            //{
                            //    ExpiryDate = Dofjoin.AddMonths(24);
                            //    ExpiredDate1 = ExpiryDate.Date;
                            //}
                            //else if (ddlprobation.SelectedItem.Text == "3 Year")
                            //{
                            //    ExpiryDate = Dofjoin.AddMonths(36);
                            //    ExpiredDate1 = ExpiryDate.Date;
                            //}
                            //else if (ddlprobation.SelectedItem.Text == "5 Year")
                            //{
                            //    ExpiryDate = Dofjoin.AddMonths(60);
                            //    ExpiredDate1 = ExpiryDate.Date;
                            //}
                            //else
                            //{
                            //    ExpiryDate = Dofjoin;
                            //    ExpiredDate1 = ExpiryDate.Date;
                            //}
                            //if (ddlcontract.SelectedItem.Text == "3 Months")
                            //{
                            //    ContractStartingDate = ExpiredDate1.AddDays(1);
                            //    ContractStartingDate1 = ContractStartingDate.Date;
                            //    ContractEndingDate = ContractStartingDate1.AddMonths(3);
                            //    ContractEndingDate1 = ContractEndingDate.Date;
                            //}
                            //else if (ddlcontract.SelectedItem.Text == "6 Months")
                            //{
                            //    ContractStartingDate = ExpiredDate1.AddDays(1);
                            //    ContractStartingDate1 = ContractStartingDate.Date;
                            //    ContractEndingDate = ContractStartingDate1.AddMonths(6);
                            //    ContractEndingDate1 = ContractEndingDate.Date;
                            //}
                            //else if (ddlcontract.SelectedItem.Text == "1 Year")
                            //{
                            //    ContractStartingDate = ExpiredDate1.AddDays(1);
                            //    ContractStartingDate1 = ContractStartingDate.Date;
                            //    ContractEndingDate = ContractStartingDate1.AddMonths(12);
                            //    ContractEndingDate1 = ContractEndingDate.Date;
                            //}
                            //else if (ddlcontract.SelectedItem.Text == "2 Year")
                            //{
                            //    ContractStartingDate = ExpiredDate1.AddDays(1);
                            //    ContractStartingDate1 = ContractStartingDate.Date;
                            //    ContractEndingDate = ContractStartingDate1.AddMonths(24);
                            //    ContractEndingDate1 = ContractEndingDate.Date;
                            //}
                            //else if (ddlcontract.SelectedItem.Text == "3 Year")
                            //{
                            //    ContractStartingDate = ExpiredDate1.AddDays(1);
                            //    ContractStartingDate1 = ContractStartingDate.Date;
                            //    ContractEndingDate = ContractStartingDate1.AddMonths(36);
                            //    ContractEndingDate1 = ContractEndingDate.Date;
                            //}
                            //else if (ddlcontract.SelectedItem.Text == "5 Year")
                            //{
                            //    ContractStartingDate = ExpiredDate1.AddDays(1);
                            //    ContractStartingDate1 = ContractStartingDate.Date;
                            //    ContractEndingDate = ContractStartingDate1.AddMonths(60);
                            //    ContractEndingDate1 = ContractEndingDate.Date;
                            //}
                            //else
                            //{
                            //    ContractStartingDate = ExpiredDate1;
                            //    ContractStartingDate1 = ContractStartingDate.Date;
                            //    ContractEndingDate = ContractStartingDate1;
                            //    ContractEndingDate1 = ContractEndingDate.Date;
                            //}
                        }

                        else if (rbtncontract.SelectedValue == "2")
                        {
                            //objOff.Dateofjoining = txtdobjoin.Text;
                            //Dofjoin = DateTime.ParseExact(objOff.Dateofjoining, "dd-MM-yyyy", null);
                            //if (ddlprobation.SelectedItem.Text == "3 Months")
                            //{
                            //    ExpiryDate = Dofjoin.AddMonths(3);
                            //    ExpiredDate1 = ExpiryDate.Date;
                            //}
                            //else if (ddlprobation.SelectedItem.Text == "6 Months")
                            //{

                            //    ExpiryDate = Dofjoin.AddMonths(6);
                            //    ExpiredDate1 = ExpiryDate.Date;
                            //}
                            //else if (ddlprobation.SelectedItem.Text == "1 Year")
                            //{
                            //    ExpiryDate = Dofjoin.AddMonths(12);
                            //    ExpiredDate1 = ExpiryDate.Date;
                            //}
                            //else if (ddlprobation.SelectedItem.Text == "2 Year")
                            //{
                            //    ExpiryDate = Dofjoin.AddMonths(24);
                            //    ExpiredDate1 = ExpiryDate.Date;
                            //}
                            //else if (ddlprobation.SelectedItem.Text == "3 Year")
                            //{
                            //    ExpiryDate = Dofjoin.AddMonths(36);
                            //    ExpiredDate1 = ExpiryDate.Date;
                            //}
                            //else if (ddlprobation.SelectedItem.Text == "5 Year")
                            //{
                            //    ExpiryDate = Dofjoin.AddMonths(60);
                            //    ExpiredDate1 = ExpiryDate.Date;
                            //}
                            //else
                            //{
                            //    ExpiryDate = Dofjoin;
                            //    ExpiredDate1 = ExpiryDate.Date;
                            //}
                            ////if (ddlcontract.SelectedItem.Text == "3 Months")
                            ////{
                            ////    ContractStartingDate = ExpiredDate1.AddDays(1);
                            ////    ContractStartingDate1 = ContractStartingDate.Date;
                            ////    ContractEndingDate = ContractStartingDate1.AddMonths(3);
                            ////    ContractEndingDate1 = ContractEndingDate.Date;
                            ////}
                            ////else if (ddlcontract.SelectedItem.Text == "6 Months")
                            ////{
                            ////    ContractStartingDate = ExpiredDate1.AddDays(1);
                            ////    ContractStartingDate1 = ContractStartingDate.Date;
                            ////    ContractEndingDate = ContractStartingDate1.AddMonths(6);
                            ////    ContractEndingDate1 = ContractEndingDate.Date;
                            ////}
                            ////else if (ddlcontract.SelectedItem.Text == "1 Year")
                            ////{
                            ////    ContractStartingDate = ExpiredDate1.AddDays(1);
                            ////    ContractStartingDate1 = ContractStartingDate.Date;
                            ////    ContractEndingDate = ContractStartingDate1.AddMonths(12);
                            ////    ContractEndingDate1 = ContractEndingDate.Date;
                            ////}
                            ////else if (ddlcontract.SelectedItem.Text == "2 Year")
                            ////{
                            ////    ContractStartingDate = ExpiredDate1.AddDays(1);
                            ////    ContractStartingDate1 = ContractStartingDate.Date;
                            ////    ContractEndingDate = ContractStartingDate1.AddMonths(24);
                            ////    ContractEndingDate1 = ContractEndingDate.Date;
                            ////}
                            ////else if (ddlcontract.SelectedItem.Text == "3 Year")
                            ////{
                            ////    ContractStartingDate = ExpiredDate1.AddDays(1);
                            ////    ContractStartingDate1 = ContractStartingDate.Date;
                            ////    ContractEndingDate = ContractStartingDate1.AddMonths(36);
                            ////    ContractEndingDate1 = ContractEndingDate.Date;
                            ////}
                            ////else if (ddlcontract.SelectedItem.Text == "5 Year")
                            ////{
                            ////    ContractStartingDate = ExpiredDate1.AddDays(1);
                            ////    ContractStartingDate1 = ContractStartingDate.Date;
                            ////    ContractEndingDate = ContractStartingDate1.AddMonths(60);
                            ////    ContractEndingDate1 = ContractEndingDate.Date;
                            ////}
                            ////else
                            ////{
                            ////    ContractStartingDate = ExpiredDate1;
                            ////    ContractStartingDate1 = ContractStartingDate.Date;
                            ////    ContractEndingDate = ContractStartingDate1;
                            ////    ContractEndingDate1 = ContractEndingDate.Date;
                            ////}
                        }
                        else
                        {
                            //objOff.Dateofjoining = txtdobjoin.Text;
                            //Dofjoin = DateTime.ParseExact(objOff.Dateofjoining, "dd-MM-yyyy", null);
                            //if (ddlprobation.SelectedItem.Text == "3 Months")
                            //{
                            //    ExpiryDate = Dofjoin.AddMonths(3);
                            //    ExpiredDate1 = ExpiryDate.Date;
                            //}
                            //else if (ddlprobation.SelectedItem.Text == "6 Months")
                            //{

                            //    ExpiryDate = Dofjoin.AddMonths(6);
                            //    ExpiredDate1 = ExpiryDate.Date;
                            //}
                            //else if (ddlprobation.SelectedItem.Text == "1 Year")
                            //{
                            //    ExpiryDate = Dofjoin.AddMonths(12);
                            //    ExpiredDate1 = ExpiryDate.Date;
                            //}
                            //else if (ddlprobation.SelectedItem.Text == "2 Year")
                            //{
                            //    ExpiryDate = Dofjoin.AddMonths(24);
                            //    ExpiredDate1 = ExpiryDate.Date;
                            //}
                            //else if (ddlprobation.SelectedItem.Text == "3 Year")
                            //{
                            //    ExpiryDate = Dofjoin.AddMonths(36);
                            //    ExpiredDate1 = ExpiryDate.Date;
                            //}

                            //else if (ddlprobation.SelectedItem.Text == "5 Year")
                            //{
                            //    ExpiryDate = Dofjoin.AddMonths(60);
                            //    ExpiredDate1 = ExpiryDate.Date;
                            //}
                            //else
                            //{
                            //    ExpiryDate = Dofjoin;
                            //    ExpiredDate1 = ExpiryDate.Date;
                            //}
                            //if (ddlcontract.SelectedValue != "0")
                            //{
                            //    if (ddlcontract.SelectedItem.Text == "3 Months")
                            //    {
                            //        ContractStartingDate = ExpiredDate1.AddDays(1);
                            //        ContractStartingDate1 = ContractStartingDate.Date;
                            //        ContractEndingDate = ContractStartingDate1.AddMonths(3);
                            //        ContractEndingDate1 = ContractEndingDate.Date;
                            //    }
                            //    else if (ddlcontract.SelectedItem.Text == "6 Months")
                            //    {
                            //        ContractStartingDate = ExpiredDate1.AddDays(1);
                            //        ContractStartingDate1 = ContractStartingDate.Date;
                            //        ContractEndingDate = ContractStartingDate1.AddMonths(6);
                            //        ContractEndingDate1 = ContractEndingDate.Date;
                            //    }
                            //    else if (ddlcontract.SelectedItem.Text == "1 Year")
                            //    {
                            //        ContractStartingDate = ExpiredDate1.AddDays(1);
                            //        ContractStartingDate1 = ContractStartingDate.Date;
                            //        ContractEndingDate = ContractStartingDate1.AddMonths(12);
                            //        ContractEndingDate1 = ContractEndingDate.Date;
                            //    }
                            //    else if (ddlcontract.SelectedItem.Text == "2 Year")
                            //    {
                            //        ContractStartingDate = ExpiredDate1.AddDays(1);
                            //        ContractStartingDate1 = ContractStartingDate.Date;
                            //        ContractEndingDate = ContractStartingDate1.AddMonths(24);
                            //        ContractEndingDate1 = ContractEndingDate.Date;
                            //    }
                            //    else if (ddlcontract.SelectedItem.Text == "3 Year")
                            //    {
                            //        ContractStartingDate = ExpiredDate1.AddDays(1);
                            //        ContractStartingDate1 = ContractStartingDate.Date;
                            //        ContractEndingDate = ContractStartingDate1.AddMonths(36);
                            //        ContractEndingDate1 = ContractEndingDate.Date;
                            //    }
                            //    else if (ddlcontract.SelectedItem.Text == "5 Year")
                            //    {
                            //        ContractStartingDate = ExpiredDate1.AddDays(1);
                            //        ContractStartingDate1 = ContractStartingDate.Date;
                            //        ContractEndingDate = ContractStartingDate1.AddMonths(60);
                            //        ContractEndingDate1 = ContractEndingDate.Date;
                            //    }
                            //    else
                            //    {
                            //        ContractStartingDate = ExpiredDate1;
                            //        ContractStartingDate1 = ContractStartingDate.Date;
                            //        ContractEndingDate = ContractStartingDate1;
                            //        ContractEndingDate1 = ContractEndingDate.Date;
                            //    }
                            //}
                        }

                    }
                    #endregion
                    #region Others
                    else
                    {
                        objOff.Dateofjoining = txtdobjoin.Text;
                        Dofjoin = DateTime.ParseExact(objOff.Dateofjoining, "dd-MM-yyyy", null);
                        objOff.Probationperiod = ddlprobation.SelectedItem.Text;
                        if (ddlprobation.SelectedItem.Text == "3 Months")
                        {
                            ExpiryDate = Dofjoin.AddMonths(3);
                            ExpiredDate1 = ExpiryDate.Date;
                        }
                        else if (ddlprobation.SelectedItem.Text == "6 Months")
                        {

                            ExpiryDate = Dofjoin.AddMonths(6);
                            ExpiredDate1 = ExpiryDate.Date;

                        }
                        else if (ddlprobation.SelectedItem.Text == "1 Year")
                        {
                            ExpiryDate = Dofjoin.AddMonths(12);
                            ExpiredDate1 = ExpiryDate.Date;
                        }
                        else if (ddlprobation.SelectedItem.Text == "2 Year")
                        {
                            ExpiryDate = Dofjoin.AddMonths(24);
                            ExpiredDate1 = ExpiryDate.Date;
                        }
                        else if (ddlprobation.SelectedItem.Text == "3 Year")
                        {
                            ExpiryDate = Dofjoin.AddMonths(36);
                            ExpiredDate1 = ExpiryDate.Date;
                        }
                        else if (ddlprobation.SelectedItem.Text == "5 Year")
                        {
                            ExpiryDate = Dofjoin.AddMonths(60);
                            ExpiredDate1 = ExpiryDate.Date;
                        }
                        else
                        {
                            ExpiryDate = Dofjoin;
                            ExpiredDate1 = ExpiryDate.Date;
                        }
                        //    if (ddlcontract.SelectedItem.Text == "3 Months")
                        //    {
                        //        ContractStartingDate = ExpiredDate1.AddDays(1);
                        //        ContractStartingDate1 = ContractStartingDate.Date;
                        //        ContractEndingDate = ContractStartingDate1.AddMonths(3);
                        //        ContractEndingDate1 = ContractEndingDate.Date;
                        //    }
                        //    else if (ddlcontract.SelectedItem.Text == "6 Months")
                        //    {
                        //        ContractStartingDate = ExpiredDate1.AddDays(1);
                        //        ContractStartingDate1 = ContractStartingDate.Date;
                        //        ContractEndingDate = ContractStartingDate1.AddMonths(6);
                        //        ContractEndingDate1 = ContractEndingDate.Date;
                        //    }
                        //    else if (ddlcontract.SelectedItem.Text == "1 Year")
                        //    {
                        //        ContractStartingDate = ExpiredDate1.AddDays(1);
                        //        ContractStartingDate1 = ContractStartingDate.Date;
                        //        ContractEndingDate = ContractStartingDate1.AddMonths(12);
                        //        ContractEndingDate1 = ContractEndingDate.Date;
                        //    }
                        //    else if (ddlcontract.SelectedItem.Text == "2 Year")
                        //    {
                        //        ContractStartingDate = ExpiredDate1.AddDays(1);
                        //        ContractStartingDate1 = ContractStartingDate.Date;
                        //        ContractEndingDate = ContractStartingDate1.AddMonths(24);
                        //        ContractEndingDate1 = ContractEndingDate.Date;
                        //    }
                        //    else if (ddlcontract.SelectedItem.Text == "3 Year")
                        //    {
                        //        ContractStartingDate = ExpiredDate1.AddDays(1);
                        //        ContractStartingDate1 = ContractStartingDate.Date;
                        //        ContractEndingDate = ContractStartingDate1.AddMonths(36);
                        //        ContractEndingDate1 = ContractEndingDate.Date;
                        //    }
                        //    else if (ddlcontract.SelectedItem.Text == "5 Year")
                        //    {
                        //        ContractStartingDate = ExpiredDate1.AddDays(1);
                        //        ContractStartingDate1 = ContractStartingDate.Date;
                        //        ContractEndingDate = ContractStartingDate1.AddMonths(60);
                        //        ContractEndingDate1 = ContractEndingDate.Date;
                        //    }
                        //    else
                        //    {
                        //        ContractStartingDate = ExpiredDate1;
                        //        ContractStartingDate1 = ContractStartingDate.Date;
                        //        ContractEndingDate = ContractStartingDate1;
                        //        ContractEndingDate1 = ContractEndingDate.Date;
                        //    }
                        //    PanelOthers.Visible = true;
                        //}
                    #endregion
                    }
                }
                if (!ErrFlag)
                {
                    if (chkpf.Checked == true)
                    {
                        pf_type = "1";
                    }
                    else
                    {
                        pf_type = "0";
                    }
                    panelError.Visible = false;
                    objOff.Dateofjoining = txtdobjoin.Text;
                    Dofjoin = DateTime.ParseExact(objOff.Dateofjoining, "dd-MM-yyyy", culterInfo.DateTimeFormat);
                    objOff.Probationperiod = ddlprobation.SelectedValue;
                    objOff.Confirmationdate = txtconfirmationdate.Text;

                    //DofConfir = DateTime.ParseExact(objOff.Confirmationdate, "dd/MM/yyyy", null);
                    if (rbtnProfie.SelectedValue == "2")
                    {
                        DofConfir = Convert.ToDateTime(objOff.Confirmationdate);
                    }
                    objOff.Pannumber = txtpannumber.Text;
                    objOff.ESICnumber = txtesic.Text;
                    objOff.PFnumber = txtpfnumber.Text;
                    objOff.InsurancecompanyName = ddlInsurance.SelectedValue;
                    objOff.Insurancenumber = txtinsuranceno.Text;
                    if (ddlcategory.SelectedValue == "1")
                    {
                        objOff.Eligibleforovertime = "2";
                    }
                    else
                    {
                        objOff.Eligibleforovertime = rbtnovertime.SelectedValue;
                    }

                    //objOff.modeofpayment = rbtnmodeofpay.SelectedValue;
                    objOff.ReportingAuthorityName = txtauthoritynm.Text;
                    if (ddlcategory.SelectedValue == "1")
                    {
                        if (txtlabourtype.Text == "Contract")
                        {
                            objOff.ContractType = "2";
                        }
                        else
                        {
                            objOff.ContractType = rbtncontract.SelectedValue;
                        }
                    }
                    else
                    {

                        objOff.ContractType = rbtncontract.SelectedValue;
                    }
                    objOff.LabourType = txtlabourtype.Text;
                    objOff.EligblePF = rbPF.SelectedValue;
                    if ((ddlcategory.SelectedValue == "2") && (PanelOthers.Visible == true))
                    {
                        objOff.WagesType = rbtnotherwages.SelectedValue;
                    }
                    else
                    {
                        objOff.WagesType = "2";
                    }
                    objOff.contractperiod = ddlcontract.SelectedValue;
                    txtbasicSalary.Text = "0";
                    objOff.BasicSalary = txtbasicSalary.Text;
                    objOff.EligibleESI = rbtnesi.SelectedValue;
                    objOff.Financialperiod = ddlFinancialPeriod.SelectedValue;
                    objOff.ProfileType = rbtnProfie.SelectedValue;
                    objOff.modeofpayment = rbtnsalarythroug.SelectedValue;
                    objOff.BankACno = txtbankacno.Text;
                    objOff.BankName = ddlbankname.SelectedValue;
                    objOff.Branchname = txtbranch.Text;
                    objOff.Ccode = SessionCcode;
                    objOff.Lcode = SessionLcode;
                    objOff.Mess = rbtnMess.SelectedValue;
                    objOff.Incentive = rbtIncentive.SelectedValue;
                    objOff.UAN = txtUAN.Text;
                    if (txtlabourtype.Text == "Contract")
                    {
                        DataTable dt_off = new DataTable();
                        dt_off = objdata.Official_contract(ddlcontract.SelectedValue);
                        if (dt_off.Rows.Count > 0)
                        {
                            objOff.EmpNo = txtempno.Text;
                            objOff.Contractname = ddlcontract.SelectedValue;
                            
                            if (dt_off.Rows[0]["ContractType"].ToString() == "1")
                            {
                                objOff.ContractType = "1";
                                objOff.yr = dt_off.Rows[0]["ContractYear"].ToString();
                                objOff.YrDays = dt_off.Rows[0]["YrDays"].ToString();
                                objOff.PlannedDays = dt_off.Rows[0]["PlannedDays"].ToString();
                                objOff.BalanceDays = dt_off.Rows[0]["PlannedDays"].ToString();
                                objOff.Days1 = "0";
                                objOff.Months = "0";
                                objOff.FixedDays = "0";
                                
                            }
                            else if (dt_off.Rows[0]["ContractType"].ToString() == "2")
                            {
                                objOff.ContractType = "2";
                                objOff.yr = "0";                                
                                objOff.Days1 = "0";
                                objOff.YrDays = "0";
                                objOff.PlannedDays = dt_off.Rows[0]["PlannedDays"].ToString();
                                objOff.BalanceDays = dt_off.Rows[0]["PlannedDays"].ToString();
                                objOff.Months = dt_off.Rows[0]["ContractMonth"].ToString();
                                objOff.FixedDays = dt_off.Rows[0]["FixedDays"].ToString();
                            }
                            else if (dt_off.Rows[0]["ContractType"].ToString() == "3")
                            {
                                objOff.ContractType = "3";
                                objOff.yr = "0";
                                objOff.YrDays = "0";
                                objOff.PlannedDays = dt_off.Rows[0]["PlannedDays"].ToString();
                                objOff.BalanceDays = dt_off.Rows[0]["PlannedDays"].ToString();
                                objOff.Days1 = dt_off.Rows[0]["ContractDays"].ToString();
                                objOff.Months = "0";
                                objOff.FixedDays = "0";
                            }
                            
                        }
                    }
                    else
                    {
                    }
                    isRegistered = true;
                }

                try
                {
                    if (isRegistered)
                    {
                        objdata.OfficalProfile(objOff, txtempno.Text, Dofjoin, DofConfir, ExpiredDate1, ContractStartingDate1, ContractEndingDate1, pf_type);
                        if (txtlabourtype.Text == "Contract")
                        {
                            objdata.Insert_ContractDet(objOff, txtdobjoin.Text);
                        }
                        if (rbPF.SelectedValue == "1")
                        {
                        }
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Your Official Profile Registered Successfully...!');", true);
                        //System.Windows.Forms.MessageBox.Show("Your Official Profile Profile Successfully", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                        Clear();
                        //panelsuccess.Visible = true;
                        //lbloutput.Text = "Saved Successfully";
                        //btnsave.Enabled = false;
                        //btnEdit.Enabled = false;
                        //btnreset.Enabled = false;
                        //btncancel.Enabled = false;
                        //btnadd.Enabled = false;
                        //btnclick.Enabled = false;
                        //btnok.Focus();
                        //panelError.Visible = false;
                        if (SessionAdmin == "1")
                        {
                            OfficialProfileDisplay_admin();

                        }
                        else if (SessionAdmin == "2")
                        {
                            OfficialProfileDisplay();
                        }

                    }

                }
                catch (Exception ex)
                {
                    //panelError.Visible = true;
                    //lblError.Text = "Server Error - Contact Admin";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Server Error Contact Admin....!');", true);
                    //System.Windows.Forms.MessageBox.Show("Server Error Contact Admin", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    ErrFlag = true;
                }
            }
        }
        catch (Exception ex)
        {
            //lblError.Text = "Contact Admin";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Please Contact Admin....!');", true);
            //System.Windows.Forms.MessageBox.Show("Please Contact Admin", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
        }

    }
    protected void btnEdit_Click(object sender, EventArgs e)
    {
        Response.Redirect("EditOfficialProfile.aspx");
    }
    protected void btnreset_Click(object sender, EventArgs e)
    {
        Clear();
    }
    protected void btncancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("OfficalProfileDetails.aspx?Cancel=cancel");
    }
    protected void btnok_Click(object sender, EventArgs e)
    {
        Clear();
        panelsuccess.Visible = false;
        lbloutput.Text = "Saved Successfully";
        btnsave.Enabled = true;
        btnEdit.Enabled = true;
        btnreset.Enabled = true;
        btncancel.Enabled = true;
        btnadd.Enabled = true;
        btnclick.Enabled = true;
        
        panelError.Visible = false;
        if (SessionAdmin == "1")
        {
            OfficialProfileDisplay_admin();

        }
        else if (SessionAdmin == "2")
        {
            OfficialProfileDisplay();
        }
    }
    protected void btnEmp_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptEmpDownload.aspx");
    }
    protected void btnatt_Click(object sender, EventArgs e)
    {
        Response.Redirect("AttenanceDownload.aspx");
    }
    protected void btnLeave_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptLeaveSample.aspx");
    }
    protected void btnOT_Click(object sender, EventArgs e)
    {
        Response.Redirect("OTDownload.aspx");
    }
    protected void btncontract_Click(object sender, EventArgs e)
    {
        Response.Redirect("ContractRPT.aspx");
    }
    protected void btnSal_Click(object sender, EventArgs e)
    {
        Response.Redirect("FrmDeductionDownload.aspx");
    }
    protected void rbtnotherwages_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
}
