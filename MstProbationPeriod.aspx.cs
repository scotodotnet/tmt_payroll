﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class MstProbationPeriod_1 : System.Web.UI.Page
{
    
    BALDataAccess objdata = new BALDataAccess();
    MastersClass objPro = new MastersClass();
    string SessionAdmin;
    static string lblprobation_Common;
    string SessionCcode;
    string SessionLcode;
    protected void Page_Load(object sender, EventArgs e)
    {
        
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        string ss = Session["UserId"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();
        if (!IsPostBack)
        {
            ProbationDisplay();
        }
        //string ss = Session["UserId"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        lblusername.Text = Session["Usernmdisplay"].ToString();
        lblComany.Text = SessionCcode + " - " + SessionLcode;
    }
    public void ProbationDisplay()
    {
        DataTable dt = new DataTable();
        dt = objdata.MstProbationDisplay();
        gvProbation.DataSource = dt;
        gvProbation.DataBind();


    }
    protected void btnsave_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;

        try
        {
            if (txtprobationcd.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Probation Period Code');", true);
                //System.Windows.Forms.MessageBox.Show("Enter the Probationary Period Code", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            else if (txtpropationmonth.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Probation Period Month or Year');", true);
                //System.Windows.Forms.MessageBox.Show("Enter the Probationary Period Month or Year", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            string constr = ConfigurationManager.AppSettings["ConnectionString"];
            SqlConnection con = new SqlConnection(constr);
            SqlCommand cmd = new SqlCommand("Select ProbationMonth from MstProbation Where ProbationMonth='" + txtpropationmonth.Text + "'", con);
            con.Open();
            SqlDataReader sdr = cmd.ExecuteReader();
            if (sdr.HasRows)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Probation Period Month or Year All Ready Registered');", true);
                //System.Windows.Forms.MessageBox.Show("Probationary Period Month or year already Registered", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            else
            {

            }
            con.Close();
            if (!ErrFlag)
            {
                string pid = objdata.Probationid_check(txtprobationcd.Text);
                string Pmonth = objdata.Probationmonth_check(txtpropationmonth.Text);
                if (pid == txtprobationcd.Text)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Probation Period Id All Ready Registered');", true);
                    //System.Windows.Forms.MessageBox.Show("Probationary Period ID already Exist", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    ErrFlag = true;
                }
                if (Pmonth.ToLower() == txtpropationmonth.Text.ToLower())
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Probation Period Month or Year All Ready Registered');", true);
                    //System.Windows.Forms.MessageBox.Show("Probationary Period Month or Year already Registered", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    ErrFlag = true;
                }
                if (!ErrFlag)
                {
                    objPro.ProbationCd = txtprobationcd.Text;
                    objPro.ProbationMonth = txtpropationmonth.Text;
                    objdata.MstProbation(objPro);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Probation Period Save Successfully...');", true);
                    //System.Windows.Forms.MessageBox.Show("Probationary Period Saved Successfully", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    txtprobationcd.Text = "";
                    txtpropationmonth.Text = "";
                    ProbationDisplay();
                }
            }
        }
        catch (Exception ex)
        {
        }
    }
    protected void RowEdit_Canceling(object sender, GridViewCancelEditEventArgs e)
    {
        gvProbation.EditIndex = -1;
        ProbationDisplay();
    }
    protected void RowEdit_Editing(object sender, GridViewEditEventArgs e)
    {
        gvProbation.EditIndex = e.NewEditIndex;
        ProbationDisplay();

    }
    protected void RowEdit_Updating(object sender, GridViewUpdateEventArgs e)
    {
        bool ErrFlag = false;
        TextBox txtprobationmong = (TextBox)gvProbation.Rows[e.RowIndex].FindControl("txtprobationmonth");

        if (txtprobationmong.Text == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Probation Period Month or Year');", true);
            //System.Windows.Forms.MessageBox.Show("Enter the Probationary Period Month or year", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            ErrFlag = true;
        }
        if (!ErrFlag)
        {
            Label lblProbation1 = (Label)gvProbation.Rows[e.RowIndex].FindControl("lblprobationcd");
            string Pmonth = objdata.Probationmonth_check(txtprobationmong.Text);
            if (Pmonth == txtprobationmong.Text)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Probation Period Month or Year All Ready Registered');", true);
                //System.Windows.Forms.MessageBox.Show("Probationan Period Month or Year already Registered", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            if (!ErrFlag)
            {
                objPro.ProbationCd = lblProbation1.Text;
                objPro.ProbationMonth = txtprobationmong.Text;
                objdata.UpdateMstProbation(objPro);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Probation Period Update Successfully....');", true);
                //System.Windows.Forms.MessageBox.Show("Probationary Period Updated Successfully", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                gvProbation.EditIndex = -1;
                ProbationDisplay();
            }
        }
    }
    protected void btncancel_Click(object sender, EventArgs e)
    {
        txtprobationcd.Text = "";
        txtpropationmonth.Text = "";
        ProbationDisplay();
    }
    protected void gvProbation_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        bool ErrFlag = false;
        string constr = ConfigurationManager.AppSettings["ConnectionString"];
        SqlConnection con = new SqlConnection(constr);
        Label lblprobation = (Label)gvProbation.Rows[e.RowIndex].FindControl("lblprobationcd");
        lblprobation_Common = lblprobation.Text;
        string Pdelete = objdata.Probation_delete(lblprobation.Text);
        if (Pdelete == lblprobation.Text)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('It cannot be deleted');", true);
            //System.Windows.Forms.MessageBox.Show("It Cannot be Deleted", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            ErrFlag = true;
        }
        if (!ErrFlag)
        {
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "script", "showConfirm();", true);
            //System.Windows.Forms.DialogResult dres = System.Windows.Forms.MessageBox.Show("Are you Sure want to Delete ?", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.YesNo, System.Windows.Forms.MessageBoxIcon.Warning);
            //if (dres.ToString() == "Yes")
            //{
            //    string qry = "delete from MstProbation where ProbationCd='" + lblprobation.Text + "'";
            //    SqlCommand cmd = new SqlCommand(qry, con);
            //    con.Open();
            //    cmd.ExecuteNonQuery();
            //    con.Close();
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Deleted Successfully');", true);
            //    //System.Windows.Forms.MessageBox.Show("Deleted Successfully", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            //    ProbationDisplay();
            //}
        }
    }

    protected void cmdOverriteGrade_Click(object sender, EventArgs e)
    {
        try
        {
            string constr = ConfigurationManager.AppSettings["ConnectionString"];
            SqlConnection con = new SqlConnection(constr);
            //Label lblprobation = lblprobation_Common.Text;
            string qry = "delete from MstProbation where ProbationCd='" + lblprobation_Common + "'";
            SqlCommand cmd = new SqlCommand(qry, con);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Deleted Successfully');", true);
            //System.Windows.Forms.MessageBox.Show("Deleted Successfully", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            ProbationDisplay();
        }
        catch (Exception ex)
        {
        }
    }

    
}
