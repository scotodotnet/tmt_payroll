﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class MstCompany : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    MastersClass objMas = new MastersClass();
    static string CompanyCd;
    string SessionAdmin;
    protected void Page_Load(object sender, EventArgs e)
    {
        CompanyCd = objdata.CompanyMaxValue();
        txtcompanycd.Text = CompanyCd;

        SessionAdmin = Session["Isadmin"].ToString();
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        string ss = Session["UserId"].ToString();
        if (!IsPostBack)
        {
            CompanyDisplay();
        }
    }
    public void CompanyDisplay()
    {
        DataTable dt = new DataTable();
        dt = objdata.MstCompanyDisplay();
        gvCompany.DataSource = dt;
        gvCompany.DataBind();
       
    }
    protected void Button1_Click(object sender, EventArgs e)
    {

        
        objMas.CompanyNm = txtcompanynm.Text;
        objdata.MstCompany(objMas);

    }
    protected void RowEdit_Canceling(object sender, GridViewCancelEditEventArgs e)
    {
        gvCompany.EditIndex = -1;
        CompanyDisplay();
    }
    protected void RowEdit_Editing(object sender, GridViewEditEventArgs e)
    {
        gvCompany.EditIndex = e.NewEditIndex;
        CompanyDisplay();
    }
    protected void RowEdit_Updating(object sender, GridViewUpdateEventArgs e)
    {
        Label lblcompanycd1 = (Label)gvCompany.Rows[e.RowIndex].FindControl("lblcompanycd");
        TextBox txtcompannm = (TextBox)gvCompany.Rows[e.RowIndex].FindControl("txtcompanynm");
        objMas.CompanyCd = lblcompanycd1.Text;
        objMas.CompanyNm = txtcompannm.Text;
        objdata.UpdateMstCompany(objMas);
        gvCompany.EditIndex = -1;
        CompanyDisplay();
    }
}
