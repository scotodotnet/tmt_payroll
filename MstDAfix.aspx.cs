﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Data.SqlClient;
using System.Configuration;

public partial class MstDAfix : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    ContractClass objcon = new ContractClass();
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        string ss = Session["UserId"].ToString();        
        SessionAdmin = Session["Isadmin"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        lblusername.Text = Session["Usernmdisplay"].ToString();
        lblComany.Text = SessionCcode + " - " + SessionLcode;

        if (!IsPostBack)
        {
            //Retrive_load();
            Load_DAA_and_DA_Value();

        }
    }
    protected void btnEmp_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptEmpDownload.aspx");
    }
    protected void btnatt_Click(object sender, EventArgs e)
    {
        Response.Redirect("AttenanceDownload.aspx");
    }
    protected void btnLeave_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptLeaveSample.aspx");
    }
    protected void btnOT_Click(object sender, EventArgs e)
    {
        Response.Redirect("OTDownload.aspx");
    }
    protected void btncontract_Click(object sender, EventArgs e)
    {
        Response.Redirect("ContractRPT.aspx");
    }
    protected void btnSal_Click(object sender, EventArgs e)
    {
        Response.Redirect("FrmDeductionDownload.aspx");
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            bool ErrFlag = false;
            string query = "";
            if (txtCurrentDA.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Current Month DA Arrears....');", true);
                ErrFlag = true;
            }
            else if (txtPrevDA.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Previous Month DA Arrears....');", true);
                ErrFlag = true;
            }
            else if (txtDAPointone.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the DA Point One....');", true);
                ErrFlag = true;
            }
            else if (txtDAPointtwo.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the DA Point Two....');", true);
                ErrFlag = true;
            }
            if (!ErrFlag)
            { 
                //Get Mutli Point Value
                DataTable DAMuPoint_Dt = new DataTable();
                query = "Select * from MstDAMutliPoint where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                DAMuPoint_Dt = objdata.RptCommonQueryRunDetails(query);
                //DA Arrears Calculation
                Decimal Current_DAA = 0; Decimal Prev_DAA = 0;
                Decimal DAA_Total = 0; Decimal DAA_Total1 = 0; Decimal DAA_M_Point = 0;
                string DAA_Final = "0.0"; string DAA_Total_Final = "0.0"; 
                if (DAMuPoint_Dt.Rows.Count.ToString() != "0") 
                {
                    DAA_M_Point = Convert.ToDecimal(DAMuPoint_Dt.Rows[0]["DAAPointMulti"].ToString()); 
                }
                Current_DAA = Convert.ToDecimal(txtCurrentDA.Text.Trim().ToString());
                Prev_DAA = Convert.ToDecimal(txtPrevDA.Text.Trim().ToString());
                DAA_Total = Current_DAA - Prev_DAA;
                DAA_Total_Final = String.Format(((Math.Round(DAA_Total) == DAA_Total) ? "{0:0}" : "{0:0.00}"), DAA_Total);
                
                DAA_Total1 = (DAA_Total * DAA_M_Point);
                DAA_Final = String.Format(((Math.Round(DAA_Total1) == DAA_Total1) ? "{0:0}" : "{0:0.00}"), DAA_Total1);

                //DA Calculation
                Decimal DA_One = 0; Decimal DA_Two = 0;Decimal DA_Total = 0;
                string DA_Final = "0.0"; string DA_Final_Tot_Cal = "0.0";
                Decimal DA_M_Point_One = 0; Decimal DA_M_Point_Two = 0; Decimal DA_M_Value_Two = 0;
                Decimal DA_M_Cal_One = 0; Decimal DA_M_Cal_Two = 0; Decimal DA_M_Cal_Final = 0;
                if (DAMuPoint_Dt.Rows.Count.ToString() != "0")
                {
                    DA_M_Point_One = Convert.ToDecimal(DAMuPoint_Dt.Rows[0]["DAPointMultone"].ToString());
                    DA_M_Point_Two = Convert.ToDecimal(DAMuPoint_Dt.Rows[0]["DAPointMulttwo"].ToString());
                    DA_M_Value_Two = Convert.ToDecimal(DAMuPoint_Dt.Rows[0]["DAPointMultVal"].ToString());
                }
                DA_One = Convert.ToDecimal(txtDAPointone.Text.Trim().ToString());
                DA_Two = Convert.ToDecimal(txtDAPointtwo.Text.Trim().ToString());
                DA_Total = DA_One - DA_Two;
                DA_M_Cal_One = DA_Total * DA_M_Point_One;
                DA_M_Cal_Two = DA_M_Value_Two * DA_M_Point_Two;
                DA_M_Cal_Final = DA_M_Cal_One + DA_M_Cal_Two;
                DA_Final_Tot_Cal=String.Format(((Math.Round(DA_Total) == DA_Total) ? "{0:0}" : "{0:0.00}"),DA_Total);
                DA_Final = String.Format(((Math.Round(DA_M_Cal_Final) == DA_M_Cal_Final) ? "{0:0}" : "{0:0.00}"), DA_M_Cal_Final);

                //DAA and DA Sava Details
                DataTable DAA_DA_DT = new DataTable();
                query = "Select * from MstDAPointFixed where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                DAA_DA_DT = objdata.RptCommonQueryRunDetails(query);
                if (DAA_DA_DT.Rows.Count.ToString() == "0")
                {
                    //Insert DAA and DA Details
                    query = "Insert into MstDAPointFixed(Ccode,Lcode,CurrentDAA,PrevDAA,DAATotal,DAAFinalValue,DAPointone,DAPointtwo,DATotal,DAFinalValue) Values('" +
                    SessionCcode + "','" + SessionLcode + "','" + txtCurrentDA.Text.Trim().ToString() + "','" + txtPrevDA.Text.Trim().ToString() + "','" +
                    DAA_Total_Final + "','" + DAA_Final + "','" + txtDAPointone.Text.Trim().ToString() + "','" + txtDAPointtwo.Text.Trim().ToString() + "','" +
                    DA_Final_Tot_Cal + "','" + DA_Final + "')";
                    objdata.RptCommonQueryRunDetails(query);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('DA Arrears And DA Inserted Successfully....');", true);
                }
                else
                {
                    query = "Update MstDAPointFixed set CurrentDAA='" + txtCurrentDA.Text.Trim().ToString() + "',PrevDAA='" + txtPrevDA.Text.Trim().ToString() + "'," +
                    "DAATotal='" + DAA_Total_Final + "',DAAFinalValue='" + DAA_Final + "',DAPointone='" + txtDAPointone.Text.Trim().ToString() + "',DAPointtwo='" +
                    txtDAPointtwo.Text.Trim().ToString() + "',DATotal='" + DA_Final_Tot_Cal + "',DAFinalValue='" + DA_Final + "' where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                    objdata.RptCommonQueryRunDetails(query);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('DA Arrears And DA Value Updated Successfully....');", true);
                }
                Load_DAA_and_DA_Value();
            }

        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('DA Save Error....');", true);
        }
    }
    private void Load_DAA_and_DA_Value()
    {
        string query = "";
        DataTable DAA_DA_DT = new DataTable();
        query = "Select * from MstDAPointFixed where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
        DAA_DA_DT = objdata.RptCommonQueryRunDetails(query);
        if (DAA_DA_DT.Rows.Count.ToString() != "0")
        {
            //DAA
            txtCurrentDA.Text = DAA_DA_DT.Rows[0]["CurrentDAA"].ToString();
            txtPrevDA.Text = DAA_DA_DT.Rows[0]["PrevDAA"].ToString();
            txtDAATotal.Text = DAA_DA_DT.Rows[0]["DAATotal"].ToString();
            //DA
            txtDAPointone.Text = DAA_DA_DT.Rows[0]["DAPointone"].ToString();
            txtDAPointtwo.Text = DAA_DA_DT.Rows[0]["DAPointtwo"].ToString();
            txtDATotal.Text = DAA_DA_DT.Rows[0]["DATotal"].ToString();
        }
        else
        {
            //DAA
            txtCurrentDA.Text = "".ToString();
            txtPrevDA.Text = "".ToString();
            txtDAATotal.Text = "".ToString();
            ChkChangeDA.Checked = false;
            //DA
            txtDAPointone.Text = "".ToString();
            txtDAPointtwo.Text = "".ToString();
            txtDATotal.Text = "".ToString();
        }
    }
    protected void ChkChangeDA_CheckedChanged(object sender, EventArgs e)
    {
        if (ChkChangeDA.Checked == true)
        {
            if (txtCurrentDA.Text.Trim().ToString() != "")
            {
                txtPrevDA.Text = txtCurrentDA.Text.Trim().ToString();
                txtCurrentDA.Text = "".ToString();
            }
        }
        else
        {
            Load_DAA_and_DA_Value();
        }
    }
}
