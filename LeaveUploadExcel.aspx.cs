﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Data.Common;
using System.Data;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using System.Configuration;
using System.Data.SqlClient;
using System.Data.OleDb;
using Payroll;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class LeaveUploadExcel : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    int tempExcess;
    int LeavdaysCount;
    string LeaveDaysCount1;
    string SessionCcode;
    string SessionLcode;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Department();
            int currentYear = Utility.GetFinancialYear;
            for (int i = 0; i < 10; i++)
            {
                ddlFinaYear.Items.Add(new ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                //  ddlShowYear.Items.Add(new ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                currentYear = currentYear - 1;
            }
        }

    }
    public void Department()
    {
        DataTable dtDip = new DataTable();
        dtDip = objdata.DropDownDepartment();
        ddldepartment.DataSource = dtDip;
        ddldepartment.DataTextField = "DepartmentNm";
        ddldepartment.DataValueField = "DepartmentCd";
        ddldepartment.DataBind();
    }
    protected void btnfileupload_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        string tempfromDate = "";
        string temptodate = "";
        DateTime DfromDate;
        DateTime DtoDate;
        DateTime ApprovedDate;
        ApprovedDate = new DateTime();
        DfromDate = new DateTime();
        DtoDate = new DateTime();
        string LeaveCode = "";
        string AllocatedLeave = "";
        string noofLeave = "";
        string ExcessLeave = "";
        string Maternalleave = "";

        bool CheckFlag = false;
        try
        {
            //string txt = Convert.ToString(fileUpload.FileName);
            if (fileUpload.HasFile)
            {
                fileUpload.SaveAs(Server.MapPath("Upload/" + fileUpload.FileName));
            }
            //string mypath = "'C:/Users/RaguramVasanth/Documents/Work/Working/Nov06/Payroll03_dec_New/UploadLeave/Leave.xls";
            //fileUpload.SaveAs(Server.MapPath("Upload/" + mypath));
            string connectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + Server.MapPath("Upload/" + fileUpload.FileName) + ";" + "Extended Properties=Excel 8.0;";

                OleDbConnection sSourceConnection = new OleDbConnection(connectionString);
                DataTable dts = new DataTable();

                using (sSourceConnection)
                {
                    sSourceConnection.Open();

                    OleDbCommand command = new OleDbCommand("Select EmpNo,LeaveType,NumberofLeave,TotalWorkingDays,FromDate,ToDate,Month,Year,Approvededby,Approveddate FROM [Sheet1$];", sSourceConnection);
                    sSourceConnection.Close();


                    using (OleDbCommand cmd = sSourceConnection.CreateCommand())
                    {
                        command.CommandText = "Select EmpNo,LeaveType,NumberofLeave,TotalWorkingDays,FromDate,ToDate,Month,Year,ExcessLeave,Approvededby,Approveddate FROM [Sheet1$];";
                        sSourceConnection.Open();
                    }
                    using (OleDbDataReader dr = command.ExecuteReader())
                    {

                        if (dr.HasRows)
                        {

                        }

                    }
                    OleDbDataAdapter objDataAdapter = new OleDbDataAdapter();
                    objDataAdapter.SelectCommand = command;
                    DataSet ds = new DataSet();

                    objDataAdapter.Fill(ds);
                    DataTable dt = new DataTable();
                    dt = ds.Tables[0];
                    for (int i = 0; i < dt.Columns.Count; i++)
                    {
                        dt.Columns[i].ColumnName = dt.Columns[i].ColumnName.Replace(" ", string.Empty).ToString();

                    }
                    string constr = ConfigurationManager.AppSettings["ConnectionString"];
                    for (int j = 0; j < dt.Rows.Count; j++)
                    {
                        SqlConnection cn = new SqlConnection(constr);
                        string EmpNo = dt.Rows[j]["EmpNo"].ToString();
                        string LeaveType = dt.Rows[j]["LeaveType"].ToString();
                        string NumberofLeave = dt.Rows[j]["NumberofLeave"].ToString();
                        string TotalWorkingDays = dt.Rows[j]["TotalWorkingDays"].ToString();
                        string FromDate = dt.Rows[j]["FromDate"].ToString();
                        string ToDate = dt.Rows[j]["ToDate"].ToString();
                        string Month = dt.Rows[j]["Month"].ToString();
                        string Year = dt.Rows[j]["Year"].ToString();
                        
                        string Approvededby = dt.Rows[j]["Approvededby"].ToString();
                        string Approveddate = dt.Rows[j]["Approveddate"].ToString();

                        if (EmpNo == "")
                        {
                            j = j + 2;
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Employee Number. The Row Number is " + j + "');", true);
                            ErrFlag = true;
                        }
                        else if (LeaveType == "")
                        {
                            j = j + 2;
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter LeaveType Number. The Row Number is " + j + " ');", true);
                            ErrFlag = true;
                        }
                        else if (NumberofLeave == "")
                        {
                            j = j + 2;
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Number of Leave. The Row Number is " + j + "');", true);
                            ErrFlag = true;

                        }
                        else if (Convert.ToInt32(NumberofLeave) == 0)
                        {
                            j = j + 2;
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Number of Leave. The Row Number is " + j + "');", true);
                            ErrFlag = true;

                        }

                        else if (TotalWorkingDays == "")
                        {
                            j = j + 2;
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Total WorkingDays. The Row Number is " + j + "');", true);
                            ErrFlag = true;
                        }
                        else if (Convert.ToInt32(TotalWorkingDays) == 0)
                        {
                            j = j + 2;
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Total WorkingDays. The Row Number is " + j + "');", true);
                            ErrFlag = true;
                        }
                        else if (FromDate == "")
                        {
                            j = j + 2;
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter From Date. The Row Number is " + j + "');", true);
                            ErrFlag = true;
                        }
                        else if (ToDate == "")
                        {
                            j = j + 2;
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter To Date. The Row Number is " + j + "');", true);
                            ErrFlag = true;
                        }
                        else if (Month == "")
                        {
                            j = j + 2;
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Month. The Row Number is " + j + "');", true);
                            ErrFlag = true;
                        }
                        else if (Year == "")
                        {
                            j = j + 2;
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Year. The Row Number is " + j + "');", true);
                            ErrFlag = true;
                        }
                       
                        else if (ExcessLeave == "")
                        {
                            j = j + 2;
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Excess Leave. The Row Number is " + j + "');", true);
                            ErrFlag = true;
                        }
                       
                        else if (Approvededby == "")
                        {
                            j = j + 2;
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Approveded by. The Row Number is " + j + "');", true);
                            ErrFlag = true;
                        }
                        else if (Approveddate == "")
                        {
                            j = j + 2;
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Approved Date. The Row Number is " + j + "');", true);
                            ErrFlag = true;
                        }
                        DfromDate = DateTime.ParseExact(FromDate, "dd/MM/yyyy", null);
                        DtoDate = DateTime.ParseExact(ToDate, "dd/MM/yyyy", null);
                        if (DfromDate > DtoDate)
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Year is incorrect Enter Correct Value. The Row Number is " + j + "');", true);
                            ErrFlag = true;
                        }

                        cn.Open();
                        string Query = "Select LeaveType from MstLeave where LeaveType='" + LeaveType + "'";
                        SqlCommand cmd = new SqlCommand(Query, cn);
                        SqlDataReader sdr = cmd.ExecuteReader();
                        if (sdr.HasRows)
                        {


                        }
                        else
                        {
                            j = j + 2;
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('This Leave Type Not Availabe in Leave type Master.The Row Number is " + j + " ');", true);
                            ErrFlag = true;

                        }
                        cn.Close();
                        cn.Open();
                        string Query1 = "Select EmpNo from EmployeeDetails where EmpNo='" + EmpNo + "' and Status = 'O'";
                        SqlCommand cmd1 = new SqlCommand(Query1, cn);
                        SqlDataReader sdr1 = cmd1.ExecuteReader();
                        if (sdr1.HasRows)
                        {


                        }
                        else
                        {
                            j = j + 2;
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('This Employee Number not Available.The Row Number is " + j + " ');", true);
                            ErrFlag = true;

                        }
                        cn.Close();

                        
                        DataTable dtchek = new DataTable();
                        dtchek = objdata.AllReadyApplyForLeaveDate(EmpNo, DfromDate, DtoDate, ddlFinaYear.SelectedValue, SessionCcode, SessionLcode);
                        if (dtchek.Rows.Count > 0)
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('You All Ready Apply to Leave From " + dtchek.Rows[0]["FromDate"].ToString() + " To " + dtchek.Rows[0]["ToDate"].ToString() + " ');", true);
                            ErrFlag = true;
                        }
                        string qry_gender = "select case Gender when '1' then 'male' else 'female' end as Gender from EmployeeDetails where EmpNo='" + EmpNo + "'";
                        SqlCommand cmd_g = new SqlCommand(qry_gender, cn);
                        cn.Open();
                        string gen = Convert.ToString(cmd_g.ExecuteScalar());
                        cn.Close();
                        if (gen == "male")
                        {
                            if (LeaveType == "Maternity Leave")
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('You cannot Apply Maternity leave for this Employee " + EmpNo + " ');", true);
                                ErrFlag = true;
                            }
                        }

                    }



                    if (!ErrFlag)
                    {

                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            DateTime EnteredDate;
                            SqlConnection cn = new SqlConnection(constr);
                            cn.Open();
                            string Query_leavetype = "Select LeaveCd from MstLeave where LeaveType='" + dt.Rows[i]["LeaveType"].ToString() + "'";
                            SqlCommand cmd_LT = new SqlCommand(Query_leavetype, cn);
                            LeaveCode = Convert.ToString(cmd_LT.ExecuteScalar());

                            string QueryAllocated = "Select AllocatedLeave from LeaveAllocation where EmpNo = '" + dt.Rows[i]["EmpNo"].ToString() + "'";
                            SqlCommand cmd_AL = new SqlCommand(QueryAllocated, cn);
                            AllocatedLeave = Convert.ToString(cmd_AL.ExecuteScalar());
                            if ((AllocatedLeave == "") || (AllocatedLeave == null))
                            {
                                AllocatedLeave = "0";
                            }
                            string Query_Old = "Select Sum(NumberofLeave) as NumberofLeave from LeaveDetails where EmpNo = '" + dt.Rows[i]["EmpNo"].ToString() + "' and FinancialYear = '" + ddlFinaYear.SelectedValue + "' and LeaveType != '3'";
                            SqlCommand cmd_old = new SqlCommand(Query_Old, cn);
                            noofLeave = Convert.ToString(cmd_old.ExecuteScalar());
                            if ((noofLeave == "") || (noofLeave == null))
                            {
                                noofLeave = "0";
                            }
                            tempExcess = (Convert.ToInt32(AllocatedLeave) - Convert.ToInt32(noofLeave));
                            if (tempExcess >= 0)
                            {
                                ExcessLeave = Convert.ToString(tempExcess);
                            }
                            else
                            {
                                ExcessLeave = Convert.ToString((tempExcess * (-1)));
                            }
                            if (dt.Rows[i]["LeaveType"].ToString() == "Maternity Leave")
                            {
                                Maternalleave = "Y";
                            }
                            else
                            {
                                Maternalleave = "N";
                            }
                            DataTable dtleave = new DataTable();
                            dtleave = objdata.AlreadyLeaveDateLoad(dt.Rows[i]["EmpNo"].ToString(), ddlFinaYear.SelectedValue, SessionCcode, SessionLcode);
                            if (dtleave.Rows.Count > 0)
                            {
                                for (int k = 0; i < dtleave.Rows.Count; i++)
                                {
                                    tempfromDate = dtleave.Rows[k]["FromDate"].ToString();
                                    temptodate = dtleave.Rows[k]["ToDate"].ToString();
                                    for (DateTime m = Convert.ToDateTime(tempfromDate); m <= Convert.ToDateTime(temptodate); )
                                    {
                                        if (m == Convert.ToDateTime(tempfromDate) || (m == Convert.ToDateTime(temptodate)))
                                        {
                                            CheckFlag = true;
                                        }
                                        else
                                        {
                                            
                                        }
                                        m = m.AddDays(1);
                                    }

                                }
                                LeavdaysCount = Convert.ToInt32(DtoDate - DfromDate);
                                LeavdaysCount = LeavdaysCount + 1;
                            }
                            if (CheckFlag == false)
                            {
                                string temp_approved = Convert.ToDateTime(dt.Rows[i]["Approveddate"].ToString()).ToShortDateString();
                                ApprovedDate = DateTime.ParseExact(temp_approved, "dd/MM/yyyy", null);
                                string Leavecode_temp = objdata.LeaveCode();
                                objdata.ApplyForLeave(dt.Rows[i]["EmpNo"].ToString(), LeaveCode, LeavdaysCount.ToString(), dt.Rows[i]["TotalWorkingDays"].ToString(), Leavecode_temp, DfromDate, DfromDate, dt.Rows[i]["Month"].ToString(), dt.Rows[i]["Year"].ToString(), AllocatedLeave, noofLeave, ExcessLeave, dt.Rows[i]["Approvededby"].ToString(), ApprovedDate, ddlFinaYear.SelectedValue, Maternalleave, SessionCcode, SessionLcode);
                            }
                            else
                            {
                            }

                        }

                        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert(' Upload Successfully...');", true);
                        //txtsheetname.Text = " ";
                        //Response.Redirect("UploadItems.aspx");

                    }
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Your File Not Upload...');", true);

                }
            //}
        }
        catch (Exception ex)
        {
            //Response.Write(ex.ToString());
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Please Upload Correct File Format...');", true);
        }
    }
    
}
