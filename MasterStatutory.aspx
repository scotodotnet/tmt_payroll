﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="MasterStatutory.aspx.cs" Inherits="MasterStatutory" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<!-- Mirrored from www.ait.sk/uniadmin/ by HTTrack Website Copier/3.x [XR&CO'2010], Tue, 20 Jul 2010 00:37:25 GMT -->
<!--   Added by HTTrack -->
<meta http-equiv="content-type" content="text/html;charset=UTF-8">
<!-- /Added by HTTrack -->
<head id="hea" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <meta name="robots" content="ALL,FOLLOW" />
    <meta name="Author" content="AIT" />
    <meta http-equiv="imagetoolbar" content="no" />
    <title>Payroll Management Systems</title>
    <link rel="stylesheet" href="css/reset.css" type="text/css" />
    <link rel="stylesheet" href="css/screen.css" type="text/css" />
    <link rel="stylesheet" href="css/fancybox.css" type="text/css" />
    <link rel="stylesheet" href="css/jquery.wysiwyg.css" type="text/css" />
    <link rel="stylesheet" href="css/jquery.ui.css" type="text/css" />
    <link rel="stylesheet" href="css/visualize.css" type="text/css" />
    <link rel="stylesheet" href="css/visualize-light.css" type="text/css" />
    <link rel="Stylesheet" href="css/form.css" type="text/css" />
    <link rel="Stylesheet" href="css/orange.css" type="text/css" />
    <!--[if IE 7]>
	<link rel="stylesheet" type="text/css" href="css/ie7.css" />
<![endif]-->

    <script type="text/javascript" src="js/jquery.js"></script>

    <script type="text/javascript" src="js/jquery.visualize.js"></script>

    <script type="text/javascript" src="js/jquery.wysiwyg.js"></script>

    <script type="text/javascript" src="js/tiny_mce/jquery.tinymce.js"></script>

    <script type="text/javascript" src="js/jquery.fancybox.js"></script>

    <script type="text/javascript" src="js/jquery.idtabs.js"></script>

    <script type="text/javascript" src="js/jquery.datatables.js"></script>

    <script type="text/javascript" src="js/jquery.jeditable.js"></script>

    <script type="text/javascript" src="js/jquery.ui.js"></script>

    <script type="text/javascript" src="js/excanvas.js"></script>

    <script type="text/javascript" src="js/cufon.js"></script>

    <script type="text/javascript" src="js/Geometr231_Hv_BT_400.font.js"></script>

    <script type="text/javascript" src="js/script.js"></script>

<script type ="text/jscript" >
        function GoBack() {
            window.history.forward();
        }
   </script>
</head>
<body onload="GoBack();">
    <div class="clear">
        <!-- Start Clear-->
        <div class="sidebar">
            <!-- *** sidebar layout *** -->
            <div class="logo clear">
               <img src="images/altius_ForDarkBackGround.png" alt="" class="picture" width="140" height="60" />
            </div>
            <%--<div class="page clear">--%>
            <div class="Elaya">
                <ul>
                
                     <li><a href="AllHome.aspx">Home</a></li>
                    <li><a href="PFForm5.aspx">PF Form 5</a></li>
                    <li><a href="FOrm10.aspx">Form 10</a></li>
                    <li><a href="PFForm3Anew.aspx">PF Form 3A</a></li>
                    <li><a href="PFform6A.aspx">PF Form 6A</a></li>
                    <li><a href="ESIForm7.aspx">ESI Form 7</a></li>
                    <li><a href="Default.aspx">Logout</a></li>
                    
                </ul>
            </div>
            <%--</div>--%>
        </div>
        <div class="main">
            <!-- Start Main -->
            <!-- *** mainpage layout *** -->
            <div class="main-wrap">
                <!-- Start Main Warp -->
                <div class="header clear">
                    <ul class="links clear">
                        <li></li>
                        <li>
                            <h1>
                                <span class="title">Payroll Management System</span></h1>
                        </li>
                    </ul>
                </div>
                <!-- *** User Name layout *** -->
                <div class="header1 clear">
                    <ul class="links1 clear">
                        <li>
                            <h4>
                                <a href="default.aspx">Logout</a></h4>
                        </li>
                    </ul>
                </div>
               

                    <!-- MODAL WINDOW -->
                    <div id="modal" class="modal-window">
                        <!-- <div class="modal-head clear"><a onclick="$.fancybox.close();" href="javascript:;" class="close-modal">Close</a></div> -->
                        
                    </div>
                    <!-- CONTENT BOXES -->
                    <div class="content-box">
                        <div class="box-header clear">
                            <%--	<ul class="tabs clear">
						<li><a href="#data-table"></a></li>
						<li><a href="#table"></a></li>
						<li><a href="#forms"></a></li>
					</ul>--%>
                        </div>
                        <div class="box-body clear">
                            <!-- TABLE -->
                            <form id="form1" runat="server" class="form">
                            <cc1:ToolkitScriptManager runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true"
                                ID="ScriptManager1" EnablePartialRendering="true">
                            </cc1:ToolkitScriptManager>
                            <div id="homepage" class="main_content">
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
                                
                                <table>
     <tr align="center">
   <td colspan="1">
    <div class="page clear">
                    <!-- Start page Clear -->
                    <div class="main-icons clear">
                        <ul class="links clear">
                            <li><a href="PFForm5.aspx" ><img src="images/reports01.png" class="icon" alt="" /><span class="text">PF Form 5</span></a></li>
				
				<li><a href="FOrm10.aspx"><img src="images/reports02.png" class="icon" alt="" /><span class="text">Form 10</span></a></li>
				
				<li><a href="PFForm3Anew.aspx"><img src="images/reports03.png" class="icon" alt="" /><span class="text">PF Form 3A</span></a></li>
				<li><a href="PFform6A.aspx"><img src="images/User group.png" class="icon" alt="" /><span class="text">PF Form 6A</span></a></li>
				
				<li><a href="ESIForm7.aspx"><img src="images/UserCP.jpg" class="icon" alt="" /><span class="text">ESI Form 7</span></a></li>
                        </ul>
                        </div>
   </td>
   </tr>
    </table>
    
    </ContentTemplate>
     </asp:UpdatePanel>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- end of page Clear -->
                <!-- end of page -->
                <div class="footer clear">
                    <span class="copy"><strong>© 2012 Copyright by <a href="http://www.altius.co.in" />Altius
                        Infosystems.</strong></span> Powered by <a href="http://www.altius.co.in/">Altius.</a>
                </div>
            </div>
            <!-- end of MainWarp -->
        </div>
        <!-- end of Main -->
    </div>
    <!-- end of Clear -->

    <script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-12958851-7']);
        _gaq.push(['_trackPageview']);

        (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();

    </script>

</body>
<!-- Mirrored from www.ait.sk/uniadmin/ by HTTrack Website Copier/3.x [XR&CO'2010], Tue, 20 Jul 2010 00:38:01 GMT -->
<!-- Added by HTTrack -->
<meta http-equiv="content-type" content="text/html;charset=UTF-8">
<!-- /Added by HTTrack -->
</html>

