﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using Payroll;
using Payroll.Data;
using Payroll.Configuration;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html;
using iTextSharp.text.html.simpleparser;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class PaySlip_gen1 : System.Web.UI.Page
{
    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    SqlConnection con;
    BALDataAccess objdata = new BALDataAccess();
    DateTime TransDate;
    string SessionAdmin;
    string Stafflabour;
    string SessionCcode;
    string SessionLcode;
    string NetBase;
    string NetFDA;
    string NetVDA;
    string NetHRA;
    string Nettotal;
    string NetPFEarnings;
    string NetPF;
    string NetESI;
    string NetUnion;
    string NetAdvance;
    string NetAll1;
    string NetAll2;
    string NetAll3;
    string NetAll4;
    string NetAll5;
    string NetDed1;
    string NetDed2;
    string NetDed3;
    string NetDed4;
    string NetDed5;
    string HomeDays;
    string Halfnight;
    string FullNight;
    string DayIncentive;
    string Spinning;
    string ThreeSided;
    string NetLOP;
    string NetStamp;
    string NetTotalDeduction;
    string NetOT;
    string NetAmt;
    string Network;
    string totNFh;
    string totweekoff;
    string totCL;
    string totwork;
    string Roundoff;
    DateTime MyDate;
    DateTime MyDate1;
    DateTime MyDate2;
    static decimal AdvAmt;
    static string ID;
    static string Adv_id = "";
    static string Dec_mont = "0";
    static string Adv_BalanceAmt = "0";
    static string Adv_due = "0";
    static string Increment_mont = "0";
    static decimal val;
    static string EmployeeDays = "0";
    string MyMonth;
    static string cl = "0";
    string TempDate;
    string Slap_Query = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        con = new SqlConnection(constr);
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionAdmin = Session["Isadmin"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        lblComany.Text = SessionCcode + " - " + SessionLcode;
        lblusername.Text = Session["Usernmdisplay"].ToString();
        string ss = Session["UserId"].ToString();
        if (!IsPostBack)
        {
            DropDwonCategory();
            Months_load();
            int currentYear = Utility.GetFinancialYear;
            for (int i = 0; i < 10; i++)
            {
                ddlFinance.Items.Add(new System.Web.UI.WebControls.ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                //  ddlShowYear.Items.Add(new ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                currentYear = currentYear - 1;
            }
        }
    }
    public void DropDwonCategory()
    {
        DataTable dtcate = new DataTable();
        dtcate = objdata.DropDownCategory();
        ddlcategory.DataSource = dtcate;
        ddlcategory.DataTextField = "CategoryName";
        ddlcategory.DataValueField = "CategoryCd";
        ddlcategory.DataBind();
    }
    public void Months_load()
    {
        DataTable dt = new DataTable();
        dt = objdata.months_load();
        ddlMonths.DataSource = dt;
        ddlMonths.DataTextField = "Months";
        ddlMonths.DataValueField = "Months";
        ddlMonths.DataBind();
    }
    public void EmployeeType()
    {
        DataTable dtemp = new DataTable();
        dtemp = objdata.EmployeeDropDown_no(ddlcategory.SelectedValue);
        ddldept.DataSource = dtemp;
        ddldept.DataTextField = "EmpType";
        ddldept.DataValueField = "EmpTypeCd";
        ddldept.DataBind();
    }
    protected void ddlcategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable dtempty = new DataTable();
        ddldept.DataSource = dtempty;
        ddldept.DataBind();
        
        if (ddlcategory.SelectedValue == "1")
        {
            Stafflabour = "S";
        }
        else if (ddlcategory.SelectedValue == "2")
        {
            Stafflabour = "L";
        }
        else
        {
            Stafflabour = "0";
        }
        EmployeeType();
    }
    protected void ddldept_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {

        try
        {
            bool ErrFlag = false;
            string CmpName = "";
            string Cmpaddress = "";
            int YR = 0;
            if (ddlMonths.SelectedValue == "January")
            {
                YR = Convert.ToInt32(ddlFinance.SelectedValue);
                YR = YR + 1;
            }
            else if (ddlMonths.SelectedValue == "February")
            {
                YR = Convert.ToInt32(ddlFinance.SelectedValue);
                YR = YR + 1;
            }
            else if (ddlMonths.SelectedValue == "March")
            {
                YR = Convert.ToInt32(ddlFinance.SelectedValue);
                YR = YR + 1;
            }
            else
            {
                YR = Convert.ToInt32(ddlFinance.SelectedValue);
            }

            if (ddlcategory.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select the Category');", true);
                ErrFlag = true;
            }
            //else if ((txtEmployeeType.SelectedValue == "") || (txtEmployeeType.SelectedValue == "0"))
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select the Employee Type');", true);
            //    ErrFlag = true;
            //}

            else if ((ddlMonths.SelectedValue == "0") || (ddlMonths.SelectedValue == ""))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select the Months');", true);
                ErrFlag = true;
            }


            if (!ErrFlag)
            {
                //if (ddldept.SelectedValue == "0")
                //{
                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select the Department Properly');", true);
                //    ErrFlag = true;
                //}
                if (!ErrFlag)
                {
                    if (ddlcategory.SelectedValue == "1")
                    {
                        Stafflabour = "S";
                    }
                    else if (ddlcategory.SelectedValue == "2")
                    {
                        Stafflabour = "L";
                    }
                    string constr = ConfigurationManager.AppSettings["ConnectionString"];
                    SqlConnection con = new SqlConnection(constr);
                    //foreach (GridViewRow gvsal in gvSalary.Rows)
                    //{
                    //    Label lblEmpNo = (Label)gvsal.FindControl("lblExist");
                    //    string qry = "Update SalaryDetails set Process_Mode='" + 1 + "' where Month='" + ddlMonths.SelectedValue + "' and FinancialYear='" + ddlFinance.SelectedValue + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and ExisistingCode='" + lblEmpNo.Text + "'";
                    //    SqlCommand cmd = new SqlCommand(qry, con);
                    //    con.Open();
                    //    cmd.ExecuteNonQuery();
                    //    con.Close();
                    ////}
                    //string Emp_ESI_Code = "";//ddESI.SelectedValue.ToString();

                    //string Payslip_Format_Type = rdbPayslipFormat.SelectedValue.ToString();

                    //string Str_PFType = RdbPFNonPF.SelectedValue.ToString();
                    //string Str_ChkLeft = "";
                    //Str_ChkLeft = ChkLeft.SelectedValue.ToString();
                    //if (ChkLeft.Checked == true)
                    //{
                    //    Str_ChkLeft = "1";
                    //}
                    //else { Str_ChkLeft = "0"; }

                    ResponseHelper.Redirect("ViewReport.aspx?Cate=" + Stafflabour + "&Depat=" + ddldept.SelectedValue + "&Months=" + ddlMonths.SelectedValue + "&yr=" + ddlFinance.SelectedValue + "&fromdate=" + txtfrom.Text + "&ToDate=" + txtTo.Text + "&Salary=" + rbsalary.SelectedValue  +  "&EmpType=" + ddldept.SelectedValue.ToString()  + "&PFTypePost="  + "&Left_Emp="  + "&Report_Type=Payslip", "_blank", "");

                }
            }
        }
        catch (Exception ex)
        {
        }

        //try
        //{
        //    bool ErrFlag = false;
        //    string CmpName = "";
        //    string Cmpaddress = "";
        //    int YR = 0;
        //    if (ddlMonths.SelectedValue == "January")
        //    {
        //        YR = Convert.ToInt32(ddlFinance.SelectedValue);
        //        YR = YR + 1;
        //    }
        //    else if (ddlMonths.SelectedValue == "February")
        //    {
        //        YR = Convert.ToInt32(ddlFinance.SelectedValue);
        //        YR = YR + 1;
        //    }
        //    else if (ddlMonths.SelectedValue == "March")
        //    {
        //        YR = Convert.ToInt32(ddlFinance.SelectedValue);
        //        YR = YR + 1;
        //    }
        //    else
        //    {
        //        YR = Convert.ToInt32(ddlFinance.SelectedValue);
        //    }

        //    if (ddlcategory.SelectedValue == "0")
        //    {
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select the Category');", true);
        //        ErrFlag = true;
        //    }
        //    else if ((ddldept.SelectedValue == "") || (ddldept.SelectedValue == "0"))
        //    {
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select the Employee Type');", true);
        //        ErrFlag = true;
        //    }
        //    else if ((ddlMonths.SelectedValue == "0") || (ddlMonths.SelectedValue == ""))
        //    {
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select the Months');", true);
        //        ErrFlag = true;
        //    }
        //    if (!ErrFlag)
        //    {
        //        if (ddldept.SelectedValue == "0")
        //        {
        //            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select the Department Properly');", true);
        //            ErrFlag = true;
        //        }
        //        if (!ErrFlag)
        //        {
        //            if (ddlcategory.SelectedValue == "1")
        //            {
        //                Stafflabour = "S";
        //            }
        //            else if (ddlcategory.SelectedValue == "2")
        //            {
        //                Stafflabour = "L";
        //            }
        //            string constr = ConfigurationManager.AppSettings["ConnectionString"];
        //            SqlConnection con = new SqlConnection(constr);
        //            foreach (GridViewRow gvsal in gvSalary.Rows)
        //            {
        //                Label lblEmpNo = (Label)gvsal.FindControl("lblExist");
        //                string qry = "Update SalaryDetails set Process_Mode='" + 1 + "' where Month='" + ddlMonths.SelectedValue + "' and FinancialYear='" + ddlFinance.SelectedValue + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and ExisistingCode='" + lblEmpNo.Text + "'";
        //                SqlCommand cmd = new SqlCommand(qry, con);
        //                con.Open();
        //                cmd.ExecuteNonQuery();
        //                con.Close();
        //            }

        //            ResponseHelper.Redirect("ViewReport.aspx?Cate=" + Stafflabour + "&Depat=" + ddldept.SelectedValue + "&Months=" + ddlMonths.SelectedValue + "&yr=" + ddlFinance.SelectedValue + "&fromdate=" + txtfrom.Text + "&ToDate=" + txtTo.Text + "&Salary=" + rbsalary.SelectedValue, "_blank", "");



        //        }
        //    }
        //}
        //catch (Exception ex)
        //{
        //}
    }
    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }
    protected void btnEmp_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptEmpDownload.aspx");
    }
    protected void btnatt_Click(object sender, EventArgs e)
    {
        Response.Redirect("AttenanceDownload.aspx");
    }
    protected void btnLeave_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptLeaveSample.aspx");
    }
    protected void btnOT_Click(object sender, EventArgs e)
    {
        Response.Redirect("OTDownload.aspx");
    }


    protected void btnOther_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        if (rbsalary.SelectedValue == "2")
        {
            if (ddlMonths.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Month Properly');", true);
                ErrFlag = true;
            }
        }
        else
        {
            if (txtfrom.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Month Properly');", true);
                ErrFlag = true;
            }
            else if (txtTo.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Month Properly');", true);
                ErrFlag = true;
            }
            if (!ErrFlag)
            {
                DateTime dfrom = Convert.ToDateTime(txtfrom.Text);
                DateTime dtto = Convert.ToDateTime(txtTo.Text);
                MyDate1 = DateTime.ParseExact(txtfrom.Text, "dd-MM-yyyy", null);
                MyDate2 = DateTime.ParseExact(txtTo.Text, "dd-MM-yyyy", null);
                if (dtto < dfrom)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the From Date...');", true);
                    txtfrom.Text = null;
                    txtTo.Text = null;
                    ErrFlag = true;
                }
            }
        }
        if ((ddlcategory.SelectedValue == "") || (ddlcategory.SelectedValue == "0"))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Category Properly');", true);
            ErrFlag = true;
        }
        else if ((ddldept.SelectedValue == "") || (ddldept.SelectedValue == "0"))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Employee Type Properly');", true);
            ErrFlag = true;
        }


        if (!ErrFlag)
        {
            string CmpName = "";
            string Cmpaddress = "";
            int YR = 0;
            string query = "";
            if (ddlMonths.SelectedValue == "January")
            {
                YR = Convert.ToInt32(ddlFinance.SelectedValue);
                YR = YR + 1;
            }
            else if (ddlMonths.SelectedValue == "February")
            {
                YR = Convert.ToInt32(ddlFinance.SelectedValue);
                YR = YR + 1;
            }
            else if (ddlMonths.SelectedValue == "March")
            {
                YR = Convert.ToInt32(ddlFinance.SelectedValue);
                YR = YR + 1;
            }
            else
            {
                YR = Convert.ToInt32(ddlFinance.SelectedValue);
            }
            if (ddlcategory.SelectedValue == "1")
            {
                Stafflabour = "S";
            }
            else if (ddlcategory.SelectedValue == "2")
            {
                Stafflabour = "L";
            }
            if (ddldept.SelectedValue == "0")
            {
                if (rbsalary.SelectedValue == "2")
                {
                    query = "Select ''as Remaining_Days,SalDet.EmpNo,EmpDet.ExisistingCode,EmpDet.EmpName,EmpDet.Designation,EmpDet.Department,SalDet.Totalworkingdays, " +
                            " SalDet.NFh,SalDet.BasicandDA,SalDet.FDA,SalDet.VDA,SalDet.HRA, SalDet.Advance,SalDet.Stamp,SalDet.Unioncg,(SalDet.WorkedDays + SalDet.NFh) as tot, " +
                            " SalDet.Words,SalDet.WorkedDays,SalDet.Month,SalDet.Year,SalDet.LeaveDays,SalDet.SalaryDate,SalDet.allowances1," +
                            " SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.allowances5,SalDet.Deduction1,SalDet.Weekoff,SalDet.CL,SalDet.Deduction2, " +
                            " SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5,SalDet.Losspay,SalDet.HomeDays,SalDet.NetPay,SalDet.TotalDeductions,SalDet.GrossEarnings, " +
                            " SalDet.OverTime,MstDpt.DepartmentNm,SalDet.ProvidentFund,SalDet.ESI,SalDet.HRA,SalDet.WorkedDays,sum(SalDet.NetPay - (SalDet.GrossEarnings - SalDet.TotalDeductions)) as Roundoff," +
                            " SalDet.PfSalary as PFEarnings, SalDet.HalfNightAmt,SalDet.FullNightAmt,SalDet.SpinningAmt,SalDet.DayIncentive,SalDet.ThreesidedAmt,SM.Base from EmployeeDetails EmpDet inner Join SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo " +
                            " inner Join MstDepartment as MstDpt on MstDpt.DepartmentCd = EmpDet.Department inner Join OfficialProfile OP on OP.EmpNo=SalDet.EmpNo inner Join SalaryMaster SM on SM.EmpNo=SalDet.EmpNo where " +
                            " SalDet.Month='" + ddlMonths.SelectedValue + "' AND SalDet.FinancialYear='" + ddlFinance.SelectedValue + "' and EmpDet.StafforLabor='" + Stafflabour + "' and " +
                            " EmpDet.Ccode='" + SessionCcode + "' and EmpDet.Lcode='" + SessionLcode + "' and OP.WagesType='" + rbsalary.SelectedValue + "' group by SalDet.EmpNo,EmpDet.ExisistingCode,EmpDet.EmpName,EmpDet.Designation,EmpDet.Department,SalDet.Totalworkingdays, " +
                            "SalDet.NFh,SalDet.BasicandDA,SalDet.FDA,SalDet.VDA,SalDet.HRA, SalDet.Advance,SalDet.Stamp,SalDet.Unioncg,SalDet.Words,SalDet.WorkedDays,SalDet.Month,SalDet.Year,SalDet.LeaveDays,SalDet.SalaryDate,SalDet.allowances1, " +
                            " SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.allowances5,SalDet.HomeDays,SalDet.Deduction1,SalDet.Weekoff,SalDet.CL,SalDet.Deduction2, SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5,SalDet.Losspay,SalDet.NetPay,SalDet.TotalDeductions,SalDet.GrossEarnings," +
                            "SalDet.OverTime,MstDpt.DepartmentNm,SalDet.ProvidentFund,SalDet.ESI,SalDet.HRA,SalDet.WorkedDays, SalDet.PfSalary,SM.Base, SalDet.HalfNightAmt,SalDet.FullNightAmt,SalDet.SpinningAmt,SalDet.DayIncentive,SalDet.ThreesidedAmt";
                }
                else
                {
                    query = "Select ''as Remaining_Days,SalDet.EmpNo,EmpDet.ExisistingCode,EmpDet.EmpName,EmpDet.Designation,EmpDet.Department,SalDet.Totalworkingdays, " +
                            " SalDet.NFh,SalDet.BasicandDA,SalDet.FDA,SalDet.VDA,SalDet.HRA, SalDet.Advance,SalDet.Stamp,SalDet.Unioncg,(SalDet.WorkedDays + SalDet.NFh) as tot, " +
                            " SalDet.Words,SalDet.WorkedDays,SalDet.Month,SalDet.Year,SalDet.LeaveDays,SalDet.SalaryDate,SalDet.allowances1," +
                            " SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.allowances5,SalDet.Deduction1,SalDet.Weekoff,SalDet.CL,SalDet.Deduction2, " +
                            " SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5,SalDet.Deduction5,SalDet.HomeDays,SalDet.Losspay,SalDet.NetPay,SalDet.TotalDeductions,SalDet.GrossEarnings, " +
                            " SalDet.OverTime,MstDpt.DepartmentNm,SalDet.ProvidentFund,SalDet.ESI,SalDet.HRA,SalDet.WorkedDays,sum(SalDet.NetPay - (SalDet.GrossEarnings - SalDet.TotalDeductions)) as Roundoff," +
                            " SalDet.PfSalary as PFEarnings,SalDet.HalfNightAmt,SalDet.FullNightAmt,SalDet.SpinningAmt,SalDet.DayIncentive,SalDet.ThreesidedAmt,SM.Base from EmployeeDetails EmpDet inner Join SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo " +
                            " inner Join MstDepartment as MstDpt on MstDpt.DepartmentCd = EmpDet.Department inner Join OfficialProfile OP on OP.EmpNo=SalDet.EmpNo inner Join SalaryMaster SM on SM.EmpNo=SalDet.EmpNo where " +
                            " SalDet.Month='" + ddlMonths.SelectedValue + "' AND SalDet.FinancialYear='" + ddlFinance.SelectedValue + "' and EmpDet.StafforLabor='" + Stafflabour + "' and " +
                            " EmpDet.Ccode='" + SessionCcode + "' and EmpDet.Lcode='" + SessionLcode + "' and OP.WagesType='" + rbsalary.SelectedValue + "' and SalDet.FromDate = convert(datetime,'" + MyDate1 + "', 105) and SalDet.ToDate = Convert(Datetime,'" + MyDate2 + "', 105) group by SalDet.EmpNo,EmpDet.ExisistingCode,EmpDet.EmpName,EmpDet.Designation,EmpDet.Department,SalDet.Totalworkingdays, " +
                            "SalDet.NFh,SalDet.BasicandDA,SalDet.FDA,SalDet.VDA,SalDet.HRA,SalDet.HomeDays, SalDet.Advance,SalDet.Stamp,SalDet.Unioncg,SalDet.Words,SalDet.WorkedDays,SalDet.Month,SalDet.Year,SalDet.LeaveDays,SalDet.SalaryDate,SalDet.allowances1, " +
                            " SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.allowances5,SalDet.Deduction1,SalDet.Weekoff,SalDet.CL,SalDet.Deduction2, SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5,SalDet.Losspay,SalDet.NetPay,SalDet.TotalDeductions,SalDet.GrossEarnings," +
                            "SalDet.OverTime,MstDpt.DepartmentNm,SalDet.ProvidentFund,SalDet.ESI,SalDet.HRA,SalDet.WorkedDays, SalDet.PfSalary,SM.Base, SalDet.HalfNightAmt,SalDet.FullNightAmt,SalDet.SpinningAmt,SalDet.DayIncentive,SalDet.ThreesidedAmt";
                }
            }
            else
            {
                if (rbsalary.SelectedValue == "2")
                {
                    query = "Select ''as Remaining_Days,SalDet.EmpNo,EmpDet.ExisistingCode,EmpDet.EmpName,EmpDet.Designation,EmpDet.Department,SalDet.Totalworkingdays, " +
                            " SalDet.NFh,SalDet.BasicandDA,SalDet.FDA,SalDet.VDA,SalDet.HRA, SalDet.Advance,SalDet.Stamp,SalDet.Unioncg,(SalDet.WorkedDays + SalDet.NFh) as tot, " +
                            " SalDet.Words,SalDet.WorkedDays,SalDet.Month,SalDet.Year,SalDet.LeaveDays,SalDet.SalaryDate,SalDet.allowances1,(round(SalDet.Netpay/5,0)*5) as Val," +
                            " SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.allowances5,SalDet.HomeDays,SalDet.Deduction1,SalDet.Weekoff,SalDet.CL,SalDet.Deduction2, " +
                            " SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5,SalDet.Losspay,SalDet.NetPay,SalDet.TotalDeductions,SalDet.GrossEarnings, " +
                            " SalDet.OverTime,MstDpt.DepartmentNm,SalDet.ProvidentFund,SalDet.ESI,SalDet.HRA,SalDet.WorkedDays,sum(SalDet.NetPay - (SalDet.GrossEarnings - SalDet.TotalDeductions)) as Roundoff," +
                            " SalDet.PfSalary as PFEarnings, SalDet.HalfNightAmt,SalDet.FullNightAmt,SalDet.SpinningAmt,SalDet.DayIncentive,SalDet.ThreesidedAmt,SM.Base from EmployeeDetails EmpDet inner Join SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo " +
                            " inner Join MstDepartment as MstDpt on MstDpt.DepartmentCd = EmpDet.Department inner Join OfficialProfile OP on OP.EmpNo=SalDet.EmpNo inner Join SalaryMaster SM on SM.EmpNo=SalDet.EmpNo where " +
                            " SalDet.Month='" + ddlMonths.SelectedValue + "' AND SalDet.FinancialYear='" + ddlFinance.SelectedValue + "' and EmpDet.StafforLabor='" + Stafflabour + "' and " +
                            " EmpDet.EmployeeType='" + ddldept.SelectedValue + "' and EmpDet.Ccode='" + SessionCcode + "' and OP.WagesType='" + rbsalary.SelectedValue + "' and EmpDet.Lcode='" + SessionLcode + "' group by SalDet.EmpNo,EmpDet.ExisistingCode,EmpDet.EmpName,EmpDet.Designation,EmpDet.Department,SalDet.Totalworkingdays, " +
                            "SalDet.NFh,SalDet.BasicandDA,SalDet.FDA,SalDet.VDA,SalDet.HRA, SalDet.Advance,SalDet.Stamp,SalDet.Unioncg,SalDet.Words,SalDet.WorkedDays,SalDet.Month,SalDet.Year,SalDet.LeaveDays,SalDet.SalaryDate,SalDet.allowances1, " +
                            " SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.HomeDays,SalDet.allowances5,SalDet.Deduction1,SalDet.Weekoff,SalDet.CL,SalDet.Deduction2, SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5,SalDet.Losspay,SalDet.NetPay,SalDet.TotalDeductions,SalDet.GrossEarnings," +
                            "SalDet.OverTime,MstDpt.DepartmentNm,SalDet.ProvidentFund,SalDet.ESI,SalDet.HRA,SalDet.WorkedDays, SalDet.PfSalary,SalDet.HalfNightAmt,SM.Base,SalDet.FullNightAmt,SalDet.SpinningAmt,SalDet.DayIncentive,SalDet.ThreesidedAmt";
                }
                else
                {
                    query = "Select ''as Remaining_Days,SalDet.EmpNo,EmpDet.ExisistingCode,EmpDet.EmpName,EmpDet.Department,SalDet.Totalworkingdays, " +
                            " SalDet.BasicandDA,(SalDet.WorkedDays + SalDet.NFh) as tot, " +
                            " SalDet.Words,SalDet.WorkedDays,SalDet.Month,SalDet.Year,SalDet.LeaveDays,SalDet.SalaryDate," +

                            " SalDet.NetPay,SalDet.TotalDeductions,SalDet.GrossEarnings, " +
                            " SalDet.OverTime,MstDpt.DepartmentNm,SalDet.HRA,SalDet.WorkedDays," +
                            " SalDet.DayIncentive,SM.Base from EmployeeDetails EmpDet inner Join SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo " +
                            " inner Join MstDepartment as MstDpt on MstDpt.DepartmentCd = EmpDet.Department inner Join OfficialProfile OP on OP.EmpNo=SalDet.EmpNo inner Join SalaryMaster SM on SM.EmpNo=SalDet.EmpNo where " +
                            " SalDet.Month='" + ddlMonths.SelectedValue + "' AND SalDet.FinancialYear='" + ddlFinance.SelectedValue + "' and EmpDet.StafforLabor='" + Stafflabour + "' and " +
                            " EmpDet.Ccode='" + SessionCcode + "' and EmpDet.Lcode='" + SessionLcode + "' and OP.WagesType='" + rbsalary.SelectedValue + "' and SalDet.FromDate = convert(datetime,'" + MyDate1 + "', 105) and SalDet.ToDate = Convert(Datetime,'" + MyDate2 + "', 105) group by SalDet.EmpNo,EmpDet.ExisistingCode,EmpDet.EmpName,EmpDet.Designation,EmpDet.Department,SalDet.Totalworkingdays, " +
                            "SalDet.NFh,SalDet.BasicandDA,SalDet.FDA,SalDet.VDA,SalDet.HRA,SalDet.HomeDays,SalDet.Advance,SalDet.Stamp,SalDet.Unioncg,SalDet.Words,SalDet.WorkedDays,SalDet.Month,SalDet.Year,SalDet.LeaveDays,SalDet.SalaryDate,SalDet.allowances1, " +
                            " SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.allowances5,SalDet.Deduction1,SalDet.Weekoff,SalDet.CL,SalDet.Deduction2, SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5,SalDet.Losspay,SalDet.NetPay,SalDet.TotalDeductions,SalDet.GrossEarnings," +
                            "SalDet.OverTime,MstDpt.DepartmentNm,SalDet.ProvidentFund,SalDet.ESI,SalDet.HRA,SalDet.WorkedDays,SalDet.PfSalary,SalDet.HalfNightAmt,SM.Base,SalDet.FullNightAmt,SalDet.SpinningAmt,SalDet.DayIncentive,SalDet.ThreesidedAmt";
                }

            }


            if (ddldept.SelectedValue == "2" && rbsalary.SelectedValue == "2") // Permanent
            {
                query = "Select SalDet.EmpNo,EmpDet.ExisistingCode,MstDpt.DepartmentNm,EmpDet.EmpName,SalDet.WorkedDays, " +
                    " SM.Base,SalDet.BasicandDA,SalDet.TotalBasic,SalDet.HRA,SalDet.FDA,SalDet.Deduction3, SalDet.Deduction4, SalDet.Deduction5, " +
                    " Sum(SpinningAmt + allowances1 + BasicandDA) as Total_Stipend,SalDet.PfSalary,SalDet.ESI,SalDet.GrossEarnings," +
                    " CAST(EmpDet.ExisistingCode AS int) as OrderToken,round(SalDet.FHRA,-1) as Vall, " +
                    " SalDet.withpay,SalDet.Availabledays,(round(SalDet.FHRA/5,0)*5) as Val,SalDet.Fbasic, " +
                    //" SalDet.Deduction1,SalDet.Deduction2,SalDet.Deduction3,SalDet.Deduction4, " +
                    " SalDet.TotalDeductions,SalDet.OverTime,SalDet.FFDA,SalDet.FHRA, " +
                    " SalDet.NetPay from EmployeeDetails " +
                    " EmpDet inner Join SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo " +
                    " inner Join MstDepartment as MstDpt on MstDpt.DepartmentCd = EmpDet.Department " +
                    " inner Join OfficialProfile OP on OP.EmpNo=SalDet.EmpNo " +
                    " inner Join SalaryMaster SM on SM.EmpNo=SalDet.EmpNo where SalDet.Month='" + ddlMonths.SelectedValue + "' " +
                    " AND SalDet.FinancialYear='" + ddlFinance.SelectedValue + "' and EmpDet.StafforLabor='" + Stafflabour + "' and " +
                    " EmpDet.EmployeeType='" + ddldept.SelectedValue + "' and EmpDet.Ccode='" + SessionCcode + "' and " +
                    " EmpDet.Lcode='" + SessionLcode + "' and OP.WagesType='" + rbsalary.SelectedValue + "' and SalDet.FFDA<> 0  " +
                    " group by SalDet.EmpNo,EmpDet.ExisistingCode,EmpDet.InsOuts,MstDpt.DepartmentNm,EmpDet.EmpName,SalDet.WorkedDays,SalDet.CL, " +
                    " SM.Base,SalDet.SpinningAmt,SalDet.BasicandDA,SalDet.PfSalary,SalDet.ESI, " +
                    " SalDet.Advance,SalDet.allowances1,SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.Deduction3, SalDet.Deduction4, SalDet.Deduction5, " +
                    " SalDet.allowances5,SalDet.Deduction1,SalDet.Deduction2,SalDet.Deduction3,SalDet.Deduction4,SalDet.withpay,SalDet.Availabledays, " +
                    " SalDet.Deduction5,SalDet.TotalDeductions,OverTime,SalDet.NetPay,SalDet.GrossEarnings,SalDet.TotalBasic,SalDet.HRA,SalDet.FDA,SalDet.FFDA,SalDet.FHRA,SalDet.Fbasic Order by OrderToken Asc";
            }

            if (ddldept.SelectedValue == "4" && rbsalary.SelectedValue == "2") //ESI Worker
            {
                query = "Select SalDet.EmpNo,EmpDet.ExisistingCode,MstDpt.DepartmentNm,EmpDet.EmpName,SalDet.WorkedDays, " +
                    " SM.Base,SalDet.BasicandDA,SalDet.TotalBasic,SalDet.HRA,SalDet.FDA,SalDet.Deduction3, SalDet.Deduction4, SalDet.Deduction5, " +
                    " Sum(SpinningAmt + allowances1 + BasicandDA) as Total_Stipend,SalDet.PfSalary,SalDet.ESI,SalDet.GrossEarnings," +
                    " CAST(EmpDet.ExisistingCode AS int) as OrderToken,round(SalDet.FHRA,-1) as Vall,SalDet.Fbasic, " +
                    " SalDet.withpay,SalDet.Availabledays,(round(SalDet.FHRA/5,0)*5) as Val, " +
                    //" SalDet.Deduction1,SalDet.Deduction2,SalDet.Deduction3,SalDet.Deduction4, " +
                    " SalDet.TotalDeductions,SalDet.OverTime,SalDet.FFDA,SalDet.FHRA, " +
                    " SalDet.NetPay from EmployeeDetails " +
                    " EmpDet inner Join SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo " +
                    " inner Join MstDepartment as MstDpt on MstDpt.DepartmentCd = EmpDet.Department " +
                    " inner Join OfficialProfile OP on OP.EmpNo=SalDet.EmpNo " +
                    " inner Join SalaryMaster SM on SM.EmpNo=SalDet.EmpNo where SalDet.Month='" + ddlMonths.SelectedValue + "' " +
                    " AND SalDet.FinancialYear='" + ddlFinance.SelectedValue + "' and EmpDet.StafforLabor='" + Stafflabour + "' and " +
                    " EmpDet.EmployeeType='" + ddldept.SelectedValue + "' and EmpDet.Ccode='" + SessionCcode + "' and " +
                    " EmpDet.Lcode='" + SessionLcode + "' and OP.WagesType='" + rbsalary.SelectedValue + "' and SalDet.FFDA<> 0 " +
                    " group by SalDet.EmpNo,EmpDet.ExisistingCode,EmpDet.InsOuts,MstDpt.DepartmentNm,EmpDet.EmpName,SalDet.WorkedDays,SalDet.CL, " +
                    " SM.Base,SalDet.SpinningAmt,SalDet.BasicandDA,SalDet.PfSalary,SalDet.ESI, " +
                    " SalDet.Advance,SalDet.allowances1,SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.Deduction3, SalDet.Deduction4, SalDet.Deduction5, " +
                    " SalDet.allowances5,SalDet.Deduction1,SalDet.Deduction2,SalDet.Deduction3,SalDet.Deduction4,SalDet.withpay,SalDet.Availabledays, " +
                    " SalDet.Deduction5,SalDet.TotalDeductions,OverTime,SalDet.NetPay,SalDet.GrossEarnings,SalDet.TotalBasic,SalDet.HRA,SalDet.FDA,SalDet.FFDA,SalDet.FHRA,SalDet.Fbasic Order by OrderToken Asc";
            }

            if (ddldept.SelectedValue == "3" && rbsalary.SelectedValue == "2") // Temporary Worker
            {

                query = "Select SalDet.EmpNo,EmpDet.ExisistingCode,MstDpt.DepartmentNm,EmpDet.EmpName,SalDet.WorkedDays, " +
                " SM.Base,SalDet.BasicandDA,SalDet.TotalBasic,SalDet.HRA,SalDet.FDA,SalDet.Deduction3, SalDet.Deduction4, SalDet.Deduction5, " +
                " Sum(SpinningAmt + allowances1 + BasicandDA) as Total_Stipend,SalDet.PfSalary,SalDet.ESI,SalDet.GrossEarnings," +
                " CAST(EmpDet.ExisistingCode AS int) as OrderToken,round(SalDet.FHRA,-1) as Vall,SalDet.Fbasic, " +
                " SalDet.withpay,SalDet.Availabledays,(round(SalDet.FHRA/5,0)*5) as Val, " +
                    //" SalDet.Deduction1,SalDet.Deduction2,SalDet.Deduction3,SalDet.Deduction4, " +
                " SalDet.TotalDeductions,SalDet.OverTime, " +
                " SalDet.NetPay from EmployeeDetails " +
                " EmpDet inner Join SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo " +
                " inner Join MstDepartment as MstDpt on MstDpt.DepartmentCd = EmpDet.Department " +
                " inner Join OfficialProfile OP on OP.EmpNo=SalDet.EmpNo " +
                " inner Join SalaryMaster SM on SM.EmpNo=SalDet.EmpNo where SalDet.Month='" + ddlMonths.SelectedValue + "' " +
                " AND SalDet.FinancialYear='" + ddlFinance.SelectedValue + "' and EmpDet.StafforLabor='" + Stafflabour + "' and " +
                " EmpDet.EmployeeType='" + ddldept.SelectedValue + "' and EmpDet.Ccode='" + SessionCcode + "' and " +
                " EmpDet.Lcode='" + SessionLcode + "' and OP.WagesType='" + rbsalary.SelectedValue + "' " +
                " group by SalDet.EmpNo,EmpDet.ExisistingCode,EmpDet.InsOuts,MstDpt.DepartmentNm,EmpDet.EmpName,SalDet.WorkedDays,SalDet.CL, " +
                " SM.Base,SalDet.SpinningAmt,SalDet.BasicandDA,SalDet.PfSalary,SalDet.ESI, " +
                " SalDet.Advance,SalDet.allowances1,SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.Deduction3, SalDet.Deduction4, SalDet.Deduction5, " +
                " SalDet.allowances5,SalDet.Deduction1,SalDet.Deduction2,SalDet.Deduction3,SalDet.Deduction4,SalDet.withpay,SalDet.Availabledays, " +
                " SalDet.Deduction5,SalDet.TotalDeductions,OverTime,SalDet.NetPay,SalDet.GrossEarnings,SalDet.TotalBasic,SalDet.HRA,SalDet.FDA,SalDet.Fbasic Order by OrderToken Asc";


            }

            
            
            if (ddldept.SelectedValue == "1" && rbsalary.SelectedValue == "2") // Staff Worker
            {
                query = "Select SalDet.EmpNo,EmpDet.ExisistingCode,MstDpt.DepartmentNm,EmpDet.EmpName,SalDet.WorkedDays, " +
                              " sum(SM.Base + SM.HRA + SM.FDA) as Base,SalDet.BasicandDA,SalDet.TotalBasic,SalDet.HRA,SalDet.FDA,SalDet.Deduction3, SalDet.Deduction4, SalDet.Deduction5, " +
                              " Sum(SpinningAmt + allowances1 + BasicandDA) as Total_Stipend,SalDet.PfSalary,SalDet.ESI,SalDet.GrossEarnings," +
                              " CAST(EmpDet.ExisistingCode AS int) as OrderToken,round(SalDet.FHRA,-1) as Vall,SalDet.Fbasic, " +
                              " SalDet.withpay,SalDet.Availabledays,SalDet.FFDA,SalDet.FHRA, " +
                    //" SalDet.Deduction1,SalDet.Deduction2,SalDet.Deduction3,SalDet.Deduction4, " +
                              " SalDet.TotalDeductions,SalDet.OverTime,(round(SalDet.FHRA/5,0)*5) as Val,  " +
                              " SalDet.NetPay from EmployeeDetails " +
                              " EmpDet inner Join SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo " +
                              " inner Join MstDepartment as MstDpt on MstDpt.DepartmentCd = EmpDet.Department " +
                              " inner Join OfficialProfile OP on OP.EmpNo=SalDet.EmpNo " +
                              " inner Join SalaryMaster SM on SM.EmpNo=SalDet.EmpNo where SalDet.Month='" + ddlMonths.SelectedValue + "' " +
                              " AND SalDet.FinancialYear='" + ddlFinance.SelectedValue + "' and EmpDet.StafforLabor='" + Stafflabour + "' and " +
                              " EmpDet.EmployeeType='" + ddldept.SelectedValue + "' and EmpDet.Ccode='" + SessionCcode + "' and " +
                              " EmpDet.Lcode='" + SessionLcode + "' and OP.WagesType='" + rbsalary.SelectedValue + "' and SalDet.FFDA<> 0 " +
                              " group by SalDet.EmpNo,EmpDet.ExisistingCode,EmpDet.InsOuts,MstDpt.DepartmentNm,EmpDet.EmpName,SalDet.WorkedDays,SalDet.CL, " +
                              " SM.Base,SalDet.SpinningAmt,SalDet.BasicandDA,SalDet.PfSalary,SalDet.ESI, " +
                              " SalDet.Advance,SalDet.allowances1,SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.Deduction3, SalDet.Deduction4, SalDet.Deduction5, " +
                              " SalDet.allowances5,SalDet.Deduction1,SalDet.Deduction2,SalDet.Deduction3,SalDet.Deduction4,SalDet.withpay,SalDet.Availabledays, " +
                              " SalDet.Deduction5,SalDet.TotalDeductions,OverTime,SalDet.NetPay,SalDet.GrossEarnings,SalDet.TotalBasic,SalDet.HRA,SalDet.FDA,SalDet.FFDA,SalDet.FHRA,SalDet.Fbasic Order by OrderToken Asc";
            }


            if (ddldept.SelectedValue == "7" && rbsalary.SelectedValue == "2") // Security Worker
            {
                query = "Select SalDet.EmpNo,EmpDet.ExisistingCode,MstDpt.DepartmentNm,EmpDet.EmpName,SalDet.WorkedDays, " +
                              " sum(SM.Base + SM.HRA + SM.FDA) as Base,SalDet.BasicandDA,SalDet.TotalBasic,SalDet.HRA,SalDet.FDA,SalDet.Deduction3, SalDet.Deduction4, SalDet.Deduction5, " +
                              " Sum(SpinningAmt + allowances1 + BasicandDA) as Total_Stipend,SalDet.PfSalary,SalDet.ESI,SalDet.GrossEarnings," +
                              " CAST(EmpDet.ExisistingCode AS int) as OrderToken,round(SalDet.FHRA,-1) as Vall,SalDet.Fbasic, " +
                              " SalDet.withpay,SalDet.Availabledays,SalDet.FFDA,SalDet.FHRA, " +
                    //" SalDet.Deduction1,SalDet.Deduction2,SalDet.Deduction3,SalDet.Deduction4, " +
                              " SalDet.TotalDeductions,SalDet.OverTime,(round(SalDet.FHRA/5,0)*5) as Val,  " +
                              " SalDet.NetPay from EmployeeDetails " +
                              " EmpDet inner Join SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo " +
                              " inner Join MstDepartment as MstDpt on MstDpt.DepartmentCd = EmpDet.Department " +
                              " inner Join OfficialProfile OP on OP.EmpNo=SalDet.EmpNo " +
                              " inner Join SalaryMaster SM on SM.EmpNo=SalDet.EmpNo where SalDet.Month='" + ddlMonths.SelectedValue + "' " +
                              " AND SalDet.FinancialYear='" + ddlFinance.SelectedValue + "' and EmpDet.StafforLabor='" + Stafflabour + "' and " +
                              " EmpDet.EmployeeType='" + ddldept.SelectedValue + "' and EmpDet.Ccode='" + SessionCcode + "' and " +
                              " EmpDet.Lcode='" + SessionLcode + "' and OP.WagesType='" + rbsalary.SelectedValue + "' and SalDet.FFDA<> 0 " +
                              " group by SalDet.EmpNo,EmpDet.ExisistingCode,EmpDet.InsOuts,MstDpt.DepartmentNm,EmpDet.EmpName,SalDet.WorkedDays,SalDet.CL, " +
                              " SM.Base,SalDet.SpinningAmt,SalDet.BasicandDA,SalDet.PfSalary,SalDet.ESI, " +
                              " SalDet.Advance,SalDet.allowances1,SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.Deduction3, SalDet.Deduction4, SalDet.Deduction5, " +
                              " SalDet.allowances5,SalDet.Deduction1,SalDet.Deduction2,SalDet.Deduction3,SalDet.Deduction4,SalDet.withpay,SalDet.Availabledays, " +
                              " SalDet.Deduction5,SalDet.TotalDeductions,OverTime,SalDet.NetPay,SalDet.GrossEarnings,SalDet.TotalBasic,SalDet.HRA,SalDet.FDA,SalDet.FFDA,SalDet.FHRA,SalDet.Fbasic Order by OrderToken Asc";
            }

            SqlCommand cmd = new SqlCommand(query, con);
            DataTable dt_1 = new DataTable();
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            //DataSet ds1 = new DataSet();
            con.Open();
            sda.Fill(dt_1);
            con.Close();
            if (ddldept.SelectedValue == "1")
            {
                GvOtherWorker.DataSource = dt_1;
                GvOtherWorker.DataBind();

                //CasualWorkerGridView.DataSource = dt_1;
                //CasualWorkerGridView.DataBind();
            }
            else if (ddldept.SelectedValue == "2")  // Permanent Worker
            {
                GvOtherWorker.DataSource = dt_1;
                GvOtherWorker.DataBind();
            }
            else if (ddldept.SelectedValue == "7")  // Security Worker
            {
                GvOtherWorker.DataSource = dt_1;
                GvOtherWorker.DataBind();
            }
            else if (ddldept.SelectedValue == "4")    //ESI Worker
            {
                GvOtherWorker.DataSource = dt_1;
                GvOtherWorker.DataBind();
                //SchemeWorkerGridView.DataSource = dt_1;
                //SchemeWorkerGridView.DataBind();
            }
            else if (ddldept.SelectedValue == "5")  // Other Worker
            {
                //CasualWorkerGridView.DataSource = dt_1;
                //CasualWorkerGridView.DataBind();


            }
            else if (ddldept.SelectedValue == "3")  // Temporory Worker
            {
                //CasualWorkerGridView.DataSource = dt_1;
                //CasualWorkerGridView.DataBind();
            }
            else
            {
                //gvSalary.DataSource = dt_1;
                //gvSalary.DataBind();
            }

            if (ddldept.SelectedValue == "1") //Casual Worker OverTime
            {
                //foreach (GridViewRow gvsal in CasualWorkerGridView.Rows)
                //{
                //    Label lbl_EmpNo = (Label)gvsal.FindControl("lblEmpNo");
                //    Label lbl_OTHours = (Label)gvsal.FindControl("lblOThours");
                //    string qry_ot = "";
                //    if (rbsalary.SelectedValue == "2")
                //    {
                //        qry_ot = "Select NoHrs from OverTime where EmpNo='" + lbl_EmpNo.Text.Trim() + "' and Lcode='" + SessionLcode + "' and Ccode='" + SessionCcode + "' and Month='" + ddlMonths.SelectedValue + "' and Financialyr='" + ddlFinance.SelectedValue + "'";
                //    }
                //    else
                //    {
                //        qry_ot = "Select NoHrs from OverTime where EmpNo='" + lbl_EmpNo.Text.Trim() + "' and Lcode='" + SessionLcode + "' and Ccode='" + SessionCcode + "' and Month='" + ddlMonths.SelectedValue + "' and Financialyr='" + ddlFinance.SelectedValue + "'" +
                //                        " and convert(Datetime, FromDate, 105)=convert(Datetime,'" + MyDate1 + "', 105) and convert(Datetime, ToDate, 105)=convert(datetime,'" + MyDate2 + "', 105)";

                //    }
                //    SqlCommand cmd_ot = new SqlCommand(qry_ot, con);
                //    con.Open();
                //    string val_ot = Convert.ToString(cmd_ot.ExecuteScalar());
                //    con.Close();
                //    if (val_ot.Trim() == "")
                //    {
                //        lbl_OTHours.Text = "0";
                //    }
                //    else
                //    {
                //        lbl_OTHours.Text = val_ot;
                //    }
                //}
            }
            else if (ddldept.SelectedValue == "4") //Scheme Worker OverTime
            {
                //foreach (GridViewRow gvsal in SchemeWorkerGridView.Rows)
                //{
                //    Label lbl_EmpNo = (Label)gvsal.FindControl("lblEmpNo");
                //    Label lbl_OTHours = (Label)gvsal.FindControl("lblOThours");
                //    string qry_ot = "";
                //    if (rbsalary.SelectedValue == "2")
                //    {
                //        qry_ot = "Select NoHrs from OverTime where EmpNo='" + lbl_EmpNo.Text.Trim() + "' and Lcode='" + SessionLcode + "' and Ccode='" + SessionCcode + "' and Month='" + ddlMonths.SelectedValue + "' and Financialyr='" + ddlFinance.SelectedValue + "'";
                //    }
                //    else
                //    {
                //        qry_ot = "Select NoHrs from OverTime where EmpNo='" + lbl_EmpNo.Text.Trim() + "' and Lcode='" + SessionLcode + "' and Ccode='" + SessionCcode + "' and Month='" + ddlMonths.SelectedValue + "' and Financialyr='" + ddlFinance.SelectedValue + "'" +
                //                        " and convert(Datetime, FromDate, 105)=convert(Datetime,'" + MyDate1 + "', 105) and convert(Datetime, ToDate, 105)=convert(datetime,'" + MyDate2 + "', 105)";

                //    }
                //    SqlCommand cmd_ot = new SqlCommand(qry_ot, con);
                //    con.Open();
                //    string val_ot = Convert.ToString(cmd_ot.ExecuteScalar());
                //    con.Close();
                //    if (val_ot.Trim() == "")
                //    {
                //        lbl_OTHours.Text = "0";
                //    }
                //    else
                //    {
                //        lbl_OTHours.Text = val_ot;
                //    }
                //}
            }

            else if (ddldept.SelectedValue == "5") //Non Scheme Worker OverTime
            {
                //foreach (GridViewRow gvsal in NonSchemeWorkerGridView.Rows)
                //{
                //    Label lbl_EmpNo = (Label)gvsal.FindControl("lblEmpNo");
                //    Label lbl_OTHours = (Label)gvsal.FindControl("lblOThours");
                //    string qry_ot = "";
                //    if (rbsalary.SelectedValue == "2")
                //    {
                //        qry_ot = "Select NoHrs from OverTime where EmpNo='" + lbl_EmpNo.Text.Trim() + "' and Lcode='" + SessionLcode + "' and Ccode='" + SessionCcode + "' and Month='" + ddlMonths.SelectedValue + "' and Financialyr='" + ddlFinance.SelectedValue + "'";
                //    }
                //    else
                //    {
                //        qry_ot = "Select NoHrs from OverTime where EmpNo='" + lbl_EmpNo.Text.Trim() + "' and Lcode='" + SessionLcode + "' and Ccode='" + SessionCcode + "' and Month='" + ddlMonths.SelectedValue + "' and Financialyr='" + ddlFinance.SelectedValue + "'" +
                //                        " and convert(Datetime, FromDate, 105)=convert(Datetime,'" + MyDate1 + "', 105) and convert(Datetime, ToDate, 105)=convert(datetime,'" + MyDate2 + "', 105)";

                //    }
                //    SqlCommand cmd_ot = new SqlCommand(qry_ot, con);
                //    con.Open();
                //    string val_ot = Convert.ToString(cmd_ot.ExecuteScalar());
                //    con.Close();
                //    if (val_ot.Trim() == "")
                //    {
                //        lbl_OTHours.Text = "0";
                //    }
                //    else
                //    {
                //        lbl_OTHours.Text = val_ot;
                //    }
                //}
            }

            else if (ddldept.SelectedValue == "2") //Staff Worker OverTime
            {
                //foreach (GridViewRow gvsal in StaffWorkerPFGridView.Rows)
                //{
                //    Label lbl_EmpNo = (Label)gvsal.FindControl("lblEmpNo");
                //    Label lbl_OTHours = (Label)gvsal.FindControl("lblOThours");
                //    string qry_ot = "";
                //    if (rbsalary.SelectedValue == "2")
                //    {
                //        qry_ot = "Select NoHrs from OverTime where EmpNo='" + lbl_EmpNo.Text.Trim() + "' and Lcode='" + SessionLcode + "' and Ccode='" + SessionCcode + "' and Month='" + ddlMonths.SelectedValue + "' and Financialyr='" + ddlFinance.SelectedValue + "'";
                //    }
                //    else
                //    {
                //        qry_ot = "Select NoHrs from OverTime where EmpNo='" + lbl_EmpNo.Text.Trim() + "' and Lcode='" + SessionLcode + "' and Ccode='" + SessionCcode + "' and Month='" + ddlMonths.SelectedValue + "' and Financialyr='" + ddlFinance.SelectedValue + "'" +
                //                        " and convert(Datetime, FromDate, 105)=convert(Datetime,'" + MyDate1 + "', 105) and convert(Datetime, ToDate, 105)=convert(datetime,'" + MyDate2 + "', 105)";

                //    }
                //    SqlCommand cmd_ot = new SqlCommand(qry_ot, con);
                //    con.Open();
                //    string val_ot = Convert.ToString(cmd_ot.ExecuteScalar());
                //    con.Close();
                //    if (val_ot.Trim() == "")
                //    {
                //        lbl_OTHours.Text = "0";
                //    }
                //    else
                //    {
                //        lbl_OTHours.Text = val_ot;
                //    }
                //}
            }
            else if (ddldept.SelectedValue == "3") //Hindi-boys  OverTime
            {
                //foreach (GridViewRow gvsal in PermanentWorkerGridView.Rows)
                //{
                //    Label lbl_EmpNo = (Label)gvsal.FindControl("lblEmpNo");
                //    Label lbl_OTHours = (Label)gvsal.FindControl("lblOThours");
                //    string qry_ot = "";
                //    if (rbsalary.SelectedValue == "2")
                //    {
                //        qry_ot = "Select NoHrs from OverTime where EmpNo='" + lbl_EmpNo.Text.Trim() + "' and Lcode='" + SessionLcode + "' and Ccode='" + SessionCcode + "' and Month='" + ddlMonths.SelectedValue + "' and Financialyr='" + ddlFinance.SelectedValue + "'";
                //    }
                //    else
                //    {
                //        qry_ot = "Select NoHrs from OverTime where EmpNo='" + lbl_EmpNo.Text.Trim() + "' and Lcode='" + SessionLcode + "' and Ccode='" + SessionCcode + "' and Month='" + ddlMonths.SelectedValue + "' and Financialyr='" + ddlFinance.SelectedValue + "'" +
                //                        " and convert(Datetime, FromDate, 105)=convert(Datetime,'" + MyDate1 + "', 105) and convert(Datetime, ToDate, 105)=convert(datetime,'" + MyDate2 + "', 105)";

                //    }
                //    SqlCommand cmd_ot = new SqlCommand(qry_ot, con);
                //    con.Open();
                //    string val_ot = Convert.ToString(cmd_ot.ExecuteScalar());
                //    con.Close();
                //    if (val_ot.Trim() == "")
                //    {
                //        lbl_OTHours.Text = "0";
                //    }
                //    else
                //    {
                //        lbl_OTHours.Text = val_ot;
                //    }
                //}
            }
            else
            {
                //foreach (GridViewRow gvsal in gvSalary.Rows)
                //{
                //    Label lbl_EmpNo = (Label)gvsal.FindControl("lblEmpNo");
                //    Label lbl_OTHours = (Label)gvsal.FindControl("lblOThours");
                //    string qry_ot = "";
                //    if (rbsalary.SelectedValue == "2")
                //    {
                //        qry_ot = "Select NoHrs from OverTime where EmpNo='" + lbl_EmpNo.Text.Trim() + "' and Lcode='" + SessionLcode + "' and Ccode='" + SessionCcode + "' and Month='" + ddlMonths.SelectedValue + "' and Financialyr='" + ddlFinance.SelectedValue + "'";
                //    }
                //    else
                //    {
                //        qry_ot = "Select NoHrs from OverTime where EmpNo='" + lbl_EmpNo.Text.Trim() + "' and Lcode='" + SessionLcode + "' and Ccode='" + SessionCcode + "' and Month='" + ddlMonths.SelectedValue + "' and Financialyr='" + ddlFinance.SelectedValue + "'" +
                //                        " and convert(Datetime, FromDate, 105)=convert(Datetime,'" + MyDate1 + "', 105) and convert(Datetime, ToDate, 105)=convert(datetime,'" + MyDate2 + "', 105)";

                //    }
                //    SqlCommand cmd_ot = new SqlCommand(qry_ot, con);
                //    con.Open();
                //    string val_ot = Convert.ToString(cmd_ot.ExecuteScalar());
                //    con.Close();
                //    if (val_ot.Trim() == "")
                //    {
                //        lbl_OTHours.Text = "0";
                //    }
                //    else
                //    {
                //        lbl_OTHours.Text = val_ot;
                //    }
                //}
            }
            string attachment = "attachment;filename=Payslip.xls";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/ms-excel";
            DataTable dt = new DataTable();
            dt = objdata.Company_retrive(SessionCcode, SessionLcode);
            if (dt.Rows.Count > 0)
            {
                CmpName = dt.Rows[0]["Cname"].ToString();
                Cmpaddress = (dt.Rows[0]["Address1"].ToString() + ", " + dt.Rows[0]["Address2"].ToString() + ", " + dt.Rows[0]["Location"].ToString() + "-" + dt.Rows[0]["Pincode"].ToString());
            }
            NetBase = "0";
            NetFDA = "0";
            NetVDA = "0";
            Nettotal = "0";
            NetPFEarnings = "0";
            NetPF = "0";
            NetESI = "0";
            NetUnion = "0";
            NetAdvance = "0";
            NetAll1 = "0";
            NetAll2 = "0";
            NetAll3 = "0";
            NetAll4 = "0";
            NetDed1 = "0";
            NetDed2 = "0";
            NetDed3 = "0";
            NetDed4 = "0";
            NetAll5 = "0";
            NetDed5 = "0";
            NetLOP = "0";
            NetStamp = "0";
            NetTotalDeduction = "0";
            NetOT = "0";
            NetAmt = "0";
            Network = "0";
            totCL = "0";
            totNFh = "0";
            totweekoff = "0";
            Roundoff = "0";
            HomeDays = "0";
            Halfnight = "0";
            FullNight = "0";
            Spinning = "0";
            DayIncentive = "0";
            ThreeSided = "0";
            //totwork = "0";
            DataTable Slap_DT = new DataTable();

            StringWriter stw = new StringWriter();
            HtmlTextWriter htextw = new HtmlTextWriter(stw);
            if (ddldept.SelectedValue == "1")
            {
                GvOtherWorker.RenderControl(htextw);
            }
            else if (ddldept.SelectedValue == "2")
            {
                GvOtherWorker.RenderControl(htextw);
            }
            else if (ddldept.SelectedValue == "7")
            {
                GvOtherWorker.RenderControl(htextw);
            }
            else if (ddldept.SelectedValue == "4")
            {
                GvOtherWorker.RenderControl(htextw);
            }
            else if (ddldept.SelectedValue == "5") 
            {
               // CasualWorkerGridView.RenderControl(htextw);


            }
            else if (ddldept.SelectedValue == "3")
            {
               // CasualWorkerGridView.RenderControl(htextw);
            }
            else
            {
               // gvSalary.RenderControl(htextw);
            }
            Response.Write("<table>");
            Response.Write("<tr align='Center'>");
            Response.Write("<td colspan='12'>");
            Response.Write("<h3><b>" + CmpName + "</b></h3>");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("<tr align='Center'>");
            Response.Write("<td colspan='12'>");
            Response.Write(" UNIT-I");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("<tr align='Center'>");
            Response.Write("<td colspan='12'>");
            Response.Write("" + Cmpaddress + "");
            Response.Write("</td>");
            Response.Write("</tr>");
            if (rbsalary.SelectedValue == "2")
            {
                Response.Write("<tr>");
                Response.Write("<td colspan='10'>");
                Response.Write("Salary Month of " + ddlMonths.SelectedValue + " - " + YR);
                Response.Write("</td>");
                Response.Write("</tr>");
            }
            else
            {
                Response.Write("<tr>");
                Response.Write("<td colspan='10'>");
                Response.Write("Salary Month of " + txtfrom.Text + " - " + txtTo.Text);
                Response.Write("</td>");
                Response.Write("</tr>");
            }
            Response.Write("</table>");

            //gvSalary.RenderControl(htextw);
            //Response.Write("Contract Details");
            Response.Write(stw.ToString());

            Response.Write("<table border='1'>");
            Response.Write("<tr Font-Bold='true' align='right'>");
            Response.Write("</td>");
            if (ddldept.SelectedValue == "3")
            {
                Response.Write("<td font-Bold='true' colspan='4'>");
                Response.Write("Grand Total");
                Response.Write("</td>");
            }
            else if (ddldept.SelectedValue == "1")
            {
                Response.Write("<td font-Bold='true' colspan='4'>");
                Response.Write("Grand Total");
                Response.Write("</td>");
            }
            else
            {
                Response.Write("<td font-Bold='true' colspan='4'>");
                Response.Write("Grand Total");
                Response.Write("</td>");
            }
            //Get Count
            string Pay_RowCount = "0";
            if (ddldept.SelectedValue == "1")
            {
                Pay_RowCount = Convert.ToDecimal(GvOtherWorker.Rows.Count + 6).ToString();
            }
            if (ddldept.SelectedValue == "7")
            {
                Pay_RowCount = Convert.ToDecimal(GvOtherWorker.Rows.Count + 6).ToString();
            }
            if (ddldept.SelectedValue == "2")
            {
                 Pay_RowCount = Convert.ToDecimal(GvOtherWorker.Rows.Count + 6).ToString();
            }
            if (ddldept.SelectedValue == "4")
            {
                 Pay_RowCount = Convert.ToDecimal(GvOtherWorker.Rows.Count + 6).ToString();
            }
            if (ddldept.SelectedValue == "5")
            {
                //Pay_RowCount = Convert.ToDecimal(CasualWorkerGridView.Rows.Count + 6).ToString();




            }
            if (ddldept.SelectedValue == "6")
            {
               // Pay_RowCount = Convert.ToDecimal(gvSalary.Rows.Count + 5).ToString();
            }
            if (ddldept.SelectedValue == "3")
            {
                //Pay_RowCount = Convert.ToDecimal(CasualWorkerGridView.Rows.Count + 6).ToString();
            }
           // if (ddldept.SelectedValue == "3") { Response.Write("<td>=sum(F6:F" + Pay_RowCount + ")</td>"); }
            //if (ddldept.SelectedValue == "1") { Response.Write("<td>=sum(F6:F" + Pay_RowCount + ")</td>"); }

            if (ddldept.SelectedValue == "2")
            {
                Response.Write("<td>=sum(E6:E" + Pay_RowCount + ")</td>");
                Response.Write("<td>=sum(F6:F" + Pay_RowCount + ")</td>");
                Response.Write("<td>=sum(G6:G" + Pay_RowCount + ")</td>");
                //Response.Write("<td>=sum(F6:F" + Pay_RowCount + ")</td>");

                //Response.Write("<td>=sum(G6:G" + Pay_RowCount + ")</td>");
                //Response.Write("<td>=sum(H6:H" + Pay_RowCount + ")</td>");
                //Response.Write("<td>=sum(I6:I" + Pay_RowCount + ")</td>");
                //Response.Write("<td>=sum(J6:J" + Pay_RowCount + ")</td>");
                //Response.Write("<td>=sum(K6:K" + Pay_RowCount + ")</td>");
                //Response.Write("<td>=sum(L6:L" + Pay_RowCount + ")</td>");
                //Response.Write("<td>=sum(M6:M" + Pay_RowCount + ")</td>");
                //Response.Write("<td>=sum(N6:N" + Pay_RowCount + ")</td>");
                //Response.Write("<td>=sum(O6:O" + Pay_RowCount + ")</td>");





                //Response.Write("<td>=sum(Q6:Q" + Pay_RowCount + ")</td>");
                //Response.Write("<td>=sum(R6:R" + Pay_RowCount + ")</td>");
                //Response.Write("<td>=sum(S6:S" + Pay_RowCount + ")</td>");
                //Response.Write("<td>=sum(T6:T" + Pay_RowCount + ")</td>");
                //Response.Write("<td>=sum(U6:U" + Pay_RowCount + ")</td>");
                //Response.Write("<td>=sum(V6:V" + Pay_RowCount + ")</td>");
                //Response.Write("<td>=sum(W6:W" + Pay_RowCount + ")</td>");
            }

            if (ddldept.SelectedValue == "3")
            {
                Response.Write("<td>=sum(G6:G" + Pay_RowCount + ")</td>");
                Response.Write("<td>=sum(H6:H" + Pay_RowCount + ")</td>");
                Response.Write("<td>=sum(I6:I" + Pay_RowCount + ")</td>");

            }

            if (ddldept.SelectedValue == "4")
            {

                Response.Write("<td>=sum(E6:E" + Pay_RowCount + ")</td>");
                Response.Write("<td>=sum(F6:F" + Pay_RowCount + ")</td>");
                Response.Write("<td>=sum(G6:G" + Pay_RowCount + ")</td>");


                //Response.Write("<td>=sum(F6:F" + Pay_RowCount + ")</td>");


                //Response.Write("<td>=sum(G6:G" + Pay_RowCount + ")</td>");
                //Response.Write("<td>=sum(H6:H" + Pay_RowCount + ")</td>");
                //Response.Write("<td>=sum(I6:I" + Pay_RowCount + ")</td>");
                //Response.Write("<td>=sum(J6:J" + Pay_RowCount + ")</td>");
                //Response.Write("<td>=sum(K6:K" + Pay_RowCount + ")</td>");
                //Response.Write("<td>=sum(L6:L" + Pay_RowCount + ")</td>");
                //Response.Write("<td>=sum(M6:M" + Pay_RowCount + ")</td>");
                //Response.Write("<td>=sum(N6:N" + Pay_RowCount + ")</td>");
                //Response.Write("<td>=sum(O6:O" + Pay_RowCount + ")</td>");
            }

            if (ddldept.SelectedValue == "5")
            {
                Response.Write("<td>=sum(F6:F" + Pay_RowCount + ")</td>");
                Response.Write("<td>=sum(G6:G" + Pay_RowCount + ")</td>");
                Response.Write("<td>=sum(H6:H" + Pay_RowCount + ")</td>");
                Response.Write("<td>=sum(I6:I" + Pay_RowCount + ")</td>");
                //Response.Write("<td>=sum(J6:J" + Pay_RowCount + ")</td>");
                //Response.Write("<td>=sum(K6:K" + Pay_RowCount + ")</td>");
                //Response.Write("<td>=sum(L6:L" + Pay_RowCount + ")</td>");
                //Response.Write("<td>=sum(M6:M" + Pay_RowCount + ")</td>");
                //Response.Write("<td>=sum(N6:N" + Pay_RowCount + ")</td>");
                //Response.Write("<td>=sum(O6:O" + Pay_RowCount + ")</td>");
                //Response.Write("<td>=sum(P6:P" + Pay_RowCount + ")</td>");
                //Response.Write("<td>=sum(Q6:Q" + Pay_RowCount + ")</td>");
                //Response.Write("<td>=sum(R6:R" + Pay_RowCount + ")</td>");
                //Response.Write("<td>=sum(S6:S" + Pay_RowCount + ")</td>");
                //Response.Write("<td>=sum(T6:T" + Pay_RowCount + ")</td>");
                //Response.Write("<td>=sum(U6:U" + Pay_RowCount + ")</td>");
                //Response.Write("<td>=sum(V6:V" + Pay_RowCount + ")</td>");
                //Response.Write("<td>=sum(W6:W" + Pay_RowCount + ")</td>");
            }

            if (ddldept.SelectedValue == "1")
            {
                Response.Write("<td>=sum(E6:E" + Pay_RowCount + ")</td>");
                Response.Write("<td>=sum(F6:F" + Pay_RowCount + ")</td>");
                Response.Write("<td>=sum(G6:G" + Pay_RowCount + ")</td>");
                //Response.Write("<td>=sum(F6:F" + Pay_RowCount + ")</td>");
                //Response.Write("<td>=sum(I6:I" + Pay_RowCount + ")</td>");
                //Response.Write("<td>=sum(J6:J" + Pay_RowCount + ")</td>");
                //Response.Write("<td>=sum(K6:K" + Pay_RowCount + ")</td>");
                //Response.Write("<td>=sum(L6:L" + Pay_RowCount + ")</td>");
                //Response.Write("<td>=sum(M6:M" + Pay_RowCount + ")</td>");
                //Response.Write("<td>=sum(N6:N" + Pay_RowCount + ")</td>");
                //Response.Write("<td>=sum(O6:O" + Pay_RowCount + ")</td>");
            }
            if (ddldept.SelectedValue == "7")
            {
                Response.Write("<td>=sum(E6:E" + Pay_RowCount + ")</td>");
                Response.Write("<td>=sum(F6:F" + Pay_RowCount + ")</td>");
                Response.Write("<td>=sum(G6:G" + Pay_RowCount + ")</td>");
                //Response.Write("<td>=sum(F6:F" + Pay_RowCount + ")</td>");
                //Response.Write("<td>=sum(I6:I" + Pay_RowCount + ")</td>");
                //Response.Write("<td>=sum(J6:J" + Pay_RowCount + ")</td>");
                //Response.Write("<td>=sum(K6:K" + Pay_RowCount + ")</td>");
                //Response.Write("<td>=sum(L6:L" + Pay_RowCount + ")</td>");
                //Response.Write("<td>=sum(M6:M" + Pay_RowCount + ")</td>");
                //Response.Write("<td>=sum(N6:N" + Pay_RowCount + ")</td>");
                //Response.Write("<td>=sum(O6:O" + Pay_RowCount + ")</td>");
            }
            Response.Write("</tr></table>");

            Response.End();
            Response.Clear();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Downloaded Successfully');", true);
        }
    }

    protected void btnExport_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        if (rbsalary.SelectedValue == "2")
        {
            if (ddlMonths.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Month Properly');", true);
                ErrFlag = true;
            }
        }
        else
        {
            if (txtfrom.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Month Properly');", true);
                ErrFlag = true;
            }
            else if (txtTo.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Month Properly');", true);
                ErrFlag = true;
            }
            if (!ErrFlag)
            {
                DateTime dfrom = Convert.ToDateTime(txtfrom.Text);
                DateTime dtto = Convert.ToDateTime(txtTo.Text);
                MyDate1 = DateTime.ParseExact(txtfrom.Text, "dd-MM-yyyy", null);
                MyDate2 = DateTime.ParseExact(txtTo.Text, "dd-MM-yyyy", null);
                if (dtto < dfrom)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the From Date...');", true);
                    txtfrom.Text = null;
                    txtTo.Text = null;
                    ErrFlag = true;
                }
            }
        }
        if ((ddlcategory.SelectedValue == "") || (ddlcategory.SelectedValue == "0"))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Category Properly');", true);
            ErrFlag = true;
        }
        else if ((ddldept.SelectedValue == "") || (ddldept.SelectedValue == "0"))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Employee Type Properly');", true);
            ErrFlag = true;
        }
        
        
        if (!ErrFlag)
        {
            string CmpName = "";
            string Cmpaddress = "";
            int YR = 0;
            string query = "";
            if (ddlMonths.SelectedValue == "January")
            {
                YR = Convert.ToInt32(ddlFinance.SelectedValue);
                YR = YR + 1;
            }
            else if (ddlMonths.SelectedValue == "February")
            {
                YR = Convert.ToInt32(ddlFinance.SelectedValue);
                YR = YR + 1;
            }
            else if (ddlMonths.SelectedValue == "March")
            {
                YR = Convert.ToInt32(ddlFinance.SelectedValue);
                YR = YR + 1;
            }
            else
            {
                YR = Convert.ToInt32(ddlFinance.SelectedValue);
            }
            if (ddlcategory.SelectedValue == "1")
            {
                Stafflabour = "S";
            }
            else if (ddlcategory.SelectedValue == "2")
            {
                Stafflabour = "L";
            }
            if (ddldept.SelectedValue == "0")
            {
                if (rbsalary.SelectedValue == "2")
                {
                    query = "Select ''as Remaining_Days,SalDet.EmpNo,EmpDet.ExisistingCode,EmpDet.EmpName,EmpDet.Designation,EmpDet.Department,SalDet.Totalworkingdays, " +
                            " SalDet.NFh,SalDet.BasicandDA,SalDet.FDA,SalDet.VDA,SalDet.HRA, SalDet.Advance,SalDet.Stamp,SalDet.Unioncg,(SalDet.WorkedDays + SalDet.NFh) as tot, " +
                            " SalDet.Words,SalDet.WorkedDays,SalDet.Month,SalDet.Year,SalDet.LeaveDays,SalDet.SalaryDate,SalDet.allowances1,SalDet.BonusDays,SalDet.NfhCount,SalDet.NfhPresent,SalDet.FineAmt,SalDet.WHPresent," +
                            " SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.allowances5,SalDet.Deduction1,SalDet.Weekoff,SalDet.CL,SalDet.Deduction2,OP.AccNo, " +
                            " SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5,SalDet.Losspay,SalDet.HomeDays,SalDet.NetPay,SalDet.TotalDeductions,SalDet.GrossEarnings, " +
                            " SalDet.OverTime,MstDpt.DepartmentNm,SalDet.ProvidentFund,SalDet.ESI,SalDet.HRA,SalDet.WorkedDays,sum(SalDet.NetPay - (SalDet.GrossEarnings - SalDet.TotalDeductions)) as Roundoff," +
                            " SalDet.PfSalary as PFEarnings, SalDet.HalfNightAmt,SalDet.FullNightAmt,SalDet.SpinningAmt,SalDet.DayIncentive,SalDet.ThreesidedAmt,SM.Base from EmployeeDetails EmpDet inner Join SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo " +
                            " inner Join MstDepartment as MstDpt on MstDpt.DepartmentCd = EmpDet.Department inner Join OfficialProfile OP on OP.EmpNo=SalDet.EmpNo inner Join SalaryMaster SM on SM.EmpNo=SalDet.EmpNo where " +
                            " SalDet.Month='" + ddlMonths.SelectedValue + "' AND SalDet.FinancialYear='" + ddlFinance.SelectedValue + "' and EmpDet.StafforLabor='" + Stafflabour + "' and " +
                            " EmpDet.Ccode='" + SessionCcode + "' and EmpDet.Lcode='" + SessionLcode + "' and OP.WagesType='" + rbsalary.SelectedValue + "' group by SalDet.EmpNo,EmpDet.ExisistingCode,EmpDet.EmpName,EmpDet.Designation,EmpDet.Department,SalDet.Totalworkingdays, " +
                            "SalDet.NFh,SalDet.BasicandDA,SalDet.FDA,SalDet.VDA,SalDet.HRA, SalDet.Advance,SalDet.Stamp,SalDet.Unioncg,SalDet.Words,SalDet.WorkedDays,SalDet.Month,SalDet.Year,SalDet.LeaveDays,SalDet.SalaryDate,SalDet.allowances1, " +
                            " SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.allowances5,SalDet.HomeDays,SalDet.Deduction1,SalDet.Weekoff,SalDet.CL,SalDet.Deduction2, SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5,SalDet.Losspay,SalDet.NetPay,SalDet.TotalDeductions,SalDet.GrossEarnings," +
                            "SalDet.OverTime,MstDpt.DepartmentNm,SalDet.ProvidentFund,SalDet.ESI,SalDet.HRA,SalDet.WorkedDays, SalDet.PfSalary,SM.Base, SalDet.HalfNightAmt,SalDet.FullNightAmt,SalDet.SpinningAmt,SalDet.DayIncentive,SalDet.ThreesidedAmt,SalDet.BonusDays,SalDet.NfhCount,SalDet.NfhPresent,SalDet.FineAmt,SalDet.WHPresent,OP.AccNo";
                }
                else
                {
                    query = "Select ''as Remaining_Days,SalDet.EmpNo,EmpDet.ExisistingCode,EmpDet.EmpName,EmpDet.Designation,EmpDet.Department,SalDet.Totalworkingdays, " +
                            " SalDet.NFh,SalDet.BasicandDA,SalDet.FDA,SalDet.VDA,SalDet.HRA, SalDet.Advance,SalDet.Stamp,SalDet.Unioncg,(SalDet.WorkedDays + SalDet.NFh) as tot, " +
                            " SalDet.Words,SalDet.WorkedDays,SalDet.Month,SalDet.Year,SalDet.LeaveDays,SalDet.SalaryDate,SalDet.allowances1,SalDet.BonusDays,SalDet.NfhCount,SalDet.NfhPresent,SalDet.FineAmt,SalDet.WHPresent," +
                            " SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.allowances5,SalDet.Deduction1,SalDet.Weekoff,SalDet.CL,SalDet.Deduction2,OP.AccNo, " +
                            " SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5,SalDet.Deduction5,SalDet.HomeDays,SalDet.Losspay,SalDet.NetPay,SalDet.TotalDeductions,SalDet.GrossEarnings, " +
                            " SalDet.OverTime,MstDpt.DepartmentNm,SalDet.ProvidentFund,SalDet.ESI,SalDet.HRA,SalDet.WorkedDays,sum(SalDet.NetPay - (SalDet.GrossEarnings - SalDet.TotalDeductions)) as Roundoff," +
                            " SalDet.PfSalary as PFEarnings,SalDet.HalfNightAmt,SalDet.FullNightAmt,SalDet.SpinningAmt,SalDet.DayIncentive,SalDet.ThreesidedAmt,SM.Base from EmployeeDetails EmpDet inner Join SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo " +
                            " inner Join MstDepartment as MstDpt on MstDpt.DepartmentCd = EmpDet.Department inner Join OfficialProfile OP on OP.EmpNo=SalDet.EmpNo inner Join SalaryMaster SM on SM.EmpNo=SalDet.EmpNo where " +
                            " SalDet.Month='" + ddlMonths.SelectedValue + "' AND SalDet.FinancialYear='" + ddlFinance.SelectedValue + "' and EmpDet.StafforLabor='" + Stafflabour + "' and " +
                            " EmpDet.Ccode='" + SessionCcode + "' and EmpDet.Lcode='" + SessionLcode + "' and OP.WagesType='" + rbsalary.SelectedValue + "' and SalDet.FromDate = convert(datetime,'" + MyDate1 + "', 105) and SalDet.ToDate = Convert(Datetime,'" + MyDate2 + "', 105) group by SalDet.EmpNo,EmpDet.ExisistingCode,EmpDet.EmpName,EmpDet.Designation,EmpDet.Department,SalDet.Totalworkingdays, " +
                            "SalDet.NFh,SalDet.BasicandDA,SalDet.FDA,SalDet.VDA,SalDet.HRA,SalDet.HomeDays, SalDet.Advance,SalDet.Stamp,SalDet.Unioncg,SalDet.Words,SalDet.WorkedDays,SalDet.Month,SalDet.Year,SalDet.LeaveDays,SalDet.SalaryDate,SalDet.allowances1, " +
                            " SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.allowances5,SalDet.Deduction1,SalDet.Weekoff,SalDet.CL,SalDet.Deduction2, SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5,SalDet.Losspay,SalDet.NetPay,SalDet.TotalDeductions,SalDet.GrossEarnings," +
                            "SalDet.OverTime,MstDpt.DepartmentNm,SalDet.ProvidentFund,SalDet.ESI,SalDet.HRA,SalDet.WorkedDays, SalDet.PfSalary,SM.Base, SalDet.HalfNightAmt,SalDet.FullNightAmt,SalDet.SpinningAmt,SalDet.DayIncentive,SalDet.ThreesidedAmt,SalDet.BonusDays,SalDet.NfhCount,SalDet.NfhPresent,SalDet.FineAmt,SalDet.WHPresent,OP.AccNo";
                }
            }
            else
            {
                if (rbsalary.SelectedValue == "2")
                {
                    query = "Select ''as Remaining_Days,SalDet.EmpNo,EmpDet.ExisistingCode,EmpDet.EmpName,EmpDet.Designation,EmpDet.Department,SalDet.Totalworkingdays, " +
                            " SalDet.NFh,SalDet.BasicandDA,SalDet.FDA,SalDet.VDA,SalDet.HRA, SalDet.Advance,SalDet.Stamp,SalDet.Unioncg,(SalDet.WorkedDays + SalDet.NFh) as tot, " +
                            " SalDet.Words,SalDet.WorkedDays,SalDet.Month,SalDet.Year,SalDet.LeaveDays,SalDet.SalaryDate,SalDet.allowances1,(round(SalDet.Netpay/5,0)*5) as Val,SalDet.BonusDays,SalDet.NfhCount,SalDet.NfhPresent,SalDet.FineAmt,SalDet.WHPresent, " +
                            " SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.allowances5,SalDet.HomeDays,SalDet.Deduction1,SalDet.Weekoff,SalDet.CL,SalDet.Deduction2, " +
                            " SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5,SalDet.Losspay,SalDet.NetPay,SalDet.TotalDeductions,SalDet.GrossEarnings,OP.AccNo, " +
                            " SalDet.OverTime,MstDpt.DepartmentNm,SalDet.ProvidentFund,SalDet.ESI,SalDet.HRA,SalDet.WorkedDays,sum(SalDet.NetPay - (SalDet.GrossEarnings - SalDet.TotalDeductions)) as Roundoff," +
                            " SalDet.PfSalary as PFEarnings, SalDet.HalfNightAmt,SalDet.FullNightAmt,SalDet.SpinningAmt,SalDet.DayIncentive,SalDet.ThreesidedAmt,SM.Base from EmployeeDetails EmpDet inner Join SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo " +
                            " inner Join MstDepartment as MstDpt on MstDpt.DepartmentCd = EmpDet.Department inner Join OfficialProfile OP on OP.EmpNo=SalDet.EmpNo inner Join SalaryMaster SM on SM.EmpNo=SalDet.EmpNo where " +
                            " SalDet.Month='" + ddlMonths.SelectedValue + "' AND SalDet.FinancialYear='" + ddlFinance.SelectedValue + "' and EmpDet.StafforLabor='" + Stafflabour + "' and " +
                            " EmpDet.EmployeeType='" + ddldept.SelectedValue + "' and EmpDet.Ccode='" + SessionCcode + "' and OP.WagesType='" + rbsalary.SelectedValue + "' and EmpDet.Lcode='" + SessionLcode + "' group by SalDet.EmpNo,EmpDet.ExisistingCode,EmpDet.EmpName,EmpDet.Designation,EmpDet.Department,SalDet.Totalworkingdays, " +
                            "SalDet.NFh,SalDet.BasicandDA,SalDet.FDA,SalDet.VDA,SalDet.HRA, SalDet.Advance,SalDet.Stamp,SalDet.Unioncg,SalDet.Words,SalDet.WorkedDays,SalDet.Month,SalDet.Year,SalDet.LeaveDays,SalDet.SalaryDate,SalDet.allowances1, " +
                            " SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.HomeDays,SalDet.allowances5,SalDet.Deduction1,SalDet.Weekoff,SalDet.CL,SalDet.Deduction2, SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5,SalDet.Losspay,SalDet.NetPay,SalDet.TotalDeductions,SalDet.GrossEarnings," +
                            "SalDet.OverTime,MstDpt.DepartmentNm,SalDet.ProvidentFund,SalDet.ESI,SalDet.HRA,SalDet.WorkedDays, SalDet.PfSalary,SalDet.HalfNightAmt,SM.Base,SalDet.FullNightAmt,SalDet.SpinningAmt,SalDet.DayIncentive,SalDet.ThreesidedAmt,SalDet.BonusDays,SalDet.NfhCount,SalDet.NfhPresent,SalDet.FineAmt,SalDet.WHPresent,OP.AccNo";
                }
                else
                {
                    query = "Select ''as Remaining_Days,SalDet.EmpNo,EmpDet.ExisistingCode,EmpDet.EmpName,EmpDet.Department,SalDet.Totalworkingdays, " +
                            " SalDet.BasicandDA,(SalDet.WorkedDays + SalDet.NFh) as tot, " +
                            " SalDet.Words,SalDet.WorkedDays,SalDet.Month,SalDet.Year,SalDet.LeaveDays,SalDet.SalaryDate," +

                            " SalDet.NetPay,SalDet.TotalDeductions,SalDet.GrossEarnings,SalDet.BonusDays,SalDet.NfhCount,SalDet.NfhPresent,SalDet.FineAmt,SalDet.WHPresent, " +
                            " SalDet.OverTime,MstDpt.DepartmentNm,SalDet.HRA,SalDet.WorkedDays,OP.AccNo," +
                            " SalDet.DayIncentive,SM.Base from EmployeeDetails EmpDet inner Join SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo " +
                            " inner Join MstDepartment as MstDpt on MstDpt.DepartmentCd = EmpDet.Department inner Join OfficialProfile OP on OP.EmpNo=SalDet.EmpNo inner Join SalaryMaster SM on SM.EmpNo=SalDet.EmpNo where " +
                            " SalDet.Month='" + ddlMonths.SelectedValue + "' AND SalDet.FinancialYear='" + ddlFinance.SelectedValue + "' and EmpDet.StafforLabor='" + Stafflabour + "' and " +
                            " EmpDet.Ccode='" + SessionCcode + "' and EmpDet.Lcode='" + SessionLcode + "' and OP.WagesType='" + rbsalary.SelectedValue + "' and SalDet.FromDate = convert(datetime,'" + MyDate1 + "', 105) and SalDet.ToDate = Convert(Datetime,'" + MyDate2 + "', 105) group by SalDet.EmpNo,EmpDet.ExisistingCode,EmpDet.EmpName,EmpDet.Designation,EmpDet.Department,SalDet.Totalworkingdays, " +
                            "SalDet.NFh,SalDet.BasicandDA,SalDet.FDA,SalDet.VDA,SalDet.HRA,SalDet.HomeDays,SalDet.Advance,SalDet.Stamp,SalDet.Unioncg,SalDet.Words,SalDet.WorkedDays,SalDet.Month,SalDet.Year,SalDet.LeaveDays,SalDet.SalaryDate,SalDet.allowances1, " +
                            " SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.allowances5,SalDet.Deduction1,SalDet.Weekoff,SalDet.CL,SalDet.Deduction2, SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5,SalDet.Losspay,SalDet.NetPay,SalDet.TotalDeductions,SalDet.GrossEarnings," +
                            "SalDet.OverTime,MstDpt.DepartmentNm,SalDet.ProvidentFund,SalDet.ESI,SalDet.HRA,SalDet.WorkedDays,SalDet.PfSalary,SalDet.HalfNightAmt,SM.Base,SalDet.FullNightAmt,SalDet.SpinningAmt,SalDet.DayIncentive,SalDet.ThreesidedAmt,SalDet.BonusDays,SalDet.NfhCount,SalDet.NfhPresent,SalDet.FineAmt,SalDet.WHPresent,OP.AccNo";
                }

            }


            if (ddldept.SelectedValue == "2" && rbsalary.SelectedValue == "2") // Permanent
            {
                query = "Select SalDet.EmpNo,EmpDet.ExisistingCode,MstDpt.DepartmentNm,EmpDet.EmpName,SalDet.WorkedDays,OP.UAN, " +
                    " SM.Base,SalDet.BasicandDA,SalDet.TotalBasic,SalDet.HRA,SalDet.FDA,SalDet.Deduction3, SalDet.Deduction4, SalDet.Deduction5, " +
                    " Sum(SpinningAmt + allowances1 + BasicandDA) as Total_Stipend,SalDet.PfSalary,SalDet.ESI,SalDet.GrossEarnings,OP.AccNo," +
                    " CAST(EmpDet.ExisistingCode AS int) as OrderToken,(round(SalDet.Netpay/5,0)*5) as Val, " +
                    " SalDet.withpay,SalDet.Availabledays, " +
                    //" SalDet.Deduction1,SalDet.Deduction2,SalDet.Deduction3,SalDet.Deduction4, " +
                    " SalDet.TotalDeductions,SalDet.OverTime,SalDet.BonusDays,SalDet.NfhCount,SalDet.NfhPresent,SalDet.FineAmt,SalDet.WHPresent, " +
                    " SalDet.NetPay,SalDet.Conveyance,SalDet.Washing from EmployeeDetails " +
                    " EmpDet inner Join SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo " +
                    " inner Join MstDepartment as MstDpt on MstDpt.DepartmentCd = EmpDet.Department " +
                    " inner Join OfficialProfile OP on OP.EmpNo=SalDet.EmpNo " +
                    " inner Join SalaryMaster SM on SM.EmpNo=SalDet.EmpNo where SalDet.Month='" + ddlMonths.SelectedValue + "' " +
                    " AND SalDet.FinancialYear='" + ddlFinance.SelectedValue + "' and EmpDet.StafforLabor='" + Stafflabour + "' and " + 
                    " EmpDet.EmployeeType='" + ddldept.SelectedValue + "' and EmpDet.Ccode='" + SessionCcode + "' and " + 
                    " EmpDet.Lcode='" + SessionLcode + "' and OP.WagesType='" + rbsalary.SelectedValue + "' " +
                    " group by SalDet.EmpNo,EmpDet.ExisistingCode,EmpDet.InsOuts,MstDpt.DepartmentNm,EmpDet.EmpName,SalDet.WorkedDays,SalDet.CL, " +
                    " SM.Base,SalDet.SpinningAmt,SalDet.BasicandDA,SalDet.PfSalary,SalDet.ESI, " +
                    " SalDet.Advance,SalDet.allowances1,SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.Deduction3, SalDet.Deduction4, SalDet.Deduction5, " +
                    " SalDet.allowances5,SalDet.Deduction1,SalDet.Deduction2,SalDet.Deduction3,SalDet.Deduction4,SalDet.withpay,SalDet.Availabledays, " +
                    " SalDet.Deduction5,SalDet.TotalDeductions,OverTime,SalDet.NetPay,SalDet.GrossEarnings,SalDet.TotalBasic,SalDet.HRA,SalDet.FDA,SalDet.Conveyance,SalDet.Washing,SalDet.BonusDays,SalDet.NfhCount,SalDet.NfhPresent,SalDet.FineAmt,SalDet.WHPresent,OP.AccNo,OP.UAN Order by OrderToken Asc";
            }

            if (ddldept.SelectedValue == "4" && rbsalary.SelectedValue == "2") //ESI Worker
            {
                query = "Select SalDet.EmpNo,EmpDet.ExisistingCode,MstDpt.DepartmentNm,EmpDet.EmpName,SalDet.WorkedDays, " +
                    " SM.Base,SalDet.BasicandDA,SalDet.TotalBasic,SalDet.HRA,SalDet.FDA,SalDet.Deduction3, SalDet.Deduction4, SalDet.Deduction5, " +
                    " Sum(SpinningAmt + allowances1 + BasicandDA) as Total_Stipend,SalDet.PfSalary,SalDet.ESI,SalDet.GrossEarnings,OP.AccNo," +
                    " CAST(EmpDet.ExisistingCode AS int) as OrderToken,(round(SalDet.Netpay/5,0)*5) as Val,OP.UAN, " +
                    " SalDet.withpay,SalDet.Availabledays, " +
                    //" SalDet.Deduction1,SalDet.Deduction2,SalDet.Deduction3,SalDet.Deduction4, " +
                    " SalDet.TotalDeductions,SalDet.OverTime,SalDet.BonusDays,SalDet.NfhCount,SalDet.NfhPresent,SalDet.FineAmt,SalDet.WHPresent, " +
                    " SalDet.NetPay,SalDet.Conveyance,SalDet.Washing from EmployeeDetails " +
                    " EmpDet inner Join SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo " +
                    " inner Join MstDepartment as MstDpt on MstDpt.DepartmentCd = EmpDet.Department " +
                    " inner Join OfficialProfile OP on OP.EmpNo=SalDet.EmpNo " +
                    " inner Join SalaryMaster SM on SM.EmpNo=SalDet.EmpNo where SalDet.Month='" + ddlMonths.SelectedValue + "' " +
                    " AND SalDet.FinancialYear='" + ddlFinance.SelectedValue + "' and EmpDet.StafforLabor='" + Stafflabour + "' and " +
                    " EmpDet.EmployeeType='" + ddldept.SelectedValue + "' and EmpDet.Ccode='" + SessionCcode + "' and " +
                    " EmpDet.Lcode='" + SessionLcode + "' and OP.WagesType='" + rbsalary.SelectedValue + "' " +
                    " group by SalDet.EmpNo,EmpDet.ExisistingCode,EmpDet.InsOuts,MstDpt.DepartmentNm,EmpDet.EmpName,SalDet.WorkedDays,SalDet.CL, " +
                    " SM.Base,SalDet.SpinningAmt,SalDet.BasicandDA,SalDet.PfSalary,SalDet.ESI, " +
                    " SalDet.Advance,SalDet.allowances1,SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.Deduction3, SalDet.Deduction4, SalDet.Deduction5, " +
                    " SalDet.allowances5,SalDet.Deduction1,SalDet.Deduction2,SalDet.Deduction3,SalDet.Deduction4,SalDet.withpay,SalDet.Availabledays, " +
                    " SalDet.Deduction5,SalDet.TotalDeductions,OverTime,SalDet.NetPay,SalDet.GrossEarnings,SalDet.TotalBasic,SalDet.HRA,SalDet.FDA,SalDet.Conveyance,SalDet.Washing,SalDet.BonusDays,SalDet.NfhCount,SalDet.NfhPresent,SalDet.FineAmt,SalDet.WHPresent,OP.AccNo,OP.UAN Order by OrderToken Asc";
            }

            if (ddldept.SelectedValue == "3" && rbsalary.SelectedValue == "2") // Temporary Worker
            {

                query = "Select SalDet.EmpNo,EmpDet.ExisistingCode,MstDpt.DepartmentNm,EmpDet.EmpName,SalDet.WorkedDays,OP.UAN, " +
                " SM.Base,SalDet.BasicandDA,SalDet.TotalBasic,SalDet.HRA,SalDet.FDA,SalDet.Deduction3, SalDet.Deduction4, SalDet.Deduction5, " +
                " Sum(SpinningAmt + allowances1 + BasicandDA) as Total_Stipend,SalDet.PfSalary,SalDet.ESI,SalDet.GrossEarnings,OP.AccNo," +
                " CAST(EmpDet.ExisistingCode AS int) as OrderToken,(round(SalDet.Netpay/5,0)*5) as Val, " +
                " SalDet.withpay,SalDet.Availabledays, " +
                    //" SalDet.Deduction1,SalDet.Deduction2,SalDet.Deduction3,SalDet.Deduction4, " +
                " SalDet.TotalDeductions,SalDet.OverTime,SalDet.BonusDays,SalDet.NfhCount,SalDet.NfhPresent,SalDet.FineAmt,SalDet.WHPresent, " +
                " SalDet.NetPay from EmployeeDetails " +
                " EmpDet inner Join SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo " +
                " inner Join MstDepartment as MstDpt on MstDpt.DepartmentCd = EmpDet.Department " +
                " inner Join OfficialProfile OP on OP.EmpNo=SalDet.EmpNo " +
                " inner Join SalaryMaster SM on SM.EmpNo=SalDet.EmpNo where SalDet.Month='" + ddlMonths.SelectedValue + "' " +
                " AND SalDet.FinancialYear='" + ddlFinance.SelectedValue + "' and EmpDet.StafforLabor='" + Stafflabour + "' and " +
                " EmpDet.EmployeeType='" + ddldept.SelectedValue + "' and EmpDet.Ccode='" + SessionCcode + "' and " +
                " EmpDet.Lcode='" + SessionLcode + "' and OP.WagesType='" + rbsalary.SelectedValue + "' " +
                " group by SalDet.EmpNo,EmpDet.ExisistingCode,EmpDet.InsOuts,MstDpt.DepartmentNm,EmpDet.EmpName,SalDet.WorkedDays,SalDet.CL, " +
                " SM.Base,SalDet.SpinningAmt,SalDet.BasicandDA,SalDet.PfSalary,SalDet.ESI, " +
                " SalDet.Advance,SalDet.allowances1,SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.Deduction3, SalDet.Deduction4, SalDet.Deduction5, " +
                " SalDet.allowances5,SalDet.Deduction1,SalDet.Deduction2,SalDet.Deduction3,SalDet.Deduction4,SalDet.withpay,SalDet.Availabledays, " +
                " SalDet.Deduction5,SalDet.TotalDeductions,OverTime,SalDet.NetPay,SalDet.GrossEarnings,SalDet.TotalBasic,SalDet.HRA,SalDet.FDA,SalDet.BonusDays,SalDet.NfhCount,SalDet.NfhPresent,SalDet.FineAmt,SalDet.WHPresent,OP.AccNo,OP.UAN Order by OrderToken Asc";

 
            }

            if (ddldept.SelectedValue == "6" && rbsalary.SelectedValue == "2") // Driver Worker
            {
                query = "Select SalDet.EmpNo,EmpDet.ExisistingCode,MstDpt.DepartmentNm,EmpDet.EmpName,SalDet.WorkedDays,OP.UAN, " +
                   " SM.Base,SalDet.BasicandDA,SalDet.TotalBasic,SalDet.HRA,SalDet.FDA,SalDet.Deduction3, SalDet.Deduction4, SalDet.Deduction5, " +
                   " Sum(SpinningAmt + allowances1 + BasicandDA) as Total_Stipend,SalDet.PfSalary,SalDet.ESI,SalDet.GrossEarnings,OP.AccNo," +
                   " CAST(EmpDet.ExisistingCode AS int) as OrderToken,(round(SalDet.Netpay/5,0)*5) as Val, " +
                   " SalDet.withpay,SalDet.Availabledays, " +
                    //" SalDet.Deduction1,SalDet.Deduction2,SalDet.Deduction3,SalDet.Deduction4, " +
                   " SalDet.TotalDeductions,SalDet.OverTime,SalDet.BonusDays,SalDet.NfhCount,SalDet.NfhPresent,SalDet.FineAmt,SalDet.WHPresent, " +
                   " SalDet.NetPay,SalDet.Conveyance,SalDet.Washing from EmployeeDetails " +
                   " EmpDet inner Join SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo " +
                   " inner Join MstDepartment as MstDpt on MstDpt.DepartmentCd = EmpDet.Department " +
                   " inner Join OfficialProfile OP on OP.EmpNo=SalDet.EmpNo " +
                   " inner Join SalaryMaster SM on SM.EmpNo=SalDet.EmpNo where SalDet.Month='" + ddlMonths.SelectedValue + "' " +
                   " AND SalDet.FinancialYear='" + ddlFinance.SelectedValue + "' and EmpDet.StafforLabor='" + Stafflabour + "' and " +
                   " EmpDet.EmployeeType='" + ddldept.SelectedValue + "' and EmpDet.Ccode='" + SessionCcode + "' and " +
                   " EmpDet.Lcode='" + SessionLcode + "' and OP.WagesType='" + rbsalary.SelectedValue + "' " +
                   " group by SalDet.EmpNo,EmpDet.ExisistingCode,EmpDet.InsOuts,MstDpt.DepartmentNm,EmpDet.EmpName,SalDet.WorkedDays,SalDet.CL, " +
                   " SM.Base,SalDet.SpinningAmt,SalDet.BasicandDA,SalDet.PfSalary,SalDet.ESI, " +
                   " SalDet.Advance,SalDet.allowances1,SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.Deduction3, SalDet.Deduction4, SalDet.Deduction5, " +
                   " SalDet.allowances5,SalDet.Deduction1,SalDet.Deduction2,SalDet.Deduction3,SalDet.Deduction4,SalDet.withpay,SalDet.Availabledays, " +
                   " SalDet.Deduction5,SalDet.TotalDeductions,OverTime,SalDet.NetPay,SalDet.GrossEarnings,SalDet.TotalBasic,SalDet.HRA,SalDet.FDA,SalDet.Conveyance,SalDet.Washing,SalDet.BonusDays,SalDet.NfhCount,SalDet.NfhPresent,SalDet.FineAmt,SalDet.WHPresent,OP.AccNo,OP.UAN Order by OrderToken Asc";


            }


            if (ddldept.SelectedValue == "1" && rbsalary.SelectedValue == "2") // Staff Worker
            {
                query = "Select SalDet.EmpNo,EmpDet.ExisistingCode,MstDpt.DepartmentNm,EmpDet.EmpName,SalDet.WorkedDays,OP.UAN, " +
                              " sum(SM.Base + SM.HRA + SM.FDA) as Base,SalDet.BasicandDA,SalDet.TotalBasic,SalDet.HRA,SalDet.FDA,SalDet.Deduction3, SalDet.Deduction4, SalDet.Deduction5, " +
                              " Sum(SpinningAmt + allowances1 + BasicandDA) as Total_Stipend,SalDet.PfSalary,SalDet.ESI,SalDet.GrossEarnings,OP.AccNo," +
                              " CAST(EmpDet.ExisistingCode AS int) as OrderToken,(round(SalDet.Netpay/5,0)*5) as Val, " +
                              " SalDet.withpay,SalDet.Availabledays,SalDet.BonusDays,SalDet.NfhCount,SalDet.NfhPresent,SalDet.FineAmt,SalDet.WHPresent, " +
                              //" SalDet.Deduction1,SalDet.Deduction2,SalDet.Deduction3,SalDet.Deduction4, " +
                              " SalDet.TotalDeductions,SalDet.OverTime, " +
                              " SalDet.NetPay,SalDet.Conveyance,SalDet.Washing from EmployeeDetails " +
                              " EmpDet inner Join SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo " +
                              " inner Join MstDepartment as MstDpt on MstDpt.DepartmentCd = EmpDet.Department " +
                              " inner Join OfficialProfile OP on OP.EmpNo=SalDet.EmpNo " +
                              " inner Join SalaryMaster SM on SM.EmpNo=SalDet.EmpNo where SalDet.Month='" + ddlMonths.SelectedValue + "' " +
                              " AND SalDet.FinancialYear='" + ddlFinance.SelectedValue + "' and EmpDet.StafforLabor='" + Stafflabour + "' and " +
                              " EmpDet.EmployeeType='" + ddldept.SelectedValue + "' and EmpDet.Ccode='" + SessionCcode + "' and " +
                              " EmpDet.Lcode='" + SessionLcode + "' and OP.WagesType='" + rbsalary.SelectedValue + "' " +
                              " group by SalDet.EmpNo,EmpDet.ExisistingCode,EmpDet.InsOuts,MstDpt.DepartmentNm,EmpDet.EmpName,SalDet.WorkedDays,SalDet.CL, " +
                              " SM.Base,SalDet.SpinningAmt,SalDet.BasicandDA,SalDet.PfSalary,SalDet.ESI, " +
                              " SalDet.Advance,SalDet.allowances1,SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.Deduction3, SalDet.Deduction4, SalDet.Deduction5, " +
                              " SalDet.allowances5,SalDet.Deduction1,SalDet.Deduction2,SalDet.Deduction3,SalDet.Deduction4,SalDet.withpay,SalDet.Availabledays, " +
                              " SalDet.Deduction5,SalDet.TotalDeductions,OverTime,SalDet.NetPay,SalDet.GrossEarnings,SalDet.TotalBasic,SalDet.HRA,SalDet.FDA,SalDet.Conveyance,SalDet.Washing,SalDet.BonusDays,SalDet.NfhCount,SalDet.NfhPresent,SalDet.FineAmt,SalDet.WHPresent,OP.AccNo,OP.UAN Order by OrderToken Asc";
            }


            if (ddldept.SelectedValue == "7" && rbsalary.SelectedValue == "2") // Staff Worker
            {
                query = "Select SalDet.EmpNo,OP.AccNo,EmpDet.ExisistingCode,MstDpt.DepartmentNm,EmpDet.EmpName,SalDet.WorkedDays,OP.UAN, " +
                              " sum(SM.Base + SM.HRA + SM.FDA) as Base,SalDet.BasicandDA,SalDet.TotalBasic,SalDet.HRA,SalDet.FDA,SalDet.Deduction3, SalDet.Deduction4, SalDet.Deduction5, " +
                              " Sum(SpinningAmt + allowances1 + BasicandDA) as Total_Stipend,SalDet.PfSalary,SalDet.ESI,SalDet.GrossEarnings," +
                              " CAST(EmpDet.ExisistingCode AS int) as OrderToken,(round(SalDet.Netpay/5,0)*5) as Val, " +
                              " SalDet.withpay,SalDet.Availabledays,SalDet.BonusDays,SalDet.NfhCount,SalDet.NfhPresent,SalDet.FineAmt,SalDet.WHPresent, " +
                    //" SalDet.Deduction1,SalDet.Deduction2,SalDet.Deduction3,SalDet.Deduction4, " +
                              " SalDet.TotalDeductions,SalDet.OverTime, " +
                              " SalDet.NetPay,SalDet.Conveyance,SalDet.Washing from EmployeeDetails " +
                              " EmpDet inner Join SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo " +
                              " inner Join MstDepartment as MstDpt on MstDpt.DepartmentCd = EmpDet.Department " +
                              " inner Join OfficialProfile OP on OP.EmpNo=SalDet.EmpNo " +
                              " inner Join SalaryMaster SM on SM.EmpNo=SalDet.EmpNo where SalDet.Month='" + ddlMonths.SelectedValue + "' " +
                              " AND SalDet.FinancialYear='" + ddlFinance.SelectedValue + "' and EmpDet.StafforLabor='" + Stafflabour + "' and " +
                              " EmpDet.EmployeeType='" + ddldept.SelectedValue + "' and EmpDet.Ccode='" + SessionCcode + "' and " +
                              " EmpDet.Lcode='" + SessionLcode + "' and OP.WagesType='" + rbsalary.SelectedValue + "' " +
                              " group by SalDet.EmpNo,EmpDet.ExisistingCode,EmpDet.InsOuts,MstDpt.DepartmentNm,EmpDet.EmpName,SalDet.WorkedDays,SalDet.CL, " +
                              " SM.Base,SalDet.SpinningAmt,SalDet.BasicandDA,SalDet.PfSalary,SalDet.ESI, " +
                              " SalDet.Advance,SalDet.allowances1,SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.Deduction3, SalDet.Deduction4, SalDet.Deduction5, " +
                              " SalDet.allowances5,SalDet.Deduction1,SalDet.Deduction2,SalDet.Deduction3,SalDet.Deduction4,SalDet.withpay,SalDet.Availabledays, " +
                              " SalDet.Deduction5,SalDet.TotalDeductions,OverTime,SalDet.NetPay,SalDet.GrossEarnings,SalDet.TotalBasic,SalDet.HRA,SalDet.FDA,SalDet.Conveyance,SalDet.Washing,SalDet.BonusDays,SalDet.NfhCount,SalDet.NfhPresent,SalDet.FineAmt,SalDet.WHPresent,OP.AccNo,OP.UAN Order by OrderToken Asc";
            }
            
            SqlCommand cmd = new SqlCommand(query, con);
            DataTable dt_1 = new DataTable();
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            //DataSet ds1 = new DataSet();
            con.Open();
            sda.Fill(dt_1);
            con.Close();
            if (ddldept.SelectedValue == "1")
            {
                //SchemeWorkerGridView.DataSource = dt_1;
                //SchemeWorkerGridView.DataBind();

                StaffWorkerNew.DataSource = dt_1;
                StaffWorkerNew.DataBind();

                //CasualWorkerGridView.DataSource = dt_1;
                //CasualWorkerGridView.DataBind();
            }
            else if (ddldept.SelectedValue == "7")  //Security
            {
                SchemeWorkerGridView.DataSource = dt_1;
                SchemeWorkerGridView.DataBind();

                //CasualWorkerGridView.DataSource = dt_1;
                //CasualWorkerGridView.DataBind();
            }


            else if (ddldept.SelectedValue == "2")  // Permanent Worker
            {
                SchemeWorkerGridView.DataSource = dt_1;
                SchemeWorkerGridView.DataBind();
            }
            else if (ddldept.SelectedValue == "4")    //ESI Worker
            {
                SchemeWorkerGridView.DataSource = dt_1;
                SchemeWorkerGridView.DataBind();
            }
            else if (ddldept.SelectedValue == "5")  // Other Worker
            {
                CasualWorkerGridView.DataSource = dt_1;
                CasualWorkerGridView.DataBind();

                            
            }
            else if (ddldept.SelectedValue == "6")  // Other Worker
            {
                SchemeWorkerGridView.DataSource = dt_1;
                SchemeWorkerGridView.DataBind();


            }

            else if (ddldept.SelectedValue == "3")  // Temporory Worker
            {
                CasualWorkerGridView.DataSource = dt_1;
                CasualWorkerGridView.DataBind();
            }
            else
            {
                gvSalary.DataSource = dt_1;
                gvSalary.DataBind();
            }

            if (ddldept.SelectedValue == "1") //Casual Worker OverTime
            {
                //foreach (GridViewRow gvsal in CasualWorkerGridView.Rows)
                //{
                //    Label lbl_EmpNo = (Label)gvsal.FindControl("lblEmpNo");
                //    Label lbl_OTHours = (Label)gvsal.FindControl("lblOThours");
                //    string qry_ot = "";
                //    if (rbsalary.SelectedValue == "2")
                //    {
                //        qry_ot = "Select NoHrs from OverTime where EmpNo='" + lbl_EmpNo.Text.Trim() + "' and Lcode='" + SessionLcode + "' and Ccode='" + SessionCcode + "' and Month='" + ddlMonths.SelectedValue + "' and Financialyr='" + ddlFinance.SelectedValue + "'";
                //    }
                //    else
                //    {
                //        qry_ot = "Select NoHrs from OverTime where EmpNo='" + lbl_EmpNo.Text.Trim() + "' and Lcode='" + SessionLcode + "' and Ccode='" + SessionCcode + "' and Month='" + ddlMonths.SelectedValue + "' and Financialyr='" + ddlFinance.SelectedValue + "'" +
                //                        " and convert(Datetime, FromDate, 105)=convert(Datetime,'" + MyDate1 + "', 105) and convert(Datetime, ToDate, 105)=convert(datetime,'" + MyDate2 + "', 105)";

                //    }
                //    SqlCommand cmd_ot = new SqlCommand(qry_ot, con);
                //    con.Open();
                //    string val_ot = Convert.ToString(cmd_ot.ExecuteScalar());
                //    con.Close();
                //    if (val_ot.Trim() == "")
                //    {
                //        lbl_OTHours.Text = "0";
                //    }
                //    else
                //    {
                //        lbl_OTHours.Text = val_ot;
                //    }
                //}
            }
            else if (ddldept.SelectedValue == "4") //Scheme Worker OverTime
            {
                //foreach (GridViewRow gvsal in SchemeWorkerGridView.Rows)
                //{
                //    Label lbl_EmpNo = (Label)gvsal.FindControl("lblEmpNo");
                //    Label lbl_OTHours = (Label)gvsal.FindControl("lblOThours");
                //    string qry_ot = "";
                //    if (rbsalary.SelectedValue == "2")
                //    {
                //        qry_ot = "Select NoHrs from OverTime where EmpNo='" + lbl_EmpNo.Text.Trim() + "' and Lcode='" + SessionLcode + "' and Ccode='" + SessionCcode + "' and Month='" + ddlMonths.SelectedValue + "' and Financialyr='" + ddlFinance.SelectedValue + "'";
                //    }
                //    else
                //    {
                //        qry_ot = "Select NoHrs from OverTime where EmpNo='" + lbl_EmpNo.Text.Trim() + "' and Lcode='" + SessionLcode + "' and Ccode='" + SessionCcode + "' and Month='" + ddlMonths.SelectedValue + "' and Financialyr='" + ddlFinance.SelectedValue + "'" +
                //                        " and convert(Datetime, FromDate, 105)=convert(Datetime,'" + MyDate1 + "', 105) and convert(Datetime, ToDate, 105)=convert(datetime,'" + MyDate2 + "', 105)";

                //    }
                //    SqlCommand cmd_ot = new SqlCommand(qry_ot, con);
                //    con.Open();
                //    string val_ot = Convert.ToString(cmd_ot.ExecuteScalar());
                //    con.Close();
                //    if (val_ot.Trim() == "")
                //    {
                //        lbl_OTHours.Text = "0";
                //    }
                //    else
                //    {
                //        lbl_OTHours.Text = val_ot;
                //    }
                //}
            }

            else if (ddldept.SelectedValue == "5") //Non Scheme Worker OverTime
            {
                //foreach (GridViewRow gvsal in NonSchemeWorkerGridView.Rows)
                //{
                //    Label lbl_EmpNo = (Label)gvsal.FindControl("lblEmpNo");
                //    Label lbl_OTHours = (Label)gvsal.FindControl("lblOThours");
                //    string qry_ot = "";
                //    if (rbsalary.SelectedValue == "2")
                //    {
                //        qry_ot = "Select NoHrs from OverTime where EmpNo='" + lbl_EmpNo.Text.Trim() + "' and Lcode='" + SessionLcode + "' and Ccode='" + SessionCcode + "' and Month='" + ddlMonths.SelectedValue + "' and Financialyr='" + ddlFinance.SelectedValue + "'";
                //    }
                //    else
                //    {
                //        qry_ot = "Select NoHrs from OverTime where EmpNo='" + lbl_EmpNo.Text.Trim() + "' and Lcode='" + SessionLcode + "' and Ccode='" + SessionCcode + "' and Month='" + ddlMonths.SelectedValue + "' and Financialyr='" + ddlFinance.SelectedValue + "'" +
                //                        " and convert(Datetime, FromDate, 105)=convert(Datetime,'" + MyDate1 + "', 105) and convert(Datetime, ToDate, 105)=convert(datetime,'" + MyDate2 + "', 105)";

                //    }
                //    SqlCommand cmd_ot = new SqlCommand(qry_ot, con);
                //    con.Open();
                //    string val_ot = Convert.ToString(cmd_ot.ExecuteScalar());
                //    con.Close();
                //    if (val_ot.Trim() == "")
                //    {
                //        lbl_OTHours.Text = "0";
                //    }
                //    else
                //    {
                //        lbl_OTHours.Text = val_ot;
                //    }
                //}
            }

            else if (ddldept.SelectedValue == "2") //Staff Worker OverTime
            {
                //foreach (GridViewRow gvsal in StaffWorkerPFGridView.Rows)
                //{
                //    Label lbl_EmpNo = (Label)gvsal.FindControl("lblEmpNo");
                //    Label lbl_OTHours = (Label)gvsal.FindControl("lblOThours");
                //    string qry_ot = "";
                //    if (rbsalary.SelectedValue == "2")
                //    {
                //        qry_ot = "Select NoHrs from OverTime where EmpNo='" + lbl_EmpNo.Text.Trim() + "' and Lcode='" + SessionLcode + "' and Ccode='" + SessionCcode + "' and Month='" + ddlMonths.SelectedValue + "' and Financialyr='" + ddlFinance.SelectedValue + "'";
                //    }
                //    else
                //    {
                //        qry_ot = "Select NoHrs from OverTime where EmpNo='" + lbl_EmpNo.Text.Trim() + "' and Lcode='" + SessionLcode + "' and Ccode='" + SessionCcode + "' and Month='" + ddlMonths.SelectedValue + "' and Financialyr='" + ddlFinance.SelectedValue + "'" +
                //                        " and convert(Datetime, FromDate, 105)=convert(Datetime,'" + MyDate1 + "', 105) and convert(Datetime, ToDate, 105)=convert(datetime,'" + MyDate2 + "', 105)";

                //    }
                //    SqlCommand cmd_ot = new SqlCommand(qry_ot, con);
                //    con.Open();
                //    string val_ot = Convert.ToString(cmd_ot.ExecuteScalar());
                //    con.Close();
                //    if (val_ot.Trim() == "")
                //    {
                //        lbl_OTHours.Text = "0";
                //    }
                //    else
                //    {
                //        lbl_OTHours.Text = val_ot;
                //    }
                //}
            }
            else if (ddldept.SelectedValue == "3") //Hindi-boys  OverTime
            {
                //foreach (GridViewRow gvsal in PermanentWorkerGridView.Rows)
                //{
                //    Label lbl_EmpNo = (Label)gvsal.FindControl("lblEmpNo");
                //    Label lbl_OTHours = (Label)gvsal.FindControl("lblOThours");
                //    string qry_ot = "";
                //    if (rbsalary.SelectedValue == "2")
                //    {
                //        qry_ot = "Select NoHrs from OverTime where EmpNo='" + lbl_EmpNo.Text.Trim() + "' and Lcode='" + SessionLcode + "' and Ccode='" + SessionCcode + "' and Month='" + ddlMonths.SelectedValue + "' and Financialyr='" + ddlFinance.SelectedValue + "'";
                //    }
                //    else
                //    {
                //        qry_ot = "Select NoHrs from OverTime where EmpNo='" + lbl_EmpNo.Text.Trim() + "' and Lcode='" + SessionLcode + "' and Ccode='" + SessionCcode + "' and Month='" + ddlMonths.SelectedValue + "' and Financialyr='" + ddlFinance.SelectedValue + "'" +
                //                        " and convert(Datetime, FromDate, 105)=convert(Datetime,'" + MyDate1 + "', 105) and convert(Datetime, ToDate, 105)=convert(datetime,'" + MyDate2 + "', 105)";

                //    }
                //    SqlCommand cmd_ot = new SqlCommand(qry_ot, con);
                //    con.Open();
                //    string val_ot = Convert.ToString(cmd_ot.ExecuteScalar());
                //    con.Close();
                //    if (val_ot.Trim() == "")
                //    {
                //        lbl_OTHours.Text = "0";
                //    }
                //    else
                //    {
                //        lbl_OTHours.Text = val_ot;
                //    }
                //}
            }
            else
            {
                //foreach (GridViewRow gvsal in gvSalary.Rows)
                //{
                //    Label lbl_EmpNo = (Label)gvsal.FindControl("lblEmpNo");
                //    Label lbl_OTHours = (Label)gvsal.FindControl("lblOThours");
                //    string qry_ot = "";
                //    if (rbsalary.SelectedValue == "2")
                //    {
                //        qry_ot = "Select NoHrs from OverTime where EmpNo='" + lbl_EmpNo.Text.Trim() + "' and Lcode='" + SessionLcode + "' and Ccode='" + SessionCcode + "' and Month='" + ddlMonths.SelectedValue + "' and Financialyr='" + ddlFinance.SelectedValue + "'";
                //    }
                //    else
                //    {
                //        qry_ot = "Select NoHrs from OverTime where EmpNo='" + lbl_EmpNo.Text.Trim() + "' and Lcode='" + SessionLcode + "' and Ccode='" + SessionCcode + "' and Month='" + ddlMonths.SelectedValue + "' and Financialyr='" + ddlFinance.SelectedValue + "'" +
                //                        " and convert(Datetime, FromDate, 105)=convert(Datetime,'" + MyDate1 + "', 105) and convert(Datetime, ToDate, 105)=convert(datetime,'" + MyDate2 + "', 105)";

                //    }
                //    SqlCommand cmd_ot = new SqlCommand(qry_ot, con);
                //    con.Open();
                //    string val_ot = Convert.ToString(cmd_ot.ExecuteScalar());
                //    con.Close();
                //    if (val_ot.Trim() == "")
                //    {
                //        lbl_OTHours.Text = "0";
                //    }
                //    else
                //    {
                //        lbl_OTHours.Text = val_ot;
                //    }
                //}
            }
            string attachment = "attachment;filename=Payslip.xls";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/ms-excel";
            DataTable dt = new DataTable();
            dt = objdata.Company_retrive(SessionCcode, SessionLcode);
            if (dt.Rows.Count > 0)
            {
                CmpName = dt.Rows[0]["Cname"].ToString();
                Cmpaddress = (dt.Rows[0]["Address1"].ToString() + ", " + dt.Rows[0]["Address2"].ToString() + ", " + dt.Rows[0]["Location"].ToString() + "-" + dt.Rows[0]["Pincode"].ToString());
            }
            NetBase = "0";
            NetFDA = "0";
            NetVDA = "0";
            Nettotal = "0";
            NetPFEarnings = "0";
            NetPF = "0";
            NetESI = "0";
            NetUnion = "0";
            NetAdvance = "0";
            NetAll1 = "0";
            NetAll2 = "0";
            NetAll3 = "0";
            NetAll4 = "0";
            NetDed1 = "0";
            NetDed2 = "0";
            NetDed3 = "0";
            NetDed4 = "0";
            NetAll5 = "0";
            NetDed5 = "0";
            NetLOP = "0";
            NetStamp = "0";
            NetTotalDeduction = "0";
            NetOT = "0";
            NetAmt = "0";
            Network = "0";
            totCL = "0";
            totNFh = "0";
            totweekoff = "0";
            Roundoff = "0";
            HomeDays = "0";
            Halfnight = "0";
            FullNight = "0";
            Spinning = "0";
            DayIncentive = "0";
            ThreeSided = "0";
            //totwork = "0";
            DataTable Slap_DT = new DataTable();
            
            StringWriter stw = new StringWriter();
            HtmlTextWriter htextw = new HtmlTextWriter(stw);
            if (ddldept.SelectedValue == "1")
            {
                StaffWorkerNew.RenderControl(htextw);
            }
            else if (ddldept.SelectedValue == "7")
            {
                SchemeWorkerGridView.RenderControl(htextw);
            }
            else if (ddldept.SelectedValue == "2")
            {
                SchemeWorkerGridView.RenderControl(htextw);
            }
            else if (ddldept.SelectedValue == "4")
            {
                SchemeWorkerGridView.RenderControl(htextw);
            }
            else if (ddldept.SelectedValue == "5")
            {
                CasualWorkerGridView.RenderControl(htextw);

                             
            }
            else if (ddldept.SelectedValue == "6")
            {
                SchemeWorkerGridView.RenderControl(htextw);


            }
            else if (ddldept.SelectedValue == "3")
            {
                CasualWorkerGridView.RenderControl(htextw);
            }
            else
            {
                gvSalary.RenderControl(htextw);
            }
            Response.Write("<table>");
            Response.Write("<tr align='Center'>");
            Response.Write("<td colspan='12'>");
            Response.Write("<h3><b>" + CmpName + "</b></h3>");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("<tr align='Center'>");
            Response.Write("<td colspan='12'>");
            Response.Write(" UNIT-I");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("<tr align='Center'>");
            Response.Write("<td colspan='12'>");
            Response.Write("" + Cmpaddress + "");
            Response.Write("</td>");
            Response.Write("</tr>");
            if (rbsalary.SelectedValue == "2")
            {
                Response.Write("<tr>");
                Response.Write("<td colspan='10'>");
                Response.Write("Salary Month of " + ddlMonths.SelectedValue + " - " + YR);
                Response.Write("</td>");
                Response.Write("</tr>");
            }
            else
            {
                Response.Write("<tr>");
                Response.Write("<td colspan='10'>");
                Response.Write("Salary Month of " + txtfrom.Text + " - " + txtTo.Text);
                Response.Write("</td>");
                Response.Write("</tr>");
            }
            Response.Write("</table>");

            //gvSalary.RenderControl(htextw);
            //Response.Write("Contract Details");
            Response.Write(stw.ToString());

            Response.Write("<table border='1'>");
            Response.Write("<tr Font-Bold='true' align='right'>");
            Response.Write("</td>");
            if (ddldept.SelectedValue == "3")
            {
                Response.Write("<td font-Bold='true' colspan='5'>");
                Response.Write("Grand Total");
                Response.Write("</td>");
            }
            else if (ddldept.SelectedValue == "1" || ddldept.SelectedValue=="2")
            {
                Response.Write("<td font-Bold='true' colspan='6'>");
                Response.Write("Grand Total");
                Response.Write("</td>");
            }
            else
            {
                Response.Write("<td font-Bold='true' colspan='5'>");
                Response.Write("Grand Total");
                Response.Write("</td>");
            }

            //Get Count
            string Pay_RowCount = "0";
            if (ddldept.SelectedValue == "1")
            {
                Pay_RowCount = Convert.ToDecimal(StaffWorkerNew.Rows.Count + 6).ToString();
            }
            if (ddldept.SelectedValue == "7")
            {
                Pay_RowCount = Convert.ToDecimal(SchemeWorkerGridView.Rows.Count + 6).ToString();
            }
            if (ddldept.SelectedValue == "2")
            {
                Pay_RowCount = Convert.ToDecimal(SchemeWorkerGridView.Rows.Count + 6).ToString();
            }
            if (ddldept.SelectedValue == "4")
            {
                Pay_RowCount = Convert.ToDecimal(SchemeWorkerGridView.Rows.Count + 6).ToString();
            }
            if (ddldept.SelectedValue == "5")
            {
                Pay_RowCount = Convert.ToDecimal(CasualWorkerGridView.Rows.Count + 6).ToString();
             
            }
            if (ddldept.SelectedValue == "6")
            {
                Pay_RowCount = Convert.ToDecimal(SchemeWorkerGridView.Rows.Count + 6).ToString();

            }
            
            if (ddldept.SelectedValue == "3")
            {
                Pay_RowCount = Convert.ToDecimal(CasualWorkerGridView.Rows.Count + 6).ToString();
            }
            //if (ddldept.SelectedValue == "3") { Response.Write("<td>=sum(F6:F" + Pay_RowCount + ")</td>"); }
            //if (ddldept.SelectedValue == "1") { Response.Write("<td>=sum(F6:F" + Pay_RowCount + ")</td>"); }

            if (ddldept.SelectedValue == "2")
            {
                Response.Write("<td>=sum(G6:G" + Pay_RowCount + ")</td>");
                Response.Write("<td>=sum(H6:H" + Pay_RowCount + ")</td>");
                Response.Write("<td>=sum(I6:I" + Pay_RowCount + ")</td>");
                Response.Write("<td>=sum(J6:J" + Pay_RowCount + ")</td>");
                Response.Write("<td>=sum(K6:K" + Pay_RowCount + ")</td>");
                Response.Write("<td>=sum(L6:L" + Pay_RowCount + ")</td>");
                Response.Write("<td>=sum(M6:M" + Pay_RowCount + ")</td>");
                Response.Write("<td>=sum(N6:N" + Pay_RowCount + ")</td>");
                Response.Write("<td>=sum(O6:O" + Pay_RowCount + ")</td>");
                Response.Write("<td>=sum(P6:P" + Pay_RowCount + ")</td>");
                Response.Write("<td>=sum(Q6:Q" + Pay_RowCount + ")</td>");
                Response.Write("<td>=sum(R6:R" + Pay_RowCount + ")</td>");
                Response.Write("<td>=sum(S6:S" + Pay_RowCount + ")</td>");
                Response.Write("<td>=sum(T6:T" + Pay_RowCount + ")</td>");
                Response.Write("<td>=sum(U6:U" + Pay_RowCount + ")</td>");
                Response.Write("<td>=sum(V6:V" + Pay_RowCount + ")</td>");
                
            }

            if (ddldept.SelectedValue == "3")
            {
                //Response.Write("<td>=sum(G6:G" + Pay_RowCount + ")</td>");
                //Response.Write("<td>=sum(H6:H" + Pay_RowCount + ")</td>");
                //Response.Write("<td>=sum(I6:I" + Pay_RowCount + ")</td>");
                //Response.Write("<td>=sum(J6:J" + Pay_RowCount + ")</td>");
                //Response.Write("<td>=sum(K6:K" + Pay_RowCount + ")</td>");
                //Response.Write("<td>=sum(L6:L" + Pay_RowCount + ")</td>");
                Response.Write("<td>=sum(F6:F" + Pay_RowCount + ")</td>");
                Response.Write("<td>=sum(G6:G" + Pay_RowCount + ")</td>");
                Response.Write("<td>=sum(H6:H" + Pay_RowCount + ")</td>");
                Response.Write("<td>=sum(I6:I" + Pay_RowCount + ")</td>");
                Response.Write("<td>=sum(J6:J" + Pay_RowCount + ")</td>");
                Response.Write("<td>=sum(K6:K" + Pay_RowCount + ")</td>");
                Response.Write("<td>=sum(L6:L" + Pay_RowCount + ")</td>");
                Response.Write("<td>=sum(M6:M" + Pay_RowCount + ")</td>");
                Response.Write("<td>=sum(N6:N" + Pay_RowCount + ")</td>");
                Response.Write("<td>=sum(O6:O" + Pay_RowCount + ")</td>");
                
            }

            if (ddldept.SelectedValue == "4")
            {
                Response.Write("<td>=sum(F6:F" + Pay_RowCount + ")</td>");
                Response.Write("<td>=sum(G6:G" + Pay_RowCount + ")</td>");
                Response.Write("<td>=sum(H6:H" + Pay_RowCount + ")</td>");
                Response.Write("<td>=sum(I6:I" + Pay_RowCount + ")</td>");
                Response.Write("<td>=sum(J6:J" + Pay_RowCount + ")</td>");
                Response.Write("<td>=sum(K6:K" + Pay_RowCount + ")</td>");
                Response.Write("<td>=sum(L6:L" + Pay_RowCount + ")</td>");
                Response.Write("<td>=sum(M6:M" + Pay_RowCount + ")</td>");
                Response.Write("<td>=sum(N6:N" + Pay_RowCount + ")</td>");
                Response.Write("<td>=sum(O6:O" + Pay_RowCount + ")</td>");
                Response.Write("<td>=sum(P6:P" + Pay_RowCount + ")</td>");
                Response.Write("<td>=sum(Q6:Q" + Pay_RowCount + ")</td>");
                Response.Write("<td>=sum(R6:R" + Pay_RowCount + ")</td>");
                //Response.Write("<td>=sum(S6:S" + Pay_RowCount + ")</td>");
                

            }                        

            if (ddldept.SelectedValue == "5")
            {
                Response.Write("<td>=sum(F6:F" + Pay_RowCount + ")</td>");
                Response.Write("<td>=sum(G6:G" + Pay_RowCount + ")</td>");
                Response.Write("<td>=sum(H6:H" + Pay_RowCount + ")</td>");
                Response.Write("<td>=sum(I6:I" + Pay_RowCount + ")</td>");
                Response.Write("<td>=sum(J6:J" + Pay_RowCount + ")</td>");
                Response.Write("<td>=sum(K6:K" + Pay_RowCount + ")</td>");
                Response.Write("<td>=sum(L6:L" + Pay_RowCount + ")</td>");
                
            }
            if (ddldept.SelectedValue == "6")
            {
                Response.Write("<td>=sum(F6:F" + Pay_RowCount + ")</td>");
                Response.Write("<td>=sum(G6:G" + Pay_RowCount + ")</td>");
                Response.Write("<td>=sum(H6:H" + Pay_RowCount + ")</td>");
                Response.Write("<td>=sum(I6:I" + Pay_RowCount + ")</td>");
                Response.Write("<td>=sum(J6:J" + Pay_RowCount + ")</td>");
                Response.Write("<td>=sum(K6:K" + Pay_RowCount + ")</td>");
                Response.Write("<td>=sum(L6:L" + Pay_RowCount + ")</td>");
                Response.Write("<td>=sum(M6:M" + Pay_RowCount + ")</td>");
                Response.Write("<td>=sum(N6:N" + Pay_RowCount + ")</td>");
                Response.Write("<td>=sum(O6:O" + Pay_RowCount + ")</td>");
                Response.Write("<td>=sum(P6:P" + Pay_RowCount + ")</td>");
                Response.Write("<td>=sum(Q6:Q" + Pay_RowCount + ")</td>");
                Response.Write("<td>=sum(R6:R" + Pay_RowCount + ")</td>");

            }

            if (ddldept.SelectedValue == "1")
            {
                Response.Write("<td>=sum(G6:G" + Pay_RowCount + ")</td>");
                Response.Write("<td>=sum(H6:H" + Pay_RowCount + ")</td>");
                Response.Write("<td>=sum(I6:I" + Pay_RowCount + ")</td>");
                Response.Write("<td>=sum(J6:J" + Pay_RowCount + ")</td>");
                Response.Write("<td>=sum(K6:K" + Pay_RowCount + ")</td>");
                Response.Write("<td>=sum(L6:L" + Pay_RowCount + ")</td>");
                Response.Write("<td>=sum(M6:M" + Pay_RowCount + ")</td>");
                Response.Write("<td>=sum(N6:N" + Pay_RowCount + ")</td>");
                Response.Write("<td>=sum(O6:O" + Pay_RowCount + ")</td>");
                Response.Write("<td>=sum(P6:P" + Pay_RowCount + ")</td>");
                Response.Write("<td>=sum(Q6:Q" + Pay_RowCount + ")</td>");
                Response.Write("<td>=sum(R6:R" + Pay_RowCount + ")</td>");
                Response.Write("<td>=sum(S6:S" + Pay_RowCount + ")</td>");
            }
            if (ddldept.SelectedValue == "7")
            {
                Response.Write("<td>=sum(F6:F" + Pay_RowCount + ")</td>");
                Response.Write("<td>=sum(G6:G" + Pay_RowCount + ")</td>");
                Response.Write("<td>=sum(H6:H" + Pay_RowCount + ")</td>");
                Response.Write("<td>=sum(I6:I" + Pay_RowCount + ")</td>");
                Response.Write("<td>=sum(J6:J" + Pay_RowCount + ")</td>");
                Response.Write("<td>=sum(K6:K" + Pay_RowCount + ")</td>");
                Response.Write("<td>=sum(L6:L" + Pay_RowCount + ")</td>");
                Response.Write("<td>=sum(M6:M" + Pay_RowCount + ")</td>");
                Response.Write("<td>=sum(N6:N" + Pay_RowCount + ")</td>");
                Response.Write("<td>=sum(O6:O" + Pay_RowCount + ")</td>");
                Response.Write("<td>=sum(P6:P" + Pay_RowCount + ")</td>");
                Response.Write("<td>=sum(Q6:Q" + Pay_RowCount + ")</td>");
                Response.Write("<td>=sum(R6:R" + Pay_RowCount + ")</td>");

                Response.Write("<td>=sum(S6:S" + Pay_RowCount + ")</td>");
                Response.Write("<td>=sum(T6:T" + Pay_RowCount + ")</td>");
                Response.Write("<td>=sum(U6:U" + Pay_RowCount + ")</td>");

                //Response.Write("<td>=sum(S6:S" + Pay_RowCount + ")</td>");
            }

            Response.Write("</tr></table>");

            Response.End();
            Response.Clear();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Downloaded Successfully');", true);
        }
    }
    protected void gvSalary_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void rbsalary_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (rbsalary.SelectedValue == "2")
        {
            pnlMonth.Visible = false;
        }
        else if (rbsalary.SelectedValue == "1")
        {
            pnlMonth.Visible = true;
        }
        else if (rbsalary.SelectedValue == "3")
        {
            pnlMonth.Visible = true;
        }
        else
        {
            pnlMonth.Visible = false;
        }
    }
    protected void txtfrom_TextChanged(object sender, EventArgs e)
    {

    }
    protected void txtTo_TextChanged(object sender, EventArgs e)
    {

    }
    protected void btncontract_Click(object sender, EventArgs e)
    {
        Response.Redirect("ContractRPT.aspx");
    }
    protected void btnConfirm_Click(object sender, EventArgs e)
    {
        try
        {
            bool ErrFlag = false;
            if (ddlcategory.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select the Category');", true);
                ErrFlag = true;
            }
            else if ((ddldept.SelectedValue == "") || (ddldept.SelectedValue == "0"))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select the Employee Type');", true);
                ErrFlag = true;
            }
            else if ((ddlMonths.SelectedValue == "0") || (ddlMonths.SelectedValue == ""))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select the Months');", true);
                ErrFlag = true;
            }
            if (!ErrFlag)
            {
                DataTable dt = new DataTable();
                dt = objdata.Employee_Payslip_Load(SessionCcode, SessionLcode, ddlMonths.SelectedValue, txtfrom.Text, txtTo.Text, ddlFinance.SelectedValue, rbsalary.SelectedValue);
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        string EmpNo = dt.Rows[i]["EmpNo"].ToString();
                        string TransDate1 = dt.Rows[i]["TransDate"].ToString();
                        string Advance = dt.Rows[i]["Advance"].ToString();
                        EmployeeDays = dt.Rows[i]["WorkedDays"].ToString();
                        cl = dt.Rows[i]["CL"].ToString();
                        MyMonth = ddlMonths.SelectedValue;
                        MyDate = DateTime.ParseExact(TransDate1, "dd-MM-yyyy", null);
                        string constr = ConfigurationManager.AppSettings["ConnectionString"];
                        SqlConnection con = new SqlConnection(constr);
                        //Label lblEmpNo = (Label)gvsal.FindControl("lblExist");
                        string qry = "Update SalaryDetails set Process_Mode='" + 1 + "' where EmpNo='" + EmpNo + "' and Month='" + ddlMonths.SelectedValue + "' and FinancialYear='" + ddlFinance.SelectedValue + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                        SqlCommand cmd = new SqlCommand(qry, con);
                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();
                        MyDate = DateTime.ParseExact(TransDate1, "dd-MM-yyyy", null);
                        TempDate = Convert.ToDateTime(TransDate1).AddMonths(0).ToShortDateString();
                        TransDate = DateTime.ParseExact(TempDate, "dd/MM/yyyy", null);
                        DataTable dt_con = new DataTable();
                        DataTable dt_val1 = new DataTable();
                        
                        dt_con = objdata.Contract_SalaryCal(EmpNo, SessionCcode, SessionLcode);
                        string MonthCount = "";
                        string BasicMonth = "";
                        if (dt_con.Rows.Count > 0)
                        {
                            dt_val1 = objdata.Contract_Eligible(dt_con.Rows[0]["ContractName"].ToString());
                            if ((Convert.ToDecimal(EmployeeDays) + Convert.ToDecimal(cl)) >= (Convert.ToDecimal(dt_con.Rows[0]["FixedDays"].ToString())))
                            {
                                if (dt_con.Rows[0]["MonthCount"].ToString() == dt_con.Rows[0]["Months"].ToString())
                                {
                                }
                                else
                                {
                                    if (Convert.ToInt32(dt_con.Rows[0]["BasicMonths"].ToString()) < Convert.ToInt32(dt_val1.Rows[0]["NotEMonths"].ToString()))
                                    {
                                        BasicMonth = "0";
                                        BasicMonth = (Convert.ToInt32(dt_con.Rows[0]["BasicMonths"].ToString()) + 1).ToString();
                                        MonthCount = "0";
                                        string upd = "Update ContractDetails set BasicMonths ='" + BasicMonth + "' where EmpNo='" + EmpNo + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                                        SqlCommand cmd_upd = new SqlCommand(upd, con);
                                        con.Open();
                                        cmd_upd.ExecuteNonQuery();
                                        con.Close();
                                    }
                                    else
                                    {
                                        if (dt_con.Rows[0]["MonthCount"].ToString() == "0")
                                        {
                                            //BasicMonth = "0";
                                            MonthCount = "1";
                                            string upd = "Update ContractDetails set MonthCount ='" + MonthCount + "' where EmpNo='" + EmpNo + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                                            SqlCommand cmd_upd = new SqlCommand(upd, con);
                                            con.Open();
                                            cmd_upd.ExecuteNonQuery();
                                            con.Close();
                                        }
                                        else
                                        {
                                            //BasicMonth = "1";
                                            MonthCount = (Convert.ToInt32(dt_con.Rows[0]["MonthCount"].ToString()) + 1).ToString();
                                            string upd1 = "Update ContractDetails set MonthCount ='" + MonthCount + "' where EmpNo='" + EmpNo + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                                            SqlCommand cmd_upd1 = new SqlCommand(upd1, con);
                                            con.Open();
                                            cmd_upd1.ExecuteNonQuery();
                                            con.Close();
                                        }
                                        string Balanceday = objdata.Contract_GetEmpDet(EmpNo, SessionCcode, SessionLcode);
                                        if (Balanceday.Trim() != "")
                                        {
                                            if (Convert.ToDecimal(Balanceday) > 0)
                                            {
                                                DataTable dt_det = new DataTable();
                                                DataTable dt_val = new DataTable();
                                                dt_det = objdata.Contract_getPlanDetails(EmpNo, SessionCcode, SessionLcode);

                                                if (dt_det.Rows.Count > 0)
                                                {
                                                    if (dt_det.Rows[0]["ContractType"].ToString() == "1")
                                                    {
                                                        val = (Convert.ToDecimal(EmployeeDays) + Convert.ToDecimal(cl));
                                                        Balanceday = (Convert.ToDecimal(Balanceday) - val).ToString();
                                                        objdata.Contract_Reducing(Balanceday, EmpNo, val.ToString(), SessionCcode, SessionLcode);
                                                    }
                                                    else if (dt_det.Rows[0]["ContractType"].ToString() == "2")
                                                    {
                                                        val = (Convert.ToDecimal(EmployeeDays) + Convert.ToDecimal(cl));
                                                        if (val >= Convert.ToDecimal(dt_det.Rows[0]["FixedDays"].ToString()))
                                                        {
                                                            string Mon = (Convert.ToInt32(dt_det.Rows[0]["DecMonth"].ToString()) - 1).ToString();
                                                            Balanceday = (Convert.ToDecimal(Balanceday) - val).ToString();
                                                            objdata.Contract_Reducing_Month(Balanceday, EmpNo, val.ToString(), SessionCcode, SessionLcode, Mon);
                                                        }
                                                        else
                                                        {
                                                            string Mon = (Convert.ToInt32(dt_det.Rows[0]["DecMonth"].ToString()) - 1).ToString();
                                                            //Balanceday = (Convert.ToDecimal(Balanceday) - val).ToString();
                                                            //objdata.Contract_Reducing(Balanceday, ddlempCodeStaff.SelectedValue, val, SessionCcode, SessionLcode, Mon);
                                                            objdata.Not_Contract_Reducing_Month(EmpNo, val.ToString(), SessionCcode, SessionLcode);
                                                        }
                                                    }
                                                    else if (dt_det.Rows[0]["ContractType"].ToString() == "3")
                                                    {
                                                        val = (Convert.ToDecimal(EmployeeDays) + Convert.ToDecimal(cl));
                                                        Balanceday = (Convert.ToDecimal(Balanceday) - val).ToString();
                                                        objdata.Contract_Reducing(Balanceday, EmpNo, val.ToString(), SessionCcode, SessionLcode);
                                                        //SaveFlag = true;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                //MonthCount = (dt_con.Rows[0]["MonthCount"].ToString());
                            }
                            else
                            {
                            }
                        }
                        else
                        {
                        }
                        //MyDate = DateTime.ParseExact(objSal.SalaryDate, "dd-MM-yyyy", null);

                        //TempDate = Convert.ToDateTime(txtsalaryday.Text).AddMonths(0).ToShortDateString();
                        //TransDate = DateTime.ParseExact(TempDate, "dd/MM/yyyy", null);
                        //objdata.SalaryDetails(objSal, EmpNo, ExistNo, MyDate, MyMonth, Year, ddlfinance.SelectedValue, TransDate, MyDate1, MyDate2);
                        

                        //DataTable dtadv = new DataTable();
                        //dtadv = objdata.Salary_advance_retrive(EmpNo, SessionCcode, SessionLcode);
                        //if (dtadv.Rows.Count > 0)
                        //{
                        //    Adv_id = dtadv.Rows[0]["ID"].ToString();
                        //    Adv_BalanceAmt = dtadv.Rows[0]["BalanceAmount"].ToString();
                        //    Adv_due = dtadv.Rows[0]["MonthlyDeduction"].ToString();
                        //    Increment_mont = dtadv.Rows[0]["IncreaseMonth"].ToString();

                        //    Dec_mont = dtadv.Rows[0]["ReductionMonth"].ToString();
                        //    if (Dec_mont == "1")
                        //    {
                        //        Advance = Adv_BalanceAmt;
                        //        //txtSAdvance.Text = Adv_BalanceAmt;
                        //        //txtWadvance.Text = Adv_BalanceAmt;
                        //    }

                        //    else if (Convert.ToDecimal(Adv_BalanceAmt) < Convert.ToDecimal(Adv_due))
                        //    {
                        //        Advance = Adv_BalanceAmt;
                        //        //txtSAdvance.Text = Adv_BalanceAmt;
                        //        //txtWadvance.Text = Adv_BalanceAmt;
                        //    }
                        //    else
                        //    {
                        //        Advance = dtadv.Rows[0]["MonthlyDeduction"].ToString();
                        //        //txtSAdvance.Text = dtadv.Rows[0]["MonthlyDeduction"].ToString();
                        //        //txtWadvance.Text = dtadv.Rows[0]["MonthlyDeduction"].ToString();
                        //    }
                        //}
                        //else
                        //{
                        //    Advance = "0";
                        //}
                        //if (dtadv.Rows.Count > 0)
                        //{
                        //    Adv_id = dtadv.Rows[0]["ID"].ToString();
                        //}
                        //else
                        //{
                        //    Adv_id = "";
                        //}

                        //if (Adv_id != "")
                        //{
                        //    if (Convert.ToInt32(Dec_mont) == 1)
                        //    {
                        //        if ((Convert.ToDecimal(Adv_BalanceAmt) == Convert.ToDecimal(Advance)) || (Convert.ToDecimal(Advance) == 0))
                        //        {
                        //            if (Convert.ToDecimal(Advance) == 0)
                        //            {
                        //                objdata.Advance_repayInsert(EmpNo, MyDate, MyMonth, Advance, Adv_id, SessionCcode, SessionLcode);
                        //                string bal = (Convert.ToDecimal(Adv_BalanceAmt) - Convert.ToDecimal(Advance)).ToString();
                        //                string decrementMonths = Convert.ToInt32(Dec_mont).ToString();
                        //                string IncrementMonths = (Convert.ToInt32(Increment_mont) + 1).ToString();
                        //                string Completed = "N";

                        //                objdata.Advance_update(EmpNo, bal, IncrementMonths, decrementMonths, Adv_id, Completed, MyDate, SessionCcode, SessionLcode);
                        //            }
                        //            else
                        //            {
                        //                objdata.Advance_repayInsert(EmpNo, MyDate, MyMonth, Advance, Adv_id, SessionCcode, SessionLcode);
                        //                string bal = (Convert.ToDecimal(Adv_BalanceAmt) - Convert.ToDecimal(Advance)).ToString();
                        //                string decrementMonths = (Convert.ToInt32(Dec_mont) - 1).ToString();
                        //                string IncrementMonths = (Convert.ToInt32(Increment_mont) + 1).ToString();
                        //                string Completed = "Y";

                        //                objdata.Advance_update(EmpNo, bal, IncrementMonths, decrementMonths, Adv_id, Completed, MyDate, SessionCcode, SessionLcode);
                        //            }


                        //            string Balanceday = objdata.Contract_GetEmpDet(EmpNo, SessionCcode, SessionLcode);
                        //            if (Balanceday.Trim() != "")
                        //            {
                        //                if (Convert.ToDecimal(Balanceday) > 0)
                        //                {
                        //                    DataTable dt_det = new DataTable();
                        //                    dt_det = objdata.Contract_getPlanDetails(EmpNo, SessionCcode, SessionLcode);
                        //                    if (dt_det.Rows.Count > 0)
                        //                    {
                        //                        if (dt_det.Rows[0]["ContractType"].ToString() == "1")
                        //                        {
                        //                            val = (Convert.ToDecimal(EmployeeDays) + Convert.ToDecimal(cl));
                        //                            Balanceday = (Convert.ToDecimal(Balanceday) - val).ToString();
                        //                            objdata.Contract_Reducing(Balanceday, EmpNo, val.ToString(), SessionCcode, SessionLcode);
                        //                        }
                        //                        else if (dt_det.Rows[0]["ContractType"].ToString() == "2")
                        //                        {
                        //                            val = (Convert.ToDecimal(EmployeeDays) + Convert.ToDecimal(cl));
                        //                            if (val >= Convert.ToDecimal(dt_det.Rows[0]["FixedDays"].ToString()))
                        //                            {
                        //                                string Mon = (Convert.ToInt32(dt_det.Rows[0]["DecMonth"].ToString()) - 1).ToString();
                        //                                Balanceday = (Convert.ToDecimal(Balanceday) - val).ToString();
                        //                                objdata.Contract_Reducing_Month(Balanceday, EmpNo, val.ToString(), SessionCcode, SessionLcode, Mon);
                        //                            }
                        //                            else
                        //                            {
                        //                                string Mon = (Convert.ToInt32(dt_det.Rows[0]["DecMonth"].ToString()) - 1).ToString();
                        //                                //Balanceday = (Convert.ToDecimal(Balanceday) - val).ToString();
                        //                                //objdata.Contract_Reducing(Balanceday, EmpNo, val, SessionCcode, SessionLcode, Mon);
                        //                                //objdata.Not_Contract_Reducing_Month(EmpNo, val.ToString(), SessionCcode, SessionLcode);
                        //                            }
                        //                        }
                        //                        else if (dt_det.Rows[0]["ContractType"].ToString() == "3")
                        //                        {
                        //                            val = (Convert.ToDecimal(EmployeeDays));
                        //                            Balanceday = (Convert.ToDecimal(Balanceday) - val).ToString();
                        //                            objdata.Contract_Reducing(Balanceday, EmpNo, val.ToString(), SessionCcode, SessionLcode);
                        //                        }
                        //                    }
                        //                }
                        //            }
                        //            //ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert(' " + MyMonth + " Month Salary has been processed');", true);
                        //            //System.Windows.Forms.MessageBox.Show(MyMonth + " Month Salary has been processed", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                        //        }
                        //    }
                        //    else
                        //    {
                        //        bool Err = false;
                        //        if (Convert.ToDecimal(Advance) > Convert.ToDecimal(Adv_BalanceAmt))
                        //        {
                        //            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Enter the Advance amount Properly');", true);
                        //            //System.Windows.Forms.MessageBox.Show(MyMonth + "Enter the Advance Amount Proprly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                        //            Err = true;
                        //        }
                        //        else if (Convert.ToDecimal(Advance) == Convert.ToDecimal(Adv_BalanceAmt))
                        //        {
                        //            objdata.Advance_repayInsert(EmpNo, MyDate, MyMonth, Advance, Adv_id, SessionCcode, SessionLcode);
                        //            string bal = (Convert.ToDecimal(Adv_BalanceAmt) - Convert.ToDecimal(Advance)).ToString();
                        //            string decrementMonths = (Convert.ToInt32(Dec_mont) - 1).ToString();
                        //            string IncrementMonths = (Convert.ToInt32(Increment_mont) + 1).ToString();
                        //            string Completed = "Y";

                        //            objdata.Advance_update(EmpNo, bal, IncrementMonths, decrementMonths, Adv_id, Completed, MyDate, SessionCcode, SessionLcode);
                        //        }
                        //        else if (Convert.ToDecimal(Advance) == 0)
                        //        {
                        //            objdata.Advance_repayInsert(EmpNo, MyDate, MyMonth, Advance, Adv_id, SessionCcode, SessionLcode);
                        //            string bal = (Convert.ToDecimal(Adv_BalanceAmt) - Convert.ToDecimal(Advance)).ToString();
                        //            string decrementMonths = Convert.ToInt32(Dec_mont).ToString();
                        //            string IncrementMonths = (Convert.ToInt32(Increment_mont) + 1).ToString();
                        //            string Completed = "N";

                        //            objdata.Advance_update(EmpNo, bal, IncrementMonths, decrementMonths, Adv_id, Completed, MyDate, SessionCcode, SessionLcode);
                        //        }
                        //        else
                        //        {
                        //            objdata.Advance_repayInsert(EmpNo, MyDate, MyMonth, Advance, Adv_id, SessionCcode, SessionLcode);
                        //            string bal = (Convert.ToDecimal(Adv_BalanceAmt) - Convert.ToDecimal(Advance)).ToString();
                        //            string decrementMonths = (Convert.ToInt32(Dec_mont) - 1).ToString();
                        //            string IncrementMonths = (Convert.ToInt32(Increment_mont) + 1).ToString();
                        //            string Completed = "N";

                        //            objdata.Advance_update(EmpNo, bal, IncrementMonths, decrementMonths, Adv_id, Completed, MyDate, SessionCcode, SessionLcode);
                        //        }
                        //    }
                        //}
                    }
                }

            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Finished Successfully');", true);
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Check the Data Properly...');", true);
        }
    }
}
