﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Data.SqlClient;

public partial class UserRegistration_1 : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    string SessionAdmin;
    static string lblprobation_Common;
    string SessionCcode;
    string SessionLcode;
    protected void Page_Load(object sender, EventArgs e)
    {
        
        if (SessionAdmin == "2")
        {
            Response.Redirect("EmployeeRegistration.aspx");
        }
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        string ss = Session["UserId"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        lblComany.Text = SessionCcode + " - " + SessionLcode;
        if (!IsPostBack)
        {
            display();
        }
        lblusername.Text = Session["Usernmdisplay"].ToString();
    }
    protected void btnsave_Click(object sender, EventArgs e)
    {
        try
        {
            UserRegistrationClass objReg = new UserRegistrationClass();

            //string passwd = s_hex_md5(txtpassword.Text);
            bool ErrrFlg = false;
            bool isRegistred = false;
            if (txtusercode.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Enter Your User Code');", true);
                //System.Windows.Forms.MessageBox.Show("Enter user Code", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrrFlg = true;
            }
            else if (txtusernm.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Enter Your Usere Name');", true);
                //System.Windows.Forms.MessageBox.Show("Enter User Name", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrrFlg = true;
            }
            else if (txtpassword.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Enter Password');", true);
                //System.Windows.Forms.MessageBox.Show("Enter Password", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrrFlg = true;
            }
            //else if (txtmobile.Text == "")
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Enter Your Mobile Number');", true);
            //    //System.Windows.Forms.MessageBox.Show("Enter Mobile Number", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            //    ErrrFlg = true;
            //}
            //else if (txtdeaprtment.Text == "")
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Enter Your Department');", true);
            //    //System.Windows.Forms.MessageBox.Show("Enter your department", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            //    ErrrFlg = true;
            //}
            //else if (txtdesignation.Text == "")
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Enter Your Designation');", true);
            //    //System.Windows.Forms.MessageBox.Show("Enter your Designation", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            //    ErrrFlg = true;
            //}
            else if (txtusercode.Enabled == true)
            {
                string user = objdata.user_verify(txtusercode.Text.Trim(), SessionCcode, SessionLcode);
                if (user == txtusercode.Text)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('User Code already Exist');", true);
                    //System.Windows.Forms.MessageBox.Show("User Code already Exist", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    ErrrFlg = true;
                }
            }
            if (!ErrrFlg)
            {
                objReg.UserCode = txtusercode.Text;
                objReg.UserName = txtusernm.Text;
                objReg.Password = s_hex_md5(txtpassword.Text);
                objReg.IsAdmin = rbtnisadmin.SelectedValue;
                objReg.Mobile = txtmobile.Text;
                objReg.Department = txtdeaprtment.Text;
                objReg.Designation = txtdesignation.Text;
                objReg.Ccode = SessionCcode;
                objReg.Lcode = SessionLcode;
                isRegistred = true;
            }
            try
            {
                if (isRegistred)
                {
                    if (txtusercode.Enabled == false)
                    {
                        objdata.UserRegistration_update(objReg);
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('User Updated Successfully...!');", true);
                        //System.Windows.Forms.MessageBox.Show("User Updated Successfully", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                        Clear();
                        display();
                    }
                    else
                    {
                        objdata.UserRegistration(objReg);
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('User Registered Successfully...!');", true);
                        //System.Windows.Forms.MessageBox.Show("User Registered Successfully", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                        Clear();
                        display();
                    }
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Server Error Contact Admin...!');", true);
                //System.Windows.Forms.MessageBox.Show("Server Error Contact Admin", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            }
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Enter Correct Value');", true);
            //System.Windows.Forms.MessageBox.Show("Enter Correct Value", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
        }

    }
    public static String s_hex_md5(String originalPassword)
    {
        UTF8Encoding encoder = new UTF8Encoding();
        MD5 md5 = new MD5CryptoServiceProvider();

        Byte[] hashedbytes = md5.ComputeHash(encoder.GetBytes(originalPassword));
        return BitConverter.ToString(hashedbytes).Replace("-", "").ToLower();
    }
    public void display()
    {
        DataTable dt = new DataTable();
        dt = objdata.user_gridLoad(SessionCcode, SessionLcode);
        gvuser.DataSource = dt;
        gvuser.DataBind();
    }
    public void Clear()
    {
        txtusercode.Text = "";
        txtusernm.Text = "";
        txtmobile.Text = "";
        txtdeaprtment.Text = "";
        txtdesignation.Text = "";
        txtusercode.Enabled = true;
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Clear();
    }
    protected void gvuser_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        if (SessionAdmin == "1")
        {
            string constr = ConfigurationManager.AppSettings["ConnectionString"];
            SqlConnection con = new SqlConnection(constr);
            Label lbluser = (Label)gvuser.Rows[e.RowIndex].FindControl("lbluser");
            lblprobation_Common = lbluser.Text;
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "script", "showConfirm();", true);
            //System.Windows.Forms.DialogResult dres = System.Windows.Forms.MessageBox.Show("Are you Sure want to Delete ?", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.YesNo, System.Windows.Forms.MessageBoxIcon.Warning);
            //if (dres.ToString() == "Yes")
            //{
            //    string qry = "Delete from MstUsers where UserCode = '" + lbluser.Text + "'";
            //    SqlCommand cmd = new SqlCommand(qry, con);
            //    con.Open();
            //    cmd.ExecuteNonQuery();
            //    con.Close();
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "confirm('User Deleted');", true);
            //    //System.Windows.Forms.MessageBox.Show("User Deleted", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            //    display();
            //}
        }


    }

    protected void cmdOverriteGrade_Click(object sender, EventArgs e)
    {
        try
        {
            string constr = ConfigurationManager.AppSettings["ConnectionString"];
            SqlConnection con = new SqlConnection(constr);
            //Label lblbank = (Label)gvBank.Rows[e.RowIndex].FindControl("lblBankcd");
            string qry = "Delete from MstUsers where UserCode = '" + lblprobation_Common + "' and CompCode='" + SessionCcode + "' and LocationCode='" + SessionLcode + "'";
            SqlCommand cmd = new SqlCommand(qry, con);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('User Deleted');", true);
            //System.Windows.Forms.MessageBox.Show("User Deleted", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            display();

        }
        catch (Exception ex)
        {
        }
    }

    protected void gvuser_RowEditing(object sender, GridViewEditEventArgs e)
    {
        Label lbledit = (Label)gvuser.Rows[e.NewEditIndex].FindControl("lbluser");
        DataTable dt = new DataTable();
        dt = objdata.user_edit(lbledit.Text);
        if (dt.Rows.Count > 0)
        {
            txtusercode.Text = dt.Rows[0]["UserCode"].ToString();
            txtusercode.Enabled = false;
            txtusernm.Text = dt.Rows[0]["UserName"].ToString();
            txtmobile.Text = dt.Rows[0]["Mobile"].ToString();
            txtdeaprtment.Text = dt.Rows[0]["Department"].ToString();
            txtdesignation.Text = dt.Rows[0]["Designation"].ToString();
            if (dt.Rows[0]["IsAdmin"].ToString().Trim() == "1")
            {
                rbtnisadmin.SelectedValue = "1";
            }
            else if (dt.Rows[0]["IsAdmin"].ToString().Trim() == "2")
            {
                rbtnisadmin.SelectedValue = "2";
            }
        }
    }
    protected void btnEmp_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptEmpDownload.aspx");
    }
    protected void btnatt_Click(object sender, EventArgs e)
    {
        Response.Redirect("AttenanceDownload.aspx");
    }
    protected void btnLeave_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptLeaveSample.aspx");
    }
    protected void btnOT_Click(object sender, EventArgs e)
    {
        Response.Redirect("OTDownload.aspx");
    }
    protected void gvuser_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void btncontract_Click(object sender, EventArgs e)
    {
        Response.Redirect("ContractRPT.aspx");
    }
    protected void btnSal_Click(object sender, EventArgs e)
    {
        Response.Redirect("FrmDeductionDownload.aspx");
    }
}
