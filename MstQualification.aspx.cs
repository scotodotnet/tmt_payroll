﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Data.SqlClient;


public partial class MstQualification_1 : System.Web.UI.Page
{
    BALDataAccess objData = new BALDataAccess();
    MastersClass objMas = new MastersClass();
    string SessionAdmin;
    static string lblprobation_Common;
    string SessionCcode;
    string SessionLcode;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        string ss = Session["UserId"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();
        if (!IsPostBack)
        {
            MstQualificationDisplay();
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        lblComany.Text = SessionCcode + " - " + SessionLcode;
        lblusername.Text = Session["Usernmdisplay"].ToString();
    }
    protected void btnsave_Click(object sender, EventArgs e)
    {
        try
        {
            bool ErrFlag = false;
            if (txtqualicd.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Qualification Code');", true);
                //System.Windows.Forms.MessageBox.Show("Enter the Qualification Code", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            else if (txtqualifname.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Qualification Name');", true);
                //System.Windows.Forms.MessageBox.Show("Enter the Qualification Name", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            if (!ErrFlag)
            {
                string Qid = objData.qualificationid_check(txtqualicd.Text);
                string Qname = objData.qualificationname_check(txtqualifname.Text);
                if (Qid == txtqualicd.Text)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Qualification ID Already Exist');", true);
                    //System.Windows.Forms.MessageBox.Show("Qualification ID already Exist", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    ErrFlag = true;
                }
                if (Qname.ToLower() == txtqualifname.Text.ToLower())
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Qualification Name Already Exist');", true);
                    //System.Windows.Forms.MessageBox.Show("Qualification Name already Exist", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    ErrFlag = true;
                }
                if (!ErrFlag)
                {
                    objMas.Qualificationcd = txtqualicd.Text;
                    objMas.QualificationNm = txtqualifname.Text;

                    objData.MstQualification(objMas);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Qualification Save Successfully...');", true);
                    //System.Windows.Forms.MessageBox.Show("Qualification Saved Successfully", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    txtqualicd.Text = "";
                    txtqualifname.Text = "";
                    MstQualificationDisplay();
                }
            }
        }
        catch (Exception ex)
        {

        }
    }
    public void MstQualificationDisplay()
    {
        DataTable dt = new DataTable();
        dt = objData.MstQualificationDisplay();
        gvQualification.DataSource = dt;
        gvQualification.DataBind();
    }
    protected void GvQualification_Edit(object sender, GridViewEditEventArgs e)
    {
        gvQualification.EditIndex = e.NewEditIndex;
        MstQualificationDisplay();
    }
    protected void GvQualificaton_Cancel(object sender, GridViewCancelEditEventArgs e)
    {
        gvQualification.EditIndex = -1;
        MstQualificationDisplay();
    }
    protected void GvQualification_Updating(object sender, GridViewUpdateEventArgs e)
    {
        bool ErrFlag = false;
        TextBox txtqalifnm1 = (TextBox)gvQualification.Rows[e.RowIndex].FindControl("txtqulinm");
        if (txtqalifnm1.Text == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Qualification Name');", true);
            //System.Windows.Forms.MessageBox.Show("Enter Qualification Name", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            ErrFlag = true;
        }
        Label lblqualicd1 = (Label)gvQualification.Rows[e.RowIndex].FindControl("lblqualicd");
        string Qname = objData.qualificationname_check(txtqalifnm1.Text);
        if (Qname.ToLower() == txtqalifnm1.Text.ToLower())
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Qualification Name Already Exist');", true);
            //System.Windows.Forms.MessageBox.Show("Qualification Name Already Exist", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            ErrFlag = true;
        }
        if (!ErrFlag)
        {
            objMas.Qualificationcd = lblqualicd1.Text;
            objMas.QualificationNm = txtqalifnm1.Text;

            objData.UpdateQualification(objMas);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Qualification Update Successfully...');", true);
            //System.Windows.Forms.MessageBox.Show("Qualification Updated Successfully", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            gvQualification.EditIndex = -1;
            MstQualificationDisplay();
        }


    }
    protected void btncancel_Click(object sender, EventArgs e)
    {
        txtqualicd.Text = "";
        txtqualifname.Text = "";
        MstQualificationDisplay();
    }
    protected void gvQualification_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        bool ErrFlag = false;
        string constr = ConfigurationManager.AppSettings["connectionstring"];
        SqlConnection con = new SqlConnection(constr);
        Label lblid = (Label)gvQualification.Rows[e.RowIndex].FindControl("lblqualicd");
        string Qdel = objData.qualification_delete(lblid.Text);
        lblprobation_Common = lblid.Text;
        if (Qdel == lblid.Text)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('It cannot be Deleted');", true);
            //System.Windows.Forms.MessageBox.Show("It Cannot be Deleted", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            ErrFlag = true;
        }
        if (!ErrFlag)
        {
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "script", "showConfirm();", true);
            //System.Windows.Forms.DialogResult dres = System.Windows.Forms.MessageBox.Show("Are you Sure want to Delete ?", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.YesNo, System.Windows.Forms.MessageBoxIcon.Warning);
            //if (dres.ToString() == "Yes")
            //{
            //    string qry = "delete from MstQualification where QualificationCd='" + lblid.Text + "'";
            //    SqlCommand cmd = new SqlCommand(qry, con);
            //    con.Open();
            //    cmd.ExecuteNonQuery();
            //    con.Close();
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Deleted Successfully');", true);
            //    //System.Windows.Forms.MessageBox.Show("Deleted Successfully", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            //    MstQualificationDisplay();
            //}
        }
    }

    protected void cmdOverriteGrade_Click(object sender, EventArgs e)
    {
        try
        {
            string constr = ConfigurationManager.AppSettings["ConnectionString"];
            SqlConnection con = new SqlConnection(constr);
            //Label lblbank = (Label)gvBank.Rows[e.RowIndex].FindControl("lblBankcd");
            string qry = "delete from MstQualification where QualificationCd='" + lblprobation_Common + "'";
            SqlCommand cmd = new SqlCommand(qry, con);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Deleted Successfully');", true);
            //System.Windows.Forms.MessageBox.Show("Deleted Successfully", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            MstQualificationDisplay();

        }
        catch (Exception ex)
        {
        }
    }

    protected void gvQualification_DataBound(object sender, EventArgs e)
    {

    }
    protected void gvQualification_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        //if (e.Row.RowType == DataControlRowType.DataRow)
        //{
        //    LinkButton db = (LinkButton)e.Row.Cells[0].Controls[2];
        //    if (db.Text == "Delete")
        //    {
        //        db.OnClientClick = "return confirm('Are you sure want to Delete ?')";
        //    }
        //}
    }
    protected void btnEmp_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptEmpDownload.aspx");
    }
    protected void btnatt_Click(object sender, EventArgs e)
    {
        Response.Redirect("AttenanceDownload.aspx");
    }
    protected void btnLeave_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptLeaveSample.aspx");
    }
    protected void btnOT_Click(object sender, EventArgs e)
    {
        Response.Redirect("OTDownload.aspx");
    }
}
