﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;

using Payroll;
using Payroll.Data;
using Payroll.Configuration;

using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html;
using iTextSharp.text.html.simpleparser;


using Altius.BusinessAccessLayer.BALDataAccess;

public partial class LoadEmpmusters : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    string SessionAdmin;
    string Stafflabour;
    string SessionCcode;
    string SessionLcode;
    int yr;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionAdmin = Session["Isadmin"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        string ss = Session["UserId"].ToString();
        lblComany.Text = SessionCcode + " - " + SessionLcode;
        lblusername.Text = Session["Usernmdisplay"].ToString();
        if (!IsPostBack)
        {
            category();
            Months_load();
            int currentYear = Utility.GetFinancialYear;
            for (int i = 0; i < 10; i++)
            {
                ddlfinance.Items.Add(new System.Web.UI.WebControls.ListItem (currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                //  ddlShowYear.Items.Add(new ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                currentYear = currentYear - 1;
            }
            //Months_load();
            //int currentYear = Utility.GetFinancialYear;
            //for (int i = 0; i < 10; i++)
            //{
            //    ddlfinance.Items.Add(new System.Web.UI.WebControls.ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
            //    //  ddlShowYear.Items.Add(new ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
            //    currentYear = currentYear - 1;
            //}
        }
    }
    public void Months_load()
    {
        DataTable dt = new DataTable();
        dt = objdata.months_load();
        ddlMonths.DataSource = dt;
        ddlMonths.DataTextField = "Months";
        ddlMonths.DataValueField = "Months";
        ddlMonths.DataBind();
    }
    public void category()
    {
        DataTable dtcate = new DataTable();
        dtcate = objdata.DropDownCategory();
        ddlcategory.DataSource = dtcate;
        ddlcategory.DataTextField = "CategoryName";
        ddlcategory.DataValueField = "CategoryCd";
        ddlcategory.DataBind();
    }
    protected void btnEmp_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptEmpDownload.aspx");
    }
    protected void btnatt_Click(object sender, EventArgs e)
    {
        Response.Redirect("AttenanceDownload.aspx");
    }
    protected void btnLeave_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptLeaveSample.aspx");
    }
    protected void btnOT_Click(object sender, EventArgs e)
    {
        Response.Redirect("OTDownload.aspx");
    }
    protected void ddlcategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        string staffLabour_temp = "";
        DataTable dtempty = new DataTable();
        gvload.DataSource = dtempty;
        gvload.DataBind();

        ddldepartment.DataSource = dtempty;
        ddldepartment.DataBind();
        if (ddlcategory.SelectedValue == "1")
        {
            staffLabour_temp = "S";
        }
        else if (ddlcategory.SelectedValue == "2")
        {
            staffLabour_temp = "L";
        }
        else
        {
            staffLabour_temp = "0";
        }
        DataTable dtDip = new DataTable();
        dtDip = objdata.DropDownDepartment_category(staffLabour_temp);
        if (dtDip.Rows.Count > 1)
        {
            ddldepartment.DataSource = dtDip;
            ddldepartment.DataTextField = "DepartmentNm";
            ddldepartment.DataValueField = "DepartmentCd";
            ddldepartment.DataBind();
        }
        else
        {
            DataTable dt = new DataTable();
            ddldepartment.DataSource = dt;
            ddldepartment.DataBind();
        }
    }
    protected void ddldepartment_SelectedIndexChanged(object sender, EventArgs e)
    {
        
    }
    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }
    protected void btnExport_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        if (ddlcategory.SelectedValue == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Category...!');", true);
            ErrFlag = true;
        }
        else if (ddlcategory.SelectedValue == "0")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the category...!');", true);
            ErrFlag = true;
        }
        else if (ddlMonths.SelectedValue == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Months...!');", true);
            ErrFlag = true;
        }
        else if (ddlMonths.SelectedValue == "0")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Months..!');", true);
            ErrFlag = true;
        }
        
        if (!ErrFlag)
        {
            if (ddlcategory.SelectedValue == "1")
            {
                Stafflabour = "S";
            }
            else if (ddlcategory.SelectedValue == "2")
            {
                Stafflabour = "L";
            }
            int month = 0;
            if (ddlMonths.SelectedValue == "January")
            {
                month = 1;
            }
            else if (ddlMonths.SelectedValue == "February")
            {
                month = 2;
            }
            else if (ddlMonths.SelectedValue == "March")
            {
                month = 3;
            }
            else if (ddlMonths.SelectedValue == "April")
            {
                month = 4;
            }
            else if (ddlMonths.SelectedValue == "May")
            {
                month = 5;
            }
            else if (ddlMonths.SelectedValue == "June")
            {
                month = 6;
            }
            else if (ddlMonths.SelectedValue == "July")
            {
                month = 7;
            }
            else if (ddlMonths.SelectedValue == "August")
            {
                month = 8;
            }
            else if (ddlMonths.SelectedValue == "September")
            {
                month = 9;
            }
            else if (ddlMonths.SelectedValue == "October")
            {
                month = 10;
            }
            else if (ddlMonths.SelectedValue == "November")
            {
                month = 11;
            }
            else if (ddlMonths.SelectedValue == "December")
            {
                month = 12;
            }
        #region Month
        string Mont = "";
        switch (month)
        {
            case 1:
                Mont = "January";
                break;

            case 2:
                Mont = "February";
                break;
            case 3:
                Mont = "March";
                break;
            case 4:
                Mont = "April";
                break;
            case 5:
                Mont = "May";
                break;
            case 6:
                Mont = "June";
                break;
            case 7:
                Mont = "July";
                break;
            case 8:
                Mont = "August";
                break;
            case 9:
                Mont = "September";
                break;
            case 10:
                Mont = "October";
                break;
            case 11:
                Mont = "November";
                break;
            case 12:
                Mont = "December";
                break;
            default:
                break;
        }
        #endregion
        if (month >= 4)
        {
            yr = Convert.ToInt32(ddlfinance.SelectedValue);
        }
        else
        {
            yr = Convert.ToInt32(ddlfinance.SelectedValue);
            yr = yr + 1;
        }
            DataTable dt = new DataTable();
            dt = objdata.Employee_muster(SessionCcode, SessionLcode, ddldepartment.SelectedValue, Stafflabour, SessionAdmin);
            gvload.DataSource = dt;
            gvload.DataBind();
            if (gvload.Rows.Count > 0)
            {
                string attachment = "attachment; filename=EmployeeMaster.xls";
                Response.ClearContent();
                Response.AddHeader("content-disposition", attachment);
                Response.ContentType = "application/ms-excel";
                StringWriter stw = new StringWriter();
                HtmlTextWriter htextw = new HtmlTextWriter(stw);
                gvload.RenderControl(htextw);
                Response.Write("<table>");
                Response.Write("<tr align='Right'>");
                Response.Write("<td>");
                Response.Write("" + ddlMonths.SelectedValue + "");
                
                Response.Write("</td>");
                Response.Write("<td>");
                Response.Write("" + yr + "");
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("</table>");
                Response.Write(stw.ToString());
                Response.End();
                Response.Clear();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('No Data Found...!');", true);
            }
        }

    }
    protected void btnprint_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        if (ddlcategory.SelectedValue == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Category...!');", true);
            ErrFlag = true;
        }
        else if (ddlcategory.SelectedValue == "0")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the category...!');", true);
            ErrFlag = true;
        }
        else if (ddlMonths.SelectedValue == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Months...!');", true);
            ErrFlag = true;
        }
        else if (ddlMonths.SelectedValue == "0")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Months..!');", true);
            ErrFlag = true;
        }
        if (!ErrFlag)
        {
            if (ddlcategory.SelectedValue == "1")
            {
                Stafflabour = "S";
            }
            else if (ddlcategory.SelectedValue == "2")
            {
                Stafflabour = "L";
            }
            int month = 0;
            if (ddlMonths.SelectedValue == "January")
            {
                month = 1;
            }
            else if (ddlMonths.SelectedValue == "February")
            {
                month = 2;
            }
            else if (ddlMonths.SelectedValue == "March")
            {
                month = 3;
            }
            else if (ddlMonths.SelectedValue == "April")
            {
                month = 4;
            }
            else if (ddlMonths.SelectedValue == "May")
            {
                month = 5;
            }
            else if (ddlMonths.SelectedValue == "June")
            {
                month = 6;
            }
            else if (ddlMonths.SelectedValue == "July")
            {
                month = 7;
            }
            else if (ddlMonths.SelectedValue == "August")
            {
                month = 8;
            }
            else if (ddlMonths.SelectedValue == "September")
            {
                month = 9;
            }
            else if (ddlMonths.SelectedValue == "October")
            {
                month = 10;
            }
            else if (ddlMonths.SelectedValue == "November")
            {
                month = 11;
            }
            else if (ddlMonths.SelectedValue == "December")
            {
                month = 12;
            }
            #region Month
            string Mont = "";
            switch (month)
            {
                case 1:
                    Mont = "January";
                    break;

                case 2:
                    Mont = "February";
                    break;
                case 3:
                    Mont = "March";
                    break;
                case 4:
                    Mont = "April";
                    break;
                case 5:
                    Mont = "May";
                    break;
                case 6:
                    Mont = "June";
                    break;
                case 7:
                    Mont = "July";
                    break;
                case 8:
                    Mont = "August";
                    break;
                case 9:
                    Mont = "September";
                    break;
                case 10:
                    Mont = "October";
                    break;
                case 11:
                    Mont = "November";
                    break;
                case 12:
                    Mont = "December";
                    break;
                default:
                    break;
            }
            #endregion
            if (month >= 4)
            {
                yr = Convert.ToInt32(ddlfinance.SelectedValue);
            }
            else
            {
                yr = Convert.ToInt32(ddlfinance.SelectedValue);
                yr = yr + 1;
            }
            ResponseHelper.Redirect("ShowMuster.aspx?Cate=" + Stafflabour + "&Depat=" + ddldepartment.SelectedValue + "&Month=" + ddlMonths.SelectedValue + "&Financialyear=" + yr, "_blank", "");
        }
    }
}
