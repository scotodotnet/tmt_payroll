﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="MstLeaveAllocationEmployee.aspx.cs" Inherits="LeaveAllocationForLabours" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
    <script type="text/javascript" src="js/jquery.js"></script>

    <script type="text/javascript" src="js/jquery.visualize.js"></script>

    <script type="text/javascript" src="js/jquery.wysiwyg.js"></script>

    <script type="text/javascript" src="js/tiny_mce/jquery.tinymce.js"></script>

    <script type="text/javascript" src="js/jquery.fancybox.js"></script>

    <script type="text/javascript" src="js/jquery.idtabs.js"></script>

    <script type="text/javascript" src="js/jquery.datatables.js"></script>

    <script type="text/javascript" src="js/jquery.jeditable.js"></script>

    <script type="text/javascript" src="js/jquery.ui.js"></script>

    <script type="text/javascript" src="js/excanvas.js"></script>

    <script type="text/javascript" src="js/cufon.js"></script>

    <script type="text/javascript" src="js/Geometr231_Hv_BT_400.font.js"></script>

    <script type="text/javascript" src="js/script.js"></script>
    
      <script type="text/javascript" >
        function SelectheaderCheckboxes(cbSelectAll) {

            var gvcheck = document.getElementById('DataGridView1');
            var i;
            //Condition to check header checkbox selected or not if that is true checked all checkboxes
            if (cbSelectAll.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    var inputs = gvcheck.rows[i].getElementsByTagName('input');
                    inputs[0].checked = true;
                }
            }
            //if condition fails uncheck all checkboxes in gridview
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    var inputs = gvcheck.rows[i].getElementsByTagName('input');
                    inputs[0].checked = false;
                }
            }
        }
        //function to check header checkbox based on child checkboxes condition
        function Selectchildcheckboxes(header) {
            var ck = header;
            var count = 0;
            var gvcheck = document.getElementById('DataGridView1');
            var headerchk = document.getElementById(header);
            var rowcount = gvcheck.rows.length;
            //By using this for loop we will count how many checkboxes has checked
            for (i = 1; i < gvcheck.rows.length; i++) {
                var inputs = gvcheck.rows[i].getElementsByTagName('input');
                if (inputs[0].checked) {
                    count++;
                }
            }
            //Condition to check all the checkboxes selected or not
            if (count == rowcount - 1) {
                headerchk.checked = true;
            }
            else {
                headerchk.checked = false;
            }
        }
    </script>
    <style type="text/css">

table {
	border-collapse: collapse;
	border-spacing: 0;
}
table, tbody, tfoot, thead, tr, th, td {
	vertical-align: top;
            margin-right: 146px;
        } 

label				{ line-height: 22px; cursor: pointer; }
        .style1
        {
            width: 150px;
            font-size: 100%;
            vertical-align: baseline;
            border-style: none;
            border-color: inherit;
            border-width: 0;
            margin: 0;
            padding: 0;
           
        }
        .style2
        {
            font-size: 100%;
            vertical-align: baseline;
            border-style: none;
            border-color: inherit;
            border-width: 0;
            margin: 0;
            padding: 0;
            
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
    </div>
    <table >
    <tr>
    <td>
    <asp:RadioButtonList ID="rbtnlaburstaff" runat="server" RepeatColumns="2" Width="150" AutoPostBack="true" onselectedindexchanged="rbtnlaburstaff_SelectedIndexChanged">
       <asp:ListItem Text="Staff" Value="1" Selected="False"></asp:ListItem>
       <asp:ListItem Text="Labor" Value="2" Selected="False"></asp:ListItem>
       </asp:RadioButtonList>
    </td>
    </tr>
    <tr>
    
    <td>
    <asp:Panel ID="panelLabourDepartment" runat="server" Visible ="false" 
            Width="488px"  >
    <asp:Label ID="lblDepartment" runat= "server" Text="Department" ></asp:Label>

    <asp:DropDownList ID="ddlDepartment" runat="server" Width="215" Height="30" 
            AutoPostBack="True" onselectedindexchanged="ddlDepartment_SelectedIndexChanged" 
            ontextchanged="ddlDepartment_TextChanged" ></asp:DropDownList>
     </asp:Panel>
    </td>
   
    </tr>
    <tr>
    <td>
    <asp:Panel ID="PanelButtons" runat="server" Visible="false" >
    <asp:Button ID="btnIndividual" runat="server" Text="Individual" 
            onclick="btnIndividual_Click" />
    <asp:Button ID="btnall" runat="server" Text="All" onclick="btnall_Click" />
    </asp:Panel>
    </td>
    </tr>
    <tr>
    <td>
    <asp:Panel ID="PanelGrouporIndividual" runat="server" Visible="false" >
    <asp:Label ID="lblempNo" runat="server" Text="Employee No" ></asp:Label>
    <asp:TextBox ID="txtEmpNo" runat="server" ></asp:TextBox>
    <asp:Button ID="btnsearch" runat="server" Text="Search" onclick="btnsearch_Click" />
    </asp:Panel>
    </td>
    </tr>
    <tr>
    <td>
    <asp:Panel ID="PanelGrid" runat="server" Visible= "false" >
    <!--<asp:CheckBox ID="chkselectall" runat="server" Text="Select All" />-->
    
    <asp:GridView ID="DataGridView1" runat="server" AutoGenerateColumns="false" 
            ShowFooter="true" AllowPaging="true" PageSize="10" 
            onrowcancelingedit="DataGridView1_RowCancelingEdit" 
            onrowediting="DataGridView1_RowEditing" 
            onrowupdating="DataGridView1_RowUpdating" Width="468px">
    <Columns>
    <asp:TemplateField HeaderText="Staff Id" >
    <ItemTemplate>
    <asp:Label ID="lblStaffID" runat="server" Text='<%# Eval("EmpNo") %>' ></asp:Label>
    </ItemTemplate>
    </asp:TemplateField>
    <asp:TemplateField HeaderText="Staff Name" >
    <ItemTemplate>
    <asp:Label ID="lblStaffName" runat="server" Text='<%# Eval("EmpName") %>' ></asp:Label>
    </ItemTemplate>
    </asp:TemplateField>
    <asp:TemplateField HeaderText="Allocation LeaveDays" >
    <ItemTemplate>
    <asp:TextBox ID="txtleaveAllocation" runat="server" Text='<%# Eval("LeaveDays") %>'></asp:TextBox>
    </ItemTemplate>
    </asp:TemplateField>
      <asp:TemplateField >
           <HeaderTemplate> 
           <asp:CheckBox ID="chkselectall" runat="server" onclick="javascript:SelectheaderCheckboxes(this);"  />
               All
           </HeaderTemplate>
            <ItemTemplate>
                 <asp:CheckBox ID="chkClear" runat="server" />
            </ItemTemplate>
           
        </asp:TemplateField>
        <asp:CommandField ButtonType="Button" CancelText="Cancel" UpdateText="Update" EditText="Edit" ShowEditButton="true" />
        
    </Columns>
    </asp:GridView>
    </asp:Panel>
    </td>
    </tr>
    <tr>
    <td>
    <asp:Button ID="btnSave" runat="server" Text="Save" onclick="btnSave_Click" />
    </td>
    </tr>
    </table>
    
    </form>
</body>
</html>
