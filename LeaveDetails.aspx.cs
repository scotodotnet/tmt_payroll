﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Payroll;
using Payroll.Configuration;
using Payroll.Data;

using Altius.BusinessAccessLayer.BALDataAccess;

public partial class LeaveDetails : System.Web.UI.Page
{
    protected System.Resources.ResourceManager rm;
    protected string PayrollRegisterContentMeta, pageTitle;
    BALDataAccess objdata = new BALDataAccess();
    LeaveClass objLea = new LeaveClass();
    static string ExisitingNo = "";
    int mon = 0;
    string Mont = "";
    string SessionAdmin;
    protected void Page_Load(object sender, EventArgs e)
    {
        SessionAdmin = Session["Isadmin"].ToString();
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        string ss = Session["UserId"].ToString();
        if (!IsPostBack)
        {
            MstEmployeeType();
            LeaveDetailsDisplayFromGrid();
        }
        lblusername.Text = Session["Usernmdisplay"].ToString();
    }
    public void MstEmployeeType()
    {
        //DataTable dt = new DataTable();
        //dt = objdata.DropDwonLeaveType();
        //ddlleavetype.DataSource = dt;
        //ddlleavetype.DataTextField = "LeaveType";
        //ddlleavetype.DataValueField = "LeaveCd";
        //ddlleavetype.DataBind();


        //DataTable dt2 = new DataTable();
        //dt2 = objdata.DropDwonLeaveType();
        //ddlleavetype1.DataSource = dt2;
        //ddlleavetype1.DataTextField = "LeaveType";
        //ddlleavetype1.DataValueField = "LeaveCd";
        //ddlleavetype1.DataBind();

        //DataTable dt3 = new DataTable();
        //dt3 = objdata.DropDwonLeaveType();
        //ddlleavetype2.DataSource = dt3;
        //ddlleavetype2.DataTextField = "LeaveType";
        //ddlleavetype2.DataValueField = "LeaveCd";
        //ddlleavetype2.DataBind();
    }
    public void LeaveDetailsDisplayFromGrid()
    {
        DataTable dtdis = new DataTable();
        dtdis = objdata.LeaveDatailsDisplayFromGridViewAllNewEntry();
        gvLeaveDetails.DataSource = dtdis;
        gvLeaveDetails.DataBind();
    }
    protected void RowEdit_Canceling(object sender, GridViewCancelEditEventArgs e)
    {
        gvLeaveDetails.EditIndex = -1;
        LeaveDetailsDisplayFromGrid();
    }
    protected void RowEdit_Editing(object sender, GridViewEditEventArgs e)
    {
        gvLeaveDetails.EditIndex = e.NewEditIndex;
        LeaveDetailsDisplayFromGrid();
    }
    public void Month()
    {
        
        switch (mon)
        {
            case 1:
                Mont = "January";
                break;

            case 2:
                Mont = "February";
                break;
            case 3:
                Mont = "March";
                break;
            case 4:
                Mont = "April";
                break;
            case 5:
                Mont = "May";
                break;
            case 6:
                Mont = "June";
                break;
            case 7:
                Mont = "July";
                break;
            case 8:
                Mont = "August";
                break;
            case 9:
                Mont = "September";
                break;
            case 10:
                Mont = "October";
                break;
            case 11:
                Mont = "November";
                break;
            case 12:
                Mont = "December";
                break;
            default:
                break;
        }

    }
    protected void btnclick_Click(object sender, EventArgs e)
    {
        DateTime MyDate;
        MyDate = new DateTime();
        objLea.LeaveDate = txtdatemonth.Text;
        MyDate = DateTime.ParseExact(objLea.LeaveDate, "dd/MM/yyyy", null);
        mon = MyDate.Month;
        Month();
        DataTable dtdis = new DataTable();
        dtdis = objdata.LeaveDatailsDisplayFromGridView(Mont);
        gvLeaveDetails.DataSource = dtdis;
        gvLeaveDetails.DataBind();
    }
    protected void RowEdit_Updating(object sender, GridViewUpdateEventArgs e)
    {
        try
        {
            if (txtdatemonth.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Please Select Date...!');", true);
            }
            else
            {
                Label lblGVEmpNo = (Label)gvLeaveDetails.Rows[e.RowIndex].FindControl("lblempno");
                TextBox txtGVCasualLeave = (TextBox)gvLeaveDetails.Rows[e.RowIndex].FindControl("txtcasualLeave");
                TextBox txtGVSickLeave = (TextBox)gvLeaveDetails.Rows[e.RowIndex].FindControl("txtsickleave");
                TextBox txtGVEarnedLeave = (TextBox)gvLeaveDetails.Rows[e.RowIndex].FindControl("txtearnedleave");
                TextBox txtGVMaternalLeave = (TextBox)gvLeaveDetails.Rows[e.RowIndex].FindControl("txtmaternalleave");
                objLea.CasualLeave = txtGVCasualLeave.Text;
                objLea.SickLeave = txtGVSickLeave.Text;
                objLea.EarnedLeave = txtGVEarnedLeave.Text;
                objLea.MaternalLeave = txtGVMaternalLeave.Text;

                int TotalLeave = Convert.ToInt32(txtGVCasualLeave.Text) + Convert.ToInt32(txtGVSickLeave.Text) + Convert.ToInt32(txtGVEarnedLeave.Text) + Convert.ToInt32(txtGVMaternalLeave.Text);
                objLea.NoofLeave = Convert.ToString(TotalLeave);

                DateTime MyDate;
                MyDate = new DateTime();
                objLea.LeaveDate = txtdatemonth.Text;
                MyDate = DateTime.ParseExact(objLea.LeaveDate, "dd/MM/yyyy", null);
                mon = MyDate.Month;
                Month();
                int yr = MyDate.Year;

                string month = Convert.ToString(mon);
                string year = Convert.ToString(yr);

                string monthYear = month + year;
                int dd = DateTime.DaysInMonth(yr, mon);
                objLea.TotalWorkingDays = Convert.ToString(dd);
                string EmpNo = lblGVEmpNo.Text;

                objdata.LeaveDetailsUpdateRowGrid(objLea, MyDate, EmpNo, Mont, year);
                gvLeaveDetails.EditIndex = -1;
                LeaveDetailsDisplayFromGrid();
            }
        }
        catch (Exception ex)
        {
 
        }
    }
    protected void btnsave_Click(object sender, EventArgs e)
    {
        //bool ErrFlag = false;
        //bool isRegistrerd = false;
        //string EmpNo="";
        //DateTime MyDate;
        //MyDate = new DateTime();
        // int mon ;
        //int yr ;

        //string month ;
        //string year ;
        //string monthYear="";
     
        

        //try
        //{
        //    if (txtdate.Text == "")
        //    {
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Please Enter Salary Date...!');", true);
        //        ErrFlag = true;
        //    }

        //    if (!ErrFlag)
        //    {
               
        //        EmpNo = txtempno.Text;
        //        objLea.TotalWorkingDays = txttotalworkingdy.Text;
        //        objLea.LeaveDate = txtdate.Text;
        //        MyDate = DateTime.ParseExact(objLea.LeaveDate, "dd/MM/yyyy", null);
        //         mon = MyDate.Month;
        //        yr = MyDate.Year;

        //        month = Convert.ToString(mon);
        //        year = Convert.ToString(yr);
        //        monthYear = month + year;

        //        string Mont = "";
        //        switch (mon)
        //        {
        //            case 1:
        //                Mont = "January";
        //                break;

        //            case 2:
        //                Mont = "February";
        //                break;
        //            case 3:
        //                Mont = "March";
        //                break;
        //            case 4:
        //                Mont = "April";
        //                break;
        //            case 5:
        //                Mont = "May";
        //                break;
        //            case 6:
        //                Mont = "June";
        //                break;
        //            case 7:
        //                Mont = "July";
        //                break;
        //            case 8:
        //                Mont = "August";
        //                break;
        //            case 9:
        //                Mont = "September";
        //                break;
        //            case 10:
        //                Mont = "October";
        //                break;
        //            case 11:
        //                Mont = "November";
        //                break;
        //            case 12:
        //                Mont = "December";
        //                break;
        //            default:
        //                break;
        //        }

        //        string Salarymonth = objdata.SalaryMonthFromLeave(txtempno.Text, monthYear);



        //        if (Salarymonth == monthYear)
        //        {
        //            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('This " + Mont + " Month Leave Details All Ready Updated...!');", true);
        //            isRegistrerd = false;
        //        }
        //        else
        //        {

        //            objLea.LeaveType1 = ddlleavetype.SelectedValue;
        //            objLea.LeaveType2 = ddlleavetype1.SelectedValue;
        //            objLea.LeaveType3 = ddlleavetype2.SelectedValue;
        //            objLea.Noofleave1 = txtnoofleave.Text;
        //            objLea.Noofleave2 = txtnoofleave2.Text;
        //            objLea.Noofleave3 = txtnofleave3.Text;
        //            isRegistrerd = true;
        //        }
        //    }
        //    if (isRegistrerd)
        //    {
        //        objdata.LeaveDetails(objLea, MyDate, EmpNo, monthYear);
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Your Leave Details Updated Successfully...!');", true);
        //    }
        //}
        //catch (Exception ex)
        //{
        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Server Error Please Contact Admin...!');", true);
        //}

        

    }
    protected void btnsearch_Click(object sender, EventArgs e)
    {
        //try
        //{

        //    if (txtempno.Text == "")
        //    {
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Please Enter Your Employee Code...!');", true);
        //    }
        //    string EmployeeNo = objdata.EmployeeNoFromOfficialProfile(txtempno.Text);
        //    if (EmployeeNo == txtempno.Text)
        //    {
        //        string EmpName = objdata.EmployeeName(txtempno.Text);
        //        if (EmpName == "")
        //        {
        //            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Enter Valid Registration Number...!');", true);
        //        }
        //        else
        //        {
        //            ExisitingNo = objdata.ExisistingNo(txtempno.Text);
        //            lblempname1.Text = EmpName;
        //        }
        //    }
        //    else
        //    {
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Please First Registrered Official Profile Details...!');", true);
               
        //    }
           
        //}
        //catch (Exception ex)
        //{
        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Please Enter Valid Registration Number...!');", true);
        //}

    }
    protected void txtdate_TextChanged(object sender, EventArgs e)
    {

        //DateTime MyDate;
        //MyDate = new DateTime();
        //objLea.LeaveDate = txtdate.Text;
        //MyDate = DateTime.ParseExact(objLea.LeaveDate, "dd/MM/yyyy", null);
        //int mon = MyDate.Month;
        //int yr = MyDate.Year;

        //string month = Convert.ToString(mon);
        //string year = Convert.ToString(yr);

        //string monthYear = month + year;
        //int dd = DateTime.DaysInMonth(yr, mon);
        //txttotalworkingdy.Text = Convert.ToString(dd);
    }

    #region Web Form Designer generated code
    override protected void OnInit(EventArgs e)
    {

        rm = (System.Resources.ResourceManager)Application["rm"];
        Utility.SetThreadCulture();
        if (((SSSCultureInfo)System.Threading.Thread.CurrentThread.CurrentCulture).Encoding != null)
            Response.ContentEncoding = System.Text.Encoding.GetEncoding(((SSSCultureInfo)System.Threading.Thread.CurrentThread.CurrentCulture).Encoding);
        PayrollRegisterContentMeta = "text/html; charset=" + ((SSSCultureInfo)System.Threading.Thread.CurrentThread.CurrentCulture).Encoding;
        if (Application[Request.PhysicalPath] == null)
            Application.Add(Request.PhysicalPath, Response.ContentEncoding.WebName);
        InitializeComponent();
        this.Load += new System.EventHandler(this.Page_Load);
        // this.PreRender += new System.EventHandler(this.Page_PreRender);
        base.OnInit(e);
        if (!DBUtility.AuthorizeUser(new string[] { "1", "2", "6" }))
        ////string UId=Session["UserId"].ToString();
        ////if(UId !=null
        //if (Session["UserId"] == null)
            Response.Redirect(Settings.AccessDeniedPage);
    }


    private void InitializeComponent()
    {
    }


    #endregion



    protected void txtdatemonth_TextChanged(object sender, EventArgs e)
    {

    }
}
