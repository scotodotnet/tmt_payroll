﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Payroll;
using System.Globalization;
using Payroll.Configuration;
using Payroll.Data;
using System.Data.SqlClient;

using Altius.BusinessAccessLayer.BALDataAccess;

public partial class GroupSalary : System.Web.UI.Page
{
    protected System.Resources.ResourceManager rm;
    protected string PayrollRegisterContentMeta, pageTitle;
    BALDataAccess objdata = new BALDataAccess();
    SalaryClass objSal = new SalaryClass();
    string Stafflabour;
    DateTime TransDate;
    string SessionCcode;
    string SessionLcode;
    string SessionAdmin;
    static int d = 0;
    static string txtDays = "0";
    static string work = "0";
    static string NFh = "0";
    static string Weekoff = "0";
    static string AbsentDays = "0";
    static string cl = "0";
    static string OT = "0";
    static string All1 = "0";
    static string All2 = "0";
    static string All3 = "0";
    static string All4 = "0";
    static string Deduction1 = "0";
    static string Deduction2 = "0";
    static string Deduction3 = "0";
    static string Deduction4 = "0";
    static string Advance = "0";
    static string stamp = "0";
    static string union = "0";
    static string LOP = "0";
    static string Basic = "0";
    static string FDA = "0";
    static string VDA = "0";
    static string HRA = "0";
    static string VDA_point = "0";
    static string PF = "0";
    static string ESI = "0";
    static string PFEarnings = "0";
    static string GrossEarnings = "0";
    static string grossDeduction = "0";
    static string NetPay = "0";
    static string TextAmount = "0";
    static string lop = "0";
    static string Basic_temp = "0";
    static string FDA_temp = "0";
    static string VDA_temp = "0";
    static string HRA_temp = "0";
    static string All1_temp = "0";
    static string All2_temp = "0";
    static string Ded1_temp = "0";
    static string Ded2_temp = "0";
    static string PF_Per = "0";
    static string PF_Salary = "0";
    static string Union_val = "0";
    static string pf_val = "0";
    static string ESI_val = "0";
    static string ESI_per = "0";
    static string Adv_id;
    static string Adv_BalanceAmt;
    static string Adv_due;
    static string Increment_mont;
    static string Dec_mont;
    static string pf_special;
    bool isUK = false;
    static string Exist;
    static string EmpNo;
    static string FixedBasic;
    static string FixedFDA;
    static string FixedHRA;
    DateTime Mydate1 = new DateTime();
    DateTime MyDate2 = new DateTime();
    protected void Page_Load(object sender, EventArgs e)
    {
        
        if (SessionAdmin == "2")
        {
            Response.Redirect("EmployeeRegistration.aspx");
        }
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionAdmin = Session["Isadmin"].ToString();
        string ss = Session["UserId"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        lblusername.Text = Session["Usernmdisplay"].ToString();
        lblComany.Text = SessionCcode + " - " + SessionLcode;
        if (!IsPostBack)
        {
            DropDwonCategory();
            int currentYear = Utility.GetFinancialYear;
            for (int i = 0; i < 10; i++)
            {
                ddlFinance.Items.Add(new System.Web.UI.WebControls.ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                //  ddlShowYear.Items.Add(new ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                currentYear = currentYear - 1;
            }
        }
    }
    public void DropDwonCategory()
    {
        DataTable dtcate = new DataTable();
        dtcate = objdata.DropDownCategory();
        ddlcategory.DataSource = dtcate;
        ddlcategory.DataTextField = "CategoryName";
        ddlcategory.DataValueField = "CategoryCd";
        ddlcategory.DataBind();
    }
    private void clr()
    {
        DataTable dt_empty = new DataTable();
        gvsalary.DataSource = dt_empty;
        gvsalary.DataBind();
    }
    public void EmployeeType()
    {
        DataTable dtemp = new DataTable();
        dtemp = objdata.EmployeeDropDown_no(ddlcategory.SelectedValue);
        ddldept.DataSource = dtemp;
        ddldept.DataTextField = "EmpType";
        ddldept.DataValueField = "EmpTypeCd";
        ddldept.DataBind();
    }
    protected void btnEmp_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptEmpDownload.aspx");
    }
    protected void btnatt_Click(object sender, EventArgs e)
    {
        Response.Redirect("AttenanceDownload.aspx");
    }
    protected void btnLeave_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptLeaveSample.aspx");
    }
    protected void btnOT_Click(object sender, EventArgs e)
    {
        Response.Redirect("OTDownload.aspx");
    }
    protected void ddlcategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        clr();
        DataTable dtempty = new DataTable();
        ddldept.DataSource = dtempty;
        ddldept.DataBind();

        if (ddlcategory.SelectedValue == "1")
        {
            Stafflabour = "S";
        }
        else if (ddlcategory.SelectedValue == "2")
        {
            Stafflabour = "L";
        }
        else
        {
            Stafflabour = "0";
        }
        EmployeeType();
    }
    public string NumerictoNumber(int Number, bool isUK)
    {
        if (Number == 0)
            return "Zero";

        string and = isUK ? "and " : "";
        if (Number == -2147483648)
            return "Minus Two Billion One Hundred " + and +
        "Forty Seven Million Four Hundred " + and + "Eighty Three Thousand " +
        "Six Hundred " + and + "Forty Eight";

        int[] num = new int[4];
        int first = 0;
        int u, h, t = 0;
        System.Text.StringBuilder sb = new System.Text.StringBuilder();
        if (Number < 0)
        {
            sb.Append("Minus");
            Number = -Number;
        }

        string[] words0 = { "", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine" };
        string[] words1 = { "Ten ", "Eleven ", "Twelve ", "Thirteen ", "Fourteen ", "Fifteen ", "Sixteen ", "Seventeen ", "Eighteen ", "Nineteen " };
        string[] words2 = { "Twenty ", "Thirty ", "Forty ", "Fifty ", "Sixty ", "Seventy ", "Eighty ", "Ninety " };
        string[] words3 = { "Thousand ", "Lak", "Crore", "Million ", "Billion " };
        num[0] = Number % 1000;
        num[1] = Number / 1000;
        num[2] = Number / 1000000;
        num[1] = num[1] - 1000 * num[2];  // thousands
        num[2] = num[2] - 100 * num[3];//laks
        num[3] = Number / 10000000;     // billions
        num[2] = num[2] - 1000 * num[3];  // millions

        for (int i = 3; i > 0; i--)
        {
            if (num[i] != 0)
            {
                first = i;
                break;
            }
        }
        for (int i = first; i >= 0; i--)
        {
            if (num[i] == 0) continue;
            u = num[i] % 10;
            t = num[i] / 10;
            h = num[i] / 100;
            t = t - 10 * h;

            if (h > 0)
                sb.Append(words0[h] + " Hundred ");
            if (u > 0 || t > 0)
            {
                if (h > 0 || i < first)
                    sb.Append(and);

                if (t == 0)
                    sb.Append(words0[u]);
                else if (t == 1)
                    sb.Append(words1[u]);
                else
                    sb.Append(words2[t - 2] + words0[u]);

            }

            if (i != 0)
                sb.Append(words3[i - 1]);

        }

        return sb.ToString().TrimEnd();



    }
    protected void ddldept_SelectedIndexChanged(object sender, EventArgs e)
    {
        clr();
       
    }
    protected void txtsalaryday_TextChanged(object sender, EventArgs e)
    {
        clr();
    }
    protected void ddlFinance_SelectedIndexChanged(object sender, EventArgs e)
    {
        clr();
    }
    protected void btnClick_Click(object sender, EventArgs e)
    {
        try
        {
            bool ErrFlag = false;
            string Months = "", MonthValue = "";
            string TempDate = "";
            if (txtsalaryday.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Windows", "alert('Enter the Salary Date');", true);
                ErrFlag = true;
            }
            else if (ddlcategory.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Windows", "alert('Enter the Category');", true);
                ErrFlag = true;
            }
            else if (ddldept.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Windows", "alert('Enter the Employee Type');", true);
                ErrFlag = true;
            }
            else if (ddldept.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Windows", "alert('Enter the Enter the Employee Type');", true);
                ErrFlag = true;
            }
            if (!ErrFlag)
            {
                TempDate = Convert.ToDateTime(txtsalaryday.Text).AddMonths(-1).ToShortDateString();
                
                string Month = TempDate;
                DateTime m;
                m = Convert.ToDateTime(TempDate);
                d = m.Month;
                int mon = m.Month;
                int yr = m.Year;
                #region Month
                string Mont = "";
                switch (d)
                {
                    case 1:
                        Mont = "January";
                        break;

                    case 2:
                        Mont = "February";
                        break;
                    case 3:
                        Mont = "March";
                        break;
                    case 4:
                        Mont = "April";
                        break;
                    case 5:
                        Mont = "May";
                        break;
                    case 6:
                        Mont = "June";
                        break;
                    case 7:
                        Mont = "July";
                        break;
                    case 8:
                        Mont = "August";
                        break;
                    case 9:
                        Mont = "September";
                        break;
                    case 10:
                        Mont = "October";
                        break;
                    case 11:
                        Mont = "November";
                        break;
                    case 12:
                        Mont = "December";
                        break;
                    default:
                        break;
                }
                #endregion
                DataTable dt = new DataTable();
                if (ddlcategory.SelectedValue == "1")
                {
                    Stafflabour = "S";
                }
                else if (ddlcategory.SelectedValue == "2")
                {
                    Stafflabour = "L";
                }
                dt = objdata.group_employee(Mont,ddlFinance.SelectedValue,SessionCcode, SessionLcode,ddldept.SelectedValue,Stafflabour);
                if (dt.Rows.Count > 0)
                {
                    gvsalary.DataSource = dt;
                    gvsalary.DataBind();
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Windows", "alert('No Data Found');", true);
                }
            }
        }
        catch (Exception ex)
        {
        }
    }
    protected void btnCalculate_Click(object sender, EventArgs e)
    {
        try
        {
            string Months = "", MonthValue = "";
            DateTime MyDate = new DateTime();
            string TempDate = "";
            Boolean ErrFlag = false;
            DataTable dtAttenance = new DataTable();
            DataTable dtsal = new DataTable();
            DataTable dtAdvance = new DataTable();
            DataTable dtpf = new DataTable();
            DataTable DtEligible = new DataTable();
            DataTable dt_ot = new DataTable();
            DataTable dtadv = new DataTable();
            //string Months;
            string dtexist = "";
            if (gvsalary.Rows.Count > 0)
            {
                for (int i = 0; i < gvsalary.Rows.Count; i++)
                {
                    TempDate = Convert.ToDateTime(txtsalaryday.Text).AddMonths(-1).ToShortDateString();
                    TransDate = DateTime.ParseExact(TempDate, "dd/MM/yyyy", null);
                    EmpNo = "0";
                    Label lblEmpNo = (Label)gvsalary.Rows[i].FindControl("lblEmpNo");
                    EmpNo = lblEmpNo.Text;
                    string Month = TempDate;
                    DateTime m;
                    m = Convert.ToDateTime(TempDate);
                    d = m.Month;
                    int mon = m.Month;
                    int yr = m.Year;
                    string Year = Convert.ToString(yr);
                    #region Month
                    string Mont = "";
                    switch (d)
                    {
                        case 1:
                            Mont = "January";
                            break;

                        case 2:
                            Mont = "February";
                            break;
                        case 3:
                            Mont = "March";
                            break;
                        case 4:
                            Mont = "April";
                            break;
                        case 5:
                            Mont = "May";
                            break;
                        case 6:
                            Mont = "June";
                            break;
                        case 7:
                            Mont = "July";
                            break;
                        case 8:
                            Mont = "August";
                            break;
                        case 9:
                            Mont = "September";
                            break;
                        case 10:
                            Mont = "October";
                            break;
                        case 11:
                            Mont = "November";
                            break;
                        case 12:
                            Mont = "December";
                            break;
                        default:
                            break;
                    }
                    #endregion

                    //Label lblEmpNo = (Label)gvsalary.FindControl("lblEmpNo");
                    dtpf = objdata.Load_pf_details(SessionCcode, SessionLcode);
                    if (dtpf.Rows.Count > 0)
                    {
                        PF_Per = dtpf.Rows[0]["PF_per"].ToString();
                        stamp = dtpf.Rows[0]["StampCg"].ToString();
                        PF_Salary = dtpf.Rows[0]["StaffSalary"].ToString();
                        VDA_point = dtpf.Rows[0]["VDA1"].ToString();
                        Union_val = dtpf.Rows[0]["Union_val"].ToString();
                        ESI_per = dtpf.Rows[0]["ESI_per"].ToString();

                    }
                    else
                    {
                    }
                    
                    {
                        Boolean Isregistered = false;
                        txtDays = "0";
                        work = "0";
                        NFh = "0";
                        Weekoff = "0";
                        AbsentDays = "0";
                        cl = "0";
                        OT = "0";
                        All1 = "0";
                        All2 = "0";
                        All3 = "0";
                        All4 = "0";
                        Deduction1 = "0";
                        Deduction2 = "0";
                        Deduction3 = "0";
                        Deduction4 = "0";
                        Advance = "0";
                        union = "0";
                        LOP = "0";
                        Basic = "0";
                        FDA = "0";
                        VDA = "0";
                        HRA = "0";
                        
                        PF = "0";
                        ESI = "0";
                        PFEarnings = "0";
                        GrossEarnings = "0";
                        grossDeduction = "0";
                        NetPay = "0";
                        TextAmount = "0";
                        lop = "0";
                        Basic_temp = "0";
                        FDA_temp = "0";
                        VDA_temp = "0";
                        HRA_temp = "0";
                        All1_temp = "0";
                        All2_temp = "0";
                        Ded1_temp = "0";
                        Ded2_temp = "0";
                        pf_val = "0";
                        ESI_val = "0";
                        Exist = "0";
                        Adv_id = null;
                        dtAttenance = objdata.GroupSalary_Load_attenance(EmpNo, SessionCcode, SessionLcode, Mont, ddlFinance.SelectedValue);
                        if (dtAttenance.Rows.Count > 0)
                        {
                            txtDays = dtAttenance.Rows[0]["Days"].ToString();
                            work = dtAttenance.Rows[0]["WorkingDays"].ToString();
                            NFh = dtAttenance.Rows[0]["NFh"].ToString();
                            Weekoff = dtAttenance.Rows[0]["Weekoff"].ToString();
                            AbsentDays = dtAttenance.Rows[0]["AbsentDays"].ToString();
                            cl = dtAttenance.Rows[0]["CL"].ToString();
                        }
                        else
                        {
                            ErrFlag = true;
                        }
                        dtexist = objdata.Group_Existing(EmpNo, SessionCcode, SessionLcode);
                        if (dtexist.Trim() == "")
                        {
                            ErrFlag = true;
                        }
                        dtsal = objdata.Group_salary_load(EmpNo, SessionCcode, SessionLcode);
                        if (dtsal.Rows.Count > 0)
                        {
                            Basic_temp = dtsal.Rows[0]["Base"].ToString();
                            FDA_temp = dtsal.Rows[0]["FDA"].ToString();
                            VDA_temp = dtsal.Rows[0]["VDA"].ToString();
                            HRA_temp = dtsal.Rows[0]["HRA"].ToString();
                            union = dtsal.Rows[0]["Unioncharges"].ToString();
                            All1_temp = dtsal.Rows[0]["Alllowance1amt"].ToString();
                            All2_temp = dtsal.Rows[0]["Allowance2amt"].ToString();
                            Ded1_temp = dtsal.Rows[0]["Deduction1"].ToString();
                            Ded2_temp = dtsal.Rows[0]["Deduction2"].ToString();
                        }
                        else
                        {
                            ErrFlag = true;
                        }

                        DtEligible = objdata.EligibleESI_PF(SessionCcode, SessionLcode, EmpNo);
                        if (DtEligible.Rows.Count > 0)
                        {
                            if (DtEligible.Rows[0]["EligiblePF"].ToString() == "1")
                            {
                                pf_val = "1";
                            }
                            else
                            {
                                pf_val = "0";
                            }
                            if (DtEligible.Rows[0]["ElgibleESI"].ToString() == "1")
                            {
                                ESI_val = "1";
                            }
                            else
                            {
                                ESI_val = "0";
                            }
                            if (DtEligible.Rows[0]["PF_Type"].ToString() == "1")
                            {
                                pf_special = "1";
                            }
                            else
                            {
                                pf_special = "0";
                            }
                            
                        }
                        else
                        {
                            ErrFlag = true;
                        }

                        dt_ot = objdata.Group_ot_load(EmpNo, Mont, ddlFinance.SelectedValue, SessionCcode, SessionLcode);
                        if (dt_ot.Rows.Count > 0)
                        {
                            OT = dt_ot.Rows[0]["netAmount"].ToString();
                        }
                        else
                        {
                            OT = "0";
                        }
                        
                        dtadv = objdata.Salary_advance_retrive(EmpNo, SessionCcode, SessionLcode);
                        if (dtadv.Rows.Count > 0)
                        {
                            Adv_id = dtadv.Rows[0]["ID"].ToString();
                            Adv_BalanceAmt = dtadv.Rows[0]["BalanceAmount"].ToString();
                            Adv_due = dtadv.Rows[0]["MonthlyDeduction"].ToString();
                            Increment_mont = dtadv.Rows[0]["IncreaseMonth"].ToString();

                            Dec_mont = dtadv.Rows[0]["ReductionMonth"].ToString();
                            if (Dec_mont == "1")
                            {
                                Advance = Adv_BalanceAmt;

                            }
                            else if (Convert.ToDecimal(Adv_BalanceAmt) < Convert.ToDecimal(Adv_due))
                            {
                                Advance = Adv_BalanceAmt;

                            }
                            else
                            {
                                Advance = dtadv.Rows[0]["MonthlyDeduction"].ToString();

                            }
                        }
                        else
                        {
                            Adv_id = null;
                            Adv_BalanceAmt = "0";
                            Adv_due = "0";
                            Increment_mont = "0";
                        }
                        if (!ErrFlag)
                        {
                            #region EmpType-1
                            if (ddldept.SelectedValue == "1")
                            {
                                FixedBasic = Basic_temp;
                                FixedFDA = "0";
                                FixedHRA = "0";
                                Basic = (Convert.ToDecimal(Basic_temp)).ToString();
                                All1 = (Convert.ToDecimal(All1_temp)).ToString();
                                All1 = ((Convert.ToDecimal(txtDays)) * Convert.ToDecimal(All1)).ToString();
                                All1 = (Math.Round(Convert.ToDecimal(All1), 0, MidpointRounding.AwayFromZero)).ToString();
                                All2 = (Convert.ToDecimal(All2_temp) / 26).ToString();

                                All2 = ((Convert.ToDecimal(txtDays) + Convert.ToInt32(cl)) * Convert.ToDecimal(All2)).ToString();
                                All2 = (Math.Round(Convert.ToDecimal(All2), 0, MidpointRounding.AwayFromZero)).ToString();
                                All3 = "0";
                                All4 = "0";
                                Deduction1 = (Convert.ToDecimal(Ded1_temp) / 26).ToString();
                                Deduction1 = ((Convert.ToDecimal(txtDays) + Convert.ToInt32(cl)) * Convert.ToDecimal(Deduction1)).ToString();
                                Deduction1 = (Math.Round(Convert.ToDecimal(Deduction1), 0, MidpointRounding.AwayFromZero)).ToString();
                                Deduction2 = (Convert.ToDecimal(Ded2_temp) / 26).ToString();
                                Deduction2 = ((Convert.ToDecimal(txtDays) + Convert.ToInt32(cl)) * Convert.ToDecimal(Deduction2)).ToString();
                                Deduction2 = (Math.Round(Convert.ToDecimal(Deduction2), 0, MidpointRounding.AwayFromZero)).ToString();
                                Deduction3 = "0";
                                Deduction4 = "0";
                                LOP = ((Convert.ToDecimal(Basic) / 26) * Convert.ToDecimal(AbsentDays)).ToString();
                                LOP = (Math.Round(Convert.ToDecimal(LOP), 0, MidpointRounding.AwayFromZero)).ToString();
                                if (pf_val == "1")
                                {
                                    if (pf_special == "1")
                                    {
                                        if (Convert.ToDecimal(Basic) >= Convert.ToDecimal(PF_Salary))
                                        {
                                            PF = ((Convert.ToDecimal(PF_Salary) * Convert.ToDecimal(PF_Per)) / 100).ToString();
                                            PF = (Math.Round(Convert.ToDecimal(PF), 0, MidpointRounding.AwayFromZero)).ToString();
                                        }
                                        else
                                        {
                                            PF = ((Convert.ToDecimal(Basic) * Convert.ToDecimal(PF_Per)) / 100).ToString();
                                            PF = (Math.Round(Convert.ToDecimal(PF), 0, MidpointRounding.AwayFromZero)).ToString();
                                        }
                                    }
                                    else
                                    {
                                        PF = ((Convert.ToDecimal(Basic) * Convert.ToDecimal(PF_Per)) / 100).ToString();
                                        PF = (Math.Round(Convert.ToDecimal(PF), 0, MidpointRounding.AwayFromZero)).ToString();
                                    }
                                }
                                else
                                {
                                    PF = "0";
                                }
                                if (ESI_val == "1")
                                {
                                    
                                    ESI = ((Convert.ToDecimal(Basic) * Convert.ToDecimal(ESI_per)) / 100).ToString();
                                    ESI = (Math.Round(Convert.ToDecimal(ESI), 2, MidpointRounding.ToEven)).ToString();
                                    string[] split_val = ESI.Split('.');
                                    string Ist = split_val[0].ToString();
                                    string IInd = split_val[1].ToString();
                                    if (Convert.ToInt32(IInd) > 0)
                                    {
                                        ESI = (Convert.ToInt32(Ist) + 1).ToString();
                                    }
                                    else
                                    {
                                        ESI = Ist;
                                    }
                                }
                                else
                                {
                                    ESI = "0";
                                }
                                if (Union_val == "1")
                                {
                                    
                                }
                                else
                                {
                                    union = "0";
                                }
                                GrossEarnings = (Convert.ToDecimal(Basic) + Convert.ToDecimal(FDA) + Convert.ToDecimal(VDA) + Convert.ToDecimal(HRA) + Convert.ToDecimal(OT) + Convert.ToDecimal(All1) + Convert.ToDecimal(All2) + Convert.ToDecimal(All3) + Convert.ToDecimal(All4)).ToString();
                                grossDeduction = (Convert.ToDecimal(PF) + Convert.ToDecimal(ESI) + Convert.ToDecimal(stamp) + Convert.ToDecimal(union) + Convert.ToDecimal(Advance) + Convert.ToDecimal(Deduction1) + Convert.ToDecimal(Deduction2) + Convert.ToDecimal(Deduction3) + Convert.ToDecimal(Deduction4)).ToString();
                                NetPay = (Convert.ToDecimal(GrossEarnings) - Convert.ToDecimal(grossDeduction)).ToString();
                                NetPay = Math.Round(Convert.ToDecimal(NetPay), 0, MidpointRounding.AwayFromZero).ToString();
                                TextAmount = NumerictoNumber(Convert.ToInt32(NetPay), isUK).ToString() + " " + "Only";
                                
                                
                                objSal.TotalWorkingDays = work;
                                objSal.NFh = NFh;
                                objSal.Lcode = SessionLcode;
                                objSal.Ccode = SessionCcode;
                                objSal.ApplyLeaveDays = AbsentDays;
                                objSal.SalaryDate = txtsalaryday.Text;
                                MyDate = DateTime.ParseExact(objSal.SalaryDate, "dd-MM-yyyy", null);
                                objSal.AvailableDays = txtDays;
                                objSal.SalaryDate = txtsalaryday.Text;
                                objSal.work = txtDays;
                                objSal.weekoff = Weekoff;
                                objSal.CL = cl;
                                objSal.BasicAndDA = Basic;
                                objSal.HRA = "0";
                                objSal.FDA = "0";
                                objSal.VDA = "0";
                                objSal.OT = "0";
                                objSal.Allowances1 = All1;
                                objSal.Allowances2 = All2;
                                objSal.Allowances3 = "0";
                                objSal.Allowances4 = "0";
                                objSal.Deduction1 = Deduction1;
                                objSal.Deduction2 = Deduction2;
                                objSal.Deduction3 = "0";
                                objSal.Deduction4 = "0";
                                objSal.ProvidentFund = PF;
                                objSal.ESI = ESI;
                                objSal.Stamp = "0";
                                objSal.Advance = Advance;
                                objSal.LossofPay = lop;
                                objSal.LOPDays = "0";
                                objSal.Union = "0";
                                objSal.GrossEarnings = GrossEarnings;
                                objSal.TotalDeduction = grossDeduction;
                                objSal.NetPay = NetPay;
                                objSal.Words = TextAmount;
                                objSal.FixedBase = FixedBasic;
                                objSal.FixedFDA = FixedFDA;
                                objSal.FixedFRA = FixedHRA;
                                
                                if (pf_val == "1")
                                {
                                    if (Convert.ToDecimal(Basic) >= Convert.ToDecimal(PF_Salary))
                                    {
                                        PFEarnings = PF_Salary;
                                    }
                                    else
                                    {
                                        PFEarnings = Basic;
                                    }
                                }
                                else
                                {
                                    PFEarnings = Basic;
                                }
                                Isregistered = true;
                                objSal.PfSalary = PFEarnings;
                                if (Convert.ToDecimal(objSal.PfSalary) > 6500)
                                {
                                    objSal.Emp_PF = "6500";
                                }
                                else
                                {
                                    objSal.Emp_PF = objSal.PfSalary;
                                }
                            }
                            #endregion
                            #region EmpType-2
                            else if (ddldept.SelectedValue == "2")
                            {
                                FixedBasic = Basic_temp;
                                FixedFDA = FDA_temp;
                                FixedHRA = HRA_temp;
                                Basic = ((Convert.ToDecimal(Basic_temp)) / 26).ToString();
                                Basic = ((Convert.ToDecimal(txtDays) + Convert.ToDecimal(cl) + Convert.ToDecimal(NFh)) * (Convert.ToDecimal(Basic))).ToString();
                                Basic = (Math.Round(Convert.ToDecimal(Basic), 2, MidpointRounding.ToEven)).ToString();
                                FDA = ((Convert.ToDecimal(FDA_temp)) / 26).ToString();
                                FDA = ((Convert.ToDecimal(txtDays) + Convert.ToDecimal(cl) + Convert.ToDecimal(NFh)) * (Convert.ToDecimal(FDA))).ToString();
                                FDA = (Math.Round(Convert.ToDecimal(FDA), 2, MidpointRounding.ToEven)).ToString();
                                //decimal vdaval = (Convert.ToDecimal(dtpf.Rows[0]["VDA1"].ToString()) - Convert.ToDecimal(dtpf.Rows[0]["VDA2"].ToString()));
                                VDA = ((Convert.ToDecimal(VDA_point) * Convert.ToDecimal(VDA_temp)) / 26).ToString();
                                VDA = ((Convert.ToDecimal(txtDays) + Convert.ToInt32(cl) + Convert.ToInt32(NFh)) * (Convert.ToDecimal(VDA))).ToString();
                                VDA = (Math.Round(Convert.ToDecimal(VDA), 2, MidpointRounding.ToEven)).ToString();
                                HRA = (Convert.ToDecimal(HRA_temp) / 26).ToString();
                                HRA = ((Convert.ToDecimal(txtDays) + Convert.ToInt32(cl)) * (Convert.ToDecimal(HRA))).ToString();
                                HRA = (Math.Round(Convert.ToDecimal(HRA), 0, MidpointRounding.ToEven)).ToString();
                                All1 = (Convert.ToDecimal(All1_temp)).ToString();
                                All1 = ((Convert.ToDecimal(txtDays)) * Convert.ToDecimal(All1)).ToString();
                                All1 = (Math.Round(Convert.ToDecimal(All1), 0, MidpointRounding.AwayFromZero)).ToString();
                                All2 = (Convert.ToDecimal(All2_temp) / 26).ToString();

                                All2 = ((Convert.ToDecimal(txtDays) + Convert.ToInt32(cl)) * Convert.ToDecimal(All2)).ToString();
                                All2 = (Math.Round(Convert.ToDecimal(All2), 0, MidpointRounding.AwayFromZero)).ToString();
                                Deduction1 = (Convert.ToDecimal(Ded1_temp) / 26).ToString();
                                Deduction1 = ((Convert.ToDecimal(txtDays) + Convert.ToInt32(cl)) * Convert.ToDecimal(Deduction1)).ToString();
                                Deduction1 = (Math.Round(Convert.ToDecimal(Deduction1), 0, MidpointRounding.AwayFromZero)).ToString();
                                Deduction2 = (Convert.ToDecimal(Ded2_temp) / 26).ToString();
                                Deduction2 = ((Convert.ToDecimal(txtDays) + Convert.ToInt32(cl)) * Convert.ToDecimal(Deduction2)).ToString();
                                Deduction2 = (Math.Round(Convert.ToDecimal(Deduction2), 0, MidpointRounding.AwayFromZero)).ToString();
                                All3 = "0";
                                All4 = "0";
                                Deduction3 = "0";
                                Deduction4 = "0";
                                if (pf_val == "1")
                                {
                                    if (pf_special == "1")
                                    {
                                        decimal PF_val_dum = (Convert.ToDecimal(Basic) + Convert.ToDecimal(FDA) + Convert.ToDecimal(VDA));
                                        if (PF_val_dum >= Convert.ToDecimal(PF_Salary))
                                        {
                                            PF = ((Convert.ToDecimal(PF_Salary) * Convert.ToDecimal(PF_Per)) / 100).ToString();
                                            PF = (Math.Round(Convert.ToDecimal(PF), 0, MidpointRounding.AwayFromZero)).ToString();
                                        }
                                        else
                                        {
                                            decimal PF_val1 = (Convert.ToDecimal(Basic) + Convert.ToDecimal(FDA) + Convert.ToDecimal(VDA));
                                            PF = ((PF_val1 * Convert.ToDecimal(PF_Per)) / 100).ToString();
                                            PF = (Math.Round(Convert.ToDecimal(PF), 0, MidpointRounding.AwayFromZero)).ToString();
                                        }
                                    }
                                    else
                                    {
                                        decimal PF_val1 = (Convert.ToDecimal(Basic) + Convert.ToDecimal(FDA) + Convert.ToDecimal(VDA));
                                        PF = ((PF_val1 * Convert.ToDecimal(PF_Per)) / 100).ToString();
                                        PF = (Math.Round(Convert.ToDecimal(PF), 0, MidpointRounding.AwayFromZero)).ToString();
                                    }
                                }
                                else
                                {
                                    PF = "0";
                                }
                                if (ESI_val == "1")
                                {
                                    decimal ESI_val_dum = (Convert.ToDecimal(Basic) + Convert.ToDecimal(FDA) + Convert.ToDecimal(VDA) + Convert.ToDecimal(HRA));
                                    ESI = ((Convert.ToDecimal(ESI_val_dum) * Convert.ToDecimal(ESI_per)) / 100).ToString();
                                    ESI = (Math.Round(Convert.ToDecimal(ESI), 2, MidpointRounding.ToEven)).ToString();
                                    string[] split_val = ESI.Split('.');
                                    string Ist = split_val[0].ToString();
                                    string IInd = split_val[1].ToString();
                                    if (Convert.ToInt32(IInd) > 0)
                                    {
                                        ESI = (Convert.ToInt32(Ist) + 1).ToString();
                                    }
                                    else
                                    {
                                        ESI = Ist;
                                    }
                                }
                                else
                                {
                                    ESI = "0";
                                }
                                if (Union_val == "1")
                                {

                                }
                                else
                                {
                                    union = "0";
                                }
                                GrossEarnings = (Convert.ToDecimal(Basic) + Convert.ToDecimal(FDA) + Convert.ToDecimal(VDA) + Convert.ToDecimal(HRA) + Convert.ToDecimal(OT) + Convert.ToDecimal(All1) + Convert.ToDecimal(All2) + Convert.ToDecimal(All3) + Convert.ToDecimal(All4)).ToString();
                                grossDeduction = (Convert.ToDecimal(PF) + Convert.ToDecimal(ESI) + Convert.ToDecimal(stamp) + Convert.ToDecimal(union) + Convert.ToDecimal(Advance) + Convert.ToDecimal(Deduction1) + Convert.ToDecimal(Deduction2) + Convert.ToDecimal(Deduction3) + Convert.ToDecimal(Deduction4)).ToString();
                                NetPay = (Convert.ToDecimal(GrossEarnings) - Convert.ToDecimal(grossDeduction)).ToString();
                                NetPay = Math.Round(Convert.ToDecimal(NetPay), 0, MidpointRounding.AwayFromZero).ToString();
                                TextAmount = NumerictoNumber(Convert.ToInt32(NetPay), isUK).ToString() + " " + "Only";
                                objSal.TotalWorkingDays = work;
                                objSal.NFh = NFh;
                                objSal.Lcode = SessionLcode;
                                objSal.Ccode = SessionCcode;
                                objSal.ApplyLeaveDays = AbsentDays;

                                objSal.AvailableDays = txtDays;
                                objSal.SalaryDate = txtsalaryday.Text;
                                objSal.work = txtDays;
                                objSal.weekoff = Weekoff;
                                objSal.CL = cl;
                                objSal.BasicAndDA = Basic;
                                objSal.HRA = HRA;
                                objSal.FDA = FDA;
                                objSal.VDA = VDA;
                                objSal.OT = OT;
                                objSal.Allowances1 = All1;
                                objSal.Allowances2 = All2;
                                objSal.Allowances3 = "0";
                                objSal.Allowances4 = "0";
                                objSal.Deduction1 = Deduction1;
                                objSal.Deduction2 = Deduction2;
                                objSal.Deduction3 = "0";
                                objSal.Deduction4 = "0";
                                objSal.ProvidentFund = PF;
                                objSal.ESI = ESI;
                                objSal.Stamp = stamp;
                                objSal.Advance = Advance;
                                objSal.LossofPay = "0.00";
                                objSal.Union = union;
                                objSal.GrossEarnings = GrossEarnings;
                                objSal.TotalDeduction = grossDeduction;
                                objSal.NetPay = NetPay;
                                objSal.Words = TextAmount;
                                objSal.SalaryDate = txtsalaryday.Text;
                                objSal.FixedBase = FixedBasic;
                                objSal.FixedFDA = FixedFDA;
                                objSal.FixedFRA = FixedHRA;
                                
                                MyDate = DateTime.ParseExact(objSal.SalaryDate, "dd-MM-yyyy", null);
                                if (pf_val == "1")
                                {
                                    if (Convert.ToDecimal(Basic) >= Convert.ToDecimal(PF_Salary))
                                    {
                                        PFEarnings = PF_Salary;
                                    }
                                    else
                                    {
                                        PFEarnings = (Convert.ToDecimal(Basic) + Convert.ToDecimal(FDA) + Convert.ToDecimal(VDA)).ToString();
                                    }
                                }
                                else
                                {
                                    PFEarnings = Basic;
                                }
                                objSal.PfSalary = PFEarnings;
                                if (Convert.ToDecimal(objSal.PfSalary) > 6500)
                                {
                                    objSal.Emp_PF = "6500";
                                }
                                else
                                {
                                    objSal.Emp_PF = objSal.PfSalary;
                                }
                                Isregistered = true;

                            }
                            #endregion
                            #region EmpType-3
                            else if (ddldept.SelectedValue == "3")
                            {
                                FixedBasic = (26 * (Convert.ToDecimal(Basic_temp))).ToString();
                                FixedFDA = "0";
                                FixedHRA = "0";
                                Basic = ((Convert.ToDecimal(txtDays) + Convert.ToDecimal(NFh)) * (Convert.ToDecimal(Basic_temp))).ToString();
                                Basic = (Math.Round(Convert.ToDecimal(Basic), 2, MidpointRounding.ToEven)).ToString();
                                All1 = ((Convert.ToDecimal(All1_temp)) * (Convert.ToDecimal(txtDays))).ToString();
                                All1 = (Math.Round(Convert.ToDecimal(All1), 2, MidpointRounding.ToEven)).ToString();
                                All2 = ((Convert.ToDecimal(All2_temp)) * (Convert.ToDecimal(txtDays))).ToString();
                                All2 = (Math.Round(Convert.ToDecimal(All2), 2, MidpointRounding.ToEven)).ToString();
                                Deduction1 = (Convert.ToDecimal(Ded1_temp) * (Convert.ToDecimal(txtDays))).ToString();
                                Deduction1 = (Math.Round(Convert.ToDecimal(Deduction1), 2, MidpointRounding.ToEven)).ToString();
                                Deduction2 = (Convert.ToDecimal(Ded2_temp) * (Convert.ToDecimal(txtDays))).ToString();
                                Deduction2 = (Math.Round(Convert.ToDecimal(Deduction2), 2, MidpointRounding.ToEven)).ToString();
                                All3= "0";
                                All4 = "0";
                                Deduction3 = "0";
                                Deduction4 = "0";
                                if (pf_val == "1")
                                {
                                    if (pf_special == "1")
                                    {
                                        if (Convert.ToDecimal(Basic) >= Convert.ToDecimal(PF_Salary))
                                        {
                                            PF = ((Convert.ToDecimal(PF_Salary) * Convert.ToDecimal(PF_Per)) / 100).ToString();
                                            PF = (Math.Round(Convert.ToDecimal(PF), 0, MidpointRounding.AwayFromZero)).ToString();
                                        }
                                        else
                                        {
                                            PF = ((Convert.ToDecimal(Basic) * Convert.ToDecimal(PF_Per)) / 100).ToString();
                                            PF = (Math.Round(Convert.ToDecimal(PF), 0, MidpointRounding.AwayFromZero)).ToString();
                                        }
                                    }
                                    else
                                    {
                                        PF = ((Convert.ToDecimal(Basic) * Convert.ToDecimal(PF_Per)) / 100).ToString();
                                        PF = (Math.Round(Convert.ToDecimal(PF), 0, MidpointRounding.AwayFromZero)).ToString();
                                    }
                                }
                                else
                                {
                                    PF = "0";
                                }
                                if (ESI_val == "1")
                                {

                                    ESI = ((Convert.ToDecimal(Basic) * Convert.ToDecimal(ESI_per)) / 100).ToString();
                                    ESI = (Math.Round(Convert.ToDecimal(ESI), 2, MidpointRounding.ToEven)).ToString();
                                    string[] split_val = ESI.Split('.');
                                    string Ist = split_val[0].ToString();
                                    string IInd = split_val[1].ToString();
                                    if (Convert.ToInt32(IInd) > 0)
                                    {
                                        ESI = (Convert.ToInt32(Ist) + 1).ToString();
                                    }
                                    else
                                    {
                                        ESI = Ist;
                                    }
                                }
                                else
                                {
                                    ESI = "0";
                                }
                                if (Union_val == "1")
                                {

                                }
                                else
                                {
                                    union = "0";
                                }
                                GrossEarnings = (Convert.ToDecimal(Basic) + Convert.ToDecimal(OT) + Convert.ToDecimal(All1) + Convert.ToDecimal(All2) + Convert.ToDecimal(All3) + Convert.ToDecimal(All4)).ToString();
                                grossDeduction = (Convert.ToDecimal(PF) + Convert.ToDecimal(ESI) + Convert.ToDecimal(stamp) + Convert.ToDecimal(union) + Convert.ToDecimal(Advance) + Convert.ToDecimal(Deduction1) + Convert.ToDecimal(Deduction2) + Convert.ToDecimal(Deduction3) + Convert.ToDecimal(Deduction4)).ToString();
                                NetPay = (Convert.ToDecimal(GrossEarnings) - Convert.ToDecimal(grossDeduction)).ToString();
                                NetPay = Math.Round(Convert.ToDecimal(NetPay), 0, MidpointRounding.AwayFromZero).ToString();
                                TextAmount = NumerictoNumber(Convert.ToInt32(NetPay), isUK).ToString() + " " + "Only";
                                objSal.TotalWorkingDays = work;
                                objSal.NFh = NFh;
                                objSal.Lcode = SessionLcode;
                                objSal.Ccode = SessionCcode;
                                objSal.ApplyLeaveDays = AbsentDays;

                                objSal.AvailableDays = txtDays;
                                objSal.SalaryDate = txtsalaryday.Text;
                                objSal.work = txtDays;
                                objSal.weekoff = Weekoff;
                                objSal.CL = cl;
                                objSal.BasicAndDA = Basic;
                                objSal.HRA = "0";
                                objSal.FDA = "0";
                                objSal.VDA = "0";
                                objSal.OT = "0";
                                objSal.Allowances1 = All1;
                                objSal.Allowances2 = All2;
                                objSal.Allowances3 = "0";
                                objSal.Allowances4 = "0";
                                objSal.Deduction1 = Deduction1;
                                objSal.Deduction2 = Deduction2;
                                objSal.Deduction3 = "0";
                                objSal.Deduction4 = "0";
                                objSal.ProvidentFund = PF;
                                objSal.ESI = ESI;
                                objSal.Stamp = "0";
                                objSal.Advance = Advance;
                                objSal.LossofPay = "0";
                                objSal.Union = union;
                                objSal.GrossEarnings = GrossEarnings;
                                objSal.TotalDeduction = grossDeduction;
                                objSal.NetPay = NetPay;
                                objSal.Words = TextAmount;
                                objSal.SalaryDate = txtsalaryday.Text;
                                objSal.FixedBase = FixedBasic;
                                objSal.FixedFDA = FixedFDA;
                                objSal.FixedFRA = FixedHRA;
                                MyDate = DateTime.ParseExact(objSal.SalaryDate, "dd-MM-yyyy", null);
                                if (pf_val == "1")
                                {
                                    if (Convert.ToDecimal(Basic) >= Convert.ToDecimal(PF_Salary))
                                    {
                                        PFEarnings = PF_Salary;
                                    }
                                    else
                                    {
                                        PFEarnings = Basic;
                                    }
                                }
                                else
                                {
                                    PFEarnings = Basic;
                                }
                                objSal.PfSalary = PFEarnings;
                                if (Convert.ToDecimal(objSal.PfSalary) > 6500)
                                {
                                    objSal.Emp_PF = "6500";
                                }
                                else
                                {
                                    objSal.Emp_PF = objSal.PfSalary;
                                }
                                Isregistered = true;
                            }
                            #endregion
                            #region EmpType-4
                            else if (ddldept.SelectedValue == "4")
                            {
                                FixedBasic = (26 * (Convert.ToDecimal(Basic_temp))).ToString();
                                FixedFDA = "0";
                                FixedHRA = "0";
                                Basic = ((Convert.ToDecimal(txtDays) + Convert.ToInt32(cl) + Convert.ToInt32(NFh)) * (Convert.ToDecimal(Basic_temp))).ToString();
                                Basic = (Math.Round(Convert.ToDecimal(Basic), 2, MidpointRounding.ToEven)).ToString();
                                //FDA = ((Convert.ToDecimal(FDA_temp)) / 26).ToString();
                                //FDA = ((Convert.ToDecimal(txtDays) + Convert.ToDecimal(cl) + Convert.ToDecimal(NFh)) * (Convert.ToDecimal(FDA))).ToString();
                                //FDA = (Math.Round(Convert.ToDecimal(FDA), 2, MidpointRounding.ToEven)).ToString();
                                ////decimal vdaval = (Convert.ToDecimal(dtpf.Rows[0]["VDA1"].ToString()) - Convert.ToDecimal(dtpf.Rows[0]["VDA2"].ToString()));
                                //VDA = ((Convert.ToDecimal(VDA_point) * Convert.ToDecimal(VDA_temp)) / 26).ToString();
                                //VDA = ((Convert.ToDecimal(txtDays) + Convert.ToInt32(cl) + Convert.ToInt32(NFh)) * (Convert.ToDecimal(VDA))).ToString();
                                //VDA = (Math.Round(Convert.ToDecimal(VDA), 2, MidpointRounding.ToEven)).ToString();
                                //HRA = (Convert.ToDecimal(HRA_temp) / 26).ToString();
                                //HRA = ((Convert.ToDecimal(txtDays) + Convert.ToInt32(cl)) * (Convert.ToDecimal(HRA))).ToString();
                                //HRA = (Math.Round(Convert.ToDecimal(HRA), 0, MidpointRounding.ToEven)).ToString();
                                FDA = "0";
                                VDA = "0";
                                HRA = "0";
                                All1 = (Convert.ToDecimal(All1_temp)).ToString();
                                All1 = ((Convert.ToDecimal(txtDays)) * Convert.ToDecimal(All1)).ToString();
                                All1 = (Math.Round(Convert.ToDecimal(All1), 0, MidpointRounding.AwayFromZero)).ToString();
                                All2 = (Convert.ToDecimal(All2_temp) / 26).ToString();

                                All2 = ((Convert.ToDecimal(txtDays) + Convert.ToInt32(cl)) * Convert.ToDecimal(All2)).ToString();
                                All2 = (Math.Round(Convert.ToDecimal(All2), 0, MidpointRounding.AwayFromZero)).ToString();
                                Deduction1 = (Convert.ToDecimal(Ded1_temp) / 26).ToString();
                                Deduction1 = ((Convert.ToDecimal(txtDays) + Convert.ToInt32(cl)) * Convert.ToDecimal(Deduction1)).ToString();
                                Deduction1 = (Math.Round(Convert.ToDecimal(Deduction1), 0, MidpointRounding.AwayFromZero)).ToString();
                                Deduction2 = (Convert.ToDecimal(Ded2_temp) / 26).ToString();
                                Deduction2 = ((Convert.ToDecimal(txtDays) + Convert.ToInt32(cl)) * Convert.ToDecimal(Deduction2)).ToString();
                                Deduction2 = (Math.Round(Convert.ToDecimal(Deduction2), 0, MidpointRounding.AwayFromZero)).ToString();
                                All3 = "0";
                                All4 = "0";
                                Deduction3 = "0";
                                Deduction4 = "0";
                                if (pf_val == "1")
                                {
                                    if (pf_special == "1")
                                    {
                                        decimal PF_val_dum = (Convert.ToDecimal(Basic));
                                        if (PF_val_dum >= Convert.ToDecimal(PF_Salary))
                                        {
                                            PF = ((Convert.ToDecimal(PF_Salary) * Convert.ToDecimal(PF_Per)) / 100).ToString();
                                            PF = (Math.Round(Convert.ToDecimal(PF), 0, MidpointRounding.AwayFromZero)).ToString();
                                        }
                                        else
                                        {
                                            decimal PF_val1 = (Convert.ToDecimal(Basic));
                                            PF = ((PF_val1 * Convert.ToDecimal(PF_Per)) / 100).ToString();
                                            PF = (Math.Round(Convert.ToDecimal(PF), 0, MidpointRounding.AwayFromZero)).ToString();
                                        }
                                    }
                                    else
                                    {
                                        decimal PF_val1 = (Convert.ToDecimal(Basic));
                                        PF = ((PF_val1 * Convert.ToDecimal(PF_Per)) / 100).ToString();
                                        PF = (Math.Round(Convert.ToDecimal(PF), 0, MidpointRounding.AwayFromZero)).ToString();
                                    }
                                }
                                else
                                {
                                    PF = "0";
                                }
                                if (ESI_val == "1")
                                {
                                    decimal ESI_val_dum = (Convert.ToDecimal(Basic));
                                    ESI = ((Convert.ToDecimal(ESI_val_dum) * Convert.ToDecimal(ESI_per)) / 100).ToString();
                                    ESI = (Math.Round(Convert.ToDecimal(ESI), 2, MidpointRounding.ToEven)).ToString();
                                    string[] split_val = ESI.Split('.');
                                    string Ist = split_val[0].ToString();
                                    string IInd = split_val[1].ToString();
                                    if (Convert.ToInt32(IInd) > 0)
                                    {
                                        ESI = (Convert.ToInt32(Ist) + 1).ToString();
                                    }
                                    else
                                    {
                                        ESI = Ist;
                                    }
                                }
                                else
                                {
                                    ESI = "0";
                                }
                                if (Union_val == "1")
                                {

                                }
                                else
                                {
                                    union = "0";
                                }
                                GrossEarnings = (Convert.ToDecimal(Basic) + Convert.ToDecimal(FDA) + Convert.ToDecimal(VDA) + Convert.ToDecimal(HRA) + Convert.ToDecimal(OT) + Convert.ToDecimal(All1) + Convert.ToDecimal(All2) + Convert.ToDecimal(All3) + Convert.ToDecimal(All4)).ToString();
                                grossDeduction = (Convert.ToDecimal(PF) + Convert.ToDecimal(ESI) + Convert.ToDecimal(stamp) + Convert.ToDecimal(union) + Convert.ToDecimal(Advance) + Convert.ToDecimal(Deduction1) + Convert.ToDecimal(Deduction2) + Convert.ToDecimal(Deduction3) + Convert.ToDecimal(Deduction4)).ToString();
                                NetPay = (Convert.ToDecimal(GrossEarnings) - Convert.ToDecimal(grossDeduction)).ToString();
                                NetPay = Math.Round(Convert.ToDecimal(NetPay), 0, MidpointRounding.AwayFromZero).ToString();
                                TextAmount = NumerictoNumber(Convert.ToInt32(NetPay), isUK).ToString() + " " + "Only";
                                objSal.TotalWorkingDays = work;
                                objSal.NFh = NFh;
                                objSal.Lcode = SessionLcode;
                                objSal.Ccode = SessionCcode;
                                objSal.ApplyLeaveDays = AbsentDays;

                                objSal.AvailableDays = txtDays;
                                objSal.SalaryDate = txtsalaryday.Text;
                                objSal.work = txtDays;
                                objSal.weekoff = Weekoff;
                                objSal.CL = cl;
                                objSal.BasicAndDA = Basic;
                                objSal.HRA = HRA;
                                objSal.FDA = FDA;
                                objSal.VDA = VDA;
                                objSal.OT = OT;
                                objSal.Allowances1 = All1;
                                objSal.Allowances2 = All2;
                                objSal.Allowances3 = "0";
                                objSal.Allowances4 = "0";
                                objSal.Deduction1 = Deduction1;
                                objSal.Deduction2 = Deduction2;
                                objSal.Deduction3 = "0";
                                objSal.Deduction4 = "0";
                                objSal.ProvidentFund = PF;
                                objSal.ESI = ESI;
                                objSal.Stamp = "0";
                                objSal.Advance = Advance;
                                objSal.LossofPay = "0.00";
                                objSal.LOPDays = AbsentDays;
                                objSal.Union = union;
                                objSal.GrossEarnings = GrossEarnings;
                                objSal.TotalDeduction = grossDeduction;
                                objSal.NetPay = NetPay;
                                objSal.Words = TextAmount;
                                objSal.SalaryDate = txtsalaryday.Text;
                                objSal.FixedBase = FixedBasic;
                                objSal.FixedFDA = FixedFDA;
                                objSal.FixedFRA = FixedHRA;
                                MyDate = DateTime.ParseExact(objSal.SalaryDate, "dd-MM-yyyy", null);
                                if (pf_val == "1")
                                {
                                    if (Convert.ToDecimal(Basic) >= Convert.ToDecimal(PF_Salary))
                                    {
                                        PFEarnings = PF_Salary;
                                    }
                                    else
                                    {
                                        PFEarnings = (Convert.ToDecimal(Basic) + Convert.ToDecimal(FDA) + Convert.ToDecimal(VDA)).ToString();
                                    }
                                }
                                else
                                {
                                    PFEarnings = Basic;
                                }
                                objSal.PfSalary = PFEarnings;
                                if (Convert.ToDecimal(objSal.PfSalary) > 6500)
                                {
                                    objSal.Emp_PF = "6500";
                                }
                                else
                                {
                                    objSal.Emp_PF = objSal.PfSalary;
                                }
                                Isregistered = true;

                            }
                            #endregion
                            #region EmpType-5
                            else
                            {
                                FixedBasic = Basic_temp;
                                FixedFDA = FDA_temp;
                                FixedHRA = HRA_temp;
                                Basic = ((Convert.ToDecimal(Basic_temp)) / 26).ToString();
                                Basic = ((Convert.ToDecimal(txtDays) + Convert.ToDecimal(cl) + Convert.ToDecimal(NFh)) * (Convert.ToDecimal(Basic))).ToString();
                                Basic = (Math.Round(Convert.ToDecimal(Basic), 2, MidpointRounding.ToEven)).ToString();
                                FDA = ((Convert.ToDecimal(FDA_temp)) / 26).ToString();
                                FDA = ((Convert.ToDecimal(txtDays) + Convert.ToDecimal(cl) + Convert.ToDecimal(NFh)) * (Convert.ToDecimal(FDA))).ToString();
                                FDA = (Math.Round(Convert.ToDecimal(FDA), 2, MidpointRounding.ToEven)).ToString();
                                //decimal vdaval = (Convert.ToDecimal(dtpf.Rows[0]["VDA1"].ToString()) - Convert.ToDecimal(dtpf.Rows[0]["VDA2"].ToString()));
                                VDA = ((Convert.ToDecimal(VDA_point) * Convert.ToDecimal(VDA_temp)) / 26).ToString();
                                VDA = ((Convert.ToDecimal(txtDays) + Convert.ToInt32(cl) + Convert.ToInt32(NFh)) * (Convert.ToDecimal(VDA))).ToString();
                                VDA = (Math.Round(Convert.ToDecimal(VDA), 2, MidpointRounding.ToEven)).ToString();
                                HRA = (Convert.ToDecimal(HRA_temp) / 26).ToString();
                                HRA = ((Convert.ToDecimal(txtDays) + Convert.ToInt32(cl)) * (Convert.ToDecimal(HRA))).ToString();
                                HRA = (Math.Round(Convert.ToDecimal(HRA), 0, MidpointRounding.ToEven)).ToString();
                                All1 = (Convert.ToDecimal(All1_temp)).ToString();
                                All1 = ((Convert.ToDecimal(txtDays)) * Convert.ToDecimal(All1)).ToString();
                                All1 = (Math.Round(Convert.ToDecimal(All1), 0, MidpointRounding.AwayFromZero)).ToString();
                                All2 = (Convert.ToDecimal(All2_temp) / 26).ToString();

                                All2 = ((Convert.ToDecimal(txtDays) + Convert.ToInt32(cl)) * Convert.ToDecimal(All2)).ToString();
                                All2 = (Math.Round(Convert.ToDecimal(All2), 0, MidpointRounding.AwayFromZero)).ToString();
                                Deduction1 = (Convert.ToDecimal(Ded1_temp) / 26).ToString();
                                Deduction1 = ((Convert.ToDecimal(txtDays) + Convert.ToInt32(cl)) * Convert.ToDecimal(Deduction1)).ToString();
                                Deduction1 = (Math.Round(Convert.ToDecimal(Deduction1), 0, MidpointRounding.AwayFromZero)).ToString();
                                Deduction2 = (Convert.ToDecimal(Ded2_temp) / 26).ToString();
                                Deduction2 = ((Convert.ToDecimal(txtDays) + Convert.ToInt32(cl)) * Convert.ToDecimal(Deduction2)).ToString();
                                Deduction2 = (Math.Round(Convert.ToDecimal(Deduction2), 0, MidpointRounding.AwayFromZero)).ToString();
                                All3 = "0";
                                All4 = "0";
                                Deduction3 = "0";
                                Deduction4 = "0";
                                if (pf_val == "1")
                                {
                                    if (pf_special == "1")
                                    {
                                        decimal PF_val_dum = (Convert.ToDecimal(Basic) + Convert.ToDecimal(FDA) + Convert.ToDecimal(VDA));
                                        if (PF_val_dum >= Convert.ToDecimal(PF_Salary))
                                        {
                                            PF = ((Convert.ToDecimal(PF_Salary) * Convert.ToDecimal(PF_Per)) / 100).ToString();
                                            PF = (Math.Round(Convert.ToDecimal(PF), 0, MidpointRounding.AwayFromZero)).ToString();
                                        }
                                        else
                                        {
                                            decimal PF_val1 = (Convert.ToDecimal(Basic) + Convert.ToDecimal(FDA) + Convert.ToDecimal(VDA));
                                            PF = ((PF_val1 * Convert.ToDecimal(PF_Per)) / 100).ToString();
                                            PF = (Math.Round(Convert.ToDecimal(PF), 0, MidpointRounding.AwayFromZero)).ToString();
                                        }
                                    }
                                    else
                                    {
                                        decimal PF_val1 = (Convert.ToDecimal(Basic) + Convert.ToDecimal(FDA) + Convert.ToDecimal(VDA));
                                        PF = ((PF_val1 * Convert.ToDecimal(PF_Per)) / 100).ToString();
                                        PF = (Math.Round(Convert.ToDecimal(PF), 0, MidpointRounding.AwayFromZero)).ToString();
                                    }
                                }
                                else
                                {
                                    PF = "0";
                                }
                                if (ESI_val == "1")
                                {
                                    decimal ESI_val_dum = (Convert.ToDecimal(Basic) + Convert.ToDecimal(FDA) + Convert.ToDecimal(VDA) + Convert.ToDecimal(HRA));
                                    ESI = ((Convert.ToDecimal(ESI_val_dum) * Convert.ToDecimal(ESI_per)) / 100).ToString();
                                    ESI = (Math.Round(Convert.ToDecimal(ESI), 2, MidpointRounding.ToEven)).ToString();
                                    string[] split_val = ESI.Split('.');
                                    string Ist = split_val[0].ToString();
                                    string IInd = split_val[1].ToString();
                                    if (Convert.ToInt32(IInd) > 0)
                                    {
                                        ESI = (Convert.ToInt32(Ist) + 1).ToString();
                                    }
                                    else
                                    {
                                        ESI = Ist;
                                    }
                                }
                                else
                                {
                                    ESI = "0";
                                }
                                if (Union_val == "1")
                                {

                                }
                                else
                                {
                                    union = "0";
                                }
                                GrossEarnings = (Convert.ToDecimal(Basic) + Convert.ToDecimal(FDA) + Convert.ToDecimal(VDA) + Convert.ToDecimal(HRA) + Convert.ToDecimal(OT) + Convert.ToDecimal(All1) + Convert.ToDecimal(All2) + Convert.ToDecimal(All3) + Convert.ToDecimal(All4)).ToString();
                                grossDeduction = (Convert.ToDecimal(PF) + Convert.ToDecimal(ESI) + Convert.ToDecimal(stamp) + Convert.ToDecimal(union) + Convert.ToDecimal(Advance) + Convert.ToDecimal(Deduction1) + Convert.ToDecimal(Deduction2) + Convert.ToDecimal(Deduction3) + Convert.ToDecimal(Deduction4)).ToString();
                                NetPay = (Convert.ToDecimal(GrossEarnings) - Convert.ToDecimal(grossDeduction)).ToString();
                                NetPay = Math.Round(Convert.ToDecimal(NetPay), 0, MidpointRounding.AwayFromZero).ToString();
                                TextAmount = NumerictoNumber(Convert.ToInt32(NetPay), isUK).ToString() + " " + "Only";
                                objSal.TotalWorkingDays = work;
                                objSal.NFh = NFh;
                                objSal.Lcode = SessionLcode;
                                objSal.Ccode = SessionCcode;
                                objSal.ApplyLeaveDays = AbsentDays;

                                objSal.AvailableDays = txtDays;
                                objSal.SalaryDate = txtsalaryday.Text;
                                objSal.work = txtDays;
                                objSal.weekoff = Weekoff;
                                objSal.CL = cl;
                                objSal.BasicAndDA = Basic;
                                objSal.HRA = HRA;
                                objSal.FDA = FDA;
                                objSal.VDA = VDA;
                                objSal.OT = OT;
                                objSal.Allowances1 = All1;
                                objSal.Allowances2 = All2;
                                objSal.Allowances3 = "0";
                                objSal.Allowances4 = "0";
                                objSal.Deduction1 = Deduction1;
                                objSal.Deduction2 = Deduction2;
                                objSal.Deduction3 = "0";
                                objSal.Deduction4 = "0";
                                objSal.ProvidentFund = PF;
                                objSal.ESI = ESI;
                                objSal.Stamp = stamp;
                                objSal.Advance = Advance;
                                objSal.LossofPay = "0.00";
                                objSal.Union = union;
                                objSal.GrossEarnings = GrossEarnings;
                                objSal.TotalDeduction = grossDeduction;
                                objSal.NetPay = NetPay;
                                objSal.Words = TextAmount;
                                objSal.SalaryDate = txtsalaryday.Text;
                                objSal.FixedBase = FixedBasic;
                                objSal.FixedFDA = FixedFDA;
                                objSal.FixedFRA = FixedHRA;
                                MyDate = DateTime.ParseExact(objSal.SalaryDate, "dd-MM-yyyy", null);
                                if (pf_val == "1")
                                {
                                    if (Convert.ToDecimal(Basic) >= Convert.ToDecimal(PF_Salary))
                                    {
                                        PFEarnings = PF_Salary;
                                    }
                                    else
                                    {
                                        PFEarnings = (Convert.ToDecimal(Basic) + Convert.ToDecimal(FDA) + Convert.ToDecimal(VDA)).ToString();
                                    }
                                }
                                else
                                {
                                    PFEarnings = Basic;
                                }
                                objSal.PfSalary = PFEarnings;
                                if (Convert.ToDecimal(objSal.PfSalary) > 6500)
                                {
                                    objSal.Emp_PF = "6500";
                                }
                                else
                                {
                                    objSal.Emp_PF = objSal.PfSalary;
                                }
                                Isregistered = true;

                            }
                            #endregion
                            if (Isregistered == true)
                            {
                                if (Adv_id != null)
                                {
                                    if (Convert.ToInt32(Dec_mont) == 1)
                                    {
                                        if ((Convert.ToDecimal(Adv_BalanceAmt) == Convert.ToDecimal(objSal.Advance)) || (Convert.ToDecimal(objSal.Advance) == 0))
                                        {
                                            if (Convert.ToDecimal(objSal.Advance) == 0)
                                            {
                                                objdata.Advance_repayInsert(EmpNo, MyDate, Mont, objSal.Advance, Adv_id, SessionCcode, SessionLcode);
                                                string bal = (Convert.ToDecimal(Adv_BalanceAmt) - Convert.ToDecimal(objSal.Advance)).ToString();
                                                string decrementMonths = Convert.ToInt32(Dec_mont).ToString();
                                                string IncrementMonths = (Convert.ToInt32(Increment_mont) + 1).ToString();
                                                string Completed = "N";

                                                objdata.Advance_update(EmpNo, bal, IncrementMonths, decrementMonths, Adv_id, Completed, MyDate, SessionCcode, SessionLcode);
                                            }
                                            else
                                            {
                                                objdata.Advance_repayInsert(EmpNo, MyDate, Mont, objSal.Advance, Adv_id, SessionCcode, SessionLcode);
                                                    string bal = (Convert.ToDecimal(Adv_BalanceAmt) - Convert.ToDecimal(objSal.Advance)).ToString();
                                                string decrementMonths = (Convert.ToInt32(Dec_mont) - 1).ToString();
                                                string IncrementMonths = (Convert.ToInt32(Increment_mont) + 1).ToString();
                                                string Completed = "Y";

                                                objdata.Advance_update(EmpNo, bal, IncrementMonths, decrementMonths, Adv_id, Completed, MyDate, SessionCcode, SessionLcode);
                                            }
                                            objdata.SalaryDetails(objSal, EmpNo, Exist, MyDate, Mont, Year, ddlFinance.SelectedValue, TransDate, Mydate1, MyDate2);
                                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert(' " + Mont + " Month Salary has been processed');", true);
                                            //System.Windows.Forms.MessageBox.Show(MyMonth + " Month Salary has been processed", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                        }
                                    }
                                    else
                                    {
                                        bool Err = false;
                                        if (Convert.ToDecimal(objSal.Advance) > Convert.ToDecimal(Adv_BalanceAmt))
                                        {
                                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Enter the Advance amount Properly');", true);
                                            //System.Windows.Forms.MessageBox.Show(MyMonth + "Enter the Advance Amount Proprly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                            Err = true;
                                        }
                                        else if (Convert.ToDecimal(objSal.Advance) == Convert.ToDecimal(Adv_BalanceAmt))
                                        {
                                            objdata.Advance_repayInsert(EmpNo, MyDate, Mont, objSal.Advance, Adv_id, SessionCcode, SessionLcode);
                                            string bal = (Convert.ToDecimal(Adv_BalanceAmt) - Convert.ToDecimal(objSal.Advance)).ToString();
                                            string decrementMonths = (Convert.ToInt32(Dec_mont) - 1).ToString();
                                            string IncrementMonths = (Convert.ToInt32(Increment_mont) + 1).ToString();
                                            string Completed = "Y";

                                            objdata.Advance_update(EmpNo, bal, IncrementMonths, decrementMonths, Adv_id, Completed, MyDate, SessionCcode, SessionLcode);
                                        }
                                        else if (Convert.ToDecimal(objSal.Advance) == 0)
                                        {
                                            objdata.Advance_repayInsert(EmpNo, MyDate, Mont, objSal.Advance, Adv_id, SessionCcode, SessionLcode);
                                            string bal = (Convert.ToDecimal(Adv_BalanceAmt) - Convert.ToDecimal(objSal.Advance)).ToString();
                                            string decrementMonths = Convert.ToInt32(Dec_mont).ToString();
                                            string IncrementMonths = (Convert.ToInt32(Increment_mont) + 1).ToString();
                                            string Completed = "N";

                                            objdata.Advance_update(EmpNo, bal, IncrementMonths, decrementMonths, Adv_id, Completed, MyDate, SessionCcode, SessionLcode);
                                        }
                                        else
                                        {
                                            objdata.Advance_repayInsert(EmpNo, MyDate, Mont, objSal.Advance, Adv_id, SessionCcode, SessionLcode);
                                            string bal = (Convert.ToDecimal(Adv_BalanceAmt) - Convert.ToDecimal(objSal.Advance)).ToString();
                                            string decrementMonths = (Convert.ToInt32(Dec_mont) - 1).ToString();
                                            string IncrementMonths = (Convert.ToInt32(Increment_mont) + 1).ToString();
                                            string Completed = "N";

                                            objdata.Advance_update(EmpNo, bal, IncrementMonths, decrementMonths, Adv_id, Completed, MyDate, SessionCcode, SessionLcode);
                                        }
                                        if (!Err)
                                        {
                                            objdata.SalaryDetails(objSal, EmpNo, dtexist, MyDate, Mont, Year, ddlFinance.SelectedValue, TransDate, Mydate1, MyDate2);
                                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert(' " + Mont + " Month Salary has been processed');", true);
                                            //System.Windows.Forms.MessageBox.Show(MyMonth + " Month Salary has been processed", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                        }
                                    }
                                }
                                else
                                {
                                    objdata.SalaryDetails(objSal, EmpNo, dtexist, MyDate, Mont, Year, ddlFinance.SelectedValue, TransDate, Mydate1, MyDate2);
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert(' " + Mont + " Month Salary has been processed');", true);
                                    //System.Windows.Forms.MessageBox.Show(MyMonth + " Month Salary has been processed", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                }
                                
                            }
                            
                        }

                    }
                }
            }
            clr();
        }
        catch (Exception ex)
        {
        }
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        clr();
        txtsalaryday.Text = "";
        ddlcategory.SelectedValue = "0";
        ddldept.SelectedValue = "0";
    }
}
