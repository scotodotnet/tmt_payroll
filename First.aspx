﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="First.aspx.cs" Inherits="ThirdThems_First" %>

<%@ Register  Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">


<!-- Mirrored from www.ait.sk/uniadmin/ by HTTrack Website Copier/3.x [XR&CO'2010], Tue, 20 Jul 2010 00:37:25 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8"><!-- /Added by HTTrack -->
<head id="hea" runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<meta name="description"  content=""/>
<meta name="keywords" content=""/>
<meta name="robots" content="ALL,FOLLOW"/>
<meta name="Author" content="AIT"/>
<meta http-equiv="imagetoolbar" content="no"/>
<title>Payroll Management Systems</title>

<link rel="stylesheet" href="css/reset.css" type="text/css"/>
<link rel="stylesheet" href="css/screen.css" type="text/css"/>
<link rel="stylesheet" href="css/fancybox.css" type="text/css"/>
<link rel="stylesheet" href="css/jquery.wysiwyg.css" type="text/css"/>
<link rel="stylesheet" href="css/jquery.ui.css" type="text/css"/>
<link rel="stylesheet" href="css/visualize.css" type="text/css"/>
<link rel="stylesheet" href="css/visualize-light.css" type="text/css"/>
<link rel="Stylesheet" href="css/form.css" type="text/css" />
<!--[if IE 7]>
	<link rel="stylesheet" type="text/css" href="css/ie7.css" />
<![endif]-->	

<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery.visualize.js"></script>
<script type="text/javascript" src="js/jquery.wysiwyg.js"></script>
<script type="text/javascript" src="js/tiny_mce/jquery.tinymce.js"></script>
<script type="text/javascript" src="js/jquery.fancybox.js"></script>
<script type="text/javascript" src="js/jquery.idtabs.js"></script>
<script type="text/javascript" src="js/jquery.datatables.js"></script>
<script type="text/javascript" src="js/jquery.jeditable.js"></script>
<script type="text/javascript" src="js/jquery.ui.js"></script>

<script type="text/javascript" src="js/excanvas.js"></script>
<script type="text/javascript" src="js/cufon.js"></script>
<script type="text/javascript" src="js/Geometr231_Hv_BT_400.font.js"></script>
<script type="text/javascript" src="js/script.js"></script>

</head>

<body>
<div class="clear">
	
	<div class="sidebar"> <!-- *** sidebar layout *** -->
		<div class="logo clear">
			
				<img src="images/logo.png" alt="" class="picture" />
				<span class="title">Altius Infosystems</span>
				<span class="text">Altius</span>
			
		</div>
		
		<div class="menu">
			<ul>
			<li><a href="#">Employee Registration</a></li>
		
			<li>
				<a href="#">Official Profile</a>
			<%--	<ul>
					<li><a href="#">View All Pages</a></li>
					<li><a href="#">Create New Page</a></li>
					<li><a href="#">Import Pages</a></li>
					<li><a href="#">Export Pages</a></li>
					<li><a href="#">Create Backup</a></li>
				</ul>--%>				
			</li>
			
			<li><a href="#">Leave Details</a></li>
			<li><a href="#">Salary Details</a></li>
			<li><a href="#">Reports</a></li>
			</ul>
		</div>
	</div>
	
	
	<div class="main"> <!-- *** mainpage layout *** -->
	<div class="main-wrap">
		<div class="header clear">
			<ul class="links clear">
			<li></li>
			<li><a href="#"> <span class="text"></span></a></li>
			<li><a href="#"><span class="text"> </span></a></li>
			<li><a href="#"> <span class="text"></span></a></li>
			</ul>
		</div>
		
		<div class="page clear">
						<div class="main-icons clear">
				<ul class="clear">
				<li><a href="#"><img src="images/ico_folder_64.png" class="icon" alt="" /><span class="text">Employee Registration</span></a></li>
				<li><a href="#"><img src="images/ico_page_64.png" class="icon" alt="" /><span class="text">Official Profile</span></a></li>
				<li><a href="#"><img src="images/date/calendar_week .png" class="icon" alt="" /><span class="text">Leave Details</span></a></li>
				<li><a href="#"><img src="images/ico_clock_64.png" class="icon" alt="" /><span class="text">Salary Details</span></a></li>
				<li><a href="#"><img src="images/ico_users_64.png" class="icon" alt="" /><span class="text">Users</span></a></li>
				<li><a href="#modal" class="modal-link"><img src="images/ico_chat_64.png" class="icon" alt="" /><span class="text">Reports</span></a></li>
				</ul>
			</div>
			
			<!-- MODAL WINDOW -->
			<div id="modal" class="modal-window">
				<!-- <div class="modal-head clear"><a onclick="$.fancybox.close();" href="javascript:;" class="close-modal">Close</a></div> -->
				
				<div class="notification note-info">
					<a href="#" class="close" title="Close notification"><span>close</span></a>
					<span class="icon"></span>
					
				</div>
				
				</div>
			
			<!-- CONTENT BOXES -->
			<div class="content-box">
				<div class="box-header clear">
				<%--	<ul class="tabs clear">
						<li><a href="#data-table"></a></li>
						<li><a href="#table"></a></li>
						<li><a href="#forms"></a></li>
					</ul>--%>
					
					
				</div>
				
				<div class="box-body clear">
					<!-- TABLE -->
					<div id="data-table">
  <form id="form1" runat="server" class="form" >
  <cc1:ToolkitScriptManager runat="server" EnableScriptGlobalization ="true" EnableScriptLocalization ="true" ID="ScriptManager1" EnablePartialRendering ="true"></cc1:ToolkitScriptManager>

					<table align="center" cellpadding="0" cellspacing="0" border="0"  >
        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
    
    <tr >
    <td colspan="5"><asp:Image ID="Image7" runat="server" ImageUrl="~/Images/citybg.gif" Width="800px" Height="5px" /></td>
    </tr>
  <%--  <tr>
    <td rowspan="3"><asp:Image ID="imge" runat="server" ImageUrl="~/Images/tabsep.jpg" Width="2px" Height="100px" /></td>
    </tr>--%>
    
    <tr>
    
    <td colspan="5" align="center"><div class="logo1 "><span class="HeaderText" >Personal Details</span></div>
   </td>
    </tr>
    
    <tr >
    <td><asp:Label ID="lblempname" runat="server" Text="Employee Name"  Font-Size="17px" ></asp:Label>
    <cc1:FilteredTextBoxExtender ID="FilterTextboxExtenderempname" runat="server" FilterMode="ValidChars" FilterType="Custom" TargetControlID="txtempname" ValidChars="a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,.A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z"></cc1:FilteredTextBoxExtender>
    </td>
    <td class="input-height"><asp:TextBox ID="txtempname" runat="server" ></asp:TextBox></td>
    <td><asp:Label ID="lblempfname" runat="server" Text="Father Name" Font-Bold="true"  ></asp:Label></td>
    <td><asp:TextBox ID="txtempfname" runat="server" ></asp:TextBox>
        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR_FatherName" runat="server" FilterMode="ValidChars" FilterType="Custom" TargetControlID="txtempfname" ValidChars="a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,.A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z"></cc1:FilteredTextBoxExtender>

    </td>
    </tr >
    <tr>
    <td><asp:Label ID="lblgender" runat="server" Text ="Gender" Font-Bold="true"  ></asp:Label></td>
    <td ><asp:RadioButtonList ID="rbtngender" runat="server" RepeatColumns="2" >
    <asp:ListItem Selected="True" Text="Male" Value="1"></asp:ListItem>
    <asp:ListItem Selected ="False" Text="Female" Value="2"></asp:ListItem>
    </asp:RadioButtonList></td>
    <td><asp:Label ID="lbldob" runat="server" Text="Date of Birth" Font-Bold="true"  ></asp:Label></td>
        <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
        
    <td><asp:TextBox ID="txtdob" runat="server" Width="150" ></asp:TextBox>
    <cc1:CalendarExtender ID="Caleder_txtdob" runat="server" TargetControlID="txtdob" Format ="dd/MM/yyyy" CssClass ="CalendarCSS" Enabled="true"></cc1:CalendarExtender>
    </ContentTemplate>
        </asp:UpdatePanel>
    
    </td>
    </tr>
    <tr>
    <td colspan="5"><asp:Image ID="img1" runat="server" ImageUrl="~/Images/citybg.gif" Width="800" Height="5px" /></td>
    </tr>
    
    
    
    <tr >
    <td colspan="2" align="left"><asp:Label ID="lblpresent" runat="server" Text="Present Address" Font-Bold="true"   ></asp:Label></td>
    <td colspan="2" align="left"><asp:CheckBox ID="ckbpmadd" runat="server" AutoPostBack="True" 
             /><asp:Label ID="lblperment" runat="server" Text="Permenant Address"  Font-Bold="true" ></asp:Label></td>
    </tr>
    <tr >
    <td><asp:Label ID="lblPSadd1" runat="server" Text="PSAddress1" Font-Bold="true" ></asp:Label></td>
    <td><asp:TextBox ID="txtPSAdd1" runat="server" ></asp:TextBox></td>
    <td><asp:Label ID="lblPMAdd1" runat="server" Text="PMAddress1" Font-Bold="true" ></asp:Label></td>
    <td><asp:TextBox ID="txtPMAdd1" runat="server" ></asp:TextBox></td>
    </tr>
        <tr >
    <td><asp:Label ID="lblPSAdd2" runat="server" Text="PSAddress2" Font-Bold="true" ></asp:Label></td>
    <td><asp:TextBox ID="txtPSAdd2" runat="server" ></asp:TextBox></td>
    <td><asp:Label ID="lblPMAdd2" runat="server" Text="PMAddress2" Font-Bold="true" ></asp:Label></td>
    <td><asp:TextBox ID="txtPMAdd2" runat="server" ></asp:TextBox></td>
    </tr>
        <tr >
    <td><asp:Label ID="lblPSAdd3" runat="server" Text="PSAddress3" Font-Bold="true" ></asp:Label></td>
    <td><asp:TextBox ID="txtPSAdd3" runat="server" ></asp:TextBox></td>
    <td><asp:Label ID="lblPMAdd3" runat="server" Text="PMAddress3" Font-Bold="true" ></asp:Label></td>
    <td><asp:TextBox ID="txtPMAdd3" runat="server" ></asp:TextBox></td>
    </tr>
    <tr >
    <td><asp:Label ID="lblPSDistrict" runat="server" Text="PSDistrict" Font-Bold="true" ></asp:Label></td>
    <td><asp:DropDownList ID="ddlPSDistrict" runat="server" Width="150" Height="30"
            AutoPostBack="True" 
              ></asp:DropDownList></td>
    <td><asp:Label ID="lblPMDistrict" runat="server" Text="PMDistrict" Font-Bold="true" ></asp:Label></td>
    <td><asp:DropDownList ID="ddlPMDistrict" runat="server" Width="150" Height="30"
            AutoPostBack="True" 
              ></asp:DropDownList></td>
    </tr>
     <tr >
    <td><asp:Label ID="lblPSTaluk" runat="server" Text="PSTaluk" Font-Bold="true" ></asp:Label></td>
    <td><asp:DropDownList ID="ddlPSTaluk" runat="server"  Width="150" Height="30" ></asp:DropDownList></td>
    <td><asp:Label ID="lblPMTaluk" runat="server" Text="PMTaluk" Font-Bold="true" ></asp:Label></td>
    <td><asp:DropDownList ID="ddlPMTaluk" runat="server"   Width="150" Height="30"></asp:DropDownList></td>
    </tr>
    <tr >
   <td><asp:Label ID="lblpsstate" runat="server" Text="PSState" Font-Bold="true" ></asp:Label></td>
   <td><asp:TextBox ID="txtpsstate" runat="server" ></asp:TextBox>
         <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR_PSState" runat="server" FilterMode="ValidChars" FilterType="Custom" TargetControlID="txtpsstate" ValidChars="a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,.A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z"></cc1:FilteredTextBoxExtender>

   </td>
   <td><asp:Label ID="lblPMstate" runat="server" Text="PMState" Font-Bold="true" ></asp:Label></td>
   <td><asp:TextBox ID="txtPMState" runat="server" ></asp:TextBox>
            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR_PMState" runat="server" FilterMode="ValidChars" FilterType="Custom" TargetControlID="txtPMState" ValidChars="a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,.A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z"></cc1:FilteredTextBoxExtender>

   </td>
    </tr>
     <tr >
    <td><asp:Label ID="lblnationlity" runat="server" Text="Nationality" Font-Bold="true" ></asp:Label></td>
    <td><asp:TextBox ID="txtnationality" runat="server" ></asp:TextBox>
                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR_Nationality" runat="server" FilterMode="ValidChars" FilterType="Custom" TargetControlID="txtnationality" ValidChars="a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,.A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z"></cc1:FilteredTextBoxExtender>

    </td>
    <td><asp:Label ID="lblmarital" runat="server" Text="Marital Status" Font-Bold="true" ></asp:Label></td>
    <td><asp:RadioButtonList ID="rbtnmaritalstatus" runat="server" RepeatColumns="2">
    <asp:ListItem Selected="True" Text="UnMarried" Value="1"></asp:ListItem>
    <asp:ListItem Selected="False" Text="Married" Value="2"></asp:ListItem>
    </asp:RadioButtonList></td>
    </tr>
    <tr >
    <td><asp:Label ID="lblphone" runat="server" Text="Phone" Font-Bold="true" ></asp:Label></td>
    <td><asp:TextBox ID="txtphone" runat="server" MaxLength="15" ></asp:TextBox>
     <cc1:FilteredTextBoxExtender ID="FilterTextboxexteder" runat="server" TargetControlID="txtphone" FilterMode="ValidChars" FilterType="Custom" ValidChars="0,1,2,3,4,5,6,7,8,9,+,-"></cc1:FilteredTextBoxExtender>
    </td>
    <td><asp:Label ID="lblmobile" runat="server" Text="Mobile" Font-Bold="true"  ></asp:Label></td>
    <td><asp:TextBox ID="txtmobile" runat="server" MaxLength="10" ></asp:TextBox>
    <cc1:FilteredTextBoxExtender ID="FilterTextboxMobile" runat="server" TargetControlID="txtmobile" FilterMode="ValidChars" FilterType="Custom" ValidChars ="0,1,2,3,4,5,6,7,8,9,+,-"></cc1:FilteredTextBoxExtender>
    </td>
    </tr>
    <tr >
    <td><asp:Label ID="lblppno" runat="server" Text="Passport No" Font-Bold="true" ></asp:Label></td>
    <td><asp:TextBox ID="txtppno" runat="server" CssClass="text"></asp:TextBox></td>
    <td><asp:Label ID="lbldrivingno" runat="server" Text="Driving License No" Font-Bold="true" ></asp:Label></td>
    <td><asp:TextBox ID="txtdrivingno" runat="server" CssClass="text"></asp:TextBox></td>
    </tr>
    <tr >
    <td><asp:Label ID="lblbloodgp" runat="server" Text="Blood Group" Font-Bold="true" ></asp:Label></td>
    <td colspan="3"><asp:TextBox ID="txtbloodgp" runat="server" CssClass="text"></asp:TextBox></td>
    </tr>
    <tr>
    <td colspan="5"><asp:Image ID="Image8" runat="server" ImageUrl="~/Images/citybg.gif" Width="800" Height="5px" /></td>
    </tr>
    <tr>
    <td colspan ="5" align="center"><div class="logo1 "><span class="HeaderText" >Education Details</span></div>
    </td>
    </tr>
    <tr >
    <td><asp:Label ID="lblqualif" runat="server" Text="Qualification" Font-Bold="true" ></asp:Label></td>
    <td><asp:DropDownList ID="ddlqualification" runat="server" CssClass="Drp" Width="150" Height="30" ></asp:DropDownList></td>
    <td><asp:Label ID="lblunive" runat="server" Text="Institude/University" Font-Bold="true" ></asp:Label></td>
    <td><asp:TextBox ID="txtuniversity" runat="server" CssClass="text"></asp:TextBox></td>
    </tr>
    <tr>
    <td colspan="5"><asp:Image ID="Image9" runat="server" ImageUrl="~/Images/citybg.gif" Width="800" Height="5px" /></td>
    </tr>
    <tr>
    <td colspan="5" align="center"><div class="logo1 "><span class="HeaderText" >Company Details</span></div>
    </td>
    
    </tr>
    <tr>
    <td><asp:Label ID="lblcomname" runat="server" Text="Company Name" Font-Bold="true" ></asp:Label></td>
    <td><asp:TextBox ID="txtcompanynm" runat="server" CssClass="text"></asp:TextBox>
    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR_CompanyName" runat="server" FilterMode="ValidChars" FilterType="Custom" TargetControlID="txtcompanynm" ValidChars="a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,.A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z"></cc1:FilteredTextBoxExtender>

    </td>
    <td><asp:Label ID="lblcomdept" runat="server" Text="Department" Font-Bold="true" ></asp:Label></td>
    <td><asp:TextBox ID="txtcomdept" runat="server" CssClass="text"></asp:TextBox>
        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR_Department" runat="server" FilterMode="ValidChars" FilterType="Custom" TargetControlID="txtcomdept" ValidChars="a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,.A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z"></cc1:FilteredTextBoxExtender>

    </td>
    </tr>
    <tr >
    <td><asp:Label ID="lbldesignation" runat="server" Text="Designation" Font-Bold="true" ></asp:Label></td>
    <td><asp:TextBox ID="txtdesignation" runat="server" CssClass="text"></asp:TextBox>
            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR_Designation" runat="server" FilterMode="ValidChars" FilterType="Custom" TargetControlID="txtdesignation" ValidChars="a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,.A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z"></cc1:FilteredTextBoxExtender>

    </td>
    <td><asp:Label ID="lblshift" runat="server" Text="Shift" Font-Bold="true" ></asp:Label></td>
    <td><asp:RadioButtonList ID="rbtnshift" runat="server" RepeatColumns="2">
    <asp:ListItem Selected="True" Text="Day" Value="1"></asp:ListItem>
    <asp:ListItem Selected="False" Text="Night" Value="2"></asp:ListItem>
    </asp:RadioButtonList></td>
    </tr>
    <tr >
    <td><asp:Label ID="lblemptype" runat="server" Text="Employee Type" Font-Bold="true" ></asp:Label></td>
    <td><asp:DropDownList ID="ddemployeetype" runat="server" CssClass="Drp" Width="150" Height="30" ></asp:DropDownList></td>
    <td><asp:Label ID="lblrolecd" runat="server" Text="Role Code" Font-Bold="true" ></asp:Label></td>
    <td><asp:TextBox ID="txtrolecd" runat="server" CssClass="text"></asp:TextBox>
        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender_RoleCd" runat="server" TargetControlID="txtrolecd" FilterMode="ValidChars" FilterType="Custom" ValidChars ="0,1,2,3,4,5,6,7,8,9,+,-"></cc1:FilteredTextBoxExtender>

    </td>
    </tr>
    <tr >
    <td><asp:Label ID="lblgrade" runat="server" Text="Grade" Font-Bold="true" ></asp:Label></td>
    <td><asp:RadioButtonList ID="rbtngrade" runat="server" RepeatColumns="3">
    <asp:ListItem Selected="True" Text="A" Value="A"></asp:ListItem>
    <asp:ListItem Selected ="False" Text="B" Value="B"></asp:ListItem>
    <asp:ListItem Selected="False" Text ="C" Value ="C"></asp:ListItem>
    
    </asp:RadioButtonList></td>
    <td><asp:Label ID="lblexisiting" runat="server" Text="Exisiting No" Font-Bold="true" ></asp:Label></td>
    <td><asp:TextBox ID="txtexistiongno" runat="server" CssClass="text"></asp:TextBox>
            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender_ExisistingCode" runat="server" TargetControlID="txtexistiongno" FilterMode="ValidChars" FilterType="Custom" ValidChars ="0,1,2,3,4,5,6,7,8,9,+,-"></cc1:FilteredTextBoxExtender>

    </td>
    </tr>
    
    <tr>
    <td colspan="5"><asp:Image ID="Image10" runat="server" ImageUrl="~/Images/citybg.gif" Width="800" Height="5px" /></td>
    </tr>
    <tr class="tabletr">
    <td><asp:Button ID="btnsave" runat="server" Text="Save"  /></td>
    <td><asp:Button ID="btncancel" runat ="server" Text="Cancel" /></td>
    </tr>
    </ContentTemplate>
        </asp:UpdatePanel>
    </table>
					
					</form>
					
					
						<form method="post" action="#">
						
					
						
					
						</form>
					</div><!-- /#table -->
					
					<!-- TABLE -->
					<!-- /#table -->
					
					<!-- Custom Forms -->
					<!-- /#forms -->
				</div> <!-- end of box-body -->
			</div> <!-- end of content-box -->
			
		
			
		
			
			
			
			
		</div><!-- end of page -->
		
		<div class="footer clear">
			<span class="copy"><strong>© 2012 Copyright by <a href="http://www.altius.co.in"/>Altius Infosystems.</a></strong></span> Powered by <a href="http://www.altius.co.in/">Altius.</a>
		</div>
	</div>
	</div>
</div>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-12958851-7']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</body>

<!-- Mirrored from www.ait.sk/uniadmin/ by HTTrack Website Copier/3.x [XR&CO'2010], Tue, 20 Jul 2010 00:38:01 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8"><!-- /Added by HTTrack -->
</html>
